<div class="largeColumn">
    <div class="col_padding">

        <?php if ($post['id']) { ?>

            <?php if (count($post['images']) > 0) { ?>
                <link rel="stylesheet" href="/extensions/<?=$_uccms_blog->Extension;?>/css/lib/swipebox.css">
                <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.1/js/jquery.swipebox.min.js"></script>
            <?php } ?>

            <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_blog->Extension;?>/css/master/post.css" />
            <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_blog->Extension;?>/css/custom/post.css" />
            <script src="/extensions/<?=$_uccms_blog->Extension;?>/js/master/post.js"></script>
            <script src="/extensions/<?=$_uccms_blog->Extension;?>/js/custom/post.js"></script>

            <div class="post">

                <?php if ($post['title']) { ?>
                    <h1 class="title"><?php echo stripslashes($post['title']); ?></h1>
                <?php } ?>

                <?php if ((!$_uccms_blog->getSetting('post_hide_date')) || (!$_uccms_blog->getSetting('post_hide_author'))) { ?>
                    <div class="meta">
                        <?php if (!$_uccms_blog->getSetting('post_hide_date')) { ?>
                            <span class="date"><?php echo date('M j, Y', strtotime($post['dt_published'])); ?></span>
                        <?php } ?>
                        <?php if (!$_uccms_blog->getSetting('post_hide_author')) { ?>
                            <span class="author"><?php echo stripslashes($post['author']['name']); ?></span>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php if ($post['images'][0]['image']) { ?>
                    <div class="image main">
                        <a href="<?php echo $_uccms_blog->imageBridgeOut($post['images'][0]['image'], 'posts'); ?>" title="<?php echo $post['images'][0]['caption']; ?>" class="swipebox" rel="gallery-1"><img src="<?php echo $_uccms_blog->imageBridgeOut($post['images'][0]['image'], 'posts'); ?>" alt="<?php echo $post['images'][0]['caption']; ?>" /></a>
                    </div>
                    <?php
                    if (!$have_gallery_shortcode) {
                        include(SERVER_ROOT. 'extensions/' .$_uccms_blog->Extension. '/templates/routed/blog/elements/post_gallery.php');
                    }
                    ?>
                <?php } ?>

                <?php if ($post['content']) { ?>
                    <div class="content">
                        <?php echo stripslashes($post['content']); ?>
                    </div>
                <?php } ?>

                <?php if (count($post['categories']) > 0) { ?>
                    <div class="categories">
                        <?php
                        $cata = array();
                        foreach ($post['categories'] as $category) {
                            $cata[] = '<a href="' .$_uccms_blog->categoryURL($category['id'], $category). '">' .stripslashes($category['title']). '</a>';
                        }
                        echo 'Categories: ' .implode(', ', $cata);
                        ?>
                    </div>
                <?php } ?>

                <?php if (count($post['tags']) > 0) { ?>
                    <div class="tags">
                        <?php
                        $taga = array();
                        foreach ($post['tags'] as $tag) {
                            $taga[] = '<a href="' .$_uccms_blog->tagURL($tag['id'], $tag). '">' .stripslashes($tag['title']). '</a>';
                        }
                        echo 'Tags: ' .implode(', ', $taga);
                        ?>
                    </div>
                <?php } ?>

                <?php if (($prev_post['id']) || ($next_post['id'])) { ?>
                    <div class="prev_next clearfix">
                        <?php if ($prev_post['id']) { ?><div class="prev"><a href="<?php echo $_uccms_blog->postURL($prev_post['id'], $prev_post); ?>"><i class="fa fa-angle-left"></i> Previous Post<span class="title"><?php echo stripslashes($prev_post['title']); ?></span></a></div><?php } ?>
                        <?php if ($next_post['id']) { ?><div class="next"><a href="<?php echo $_uccms_blog->postURL($next_post['id'], $next_post); ?>">Next Post <i class="fa fa-angle-right"></i><span class="title"><?php echo stripslashes($next_post['title']); ?></span></a></div><?php } ?>
                    </div>
                <?php } ?>

                <?php include(SERVER_ROOT. 'extensions/' .$_uccms_blog->Extension. '/templates/routed/blog/elements/comments.php'); ?>

            </div>

            <?php if (count($post['images']) > 0) { ?>
                <script type="text/javascript">
                    ;(function($) {
                        $('.swipebox').swipebox({
                            removeBarsOnMobile: false
                        });
                    })(jQuery);
                </script>
            <?php } ?>

        <?php } else { ?>

            <h1>Blog</h1>

            <div class="noposts">
                Post not found.
            </div>

        <?php } ?>

    </div>
</div>