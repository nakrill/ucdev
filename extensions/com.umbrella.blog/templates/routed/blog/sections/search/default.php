<div class="largeColumn">
    <div class="col_padding">

        <h1>Search Results</h1>

        <?php if ($paging) { ?>

            <div class="paging top">
                <?php echo $paging; ?>
            </div>

            <div class="bold_heading">
                Showing results for "<?php echo $_GET['q']; ?>"
            </div>

            <div class="posts clearfix">
                <?php
                foreach ($posts as $post) {
                    include(SERVER_ROOT. 'extensions/' .$_uccms_blog->Extension. '/templates/routed/blog/elements/post.php');
                }
                ?>
            </div>

            <div class="paging bottom">
                <?php echo $paging; ?>
            </div>

        <?php } else { ?>

            <div class="bold_heading" style="clear: both;">
                Showing results for "<?php echo $_GET['q']; ?>"
            </div>

            <div class="noposts">
                No matches found.
            </div>

        <?php } ?>

    </div>
</div>