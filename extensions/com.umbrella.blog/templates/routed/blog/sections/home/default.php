<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_blog->Extension;?>/css/master/home.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_blog->Extension;?>/css/custom/home.css" />
<script src="/extensions/<?=$_uccms_blog->Extension;?>/js/master/home.js"></script>
<script src="/extensions/<?=$_uccms_blog->Extension;?>/js/custom/home.js"></script>

<div class="largeColumn">
    <div class="col_padding">

        <h1><?php echo $bigtree['resources']['heading']; ?></h1>

        <?php if ($paging) { ?>

            <div class="paging top">
                <?php echo $paging; ?>
            </div>

            <div class="posts clearfix">
                <?php
                foreach ($posts as $post) {
                    include(SERVER_ROOT. 'extensions/' .$_uccms_blog->Extension. '/templates/routed/blog/elements/post.php');
                }
                ?>
            </div>

            <div class="paging bottom">
                <?php echo $paging; ?>
            </div>

        <?php } else { ?>

            <div class="noposts">
                No posts.
            </div>

        <?php } ?>

    </div>
</div>