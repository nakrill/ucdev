<?php header('Content-Type: application/rss+xml; charset=UTF-8'); ?><rss version="2.0">
    <channel>
        <title><?php echo $cms->getSetting('site_name'); ?> Blog Feed</title>
        <link><?php echo WWW_ROOT . $_uccms_blog->frontendPath(); ?>/rss/</link>
        <description>RSS feed for <?php echo $cms->getSetting('site_name'); ?> blog.</description>
        <language>en-us</language>
        <generator>Umbrella Consultants CMS</generator>
        <?php

        $site_email_data = $cms->getSetting('bigtree-internal-email-service');
        $site_email = $site_email_data['settings']['bigtree_from'];

        // POST DATA
        $pda = $_uccms_blog->getPosts();

        // HAVE POSTS
        if (count($pda['posts']) > 0) {

            // AUTHOR ARRAY
            $aa = array();

            // LOOP
            foreach ($pda['posts'] as $post) {

                // GET AUTHOR INFO
                if (!$aa[$post['author_id']]) {
                    $aa[$post['author_id']] = $_uccms_blog->getAuthorInfo($post['author_id']);
                }

                ?>
                <item>
                    <guid isPermaLink="true"><?php echo substr(WWW_ROOT, 0, -1) . $_uccms_blog->postURL($post['id'], $post); ?></guid>
                    <title><![CDATA[<?php echo stripslashes($post['title']); ?>]]></title>
                    <description><![CDATA[<?php echo stripslashes($post['content']); ?>]]></description>
                    <link><?php echo substr(WWW_ROOT, 0, -1) . $_uccms_blog->postURL($post['id'], $post); ?></link>
                    <author><?php echo $site_email; if ($aa[$post['author_id']]['name']) { ?> (<?php echo stripslashes($aa[$post['author_id']]['name']); ?>)<?php } ?></author>
                    <pubDate><?=date('D, d M Y H:i:s T', strtotime($post['dt_published']))?></pubDate>
                </item>
                <?

            }

        }

        ?>
    </channel>
</rss>