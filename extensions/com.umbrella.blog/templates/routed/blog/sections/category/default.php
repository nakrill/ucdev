<div class="largeColumn">
    <div class="col_padding">

        <?php if ($category['id']) { ?>

            <h1><?php echo stripslashes($category['title']); ?></h1>

            <?php if ($paging) { ?>

                <div class="paging top">
                    <?php echo $paging; ?>
                </div>

                <div class="posts clearfix">
                    <?php
                    foreach ($posts as $post) {
                        include(SERVER_ROOT. 'extensions/' .$_uccms_blog->Extension. '/templates/routed/blog/elements/post.php');
                    }
                    ?>
                </div>

                <div class="paging bottom">
                    <?php echo $paging; ?>
                </div>

            <?php } else { ?>

                <div class="noposts">
                    No posts found in category.
                </div>

            <?php } ?>

        <?php } else { ?>

            <h1>Blog</h1>

            <div class="noposts">
                Category not found.
            </div>

        <?php } ?>

    </div>
</div>