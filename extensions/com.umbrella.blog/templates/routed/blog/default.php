<?php

// INCLUDE COMPOSER
include_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

// NAMESPACES
use MediaEmbed\MediaEmbed;

// MODULE CLASS
$_uccms_blog = new uccms_Blog;

// GET SETTING(S)
$blog['settings'] = $_uccms_blog->getSettings();

// SECTIONS DIRECTORY
$sections_dir = dirname(__FILE__). '/sections';

// DATA DIRECTORY
$data_dir = dirname(__FILE__). '/data';

// RSS
if ($bigtree['path'][1] == 'rss') {

    $bigtree['layout'] = 'blank';

    //include($data_dir. '/rss.php');
    include($sections_dir. '/rss/default.php');

// REST
} else {

    // MAKE BASE PAGE AND PAGE 1 SAME URL
    if ((!$_GET['page']) || ($_GET['page'] == 1)) {
        $bigtree['page']['canonical'] = WWW_ROOT . $_REQUEST['bigtree_htaccess_url'];
    }

    // ADD RSS META TAG
    $bigtree['page']['meta'][] = '<link rel="alternate" type="application/rss+xml" title="' .$cms->getSetting('site_name'). ' - All" href="' .WWW_ROOT . $_uccms_blog->frontendPath(). '/rss/">';

    // INIT MediaEmbed CLASS
    $MediaEmbed = new MediaEmbed();

    // NUMBER OF LEVELS IN URL PATH
    $path_num = count($bigtree['path']);

    ?>

    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_blog->Extension;?>/css/master/master.css" />
    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_blog->Extension;?>/css/custom/master.css" />
    <script src="/extensions/<?=$_uccms_blog->Extension;?>/js/master/master.js"></script>
    <script src="/extensions/<?=$_uccms_blog->Extension;?>/js/custom/master.js"></script>

    <section id="blogContainer">

        <?php

        // CAROUSEL CONFIG
        $carousel = array(
            'slides'    => $bigtree['resources']['hero'],
            'interval'  => 4000,
            'height'    => 30
        );

        // INCLUDE ELEMENT
        include(SERVER_ROOT. 'templates/elements/carousel.php');

        ?>

        <div class="container main">
            <div class="row">

                <div class="colLeft col-md-<?php echo ($blog['settings']['sidebar_enabled'] ? '8' : '12'); ?>">

                    <?php

                    // DISPLAY ANY SITE MESSAGES
                    echo $_uccms['_site-message']->display();

                    // HOME
                    if ($path_num == 1) {
                        include($data_dir. '/home.php');
                        include($sections_dir. '/home/default.php');

                    // ONE LEVEL DEEP
                    } else if ($path_num == 2) {

                        // SEARCH
                        if ($bigtree['path'][1] == 'search') {
                            include($data_dir. '/search.php');
                            include($sections_dir. '/search/default.php');
                        }

                    // TWO LEVELS DEEP
                    } else if ($path_num == 3) {

                        // GET LAST PATH PARTS
                        $ppa = explode('-', $bigtree['path'][2]);

                        // IS CATEGORY
                        if ($bigtree['path'][1] == 'category') {
                            include($data_dir. '/category.php');
                            include($sections_dir. '/category/default.php');

                        // IS TAG
                        } else if ($bigtree['path'][1] == 'tag') {
                            include($data_dir. '/tag.php');
                            include($sections_dir. '/tag/default.php');

                        // IS AUTHOR
                        } else if ($bigtree['path'][1] == 'author') {
                            include($data_dir. '/author.php');
                            include($sections_dir. '/author/default.php');

                        // IS POST
                        } else if ($bigtree['path'][1] == 'post') {
                            include($data_dir. '/post.php');
                            include($sections_dir. '/post/default.php');
                        }

                    }

                    ?>

                </div>

                <?php

                // SIDEBAR ENABLED
                if ($blog['settings']['sidebar_enabled']) {
                    include(SERVER_ROOT. 'extensions/' .$_uccms_blog->Extension. '/templates/routed/blog/elements/sidebar.php');
                }

                ?>

            </div>

        </div>
    </section>

    <?php

}

?>