<?php

// HAVE MORE THAN ONE IMAGE
if (count($post['images']) > 1) {
    $ii = 1;
    ?>
    <div class="gallery contain">
        <?php
        foreach ($post['images'] as $image) {
            if ($ii == 1) {
                $ii++;
                continue;
            }
            if ($image['image']) {
                ?>
                <div class="thumb">
                    <a href="<?php echo $_uccms_blog->imageBridgeOut($image['image'], 'posts'); ?>" title="<?php echo $image['caption']; ?>" class="swipebox" rel="gallery-1">
                        <img src="<?php echo $_uccms_blog->imageBridgeOut($image['image'], 'posts'); ?>" alt="<?php echo $image['caption']; ?>" />
                    </a>
                </div>
                <?php
            }
            $ii++;
        }
        ?>
    </div>
    <?php
}

?>