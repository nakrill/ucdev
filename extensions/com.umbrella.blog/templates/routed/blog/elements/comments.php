<?php

// COMMENTS ENABLED
if ($blog['settings']['post_comments_enabled']) {

    ?>
    <div class="comments">
        <?php

        // FACEBOOK
        if ($blog['settings']['post_comments_source'] == 'facebook') {
            if ($blog['settings']['post_comments_facebook_appid']) {
                if (!$blog['settings']['post_comments_facebook_num']) $blog['settings']['post_comments_facebook_num'] = 10;
                ?>
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=<?php echo $blog['settings']['post_comments_facebook_appid']; ?>";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-comments" data-href="<?php echo $bigtree['page']['canonical']; ?>" data-width="100%" data-numposts="<?php echo $blog['settings']['post_comments_facebook_num']; ?>"></div>
                <?php
            }

        // DISQUS
        } else if ($blog['settings']['post_comments_source'] == 'disqus') {
            if ($blog['settings']['post_comments_disqus_shortname']) {
                ?>
                <div id="disqus_thread"></div>
                <script type="text/javascript">
                    var disqus_shortname = '<?php echo $blog['settings']['post_comments_disqus_shortname']; ?>';
                    var disqus_identifier = '<?php echo $post['id']; ?>';
                    var disqus_url = '<?php echo $bigtree['page']['canonical']; ?>';
                    (function() {
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                </script>
                <?php
            }
        }

        ?>
    </div>
    <?php

}

?>