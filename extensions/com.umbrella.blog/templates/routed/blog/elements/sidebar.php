<?php

// SIDEBAR WIDGET DATA
$swda = $_uccms_blog->sidebarWidgetData();

// HAVE SIDEBAR WIDGETS
if (count($swda) > 0) {
    ?>
    <div class="sidebar col-md-4">
        <?php
        foreach ($swda as $widget_id => $widget) {
            include(dirname(__FILE__). '/.sidebar_widgets/' .$widget_id. '.php');
        }
        ?>
    </div>
    <?php
}

?>