<?php

// IS ACTIVE
if ($widget['active']) {

    // CODE
    $widget['code'] = $_uccms_blog->getSetting('sidebar_widgets_' .$widget_id. '_code');

    // HAVE CODE
    if ($widget['code']) {

        ?>

        <div class="widget <?php echo $widget_id; ?>">
            <?php if ($widget['heading']) { ?><h4><?php echo stripslashes($widget['heading']); ?></h4><?php } ?>
            <div class="content">
                <?php echo $widget['code']; ?>
            </div>
        </div>

        <?php

    }

}

?>