<?php

// IS ACTIVE
if ($widget['active']) {

    // CODE
    $widget['code'] = $_uccms_blog->getSetting('sidebar_widgets_' .$widget_id. '_code');

    // HAVE CODE
    if ($widget['code']) {

        ?>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=<?php echo $blog['settings']['post_comments_facebook_appid']; ?>";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <div class="widget <?php echo $widget_id; ?>">
            <?php if ($widget['heading']) { ?><h4><?php echo stripslashes($widget['heading']); ?></h4><?php } ?>
            <div class="content">
                <?php echo $widget['code']; ?>
            </div>
        </div>

        <?php

    }

}

?>