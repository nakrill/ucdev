<?php

// IS ACTIVE
if ($widget['active']) {

    if (!$widget['display_num']) $widget['display_num'] = 8;

    ?>

    <div class="widget <?php echo $widget_id; ?>">
        <?php if ($widget['heading']) { ?><h4><?php echo stripslashes($widget['heading']); ?></h4><?php } ?>
        <div class="content">
            <ul>
                <?php

                // GET LATEST POSTS
                $lposts_query = "SELECT * FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`status`=1) ORDER BY `dt_published` DESC LIMIT " .(int)$widget['display_num'];
                $lposts_q = sqlquery($lposts_query);

                while ($lpost = sqlfetch($lposts_q)) {
                    ?>
                    <li><a href="<?php echo $_uccms_blog->postURL($lpost['id'], $lpost); ?>"><?php echo stripslashes($lpost['title']); ?></a></li>
                    <?php
                }

                ?>
            </ul>
        </div>
    </div>

    <?php

}

?>