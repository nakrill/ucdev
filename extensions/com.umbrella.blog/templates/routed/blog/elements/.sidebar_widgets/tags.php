<?php

// IS ACTIVE
if ($widget['active']) {

    ?>

    <div class="widget <?php echo $widget_id; ?>">
        <?php if ($widget['heading']) { ?><h4><?php echo stripslashes($widget['heading']); ?></h4><?php } ?>
        <div class="content">
            <ul>
                <?php

                // GET TAGS
                $tag_query = "SELECT * FROM `" .$_uccms_blog->tables['tags']. "` WHERE (`active`=1) ORDER BY `title` ASC, `id` ASC";
                $tag_q = sqlquery($tag_query);
                $num_tags = sqlrows($tag_q);

                while ($tag = sqlfetch($tag_q)) {
                    ?>
                    <li><a href="<?php echo $_uccms_blog->tagURL($tag['id'], $tag); ?>"><?php echo stripslashes($tag['title']); ?></a></li>
                    <?php
                }

                ?>
            </ul>
        </div>
    </div>

    <?php

}

?>