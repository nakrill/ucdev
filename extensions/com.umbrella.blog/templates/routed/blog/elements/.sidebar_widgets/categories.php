<?php

// IS ACTIVE
if ($widget['active']) {

    ?>

    <div class="widget <?php echo $widget_id; ?>">
        <?php if ($widget['heading']) { ?><h4><?php echo stripslashes($widget['heading']); ?></h4><?php } ?>
        <div class="content">
            <ul>
                <?php

                // GET CATEGORIES
                $category_query = "SELECT * FROM `" .$_uccms_blog->tables['categories']. "` WHERE (`active`=1) ORDER BY `title` ASC, `id` ASC";
                $category_q = sqlquery($category_query);
                $num_categories = sqlrows($category_q);

                while ($category = sqlfetch($category_q)) {
                    ?>
                    <li><a href="<?php echo $_uccms_blog->categoryURL($category['id'], $category); ?>"><?php echo stripslashes($category['title']); ?></a></li>
                    <?php
                }

                ?>
            </ul>
        </div>
    </div>

    <?php

}

?>