<?php

// IS ACTIVE
if ($widget['active']) {

    ?>

    <div class="widget <?php echo $widget_id; ?>">
        <?php if ($widget['heading']) { ?><h4><?php echo stripslashes($widget['heading']); ?></h4><?php } ?>
        <div class="content">
            <form action="/<?php echo $bigtree['path'][0]; ?>/search/" method="get">
                <div class="input-group">
                    <input class="form-control" name="q" value="<?php echo $_GET['q']; ?>" type="text" size="20" placeholder="Keyword">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php

}

?>