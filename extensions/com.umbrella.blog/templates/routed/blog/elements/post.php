<?php if ($post['id']) { ?>
    <div class="post v3 <?php if ($post['images'][0]['image']) { echo 'with-image'; } ?>">
        <div class="contain">
            <div class="row">
                <?php if ($post['images'][0]['image']) { ?>
                    <div class="image main col-md-4">
                        <a href="<?php echo $_uccms_blog->postURL($post['id']); ?>" style="background-image: url('<?php echo $_uccms_blog->imageBridgeOut($post['images'][0]['image'], 'posts'); ?>');"></a>
                    </div>
                <?php } ?>
                <div class="info col-md-<?php if ($post['images'][0]['image']) { echo 8; } else { echo 12; } ?>">
                    <?php if ($post['title']) { ?>
                        <h1><a href="<?php echo $_uccms_blog->postURL($post['id']); ?>"><?php echo stripslashes($post['title']); ?></a></h1>
                    <?php } ?>
                    <?php if ((!$_uccms_blog->getSetting('post_hide_content')) && ($post['content'])) { ?>
                        <div class="content clearfix">
                            <?php echo substrWord(strip_tags(stripslashes($post['content'])), '200', $delim='..'); ?>
                        </div>
                    <?php } ?>
                    <?php if ((!$_uccms_blog->getSetting('post_hide_date')) || (!$_uccms_blog->getSetting('post_hide_author'))) { ?>
                        <div class="meta clearfix">
                            <?php if (!$_uccms_blog->getSetting('post_hide_author')) { ?>
                                <span class="author"><?php echo stripslashes($post['author']['name']); ?></span>
                            <?php } ?>
                            <?php if (!$_uccms_blog->getSetting('post_hide_date')) { ?>
                                <span class="date"><?php echo date('M j, Y', strtotime($post['dt_published'])); ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>