<?php

// CATEGORY ID
$category_id = (int)$ppa[count($ppa) - 1];

// HAVE CATEGORY ID
if ($category_id) {

    // GET CATEGORY
    $category_query = "SELECT * FROM `" .$_uccms_blog->tables['categories']. "` WHERE (`id`=" .$category_id. ") AND (`active`=1)";
    $category_q = sqlquery($category_query);
    $category = sqlfetch($category_q);

    // CATEGORY FOUND
    if ($category['id']) {

        // CUSTOM EDIT LINK
        $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_blog->Extension. '*blog/categories/edit/?id=' .$category['id'];

        // META
        if ($category['meta_title']) {
            $bigtree['page']['title'] = stripslashes($category['meta_title']);
        } else {
            $bigtree['page']['title'] = stripslashes($category['title']);
        }
        if ($_GET['page'] > 1) {
            $bigtree['page']['title'] .= ' - Page ' .(int)$_GET['page'];
        }
        if ($category['meta_description']) $bigtree['page']['meta_description'] = stripslashes($category['meta_description']);
        if ($category['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($category['meta_keywords']);

        // POST DATA
        $pda = $_uccms_blog->getPosts(array(
            'page'          => $_GET['page'],
            'category_id'   => $category['id']
        ));

        // PAGING
        $paging = $pda['pages'];

        // POSTS
        $posts = $pda['posts'];

    }

}

?>