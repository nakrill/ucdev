<?php

// AUTHOR ID
$author_id = (int)$ppa[count($ppa) - 1];

// HAVE AUTHOR ID
if ($author_id) {

    // GET AUTHOR
    $author_query = "
    SELECT `id`, `email`, `name`, `company`
    FROM `bigtree_users` AS `bu`
    WHERE (bu.id=" .$author_id. ")
    LIMIT 1
    ";
    $author_q = sqlquery($author_query);
    $author = sqlfetch($author_q);

    // AUTHOR FOUND
    if ($author['id']) {

        // CUSTOM EDIT LINK
        $bigtree['bar_edit_link'] = ADMIN_ROOT . 'admin/users/edit/' .$author['id']. '/';

        // POST DATA
        $pda = $_uccms_blog->getPosts(array(
            'page'      => $_GET['page'],
            'author_id' => $author['id']
        ));

        // PAGING
        $paging = $pda['pages'];

        // POSTS
        $posts = $pda['posts'];

        // META
        if ($_GET['page'] > 1) {
            $bigtree['page']['title'] .= ' - Page ' .(int)$_GET['page'];
        }

    }

}

?>