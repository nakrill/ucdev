<?php

// TAG ID
$tag_id = (int)$ppa[count($ppa) - 1];

// HAVE TAG ID
if ($tag_id) {

    // GET TAG
    $tag_query = "SELECT * FROM `" .$_uccms_blog->tables['tags']. "` WHERE (`id`=" .$tag_id. ") AND (`active`=1)";
    $tag_q = sqlquery($tag_query);
    $tag = sqlfetch($tag_q);

    // TAG FOUND
    if ($tag['id']) {

        // CUSTOM EDIT LINK
        $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_blog->Extension. '*blog/tags/edit/?id=' .$tag['id'];

        // META
        if ($tag['meta_title']) {
            $bigtree['page']['title'] = stripslashes($tag['meta_title']);
        } else {
            $bigtree['page']['title'] = stripslashes($tag['title']);
        }
        if ($_GET['page'] > 1) {
            $bigtree['page']['title'] .= ' - Page ' .(int)$_GET['page'];
        }
        if ($tag['meta_description']) $bigtree['page']['meta_description'] = stripslashes($tag['meta_description']);
        if ($tag['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($tag['meta_keywords']);

        // POST DATA
        $pda = $_uccms_blog->getPosts(array(
            'page'   => $_GET['page'],
            'tag_id' => $tag['id']
        ));

        // PAGING
        $paging = $pda['pages'];

        // POSTS
        $posts = $pda['posts'];

    }

}

?>