<?php

// POST ID
$post_id = (int)$ppa[count($ppa) - 1];

// HAVE POST ID
if ($post_id) {

    // GET POST
    $post = $_uccms_blog->getPostInfo($post_id);

    // POST FOUND
    if ($post['id']) {

        // CUSTOM EDIT LINK
        $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_blog->Extension. '*blog/posts/edit/?id=' .$post['id'];

        // META
        if ($post['meta_title']) {
            $bigtree['page']['title'] = stripslashes($post['meta_title']);
        } else {
            $bigtree['page']['title'] = stripslashes($post['title']);
        }
        if ($post['meta_description']) $bigtree['page']['meta_description'] = stripslashes($post['meta_description']);
        if ($post['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($post['meta_keywords']);

        // CANONICAL URL
        $bigtree['page']['canonical'] = WWW_ROOT . substr($_uccms_blog->postURL($post['id']), 1);

        // HAVE MAIN IMAGE
        if ($post['images'][0]['image']) {
            $bigtree['page']['share_image'] = $_uccms_blog->imageBridgeOut($post['images'][0]['image'], 'posts');
        }

        // AUTO-LINK CONTENT
        if ($post['content']) {
            $post['content'] = autoEmbed($post['content']);
        }

        $have_gallery_shortcode = false;

        // HAVE GALLERY SHORTCODE
        if (strpos($post['content'], '[gallery]') !== false) {
            $have_gallery_shortcode = true;
            $post['content'] = preg_replace_callback('(\[gallery\])', function($match) use($_uccms_blog, $post) {
                ob_start();
                include(SERVER_ROOT. 'extensions/' .$_uccms_blog->Extension. '/templates/routed/blog/elements/post_gallery.php');
                return ob_get_clean();
            }, stripslashes($post['content']));
        }

        // GET PREVIOUS POST
        $prev_query = "SELECT * FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`dt_published`<'" .$post['dt_published']. "') AND (`status`=1) ORDER BY `dt_published` DESC LIMIT 1";
        $prev_q = sqlquery($prev_query);
        $prev_post = sqlfetch($prev_q);

        // GET NEXT POST
        $next_query = "SELECT * FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`dt_published`>'" .$post['dt_published']. "') AND (`status`=1) ORDER BY `dt_published` ASC LIMIT 1";
        $next_q = sqlquery($next_query);
        $next_post = sqlfetch($next_q);

    }

}

?>