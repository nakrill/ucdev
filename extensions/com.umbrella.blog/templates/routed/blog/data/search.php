<?php

// SEARCH NOT DISABLED
if (!$_uccms_blog->getSetting('search_disabled')) {

    // POST DATA
    $pda = $_uccms_blog->getPosts(array(
        'page'  => $_GET['page'],
        'query' => $_REQUEST['q']
    ));

    // PAGING
    $paging = $pda['pages'];

    // POSTS
    $posts = $pda['posts'];

    // META
    if ($_GET['page'] > 1) {
        $bigtree['page']['title'] .= ' - Page ' .(int)$_GET['page'];
    }

}

?>