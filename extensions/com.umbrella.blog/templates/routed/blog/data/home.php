<?php

// CUSTOM EDIT LINK
$bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_blog->Extension. '*blog/settings/';

// POST DATA
$pda = $_uccms_blog->getPosts(array(
    'page' => $_GET['page']
));

// PAGING
$paging = $pda['pages'];

// POSTS
$posts = $pda['posts'];

// META
if ($_GET['page'] > 1) {
    $bigtree['page']['title'] .= ' - Page ' .(int)$_GET['page'];
}

?>