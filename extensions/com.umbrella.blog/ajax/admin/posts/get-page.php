<?php

// MODULE CLASS
if (!$_uccms_blog) $_uccms_blog = new uccms_Blog;

// HAS ACCESS
if ($_uccms_blog->adminModulePermission()) {

    // CLEAN UP
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
    $category_id    = (int)$_GET['category_id'];
    $tag_id         = (int)$_GET['tag_id'];
    $author_id      = (int)$_GET['author_id'];
    $limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_blog->perPage();

    if (isset($_GET['base_url'])) {
        $base_url = $_GET['base_url'];
    } else if (defined('MODULE_ROOT')) {
        $base_url = MODULE_ROOT. 'posts';
    } else {
        $base_url = '.';
    }

    //$_uccms_blog->setPerPage(5);

    // WHERE ARRAY
    unset($wa);
    $wa[] = "p.id>0";

    // STATUSES
    $statusa = array(
        'draft'     => 0,
        'published' => 1,
        'scheduled' => 2,
        'deleted'   => 9
    );
    if (isset($statusa[$_REQUEST['status']])) {
        $wa[] = "p.status=" .$statusa[$_REQUEST['status']];
    } else {
        $wa[] = "p.status!=9";
    }

    // CATEGORY SPECIFIED
    if ($category_id > 0) {
        $inner_join_cat = "INNER JOIN `" .$_uccms_blog->tables['post_categories']. "` AS `pc` ON p.id=pc.post_id";
        $wa[] = "pc.category_id=" .$category_id;
    }

    // TAG SPECIFIED
    if ($tag_id > 0) {
        $inner_join_tag = "INNER JOIN `" .$_uccms_blog->tables['post_tags']. "` AS `tc` ON p.id=tc.post_id";
        $wa[] = "tc.category_id=" .$tag_id;
    }

    // AUTHOR SPECIFIED
    if ($author_id > 0) {
        $wa[] = "p.author_id=" .$author_id;
    }

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(p.title) LIKE '%$part%') OR (LOWER(p.content) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED POSTS
    $post_query = "
    SELECT p.*
    FROM `" .$_uccms_blog->tables['posts']. "` AS `p`
    " .$inner_join_cat. " " .$inner_join_tag. "
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY p.dt_created DESC, p.id DESC
    LIMIT " .(($page - 1) * $limit). "," .$limit;
    $post_q = sqlquery($post_query);

    // NUMBER OF PAGED POSTS
    $num_posts = sqlrows($post_q);

    // TOTAL NUMBER OF POSTS
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_blog->tables['posts']. "` AS `p` " .$inner_join_cat. " " .$inner_join_tag. " WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_blog->pageCount($total_results['total']);

    // HAVE POSTS
    if ($num_posts > 0) {

        // AUTHORS ARRAY
        $accta = array();

        // LOOP
        while ($post = sqlfetch($post_q)) {

            // DATE
            switch ($post['status']) {
                case 0:
                    $post_date = $post['dt_created'];
                    break;
                case 1:
                    $post_date = $post['dt_published'];
                    break;
                case 2:
                    $post_date = $post['dt_publish'];
                    break;
            }

            // CATEGORIES ARRAY
            $cata = array();

            // HAVE AUTHOR ID
            if ($post['author_id'] > 0) {

                // NOT IN AUTHOR ARRAY
                if (!$accta[$post['author_id']]) {

                    // GET ACCOUNT INFO
                    $acct_query = "
                    SELECT *
                    FROM `bigtree_users` AS `bu`
                    WHERE (bu.id=" .$post['author_id']. ")
                    LIMIT 1
                    ";
                    $acct_q = sqlquery($acct_query);
                    $acct = sqlfetch($acct_q);
                    $accta[$post['author_id']] = $acct;

                }

            }

            // GET CATEGORIES
            $catr_query = "
            SELECT c.id, c.title
            FROM `" .$_uccms_blog->tables['post_categories']. "` AS `pc`
            INNER JOIN `" .$_uccms_blog->tables['categories']. "` AS `c` ON pc.category_id=c.id
            WHERE (pc.post_id=" .$post['id']. ")
            ORDER BY c.title ASC
            ";
            $catr_q = sqlquery($catr_query);
            while ($catr = sqlfetch($catr_q)) {
                $cata[] = '<a href="' .$base_url. '/?category_id=' .$catr['id']. '">' .stripslashes($catr['title']). '</a>';
            }

            ?>

            <li class="item">
                <section class="post_title">
                    <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $post['id']; ?>"><?php echo stripslashes($post['title']); ?></a>
                </section>
                <section class="post_author">
                    <?php echo stripslashes($accta[$post['author_id']]['name']); ?>
                </section>
                <section class="post_categories">
                    <?php echo implode(', ', $cata); ?>
                </section>
                <section class="post_date">
                    <?php echo date('n/j/Y g:i A', strtotime($post_date)); ?>
                </section>
                <section class="post_status status_<?php if ($post['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php echo $_uccms_blog->statuses[$post['status']]; ?>
                </section>
                <section class="post_edit">
                    <a class="icon_edit" title="Edit post" href="<?php echo $base_url; ?>/edit/?id=<?php echo $post['id']; ?>"></a>
                </section>
                <section class="post_delete">
                    <?php if ($_uccms_blog->adminModulePermission() == 'p') { ?>
                        <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $post['id']; ?>" class="icon_delete" title="Delete Post" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
                    <?php } ?>
                </section>
            </li>

            <?php

        }

        unset($accta);

    // NO posts
    } else {
        ?>
        <li style="text-align: center;">
            No posts.
        </li>
        <?php
    }

    ?>

    <script>
	    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>