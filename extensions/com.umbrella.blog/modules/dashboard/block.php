<?php

// MODULE CLASS
$_uccms_blog = new uccms_Blog;

// HAS ACCESS
if ($_uccms_blog->adminModulePermission()) {

    ?>

    <style type="text/css">

        #block_blog #latest_posts header span, #block_blog #latest_posts .item section {
            text-align: left;
        }

        #block_blog #latest_posts .post_title {
            width: 300px;
        }
        #block_blog #latest_posts .post_author {
            width: 150px;
        }
        #block_blog #latest_posts .post_categories {
            width: 160px;
        }
        #block_blog #latest_posts .post_tags {
            width: 200px;
        }
        #block_blog #latest_posts .post_date {
            width: 130px;
        }
        #block_blog #latest_posts .post_status {
            width: 90px;
        }
        #block_blog #latest_posts .post_edit {
            width: 55px;
        }
        #block_blog #latest_posts .post_delete {
            width: 55px;
        }

    </style>

    <div id="block_blog" class="block">
        <h2><a href="../<?php echo $extension; ?>*blog/">Blog</a></h2>
        <div id="latest_posts" class="table">
            <summary>
                <h2>Latest Posts</h2>
                <a class="btn btn-secondary btn-sm" href="../<?php echo $extension; ?>*blog/posts/">
                    View All
                </a>
            </summary>
            <header style="clear: both;">
                <span class="post_title">Title</span>
                <span class="post_author">Author</span>
                <span class="post_categories">Categories</span>
                <span class="post_date">Date</span>
                <span class="post_status">Status</span>
                <span class="post_edit">Edit</span>
                <span class="post_delete">Delete</span>
            </header>
            <ul id="results" class="items">
                <?php
                $_GET['base_url'] = '../' .$extension. '*blog/posts';
                $_GET['limit'] = 5;
                include($extension_dir. '/ajax/admin/posts/get-page.php');
                ?>
            </ul>
        </div>
    </div>

    <?php

}

unset($_uccms_blog);

?>