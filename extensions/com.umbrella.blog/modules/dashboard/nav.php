<?php

$extension_nav =
["link" => "com.umbrella.blog*blog", "title" => "Blog", "access" => 1, "children" => [
    ["link" => ".", "title" => "Dashboard", "access" => 1],
    ["link" => "categories", "title" => "Categories", "access" => 1],
    ["link" => "tags", "title" => "Tags", "access" => 1],
    ["link" => "Posts", "title" => "Posts", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>