<tr class="item">
    <td class="sort"><span class="icon_sort ui-sortable-handle"></span></td>
    <td class="name w-expand">
        Content
    </td>
    <td class="active">
        <input type="checkbox" name="setting[sidebar_widgets][<?php echo $widget_id; ?>][active]" value="1" <?php if ($swda[$widget_id]['active']) { ?>checked="checked"<?php } ?> /> Active
    </td>
    <td class="settings">
        <fieldset class="heading">
            <input type="text" name="setting[sidebar_widgets][<?php echo $widget_id; ?>][heading]" value="<?php echo $swda[$widget_id]['heading']; ?>" placeholder="Heading" />
        </fieldset>
        <div class="expand">
            <fieldset>
                <label>Content</label>
                <div>
                    <?php
                    $field = array(
                        'key'       => 'setting[sidebar_widgets][' .$widget_id. '][code]', // The value you should use for the "name" attribute of your form field
                        'value'     => $_uccms_blog->getSetting('sidebar_widgets_' .$widget_id. '_code'), // The existing value for this form field
                        'id'        => 'sidebar_widgets_html1', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'simple' => false
                        )
                    );
                    include(BigTree::path('admin/form-field-types/draw/html.php'));
                    ?>
                </div>
            </fieldset>
        </div>
    </td>
    <td class="expand_arrow w-expand">
        <i class="fa fa-caret-down"></i>
    </td>
</tr>