<tr class="item">
    <td class="sort"><span class="icon_sort ui-sortable-handle"></span></td>
    <td class="name">
        Categories
    </td>
    <td class="active">
        <input type="checkbox" name="setting[sidebar_widgets][<?php echo $widget_id; ?>][active]" value="1" <?php if ($swda[$widget_id]['active']) { ?>checked="checked"<?php } ?> /> Active
    </td>
    <td class="settings">
        <fieldset class="heading">
            <input type="text" name="setting[sidebar_widgets][<?php echo $widget_id; ?>][heading]" value="<?php echo $swda[$widget_id]['heading']; ?>" placeholder="Heading" />
        </fieldset>
    </td>
    <td class="expand_arrow">
        &nbsp;
    </td>
</tr>