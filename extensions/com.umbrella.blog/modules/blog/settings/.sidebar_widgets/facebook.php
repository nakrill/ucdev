<tr class="item">
    <td class="sort"><span class="icon_sort ui-sortable-handle"></span></td>
    <td class="name w-expand">
        Facebook
        <div class="link"><a href="https://developers.facebook.com/docs/plugins/page-plugin/" target="_blank">Set Up</a></div>
    </td>
    <td class="active">
        <input type="checkbox" name="setting[sidebar_widgets][<?php echo $widget_id; ?>][active]" value="1" <?php if ($swda[$widget_id]['active']) { ?>checked="checked"<?php } ?> /> Active
    </td>
    <td class="settings">
        <fieldset class="heading">
            <input type="text" name="setting[sidebar_widgets][<?php echo $widget_id; ?>][heading]" value="<?php echo $swda[$widget_id]['heading']; ?>" placeholder="Heading" />
        </fieldset>
        <div class="expand">
            <fieldset>
                <label>Code</label>
                <textarea name="setting[sidebar_widgets][<?php echo $widget_id; ?>][code]"><?php echo $_uccms_blog->getSetting('sidebar_widgets_' .$widget_id. '_code'); ?></textarea>
            </fieldset>
        </div>
    </td>
    <td class="expand_arrow w-expand">
        <i class="fa fa-caret-down"></i>
    </td>
</tr>