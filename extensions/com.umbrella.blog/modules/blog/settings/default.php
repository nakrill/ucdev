<?php

// GET SETTINGS
$settings = $_uccms_blog->getSettings();

// SIDEBAR WIDGET DATA
$swda = $_uccms_blog->sidebarWidgetData();

?>

<style type="text/css">
    .sidebar_widgets .item td {
        vertical-align: top;
    }
    .sidebar_widgets .item .w-expand {
        cursor: pointer;
    }
    .sidebar_widgets .item .sort {
        padding-right: 0px;
    }
    .sidebar_widgets .item .sort .icon_sort {
    }
    .sidebar_widgets .item .name {
        width: 25%;
        padding: 21px 0px 10px 15px;
    }
    .sidebar_widgets .item .name .link {
        padding-top: 6px;
        font-size: .8em;
    }
    .sidebar_widgets .item .active {
        width: 15%;
        padding: 21px 0px 10px 15px;
    }
    .sidebar_widgets .item .settings {
        width: 60%;
        padding-right: 0px;
    }
    .sidebar_widgets .item .settings fieldset.heading {
        margin: 0px;
    }
    .sidebar_widgets .item .settings .expand {
        display: none;
        padding-top: 15px;
    }
    .sidebar_widgets .item .settings .expand.open {
        display: block;
    }
    .sidebar_widgets .item .expand_arrow {
        padding: 21px 15px 0;
    }
    .sidebar_widgets .item .checkbox {
        margin-top: -2px;
    }
    .sidebar_widgets .item input[type="text"], .sidebar_widgets .item textarea {
        width: 90%;
    }
</style>

<script type="text/javascript">

    $(document).ready(function() {

        // SIDEBAR ENABLED TOGGLE
        $('#sidebar_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('.sidebar_enabled_content').show();
            } else {
                $('.sidebar_enabled_content').hide();
            }
        });

        // POSTS - COMMENTS ENABLED TOGGLE
        $('#post_comments_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('.post_comments_enabled_content').show();
            } else {
                $('.post_comments_enabled_content').hide();
            }
        });

        // POSTS - COMMENTS SOURCE CHANGE
        $('#post_comments_source').change(function(e) {
            var what = $(this).val();
            $('.post_comments_source_content').hide();
            if (what) {
                $('.post_comments_source_content.' +what).show();
            }
        });

        // POSTS - PINGBACKS ENABLED TOGGLE
        $('#post_ping_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('.post_ping_enabled_content').show();
            } else {
                $('.post_ping_enabled_content').hide();
            }
        });

        // SIDEBAR WIDGETS - EXPAND
        $('form .sidebar_widgets .item .w-expand').click(function(e) {
            var expand = $(this).closest('.item').find('.expand');
            if (expand.hasClass('open')) {
                expand.removeClass('open');
                $(this).closest('.item').find('.expand_arrow i.fa').removeClass('fa-caret-up').addClass('fa-caret-down');
            } else {
                expand.addClass('open');
                $(this).closest('.item').find('.expand_arrow i.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
            }
        });

        // SIDEBAR WIDGETS - SORTABLE
        $('form .sidebar_widgets table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>General</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Posts Per Page</label>
                    <input type="text" name="setting[posts_per_page]" value="<?php echo stripslashes($settings['posts_per_page']); ?>" placeholder="<?php echo $cms->getSetting('bigtree-internal-per-page'); ?>" class="smaller_60" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="setting[search_disabled]" value="1" <?php if ($settings['search_disabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Search disabled <small>Don't allow searching.</small></label>
                </fieldset>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Sidebar</h2>
    </header>

    <section>

        <div class="contain">

            <fieldset class="last">
                <input id="sidebar_enabled" type="checkbox" name="setting[sidebar_enabled]" value="1" <?php if ($settings['sidebar_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Sidebar enabled <small>The sidebar will be displayed on your blog.</small></label>
            </fieldset>

            <div class="sidebar_enabled_content" style="<?php if (!$settings['sidebar_enabled']) { ?>display: none; <?php } ?> padding-top: 20px;">

                <fieldset>
                    <label>Position</label>
                    <select name="setting[sidebar_position]">
                        <option value="right" <?php if ($settings['sidebar_position'] == 'right') { ?>selected="selected"<?php } ?>>Right</option>
                        <option value="left" <?php if ($settings['sidebar_position'] == 'left') { ?>selected="selected"<?php } ?>>Left</option>
                    </select>
                </fieldset>

                <fieldset class="sidebar_widgets">
                    <label>Widgets</label>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                        <?php
                        $wa = array_merge($swda, $_uccms_blog->sidebarWidgets());
                        foreach ($wa as $widget_id => $null) {
                            include(dirname(__FILE__). '/.sidebar_widgets/' .$widget_id. '.php');
                        }
                        ?>
                    </table>
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Posts</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <input type="checkbox" name="setting[post_hide_date]" value="1" <?php if ($settings['post_hide_date']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Hide Date</label>
                </fieldset>

                <fieldset>
                    <input type="checkbox" name="setting[post_hide_author]" value="1" <?php if ($settings['post_hide_author']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Hide Author</label>
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="setting[post_hide_content]" value="1" <?php if ($settings['post_hide_content']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Hide Content</label>
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <input id="post_comments_enabled" type="checkbox" name="setting[post_comments_enabled]" value="1" <?php if ($settings['post_comments_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Comments enabled <small>Display comments on blog posts.</small></label>
                </fieldset>

                <div class="post_comments_enabled_content" style="<?php if (!$settings['post_comments_enabled']) { ?>display: none; <?php } ?> padding: 0 0 15px 0;">

                    <fieldset>
                        <label>Source</label>
                        <select id="post_comments_source" name="setting[post_comments_source]">
                            <option value="">Select</option>
                            <option value="facebook" <?php if ($settings['post_comments_source'] == 'facebook') { ?>selected="selected"<?php } ?>>Facebook</option>
                            <option value="disqus" <?php if ($settings['post_comments_source'] == 'disqus') { ?>selected="selected"<?php } ?>>Disqus</option>
                        </select>
                    </fieldset>

                    <div class="post_comments_source_content facebook" style="<?php if ($settings['post_comments_source'] != 'facebook') { ?>display: none; <?php } ?>">

                        <fieldset>
                            <label>App ID <small>(<a href="https://developers.facebook.com/docs/plugins/comments" target="_blank">Create app</a>)</small></label>
                            <input type="text" name="setting[post_comments_facebook_appid]" value="<?php echo stripslashes($settings['post_comments_facebook_appid']); ?>" />
                        </fieldset>

                        <fieldset>
                            <label>Number of Comments to Display</label>
                            <input type="text" name="setting[post_comments_facebook_num]" value="<?php echo stripslashes($settings['post_comments_facebook_num']); ?>" placeholder="10" class="smaller_60" />
                        </fieldset>

                    </div>

                    <div class="post_comments_source_content disqus" style="<?php if ($settings['post_comments_source'] != 'disqus') { ?>display: none; <?php } ?>">

                        <fieldset>
                            <label>Shortname <small>(<a href="http://disqus.com/register" target="_blank">Register site</a>)</small></label>
                            <input type="text" name="setting[post_comments_disqus_shortname]" value="<?php echo stripslashes($settings['post_comments_disqus_shortname']); ?>" />
                        </fieldset>

                    </div>

                </div>

                <fieldset class="last">
                    <input id="post_ping_enabled" type="checkbox" name="setting[post_ping_enabled]" value="1" <?php if ($settings['post_ping_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Pingbacks / Trackbacks enabled</label>
                </fieldset>

            </div>

            <div class="post_ping_enabled_content" style="clear: both; <?php if (!$settings['post_ping_enabled']) { ?>display: none; <?php } ?> padding-top: 20px;">

                <? /*
                <fieldset>
                    <input type="checkbox" name="setting[post_ping_from_links]" value="1" <?php if ($settings['post_ping_from_links']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Send pingbacks to urls within post.</label>
                </fieldset>
                */ ?>

                <fieldset>
                    <label>Ping Services</label>
                    <textarea name="setting[post_ping_list]"><?php echo stripslashes($settings['post_ping_list']); ?></textarea>
                    <label><small>One URL per line.</small></label>
                    <div>
                        These services are enabled by default:
                        <div style="margin: 5px 0 0 5px; font-size: 11px; color: #999;">
                            <?php foreach ($_uccms_blog->pingbackServices() as $pingback_url) { ?>
                                <span style="display: block;"><?php echo $pingback_url; ?></span>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>

</form>