<style type="text/css">

    #blog_dashboard {
        height: 100%;
    }

    #blog_dashboard > table {
        height: calc(100% - 72px);
    }

    #blog_dashboard .col_left {
        width: 100%;
        padding-top: 0px;
        padding-left: 0px;
        vertical-align: top;
    }

    #blog_dashboard .col_right {
        margin-left: 30px;
        border-left: 1px solid #ccc;
        background-color: #f5f5f5;
    }
    #blog_dashboard .col_right .size {
        width: 200px;
        min-height: 400px;
        padding: 0 15px;
    }

    #blog_dashboard .toggle_bar {
        margin: 0;
        padding: 0 0 2px;
        background-color: #f8f8f8;
        border-bottom: 1px solid rgba(0, 0, 0, .1);
    }
    #blog_dashboard .toggle_bar ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #blog_dashboard .toggle_bar ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 10px;
        font-weight: bold;
        text-transform: uppercase;
    }
    #blog_dashboard .toggle_bar ul li a {
        color: #aaa;
    }
    #blog_dashboard .toggle_bar ul li a:hover, #blog_dashboard .toggle_bar ul li a.active {
        color: #333;
    }
    #blog_dashboard .toggle_bar ul li a .num {
        font-size: .9em;
        font-weight: normal;
        color: #aaa;
    }

    #blog_dashboard .toggle_content_container {
    }
    #blog_dashboard .toggle_content_container .toggle {
        display: none;
    }
    #blog_dashboard .toggle_content_container .toggle:first-child {
        display: block;
    }
    #blog_dashboard .toggle_content_container .toggle .none {
        padding: 15px;
        text-align: center;
    }

    #blog_dashboard .toggle_content_container .item {
    }
    #blog_dashboard .toggle_content_container .item > .contain {
        padding: 10px 15px;
        background-color: rgba(0, 0, 0, .025);
        border-bottom: 1px solid rgba(0, 0, 0, .1);
        transition: .5s;
    }
    #blog_dashboard .toggle_content_container .item:hover > .contain {
        background-color: rgba(0, 0, 0, .05);
    }
    #blog_dashboard .toggle_content_container .item table {
        width: 100%;
        margin: 0px;
        padding: 0px;
        border: 0px none;
    }
    #blog_dashboard .toggle_content_container .item table td {
        padding: 0px;
        vertical-align: top;
    }
    #blog_dashboard .toggle_content_container .item table td table td {
        font-size: 1em;
        color: #999;
        text-transform: uppercase;
    }
    #blog_dashboard .toggle_content_container .item td.icon {
        padding: 0 15px 0 0;
        white-space: nowrap;
        font-size: 2em;
        opacity: .6;
    }
    #blog_dashboard .toggle_content_container .item .icon i.fa {
        margin-top: -3px;
    }
    #blog_dashboard .toggle_content_container .item td.content {
        width: 100%;
    }
    #blog_dashboard .toggle_content_container .item .content .title {
        padding-bottom: 0px;
        font-size: 1.2em;
        font-weight: bold;
    }
    #blog_dashboard .toggle_content_container .item .content .title a {
        color: #555;
    }
    #blog_dashboard .toggle_content_container .item .content .status {
        width: 25%;
    }
    #blog_dashboard .toggle_content_container .item .content .status .published {
        color: #6BD873;
    }
    #blog_dashboard .toggle_content_container .item .content .date {
        width: 25%;
    }
    #blog_dashboard .toggle_content_container .item .content .actions {
        width: 50%;
        text-align: right;
    }
    #blog_dashboard .toggle_content_container .item .content .actions ul {
        opacity: 0;
    }
    #blog_dashboard .toggle_content_container .item:hover .content .actions ul {
        opacity: 1;
    }
    #blog_dashboard .toggle_content_container .item ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #blog_dashboard .toggle_content_container .item ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 0 0 8px;
        line-height: 1em;
    }
    #blog_dashboard .toggle_content_container .more {
        padding-top: 15px;
        text-align: center;
    }

    #blog_dashboard .activity_container {
        border-left: 2px solid #bbb;
    }

    #blog_dashboard .activity_container .day {
        position: relative;
        margin-top: 20px;
        padding-bottom: 20px;
    }

    #blog_dashboard .activity_container .day:first-child {
        margin-top: 0px;
    }

    #blog_dashboard .activity_container .day .icon {
        position: absolute;
        top: 5px;
        left: -7px;
        width: 0.9em;
        height: 0.9em;
        background-color: #bbb;
        border-radius: 0.9em;
    }

    #blog_dashboard .activity_container .day .date {
        margin-left: 20px;
        padding: 5px 10px;
        background-color: #e0e0e0;
        border-radius: 10px;
        text-align: center;
        font-weight: bold;
        color: #666;
    }

    #blog_dashboard .activity_container .item {
        position: relative;
    }

    #blog_dashboard .activity_container .item .icon {
        position: absolute;
        top: -3px;
        left: -10px;
        width: 1.6em;
        height: 1.6em;
        background-color: #aaa;
        text-align: center;
        font-size: 1em;
        line-height: 1.6em;
        color: #fff;
        border-radius: 1.6em;
    }

    #blog_dashboard .activity_container .item .main {
        padding-left: 20px;
    }

    #blog_dashboard .activity_container .item .main .title, #blog_dashboard .activity_container .item .main .title a {
        padding-bottom: 3px;
        font-size: 1.05em;
        font-weight: bold;
        color: #555;
    }

    #blog_dashboard .activity_container .item .main .time {
        padding-bottom: 3px;
        font-size: .9em;
        color: #777;
    }

    #blog_dashboard .activity_container .item .main .details {
        padding-top: 1px;
        padding-bottom: 3px;
        font-size: .9em;
        color: #999;
    }

    #blog_dashboard .activity_container .item .main .status {
        padding-bottom: 2px;
    }
    #blog_dashboard .activity_container .item .main .action {
        color: #777;
    }

    #blog_dashboard .activity_container .split {
        height: 1px;
        margin: 15px 0;
        border-bottom: 1px dashed #ddd;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        $('#blog_dashboard .toggle_bar a').click(function(e) {
            e.preventDefault();
            var set = $(this).closest('.toggle_bar').attr('data-set');
            var what = $(this).attr('data-what');
            $('#blog_dashboard .toggle_bar a').removeClass('active');
            $(this).addClass('active');
            $('#blog_dashboard .toggle_content_container.set-' +set+ ' .toggle').hide();
            $('#blog_dashboard .toggle_content_container.set-' +set+ ' .toggle.' +what).show();

        });

    });

</script>


<div id="blog_dashboard">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 0px;">
        <tr>

            <td class="col_left">

                <?php

                // GET ALL POSTS
                $all_query = "SELECT `id`, `status` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`status`!=9)";
                $all_q = sqlquery($all_query);
                $num_all = sqlrows($all_q);
                $all_query = $all_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $all_q = sqlquery($all_query);

                // GET PUBLISHED POSTS
                $published_query = "SELECT `id`, `status` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`status`=1)";
                $published_q = sqlquery($published_query);
                $num_published = sqlrows($published_q);
                $published_query = $published_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $published_q = sqlquery($published_query);

                // GET SCHEDULED POSTS
                $scheduled_query = "SELECT `id`, `status` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`status`=2)";
                $scheduled_q = sqlquery($scheduled_query);
                $num_scheduled = sqlrows($scheduled_q);
                $scheduled_query = $scheduled_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $scheduled_q = sqlquery($scheduled_query);

                // GET DRAFT POSTS
                $draft_query = "SELECT `id`, `status` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`status`=0)";
                $draft_q = sqlquery($draft_query);
                $num_draft = sqlrows($draft_q);
                $draft_query = $draft_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $draft_q = sqlquery($draft_query);

                ?>

                <div class="toggle_bar" data-set="posts">
                    <ul>
                        <li><a href="#" class="active" data-what="all">All Posts <span class="num">(<?php echo number_format($num_all, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="published">Published <span class="num">(<?php echo number_format($num_published, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="scheduled">Scheduled <span class="num">(<?php echo number_format($num_scheduled, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="draft">Drafts <span class="num">(<?php echo number_format($num_draft, 0); ?>)</span></a></li>
                    </ul>
                </div>

                <div class="toggle_content_container set-posts">

                    <div class="toggle all">
                        <?php if ($num_all > 0) { ?>
                            <div class="items">
                                <?php
                                while ($post = sqlfetch($all_q)) {
                                    echo this_item_post($_uccms_blog->getPostInfo($post['id'], array(), $post['status']));
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./posts/" class="button">All Posts</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No posts.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle published">
                        <?php if ($num_published > 0) { ?>
                            <div class="items">
                                <?php
                                while ($post = sqlfetch($published_q)) {
                                    echo this_item_post($_uccms_blog->getPostInfo($post['id'], array(), $post['status']));
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./posts/?status=published" class="button">All Published Posts</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No published posts.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle scheduled">
                        <?php if ($num_scheduled > 0) { ?>
                            <div class="items">
                                <?php
                                while ($post = sqlfetch($scheduled_q)) {
                                    echo this_item_post($_uccms_blog->getPostInfo($post['id'], array(), $post['status']));
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./posts/?status=scheduled" class="button">All Scheduled Posts</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No scheduled posts.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle draft">
                        <?php if ($num_draft > 0) { ?>
                            <div class="items">
                                <?php
                                while ($post = sqlfetch($draft_q)) {
                                    echo this_item_post($_uccms_blog->getPostInfo($post['id'], array(), $post['status']));
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./posts/?status=draft" class="button">All Drafts</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No drafts.
                            </div>
                        <?php } ?>
                    </div>

                </div>

            </td>

            <td class="col_right" valign="top">
                <div class="size">

                    <h3 style="text-align: center;">Recent Activity</h3>

                    <div class="activity_container">

                        <?php

                        // ITEM ARRAY
                        $itema = array();

                        // LAST CREATED
                        $lcreated_query = "SELECT *, `dt_created` AS `sort_by`, 'created' AS `item_what` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`dt_created`!='0000-00-00 00:00:00') ORDER BY `dt_created` DESC LIMIT 10";
                        $lcreated_q = sqlquery($lcreated_query);
                        while ($lcreated = sqlfetch($lcreated_q)) {
                            $itema[] = $lcreated;
                        }

                        // LAST SCHEDULED
                        $lscheduled_query = "SELECT *, `dt_saved` AS `sort_by`, 'scheduled' AS `item_what` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`dt_publish`!='0000-00-00 00:00:00') ORDER BY `dt_saved` DESC LIMIT 10";
                        $lscheduled_q = sqlquery($lscheduled_query);
                        while ($lscheduled = sqlfetch($lscheduled_q)) {
                            $itema[] = $lscheduled;
                        }

                        // LAST SAVED
                        $lsaved_query = "SELECT *, `dt_saved` AS `sort_by`, 'saved' AS `item_what` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`dt_saved`!='0000-00-00 00:00:00') ORDER BY `dt_saved` DESC LIMIT 10";
                        $lsaved_q = sqlquery($lsaved_query);
                        while ($lsaved = sqlfetch($lsaved_q)) {
                            $itema[] = $lsaved;
                        }

                        // LAST PUBLISHED
                        $lpublished_query = "SELECT *, `dt_published` AS `sort_by`, 'published' AS `item_what` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`dt_published`!='0000-00-00 00:00:00') ORDER BY `dt_published` DESC LIMIT 10";
                        $lpublished_q = sqlquery($lpublished_query);
                        while ($lpublished = sqlfetch($lpublished_q)) {
                            $itema[] = $lpublished;
                        }

                        // LAST DELETED
                        $ldeleted_query = "SELECT *, `dt_deleted` AS `sort_by`, 'deleted' AS `item_what` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`dt_deleted`!='0000-00-00 00:00:00') ORDER BY `dt_deleted` DESC LIMIT 10";
                        $ldeleted_q = sqlquery($ldeleted_query);
                        while ($ldeleted = sqlfetch($ldeleted_q)) {
                            $itema[] = $ldeleted;
                        }

                        // HAVE ITEMS
                        if (count($itema) > 0) {

                            // SORT BY DATE/TIME ASCENDING
                            usort($itema, function($a, $b) {
                                return strtotime($b['sort_by']) - strtotime($a['sort_by']);
                            });

                            // STATUSES
                            $statusa = array(
                                'created'   => array(
                                    'title'     => 'Created',
                                    'icon'      => 'fa-plus-circle',
                                    'color'     => '#85eeb8',
                                    'actions'   => '<a href="./posts/edit/?id={id}">Edit</a>'
                                ),
                                'saved'     => array(
                                    'title'     => 'Saved',
                                    'icon'      => 'fa-floppy-o',
                                    'color'     => '#4dd0e1',
                                    'actions'   => '<a href="./posts/edit/?id={id}">Edit</a>'
                                ),
                                'published' => array(
                                    'title'     => 'Published',
                                    'icon'      => 'fa-check-circle',
                                    'color'     => '#00c05d',
                                    'actions'   => '<a href="{link_view}">View</a> | <a href="./posts/edit/?id={id}">Edit</a>'
                                ),
                                'scheduled' => array(
                                    'title'     => 'Scheduled',
                                    'icon'      => 'fa-clock-o',
                                    'color'     => '#4db6ac',
                                    'actions'   => '<a href="{link_view}">View</a> | <a href="./posts/edit/?id={id}">Edit</a>'
                                ),
                                'deleted'   => array(
                                    'title'     => 'Deleted',
                                    'icon'      => 'fa-times-circle',
                                    'color'     => '#e53935'
                                )
                            );

                            $i = 1;

                            // LOOP
                            foreach ($itema as $item) {
                                $actiona = array(
                                    $item['id'],
                                    $_uccms_blog->postURL($item['id'], 0, $item)
                                );
                                ?>
                                <div class="item contain">
                                    <?php if ($statusa[$item['item_what']]['icon']) { ?>
                                        <div class="icon" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'background-color: ' .$statusa[$item['item_what']]['color']; } ?>"><i class="fa fa-fw <?php echo $statusa[$item['item_what']]['icon']; ?>"></i></div>
                                    <?php } ?>
                                    <div class="main">
                                        <?php if ($statusa[$item['item_what']]['title']) { ?>
                                            <div class="status" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'color: ' .$statusa[$item['item_what']]['color']; } ?>">
                                                <?php echo $statusa[$item['item_what']]['title']; ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($item['title']) { ?>
                                            <div class="title">
                                                <?php if ($item['status'] != 9) { ?>
                                                    <a href="./posts/edit/?id=<?php echo $item['id']; ?>">
                                                <?php } ?>
                                                <?php echo stripslashes($item['title']); ?>
                                                <?php if ($item['status'] != 9) { ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="time"><?php echo date('j M g:i A', strtotime($item['sort_by'])); ?></div>
                                        <?php if ($statusa[$item['item_what']]['actions']) { ?>
                                            <div class="actions">
                                                <i class="fa fa-caret-right"></i> <?php echo str_replace(array('{id}','{link_view}'), $actiona, $statusa[$item['item_what']]['actions']); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if ($i == 10) {
                                    break;
                                } else {
                                    ?>
                                    <div class="split"></div>
                                    <?php
                                    $i++;
                                }
                            }

                            unset($itema);

                        }

                        ?>

                    </div>

                </div>
            </td>

        </tr>
    </table>

</div>

<?php

function this_item_post($post) {
    global $_uccms_blog;
    if ($post['id']) {
        $out = '
        <div class="item">
            <div class="contain">
                <table>
                    <tr>
                        <td class="icon"><i class="far fa-newspaper"></i></td>
                        <td class="content">
                            <div class="title"><a href="./posts/edit/?id=' .$post['id']. '">' .stripslashes($post['title']). '</a></div>
                            <table>
                                <tr>
                                    <td class="status">
                                        ';
                                        if ($post['status'] == 1) {
                                            $out .= '<span class="published">Published</span>';
                                        } else if ($post['status'] == 2) {
                                            $out .= '<span class="draft">Scheduled</span>';
                                        } else {
                                            $out .= '<span class="draft">Draft</span>';
                                        }
                                        $out .= '
                                    </td>
                                    <td class="date">
                                        ';
                                        if ($post['status'] == 1) {
                                            $out .= date('j M', strtotime($post['dt_published']));
                                        } else {
                                            $out .= date('j M', strtotime($post['dt_saved']));
                                        }
                                        $out .= '
                                    </td>
                                    <td class="actions">
                                        <ul>
                                            <li><a href="./posts/edit/?id=' .$post['id']. '">Edit</a></li>
                                            <li><a href="' .$_uccms_blog->postURL($post['id'], 0, $post). '">View</a></li>
                                            ';
                                            if ($_uccms_blog->adminModulePermission() == 'p') {
                                                $out .= '<li><a href="./posts/delete/?id=' .$post['id']. '&from=dashboard" onclick="return confirmPrompt(this.href, \'Are you sure you want to delete this this?\');">Delete</a></li>';
                                            }
                                            $out .= '
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        ';
    }
    return $out;
}

?>