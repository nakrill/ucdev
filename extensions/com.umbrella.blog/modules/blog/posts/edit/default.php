<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET POST INFO
    $post_query = "SELECT * FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`id`=" .$id. ")";
    $post_q = sqlquery($post_query);
    $post = sqlfetch($post_q);

    // HAVE POST ID
    if ($post['id']) {

        // REVSOCIAL INFO
        if ($post['revsocial']) {
            $post['revsocial'] = json_decode(stripslashes($post['revsocial']), true);
        }

    }

}

if (!is_array($post['revsocial'])) $post['revsocial'] = array();

?>

<style type="text/css">

    #col_right > .contain {
        padding: 15px;
        background-color: #eee;
    }
    #col_right table td {
        padding: 5px 0;
    }
    #col_right table td:first-child {
        padding-left: 10px;
    }
    #col_right table td:last-child {
        padding-right: 10px;
    }
    #col_right table tr:nth-child(2n) td {
        background-color: transparent;
    }
    #col_right h4 {
        color: #444;
        text-transform: uppercase;
    }
    #col_right h4 a {
        color: #888;
    }
    #col_right h4 a.active {
        color: #444;
    }

    #col_right .dt_publish .contain {
        position: relative;
        text-align: right;
    }
    #col_right .dt_publish .icon_small {
        position: absolute;
        top: 8px;
        right: 5px;
    }
    #col_right input.date_picker, #col_right input.time_picker, #col_right input.date_time_picker {
        float: right;
        width: 180px !important;
    }

    #col_right .revsocial_streams .item {
        padding-bottom: 5px;
    }
    #col_right .revsocial_streams .item:last-child {
        padding-bottom: 0px;
    }
    #col_right .revsocial_streams .item i {
        padding-right: 4px;
        font-size: 1.4em;
        opacity: 0.4;
    }

    #col_right .images .items.thumbs {
        margin-right: -5%;
    }
    #col_right .images .item {
        float: left;
        position: relative;
        width: 45%;
        margin-right: 5%;
    }
    #col_right .images .item.main {
        float: none;
        width: 100%;
        margin: 0px;
    }
    #col_right .images .item img {
        width: 100%;
        height: auto;
    }
    #col_right .images .item .buttons {
        display: none;
        position: absolute;
        top: 5px;
        right: 5px;
    }
    #col_right .images .item .buttons a {
        color: #fff;
        text-shadow: 0 0 2px #777;
    }
    #col_right .images .item .buttons a:hover {
        color: #222;
    }
    #col_right .images .item:hover .buttons {
        display: block;
    }

    #col_right .toggle_author_select .select {
        float: right;
    }

    #cats_tags .switch {
        padding: 10px;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    #cats_tags .switch .item {
        padding-bottom: 8px;
    }
    #cats_tags .switch .checkbox {
        margin-top: -2px;
    }
    #cats_tags .add .add_link {
        text-align: center;
    }
    #cats_tags .add .add_form button {
        width: 100%;
        margin: -2px 0 0 -1px;
        padding: 4px 0;
        text-transform: uppercase;
        cursor: pointer;
    }

    #cats_tags .misc .select {
        width: 183px !important;
    }
    #cats_tags .misc .select > span {
        width: 128px !important;
    }
    #cats_tags .misc .select .select_options {
        width: auto !important;
        min-width: 183px !important;
    }

    #post_content {
        height: 300px;
    }

    #add_image_container {
        margin-top: 30px;
    }
    #add_image_container h6 {
        margin-bottom: 0px;
    }
    #add_image_container > .contain {
        border: 1px solid rgba(0, 0, 0, .1);
        background-color: rgba(0, 0, 0, .05);
    }
    #images {
        margin-top: 30px;
    }

</style>

<script type="text/javascript">
    $(function() {

        var new_cat_id = 0;

        // EDIT AUTHOR CLICK
        $('#col_right .toggle_author a').click(function(e) {
            e.preventDefault();
            $('#col_right .toggle_author').hide();
            $('#col_right .toggle_author_select').show();
        });

        // EDIT PUBLISH DATE/TIME
        $('#col_right .toggle_publish_dt a').click(function(e) {
            e.preventDefault();
            $('#col_right .toggle_publish_dt_content').toggle();
        });

        // ADD IMAGE CLICK
        $('#col_right .images .add').click(function(e) {
            e.preventDefault();
            $('#add_image_container').show();
            $('html, body').animate({
                scrollTop: $('#add_image_container').offset().top
            }, 700);
        });

        // EDIT IMAGE CLICK
        $('#col_right .images .item .buttons .edit').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('html, body').animate({
                scrollTop: $('#image-' +id).offset().top
            }, 700);
        });

        // DELETE IMAGE CLICK
        $('#col_right .images .item .buttons .delete').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('html, body').animate({
                scrollTop: $('#image-' +id).offset().top
            }, 700);
        });

        // CATEGORY / TAG SWITCH
        $('#cats_tags h4 a').click(function(e) {
            e.preventDefault();
            var what = $(this).attr('data-what');
            $('#cats_tags h4 a').removeClass('active');
            $(this).addClass('active');
            $('#cats_tags .switch').hide();
            $('#cats_tags .switch.' +what).show();
        });

        // CATEGORY / TAG ADD CLICK
        $('#cats_tags .add .add_link a').click(function(e) {
            e.preventDefault();
            var what = $(this).attr('data-what');
            $('#cats_tags .switch.' +what+ ' .add .add_link').hide();
            $('#cats_tags .switch.' +what+ ' .add .add_form').show();
            $('#cats_tags .switch.' +what+ ' .add .add_form input[type="text"]').focus();
        });

        // CATEGORY / TAG ADD
        $('#cats_tags .add .add_form button').click(function(e) {
            e.preventDefault();
            var what = $(this).attr('data-what');
            if (what == 'categories') {
                var what_singular = 'category';
            } else if (what == 'tags') {
                var what_singular = 'tag';
            }
            new_cat_id++;
            $('#cats_tags .switch.' +what+ ' .items').append('<input type="hidden" name="new_' +what_singular+ '_title[' +new_cat_id+ ']" value="' +$('#cats_tags .switch.' +what+ ' .add .add_form input[name="add_title"]').val()+ '" /><div class="item contain"><input type="checkbox" name="' +what_singular+ '[new][]" value="' +new_cat_id+ '" checked="checked" /> ' +$('#cats_tags .switch.' +what+ ' .add .add_form input[name="add_title"]').val()+ '</div>');
            BigTreeCustomControls();
            $('#cats_tags .switch.' +what+ ' .add .add_form').hide();
            $('#cats_tags .switch.' +what+ ' .add .add_link').show();
            $('#cats_tags .switch.' +what+ ' .add .add_form input[name="add_title"]').val('');
        });

    });
</script>

<form id="post-edit" enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="post[id]" value="<?php echo $post['id']; ?>" />

<h3><?php if ($post['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Post</h3>

<div class="contain row">

    <div id="col_left" class="col-lg-9">

        <fieldset>
            <label>Title</label>
            <input type="text" name="post[title]" value="<?php echo stripslashes($post['title']); ?>" />
        </fieldset>

        <fieldset>
            <label>Content</label>
            <div>
                <?php
                $field = array(
                    'key'       => 'post[content]', // The value you should use for the "name" attribute of your form field
                    'value'     => stripslashes($post['content']), // The existing value for this form field
                    'id'        => 'post_content', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        //'simple' => true
                    )
                );
                include(BigTree::path('admin/form-field-types/draw/html.php'));
                ?>
            </div>
        </fieldset>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="post[slug]" value="<?php echo stripslashes($post['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="post[meta_title]" value="<?php echo stripslashes($post['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="post[meta_description]" style="height: 64px;"><?php echo stripslashes($post['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="post[meta_keywords]" style="height: 64px;"><?php echo stripslashes($post['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </div>

    <div id="col_right" class="col-lg-3">
        <div class="contain">

            <?php

            // WEBTEXTTOOL UI CONFIG
            $wtt_ui = array(
                'id'        => uniqid(),
                'keywords'  => '',
                'callbacks' => array(
                    'page'  => array(
                        'before' => 'blog_WTT_callbacks.page.before',
                        //'after' => 'blog_WTT_callbacks.page.after'
                    ),
                    'word'  => array(

                    )
                )
            );

            ?>

            <style type="text/css">

                #wtt-<?php echo $wtt_ui['id']; ?> {
                    margin-bottom: 30px;
                }

                #wtt-<?php echo $wtt_ui['id']; ?> .results .suggestions .group h4 {
                    color: #5386A6;
                }

            </style>

            <script type="text/javascript">

                blog_WTT_callbacks = {

                    'page': {

                        'before': function(params, callback) {

                            params.page.body = tinymce.get('post_content').getContent();

                            var title = $('#post-edit input[name="post[meta_title]"]').val();
                            if (!title) {
                                title = $('#post-edit input[name="post[title]"]').val();
                            }
                            params.page.title = title;

                            params.page.description = $('#post-edit textarea[name="post[meta_description]"]').val();

                            return callback(params);

                        },

                        'after': function(data) {
                            //console.log('after', data);
                        }

                    }

                };

            </script>

            <?php

            // WEBTEXTTOOL UI
            include(SERVER_ROOT. 'custom/admin/ajax/misc/webtexttool/ui.php');

            ?>

            <div style="padding: 8px 0 5px; background-color: #fff; border: 1px solid #ddd;">
                <table style="width: 100%; margin: 0px; padding: 0px; border: 0px;">
                    <?php if ($post['id']) { ?>
                        <tr>
                            <td>Status</td>
                            <td style="text-align: right;">
                                <?php echo $_uccms_blog->statuses[$post['status']]; ?> <span style="font-size: .8em;">(<a href="<?php echo $_uccms_blog->postURL($post['id'], $post); ?>" target="_blank"><?php if ($post['status'] == 1) { echo 'View'; } else { echo 'Preview'; } ?></a>)</span>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if (($post['dt_created']) && ($post['dt_created'] != '0000-00-00 00:00:00')) { ?>
                        <tr>
                            <td>Created</td>
                            <td style="text-align: right;">
                                <?php echo date('n/j/Y g:i A', strtotime($post['dt_created'])); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>Author</td>
                        <td style="text-align: right;">
                            <?php
                            if (!$post['author_id']) $post['author_id'] = $_uccms_blog->adminID();
                            $acct_query = "
                            SELECT *
                            FROM `bigtree_users` AS `bu`
                            WHERE (bu.id=" .$post['author_id']. ")
                            LIMIT 1
                            ";
                            $acct_q = sqlquery($acct_query);
                            $acct = sqlfetch($acct_q);
                            ?>
                            <div class="toggle_author">
                                <a href="#" class="edit_author"><i class="fa fa-pencil"></i></a> <?php echo stripslashes($acct['name']); ?>
                            </div>
                            <div class="toggle_author_select" style="display: none;">
                                <select name="post[author]">
                                    <?php
                                    $auth_query = "SELECT `id`, `name` FROM `bigtree_users` ORDER BY `name` ASC, `id` ASC";
                                    $auth_q = sqlquery($auth_query);
                                    while ($auth = sqlfetch($auth_q)) {
                                        ?>
                                        <option value="<?php echo $auth['id']; ?>" <?php if ($auth['id'] == $post['author_id']) { echo 'selected="selected"'; } ?>><?php echo stripslashes($auth['name']); ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <?php if (!$post['id']) { ?>
                        <tr>
                            <td>Date & Time</td>
                            <td class="dt_publish">
                                <div class="contain">
                                    <?php

                                    // FIELD VALUES
                                    $field = array(
                                        'id'        => 'dt_publish',
                                        'key'       => 'post[dt_publish]',
                                        'value'     => (($post['dt_publish'] != '0000-00-00 00:00:00') ? $post['dt_publish'] : ''),
                                        'required'  => false,
                                        'options'   => array(
                                            'default_now'   => true
                                        )
                                    );

                                    // INCLUDE FIELD
                                    include(BigTree::path('admin/form-field-types/draw/datetime.php'));

                                    ?>
                                </div>
                            </td>
                        </tr>
                    <?php } else {?>

                        <?php

                        // GET DATE TO DISPLAY
                        if ($post['status'] == 0) {
                            if ($post['dt_publish'] != '0000-00-00 00:00:00') {
                                $status_title = 'Publish';
                                $status_date = $post['dt_publish'];
                            } else {
                                $status_title = 'Date';
                                $status_date = $post['dt_saved'];
                            }
                        } else if ($post['status'] == 1) {
                            $status_title = 'Published';
                            $status_date = $post['dt_published'];
                        } else if ($post['status'] == 2) {
                            $status_title = 'Publish';
                            $status_date = $post['dt_publish'];
                        }
                        if (!$status_date) {
                            $status_title = 'Date';
                            $status_date = date('Y-m-d H:i:s');
                        }

                        ?>

                        <tr>
                            <td><?php echo $status_title; ?></td>
                            <td class="toggle_publish_dt" style="text-align: right;">
                                <a href="#" class="edit_publish_dt"><i class="fa fa-pencil"></i></a> <?php echo date('n/j/Y g:i A', strtotime($status_date)); ?>
                            </td>
                        </tr>

                        <tr class="toggle_publish_dt_content" style="display: none;">
                            <td colspan="2" class="dt_publish" style="padding-top: 0px;">
                                <div class="contain">
                                    <?php

                                    // FIELD VALUES
                                    $field = array(
                                        'id'        => 'dt_publish',
                                        'key'       => 'post[dt_publish]',
                                        'value'     => $status_date,
                                        'required'  => false,
                                        'options'   => array(
                                            'default_now'   => true
                                        )
                                    );

                                    // INCLUDE FIELD
                                    include(BigTree::path('admin/form-field-types/draw/datetime.php'));

                                    ?>
                                </div>
                            </td>
                        </tr>

                        <?php if (($post['dt_saved']) && ($post['dt_saved'] != '0000-00-00 00:00:00')) { ?>
                            <tr>
                                <td>Last Saved</td>
                                <td style="text-align: right;">
                                    <?php echo date('n/j/Y g:i A', strtotime($post['dt_saved'])); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>

            <div style="padding: 15px 0 10px 0; text-align: center;">
                <input name="draft" class="" type="submit" value="Save Draft" />&nbsp;&nbsp;<input name="publish" class="blue" type="submit" value="Publish" />
            </div>

            <?php

            // REVSOCIAL API KEY
            $api_key = $cms->getSetting('rs_api-key');

            // HAVE API KEY
            if ($api_key) {

                // REST CLIENT
                require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

                // INIT REST API CLASS
                $_api = new RestClient(array(
                    'base_url' => 'https://api.revsocial.com/v1'
                ));

                try {

                    // MAKE API CALL
                    $result = $_api->get('streams/all', array(
                        'api_key'   => $api_key,
                        'data'      => array(
                            'active'    => true,
                            'tracking'  => true
                        )
                    ));

                    // API RESPONSE
                    $resp = $result->decode_response();

                    // HAVE STREAMS
                    if (count((array)$resp->streams) > 0) {

                        // BUILD STREAM ARRAY
                        foreach ($resp->streams as $stream) {
                            $streama[$stream->id] = (array)$stream;
                        }

                        // ICONS
                        $icona = array(
                            'facebook' => '<i class="fa fa-facebook-square"></i>',
                            'twitter' => '<i class="fa fa-twitter-square"></i>',
                            'instagram' => '<i class="fa fa-instagram"></i>'
                        );

                        ?>
                        <div class="revsocial_streams" style="padding-top: 30px;">
                            <div class="contain">
                                <?php if ($post['revsocial']['published']) { ?>
                                    <h6>Posted To Streams</h6>
                                    <div class="items">
                                        <?php
                                        if (is_array($post['revsocial']['streams'])) {
                                            foreach ($post['revsocial']['streams'] as $stream_id => $stream_enabled) {
                                                ?>
                                                <div class="item contain">
                                                    <?php echo $icona[$streama[$stream_id]['source']]; ?><?php echo stripslashes($streama[$stream_id]['title']); ?>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php } else { ?>
                                    <h6>Post To Streams</h6>
                                    <div class="items">
                                        <?php foreach ($streama as $stream) { ?>
                                            <div class="item contain">
                                                <input type="checkbox" name="revsocial[streams][<?php echo $stream['id']; ?>]" value="1" <?php if ($post['revsocial']['streams'][$stream['id']]) { ?>checked="checked"<?php } ?> /> <?php echo $icona[$stream['source']]; ?><?php echo stripslashes($stream['title']); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                        <?php
                    }

                } catch (Exception $e) {
                    ?>
                    <div class="revsocial_api-error" style="margin-top: 20px;">
                        Error connecting to RevSocial API.
                    </div>
                    <?php
                }

            }

            ?>

            <?php if ($post['id']) { ?>
                <div class="images" style="padding-top: 30px;">
                    <div class="contain">
                        <h6 style="float: left;">Images</h6>
                        <a class="add" href="#" style="float: right; padding-top: 3px;"><span></span>Add Image</a>
                    </div>

                    <?php

                    // GET IMAGES
                    $image_query = "SELECT * FROM `" .$_uccms_blog->tables['post_images']. "` WHERE (`post_id`=" .$post['id']. ") ORDER BY `sort` ASC, `id` ASC";
                    $image_q = sqlquery($image_query);

                    // NUMBER OF IMAGES
                    $num_images = sqlrows($image_q);

                    // HAVE IMAGES
                    if ($num_images > 0) {

                        ?>

                        <div class="items contain">

                            <?php

                            $ii = 1;

                            // LOOP
                            while ($image = sqlfetch($image_q)) {
                                if ($image['image']) {

                                    if ($ii == 2) {
                                        ?>
                                        </div>
                                        <div class="contain" style="clear: both; margin: 15px 0 5px 0; padding-bottom: 2px; border-bottom: 1px solid #ccc;">
                                            <b>Gallery</b>
                                            <span style="float: right;">[gallery]</span>
                                        </div>
                                        <div class="items thumbs contain">
                                        <?php
                                    }

                                    ?>
                                    <div class="item <?php if ($ii == 1) { echo 'main'; } ?>">
                                        <img src="<?php echo $_uccms_blog->imageBridgeOut($image['image'], 'posts'); ?>" alt="<?php echo $image['alt']; ?>" />
                                        <div class="buttons">
                                            <a href="#" class="edit" data-id="<?php echo $image['id']; ?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href="#" class="delete" data-id="<?php echo $image['id']; ?>"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <?php
                                    $ii++;
                                }
                            }

                            ?>

                        </div>

                        <?php

                    }

                    ?>
                </div>
            <?php } ?>

            <div id="cats_tags" style="padding-top: 30px;">
                <h6><a href="#" class="active" data-what="categories">Categories</a>&nbsp;&nbsp;<span style="font-weight: normal;">|</span>&nbsp;&nbsp;<a href="#" data-what="tags">Tags</a>&nbsp;&nbsp;<span style="font-weight: normal;">|</span>&nbsp;&nbsp;<a href="#" data-what="misc">Misc</a></h6>
                <div style="background-color: #fff;">

                    <div class="categories switch contain">
                        <div class="items">
                            <?php

                            // ARRAY OF CATEGORIES
                            $cata = array();

                            // GET CURRENT RELATIONS
                            $query = "SELECT `category_id` FROM `" .$_uccms_blog->tables['post_categories']. "` WHERE (`post_id`=" .$id. ")";
                            $q = sqlquery($query);
                            while ($row = sqlfetch($q)) {
                                $cata[$row['category_id']] = $row['category_id'];
                            }

                            // GET CATEGORIES
                            $query = "SELECT `id`, `title` FROM `" .$_uccms_blog->tables['categories']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                            $q = sqlquery($query);
                            while ($cat = sqlfetch($q)) {
                                ?>
                                <div class="item contain">
                                    <input type="checkbox" name="category[<?php echo $cat['id']; ?>]" value="1" <?php if ($cata[$cat['id']]) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($cat['title']); ?>
                                </div>
                                <?php
                            }

                            ?>
                        </div>
                        <div class="add">
                            <div class="add_link"><a href="#" data-what="categories">Add Category</a></div>
                            <div class="add_form" style="display: none;">
                                <input type="text" name="add_title" value="" placeholder="Category Name" style="width: 164px;" />
                                <button data-what="categories">Add Category</button>
                            </div>
                        </div>
                    </div>

                    <div class="tags switch contain" style="display: none;">
                        <div class="items">
                            <?php

                            // ARRAY OF TAGS
                            $taga = array();

                            // GET CURRENT RELATIONS
                            $query = "SELECT `tag_id` FROM `" .$_uccms_blog->tables['post_tags']. "` WHERE (`post_id`=" .$id. ")";
                            $q = sqlquery($query);
                            while ($row = sqlfetch($q)) {
                                $taga[$row['tag_id']] = $row['tag_id'];
                            }

                            // GET TAGS
                            $query = "SELECT `id`, `title` FROM `" .$_uccms_blog->tables['tags']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                            $q = sqlquery($query);
                            while ($tag = sqlfetch($q)) {
                                ?>
                                <div class="item contain">
                                    <input type="checkbox" name="tag[<?php echo $tag['id']; ?>]" value="1" <?php if ($taga[$tag['id']]) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($tag['title']); ?>
                                </div>
                                <?php
                            }

                            ?>
                        </div>
                        <div class="add">
                            <div class="add_link"><a href="#" data-what="tags">Add Tag</a></div>
                            <div class="add_form" style="display: none;">
                                <input type="text" name="add_title" value="" placeholder="Tag" style="width: 164px;" />
                                <button data-what="tags">Add Tag</button>
                            </div>
                        </div>
                    </div>

                    <div class="misc switch contain" style="display: none;">

                        <?php

                        // HAS BUSINESS DIRECTORY EXTENSION
                        if (is_dir(SERVER_ROOT. 'extensions/com.umbrella.business-directory')) {

                            ?>

                            <div class="field" style="width: 183px;">
                                <div class="label">Business</div>
                                <select name="post[business_id]">
                                    <option value="0">None</option>
                                    <?php
                                    $business_query = "SELECT * FROM `uccms_businessdirectory_businesses` WHERE (`pending`=0) AND (`status`!=9) ORDER BY `name` ASC";
                                    $business_q = sqlquery($business_query);
                                    while ($business = sqlfetch($business_q)) {
                                        ?>
                                        <option value="<?php echo $business['id']; ?>" <?php if ($business['id'] == $post['business_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($business['name']); ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <?php
                        }
                        ?>

                    </div>

                </div>
            </div>

        </div>
    </div>

</div>

</form>

<?php if ($post['id']) { ?>

    <style type="text/css">

        #images .add_container {
            padding: 10px;
            background-color: #fafafa;
            border-bottom: 1px solid #ccc;
        }

        #images .add_container td {
            vertical-align: top;
        }

        #images .table section a, #categories.table section a {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #images .table > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }

        #images .table > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }

        #images .table > ul li section:first-child {
            padding-left: 0px;
        }

        #images .items .item .image {
            width: 500px;
            padding: 0 25px;
        }
        #images .items .item .title input {
            width: 100%;
        }

        #images .table .currently .remove_resource {
            margin-top: -3px;
            margin-bottom: -10px;
        }

        #images .items .item .title fieldset label {
            margin-bottom: 2px;
            text-align: left;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function() {

            // IMAGES - SORTABLE
            $('#images ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('id').replace('row_', ''));
                    });
                    $('#images input[name="sort"]').val(sort.join());
                }
            });

            // IMAGES - ADD CLICK
            $('#images .add_resource').click(function(e) {
                e.preventDefault();
                $('#add_image_container').toggle();
            });

            // IMAGES - REMOVE CLICK
            $('#images .icon_delete').click(function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var del = $('#images input[name="delete"]').val();
                if (del) del += ',';
                del += id;
                $('#images input[name="delete"]').val(del);
                $('#images li#image-' +id).fadeOut(500);
            });

            // IMAGES - NEW FORM SUBMIT
            $('#add_image_container form').submit(function(e) {
                var c = confirm('Are you sure you want to add this image?\nDoing so will reload the page, so make sure your post is saved first.');
                return c;
            });

            // IMAGES - FORM SUBMIT
            $('#images form').submit(function(e) {
                var c = confirm('Are you sure you want to save the images?\nDoing so will reload the page, so make sure your post is saved first.');
                return c;
            });

        });

    </script>

    <a name="images"></a>

    <div id="add_image_container" style="display: none;">
        <h6>Add Image</h6>
        <div class="contain">

            <form enctype="multipart/form-data" action="./images/process/" method="post">
            <input type="hidden" name="post[id]" value="<?php echo $post['id']; ?>" />

            <table style="margin-bottom: 0px; border: 0px;">
                <tr>
                    <td style="width: 470px;">
                        <?php
                        $field = array(
                            'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                            'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                            'key'       => 'new', // The value you should use for the "name" attribute of your form field
                            'value'     => '', // The existing value for this form field
                            'id'        => 'image-new', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'image' => true
                            ),
                            'required'  => false // A boolean value of whether this form field is required or not
                        );
                        include(BigTree::path('admin/form-field-types/draw/upload.php'));
                        ?>
                    </td>
                    <td>
                        <input type="text" name="new-caption" value="" placeholder="Caption / Alt Text" style="width: 300px;" />
                    </td>
                    <td style="text-align: right;">
                        <input class="blue" type="submit" value="Add" />
                    </td>
                </tr>
            </table>

            </form>

        </div>
    </div>

    <div id="images" class="container legacy" style="margin-bottom: 0px; border: 0px none;">

        <form enctype="multipart/form-data" action="./images/process/" method="post">
        <input type="hidden" name="post[id]" value="<?php echo $post['id']; ?>" />

        <div class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Images</h2>
                <a class="add_resource add" href="#"><span></span>Add Image</a>
            </summary>

            <?php

            // GET IMAGES
            $image_query = "SELECT * FROM `" .$_uccms_blog->tables['post_images']. "` WHERE (`post_id`=" .$post['id']. ") ORDER BY `sort` ASC, `id` ASC";
            $image_q = sqlquery($image_query);

            // NUMBER OF IMAGES
            $num_images = sqlrows($image_q);

            // HAVE IMAGES
            if ($num_images > 0) {

                ?>

                <ul class="items ui-sortable">

                    <?php

                    // LOOP
                    while ($image = sqlfetch($image_q)) {

                        ?>

                        <li id="image-<?php echo $image['id']; ?>" class="item contain">
                            <section class="sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="image">
                                <?php
                                $field = array(
                                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                    'key'       => 'image-' .$image['id'], // The value you should use for the "name" attribute of your form field
                                    'value'     => ($image['image'] ? $_uccms_blog->imageBridgeOut($image['image'], 'posts') : ''), // The existing value for this form field
                                    'id'        => 'image-' .$image['id'], // A unique ID you can assign to your form field for use in JavaScript
                                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                    'options'   => array(
                                        'image' => true
                                    ),
                                    'required'  => false // A boolean value of whether this form field is required or not
                                );
                                include(BigTree::path('admin/form-field-types/draw/upload.php'));
                                ?>
                            </section>
                            <section class="title">
                                <fieldset>
                                    <input type="text" name="image-<?php echo $image['id']; ?>-caption" value="<?php echo stripslashes($image['caption']); ?>" placeholder="Caption / Alt Text" style="width: 300px;" />
                                </fieldset>
                            </section>
                            <section class="view_action">
                                <a class="icon_delete" href="#" data-id="<?php echo $image['id']; ?>"></a>
                            </section>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <footer>
                    <input class="blue" type="submit" value="Save" />
                </footer>

                <?php

            } else {

                ?>

                <div style="padding: 10px; text-align: center;">
                    No item images added yet.
                </div>

                <?php

            }

            ?>

        </div>

        </form>

    </div>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>