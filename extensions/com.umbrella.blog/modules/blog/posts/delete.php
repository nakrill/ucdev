<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE BUSINESS
    $query = "UPDATE `" .$_uccms_blog->tables['posts']. "` SET " .uccms_createSet($columns). ", `dt_deleted`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Post', 'Post deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Post', 'Failed to delete post.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Post', 'No posts specified.');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT.'posts/');
}

?>