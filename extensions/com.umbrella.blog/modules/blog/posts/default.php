<style type="text/css">

    #items header span, #items .item section {
        text-align: left;
    }

    #items .post_title {
        width: 300px;
    }
    #items .post_author {
        width: 150px;
    }
    #items .post_categories {
        width: 160px;
    }
    #items .post_tags {
        width: 200px;
    }
    #items .post_date {
        width: 130px;
    }
    #items .post_status {
        width: 90px;
    }
    #items .post_edit {
        width: 55px;
    }
    #items .post_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#posts .status select').change(function() {
            $('#form_posts').submit();
        });

    });

</script>

<div id="posts">

    <form id="form_posts">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <div class="status contain" style="margin-bottom: -28px;">
        <div style="float: right; padding-left: 10px;">
            <select name="status">
                <option value="">Status (All)</option>
                <option value="published" <?php if ($_REQUEST['status'] == 'published') { ?>selected="selected"<?php } ?>>Published</option>
                <option value="scheduled" <?php if ($_REQUEST['status'] == 'scheduled') { ?>selected="selected"<?php } ?>>Secheduled</option>
                <option value="draft" <?php if ($_REQUEST['status'] == 'draft') { ?>selected="selected"<?php } ?>>Draft</option>
            </select>
        </div>
        <div style="float: right; padding-left: 10px;">
            <select name="category_id">
                <option value="">Category (All)</option>
                <?php
                $cat_query = "SELECT `id`, `title` FROM `" .$_uccms_blog->tables['categories']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                $cat_q = sqlquery($cat_query);
                while ($cat = sqlfetch($cat_q)) {
                    ?>
                    <option value="<?php echo $cat['id']; ?>" <?php if ($cat['id'] == $_REQUEST['category_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($cat['title']); ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Posts</h2>
            <a class="add_resource add" href="./edit/"><span></span>Add Post</a>
        </summary>
        <header style="clear: both;">
            <span class="post_title">Title</span>
            <span class="post_author">Author</span>
            <span class="post_categories">Categories</span>
            <span class="post_date">Date</span>
            <span class="post_status">Status</span>
            <span class="post_edit">Edit</span>
            <span class="post_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/posts/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_blog->Extension?>/ajax/admin/posts/get-page/?page=1&status=" +escape($('#posts .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>&tag_id=<?=$_REQUEST['tag_id']?>&author_id=<?=$_REQUEST['author_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_blog->Extension?>/ajax/admin/posts/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=" +escape($('#posts .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>&tag_id=<?=$_REQUEST['tag_id']?>&author_id=<?=$_REQUEST['author_id']?>");
        return false;
    });
</script>