<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Blog';

// BIGTREE
$bigtree['css'][]   = 'master.css';
$bigtree['js'][]    = 'master.js';

// MODULE CLASS
$_uccms_blog = new uccms_Blog;

// MODULE MAIN NAV SELECTED ARRAY
if ($bigtree['path'][2]) {
    $mmnsa[$bigtree['path'][2]] = 'active';
} else {
    $mmnsa['dashboard'] = 'active';
}

?>

<style type="text/css">

    h1 a.first, h1 .divider {
        display: none !important;
    }

</style>

<nav class="main">
    <section>
        <ul>
            <li class="<?php echo $mmnsa['dashboard']; ?>">
                <a href="<?=MODULE_ROOT;?>"><span class="dashboard"></span>Dashboard</a>
                <?php /*
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>calendar/">Calendar</a></li>
                </ul>
                */ ?>
            </li>
            <li class="<?php echo $mmnsa['categories']; ?>">
                <a href="<?=MODULE_ROOT;?>categories/"><span class="pages"></span>Categories</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>categories/">All</a></li>
                    <li class="split-top"><a href="<?=MODULE_ROOT;?>categories/edit/">New Category</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['tags']; ?>">
                <a href="<?=MODULE_ROOT;?>tags/"><span class="pages"></span>Tags</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>tags/">All</a></li>
                    <li class="split-top"><a href="<?=MODULE_ROOT;?>tags/?do=add">New Tag</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['posts']; ?>">
                <a href="<?=MODULE_ROOT;?>posts/"><span class="developer"></span>Posts</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>posts/">All</a></li>
                    <li><a href="<?=MODULE_ROOT;?>posts/?status=published">Published</a></li>
                    <li><a href="<?=MODULE_ROOT;?>posts/?status=scheduled">Scheduled</a></li>
                    <li><a href="<?=MODULE_ROOT;?>posts/?status=draft">Drafts</a></li>
                    <li class="split-top"><a href="<?=MODULE_ROOT;?>posts/edit/?id=new">New Post</a></li>
                </ul>
            </li>
            <?php if ($_uccms_blog->adminModulePermission() == 'p') { ?>
                <li class="<?php echo $mmnsa['settings']; ?>">
                    <a href="<?=MODULE_ROOT;?>settings/"><span class="settings"></span>Settings</a>
                    <ul>
                        <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </section>
</nav>
