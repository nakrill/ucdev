<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET tag INFO
    $tag_query = "SELECT * FROM `" .$_uccms_blog->tables['tags']. "` WHERE (`id`=" .$id. ")";
    $tag_q = sqlquery($tag_query);
    $tag = sqlfetch($tag_q);

}

?>

<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

    #tags .tag_title {
        width: 530px;
    }
    #tags .tag_posts {
        width: 100px;
    }
    #tags .tag_status {
        width: 100px;
    }
    #tags .tag_edit {
        width: 55px;
    }
    #tags .tag_delete {
        width: 55px;
    }

</style>

<div class="container legacy">

    <form enctype="multipart/form-data" action="../process/" method="post">
    <input type="hidden" name="tag[id]" value="<?php echo $tag['id']; ?>" />

    <header>
        <h2><?php if ($tag['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Tag</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Title</label>
                    <input type="text" name="tag[title]" value="<?php echo stripslashes($tag['title']); ?>" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="tag[active]" value="1" <?php if ($tag['active']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

            </div>

            <div class="right">

            </div>

        </div>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="tag[slug]" value="<?php echo stripslashes($tag['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="tag[meta_title]" value="<?php echo stripslashes($tag['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="tag[meta_description]" style="height: 64px;"><?php echo stripslashes($tag['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="tag[meta_keywords]" style="height: 64px;"><?php echo stripslashes($tag['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>