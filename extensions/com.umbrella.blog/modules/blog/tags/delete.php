<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_blog->tables['post_tags']. "` WHERE (`tag_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_blog->tables['tags']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Tag', 'Tag deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Tag', 'Failed to delete tag.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Tag', 'No tag specified.');
}

BigTree::redirect(MODULE_ROOT.'tags/');

?>