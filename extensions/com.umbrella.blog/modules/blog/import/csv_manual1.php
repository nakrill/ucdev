<?php

//echo 'Memory Limit: ' .ini_get('memory_limit'). "\n";
/*
    - If limit is too low, edit /site/php.ini and set memory_limit = 512M
    - Running from command line: php -d memory_limit=512M manual.php
*/

// CONTENT TO FIND
$content_replace_find = array(
);

// CONTENT TO REPLACE
$content_replace_replace = array(
);

##########################################################

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.blog/modules/blog/import/csv_manual1.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INIT BLOG CLASS
$_uccms_blog = new uccms_Blog;

// READ FILE
if (($handle = fopen('data.csv', "r")) !== FALSE) {

    // NEW REWRITE FILE
    $rewrite_file = dirname(__FILE__). '/rewrite-' .date('Y-m-d_H:i:s'). '.txt';

    // CATEGORY ARRAY
    $ca = array();

    // TAG ARRAY
    $ta = array();

    // LOOP
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

        $post = array();

        // TITLE
        $post['title'] = $data[2];

        // SLUG
        if ($data[1]) {
            $post['slug'] = $_uccms_blog->makeRewrite($data[1]);
        } else {
            $post['slug'] = $_uccms_blog->makeRewrite($post['title']);
        }

        // STATUS
        if ($data[4] == 1) {
            $post['status'] = 1;
            $post['dt_published']   = date('Y-m-d H:i:s', strtotime($data[5]));
            $post['dt_created']     = $post['dt_published'];
            $post['dt_saved']       = $post['dt_published'];
        } else {
            $post['status'] = 0;
            $post['dt_saved']       = date('Y-m-d H:i:s', strtotime($data[5]));
            $post['dt_created']     = $post['dt_saved'];
        }

        // CONTENT
        $post['content'] = htmlspecialchars_decode($data[3]);

        // HAVE ID - IS UPDATING
        if ($post['id']) {
        // if ($data[0]) {

            // DB QUERY
            $query = "UPDATE `" .$_uccms_blog->tables['posts']. "` SET " .uccms_createSet($post). " WHERE (`id`=" .(int)$data[0]. ")";

        // NO ID - IS NEW
        } else {

            // DB QUERY
            $query = "INSERT INTO `" .$_uccms_blog->tables['posts']. "` SET " .uccms_createSet($post). "";

        }

        //echo $query;
        //exit;

        // QUERY SUCCESSFUL
        if (sqlquery($query)) {

            // UPDATED
            if ($post['id']) {
                echo $post['title']. ' - Updated';

            // ADDED
            } else {

                echo $post['title']. ' - Added';

                // NEW BUSINESS ID
                $post['id'] = sqlid();

                // POST LINK FOR REWRITE
                if ($data[8]) {
                    $rewrite = 'Redirect 301 ' .$data[8]. ' ' .WWW_ROOT . $_uccms_blog->frontendPath(). '/post/' .$post['slug']. '-' .$post['id']. '/' . "\n";
                    file_put_contents($rewrite_file, $rewrite, FILE_APPEND);
                }

            }

            ###############################
            # CATEGORIES
            ###############################

            $create_relation = false;
            unset($cat_del);

            // GET CURRENT CATEGORY RELATIONS
            $rel_query = "SELECT `category_id` FROM `" .$_uccms_blog->tables['post_categories']. "` WHERE (`post_id`=" .$post['id']. ")";
            $rel_q = sqlquery($rel_query);
            while ($rel = sqlfetch($rel_q)) {
                $cat_del[$rel['category_id']] = $rel['category_id']; // SAVE TO DELETE ARRAY
            }

            // HAVE CATEGORIES
            if ($data[6]) {

                // POST CATEGORIES
                $pca = explode('|', $data[6]);

                // LOOP
                foreach ($pca as $cat_title) {

                    // CLEAN UP
                    $cat_title = trim(sqlescape($cat_title));

                    // HAVE TITLE
                    if ($cat_title) {

                        // NOT IN ARRAY
                        if (!$ca[$cat_title]) {

                            // LOOK FOR MATCH
                            $cat_query = "SELECT `id` FROM `" .$_uccms_blog->tables['categories']. "` WHERE (`title`='" .$cat_title. "')";
                            $cat_q = sqlquery($cat_query);
                            $cat = sqlfetch($cat_q);

                            // ADD TO ARRAY
                            $ca[$cat_title] = $cat['id'];

                        }

                        // FOUND
                        if ($ca[$cat_title]) {

                            // RELATION ALREADY EXISTS
                            if ($cat_del[$ca[$cat_title]]) {
                                unset($cat_del[$ca[$cat_title]]);

                            // CREATE RELATION
                            } else {
                                $create_relation = true;
                            }

                        // NOT FOUND
                        } else {

                            // MAKE SLUG
                            $cat_slug = $_uccms_blog->makeRewrite($cat_title);

                            // CREATE NEW
                            $newcat_query = "INSERT INTO `" .$_uccms_blog->tables['categories']. "` SET `title`='" .$cat_title. "', `slug`='" .$cat_slug. "'";
                            $newcat_q = sqlquery($newcat_query);

                            // NEW ID
                            $ca[$cat_title] = sqlid();

                            $create_relation = true;

                        }

                        // CREATE RELATION
                        if (($create_relation) && ($ca[$cat_title])) {
                            $rel_query = "INSERT INTO `" .$_uccms_blog->tables['post_categories']. "` SET `category_id`=" .$ca[$cat_title]. ", `post_id`=" .$post['id'];
                            sqlquery($rel_query);
                        }

                        // HAVE ANY TO DELETE
                        if (count($cat_del) > 0) {
                            foreach ($cat_del as $cat_id) {
                                $del_query = "DELETE FROM `" .$_uccms_blog->tables['post_categories']. "` WHERE (`post_id`=" .$post['id']. ") AND (`" .$type. "_id`=" .$cat_id. ")";
                                sqlquery($del_query);
                            }
                        }

                    }
                }

            }

            ###############################
            # TAGS
            ###############################

            $create_relation = false;
            unset($tag_del);

            // GET CURRENT TAG RELATIONS
            $rel_query = "SELECT `tag_id` FROM `" .$_uccms_blog->tables['post_tags']. "` WHERE (`post_id`=" .$post['id']. ")";
            $rel_q = sqlquery($rel_query);
            while ($rel = sqlfetch($rel_q)) {
                $tag_del[$rel['tag_id']] = $rel['tag_id']; // SAVE TO DELETE ARRAY
            }

            // HAVE TAGS
            if ($data[7]) {

                // POST TAGS
                $pta = explode('|', $data[7]);

                // LOOP
                foreach ($pta as $tag_title) {

                    // CLEAN UP
                    $tag_title = trim(sqlescape($tag_title));

                    // HAVE TITLE
                    if ($tag_title) {

                        // NOT IN ARRAY
                        if (!$ta[$tag_title]) {

                            // LOOK FOR MATCH
                            $tag_query = "SELECT `id` FROM `" .$_uccms_blog->tables['tags']. "` WHERE (`title`='" .$tag_title. "')";
                            $tag_q = sqlquery($tag_query);
                            $tag = sqlfetch($tag_q);

                            // ADD TO ARRAY
                            $ta[$tag_title] = $tag['id'];

                        }

                        // FOUND
                        if ($ta[$tag_title]) {

                            // RELATION ALREADY EXISTS
                            if ($tag_del[$ta[$tag_title]]) {
                                unset($tag_del[$ta[$tag_title]]);

                            // CREATE RELATION
                            } else {
                                $create_relation = true;
                            }

                        // NOT FOUND
                        } else {

                            // MAKE SLUG
                            $tag_slug = $_uccms_blog->makeRewrite($tag_title);

                            // CREATE NEW
                            $newcat_query = "INSERT INTO `" .$_uccms_blog->tables['tags']. "` SET `title`='" .$tag_title. "', `slug`='" .$tag_slug. "'";
                            $newcat_q = sqlquery($newcat_query);

                            // NEW ID
                            $ta[$tag_title] = sqlid();

                            $create_relation = true;

                        }

                        // CREATE RELATION
                        if (($create_relation) && ($ta[$tag_title])) {
                            $rel_query = "INSERT INTO `" .$_uccms_blog->tables['post_tags']. "` SET `tag_id`=" .$ta[$tag_title]. ", `post_id`=" .$post['id'];
                            sqlquery($rel_query);
                        }

                        // HAVE ANY TO DELETE
                        if (count($tag_del) > 0) {
                            foreach ($tag_del as $tag_id) {
                                $del_query = "DELETE FROM `" .$_uccms_blog->tables['post_tags']. "` WHERE (`post_id`=" .$post['id']. ") AND (`" .$type. "_id`=" .$tag_id. ")";
                                sqlquery($del_query);
                            }
                        }

                    }
                }

            }

        // QUERY FAILED
        } else {

            echo $post['title']. ' - Failed';

        }

        unset($post);

        echo ($cli ? "\n" : '<br />');

    }

    fclose($handle);

// FAILED TO READ FILE
} else {
    echo 'Failed to open data.csv';
}

echo ($cli ? "\n" : '<br />');
echo '<b>All done.</b>';

?>