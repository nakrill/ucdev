<?php

//echo 'Memory Limit: ' .ini_get('memory_limit'). "\n";
/*
    - If limit is too low, edit /site/php.ini and set memory_limit = 512M
    - Running from command line: php -d memory_limit=512M manual.php
*/

// CONTENT TO FIND
$content_replace_find = array(
);

// CONTENT TO REPLACE
$content_replace_replace = array(
);

##########################################################

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.blog/modules/blog/import/wordpress_manual1.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INCLUDE COMPOSER
include_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

// INIT BLOG CLASS
$_uccms_blog = new uccms_Blog;

// READ XML
$xml = simplexml_load_file(dirname(__FILE__). '/data.xml');

// HAVE XML
if ($xml) {

    // CATEGORY & TAG ARRAY
    $cta = array();

    // LOOP THROUGH ITEMS
    foreach ($xml->channel->item as $content) {

        // PARSE DATA
        $namespaces         = $content->getNameSpaces(true);
        $wp_children        = $content->children($namespaces['wp']);
        $dc_children        = $content->children($namespaces['dc']);
        $content_children   = $content->children($namespaces['content']);
        $category_children  = $content->children($namespaces['category']);

        // IS POST
        if ($wp_children->post_type == 'post') {

            $post = array();

            // LINK
            if ($wp_children->post_name) {
                $post['slug'] = (string)$wp_children->post_name;
            } else {
                $link = (string)$content->link;
                $lpa = explode('/', $link);
                if ($lpa[count($lpa)-1]) {
                    $post['slug'] = $_uccms_blog->makeRewrite($post['title']);
                } elseif ($lpa[count($lpa)-2]) {
                    $post['slug'] = $lpa[count($lpa)-2];
                } else {
                    $post['slug'] = $_uccms_blog->makeRewrite($post['title']);
                }
            }

            // STATUS
            $status = (string)$wp_children->status;
            if ($status == 'publish') {
                $post['status'] = 1;
                $post['dt_published']   = date('Y-m-d H:i:s', strtotime($wp_children->post_date));
                $post['dt_created']     = $post['dt_published'];
                $post['dt_saved']       = $post['dt_published'];
            } else {
                $post['status'] = 0;
                $post['dt_saved']       = date('Y-m-d H:i:s', strtotime($wp_children->post_date));
                $post['dt_created']     = $post['dt_saved'];
            }

            // TITLE
            $post['title'] = (string)$content->title;

            // CONTENT
            $post['content'] = this_nl2p((string)$content_children->encoded);
            $post['content'] = preg_replace('/[^[:print:]]/', ' ', $post['content']); // REMOVE WEIRD CHARACTERS
            $post['content'] = str_replace($content_replace_find, $content_replace_replace, $post['content']);

            // author_id

            /*
            foreach ($wp_children->postmeta as $wp_meta) {
                $meta_key = $wp_meta->meta_key;
                if ($meta_key == '_thumbnail_id') {
                    $post_thumbnail_id = (int)$wp_meta->meta_value;
                }
            }
            */

            //echo print_r($post);
            //exit;

            // HAVE ID - IS UPDATING
            if ($post['id']) {

                // DB QUERY
                $query = "UPDATE `" .$_uccms_blog->tables['posts']. "` SET " .uccms_createSet($post). "";

            // NO ID - IS NEW
            } else {

                // DB QUERY
                $query = "INSERT INTO `" .$_uccms_blog->tables['posts']. "` SET " .uccms_createSet($post). "";

            }

            //echo $query;
            //exit;

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {

                // UPDATED
                if ($post['id']) {
                    echo $post['title']. ' - Updated';

                // ADDED
                } else {

                    echo $post['title']. ' - Added';

                    // NEW BUSINESS ID
                    $post['id'] = sqlid();

                }

                ###############################
                # CATEGORIES / TAGS
                ###############################

                $create_relation = false;
                unset($ct_del);

                // GET CURRENT CATEGORY RELATIONS
                $rel_query = "SELECT `category_id` FROM `" .$_uccms_blog->tables['post_categories']. "` WHERE (`post_id`=" .$post['id']. ")";
                $rel_q = sqlquery($rel_query);
                while ($rel = sqlfetch($rel_q)) {
                    $ct_del['category'][$rel['category_id']] = $rel['category_id']; // SAVE TO DELETE ARRAY
                }

                // GET CURRENT TAG RELATIONS
                $rel_query = "SELECT `tag_id` FROM `" .$_uccms_blog->tables['post_tags']. "` WHERE (`post_id`=" .$post['id']. ")";
                $rel_q = sqlquery($rel_query);
                while ($rel = sqlfetch($rel_q)) {
                    $ct_del['tag'][$rel['tag_id']] = $rel['tag_id']; // SAVE TO DELETE ARRAY
                }

                // LOOP
                foreach ($category_children->category as $cti) {

                    // GET INFO
                    $ct_attributes  = $cti->attributes();
                    $ct_type        = $ct_attributes->domain;
                    $ct_slug        = $ct_attributes->nicename;

                    // CLEAN UP
                    $ct_title = trim(sqlescape((string)$cti));

                    // IS A VALUE
                    if ($ct_title) {

                        // TYPE
                        if ($ct_type == 'category') {
                            $type = 'category';
                            $type_plural = 'categories';
                        } else if ($ct_type == 'post_tag') {
                            $type = 'tag';
                            $type_plural = 'tags';
                        } else {
                            $type = '';
                        }

                        // HAVE TYPE
                        if ($type) {

                            // NOT IN ARRAY
                            if (!$cta[$type][$ct_title]) {

                                // LOOK FOR MATCH
                                $ct_query = "SELECT `id` FROM `" .$_uccms_blog->tables[$type_plural]. "` WHERE (`title`='" .$ct_title. "')";
                                $ct_q = sqlquery($ct_query);
                                $ct = sqlfetch($ct_q);

                                // ADD TO ARRAY
                                $cta[$type][$ct_title] = $ct['id'];

                            }

                            // FOUND
                            if ($cta[$type][$ct_title]) {

                                // RELATION ALREADY EXISTS
                                if ($ct_del[$type][$cta[$type][$ct_title]]) {
                                    unset($ct_del[$type][$cta[$type][$ct_title]]);

                                // CREATE RELATION
                                } else {
                                    $create_relation = true;
                                }

                            // NOT FOUND
                            } else {

                                if ($ct_slug) {
                                    $ct_slug = trim(sqlescape((string)$ct_slug));
                                } else {
                                    $ct_slug = $_uccms_blog->makeRewrite($ct_title);
                                }

                                // CREATE NEW
                                $newct_query = "INSERT INTO `" .$_uccms_blog->tables[$type_plural]. "` SET `title`='" .$ct_title. "', `slug`='" .$ct_slug. "'";
                                $newct_q = sqlquery($newct_query);

                                // NEW ID
                                $cta[$type][$ct_title] = sqlid();

                                $create_relation = true;

                            }

                            // CREATE RELATION
                            if (($create_relation) && ($cta[$type][$ct_title])) {

                                $rel_query = "INSERT INTO `" .$_uccms_blog->tables['post_' .$type_plural]. "` SET `" .$type. "_id`=" .$cta[$type][$ct_title]. ", `post_id`=" .$post['id'];
                                sqlquery($rel_query);

                            }

                            // HAVE ANY TO DELETE
                            if (count($ct_del) > 0) {
                                foreach ($ct_del as $ct_id) {
                                    $del_query = "DELETE FROM `" .$_uccms_blog->tables['post_' .$type_plural]. "` WHERE (`post_id`=" .$post['id']. ") AND (`" .$type. "_id`=" .$ct_id. ")";
                                    sqlquery($del_query);
                                }
                            }

                            unset($ct);

                        }

                    }

                }

            // QUERY FAILED
            } else {

                echo $post['title']. ' - Failed';

            }

            unset($post);

            echo ($cli ? "\n" : '<br />');

        }

    }

// NO XML
} else {
    echo 'Not valid XML.';
}

echo ($cli ? "\n" : '<br />');
echo '<b>All done.</b>';


##########################################################


// CONVERT LINE BREAKS TO PARAGRAPHS
function this_nl2p($string, $line_breaks=true, $xml=true) {
    $string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);
    if ($line_breaks == true) {
        return '<p>'.preg_replace(array("/([\n]{2,})/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'), trim($string)).'</p>';
    } else {
        return '<p>'.preg_replace(
            array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
            array("</p>\n<p>", "</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'),
            trim($string)
        ).'</p>';
    }
}

?>