<?php

//echo 'Memory Limit: ' .ini_get('memory_limit'). "\n";
/*
    - If limit is too low, edit /site/php.ini and set memory_limit = 512M
    - Running from command line: php -d memory_limit=512M manual.php
*/

##########################################################

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.blog/modules/blog/export/dam.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INIT CLASSES
$_uccms_blog    = new uccms_Blog;
$_dam           = new DigitalAssetManager();

// GET ALL BLOG POSTS
$posts_query = "SELECT * FROM `" .$_uccms_blog->tables['posts']. "`";
$posts_q = sqlquery($posts_query);

echo 'Business ID: ' .$_GLOB['REVSOCIAL']['business_id'];
echo ($cli ? "\n" : '<br />');
echo sqlrows($posts_q). ' posts found.';
echo ($cli ? "\n" : '<br />');

$num_success    = 0;
$num_failed     = 0;

$site = parse_url(WWW_ROOT);

// LOOP
while ($post = sqlfetch($posts_q)) {

    // HAVE CONTENT
    if ($post['content']) {

        // GET HTML FROM CONTENT
        $dom = new DOMDocument('1.0');
        @$dom->loadHTML(stripslashes($post['content']));
        $xpath = new DOMXpath($dom);

        //$html = iconv('UTF-8', 'UTF-8//IGNORE', $dom->saveHTML());

        // GET IMAGES
        $images = $xpath->query("//img[@src]");

        // HAVE IMAGES
        if ($images->length > 0) {

            // LOOP THROUGH IMAGES
            foreach ($images as $image) {

                // SOURCE
                $src = $image->getAttribute('src');

                // HAVE INITIAL SLASH
                if (substr($src, 0, 1) == '/') {

                    // REMOVE IT
                    $src = substr($src, 1);

                }

                // IS NOT FULL URL
                if (substr($src, 0, 4) != 'http') {

                    // PREPEND SITE URL
                    $src = WWW_ROOT . $src;

                }

                /*
                $isite = parse_url($src);

                if ($isite['host'] != $site['host']) {

                }
                */

                $alt = $image->getAttribute('alt');

                // DIGITAL ASSET MANAGER VARS
                $dam_vars = array(
                    'website_extension'         => $_uccms_blog->Extension,
                    'website_extension_item'    => 'post',
                    'website_extension_item_id' => $post['id'],
                    'image_url'                 => $src
                );

                if ($alt) {
                    $dam_vars['alt'] = stripslashes($alt);
                }

                // ADD MEDIA
                $dam_result = $_dam->addMedia($dam_vars);

                //echo print_r($dam_result);
                //exit;

                // SUCCESS
                if ($dam_result['success']) {

                    $num_success++;

                // FAILED
                } else {

                    $error      = print_r($dam_vars, true). "<br />" .print_r($dam_result, true). "<br /><br />";
                    $error_text = str_replace('<br />', "\n", $error);

                    echo ($cli ? $error_text : $error);

                    error_log($error_text);

                }

            }

        }

    }

    // GET IMAGES
    $images_query = "SELECT * FROM `" .$_uccms_blog->tables['post_images']. "` WHERE (`post_id`=" .$post['id']. ") ORDER BY `sort` ASC, `id` ASC";
    $images_q = sqlquery($images_query);
    while ($image = sqlfetch($images_q)) {

        // DIGITAL ASSET MANAGER VARS
        $dam_vars = array(
            'website_extension'         => $_uccms_blog->Extension,
            'website_extension_item'    => 'post',
            'website_extension_item_id' => $post['id'],
            'image_url'                 => $_uccms_blog->imageBridgeOut($image['image'], 'posts')
        );

        if ($image['caption']) {
            $dam_vars['caption'] = stripslashes($image['caption']);
        }

        // ADD MEDIA
        $dam_result = $_dam->addMedia($dam_vars);

        //echo print_r($dam_result);
        //exit;

        // SUCCESS
        if ($dam_result['success']) {

            $num_success++;

        // FAILED
        } else {

            $num_failed++;

            $error      = print_r($image, true). "<br />" .print_r($dam_vars, true). "<br />" .print_r($dam_result, true). "<br /><br />";
            $error_text = str_replace('<br />', "\n", $error);

            echo ($cli ? $error_text : $error);

            error_log($error_text);

        }

    }

}

//echo ($cli ? "\n" : '<br />');

echo ($cli ? "\n" : '<br />');
echo '<b>All done. (' .$num_success. ' Success. ' .$num_failed. ' Failed.)</b>';

?>