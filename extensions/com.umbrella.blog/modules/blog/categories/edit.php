<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET CATEGORY INFO
    $category_query = "SELECT * FROM `" .$_uccms_blog->tables['categories']. "` WHERE (`id`=" .$id. ")";
    $category_q = sqlquery($category_query);
    $category = sqlfetch($category_q);

}

?>

<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

    #categories .category_title {
        width: 530px;
    }
    #categories .category_posts {
        width: 100px;
    }
    #categories .category_status {
        width: 100px;
    }
    #categories .category_edit {
        width: 55px;
    }
    #categories .category_delete {
        width: 55px;
    }

</style>

<div class="container legacy">

    <form enctype="multipart/form-data" action="../process/" method="post">
    <input type="hidden" name="category[id]" value="<?php echo $category['id']; ?>" />

    <header>
        <h2><?php if ($category['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Category</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Title</label>
                    <input type="text" name="category[title]" value="<?php echo stripslashes($category['title']); ?>" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="category[active]" value="1" <?php if ($category['active']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

            </div>

            <div class="right">

            </div>

        </div>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="category[slug]" value="<?php echo stripslashes($category['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="category[meta_title]" value="<?php echo stripslashes($category['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="category[meta_description]" style="height: 64px;"><?php echo stripslashes($category['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="category[meta_keywords]" style="height: 64px;"><?php echo stripslashes($category['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>