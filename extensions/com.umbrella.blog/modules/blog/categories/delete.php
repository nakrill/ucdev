<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_blog->tables['post_categories']. "` WHERE (`category_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_blog->tables['categories']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Category', 'Category deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Category', 'Failed to delete category.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Category', 'No category specified.');
}

BigTree::redirect(MODULE_ROOT.'categories/');

?>