<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['category']['id'];

    // USE CATEGORY TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['category']['slug']) {
        $_POST['category']['slug'] = $_POST['category']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_blog->makeRewrite($_POST['category']['slug']),
        'active'            => (int)$_POST['category']['active'],
        'title'             => $_POST['category']['title'],
        'meta_title'        => $_POST['category']['meta_title'],
        'meta_description'  => $_POST['category']['meta_description'],
        'meta_keywords'     => $_POST['category']['meta_keywords']
    );

    // HAVE CATEGORY ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_blog->tables['categories']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO CATEGORY ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_blog->tables['categories']. "` SET " .uccms_createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Category', 'Category added!');
        } else {
            $admin->growl('Category', 'Category updated!');
        }

    // QUERY FAILED
    } else {
        $admin->growl('Category', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'categories/edit/?id=' .$id);

?>