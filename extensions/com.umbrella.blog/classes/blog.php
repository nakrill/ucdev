<?
    class uccms_Blog extends BigTreeModule {

        var $Table = "";

        var $Extension = "com.umbrella.blog";

        // DB TABLES
        var $tables = array(
            'categories'            => 'uccms_blog_categories',
            'post_categories'       => 'uccms_blog_post_category_rel',
            'post_images'           => 'uccms_blog_post_images',
            'post_tags'             => 'uccms_blog_post_tag_rel',
            'posts'                 => 'uccms_blog_posts',
            'settings'              => 'uccms_blog_settings',
            'tags'                  => 'uccms_blog_tags'
        );

        // POST STATUSES
        var $statuses = array(
            0 => 'Draft',
            1 => 'Published',
            2 => 'Scheduled',
            9 => 'Deleted'
        );

        var $settings, $misc;

        // START AS 0, WILL ADJUST LATER
        static $PerPage = 0;


        function __construct() {

        }


        #############################
        # GENERAL - MISC
        #############################


        // FRONTEND PATH
        public function frontendPath() {
            if (!isset($this->settings['frontend_path'])) {
                $this->settings['frontend_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*blog')"))['path']);
            }
            if (!$this->settings['frontend_path']) $this->settings['frontend_path'] = 'blog';
            return $this->settings['frontend_path'];
        }


        // MODULE ID
        public function moduleID() {
            if (!isset($this->settings['module_id'])) {
                $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
            }
            return $this->settings['module_id'];
        }


        // ADMIN USER ID
        public function adminID() {
            return $_SESSION['bigtree_admin']['id'];
        }


        // ADMIN ACCESS LEVEL
        public function adminModulePermission() {
            global $admin;
            if (!isset($this->settings['admin_module_permission'])) {
                $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
            }
            return $this->settings['admin_module_permission'];
        }


        // STRING IS JSON
        public function isJson($string) {
            if (substr($string, 0, 2) == '{"') {
                return true;
            }
        }


        // CONVERTS A STRING INTO A REWRITE-ABLE URL
        public function makeRewrite($string) {

            $string = strtolower(trim($string));

            // REMOVE ALL QUOTES
            $string = str_replace("'", '', $string);
            $string = str_replace('"', '', $string);

            // STRIP ALL NON WORD CHARS
            $string = preg_replace('/\W/', ' ', $string);

            // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
            $string = preg_replace('/\ +/', '-', $string);

            // TRIM DASHES
            $string = preg_replace('/\-$/', '', $string);
            $string = preg_replace('/^\-/', '', $string);

            return $string;

        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
        public function imageBridgeIn($string) {
            $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $dpa = explode('/', $string);
                return array_pop($dpa);
                //$what = array_pop($dpa);
            }
        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
        public function imageBridgeOut($string, $what, $file=false) {
            $string = stripslashes($string);
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $base = ($file ? SITE_ROOT : STATIC_ROOT);
                return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
            }
        }


        // CONVERT BIGTREE FILE LOCATION TO FULL URL
        public function bigtreeFileURL($file) {
            return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
        }


        // SET NUMBER PER PAGE
        public function setPerPage($num) {
            static::$PerPage = (int)$num;
        }


        // NUMBER PER PAGE
        public function perPage() {
            global $cms;
            if (!static::$PerPage) {
                $pp = $this->getSetting('posts_per_page');
                if ($pp) {
                    $this->setPerPage($pp);
                }
            }
            if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
            if (!static::$PerPage) $this->setPerPage(15);
            return static::$PerPage;
        }


        // GET NUMBER OF PAGES FROM QUERY
        public function pageCount($num) {
            return ceil($num / $this->perPage());
        }


        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            );
        }


        // TRIM URL
        public function trimURL($string) {
            return trim(preg_replace('#^https?://#', '', $string));
        }


        // CONVERT ARRAY TO CSV
        public function arrayToCSV($array=array()) {
            if (is_array($array)) {
                return implode(',', $array);
            }
        }


        #############################
        # GENERAL - SETTINGS
        #############################


        // GET SETTING
        public function getSetting($id='') {
            $id = sqlescape($id);
            if ($id) {
                if (isset($this->settings[$id])) {
                    return $this->settings[$id];
                } else {
                    $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tables['settings']. "` WHERE `id`='" .$id. "'"));
                    if ($this->isJson($f['value'])) {
                        $val = json_decode($f['value'], true);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$id] = $val;
                }
                return $val;
            }
        }


        // GET SETTINGS
        public function getSettings($array='') {
            $out = array();
            if (is_array($array)) {
                $oa = array();
                foreach ($array as $id) {
                    $id = sqlescape($id);
                    if ($id) {
                        $oa[] = "`id`='" .$id. "'";
                    }
                }
                if (count($oa) > 0) {
                    $or = "(" .implode(") OR (", $oa). ")";
                }
                if ($or) {
                    $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` WHERE " .$or;
                }
            } else {
                $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` ORDER BY `id` ASC";
            }
            if ($query) {
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    if ($this->isJson($string)) {
                        $val = json_decode($f['value']);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$f['id']] = $val;
                    $out[$f['id']] = $val;
                }
            }
            return $out;
        }


        // SET SETTING
        public function setSetting($id='', $value='') {

            // CLEAN UP
            $id = sqlescape($id);

            // HAVE ID
            if ($id) {

                // VALUE IS ARRAY
                if (is_array($value)) {
                    $value = json_encode($value);

                // VALUE IS STRING
                } else {
                    $value = sqlescape($value);
                }

                $query = "
                INSERT INTO `" .$this->tables['settings']. "`
                SET `id`='" .$id. "', `value`='" .$value. "'
                ON DUPLICATE KEY UPDATE `value`='" .$value. "'
                ";
                if (sqlquery($query)) {
                    return true;
                }

            }

        }


        // SIDEBAR WIDGETS
        public function sidebarWidgets() {
            return array(
                'search' => array(
                    'name'      => 'Search',
                    'heading'   => 'Search'
                ),
                'latest_posts' => array(
                    'name'      => 'Latest Posts',
                    'heading'   => 'Most Recent'
                ),
                'categories' => array(
                    'name'      => 'Categories',
                    'heading'   => 'Categories'
                ),
                'tags' => array(
                    'name'      => 'Tags',
                    'heading'   => 'Tags'
                ),
                'facebook' => array(
                    'name'      => 'Facebook',
                    'heading'   => 'Facebook'
                ),
                'twitter' => array(
                    'name'      => 'Twitter',
                    'heading'   => 'Twitter'
                ),
                'html1' => array(
                    'name'      => 'Content',
                    'heading'   => 'Content'
                ),
                'text1' => array(
                    'name'      => 'Text',
                    'heading'   => 'Text'
                )
            );
        }


        // GET SIDEBAR WIDGET DATA
        public function sidebarWidgetData() {
            $sidebar_widgets = $this->getSetting('sidebar_widgets');
            if ($sidebar_widgets) {
                if (!is_array($sidebar_widgets)) {
                    $sidebar_widgets = json_decode(stripslashes($sidebar_widgets), true);
                }
                return $sidebar_widgets;
            }
            return array();
        }


        // PINGBACK SERVICES
        public function pingbackServices() {
            return array(
                'http://rpc.pingomatic.com',
                'http://rpc.twingly.com',
                'http://www.pingmyblog.com'
            );
        }


        #############################
        # GENERAL - CATEGORIES
        #############################


        public function getCategoryTree($parent=0, $active='', $depth=100) {

            $out = array();

            // CLEAN UP
            $parent = (int)$parent;

            if ($active === true) {
                $active_sql = "WHERE (active=1)";
            } else if ($active === false) {
                $active_sql = "WHERE (active=0)";
            } else {
                $active_sql = "";
            }

            // GET CATEGORIES
            $query = "SELECT * FROM `" .$this->tables['categories']. "` " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($item = sqlfetch($q)) {
                $ref = & $refs[$item['id']];
                foreach ($item as $name => $value) {
                    $ref[$name] = $value;
                }
                if ($item['parent'] == 0) {
                    $out[$item['id']] = & $ref;
                } else {
                    $refs[$item['parent']]['children'][$item['id']] = & $ref;
                }
            }

            unset($refs);

            // PARENT SPECIFIED
            if ($parent) {
                return $this->findArrayKey($out, $parent);
            // NO PARENT SPECIFIED
            } else {
                return $out;
            }

            return $out;

        }


        // OUTPUT THE CATEGORY TREE IN A DROPDOWN (TREE ARRAY, SELECTED ID,
        function printCategoryDropdown($tree, $selected=0, $id=0, $parent=0, $r=0) {
            foreach ($tree as $i => $t) {
                $dash = ($t['parent'] == 0) ? '' : str_repeat('-', $r) .' ';
                $sel = ($t['id'] == $selected ? ' selected="selected"' : '');
                $dis = ($t['id'] == $id ? ' disabled="disabled"' : '');
                echo "\t" . '<option value="' .$t['id']. '"' .$sel . $dis. '>' .$dash . stripslashes($t['title']). '</option>' . "\n";
                if (isset($t['children'])) {
                    $this->printCategoryDropdown($t['children'], $selected, $id, $t['parent'], $r+1);
                }
            }
        }


        // GET A SUB-ARRAY BY MATCHING KEY
        public function findArrayKey($array, $index) {
            if (!is_array($array)) return null;
            if (isset($array[$index])) return $array[$index];
            foreach ($array as $item) {
                $return = $this->findArrayKey($item, $index);
                if (!is_null($return)) {
                    return $return;
                }
            }
            return null;
        }


        // GET THE PARENT(S) OF A CATEGORY
        public function getCategoryParents($id=0) {
            $out = array();
            $id = (int)$id;
            if ($id > 0) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .$id. ")";
                $cat = sqlfetch(sqlquery($query));
                array_push($out, $cat);
                if ($cat['parent']) {
                    $out = array_merge($out, $this->getCategoryParents($cat['parent']));
                }
            }
            return $out;
        }


        // GET SUB-CATEGORIES
        public function subCategories($id, $active=null, $by_pricegroup=false) {
            $out = array();

            if ($active === true) {
                $active_sql = " AND (`active`=1)";
            } else if ($active === false) {
                $active_sql = " AND (`active`=0)";
            } else {
                $active_sql = "";
            }

            // GET SUB CATEGORIES FROM DB
            $sub_query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`parent`=" .(int)$id. ") " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $sub_q = sqlquery($sub_query);
            while ($sub = sqlfetch($sub_q)) {
                $out[] = $sub;
            }

            return $out;

        }


        /*
        // GET POSTS IN CATEGORY WITH PAGING
        public function categoryPosts($id, $vars=array()) {
            global $_paging;

            $out = array();

            if ($id) {

                $sql_sort = "b.name ASC, b.id ASC";

                // HOW MANY LISTINGS PER PAGE
                if ($vars['limit']) $limit = (int)$vars['limit']; // SPECIFIED DIRECTLY
                if (!$limit) $limit = $this->getSetting('listings_per_page'); // USE EXTENSION SETTING
                if (!$limit) $limit = $this->perPage(); // USE BIGTREE / DEFAULT

                // CURRENT PAGE
                $page = isset($vars['page']) ? intval($vars['page']) : 1;

                // GET BUSINESSES
                $business_query = "
                SELECT b.*
                FROM `" .$this->tables['businesses']. "` AS `b`
                INNER JOIN `" .$this->tables['business_categories']. "` AS `bc` ON b.id=bc.business_id
                WHERE (bc.category_id=" .$id. ") AND (b.status=1)
                ORDER BY " .$sql_sort. "
                LIMIT " .(($page - 1) * $limit). "," .$limit. "
                ";
                $business_q = sqlquery($business_query);
                while ($business = sqlfetch($business_q)) {
                    $out[] = $business;
                }

                // HAVE PAGING CLASS
                if ($_paging) {

                    // PREPARE PAGING (RESULTS PER PAGE, NUMBER OF PAGE NUMBERS, CURRENT PAGE, REQUEST VARIABLES (_GET, _POST), USE MOD_REWRITE)
                    $_paging->prepare($limit, 5, $page, $_GET, false);

                }

            }

            return $out;

        }


        // GET TOTAL POSTS IN CATEGORY
        public function categoryPostsTotal($id, $vars=array()) {

            $count = 0;

            if ($id) {

                // GET BUSINESSES
                $business_query = "
                SELECT i.*
                FROM `" .$this->tables['posts']. "` AS `b`
                INNER JOIN `" .$this->tables['post_categories']. "` AS `bc` ON b.id=bc.business_id
                WHERE (bc.category_id=" .$id. ") AND (b.status=1)
                ";
                $business_q = sqlquery($business_query);
                $count = sqlrows($business_q);

            }

            return $count;

        }
        */


        // GENERATE CATEGORY URL
        public function categoryURL($id, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/category/' .trim($slug). '-' .$id. '/';
        }


        // BREADCRUMB OUTPUT
        public function generateBreadcrumbs($array, $options=array()) {

            // OPTIONS
            if (!$options['base']) $options['base'] = './';
            if (!$options['home_title']) $options['home_title'] = 'Blog'; // MATTD: Make into a configurable setting
            if (!$options['sep']) $options['sep'] = ' / '; // MATTD: Make into a configurable setting

            // ELEMENTS
            $els = array();

            // SHOW HOME LINK
            if (!$options['no_home']) {
                $els[] = '<a href="' .$options['base']. '">' .$options['home_title']. '</a>';
            }

            // NUMBER OF PARENT CATEGORIES
            $num_cat = count($array);

            // HAVE PARENT CATEGORIES
            if ($num_cat > 0) {

                // REVERSE ARRAY FOR EASIER OUTPUT
                $tcat = array_reverse($array);

                $i = 1;

                // LOOP
                foreach ($tcat as $cat) {
                    if ($options['rewrite']) {
                        $url = $this->categoryURL($cat['id'], $cat);
                    } else {
                        $url = $options['base']. '?id=' .$cat['id'];
                    }
                    $line = '';
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '<a href="' .$url. '">';
                    }
                    $line .= stripslashes($cat['title']);
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '</a>';
                    }
                    $els[] = $line;
                    $i++;
                }

            }

            echo implode($options['sep'], $els);

        }


        #############################
        # TAG
        #############################


        // GENERATE TAG URL
        public function tagURL($id, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['tags']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/tag/' .trim($slug). '-' .$id. '/';
        }


        #############################
        # POST
        #############################


        // GENERATE POST URL
        public function postURL($id, $category_id=0, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['posts']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            /*
            $category_slug = $data['category_slug']. '-' .$category_id;
            if (!$data['category_slug']) {
                if ($category_id > 0) {
                    $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$id. ")";
                    $data = sqlfetch(sqlquery($query));
                    if ($data['slug']) {
                        $category_slug = stripslashes($data['slug']). '-' .$data['id'];
                    } else {
                        $category_slug = $this->makeRewrite($data['title']). '-' .$id;
                    }
                } else {
                    $category_slug = 'post';
                }
            }
            return WWW_ROOT . $bigtree['path'][0]. '/' .trim($category_slug). '/' .trim($slug). '-' .$id. '/';
            */
            return '/' .$this->frontendPath(). '/post/' .trim($slug). '-' .$id. '/';
        }


        // ADD / UPDATE POST
        public function post_addUpdate($data) {

            // CLEAN UP
            $id = (int)$data['post']['id'];

            // HAVE ID
            if ($id) {

                // GET CURRENT INFO
                $post_query = "SELECT * FROM `" .$this->tables['posts']. "` WHERE (`id`=" .$id. ")";
                $post_q = sqlquery($post_query);
                $post = sqlfetch($post_q);

                // REVSOCIAL INFO
                if ($post['revsocial']) {
                    $post['revsocial'] = json_decode(stripslashes($post['revsocial']), true);
                }

            }

            $published = false;

            if (!is_array($post['revsocial'])) $post['revsocial'] = array();

            // USE post TITLE FOR SLUG IF NOT SPECIFIED
            if (!$data['post']['slug']) {
                $data['post']['slug'] = $data['post']['title'];
            }

            // PUBLISH DATE & TIME
            if ($data['post']['dt_publish']) {
                $dt_publish = date('Y-m-d H:i:s', strtotime($data['post']['dt_publish']));
            } else {
                $dt_publish = '0000-00-00 00:00:00';
            }

            // AUTHOR
            $author_id = (int)$data['post']['author'];
            if (!$author_id) $author_id = $this->adminID();

            // REVSOCIAL INFO
            if (count($data['revsocial']['streams']) > 0) {
                $post['revsocial']['streams'] = $data['revsocial']['streams'];
            }

            // DB COLUMNS
            $columns = array(
                'slug'                      => $this->makeRewrite($data['post']['slug']),
                'dt_publish'                => $dt_publish,
                'title'                     => $data['post']['title'],
                'content'                   => $data['post']['content'],
                'author_id'                 => $author_id,
                'business_id'               => (int)$data['post']['business_id'],
                'meta_title'                => $data['post']['meta_title'],
                'meta_description'          => $data['post']['meta_description'],
                'meta_keywords'             => $data['post']['meta_keywords'],
                'revsocial'                 => json_encode($post['revsocial']),
                'dt_saved'                  => date('Y-m-d H:i:s')
            );

            // STATUS
            if ($data['publish']) {

                // PUBLISH DATE NOT SPECIFIED OR IS BEFORE NOW
                if ((!$data['post']['dt_publish']) || (strtotime($data['post']['dt_publish']) <= time())) {
                    $columns['status'] = 1;
                    $columns['dt_publish'] = '0000-00-00 00:00:00';
                    $columns['dt_published'] = date('Y-m-d H:i:s', ($data['post']['dt_publish'] ? strtotime($data['post']['dt_publish']) : time()));

                // PUBLISH IN THE FUTURE
                } else if (strtotime($data['post']['dt_publish']) > time()) {
                    $columns['status'] = 2;
                    $columns['dt_published'] = '0000-00-00 00:00:00';
                }

            } else {
                $columns['status'] = 0;
            }

            // HAVE POST ID - IS UPDATING
            if ($id) {

                // DB QUERY
                $query = "UPDATE `" .$this->tables['posts']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

            // NO ITEM ID - IS NEW
            } else {

                // DB QUERY
                $query = "INSERT INTO `" .$this->tables['posts']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW()";

            }

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {

                // NO ID (WAS NEW)
                if (!$id) {
                    $id = sqlid();
                }

                if ($data['publish']) {

                    $out['success'] = true;

                    // POST PUBLISHED
                    if ($columns['status'] == 1) {

                        $out['message'] = 'Post published!';

                        // CLEAR PUBLISH DATE
                        $pub_query = "UPDATE `" .$this->tables['posts']. "` SET `dt_publish`='0000-00-00 00:00:00' WHERE (`id`=" .$id. ")";
                        sqlquery($pub_query);

                        // PINGBACKS ENABLED
                        if ($this->getSetting('post_ping_enabled')) {

                            // GENERATE POST'S URL
                            $data['post']['url'] = WWW_ROOT . substr($this->postURL($id), 1);

                            $pingback_count = 0;

                            // SEND TO PING SERVICES
                            $pingback_result = $this->pingbackSend($data);

                            // HAVE PINGBACK RESULTS
                            if (count($pingback_result) > 0) {
                                foreach ($pingback_result as $pbr) {
                                    if ($pbr['status'] == 'ok') {
                                        $pingback_count++;
                                    } else {
                                        $pingback_errors[] = '<br />' .$pbr['url']. ' - Failed.';
                                    }
                                }
                            }

                            $out['message'] = 'Post published. ' .$pingback_count. ' pingback services notified.';

                            // HAVE PINGBACK ERRORS - ADD TO MESSAGE
                            if (count($pingback_errors) > 0) {
                                foreach ($pingback_errors as $pb_error) {
                                    $out['message'] .= $pb_error;
                                }
                            }

                        }

                    // POST SCHEDULED
                    } else if ($columns['status'] == 2) {
                        $out['message'] = 'Post scheduled.';
                    }

                } else {
                    $out['success'] = true;
                    $out['message'] = 'Draft saved!';
                }

                ###########################
                # CATEGORIES
                ###########################

                // HAVE CATEGORIES
                if (count($data['category']) > 0) {

                    // NEW CATEGORIES
                    $ncata = $data['category']['new'];
                    unset($data['category']['new']);

                    // HAVE NEW CATEGORIES
                    if (count($ncata) > 0) {
                        foreach ($ncata as $nid) {

                            unset($title);
                            unset($new_cat_id);

                            // TITLE
                            $title = $data['new_category_title'][$nid];

                            // HAVE TITLE
                            if ($title) {

                                $columns = array(
                                    'slug'      => $this->makeRewrite($title),
                                    'active'    => 1,
                                    'title'     => $title
                                );

                                $cat_query = "INSERT INTO `" .$this->tables['categories']. "` SET " .uccms_createSet($columns);
                                sqlquery($cat_query);

                                $new_cat_id = sqlid();

                                if ($new_cat_id) {
                                    $data['category'][$new_cat_id] = 1;
                                }

                            }
                        }
                    }

                    $cata = array();

                    // GET CURRENT RELATIONS
                    $query = "SELECT `category_id` FROM `" .$this->tables['post_categories']. "` WHERE (`post_id`=" .$id. ")";
                    $q = sqlquery($query);
                    while ($row = sqlfetch($q)) {
                        $cata[$row['category_id']] = $row['category_id'];
                    }

                    // LOOP
                    foreach ($data['category'] as $category_id => $selected) {

                        // CLEAN UP
                        $category_id = (int)$category_id;

                        // HAVE CATEGORY ID
                        if ($category_id) {

                            // NOT IN EXISTING CATEGORY RELATIONS
                            if (!$cata[$category_id]) {

                                // DB COLUMNS
                                $columns = array(
                                    'post_id'       => $id,
                                    'category_id'   => $category_id
                                );

                                // ADD RELATIONSHIP TO DB
                                $query = "INSERT INTO `" .$this->tables['post_categories']. "` SET " .uccms_createSet($columns). "";
                                sqlquery($query);

                            }

                            // REMOVE FROM RELATIONS
                            unset($cata[$category_id]);

                        }

                    }

                    // HAVE LEFT OVER (OLD) RELATIONS
                    if (count($cata) > 0) {

                        // LOOP
                        foreach ($cata as $category_id) {

                            // REMOVE RELATIONSHIP FROM DB
                            $query = "DELETE FROM `" .$this->tables['post_categories']. "` WHERE (`post_id`=" .$id. ") AND (`category_id`=" .$category_id. ")";
                            sqlquery($query);

                        }

                    }

                // NO CATEGORIES
                } else {
                    $cat_query = "DELETE FROM `" .$this->tables['post_categories']. "` WHERE (`post_id`=" .$id. ")";
                    sqlquery($cat_query);
                }

                ###########################
                # TAGS
                ###########################

                // HAVE TAGS
                if (count($data['tag']) > 0) {

                    // NEW TAGS
                    $ntaga = $data['tag']['new'];
                    unset($data['tag']['new']);

                    // HAVE NEW TAGS
                    if (count($ntaga) > 0) {
                        foreach ($ntaga as $nid) {

                            unset($title);
                            unset($new_tag_id);

                            // TITLE
                            $title = $data['new_tag_title'][$nid];

                            // HAVE TITLE
                            if ($title) {

                                $columns = array(
                                    'slug'      => $this->makeRewrite($title),
                                    'active'    => 1,
                                    'title'     => $title
                                );

                                $cat_query = "INSERT INTO `" .$this->tables['tags']. "` SET " .uccms_createSet($columns);
                                sqlquery($cat_query);

                                $new_tag_id = sqlid();

                                if ($new_tag_id) {
                                    $data['tag'][$new_tag_id] = 1;
                                }

                            }
                        }
                    }

                    $taga = array();

                    // GET CURRENT RELATIONS
                    $query = "SELECT `tag_id` FROM `" .$this->tables['post_tags']. "` WHERE (`post_id`=" .$id. ")";
                    $q = sqlquery($query);
                    while ($row = sqlfetch($q)) {
                        $taga[$row['tag_id']] = $row['tag_id'];
                    }

                    // LOOP
                    foreach ($data['tag'] as $tag_id => $selected) {

                        // CLEAN UP
                        $tag_id = (int)$tag_id;

                        // HAVE TAG ID
                        if ($tag_id) {

                            // NOT IN EXISTING tag RELATIONS
                            if (!$taga[$tag_id]) {

                                // DB COLUMNS
                                $columns = array(
                                    'post_id'  => $id,
                                    'tag_id'   => $tag_id
                                );

                                // ADD RELATIONSHIP TO DB
                                $query = "INSERT INTO `" .$this->tables['post_tags']. "` SET " .uccms_createSet($columns). "";
                                sqlquery($query);

                            }

                            // REMOVE FROM RELATIONS
                            unset($taga[$tag_id]);

                        }

                    }

                    // HAVE LEFT OVER (OLD) RELATIONS
                    if (count($taga) > 0) {

                        // LOOP
                        foreach ($taga as $tag_id) {

                            // REMOVE RELATIONSHIP FROM DB
                            $query = "DELETE FROM `" .$this->tables['post_tags']. "` WHERE (`post_id`=" .$id. ") AND (`tag_id`=" .$tag_id. ")";
                            sqlquery($query);

                        }

                    }

                // NO TAGS
                } else {
                    $cat_query = "DELETE FROM `" .$this->tables['post_tags']. "` WHERE (`post_id`=" .$id. ")";
                    sqlquery($cat_query);
                }

                ##########################
                # REVSOCIAL
                ##########################

                // POST PUBLISHED
                if ($columns['status'] == 1) {

                    // POSTING TO REVSOCIAL
                    if (count($data['revsocial']['streams']) > 0) {

                        /*
                        // MESSAGE
                        $message = str_replace(
                            array(
                                '<p>[gallery]</p>',
                                '[gallery]'
                            ),
                            array(
                                '',
                                ''
                            ),
                            strip_tags($data['post']['content'])
                        );
                        */

                        // POST INFO
                        $post_data = array(
                            'post' => array(
                                'id'                    => $post['revsocial']['id'],
                                'streams'               => array_keys($data['revsocial']['streams']),
                                'message'               => $data['post']['title'],
                                'link'                  => substr(WWW_ROOT, 0, -1) . $this->postURL($id, $post),
                                //'schedule'            => '2015-11-24 14:00:00', // don't specify to post now
                                //'campaign'            => 'new',
                                //'new_campaign'        => 'Test New Campaign 1',
                                //'dont_link_shorten'   => false
                            )
                        );

                        // IS PUBLISHING
                        if ($data['publish']) {
                            $post_data['post']['draft'] = false;
                        } else {
                            $post_data['post']['draft'] = true;
                        }

                        // GET IMAGE
                        $image_query = "SELECT * FROM `" .$this->tables['post_images']. "` WHERE (`post_id`=" .$id. ") ORDER BY `sort` ASC, `id` ASC LIMIT 1";
                        $image_q = sqlquery($image_query);
                        $image = sqlfetch($image_q);

                        // HAVE IMAGE
                        if ($image['id']) {
                            $post_data['post']['image_url'] = $this->imageBridgeOut($image['image'], 'posts');
                        }

                        //echo print_r($post_data);
                        //exit;

                        // POST TO REVSOCIAL
                        $rs_result = $this->revsocial_postAdd($post_data);

                        // API RESPONSE
                        $rs_resp = $rs_result->decode_response();

                        //echo print_r($rs_result);
                        //exit;

                        if ($rs_resp->id) {

                            // RECORD POST ID FROM REVSOCIAL
                            $post['revsocial']['id'] = $rs_resp->id;

                            // WAS PUBLISHED
                            if ($rs_resp->published) {
                                $post['revsocial']['published'] = true;
                            }

                            // DB COLUMNS
                            $columns = array(
                                'revsocial' => json_encode($post['revsocial'])
                            );

                            // UPDATE POST
                            $query = "UPDATE `" .$this->tables['posts']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";
                            sqlquery($query);

                        }

                    }

                }

            // QUERY FAILED
            } else {
                if ($data['publish']) {
                    $out['message'] = 'Failed to publish.';
                } else {
                    $out['message'] = 'Failed to save draft.';
                }
            }

            $out['id'] = $id;

            return $out;

        }

        // GET POSTS
        public function getPosts($vars=array()) {

            $out = array();

            // CLEAN UP
            $page           = isset($vars['page']) ? intval($_GET['page']) : 1;
            $query          = trim($vars['query']);
            $category_id    = (int)$vars['category_id'];
            $tag_id         = (int)$vars['tag_id'];
            $author_id      = (int)$vars['author_id'];
            $limit          = ($vars['limit'] ? (int)$vars['limit'] : $this->perPage());

            // WHERE ARRAY
            $wa[] = "p.status=1";

            // CATEGORY SPECIFIED
            if ($category_id > 0) {
                $inner_join_cat = "INNER JOIN `" .$this->tables['post_categories']. "` AS `pc` ON p.id=pc.post_id";
                $wa[] = "pc.category_id=" .$category_id;
            }

            // TAG SPECIFIED
            if ($tag_id > 0) {
                $inner_join_tag = "INNER JOIN `" .$this->tables['post_tags']. "` AS `pt` ON p.id=pt.post_id";
                $wa[] = "pt.tag_id=" .$tag_id;
            }

            // AUTHOR SPECIFIED
            if ($author_id > 0) {
                $wa[] = "p.author_id=" .$author_id;
            }

            // IS SEARCHING
            if ($query) {
                $qparts = explode(" ", $query);
                $twa = array();
                foreach ($qparts as $part) {
                    $part = sqlescape(strtolower($part));
                    $twa[] = "(LOWER(p.title) LIKE '%$part%') OR (LOWER(p.content) LIKE '%$part%')";
                }
                $wa[] = "(" .implode(" OR ", $twa). ")";
            }

            // GET PAGED POSTS
            $post_query = "
            SELECT p.*
            FROM `" .$this->tables['posts']. "` AS `p`
            " .$inner_join_cat. " " .$inner_join_tag. "
            WHERE (" .implode(") AND (", $wa). ")
            ORDER BY p.dt_published DESC, p.id DESC
            LIMIT " .(($page - 1) * $limit). "," .$limit;
            $post_q = sqlquery($post_query);

            // NUMBER OF PAGED POSTS
            $num_posts = sqlrows($post_q);

            // TOTAL NUMBER OF POSTS
            $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$this->tables['posts']. "` AS `p` " .$inner_join_cat. " " .$inner_join_tag. " WHERE (" .implode(") AND (", $wa). ")"));

            // HAVE POSTS
            if ($num_posts > 0) {

                // PAGING CLASS
                require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

                // INIT PAGING CLASS
                $_paging = new paging();

                // OPTIONAL - URL VARIABLES TO IGNORE
                $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

                // PREPARE PAGING
                $_paging->prepare($this->perPage(), 8, $page, $_GET, false);

                // GENERATE PAGING
                $out['pages'] = $_paging->output($num_posts, $total_results['total']);

                // AUTHORS ARRAY
                $autha = array();

                // LOOP
                while ($post = sqlfetch($post_q)) {
                    $out['posts'][] = $this->getPostInfo($post['id'], $post);
                }

            }

            return $out;

        }


        // GET POST INFO
        public function getPostInfo($id=0, $post=array(), $status=1) {

            $out = array();

            $id         = (int)$id;
            $status     = (int)$status;

            // NO POST DATA AND HAVE ID
            if ((!$post['id']) && ($id > 0)) {

                // INIT ADMIN
                $admin = new BigTreeAdmin;

                // IS ADMIN
                if ($admin->Level) {
                    $post_query = "SELECT * FROM `" .$this->tables['posts']. "` WHERE (`id`=" .$id. ")";
                } else {
                    $post_query = "SELECT * FROM `" .$this->tables['posts']. "` WHERE (`id`=" .$id. ") AND (`status`=" .$status. ")";
                }

                // GET POST
                $post_q = sqlquery($post_query);
                $post = sqlfetch($post_q);

                // NOT PUBLISHED YET - USE SAVED DATE
                if ((!$post['dt_published']) || ($post['dt_published'] == '0000-00-00 00:00:00')) {
                    $post['dt_published'] = $post['dt_saved'];
                }

            }

            // HAVE POST ID
            if ($post['id']) {

                // HAVE AUTHOR ID
                if ($post['author_id'] > 0) {
                    $post['author'] = $this->getAuthorInfo($post['author_id']);
                }

                // GET IMAGES
                $image_query = "SELECT * FROM `" .$this->tables['post_images']. "` WHERE (`post_id`=" .$post['id']. ") ORDER BY `sort` ASC, `id` ASC";
                $image_q = sqlquery($image_query);
                while ($image = sqlfetch($image_q)) {
                    $post['images'][] = $image;
                }

                // GET CATEGORIES
                $category_query = "
                SELECT c.*
                FROM `" .$this->tables['post_categories']. "` AS `pc`
                INNER JOIN `" .$this->tables['categories']. "` AS `c` ON pc.category_id=c.id
                WHERE (pc.post_id=" .$post['id']. ") AND (c.active=1)
                ORDER BY c.title ASC, c.id ASC
                ";
                $category_q = sqlquery($category_query);
                while ($category = sqlfetch($category_q)) {
                    $post['categories'][] = $category;
                }

                // GET TAGS
                $tag_query = "
                SELECT t.*
                FROM `" .$this->tables['post_tags']. "` AS `pt`
                INNER JOIN `" .$this->tables['tags']. "` AS `t` ON pt.tag_id=t.id
                WHERE (pt.post_id=" .$post['id']. ") AND (t.active=1)
                ORDER BY t.title ASC, t.id ASC
                ";
                $tag_q = sqlquery($tag_query);
                while ($tag = sqlfetch($tag_q)) {
                    $post['tags'][] = $tag;
                }

                $out = $post;
            }

            return $out;

        }


        // GET AUTHOR INFO
        public function getAuthorInfo($id=0) {

            $out = array();

            $id = (int)$id;

            // HAVE ID
            if ($id) {

                // GET AUTHOR INFO
                $author_query = "
                SELECT `id`, `email`, `name`, `company`
                FROM `bigtree_users` AS `bu`
                WHERE (bu.id=" .$id. ")
                LIMIT 1
                ";
                $author_q = sqlquery($author_query);
                $out = sqlfetch($author_q);

            }

            return $out;

        }


        #############################
        # REVSOCIAL
        #############################


        // SAVE POST TO REVSOCIAL
        public function revsocial_postAdd($data) {
            global $cms;

            // REVSOCIAL API KEY
            $api_key = $cms->getSetting('rs_api-key');

            // HAVE API KEY
            if ($api_key) {

                // REST CLIENT
                require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

                // INIT REST API CLASS
                $_api = new RestClient(array(
                    'base_url' => 'https://api.revsocial.com/v1'
                ));

                // MAKE API CALL
                $result = $_api->post('post/add', array(
                    'api_key'   => $api_key,
                    'data'      => $data
                ));

            }

            return $result;

        }


        #############################
        # 3RD PARTY
        #############################


        // SEND TO PINGBACK SERVICES
        public function pingbackSend($data) {

            // GET BASE PINGBACK SERVICES
            $services = $this->pingbackServices();

            // GET ADMIN-DEFINED PINGBACK SERVICES
            $services2 = array();
            $services2_data = $this->getSetting('post_ping_list');
            $services2_array = explode("\n", stripslashes($services2_data));
            if (is_array($services2_array)) {
                foreach ($services2_array as $service) {
                    if ($service) {
                        $services2[] = trim($service);
                    }
                }
            }

            // MERGE PINGBACK SERVICES
            $services = array_merge($services, $services2);

            // URL'S FROM POST CONTENT
            $urls = array();

            // MERGE SERVICES AND URLS
            $urls = array_merge($urls, $services);

            // HAVE URLS
            if (count($urls) > 0) {

                // MAKE URLS UNIQUE
                $ping_urls = array_unique($urls);

                /*
                --------------------------------------------
                 $title contains the title of the page you're sending
                 $url is the url of the page
                --------------------------------------------
                 the output is an array with two elements:
                 status: ok / ko
                 msg: the text response from pingomatic
                --------------------------------------------
                */

                $content = '<?xml version="1.0"?>'.
                    '<methodCall>'.
                    ' <methodName>weblogUpdates.ping</methodName>'.
                    '  <params>'.
                    '   <param>'.
                    '    <value>' .$data['post']['title']. '</value>'.
                    '   </param>'.
                    '  <param>'.
                    '   <value>' .$data['post']['url']. '</value>'.
                    '  </param>'.
                    ' </params>'.
                    '</methodCall>';

                // LOOP
                foreach ($ping_urls as $url) {

                    // GET PARTS OF URL
                    $url_parts = parse_url($url);

                    $headers = "POST / HTTP/1.0\r\n".
                    "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5 (.NET CLR 3.5.30729)\r\n".
                    "Host: " .$url_parts['host']. "\r\n".
                    "Content-Type: text/xml\r\n".
                    "Content-length: ".strlen($content);

                    $request = $headers. "\r\n\r\n" .$content;

                    $response = "";
                    $fs = @fsockopen($url_parts['host'], 80, $errno, $errstr);
                    if ($fs) {
                        @fwrite ($fs, $request);
                        while (!feof($fs)) $response .= fgets($fs);
                        @fclose ($fs);
                        @preg_match_all("/<(name|value|boolean|string)>(.*)<\/(name|value|boolean|string)>/U", $response, $ar, PREG_PATTERN_ORDER);
                        for ($i=0; $i<count($ar[2]); $i++) {
                            $ar[2][$i] = strip_tags($ar[2][$i]);
                        }
                        $result[] = array(
                            'url'       => $url,
                            'status'    => ($ar[2][1]==1 ? 'failed' : 'ok' ),
                            'message'   => $ar[2][3]
                        );
                    } else {
                        $result[] = array(
                            'url'       => $url,
                            'status'    => 'failed',
                            'message'   => $errstr." (".$errno.")"
                        );
                    }

                }

            }

            return $result;

         }


        #############################
        # CRON
        #############################


        // RUN THE MAIN CRON
        public function runCron() {

            $out = array();

            // DATE & TIME
            $publish_dt_from = date('Y-m-d H:i:s', strtotime('-10 Minutes'));
            $publish_dt_to = date('Y-m-d H:i:s');

            // GET POSTS PUBLISHING IN PAST 10 MINUTES
            $publishing_query = "SELECT * FROM `" .$this->tables['posts']. "` WHERE (`status`=2) AND ((`dt_publish`>'" .$publish_dt_from. "') AND (`dt_publish`<='" .$publish_dt_to. "'))";
            $publishing_q = sqlquery($publishing_query);

            $out[] = sqlrows($publishing_q). ' posts to publish.';

            // LOOP THROUGH POSTS
            while ($publishing = sqlfetch($publishing_q)) {

                // STRIPSLASHES
                foreach ($publishing as $name => $value) {
                    $data['post'][$name] = stripslashes($value);
                }

                // MARK TO PUBLISH
                $data['publish'] = true;

                // GET CURRENT CATEGORIES
                $cat_query = "SELECT `category_id` FROM `" .$this->tables['post_categories']. "` WHERE (`post_id`=" .$publishing['id']. ")";
                $cat_q = sqlquery($cat_query);
                while ($cat = sqlfetch($cat_q)) {
                    $data['category'][$cat['category_id']] = $cat['category_id'];
                }

                // GET CURRENT TAGS
                $tag_query = "SELECT `tag_id` FROM `" .$this->tables['post_tags']. "` WHERE (`post_id`=" .$publishing['id']. ")";
                $tag_q = sqlquery($tag_query);
                while ($tag = sqlfetch($tag_q)) {
                    $data['tag'][$tag['tag_id']] = $tag['tag_id'];
                }

                // DECODE REVSOCIAL DATA
                $data['revsocial'] = json_decode($publishing['revsocial'], true);

                //echo print_r($data);
                //exit;

                // PUBLISH POST
                $result = $this->post_addUpdate($data);

                $out[] = 'ID: ' .$publishing['id']. ' - ' .$result['message'];

            }

            return implode("\n", $out). "\n";

        }


        #############################
        # GLOBAL SEARCH
        #############################


        public function globalSearch($q='', $params=array()) {

            $results = array();

            // GET MATCHING POSTS
            $results_query = "SELECT `id`, `slug`, `title` FROM `" .$this->tables['posts']. "` WHERE (`title` LIKE '%" .$q. "%') ORDER BY `dt_published` DESC LIMIT " .(int)$params['limit'];
            $results_q = sqlquery($results_query);
            while ($row = sqlfetch($results_q)) {
                $results[] = array(
                    'title' => stripslashes($row['title']),
                    'url'   => $this->postURL($row['id'], 0, $row),
                    'type'  => 'blog'
                );
            }

            return $results;

        }


        #############################
        # SITEMAP
        #############################


        public function getSitemap($page=array()) {
            global $bigtree;

            $out = array();

            // CATEGORIES
            $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->categoryURL($r['id'], $r)
                );
            }

            // POSTS
            $query = "SELECT * FROM `" .$this->tables['posts']. "` WHERE (`status`=1) ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->postURL($r['id'], 0, $r)
                );
            }

            return $out;

        }


    }

?>