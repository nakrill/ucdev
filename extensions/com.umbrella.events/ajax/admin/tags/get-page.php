<?php

$admin->requireLevel(1);

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

// MODULE CLASS
if (!$_uccms_events) $_uccms_events = new uccms_Events;

//$_uccms_events->setPerPage(5);

// WHERE ARRAY
$wa[] = "t.id>0";

// STATUSES
$statusa = array(
    'inactive'  => 0,
    'active'    => 1,
);
if (isset($statusa[$_REQUEST['status']])) {
    $wa[] = "t.active=" .$statusa[$_REQUEST['status']];
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(t.title) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED TAGS
$tag_query = "
SELECT t.*
FROM `" .$_uccms_events->tables['tags']. "` AS `t`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY t.title ASC, t.id ASC
LIMIT " .(($page - 1) * $_uccms_events->perPage()). "," .$_uccms_events->perPage();
$tag_q = sqlquery($tag_query);

// NUMBER OF PAGED TAGS
$num_tags = sqlrows($tag_q);

// TOTAL NUMBER OF TAGS
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_events->tables['tags']. "` AS `t` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_events->pageCount($total_results['total']);

// HAVE TAGS
if ($num_tags > 0) {

    // LOOP
    while ($tag = sqlfetch($tag_q)) {

        ?>

        <li class="item">
            <section class="tag_title">
                <a href="./edit/?id=<?php echo $tag['id']; ?>"><?php echo stripslashes($tag['title']); ?></a>
            </section>
            <section class="tag_events">
                <?php
                $pc_query = "SELECT `id` FROM `" .$_uccms_events->tables['event_tags']. "` WHERE (`tag_id`=" .$tag['id']. ")";
                $pc_q = sqlquery($pc_query);
                ?>
                <a href="../events/?tag_id=<?php echo $tag['id']; ?>"><?php echo number_format(sqlrows($pc_q), 0); ?></a>
            </section>
            <section class="tag_status status_<?php if ($tag['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                <?php if ($tag['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
            </section>
            <section class="tag_edit">
                <a class="icon_edit" title="Edit Tag" href="./edit/?id=<?php echo $tag['id']; ?>"></a>
            </section>
            <section class="tag_delete">
                <a href="./delete/?id=<?php echo $tag['id']; ?>" class="icon_delete" title="Delete Tag" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
            </section>
        </li>

        <?php

    }

// NO tags
} else {
    ?>
    <li style="text-align: center;">
        No tags.
    </li>
    <?php
}

?>

<script>
	BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>