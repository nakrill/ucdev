<?php

$admin->requireLevel(1);

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

// MODULE CLASS
if (!$_uccms_events) $_uccms_events = new uccms_Events;

//$_uccms_events->setPerPage(5);

// WHERE ARRAY
$wa[] = "c.id>0";

// STATUSES
$statusa = array(
    'inactive'  => 0,
    'active'    => 1,
);
if (isset($statusa[$_REQUEST['status']])) {
    $wa[] = "c.active=" .$statusa[$_REQUEST['status']];
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(c.title) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED CATEGORIES
$category_query = "
SELECT c.*
FROM `" .$_uccms_events->tables['categories']. "` AS `c`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY c.title ASC, c.id ASC
LIMIT " .(($page - 1) * $_uccms_events->perPage()). "," .$_uccms_events->perPage();
$category_q = sqlquery($category_query);

// NUMBER OF PAGED CATEGORIES
$num_categories = sqlrows($category_q);

// TOTAL NUMBER OF CATEGORIES
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_events->tables['categories']. "` AS `c` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_events->pageCount($total_results['total']);

// HAVE CATEGORIES
if ($num_categories > 0) {

    // LOOP
    while ($category = sqlfetch($category_q)) {

        ?>

        <li class="item">
            <section class="category_title">
                <a href="./edit/?id=<?php echo $category['id']; ?>"><?php echo stripslashes($category['title']); ?></a>
            </section>
            <section class="category_events">
                <?php
                $pc_query = "SELECT `id` FROM `" .$_uccms_events->tables['event_categories']. "` WHERE (`category_id`=" .$category['id']. ")";
                $pc_q = sqlquery($pc_query);
                ?>
                <a href="../events/?category_id=<?php echo $category['id']; ?>"><?php echo number_format(sqlrows($pc_q), 0); ?></a>
            </section>
            <section class="category_status status_<?php if ($category['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                <?php if ($category['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
            </section>
            <section class="category_edit">
                <a class="icon_edit" title="Edit Category" href="./edit/?id=<?php echo $category['id']; ?>"></a>
            </section>
            <section class="category_delete">
                <a href="./delete/?id=<?php echo $category['id']; ?>" class="icon_delete" title="Delete Category" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
            </section>
        </li>

        <?php

    }

// NO CATEGORIES
} else {
    ?>
    <li style="text-align: center;">
        No categories.
    </li>
    <?php
}

?>

<script>
	BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>