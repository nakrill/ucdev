<?php

$admin->requireLevel(1);

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

// MODULE CLASS
if (!$_uccms_events) $_uccms_events = new uccms_Events;

//$_uccms_events->setPerPage(5);

// WHERE ARRAY
$wa[] = "l.id>0";

// STATUSES
$statusa = array(
    'inactive'  => 0,
    'active'    => 1,
);
if (isset($statusa[$_REQUEST['status']])) {
    $wa[] = "l.active=" .$statusa[$_REQUEST['status']];
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(l.title) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED LOCATIONS
$location_query = "
SELECT l.*
FROM `" .$_uccms_events->tables['locations']. "` AS `l`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY l.title ASC, l.id ASC
LIMIT " .(($page - 1) * $_uccms_events->perPage()). "," .$_uccms_events->perPage();
$location_q = sqlquery($location_query);

// NUMBER OF PAGED LOCATIONS
$num_locations = sqlrows($location_q);

// TOTAL NUMBER OF LOCATIONS
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_events->tables['locations']. "` AS `l` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_events->pageCount($total_results['total']);

// HAVE LOCATIONS
if ($num_locations > 0) {

    // LOOP
    while ($location = sqlfetch($location_q)) {

        ?>

        <li class="item">
            <section class="location_title">
                <a href="./edit/?id=<?php echo $location['id']; ?>"><?php echo stripslashes($location['title']); ?></a>
            </section>
            <section class="location_events">
                <?php
                $pc_query = "SELECT `id` FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`location_id`=" .$location['id']. ")";
                $pc_q = sqlquery($pc_query);
                ?>
                <a href="../events/?location_id=<?php echo $location['id']; ?>"><?php echo number_format(sqlrows($pc_q), 0); ?></a>
            </section>
            <section class="location_status status_<?php if ($location['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                <?php if ($location['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
            </section>
            <section class="location_edit">
                <a class="icon_edit" title="Edit Location" href="./edit/?id=<?php echo $location['id']; ?>"></a>
            </section>
            <section class="location_delete">
                <a href="./delete/?id=<?php echo $location['id']; ?>" class="icon_delete" title="Delete Location" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
            </section>
        </li>

        <?php

    }

// NO LOCATIONS
} else {
    ?>
    <li style="text-align: center;">
        No locations.
    </li>
    <?php
}

?>

<script>
	BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>