<?php

// MODULE CLASS
if (!$_uccms_events) $_uccms_events = new uccms_Events;

// HAS ACCESS
if ($_uccms_events->adminModulePermission()) {

    //$_uccms_events->setPerPage(5);

    // CLEAN UP
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
    $category_id    = (int)$_GET['category_id'];
    $tag_id         = (int)$_GET['tag_id'];
    $account_id     = (int)$_GET['account_id'];
    $limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_events->perPage();

    if (isset($_GET['base_url'])) {
        $base_url = $_GET['base_url'];
    } else if (defined('MODULE_ROOT')) {
        $base_url = MODULE_ROOT. 'events';
    } else {
        $base_url = '.';
    }

    // WHERE ARRAY
    unset($wa);
    $wa[] = "e.id>0";

    // STATUSES
    $statusa = array(
        'inactive'  => 0,
        'active'    => 1,
        'personal'  => 2,
        'deleted'   => 9
    );
    if (isset($statusa[$_REQUEST['status']])) {
        $wa[] = "e.status=" .$statusa[$_REQUEST['status']];
    } else {
        $wa[] = "e.status!=9";
    }

    // CATEGORY SPECIFIED
    if ($category_id > 0) {
        $inner_join_cat = "INNER JOIN `" .$_uccms_events->tables['event_categories']. "` AS `pc` ON e.id=pc.event_id";
        $wa[] = "pc.category_id=" .$category_id;
    }

    // TAG SPECIFIED
    if ($tag_id > 0) {
        $inner_join_tag = "INNER JOIN `" .$_uccms_events->tables['event_tags']. "` AS `tc` ON e.id=tc.event_id";
        $wa[] = "tc.category_id=" .$tag_id;
    }

    // ACCOUNT SPECIFIED
    if ($account_id > 0) {
        $wa[] = "e.account_id=" .$account_id;
    }

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(e.title) LIKE '%$part%') OR (LOWER(e.description) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED EVENTS
    $event_query = "
    SELECT e.*
    FROM `" .$_uccms_events->tables['events']. "` AS `e`
    " .$inner_join_cat. " " .$inner_join_tag. "
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY e.dt_created ASC, e.id DESC
    LIMIT " .(($page - 1) * $limit). "," .$limit;
    $event_q = sqlquery($event_query);

    // NUMBER OF PAGED EVENTS
    $num_events = sqlrows($event_q);

    // TOTAL NUMBER OF EVENTS
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_events->tables['events']. "` AS `e` " .$inner_join_cat. " " .$inner_join_tag. " WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_events->pageCount($total_results['total']);

    // HAVE EVENTS
    if ($num_events > 0) {

        // ACCOUNTS ARRAY
        $accta = array();

        // LOOP
        while ($event = sqlfetch($event_q)) {

            // CATEGORIES ARRAY
            $cata = array();

            // HAVE ACCOUNT ID
            if ($event['account_id'] > 0) {

                // NOT IN ACCOUNT ARRAY
                if (!$accta[$event['account_id']]) {

                    // GET ACCOUNT INFO
                    $acct_query = "
                    SELECT *
                    FROM `bigtree_users` AS `bu`
                    WHERE (bu.id=" .$event['account_id']. ")
                    LIMIT 1
                    ";
                    $acct_q = sqlquery($acct_query);
                    $acct = sqlfetch($acct_q);
                    $accta[$event['account_id']] = $acct;

                }

            }

            // GET CATEGORIES
            $catr_query = "
            SELECT c.id, c.title
            FROM `" .$_uccms_events->tables['event_categories']. "` AS `ec`
            INNER JOIN `" .$_uccms_events->tables['categories']. "` AS `c` ON ec.category_id=c.id
            WHERE (ec.event_id=" .$event['id']. ")
            ORDER BY c.title ASC
            ";
            $catr_q = sqlquery($catr_query);
            while ($catr = sqlfetch($catr_q)) {
                $cata[] = '<a href="' .$base_url. '/?category_id=' .$catr['id']. '">' .stripslashes($catr['title']). '</a>';
            }

            ?>

            <li class="item">
                <section class="event_title">
                    <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $event['id']; ?>"><?php echo stripslashes($event['title']); ?></a>
                </section>
                <section class="event_date">
                    <?php echo date('n/j/Y g:i A', strtotime($event['dt_start'])); ?>
                </section>
                <section class="event_categories">
                    <?php echo implode(', ', $cata); ?>
                </section>
                <section class="event_account">

                </section>
                <section class="event_status status_<?php if ($event['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php echo $_uccms_events->statuses[$event['status']]; ?>
                </section>
                <section class="event_edit">
                    <a class="icon_edit" title="Edit Event" href="<?php echo $base_url; ?>/edit/?id=<?php echo $event['id']; ?>"></a>
                </section>
                <section class="event_delete">
                    <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $event['id']; ?>" class="icon_delete" title="Delete Event" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
                </section>
            </li>

            <?php

        }

        unset($accta);

    // NO EVENTS
    } else {
        ?>
        <li style="text-align: center;">
            No events.
        </li>
        <?php
    }

    ?>

    <script>
	    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>