<?
    class uccms_Events extends BigTreeModule {

        var $Table = "";

        var $Extension = "com.umbrella.events";

        // DB TABLES
        var $tables = array(
            'categories'            => 'uccms_events_categories',
            'event_categories'      => 'uccms_events_event_category_rel',
            'event_ecommerce_items' => 'uccms_events_event_ecommerce_item_rel',
            'event_images'          => 'uccms_events_event_images',
            'event_locations'       => 'uccms_events_event_location_rel',
            'event_tags'            => 'uccms_events_event_tag_rel',
            'events'                => 'uccms_events_event',
            'locations'             => 'uccms_events_locations',
            'settings'              => 'uccms_events_settings',
            'tags'                  => 'uccms_events_tags'
        );

        // EVENT STATUSES
        var $statuses = array(
            0 => 'Inactive',
            1 => 'Active',
            2 => 'Personal',
            9 => 'Deleted'
        );

        var $settings, $misc;

        // START AS 0, WILL ADJUST LATER
        static $PerPage = 0;


        function __construct() {

        }


        #############################
        # GENERAL - MISC
        #############################


        // FRONTEND PATH
        public function frontendPath() {
            if (!isset($this->settings['frontend_path'])) {
                $this->settings['frontend_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*events')"))['path']);
            }
            if (!$this->settings['frontend_path']) $this->settings['frontend_path'] = 'directory';
            return $this->settings['frontend_path'];
        }


        // MODULE ID
        public function moduleID() {
            if (!isset($this->settings['module_id'])) {
                $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
            }
            return $this->settings['module_id'];
        }


        // ADMIN USER ID
        public function adminID() {
            return $_SESSION['bigtree_admin']['id'];
        }


        // ADMIN ACCESS LEVEL
        public function adminModulePermission() {
            global $admin;
            if (!isset($this->settings['admin_module_permission'])) {
                $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
            }
            return $this->settings['admin_module_permission'];
        }


        // STRING IS JSON
        public function isJson($string) {
            if (substr($string, 0, 2) == '{"') {
                return true;
            }
        }


        // CONVERTS A STRING INTO A REWRITE-ABLE URL
        public function makeRewrite($string) {

            $string = strtolower(trim($string));

            // REMOVE ALL QUOTES
            $string = str_replace("'", '', $string);
            $string = str_replace('"', '', $string);

            // STRIP ALL NON WORD CHARS
            $string = preg_replace('/\W/', ' ', $string);

            // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
            $string = preg_replace('/\ +/', '-', $string);

            // TRIM DASHES
            $string = preg_replace('/\-$/', '', $string);
            $string = preg_replace('/^\-/', '', $string);

            return $string;

        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
        public function imageBridgeIn($string) {
            $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $dpa = explode('/', $string);
                return array_pop($dpa);
                //$what = array_pop($dpa);
            }
        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
        public function imageBridgeOut($string, $what, $file=false) {
            $string = stripslashes($string);
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $base = ($file ? SITE_ROOT : STATIC_ROOT);
                return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
            }
        }


        // CONVERT BIGTREE FILE LOCATION TO FULL URL
        public function bigtreeFileURL($file) {
            return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
        }


        // SET NUMBER PER PAGE
        public function setPerPage($num) {
            static::$PerPage = (int)$num;
        }


        // NUMBER PER PAGE
        public function perPage() {
            global $cms;
            if (!static::$PerPage) {
                $pp = $this->getSetting('events_per_page');
                if ($pp) {
                    $this->setPerPage($pp);
                }
            }
            if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
            if (!static::$PerPage) $this->setPerPage(15);
            return static::$PerPage;
        }


        // GET NUMBER OF PAGES FROM QUERY
        public function pageCount($num) {
            return ceil($num / $this->perPage());
        }


        // TRIM URL
        public function trimURL($string) {
            return trim(preg_replace('#^https?://#', '', $string));
        }


        // CONVERT ARRAY TO CSV
        public function arrayToCSV($array=array()) {
            if (is_array($array)) {
                return implode(',', $array);
            }
        }


        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                7 => array(
                    'code'  => 'SU',
                    'title' => 'Sunday'
                ),
                1 => array(
                    'code'  => 'MO',
                    'title' => 'Monday'
                ),
                2 => array(
                    'code'  => 'TU',
                    'title' => 'Tuesday'
                ),
                3 => array(
                    'code'  => 'WE',
                    'title' => 'Wednesday'
                ),
                4 => array(
                    'code'  => 'TH',
                    'title' => 'Thursday'
                ),
                5 => array(
                    'code'  => 'FR',
                    'title' => 'Friday'
                ),
                6 => array(
                    'code'  => 'SA',
                    'title' => 'Saturday'
                )
            );
        }


        // CREATE ARRAY OF TIMEZONES
        public function timezoneArray($show_time=true, $show_offset=true) {
            $tz_list = DateTimeZone::listAbbreviations();
            $tz_idents = DateTimeZone::listIdentifiers();
            $tz_data = $offset = $added = array();
            foreach ($tz_list as $abbr => $info) {
                foreach ($info as $zone) {
                    if ((!empty($zone['timezone_id'])) && (!in_array($zone['timezone_id'], $added)) && (in_array($zone['timezone_id'], $tz_idents))) {
                        $z = new DateTimeZone($zone['timezone_id']);
                        $c = new DateTime(null, $z);
                        $zone['time'] = $c->format('h:i A');
                        $zone['offset'] = $z->getOffset($c);
                        $tz_data[] = $zone;
                        $offset[] = $z->getOffset($c);
                        $added[] = $zone['timezone_id'];
                    }
                }
            }
            array_multisort($offset, SORT_ASC, $tz_data);
            $retval = array();
            foreach ($tz_data as $key => $row) {
                $out = '';
                if ($show_time) {
                    $out .= $row['time']. ' - ';
                }
                $out .= $row['timezone_id'];
                if ($show_offset) {
                    $out .= ' (' .$this->timezone_formatOffset($row['offset']). ')';
                }
                $retval[$row['timezone_id']] = $out;
            }
            return $retval;
        }


        // FORMAT TIMEZONE OFFSET
        public function timezone_formatOffset($offset) {
            $hours = $offset / 3600;
            $remainder = $offset % 3600;
            $sign = $hours > 0 ? '+' : '-';
            $hour = (int) abs($hours);
            $minutes = (int) abs($remainder / 60);
            if ($hour == 0 && $minutes == 0) {
                $sign = ' ';
            }
            return 'GMT' . $sign . str_pad($hour, 2, '0', STR_PAD_LEFT).':'. str_pad($minutes,2, '0');
        }


        // GET TIMEZONE
        public function timezone() {
            $timezone = $this->getSetting('timezone');
            if (!$timezone) $timezone = 'America/New_York';
            return $timezone;
        }

        #############################
        # GENERAL - SETTINGS
        #############################


        // GET SETTING
        public function getSetting($id='') {
            $id = sqlescape($id);
            if ($id) {
                if (isset($this->settings[$id])) {
                    return $this->settings[$id];
                } else {
                    $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tables['settings']. "` WHERE `id`='" .$id. "'"));
                    if ($this->isJson($f['value'])) {
                        $val = json_decode($f['value'], true);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$id] = $val;
                }
                return $val;
            }
        }


        // GET SETTINGS
        public function getSettings($array='') {
            $out = array();
            if (is_array($array)) {
                $oa = array();
                foreach ($array as $id) {
                    $id = sqlescape($id);
                    if ($id) {
                        $oa[] = "`id`='" .$id. "'";
                    }
                }
                if (count($oa) > 0) {
                    $or = "(" .implode(") OR (", $oa). ")";
                }
                if ($or) {
                    $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` WHERE " .$or;
                }
            } else {
                $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` ORDER BY `id` ASC";
            }
            if ($query) {
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    if ($this->isJson($string)) {
                        $val = json_decode($f['value']);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$f['id']] = $val;
                    $out[$f['id']] = $val;
                }
            }
            return $out;
        }


        // SET SETTING
        public function setSetting($id='', $value='') {

            // CLEAN UP
            $id = sqlescape($id);

            // HAVE ID
            if ($id) {

                // VALUE IS ARRAY
                if (is_array($value)) {
                    $value = json_encode($value);

                // VALUE IS STRING
                } else {
                    $value = sqlescape($value);
                }

                $query = "
                INSERT INTO `" .$this->tables['settings']. "`
                SET `id`='" .$id. "', `value`='" .$value. "'
                ON DUPLICATE KEY UPDATE `value`='" .$value. "'
                ";
                if (sqlquery($query)) {
                    return true;
                }

            }

        }


        #############################
        # GENERAL - CATEGORIES
        #############################


        public function getCategoryTree($parent=0, $active='', $depth=100) {

            $out = array();

            // CLEAN UP
            $parent = (int)$parent;

            if ($active === true) {
                $active_sql = "WHERE (active=1)";
            } else if ($active === false) {
                $active_sql = "WHERE (active=0)";
            } else {
                $active_sql = "";
            }

            // GET CATEGORIES
            $query = "SELECT * FROM `" .$this->tables['categories']. "` " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($item = sqlfetch($q)) {
                $ref = & $refs[$item['id']];
                foreach ($item as $name => $value) {
                    $ref[$name] = $value;
                }
                if ($item['parent'] == 0) {
                    $out[$item['id']] = & $ref;
                } else {
                    $refs[$item['parent']]['children'][$item['id']] = & $ref;
                }
            }

            unset($refs);

            // PARENT SPECIFIED
            if ($parent) {
                return $this->findArrayKey($out, $parent);
            // NO PARENT SPECIFIED
            } else {
                return $out;
            }

            return $out;

        }


        // OUTPUT THE CATEGORY TREE IN A DROPDOWN (TREE ARRAY, SELECTED ID,
        function printCategoryDropdown($tree, $selected=0, $id=0, $parent=0, $r=0) {
            foreach ($tree as $i => $t) {
                $dash = ($t['parent'] == 0) ? '' : str_repeat('-', $r) .' ';
                $sel = ($t['id'] == $selected ? ' selected="selected"' : '');
                $dis = ($t['id'] == $id ? ' disabled="disabled"' : '');
                echo "\t" . '<option value="' .$t['id']. '"' .$sel . $dis. '>' .$dash . stripslashes($t['title']). '</option>' . "\n";
                if (isset($t['children'])) {
                    $this->printCategoryDropdown($t['children'], $selected, $id, $t['parent'], $r+1);
                }
            }
        }


        // GET A SUB-ARRAY BY MATCHING KEY
        public function findArrayKey($array, $index) {
            if (!is_array($array)) return null;
            if (isset($array[$index])) return $array[$index];
            foreach ($array as $item) {
                $return = $this->findArrayKey($item, $index);
                if (!is_null($return)) {
                    return $return;
                }
            }
            return null;
        }


        // GET THE PARENT(S) OF A CATEGORY
        public function getCategoryParents($id=0) {
            $out = array();
            $id = (int)$id;
            if ($id > 0) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .$id. ")";
                $cat = sqlfetch(sqlquery($query));
                array_push($out, $cat);
                if ($cat['parent']) {
                    $out = array_merge($out, $this->getCategoryParents($cat['parent']));
                }
            }
            return $out;
        }


        // GET SUB-CATEGORIES
        public function subCategories($id, $active=null, $by_pricegroup=false) {
            $out = array();

            if ($active === true) {
                $active_sql = " AND (`active`=1)";
            } else if ($active === false) {
                $active_sql = " AND (`active`=0)";
            } else {
                $active_sql = "";
            }

            // GET SUB CATEGORIES FROM DB
            $sub_query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`parent`=" .(int)$id. ") " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $sub_q = sqlquery($sub_query);
            while ($sub = sqlfetch($sub_q)) {
                $out[] = $sub;
            }

            return $out;

        }


        /*
        // GET POSTS IN CATEGORY WITH PAGING
        public function categoryPosts($id, $vars=array()) {
            global $_paging;

            $out = array();

            if ($id) {

                $sql_sort = "b.name ASC, b.id ASC";

                // HOW MANY LISTINGS PER PAGE
                if ($vars['limit']) $limit = (int)$vars['limit']; // SPECIFIED DIRECTLY
                if (!$limit) $limit = $this->getSetting('listings_per_page'); // USE EXTENSION SETTING
                if (!$limit) $limit = $this->perPage(); // USE BIGTREE / DEFAULT

                // CURRENT PAGE
                $page = isset($vars['page']) ? intval($vars['page']) : 1;

                // GET BUSINESSES
                $business_query = "
                SELECT b.*
                FROM `" .$this->tables['businesses']. "` AS `b`
                INNER JOIN `" .$this->tables['business_categories']. "` AS `bc` ON b.id=bc.business_id
                WHERE (bc.category_id=" .$id. ") AND (b.status=1)
                ORDER BY " .$sql_sort. "
                LIMIT " .(($page - 1) * $limit). "," .$limit. "
                ";
                $business_q = sqlquery($business_query);
                while ($business = sqlfetch($business_q)) {
                    $out[] = $business;
                }

                // HAVE PAGING CLASS
                if ($_paging) {

                    // PREPARE PAGING (RESULTS PER PAGE, NUMBER OF PAGE NUMBERS, CURRENT PAGE, REQUEST VARIABLES (_GET, _POST), USE MOD_REWRITE)
                    $_paging->prepare($limit, 5, $page, $_GET, false);

                }

            }

            return $out;

        }


        // GET TOTAL POSTS IN CATEGORY
        public function categoryPostsTotal($id, $vars=array()) {

            $count = 0;

            if ($id) {

                // GET BUSINESSES
                $business_query = "
                SELECT i.*
                FROM `" .$this->tables['posts']. "` AS `b`
                INNER JOIN `" .$this->tables['post_categories']. "` AS `bc` ON b.id=bc.business_id
                WHERE (bc.category_id=" .$id. ") AND (b.status=1)
                ";
                $business_q = sqlquery($business_query);
                $count = sqlrows($business_q);

            }

            return $count;

        }
        */


        // GENERATE CATEGORY URL
        public function categoryURL($id, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/category/' .trim($slug). '-' .$id. '/';
        }


        // BREADCRUMB OUTPUT
        public function generateBreadcrumbs($array, $options=array()) {

            // OPTIONS
            if (!$options['base']) $options['base'] = './';
            if (!$options['home_title']) $options['home_title'] = 'Events'; // MATTD: Make into a configurable setting
            if (!$options['sep']) $options['sep'] = ' / '; // MATTD: Make into a configurable setting

            // ELEMENTS
            $els = array();

            // SHOW HOME LINK
            if (!$options['no_home']) {
                $els[] = '<a href="' .$options['base']. '">' .$options['home_title']. '</a>';
            }

            // NUMBER OF PARENT CATEGORIES
            $num_cat = count($array);

            // HAVE PARENT CATEGORIES
            if ($num_cat > 0) {

                // REVERSE ARRAY FOR EASIER OUTPUT
                $tcat = array_reverse($array);

                $i = 1;

                // LOOP
                foreach ($tcat as $cat) {
                    if ($options['rewrite']) {
                        $url = $this->categoryURL($cat['id'], $cat);
                    } else {
                        $url = $options['base']. '?id=' .$cat['id'];
                    }
                    $line = '';
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '<a href="' .$url. '">';
                    }
                    $line .= stripslashes($cat['title']);
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '</a>';
                    }
                    $els[] = $line;
                    $i++;
                }

            }

            echo implode($options['sep'], $els);

        }


        #############################
        # TAG
        #############################


        // GENERATE TAG URL
        public function tagURL($id, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['tags']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/tag/' .trim($slug). '-' .$id. '/';
        }


        #############################
        # LOCATION
        #############################


        // GENERATE LOCATION URL
        public function locationURL($id, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['locations']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/location/' .trim($slug). '-' .$id. '/';
        }


        #############################
        # EVENTS
        #############################


        // GENERATE EVENT URL
        public function eventURL($id, $category_id=0, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['events']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            /*
            $category_slug = $data['category_slug']. '-' .$category_id;
            if (!$data['category_slug']) {
                if ($category_id > 0) {
                    $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$id. ")";
                    $data = sqlfetch(sqlquery($query));
                    if ($data['slug']) {
                        $category_slug = stripslashes($data['slug']). '-' .$data['id'];
                    } else {
                        $category_slug = $this->makeRewrite($data['title']). '-' .$id;
                    }
                } else {
                    $category_slug = 'post';
                }
            }
            return WWW_ROOT . $bigtree['path'][0]. '/' .trim($category_slug). '/' .trim($slug). '-' .$id. '/';
            */
            $out = '/' .$this->frontendPath(). '/event/' .trim($slug). '-' .$id. '/';
            if ($data['date']) {
                $out = $out. '?date=' .date('Y-m-d', strtotime($data['date']));
            }
            return $out;
        }


        // GET EVENTS
        public function getEvents($timeframe_start='', $timeframe_end='', $vars=array()) {

            $out = array();

            // CLEAN UP
            $query          = trim($vars['query']);
            $category_id    = (int)$vars['category_id'];
            $tag_id         = (int)$vars['tag_id'];
            $location_id    = (int)$vars['location_id'];
            $account_id     = (int)$vars['account_id'];

            // TIMEFRAME
            if ($timeframe_start) {
                $timeframe_start = strtotime($timeframe_start);
            } else {
                $timeframe_start = time();
            }
            if ($timeframe_end) {
                $timeframe_end = strtotime($timeframe_end);
            } else {
                $timeframe_end = time();
            }
            $timeframe_start = date('Y-m-d 00:00:00', $timeframe_start);
            $timeframe_end = date('Y-m-d 23:59:59', $timeframe_end);

            // WHERE ARRAY
            if ($vars['status'] == 'all') {
            } else if (isset($vars['status'])) {
                $wa[] = "e.status=" .(int)$vars['status'];
            } else {
                $wa[] = "e.status=1";
            }

            // TIMEFRAME
            $wa[] = "((`dt_start`>='" .$timeframe_start. "') AND (`dt_start`<='" .$timeframe_end. "')) OR ((`dt_start`<='" .$timeframe_end. "') AND (DATE(`dt_end`)='0000-00-00'))";

            // CATEGORY SPECIFIED
            if ($category_id > 0) {
                $inner_join_cat = "INNER JOIN `" .$this->tables['event_categories']. "` AS `ec` ON e.id=ec.event_id";
                $wa[] = "ec.category_id=" .$category_id;
            }

            // TAG SPECIFIED
            if ($tag_id > 0) {
                $inner_join_tag = "INNER JOIN `" .$this->tables['event_tags']. "` AS `et` ON e.id=et.event_id";
                $wa[] = "et.tag_id=" .$tag_id;
            }

            // LOCATION SPECIFIED
            if ($location_id > 0) {
                $inner_join_location = "INNER JOIN `" .$this->tables['event_locations']. "` AS `el` ON e.id=el.event_id";
                $wa[] = "el.location_id=" .$location_id;
            }

            // ACCOUNT SPECIFIED
            if ($account_id > 0) {
                $wa[] = "e.account_id=" .$account_id;
            }

            // IS SEARCHING
            if ($query) {
                $qparts = explode(" ", $query);
                $twa = array();
                foreach ($qparts as $part) {
                    $part = sqlescape(strtolower($part));
                    $twa[] = "(LOWER(e.title) LIKE '%$part%') OR (LOWER(e.description) LIKE '%$part%')";
                }
                $wa[] = "(" .implode(" OR ", $twa). ")";
            }

            // GET MATCHING EVENTS
            $event_query = "
            SELECT e.* FROM `" .$this->tables['events']. "` AS `e`
            " .$inner_join_cat. " " .$inner_join_tag. " " .$inner_join_location. "
            WHERE (" .implode(") AND (", $wa). ")
            ";

            //echo $event_query;
            //exit;

            $event_q = sqlquery($event_query);
            while ($event = sqlfetch($event_q)) {

                //echo print_r($event);

                // IS RECURRING
                if ($event['recurring']) {

                    // GENERATE RECURRING RULE
                    $rrule = $this->rrule_generate($event);

                    //echo $rrule. '<br />';

                    // TRY When
                    try {

                        $r = new When\When();

                        // GET OCCURANCES
                        $r->startDate(new DateTime($event['dt_start']))->rrule($rrule)->generateOccurrences();

                        //print_r($r->occurrences);

                        // HAVE OCCURRENCES
                        if (count($r->occurrences) > 0) {

                            // LOOP
                            foreach ($r->occurrences as $occ) {

                                $event['date'] = $occ->format('Y-m-d H:i:s');

                                // DATE IS WITHIN TIMEFRAME
                                if ((strtotime($event['date']) >= strtotime($timeframe_start)) && (strtotime($event['date']) <= strtotime($timeframe_end))) {
                                    $out[] = $event;
                                }

                            }

                        }

                    // When ERROR
                    } catch (Exception $e) {
                        //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }

                // SINGLE EVENT
                } else {
                    $event['date'] = $event['dt_start'];
                    $out[] = $event;
                }

            }

            // HAVE EVENTS
            if (count($out) > 0) {

                // SORT BY DATE/TIME ASCENDING
                usort($out, function($a, $b) {
                    return strtotime($a['date']) - strtotime($b['date']);
                });

            }

            return $out;

        }


        // GET EVENT INFO
        public function getEventInfo($id=0, $event=array(), $status=1, $vars=array()) {

            $out = array();

            $id         = (int)$id;
            $status     = (int)$status;

            // NO EVENT DATA AND HAVE ID
            if ((!$event['id']) && ($id > 0)) {

                // GET EVENT
                $event_query = "SELECT * FROM `" .$this->tables['events']. "` WHERE (`id`=" .$id. ") AND (`status`=" .$status. ")";
                $event_q = sqlquery($event_query);
                $event = sqlfetch($event_q);

            }

            // HAVE EVENT ID
            if ($event['id']) {

                // HAVE ACCOUNT ID
                if ($event['account_id'] > 0) {
                    $event['account'] = $this->getaccountInfo($event['account_id']);
                }

                // GET IMAGES
                $image_query = "SELECT * FROM `" .$this->tables['event_images']. "` WHERE (`event_id`=" .$event['id']. ") ORDER BY `sort` ASC, `id` ASC";
                $image_q = sqlquery($image_query);
                while ($image = sqlfetch($image_q)) {
                    $event['images'][] = $image;
                }

                // GET CATEGORIES
                $category_query = "
                SELECT c.*
                FROM `" .$this->tables['event_categories']. "` AS `ec`
                INNER JOIN `" .$this->tables['categories']. "` AS `c` ON ec.category_id=c.id
                WHERE (ec.event_id=" .$event['id']. ") AND (c.active=1)
                ORDER BY c.title ASC, c.id ASC
                ";
                $category_q = sqlquery($category_query);
                while ($category = sqlfetch($category_q)) {
                    $event['categories'][] = $category;
                }

                // GET TAGS
                $tag_query = "
                SELECT t.*
                FROM `" .$this->tables['event_tags']. "` AS `et`
                INNER JOIN `" .$this->tables['tags']. "` AS `t` ON et.tag_id=t.id
                WHERE (et.event_id=" .$event['id']. ") AND (t.active=1)
                ORDER BY t.title ASC, t.id ASC
                ";
                $tag_q = sqlquery($tag_query);
                while ($tag = sqlfetch($tag_q)) {
                    $event['tags'][] = $tag;
                }

                // GET LOCATIONS
                $location_query = "
                SELECT l.*
                FROM `" .$this->tables['event_locations']. "` AS `el`
                INNER JOIN `" .$this->tables['locations']. "` AS `l` ON el.location_id=l.id
                WHERE (el.event_id=" .$event['id']. ") AND (l.active=1)
                ORDER BY l.title ASC, l.id ASC
                ";
                $location_q = sqlquery($location_query);
                while ($location = sqlfetch($location_q)) {
                    $event['locations'][] = $location;
                }

                // IS RECURRING
                if ($event['recurring']) {

                    if (!$vars['timeframe_start']) {
                        $vars['timeframe_start'] = date('Y-m-d H:i:s');
                    }
                    if (!$vars['timeframe_end']) {
                        $vars['timeframe_end'] = date('Y-m-d H:i:s', strtotime('+30 days'));
                    }

                    // GENERATE RECURRING RULE
                    $rrule = $this->rrule_generate($event);

                    //echo $rrule. '<br />';

                    // TRY When
                    try {

                        $r = new When\When();

                        // GET OCCURANCES
                        $r->startDate(new DateTime($event['dt_start']))->rrule($rrule)->generateOccurrences();

                        //print_r($r->occurrences);

                        // HAVE OCCURRENCES
                        if (count($r->occurrences) > 0) {

                            // LOOP
                            foreach ($r->occurrences as $occ) {

                                $date = $occ->format('Y-m-d H:i:s');

                                // DATE SPECIFIED FROM URL
                                if ($vars['date']) {
                                    $event['date'] = $vars['date']. ' ' .date('H:i:s', strtotime($date));
                                }

                                // DATE IS WITHIN TIMEFRAME
                                if ((strtotime($date) >= strtotime($vars['timeframe_start'])) && (strtotime($date) <= strtotime($vars['timeframe_end']))) {

                                    // ADD TO OCCURRENCES ARRAY
                                    $event['occurrences'][] = $date;

                                    // DATE SPECIFIED FROM URL
                                    if ($vars['date']) {

                                        // DATE MATCH
                                        if (date('Y-m-d', strtotime($vars['date'])) == date('Y-m-d', strtotime($date))) {
                                            $event['date'] = $date;
                                        }

                                    // NO DATE FROM URL
                                    } else {

                                        // DAY IS TODAY
                                        if (date('Y-m-d') == date('Y-m-d', strtotime($date))) {
                                            $event['date'] = $date;
                                        }
                                    }

                                }

                            }

                            // NO EVENT DATE YET
                            if (!$event['date']) {
                                $event['date'] = $event['occurrences'][0];
                            }

                            // STILL NO EVENT DATE
                            if (!$event['date']) {
                                $event['date'] = $event['dt_start'];
                            }

                        }

                    // When ERROR
                    } catch (Exception $e) {
                        //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }

                // SINGLE EVENT
                } else {
                    $event['date'] = $event['dt_start'];
                }

                $out = $event;

            }

            return $out;

        }


        // GET EVENT'S E-COMMERCE ITEMS
        public function event_getEcommerceItems($event_id, $param=array()) {

            $out = array();

            $event_id = (int)$event_id;

            if ($event_id) {

                // GET EVENT - ECOMM ITEM RELATIONS
                $query = "SELECT * FROM `" .$this->tables['event_ecommerce_items']. "` WHERE (`event_id`=" .$event_id. ") ORDER BY `sort` ASC";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['ecommerce_item_id']] = $row;
                }

            }

            return $out;

        }


        // GET ACCOUNT INFO
        public function getAccountInfo($id=0) {

            $out = array();

            $id = (int)$id;

            // HAVE ID
            if ($id) {

                // GET ACCOUNT INFO
                $account_query = "
                SELECT a.username, a.email, ad.firstname, ad.lastname
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$id. ")
                LIMIT 1
                ";
                $account_q = sqlquery($account_query);
                $out = sqlfetch($account_q);

            }

            return $out;

        }


        // BUILD RRULE
        public function rrule_generate($event) {
            $out = '';

            // IS RECURRING
            if ($event['recurring']) {

                $parts = array();

                // FREQUENCY
                switch ($event['recurring_freq']) {
                    case 'day':
                        $parts['FREQ'] = 'DAILY';
                        break;
                    case 'week':
                        $parts['FREQ'] = 'WEEKLY';
                        break;
                    case 'month':
                        $parts['FREQ'] = 'MONTHLY';
                        break;
                    case 'year':
                        $parts['FREQ'] = 'YEARLY';
                        break;
                }

                // INTERVAL
                if ($event['recurring_interval'] > 0) {
                    $parts['INTERVAL'] = $event['recurring_interval'];
                }

                // COUNT
                if ($event['recurring_count'] > 0) {
                    $parts['COUNT'] = $event['recurring_count'];
                }

                // BY DAY
                if ($event['recurring_byday']) {
                    $parts['BYDAY'] = stripslashes($event['recurring_byday']);
                }

                // BY DAY OF MONTH
                if ($event['recurring_bymonthday']) {
                    $parts['BYMONTHDAY'] = stripslashes($event['recurring_bymonthday']);
                }

                // HAVE END DATE
                if (substr($event['dt_end'], 0, 10) != '0000-00-00') {
                    $parts['UNTIL'] = date('c', strtotime($event['dt_end']));
                }

                // HAVE PARTS
                if (count($parts) > 0) {
                    $tpa = array();
                    foreach ($parts as $name => $value) {
                        $tpa[] = $name. '=' .$value;
                    }
                    $out = implode(';', $tpa);
                    unset($parts);
                    unset($tpa);
                }

            }

            return $out;

        }


    }
?>
