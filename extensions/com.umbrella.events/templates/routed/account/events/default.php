<?php

// MODULE CLASS
$_uccms_events = new uccms_Events;

// GET SETTING(S)
//$ecomm['settings'] = $_uccms_events->getSettings();

// DEFAULT FILE
$include_file = dirname(__FILE__). '/dashboard/default.php';

// HAVE COMMANDS
if (count($bigtree['commands']) > 1) {

    $base = array_shift($bigtree['commands']);

    // GET BIGTREE ROUTING
    list($include, $commands) = BigTree::route(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/account/' .$base. '/', $bigtree['commands']);

    // FILE TO INCLUDE
    if ($include) {
        $include_file = $include;
    }

}

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/account/master/main.css" />
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/account/master/main.js"></script>

<?php

// INCLUDE ROUTED FILE
include($include_file);

?>