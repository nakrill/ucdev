<?php

$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET EVENT
    $event_query = "SELECT * FROM `" .$_uccms_events->tables['events']. "` WHERE (`id`=" .$id. ") AND (`account_id`=" .$_uccms['_account']->userID(). ") AND (`status`!=9)";
    $event_q = sqlquery($event_query);
    $event = sqlfetch($event_q);

}

/*
aria-describedby="fch_eventTitle"
<small id="fch_eventTitle" class="form-text text-muted">We'll never share your email with anyone else.</small>
*/

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/account/master/edit.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/account/custom/edit.css" />
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/account/master/edit.js"></script>
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/account/custom/edit.js"></script>

<h3><?php echo ($event['id'] ? 'Edit' : 'Add'); ?> Event</h3>

<form id="eventEdit" action="./process/" method="post">

    <div class="section">

        <h3><i class="fa fa-calendar" aria-hidden="true"></i>Main</h3>

        <div class="form-group">
            <label for="fc_eventTitle">Title</label>
            <input id="fc_eventTitle" type="text" name="event[title]" placeholder="" value="<?php echo stripslashes($event['title']); ?>" class="form-control">
        </div>

        <div class="form-group">
            <label for="fc_eventDescription">Description</label>
            <textarea id="fc_eventDescription" name="event[description]" placeholder="" class="form-control"><?php echo stripslashes($event['title']); ?></textarea>
        </div>

        <div class="form-group">
            <label for="fc_eventStartDate">Event Start Date</label>
            <?php

            // FIELD VALUES
            $field = array(
                'id'        => 'fc_eventStartDate',
                'key'       => 'event[start_date]',
                'value'     => $event['dt_start'],
                'required'  => true,
                'options'   => array(
                    'default_today' => false
                )
            );

            // INCLUDE FIELD
            include(BigTree::path('admin/form-field-types/draw/date.php'));

            ?>
        </div>

        <div class="form-group">
            <label for="fc_eventStartTime">Event Start Time</label>
            <?php

            // FIELD VALUES
            $field = array(
                'id'        => 'fc_eventStartTime',
                'key'       => 'event[start_time]',
                'value'     => $event['dt_start'],
                'required'  => true
            );

            // INCLUDE FIELD
            include(BigTree::path('admin/form-field-types/draw/time.php'));

            ?>
        </div>

        <div class="form-group">
            <label for="fc_eventEndDate">Event End Date</label>
            <?php

            // FIELD VALUES
            $field = array(
                'id'        => 'fc_eventEndDate',
                'key'       => 'event[end_date]',
                'value'     => $event['dt_end'],
                'required'  => true,
                'options'   => array(
                    'default_today' => false
                )
            );

            // INCLUDE FIELD
            include(BigTree::path('admin/form-field-types/draw/date.php'));

            ?>
        </div>

        <div class="form-group">
            <label for="fc_eventEndTime">Event End Time</label>
            <?php

            // FIELD VALUES
            $field = array(
                'id'        => 'fc_eventEndTime',
                'key'       => 'event[end_time]',
                'value'     => $event['dt_end'],
                'required'  => true
            );

            // INCLUDE FIELD
            include(BigTree::path('admin/form-field-types/draw/time.php'));

            ?>
        </div>

        <div class="repeats">

            <div class="form-check">
                <input id="fc_eventRecurring" type="checkbox" class="form-check-input">
                <label for="fc_eventRecurring" class="form-check-label">Repeats</label>
            </div>

            <div class="repeats_enabled_content" style="padding-top: 15px;<?php if (!$event['recurring']) { echo ' display: none;'; } ?>">

                <div class="form-group">
                    <select id="fc_eventRecurringFrequency" name="event[recurring_freq]">
                        <option value="day" <?php if ($event['recurring_freq'] == 'day') { ?>selected="selected"<?php } ?>>Daily</option>
                        <option value="week" <?php if ($event['recurring_freq'] == 'week') { ?>selected="selected"<?php } ?>>Weekly</option>
                        <option value="month" <?php if ($event['recurring_freq'] == 'month') { ?>selected="selected"<?php } ?>>Monthly</option>
                        <option value="year" <?php if ($event['recurring_freq'] == 'year') { ?>selected="selected"<?php } ?>>Yearly</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="fc_eventRecurringInterval">Every</label>
                    <select name="event[recurring_interval]">
                        <?php for ($i=1; $i<=30; $i++) { ?>
                            <option value="<?php echo $i; ?>" <?php if ($i == $event['recurring_interval']) { echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
                        <?php } ?>
                    </select> <span class="re day" style="<?php if (($event['recurring_freq'] != 'day') && ($event['recurring_freq'])) { ?>display: none;<?php } ?>">Days</span><span class="re week" style="<?php if ($event['recurring_freq'] != 'week') { ?>display: none;<?php } ?>">Weeks</span><span class="re month" style="<?php if ($event['recurring_freq'] != 'month') { ?>display: none;<?php } ?>">Months</span><span class="re year" style="<?php if ($event['recurring_freq'] != 'year') { ?>display: none;<?php } ?>">Years</span>
                </div>

                <div class="freq_content_container">

                    <div class="freq_content week" style="<?php if ($event['recurring_freq'] != 'week') { echo 'display: none;'; } ?>">
                        <label>Repeat On</label>
                        <div class="contain">
                            <?php
                            $seldaya = array();
                            if ($event['recurring_freq'] == 'week') {
                                $seldaya = explode(',', stripslashes($event['recurring_byday']));
                            }
                            foreach ($_uccms_events->daysOfWeek() as $day_id => $day) {
                                ?>
                                <div style="float: left; <?php if ($day['code'] != 'SA') { ?>padding-right: 5px;<?php } ?>">
                                    <input type="checkbox" name="recurring_days[]" value="<?php echo $day['code']; ?>" <?php if (in_array($day['code'], $seldaya)) { ?>checked="checked"<?php } ?> /> <?php echo substr($day['title'], 0, 1); ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div style="padding-top: 4px; font-size: .8em; opacity: .7;">(Start day must be selected.)</div>
                    </div>

                    <div class="freq_content month" style="<?php if ($event['recurring_freq'] != 'month') { echo 'display: none;'; } ?>">
                        <label>Repeat By</label>
                        <div class="contain">
                            <div style="float: left;">
                                <input type="radio" name="event[recurring_by]" value="dotw" <?php if ($event['recurring_byday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Week</label>
                            </div>
                            <div style="float: left;">
                                <input type="radio" name="event[recurring_by]" value="dotm" <?php if ($event['recurring_bymonthday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Month</label>
                            </div>
                        </div>
                    </div>

                </div>

                <fieldset class="repeats_end">
                    <label>Ends</label>
                    <div>
                        <fieldset class="last">
                            <input type="radio" name="event[recurring_end]" value="0" <?php if (!$event['recurring_count']) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">On end date</label>
                        </fieldset>
                        <fieldset class="last radio_padding">
                            <input type="radio" name="event[recurring_end]" value="1" <?php if ($event['recurring_count'] > 0) { echo 'checked="checked"'; } ?> style="margin-top: 3px;" /><label class="for_checkbox">After <input type="text" name="event[recurring_count]" value="<?php if ($event['recurring_count'] > 0) { echo $event['recurring_count']; } ?>" style="float: none; display: inline; width: 22px; height: auto; padding: 2px 5px;" /> occurances</label>
                        </fieldset>
                        <fieldset>
                            <input type="radio" name="event[recurring_end]" value="-1" <?php if ($event['recurring_count'] == -1) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">Never</label>
                        </fieldset>
                    </div>
                </fieldset>

            </div>

        </div>

    </div>

    <div class="section">

        <h3><i class="fa fa-map-marker" aria-hidden="true"></i>Address</h3>

        <div class="form-group">
            <label for="fc_eventState">State</label>
            <select name="event[state]" class="form-control">
                <option value="">Select</option>
                <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                    <option value="<?=$state_code?>" <?php if ($state_code == $event['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                <?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label for="fc_eventCity">City</label>
            <input id="fc_eventCity" type="text" name="event[city]" placeholder="" value="<?php echo stripslashes($event['city']); ?>" class="form-control">
        </div>

        <div class="form-group">
            <label for="fc_eventAddress">Address</label>
            <input id="fc_eventAddress" type="text" name="event[address1]" placeholder="" value="<?php echo stripslashes($event['address1']); ?>" class="form-control">
        </div>

        <div class="form-group">
            <label for="fc_eventZip">Zip Code</label>
            <input id="fc_eventZip" type="text" name="event[zip]" placeholder="" value="<?php echo stripslashes($event['zip']); ?>" class="form-control">
        </div>

    </div>

    <div class="section">

        <h3><i class="fa fa-envelope-o" aria-hidden="true"></i>Contact</h3>

        <div class="form-group">
            <label for="fc_eventContactName">Name</label>
            <input id="fc_eventContactName" type="text" name="event[name_contact]" placeholder="" value="<?php echo stripslashes($event['name_contact']); ?>" class="form-control">
        </div>

        <div class="form-group">
            <label for="fc_eventContactPhone">Phone</label>
            <input id="fc_eventContactPhone" type="text" name="event[phone]" placeholder="" value="<?php echo stripslashes($event['phone']); ?>" class="form-control">
        </div>

        <div class="form-group">
            <label for="fc_eventContactEmail">Email</label>
            <input id="fc_eventContactEmail" type="email" name="event[email]" placeholder="" value="<?php echo stripslashes($event['email']); ?>" class="form-control">
        </div>

        <div class="form-group">
            <label for="fc_eventContactWebsite">Website</label>
            <input id="fc_eventContactWebsite" type="text" name="event[url]" placeholder="www.domain.com" value="<?php echo stripslashes($event['url']); ?>" class="form-control">
        </div>

    </div>



    <? /*
    <div class="contain">
        <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
        <div class="contain uccms_toggle-seo" style="display: none;">

            <fieldset>
                <label>URL</label>
                <input type="text" name="event[slug]" value="test-event">
            </fieldset>

            <fieldset>
                <label>Meta Title</label>
                <input type="text" name="event[meta_title]" value="">
            </fieldset>

            <fieldset>
                <label>Meta Description</label>
                <textarea name="event[meta_description]" style="height: 64px;"></textarea>
            </fieldset>

            <fieldset>
                <label>Meta Keywords</label>
                <textarea name="event[meta_keywords]" style="height: 64px;"></textarea>
            </fieldset>

        </div>
    </div>
    */ ?>
