<?php

// GET EVENTS
$events_query = "SELECT * FROM `" .$_uccms_events->tables['events']. "` WHERE (`account_id`=" .$_uccms['_account']->userID(). ") AND (`status`!=9) ORDER BY `dt_created` DESC";
$events_q = sqlquery($events_query);

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/account/master/dashboard.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/account/custom/dashboard.css" />
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/account/master/dashboard.js"></script>
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/account/custom/dashboard.js"></script>

<h3>My Events</h3>

<?php

// HAVE EVENTS
if (sqlrows($events_q) > 0 ) {

    ?>

    <div class="events row clearfix">

        <?php

        // LOOP
        while ($event = sqlfetch($events_q)) {

            // GET EVENT INFO
            $event = $_uccms_events->getEventInfo($event['id'], $event);

            // EVENT ELEMENT
            include(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/events/elements/event.php');

        }

        ?>

    </div>

    <?php

// NO EVENTS
} else {
    ?>
    It doesn't look like you have any events.
    <?php
}

?>