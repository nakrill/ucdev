<?php

// INCLUDE COMPOSER
include_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

use When\When;

// MODULE CLASS
$_uccms_events = new uccms_Events;

// GET SETTING(S)
$ex_events['settings'] = $_uccms_events->getSettings();

// NUMBER OF LEVELS IN URL PATH
$path_num = count($bigtree['path']);

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/master/master.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/custom/master.css" />
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/master/master.js"></script>
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/custom/master.js"></script>

<div class="site-width block_center">
    <div class="page">

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>

        <div id="eventsContainer" class="clearfix <?php if ($ex_events['settings']['sidebar_enabled']) { echo 'w-sidebar'; if ($ex_events['settings']['sidebar_position'] == 'left') { echo ' sidebar-left'; } else { echo ' sidebar-right'; } } ?>">

            <?php

            /*
            // SIDEBAR ENABLED
            if ($ex_events['settings']['sidebar_enabled']) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/blog/elements/sidebar.php');
            }
            */

            // SECTIONS DIRECTORY
            $sections_dir = dirname(__FILE__). '/sections';

            // DATA DIRECTORY
            $data_dir = dirname(__FILE__). '/data';

            // HOME
            if ($path_num == 1) {
                include($data_dir. '/home.php');
                include($sections_dir. '/home/default.php');

            // ONE LEVEL DEEP
            } else if ($path_num == 2) {

                /*
                // SEARCH
                if ($bigtree['path'][1] == 'search') {
                    include($data_dir. '/search.php');
                    include($sections_dir. '/search/default.php');
                }
                */

            // TWO LEVELS DEEP
            } else if ($path_num == 3) {

                // GET LAST PATH PARTS
                $ppa = explode('-', $bigtree['path'][2]);

                // IS CATEGORY
                if ($bigtree['path'][1] == 'category') {
                    $_REQUEST['category'] = (int)$ppa[count($ppa)-1];
                    include($data_dir. '/home.php');
                    include($sections_dir. '/home/default.php');

                // IS TAG
                } else if ($bigtree['path'][1] == 'tag') {
                    $_REQUEST['tag'] = (int)$ppa[count($ppa)-1];
                    include($data_dir. '/home.php');
                    include($sections_dir. '/home/default.php');

                // IS LOCATION
                } else if ($bigtree['path'][1] == 'location') {
                    $_REQUEST['location'] = (int)$ppa[count($ppa)-1];
                    include($data_dir. '/home.php');
                    include($sections_dir. '/home/default.php');

                // IS EVENT
                } else if ($bigtree['path'][1] == 'event') {
                    $event_id = (int)$ppa[count($ppa)-1];
                    include($data_dir. '/event.php');
                    include($sections_dir. '/event/default.php');
                }

            }

            ?>

        </div>

    </div>
</div>