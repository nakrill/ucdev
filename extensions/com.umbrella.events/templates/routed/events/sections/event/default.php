<?php /*
https://schema.org/Event
*/ ?>

<link rel="stylesheet" href="/extensions/<?=$_uccms_events->Extension;?>/css/lib/swipebox.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.1/js/jquery.swipebox.min.js"></script>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/master/event.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/custom/event.css" />
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/master/event.js"></script>
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/custom/event.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

// HAVE EVENT
if ($event['id']) {

    ?>

    <div class="item_container" itemscope itemtype="http://schema.org/Event">

        <?php

        /*
        // BREADCRUMB OPTIONS
        $bcoptions = array(
            'base'      => WWW_ROOT . $bigtree['path'][0]. '/',
            'rewrite'   => true,
            'link_last' => true
        );

        // BREADCRUMBS
        include(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/event-directory/elements/breadcrumbs.php');
        */

        ?>

        <div class="smallColumn">

            <h1 itemprop="name"><?php echo stripslashes($event['title']); ?></h1>

            <div class="date pad-top">
                <span class="dt_start" itemprop="startDate">
                    <?php
                    echo date('n/j/Y', strtotime($event['date']));
                    echo ' ' .date('g:i A', strtotime($event['dt_start']));
                    ?>
                </span>
                 -
                <span class="dt_end" itemprop="endDate" content="<?php echo date('n/j/Y', strtotime($event['date'])). ' ' .date('g:i A', strtotime($event['dt_end'])); ?>">
                    <?php
                    if (!$event['recurring']) {
                        if (date('n/j/Y', strtotime($event['date'])) != date('n/j/Y', strtotime($event['dt_end']))) {
                            echo date('n/j/Y', strtotime($event['date']));
                        }
                    }
                    echo ' ' .date('g:i A', strtotime($event['dt_end']));
                    ?>
                </span>
            </div>

            <?php if (($event['recurring']) && (count($event['occurrences']) > 0)) { ?>
                <div class="repeats">
                    <a href="#" class="expand">Repeats</a>
                    <div class="repeats-expand-content" style="display: none;">
                        <?php foreach ($event['occurrences'] as $occ) { ?>
                            <div class="occurrence"><?php echo date('n/j/Y', strtotime($occ)); ?></div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <?php

            // LOCATIONS
            if ((is_array($event['locations'])) && (count($event['locations']) > 0)) {
                foreach ($event['locations'] as $location) {
                    $tloca[] = '<a href="' .$_uccms_events->locationURL($location['id'], $location). '">' .stripslashes($location['title']). '</a>';
                }
                ?>
                <div class="location pad-top">
                    Location: <?php echo implode(', ', $tloca); ?>
                </div>
                <?php
            }


            // HAVE ADDRESS INFO
            if (($event['address1']) || ($event['address2']) || ($event['city']) || ($event['state']) || ($event['zip'])) {
                ?>
                <div class="map pad-top">
                    <iframe width="100%" height="150" frameborder="0" style="margin: 0; border: 0;" src="https://www.google.com/maps/embed/v1/place?q=<?php echo stripslashes($event['address1']); if ($event['address2']) { echo ',' .stripslashes($event['address2']); } if ($event['city']) { echo ',' .stripslashes($event['city']); } if ($event['state']) { echo ',' .stripslashes($event['state']); } if ($event['zip']) { echo ',' .stripslashes($event['zip']); } ?>&key=AIzaSyDb9IKaXNmMAc8bMuRtOmmEx35cOW1HZEo" allowfullscreen></iframe>
                </div>

                <div class="address_container" itemprop="location" itemscope itemtype="http://schema.org/Place">
                    <div itemprop="name" style="display: none;">
                        <?php if ($event['address1']) { ?>
                            <?php echo stripslashes($event['address1']); ?>
                        <?php } ?>
                        <?php if ($event['address2']) { ?>
                            &nbsp;<?php echo stripslashes($event['address2']); ?>
                        <?php } ?>
                    </div>
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <?php if (($event['address1']) || ($event['address2'])) { ?>
                            <div class="address" itemprop="streetAddress">
                                <?php if ($event['address1']) { ?>
                                    <span><?php echo stripslashes($event['address1']); ?></span>
                                <?php } ?>
                                <?php if ($event['address2']) { ?>
                                    <span><?php echo stripslashes($event['address2']); ?></span>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if (($event['city']) || ($event['state']) || ($event['zip'])) { ?>
                            <div class="csz">
                                <?php if ($event['city']) { ?>
                                    <span itemprop="addressLocality"><?php echo stripslashes($event['city']); ?><?php if ($event['state']) { ?>, <?php } ?></span>
                                <?php } ?>
                                <?php if ($event['state']) { ?>
                                    <span itemprop="addressRegion"><?php echo stripslashes($event['state']); ?></span>
                                <?php } ?>
                                <?php if ($event['zip']) { ?>
                                    <span itemprop="postalCode"><?php echo $event['zip']; ?></span>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }

            ?>

            <div style="height: 20px; overflow: hidden;"></div>

            <?php

            // CONTACT NAME
            if ($event['name_contact']) {
                ?>
                <div class="contact" itemprop="organizer" itemscope itemtype="http://schema.org/Person">
                    Contact: <span itemprop="givenName"><?php echo stripslashes($event['name_contact']); ?></span>
                </div>
                <?php
            }

            // PHONE
            if ($event['phone']) {
                ?>
                <div class="phone">
                    Phone: <span><?php echo stripslashes($event['phone']); ?></span>
                </div>
                <?php
            }

            // EMAIL
            if ($event['email']) {
                ?>
                <div class="email">
                    Email: <a href="mailto:<?php echo stripslashes($event['email']); ?>" target="_blank"><?php echo stripslashes($event['email']); ?></a>
                </div>
                <?php
            }

            // LINK
            if ($event['url']) {
                ?>
                <div class="link">
                    Link: <a href="<?php echo stripslashes($event['url']); ?>" target="_blank"><?php echo stripslashes($event['url']); ?></a>
                </div>
                <?php
            }

            // CATEGORIES
            if ((is_array($event['categories'])) && (count($event['categories']) > 0)) {
                foreach ($event['categories'] as $category) {
                    $tcata[] = '<a href="' .$_uccms_events->categoryURL($category['id'], $category). '">' .stripslashes($category['title']). '</a>';
                }
                ?>
                <div class="category pad-top">
                    Category: <?php echo implode(', ', $tcata); ?>
                </div>
                <?php
            }
            ?>

            <?php

            // TAGS
            if ((is_array($event['tags'])) && (count($event['tags']) > 0)) {
                foreach ($event['tags'] as $tag) {
                    $ttaga[] = '<a href="' .$_uccms_events->tagURL($tag['id'], $tag). '">' .stripslashes($tag['title']). '</a>';
                }
                ?>
                <div class="tag pad-top">
                    Tags: <?php echo implode(', ', $ttaga); ?>
                </div>
                <?php
            }
            ?>

        </div>

        <div class="largeColumn withSmallColumn">

            <div class="images_container">

                <div class="main">

                    <?php

                    // HAVE MAIN IMAGE
                    if ($imagea[0]['id']) {
                        $image = $imagea[0];
                        ?>
                        <a href="<?php echo $image['url']; ?>" title="<?php echo $image['caption']; ?>" class="swipebox" rel="gallery-1">
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['caption']; ?>" itemprop="image" />
                        </a>
                        <?php
                    }

                    ?>

                </div>

                <?php

                // HAVE MORE THAN ONE IMAGE
                if (count($imagea) > 1) {
                    $ii = 1;
                    ?>
                    <div class="thumbs contain">
                        <?php
                        foreach ($imagea as $image) {
                            if ($ii == 1) {
                                $ii++;
                                continue;
                            }
                            if ($image['image']) {
                                ?>
                                <div class="thumb">
                                    <a href="<?php echo $image['url']; ?>" title="<?php echo $image['caption']; ?>" class="swipebox" rel="gallery-1">
                                        <img src="<?php echo $image['url_thumb']; ?>" alt="<?php echo $image['caption']; ?>" />
                                    </a>
                                </div>
                                <?php
                            }
                            $ii++;
                        }
                        ?>
                    </div>
                    <?php
                }

                ?>

            </div>

            <?php if ($event['description']) { ?>
                <div class="content pad-top" itemprop="description">
                    <?php echo stripslashes($event['description']); ?>
                </div>
            <?php } ?>

        </div>

    </div>

    <script type="text/javascript">
        ;(function($) {
            $('.swipebox').swipebox({
                removeBarsOnMobile: false
            });
        })(jQuery);
    </script>

    <?php

// EVENT NOT FOUND
} else {
    ?>

    <div class="largeColumn event_container">

        <h1>Events</h1>

        Event not found.

    </div>

    <?php
}

?>