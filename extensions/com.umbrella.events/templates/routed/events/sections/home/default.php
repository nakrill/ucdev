<?php

/*
// META
if ($ecomm['settings']['home_meta_title']) $bigtree['page']['title'] = stripslashes($ecomm['settings']['home_meta_title']);
if ($ecomm['settings']['home_meta_description']) $bigtree['page']['meta_description'] = stripslashes($ecomm['settings']['home_meta_description']);
if ($ecomm['settings']['home_meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($ecomm['settings']['home_meta_keywords']);
*/

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/master/home.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_events->Extension;?>/css/custom/home.css" />
<script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_events->Extension; ?>/js/lib/jquery-ui-timepicker-addon.js"></script>
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/master/home.js"></script>
<script src="/extensions/<?=$_uccms_events->Extension;?>/js/custom/home.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="largeColumn">

    <?php

    /*
    // HAVE CONTENT
    if ($ex_events['settings']['home_content']) {
        ?>
        <div class="content">
            <?php echo stripslashes($ex_events['settings']['home_content']); ?>
        </div>
        <?php
    }

    // DISPLAYING FEATURED
    if ((int)$ex_events['settings']['home_featured_num'] > 0) {
        include(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/events/elements/featured_events.php');
    }

    // DISPLAYING UPCOMING
    if ((int)$ex_events['settings']['home_upcoming_num'] > 0) {
        include(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/events/elements/upcoming.php');
    }
    */

    ?>

    <h1>Events</h1>

    <div id="form_control">
        <form action="/<?php echo $_uccms_events->frontendPath(); ?>/" method="get">

        <div class="grid">
            <div class="col-1-2">

                <?php

                // GET CATEGORIES
                $category_query = "SELECT * FROM `" .$_uccms_events->tables['categories']. "` WHERE (`active`=1) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                $category_q = sqlquery($category_query);

                // HAVE CATEGORIES
                if (sqlrows($category_q) > 0) {
                    ?>
                    <select name="category">
                        <option value="">All Categories</option>
                        <?php while ($category = sqlfetch($category_q)) { ?>
                            <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $_REQUEST['category']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($category['title']); ?></option>
                        <?php } ?>
                    </select>
                    <?php
                }

                ?>

            </div>
            <div class="col-1-2 right">

                <? /*
                View by: <select name="view">
                    <option value="">Month</option>
                    <option value="week" <?php if ($_REQUEST['view'] == 'week') { ?>selected="selected"<?php } ?>>Week</option>
                </select>
                */ ?>

                <?php

                // GET LOCATIONS
                $location_query = "SELECT * FROM `" .$_uccms_events->tables['locations']. "` WHERE (`active`=1) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                $location_q = sqlquery($location_query);

                // HAVE LOCATIONS
                if (sqlrows($location_q) > 0) {
                    ?>
                    <select name="location">
                        <option value="">All Locations</option>
                        <?php while ($location = sqlfetch($location_q)) { ?>
                            <option value="<?php echo $location['id']; ?>" <?php if ($location['id'] == $_REQUEST['location']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($location['title']); ?></option>
                        <?php } ?>
                    </select>
                    <?php
                }

                ?>

            </div>
        </div>

        <div class="grid">
            <div class="col-1-2">
                <input type="text" name="q" value="<?php echo $_REQUEST['q']; ?>" placeholder="Keywords" /> <input type="submit" value="Search" class="button" style="padding: 0 15px;" />
            </div>
            <div class="col-1-2 right">
                <div>
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'events-start',
                        'key'       => 'start',
                        'value'     => $timeframe_start,
                        'required'  => false,
                        'options'   => array(
                            'default_today' => false
                        )
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                    ?>
                     -
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'events-end',
                        'key'       => 'end',
                        'value'     => $timeframe_end,
                        'required'  => false,
                        'options'   => array(
                            'default_today' => false
                        )
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                    ?>
                     <input type="submit" value="Go" class="button" style="padding: 0 5px;" />
                </div>
            </div>
        </div>

        </form>
    </div>

    <?php

    // VIEWING BY WEEK
    if ($_REQUEST['view'] == 'week') {
        $heading = 'Week: ' .date('n/j/Y', strtotime($timeframe_start)). ' - ' .date('n/j/Y', strtotime($timeframe_end));

    // VIEWING BY MONTH
    } else {
        $heading = date('F, Y', strtotime($timeframe_start));
    }

    ?>

    <div class="current">
        <a href="#" class="prev" data-start="<?php echo $prev_start; ?>" data-end="<?php echo $prev_end; ?>">Prev</a> <h2><?php echo $heading; ?></h2> <a href="#" class="next" data-start="<?php echo $next_start; ?>" data-end="<?php echo $next_end; ?>">Next</a>
    </div>

    <?php

    // HAVE EVENTS
    if (count($events) > 0) {

        ?>

        <div class="item_container contain">

            <?php

            // LOOP
            foreach ($events as $event) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/events/elements/event.php');
            }

            ?>

        </div>

        <?php

    // NO EVENTS
    } else {
        ?>
        <div class="no_events">
            No events.
        </div>
        <?php
    }

    ?>

</div>