<?php

// LIMIT
$upcoming_limit = ($ex_events['settings']['home_upcoming_num'] ? (int)$ex_events['settings']['home_upcoming_num'] : 9);

// GET UPCOMING EVENTS

// HAVE EVENTS
if (count($events) > 0) {

    // HEADING
    if (!$ex_events['settings']['home_upcoming_title']) $ex_events['settings']['home_featured_title'] = 'Upcoming Events';

    ?>

    <div class="upcoming">

        <h3><?php echo stripslashes($ex_events['settings']['home_upcoming_title']); ?></h3>

        <div class="item_container contain">

            <?php

            // LOOP
            foreach ($events as $event) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_events->Extension. '/templates/routed/events/elements/event.php');
            }

            ?>

        </div>

    </div>

    <?php

}

?>