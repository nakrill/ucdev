<?php

// HAVE EVENT ID
if ($event['id']) {

    // LINK
    $link = $_uccms_events->eventURL($event['id'], (int)$event['category_id'], $event);

    // GET IMAGE
    $image_query = "SELECT * FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`event_id`=" .$event['id']. ") ORDER BY `sort` ASC LIMIT 1";
    $image = sqlfetch(sqlquery($image_query));

    // HAVE IMAGE
    if ($image['image']) {
        $image_url = BigTree::prefixFile($_uccms_events->imageBridgeOut($image['image'], 'events'), 't_');
    } else {
        $image_url = WWW_ROOT. 'extensions/' .$_uccms_events->Extension. '/images/event_no-image.jpg';
    }

    ?>

    <div class="event item col-md-3" data-id="<?php echo $event['id']; ?>">
        <div class="contain">
            <div class="image" style="background-image: url('<?php echo $image_url; ?>');">
                <a href="<?php echo $link; ?>"></a>
            </div>
            <?php if ($event['title']) { ?>
                <div class="title"><a href="<?php echo $link; ?>"><?php echo stripslashes($event['title']); ?></a></div>
            <?php } ?>
            <div class="date">
                <?php
                echo date('n/j/Y', strtotime($event['date']));
                echo ' ' .date('g:i A', strtotime($event['dt_start']));
                echo ' - ';
                if (!$event['recurring']) {
                    if (date('n/j/Y', strtotime($event['date'])) != date('n/j/Y', strtotime($event['dt_end']))) {
                        echo date('n/j/Y', strtotime($event['date']));
                    }
                }
                echo ' ' .date('g:i A', strtotime($event['dt_end']));
                ?>
            </div>
        </div>
    </div>

    <?php

}

?>