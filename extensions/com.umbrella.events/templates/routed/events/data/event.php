<?php

// https://developers.google.com/structured-data/rich-snippets/products?hl=en&rd=1
// https://developers.google.com/structured-data/testing-tool/

// CLEAN UP
$event_id = (int)$event_id;

// HAVE EVENT ID
if ($event_id > 0) {

    // GET EVENT
    $event = $_uccms_events->getEventInfo($event_id, array(), 1, array(
        'date'  => $_REQUEST['date']
    ));

    //echo print_r($event);
    //exit;

}

// HAVE EVENT
if ($event['id']) {

    // CUSTOM EDIT LINK
    $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_events->Extension. '*events/events/edit/?id=' .$event['id'];

    // EVENT SLUG
    if ($event['slug']) {
        $slug = stripslashes($event['slug']);
    } else {
        $slug = $_uccms_events->makeRewrite(stripslashes($event['title']));
    }

    // SLUGS DON'T MATCH - 301 TO CURRENT
    if ($bigtree['path'][2] != $slug. '-' .$event['id']) {

        $catgory_id = 0;

        // IS IN CATEGORY
        if ($bigtree['path'][1] != 'event') {

            // GET LAST PATH PARTS
            $ppa = explode('-', $bigtree['path'][1]);

            // HAS NUMBER ON END - IS CATEGORY
            if (is_numeric($ppa[count($ppa) - 1])) {
                $category_id = $ppa[count($ppa) - 1];
            }
        }

        // 301 REDIRECT TO CURRENT EVENT URL
        header('Location: ' .$_uccms_events->eventURL($event['id'], $category_id, $event), true, 301);
        exit;

    }

    // META
    if ($event['meta_title']) {
        $bigtree['page']['title'] = stripslashes($event['meta_title']);
    } else {
        $bigtree['page']['title'] = stripslashes($event['title']);
    }
    if ($event['meta_description']) {
        $bigtree['page']['meta_description'] = stripslashes($event['meta_description']);
    } else if ($event['description']) {
        $bigtree['page']['description'] = stripslashes($event['description']);
    }
    if ($event['meta_keywords']) {
        $bigtree['page']['meta_keywords'] = stripslashes($event['meta_keywords']);
    } else if ($event['keywords']) {
        $bigtree['page']['meta_keywords'] = stripslashes($event['keywords']);
    }

    // CANONICAL URL
    $bigtree['page']['canonical'] = $_uccms_events->eventURL($event['id'], 0, $event);

    // CATEGORY PARENT(S) ARRAY
    $catpa = array();

    // CATEGORY MASTER PARENT ID
    $category_master_parent_id = 0;

    // IS IN CATEGORY
    if ($bigtree['path'][1] != 'event') {

        // GET LAST PATH PARTS
        $ppa = explode('-', $bigtree['path'][1]);

        // HAS NUMBER ON END - IS CATEGORY
        if (is_numeric($ppa[count($ppa) - 1])) {
            $category_id = $ppa[count($ppa) - 1];
        }

        // CATEGORY PARENT(S) ARRAY
        $catpa = $_uccms_events->getCategoryParents($category_id);

        // CATEGORY MASTER PARENT ID
        $category_master_parent_id = $catpa[count($catpa) - 1]['id'];

    }

    // IMAGE ARRAY
    $imagea = array();

    $ii = 0;

    // GET IMAGES
    $image_query = "SELECT * FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`event_id`=" .$event['id']. ") ORDER BY `sort` ASC";
    $image_q = sqlquery($image_query);
    while ($image = sqlfetch($image_q)) {
        if ($image['image']) {
            $imagea[$ii] = $image;
            $imagea[$ii]['url'] = $_uccms_events->imageBridgeOut($image['image'], 'events');
            $imagea[$ii]['url_thumb'] = BigTree::prefixFile($_uccms_events->imageBridgeOut($image['image'], 'events'), 't_');
            $ii++;
        }
    }

    // IS RECURRING
    if ($event['recurring']) {



    }

// EVENT NOT FOUND
} else {
    header('HTTP/1.0 404 Not Found');
    $bigtree['page']['title'] = 'Event Not Found';
    $bigtree['page']['meta_description'] = '';
    $bigtree['page']['meta_keywords'] = '';
}

?>