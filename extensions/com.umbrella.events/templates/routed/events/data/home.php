<?php

// SET DEFAULT DATE FORMAT IF NONE SPECIFIED
if (!$bigtree["config"]["date_format"]) {
    $bigtree["config"]["date_format"] = 'm/d/Y';
}

$event_vars = array();

// CATEGORY SPECIFIED
if ($_REQUEST['category']) {
    $event_vars['category_id'] = (int)$_REQUEST['category'];
}

// TAG SPECIFIED
if ($_REQUEST['tag']) {
    $event_vars['tag_id'] = (int)$_REQUEST['tag'];
}

// LOCATION SPECIFIED
if ($_REQUEST['location']) {
    $event_vars['location_id'] = (int)$_REQUEST['location'];
}

// START DATE SPECIFIED
if ($_REQUEST['start']) {

    $timeframe_start = date('Y-m-d', strtotime($_REQUEST['start']));

    // HAVE END DATE
    if ($_REQUEST['end']) {

        // END DATE IS AFTER START
        if (strtotime($_REQUEST['end']) > strtotime($timeframe_start)) {
            $timeframe_end = $_REQUEST['end'];
        } else {
            $timeframe_end = date('Y-m-t', strtotime($timeframe_start)); // USE END OF THE MONTH
        }

    // NO END DATE
    } else {
        $timeframe_end = date('Y-m-t', strtotime($timeframe_start)); // USE LAST DAY OF THE MONTH
    }

// NO START DATE
} else {
    $timeframe_start = date('Y-m-01'); // USE FIRST OF THIS MONTH
    $timeframe_end = date('Y-m-t', strtotime($timeframe_start)); // USE LAST DAY OF THIS MONTH
}

// IS FULL MONTH
if ((date('j', strtotime($timeframe_start)) == 1) && (date('Y-m-d', strtotime($timeframe_end)) == date('Y-m-t', strtotime($timeframe_start)))) {

    // PREVIOUS START
    $prev_start = date('m/d/Y', strtotime('-1 Month', strtotime($timeframe_start)));

    // NEXT START
    $next_start = date('m/d/Y', strtotime('+1 Month', strtotime($timeframe_start)));

// IS CUSTOM TIMEFRAME
} else {

    // DAYS DIFFERENCE
    $days_diff = floor((strtotime($timeframe_end) - strtotime($timeframe_start)) / (60*60*24));

    //echo $days_diff;
    //exit;

    // PREVIOUS START
    $prev_start = date('m/d/Y', strtotime('-' .($days_diff + 1). ' Days', strtotime($timeframe_start)));

    // PREVIOUS END
    $prev_end = date('m/d/Y', strtotime('-1 Day', strtotime($timeframe_start)));

    // NEXT START
    $next_start = date('m/d/Y', strtotime('+1 Day', strtotime($timeframe_end)));

    // NEXT END
    $next_end = date('m/d/Y', strtotime('+ '.($days_diff + 1). ' Days', strtotime($timeframe_end)));

}

/*
// VIEWING BY WEEK
if ($_REQUEST['view'] == 'week') {

    // HAVE START DATE
    if ($_REQUEST['start']) {

        // IF DAY IS SUNDAY
        if (date('N', strtotime($_REQUEST['start'])) == 7) {
            $timeframe_start = date('Y-m-d', strtotime($_REQUEST['start']));
        } else {
            $timeframe_start = date('Y-m-d', strtotime('Last Sunday', strtotime($_REQUEST['start']))); // USE LAST SUNDAY
        }

    // NO START DATE
    } else {

        // IF TODAY IS SUNDAY
        if (date('N') == 7) {
            $timeframe_start = date('Y-m-d');
        } else {
            $timeframe_start = date('Y-m-d', strtotime('Last Sunday')); // USE LAST SUNDAY
        }

    }

    // SEVEN DAYS OUT
    $seven_out = date('Y-m-d', strtotime('+6 Days', strtotime($timeframe_start)));

    // HAVE END DATE
    if ($_REQUEST['end']) {

        // END DATE SPECIFIED IS GREATER THAN END OF THIS MONTH
        if (strtotime($_REQUEST['end']) > strtotime($seven_out)) {
            $timeframe_end = $seven_out; // USE END OF THIS MONTH
        } else {
            $timeframe_end = $_REQUEST['end'];
        }

    // NO END DATE
    } else {
        $timeframe_end = $seven_out;
    }

    // PREVIOUS START
    $prev_start = date('m/d/Y', strtotime('Last Sunday', strtotime($timeframe_start)));

    // NEXT START
    $next_start = date('m/d/Y', strtotime('Next Sunday', strtotime($timeframe_end)));

// VIEWING BY MONTH
} else {

    // HAVE START DATE
    if ($_REQUEST['start']) {
        $timeframe_start = $_REQUEST['start'];

    // NO START DATE
    } else {
        $timeframe_start = date('Y-m-01'); // USE FIRST OF THIS MONTH
    }

    // HAVE END DATE
    if ($_REQUEST['end']) {

        // END DATE SPECIFIED IS GREATER THAN END OF THIS MONTH
        if (strtotime($_REQUEST['end']) > strtotime(date('Y-m-t', strtotime($timeframe_start)))) {
            $timeframe_end = date('Y-m-t', strtotime($timeframe_start)); // USE END OF THIS MONTH
        } else {
            $timeframe_end = $_REQUEST['end'];
        }

    // NO END DATE
    } else {
        $timeframe_end = date('Y-m-t', strtotime($timeframe_start)); // USE LAST DAY OF THIS MONTH
    }

    // PREVIOUS START
    $prev_start = date('m/d/Y', strtotime('-1 Month', strtotime($timeframe_start)));

    // NEXT START
    $next_start = date('m/d/Y', strtotime('+1 Month', strtotime($timeframe_start)));

}
*/

// QUERY SPECIFIED
if ($_REQUEST['q']) {
    $event_vars['query'] = $_REQUEST['q'];
}

//echo 'Start: ' .$timeframe_start. '<br />';
//echo 'End: ' .$timeframe_end;

// GET EVENTS
$events = $_uccms_events->getEvents($timeframe_start, $timeframe_end, $event_vars);

//echo print_r($events);
//exit;

?>