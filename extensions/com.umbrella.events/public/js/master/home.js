$(document).ready(function() {

    // CATEGORY / LOCATION CHANGE
    $('#eventsContainer #form_control select[name="category"], #eventsContainer #form_control select[name="location"]').change(function() {
        $('#eventsContainer #form_control form').submit();
    });

    // VIEW CHANGE
    $('#eventsContainer #form_control select[name="view"]').change(function() {
        $('#eventsContainer #form_control form').submit();
    });

    // DATE PICKER
    $('.date_picker').datepicker({ dateFormat: 'mm/dd/yy', duration: 200, showAnim: 'slideDown' });

    // PREV CLICK
    $('#eventsContainer .current .prev, #eventsContainer .current .next').click(function(e) {
        e.preventDefault();
        $('#eventsContainer #events-start').val($(this).attr('data-start'));
        $('#eventsContainer #events-end').val($(this).attr('data-end'));
        $('#eventsContainer #form_control form').submit();
    });

});