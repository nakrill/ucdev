$(function() {

    var new_cat_id = 0;

    // REPEATS TOGGLE
    $('#fc_eventRecurring').change(function(e) {
        if ($(this).prop('checked')) {
            $('form#eventEdit .repeats_enabled_content').show();
        } else {
            $('form#eventEdit .repeats_enabled_content').hide();
        }
    });

    // REPEATS FREQUENCY CHANGE
    $('#fc_eventRecurringFrequency').change(function() {
        var freq = $(this).val();
        $('form#eventEdit .freq_content_container .freq_content').hide();
        $('form#eventEdit .freq_content_container .freq_content.' +freq).show();
        $('form#eventEdit .repeats_enabled_content .repeat_every .re').hide();
        $('form#eventEdit .repeats_enabled_content .repeat_every .re.' +freq).show();
    });

});