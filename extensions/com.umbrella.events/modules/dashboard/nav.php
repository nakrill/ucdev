<?php

$extension_nav =
["link" => "com.umbrella.events*events", "title" => "Events", "access" => 1, "children" => [
    ["link" => ".", "title" => "Dashboard", "access" => 1],
    ["link" => "categories", "title" => "Categories", "access" => 1],
    ["link" => "tags", "title" => "Tags", "access" => 1],
    ["link" => "locations", "title" => "Locations", "access" => 1],
    ["link" => "events", "title" => "Events", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>