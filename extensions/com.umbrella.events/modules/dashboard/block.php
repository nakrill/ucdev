<?php

// MODULE CLASS
$_uccms_events = new uccms_Events;

// HAS ACCESS
if ($_uccms_events->adminModulePermission()) {

    ?>

    <style type="text/css">

        #block_events #upcoming_events header span, #block_events #upcoming_events .item section {
            text-align: left;
        }

        #block_events #upcoming_events .event_title {
            width: 300px;
        }
        #block_events #upcoming_events .event_date {
            width: 150px;
        }
        #block_events #upcoming_events .event_categories {
            width: 160px;
        }
        #block_events #upcoming_events .event_tags {
            width: 200px;
        }
        #block_events #upcoming_events .event_account {
            width: 130px;
        }
        #block_events #upcoming_events .event_status {
            width: 90px;
        }
        #block_events #upcoming_events .event_edit {
            width: 55px;
        }
        #block_events #upcoming_events .event_delete {
            width: 55px;
        }

    </style>

    <div id="block_events" class="block">
        <h2><a href="../<?php echo $extension; ?>*events/">Events</a></h2>
        <div id="upcoming_events" class="table">
            <summary>
                <h2>Latest Events</h2>
                <a class="add_resource add" href="../<?php echo $extension; ?>*events/events/">
                    View All
                </a>
            </summary>
            <header style="clear: both;">
                <span class="event_title">Title</span>
                <span class="event_date">Date</span>
                <span class="event_categories">Categories</span>
                <span class="event_account">Account</span>
                <span class="event_status">Status</span>
                <span class="event_edit">Edit</span>
                <span class="event_delete">Delete</span>
            </header>
            <ul id="results" class="items">
                <?php
                $_GET['base_url'] = '../' .$extension. '*events/events';
                $_GET['limit'] = 5;
                include($extension_dir. '/ajax/admin/events/get-page.php');
                ?>
            </ul>
        </div>
    </div>

    <?php

}

unset($_uccms_events);

?>