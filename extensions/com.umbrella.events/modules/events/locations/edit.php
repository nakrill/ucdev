<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Locations', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET LOCATION INFO
    $location_query = "SELECT * FROM `" .$_uccms_events->tables['locations']. "` WHERE (`id`=" .$id. ")";
    $location_q = sqlquery($location_query);
    $location = sqlfetch($location_q);

}

?>

<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

    #locations .location_title {
        width: 530px;
    }
    #locations .location_events {
        width: 100px;
    }
    #locations .location_status {
        width: 100px;
    }
    #locations .location_edit {
        width: 55px;
    }
    #locations .location_delete {
        width: 55px;
    }

</style>

<div class="container">

    <form enctype="multipart/form-data" action="../process/" method="post">
    <input type="hidden" name="location[id]" value="<?php echo $location['id']; ?>" />

    <header>
        <h2><?php if ($location['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Location</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset class="form-group">
                    <label>Title</label>
                    <input type="text" name="location[title]" value="<?php echo stripslashes($location['title']); ?>" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="location[active]" value="1" <?php if ($location['active']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

            </div>

            <div class="right">

            </div>

        </div>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset class="form-group">
                    <label>URL</label>
                    <input type="text" name="location[slug]" value="<?php echo stripslashes($location['slug']); ?>" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Title</label>
                    <input type="text" name="location[meta_title]" value="<?php echo stripslashes($location['meta_title']); ?>" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Description</label>
                    <textarea name="location[meta_description]" style="height: 64px;"><?php echo stripslashes($location['meta_description']); ?></textarea>
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Keywords</label>
                    <textarea name="location[meta_keywords]" style="height: 64px;"><?php echo stripslashes($location['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>