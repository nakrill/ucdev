<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`location_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_events->tables['locations']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Location', 'Location deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Location', 'Failed to delete location.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Location', 'No location specified.');
}

BigTree::redirect(MODULE_ROOT.'locations/');

?>