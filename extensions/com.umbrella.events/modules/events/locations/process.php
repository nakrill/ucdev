<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['location']['id'];

    // USE LOCATION TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['location']['slug']) {
        $_POST['location']['slug'] = $_POST['location']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_events->makeRewrite($_POST['location']['slug']),
        'active'            => (int)$_POST['location']['active'],
        'title'             => $_POST['location']['title'],
        'meta_title'        => $_POST['location']['meta_title'],
        'meta_description'  => $_POST['location']['meta_description'],
        'meta_keywords'     => $_POST['location']['meta_keywords']
    );

    // HAVE LOCATION ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_events->tables['locations']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO LOCATION ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_events->tables['locations']. "` SET " .uccms_createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Location', 'Location added!');
        } else {
            $admin->growl('Location', 'Location updated!');
        }

    // QUERY FAILED
    } else {
        $admin->growl('Location', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'locations/edit/?id=' .$id);

?>