<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Events', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #items header span, #items .item section {
        text-align: left;
    }

    #items .event_title {
        width: 300px;
    }
    #items .event_date {
        width: 150px;
    }
    #items .event_categories {
        width: 160px;
    }
    #items .event_tags {
        width: 200px;
    }
    #items .event_account {
        width: 130px;
    }
    #items .event_status {
        width: 90px;
    }
    #items .event_edit {
        width: 55px;
    }
    #items .event_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#events .status select').change(function() {
            $('#form_events').submit();
        });

    });

</script>

<div id="events">

    <form id="form_events">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <div class="status contain">
        <? /*
        <div style="float: right; padding-left: 10px;">
            <select name="status">
                <option value="">Status (All)</option>
                <option value="published" <?php if ($_REQUEST['status'] == 'published') { ?>selected="selected"<?php } ?>>Published</option>
                <option value="draft" <?php if ($_REQUEST['status'] == 'draft') { ?>selected="selected"<?php } ?>>Draft</option>
            </select>
        </div>
        */ ?>
        <div style="float: right; padding-left: 10px;">
            <select name="category_id">
                <option value="">Category (All)</option>
                <?php
                $cat_query = "SELECT `id`, `title` FROM `" .$_uccms_events->tables['categories']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                $cat_q = sqlquery($cat_query);
                while ($cat = sqlfetch($cat_q)) {
                    ?>
                    <option value="<?php echo $cat['id']; ?>" <?php if ($cat['id'] == $_REQUEST['category_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($cat['title']); ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Events</h2>
            <a class="add_resource add" href="./edit/"><span></span>Add Event</a>
        </summary>
        <header style="clear: both;">
            <span class="event_title">Title</span>
            <span class="event_date">Date</span>
            <span class="event_categories">Categories</span>
            <span class="event_account">Account</span>
            <span class="event_status">Status</span>
            <span class="event_edit">Edit</span>
            <span class="event_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/events/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_events->Extension?>/ajax/admin/events/get-page/?page=1&status=" +escape($('#events .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>&tag_id=<?=$_REQUEST['tag_id']?>&account_id=<?=$_REQUEST['account_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_events->Extension?>/ajax/admin/events/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=" +escape($('#events .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>&tag_id=<?=$_REQUEST['tag_id']?>&account_id=<?=$_REQUEST['account_id']?>");
        return false;
    });
</script>