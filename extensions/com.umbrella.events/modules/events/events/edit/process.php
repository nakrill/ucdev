<?php

//echo print_r($_POST);
//exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['event']['id'];

    // HAVE ID
    if ($id) {

        // GET CURRENT INFO
        $event_query = "SELECT * FROM `" .$_uccms_events->tables['events']. "` WHERE (`id`=" .$id. ")";
        $event_q = sqlquery($event_query);
        $event = sqlfetch($event_q);

    }

    // USE EVENT TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['event']['slug']) {
        $_POST['event']['slug'] = $_POST['event']['title'];
    }

    // START DATE / TIME
    if ($_POST['event']['start_date']) {
        $dt_start = date('Y-m-d H:i:s', strtotime($_POST['event']['start_date']. ' ' .$_POST['event']['start_time']));
    } else {
        $dt_start = '0000-00-00 00:00:00';
    }

    // END DATE / TIME
    if ($_POST['event']['end_date']) {
        $dt_end = date('Y-m-d H:i:s', strtotime($_POST['event']['end_date']. ' ' .$_POST['event']['end_time']));
    } else {
        $dt_end = '0000-00-00 00:00:00';
    }

    $recurring_byday = '';
    $recurring_bymonthday = 0;

    // FREQ - WEEKLY
    if ($_POST['event']['recurring_freq'] == 'week') {
        $recurring_byday = implode(',', $_POST['recurring_days']);

    // FREQ - MONTHLY
    } else if ($_POST['event']['recurring_freq'] == 'month') {
        if ($_POST['event']['recurring_by'] == 'dotw') {
            $dowa = $_uccms_events->daysOfWeek();
            $recurring_byday = $dowa[date('N', strtotime($dt_start))]['code'];
        } else {
            $recurring_bymonthday = date('j', strtotime($dt_start));
        }
    }

    // RECURRING END - ON END DATE
    if ($_POST['event']['recurring_end'] == 0) {
        if (!$_POST['event']['end_date']) {
            $dt_end = $dt_start;
        }
    }

    // RECURRING END - AFTER COUNT
    if ($_POST['event']['recurring_end'] == 1) {
        $dt_end = '0000-00-00 ' .date('H:i:s', strtotime($_POST['event']['end_time']));
    } else {
        $_POST['event']['recurring_count'] = 0;
    }

    // RECURRING END - NEVER
    if ($_POST['event']['recurring_end'] == -1) {
        $dt_end = '0000-00-00 ' .date('H:i:s', strtotime($_POST['event']['end_time']));
        $_POST['event']['recurring_count'] = -1;
    }

    // DB COLUMNS
    $columns = array(
        'status'                    => (int)$_POST['event']['status'],
        'account_id'                => (int)$_POST['event']['account_id'],
        'dt_start'                  => $dt_start,
        'dt_end'                    => $dt_end,
        'slug'                      => $_uccms_events->makeRewrite($_POST['event']['slug']),
        'title'                     => $_POST['event']['title'],
        'description'               => $_POST['event']['description'],
        'name_contact'              => $_POST['event']['name_contact'],
        'address1'                  => $_POST['event']['address1'],
        'city'                      => $_POST['event']['city'],
        'state'                     => $_POST['event']['state'],
        'zip'                       => $_POST['event']['zip'],
        'phone'                     => $_POST['event']['phone'],
        'email'                     => $_POST['event']['email'],
        'url'                       => $_POST['event']['url'],
        'recurring'                 => (int)$_POST['event']['recurring'],
        'recurring_freq'            => $_POST['event']['recurring_freq'],
        'recurring_interval'        => (int)$_POST['event']['recurring_interval'],
        'recurring_count'           => (float)$_POST['event']['recurring_count'],
        'recurring_byday'           => $recurring_byday,
        'recurring_bymonthday'      => $recurring_bymonthday,
        'meta_title'                => $_POST['event']['meta_title'],
        'meta_description'          => $_POST['event']['meta_description'],
        'meta_keywords'             => $_POST['event']['meta_keywords'],
        'dt_saved'                  => date('Y-m-d H:i:s')
    );

    // HAVE EVENT ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_events->tables['events']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO ITEM ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_events->tables['events']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Event', 'Event saved!');

        ###########################
        # CATEGORIES
        ###########################

        // HAVE CATEGORIES
        if (count($_POST['category']) > 0) {

            // NEW CATEGORIES
            $ncata = $_POST['category']['new'];
            unset($_POST['category']['new']);

            // HAVE NEW CATEGORIES
            if (count($ncata) > 0) {
                foreach ($ncata as $nid) {

                    unset($title);
                    unset($new_cat_id);

                    // TITLE
                    $title = $_POST['new_category_title'][$nid];

                    // HAVE TITLE
                    if ($title) {

                        $columns = array(
                            'slug'      => $_uccms_events->makeRewrite($title),
                            'active'    => 1,
                            'title'     => $title
                        );

                        $cat_query = "INSERT INTO `" .$_uccms_events->tables['categories']. "` SET " .uccms_createSet($columns);
                        sqlquery($cat_query);

                        $new_cat_id = sqlid();

                        if ($new_cat_id) {
                            $_POST['category'][$new_cat_id] = 1;
                        }

                    }
                }
            }

            $cata = array();

            // GET CURRENT RELATIONS
            $query = "SELECT `category_id` FROM `" .$_uccms_events->tables['event_categories']. "` WHERE (`event_id`=" .$id. ")";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $cata[$row['category_id']] = $row['category_id'];
            }

            // LOOP
            foreach ($_POST['category'] as $category_id => $selected) {

                // CLEAN UP
                $category_id = (int)$category_id;

                // HAVE CATEGORY ID
                if ($category_id) {

                    // NOT IN EXISTING CATEGORY RELATIONS
                    if (!$cata[$category_id]) {

                        // DB COLUMNS
                        $columns = array(
                            'event_id'      => $id,
                            'category_id'   => $category_id
                        );

                        // ADD RELATIONSHIP TO DB
                        $query = "INSERT INTO `" .$_uccms_events->tables['event_categories']. "` SET " .uccms_createSet($columns). "";
                        sqlquery($query);

                    }

                    // REMOVE FROM RELATIONS
                    unset($cata[$category_id]);

                }

            }

            // HAVE LEFT OVER (OLD) RELATIONS
            if (count($cata) > 0) {

                // LOOP
                foreach ($cata as $category_id) {

                    // REMOVE RELATIONSHIP FROM DB
                    $query = "DELETE FROM `" .$_uccms_events->tables['event_categories']. "` WHERE (`event_id`=" .$id. ") AND (`category_id`=" .$category_id. ")";
                    sqlquery($query);

                }

            }

        // NO CATEGORIES
        } else {
            $cat_query = "DELETE FROM `" .$_uccms_events->tables['event_categories']. "` WHERE (`event_id`=" .$id. ")";
            sqlquery($cat_query);
        }

        ###########################
        # TAGS
        ###########################

        // HAVE TAGS
        if (count($_POST['tag']) > 0) {

            // NEW TAGS
            $ntaga = $_POST['tag']['new'];
            unset($_POST['tag']['new']);

            // HAVE NEW TAGS
            if (count($ntaga) > 0) {
                foreach ($ntaga as $nid) {

                    unset($title);
                    unset($new_tag_id);

                    // TITLE
                    $title = $_POST['new_tag_title'][$nid];

                    // HAVE TITLE
                    if ($title) {

                        $columns = array(
                            'slug'      => $_uccms_events->makeRewrite($title),
                            'active'    => 1,
                            'title'     => $title
                        );

                        $cat_query = "INSERT INTO `" .$_uccms_events->tables['tags']. "` SET " .uccms_createSet($columns);
                        sqlquery($cat_query);

                        $new_tag_id = sqlid();

                        if ($new_tag_id) {
                            $_POST['tag'][$new_tag_id] = 1;
                        }

                    }
                }
            }

            $taga = array();

            // GET CURRENT RELATIONS
            $query = "SELECT `tag_id` FROM `" .$_uccms_events->tables['event_tags']. "` WHERE (`event_id`=" .$id. ")";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $taga[$row['tag_id']] = $row['tag_id'];
            }

            // LOOP
            foreach ($_POST['tag'] as $tag_id => $selected) {

                // CLEAN UP
                $tag_id = (int)$tag_id;

                // HAVE TAG ID
                if ($tag_id) {

                    // NOT IN EXISTING tag RELATIONS
                    if (!$taga[$tag_id]) {

                        // DB COLUMNS
                        $columns = array(
                            'event_id' => $id,
                            'tag_id'   => $tag_id
                        );

                        // ADD RELATIONSHIP TO DB
                        $query = "INSERT INTO `" .$_uccms_events->tables['event_tags']. "` SET " .uccms_createSet($columns). "";
                        sqlquery($query);

                    }

                    // REMOVE FROM RELATIONS
                    unset($taga[$tag_id]);

                }

            }

            // HAVE LEFT OVER (OLD) RELATIONS
            if (count($taga) > 0) {

                // LOOP
                foreach ($taga as $tag_id) {

                    // REMOVE RELATIONSHIP FROM DB
                    $query = "DELETE FROM `" .$_uccms_events->tables['event_tags']. "` WHERE (`event_id`=" .$id. ") AND (`tag_id`=" .$tag_id. ")";
                    sqlquery($query);

                }

            }

        // NO TAGS
        } else {
            $cat_query = "DELETE FROM `" .$_uccms_events->tables['event_tags']. "` WHERE (`event_id`=" .$id. ")";
            sqlquery($cat_query);
        }

        ###########################
        # LOCATIONS
        ###########################

        // HAVE LOCATIONS
        if (count($_POST['location']) > 0) {

            // NEW LOCATIONS
            $nlocationa = $_POST['location']['new'];
            unset($_POST['location']['new']);

            // HAVE NEW LOCATIONS
            if (count($nlocationa) > 0) {
                foreach ($nlocationa as $nid) {

                    unset($title);
                    unset($new_location_id);

                    // TITLE
                    $title = $_POST['new_location_title'][$nid];

                    // HAVE TITLE
                    if ($title) {

                        $columns = array(
                            'slug'      => $_uccms_events->makeRewrite($title),
                            'active'    => 1,
                            'title'     => $title
                        );

                        $cat_query = "INSERT INTO `" .$_uccms_events->tables['locations']. "` SET " .uccms_createSet($columns);
                        sqlquery($cat_query);

                        $new_location_id = sqlid();

                        if ($new_location_id) {
                            $_POST['location'][$new_location_id] = 1;
                        }

                    }
                }
            }

            $locationa = array();

            // GET CURRENT RELATIONS
            $query = "SELECT `location_id` FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`event_id`=" .$id. ")";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $locationa[$row['location_id']] = $row['location_id'];
            }

            // LOOP
            foreach ($_POST['location'] as $location_id => $selected) {

                // CLEAN UP
                $location_id = (int)$location_id;

                // HAVE LOCATION ID
                if ($location_id) {

                    // NOT IN EXISTING LOCATION RELATIONS
                    if (!$locationa[$location_id]) {

                        // DB COLUMNS
                        $columns = array(
                            'event_id'      => $id,
                            'location_id'   => $location_id
                        );

                        // ADD RELATIONSHIP TO DB
                        $query = "INSERT INTO `" .$_uccms_events->tables['event_locations']. "` SET " .uccms_createSet($columns). "";
                        sqlquery($query);

                    }

                    // REMOVE FROM RELATIONS
                    unset($locationa[$location_id]);

                }

            }

            // HAVE LEFT OVER (OLD) RELATIONS
            if (count($locationa) > 0) {

                // LOOP
                foreach ($locationa as $location_id) {

                    // REMOVE RELATIONSHIP FROM DB
                    $query = "DELETE FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`event_id`=" .$id. ") AND (`location_id`=" .$location_id. ")";
                    sqlquery($query);

                }

            }

        // NO LOCATIONS
        } else {
            $cat_query = "DELETE FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`event_id`=" .$id. ")";
            sqlquery($cat_query);
        }

        ###########################
        # E-COMMERCE ITEMS
        ###########################

        // GET EVENT'S EXISTING E-COMMERCE ITEMS
        $eeia = $_uccms_events->event_getEcommerceItems($id);

        // LEFTOVER EVENT E-COMMERCE ITEMS
        $leia = $eeia;

        // HAVE SPECIFIED
        if (count($_POST['event']['ecommerce']['item_id']) > 0) {

            $si = 0;

            // LOOP
            foreach ($_POST['event']['ecommerce']['item_id'] as $item_id) {

                $item_id = (int)$item_id;

                // HAVE ITEM ID
                if ($item_id) {

                    // EVENT ALREADY HAS ITEM
                    if ($eeia[$item_id]) {

                        $ucolumns = array(
                            'sort'  => $si
                        );

                        // UPDATE
                        $update_query = "UPDATE `" .$_uccms_events->tables['event_ecommerce_items']. "` SET " .uccms_createSet($ucolumns). " WHERE (`id`=" .$eeia[$item_id]['id']. ")";
                        sqlquery($update_query);

                        unset($leia[$item_id]);

                    // NEW ITEM FOR event
                    } else {

                        $ncolumns = array(
                            'event_id'          => $id,
                            'ecommerce_item_id' => $item_id,
                            'sort'              => $si
                        );

                        // ADD TO DB
                        $new_query = "INSERT INTO `" .$_uccms_events->tables['event_ecommerce_items']. "` SET " .uccms_createSet($ncolumns). "";
                        sqlquery($new_query);

                    }

                    $si++;

                }

            }

        }

        // HAVE LEFTOVERS
        if (count($leia) > 0) {

            // LOOP
            foreach ($leia as $beitem) {

                // DELETE FROM DB
                $delete_query = "DELETE FROM `" .$_uccms_events->tables['event_ecommerce_items']. "` WHERE (`id`=" .$beitem['id']. ")";
                sqlquery($delete_query);

            }

        }

        unset($eeia, $leia);


    // QUERY FAILED
    } else {
        $admin->growl('Event', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT. 'events/edit/?id=' .$id);

?>