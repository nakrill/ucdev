<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET EVENT INFO
    $event_query = "SELECT * FROM `" .$_uccms_events->tables['events']. "` WHERE (`id`=" .$id. ")";
    $event_q = sqlquery($event_query);
    $event = sqlfetch($event_q);

    // HAVE EVENT ID
    if ($event['id']) {

    }

}

// GET FRONTEND ACCOUNTS
$account_query = "
SELECT CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, a.*, ad.*
FROM `uccms_accounts` AS `a`
INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
ORDER BY `fullname` ASC, `email` ASC, a.id ASC
";
$account_q = sqlquery($account_query);
while ($account = sqlfetch($account_q)) {
    $accta[$account['id']] = $account;
}

// ARRAYS
if (!is_array($accta)) $accta = array();

// SET DEFAULT DATE FORMAT IF NONE SPECIFIED
if (!$bigtree["config"]["date_format"]) {
    $bigtree["config"]["date_format"] = 'm/d/Y';
}

?>

<style type="text/css">
    #col_left {

    }

    #col_right > .contain {
        padding: 15px;
        background-color: #eee;
    }
    #col_right h4 {
        color: #444;
        text-transform: uppercase;
    }
    #col_right h4 a {
        color: #888;
    }
    #col_right h4 a.active {
        color: #444;
    }

    #col_right .form-group .date_picker_icon, #col_right .form-group .time_picker_icon {
        right: 25px;
    }

    #col_right .repeats {
        margin-top: 15px;
    }
    #col_right .repeats > fieldset {
        font-weight: 600;
        font-size: 14px;
    }
    #col_right .repeats [name="event[recurring_interval]"] {
        display: inline-block;
        width: 75%;
    }
    #col_right .repeats [name="event[recurring_count]"] {
        display: inline-block;
        width: 80px;
    }
    #col_right .repeats .repeats_end .radio_button {
        margin-top: 4px;
    }
    #col_right .repeats .repeats_end .num-occurances .radio_button {
        margin-top: 10px;
    }

    #col_right .images {
        margin-top: 20px;
    }
    #col_right .images .items {
        margin-top: 10px;
    }
    #col_right .images .item {
        float: left;
        position: relative;
        width: 45%;
        margin-right: 5%;
    }
    #col_right .images .item.main {
        float: none;
        width: 100%;
        margin: 0px;
    }
    #col_right .images .item img {
        width: 100%;
        height: auto;
    }
    #col_right .images .item .buttons {
        display: none;
        position: absolute;
        top: 5px;
        right: 5px;
    }
    #col_right .images .item .buttons a {
        color: #fff;
        text-shadow: 0 0 2px #777;
    }
    #col_right .images .item .buttons a:hover {
        color: #222;
    }
    #col_right .images .item:hover .buttons {
        display: block;
    }

    #cats_tags {
        margin-top: 12px;
    }
    #cats_tags .switch {
        padding: 10px;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    #cats_tags .switch .item {
        padding: 4px 0;
        font-size: .9em;
    }
    #cats_tags .switch .checkbox {
    }
    #cats_tags .add .add_link {
        text-align: center;
    }
    #cats_tags .add .add_form button {
        width: 100%;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
    }

    #event_content {
        height: 300px;
    }

</style>

<script type="text/javascript">
    $(function() {

        var new_cat_id = 0;

        // REPEATS TOGGLE
        $('#repeats_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('.repeats_enabled_content').show();
            } else {
                $('.repeats_enabled_content').hide();
            }
        });

        // REPEATS FREQUENCY CHANGE
        $('.repeats_enabled_content select[name="event[recurring_freq]"]').change(function() {
            var freq = $(this).val();
            $('.freq_content_container .freq_content').hide();
            $('.freq_content_container .freq_content.' +freq).show();
            $('.repeats_enabled_content .repeat_every .re').hide();
            $('.repeats_enabled_content .repeat_every .re.' +freq).show();
        });

        // ADD IMAGE CLICK
        $('#col_right .images .add').click(function(e) {
            e.preventDefault();
            $('#add_image_container').show();
            $('html, body').animate({
                scrollTop: $('#add_image_container').offset().top
            }, 700);
        });

        // EDIT IMAGE CLICK
        $('#col_right .images .item .buttons .edit').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('html, body').animate({
                scrollTop: $('#image-' +id).offset().top
            }, 700);
        });

        // DELETE IMAGE CLICK
        $('#col_right .images .item .buttons .delete').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('html, body').animate({
                scrollTop: $('#image-' +id).offset().top
            }, 700);
        });

        // CATEGORY / TAG SWITCH
        $('#cats_tags h4 a').click(function(e) {
            e.preventDefault();
            var what = $(this).attr('data-what');
            $('#cats_tags h4 a').removeClass('active');
            $(this).addClass('active');
            $('#cats_tags .switch').hide();
            $('#cats_tags .switch.' +what).show();
        });

        // CATEGORY / TAG ADD CLICK
        $('#cats_tags .add .add_link a').click(function(e) {
            e.preventDefault();
            var what = $(this).attr('data-what');
            $('#cats_tags .switch.' +what+ ' .add .add_link').hide();
            $('#cats_tags .switch.' +what+ ' .add .add_form').show();
            $('#cats_tags .switch.' +what+ ' .add .add_form input[type="text"]').focus();
        });

        // CATEGORY / TAG ADD
        $('#cats_tags .add .add_form button').click(function(e) {
            e.preventDefault();
            var what = $(this).attr('data-what');
            if (what == 'categories') {
                var what_singular = 'category';
            } else if (what == 'tags') {
                var what_singular = 'tag';
            } else if (what == 'locations') {
                var what_singular = 'location';
            }
            new_cat_id++;
            $('#cats_tags .switch.' +what+ ' .items').append('<input type="hidden" name="new_' +what_singular+ '_title[' +new_cat_id+ ']" value="' +$('#cats_tags .switch.' +what+ ' .add .add_form input[name="add_title"]').val()+ '" /><div class="item contain"><input type="checkbox" name="' +what_singular+ '[new][]" value="' +new_cat_id+ '" checked="checked" /> ' +$('#cats_tags .switch.' +what+ ' .add .add_form input[name="add_title"]').val()+ '</div>');
            BigTreeCustomControls();
            $('#cats_tags .switch.' +what+ ' .add .add_form').hide();
            $('#cats_tags .switch.' +what+ ' .add .add_link').show();
            $('#cats_tags .switch.' +what+ ' .add .add_form input[name="add_title"]').val('');
        });

    });
</script>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="event[id]" value="<?php echo $event['id']; ?>" />

<h3><?php if ($event['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Event</h3>

<div class="contain">

    <div class="row">

        <div id="col_left" class="col-xl-8">

            <fieldset class="form-group">
                <label>Title</label>
                <input type="text" name="event[title]" value="<?php echo stripslashes($event['title']); ?>" class="form-control" />
            </fieldset>

            <fieldset class="form-group">
                <label>Description</label>
                <div>
                    <?php
                    $field = array(
                        'key'       => 'event[description]', // The value you should use for the "name" attribute of your form field
                        'value'     => stripslashes($event['description']), // The existing value for this form field
                        'id'        => 'event_description', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            //'simple' => true
                        )
                    );
                    include(BigTree::path('admin/form-field-types/draw/html.php'));
                    ?>
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label>Contact Name</label>
                <input type="text" value="<?=$event['name_contact']?>" name="event[name_contact]" class="form-control" />
            </fieldset>

            <fieldset class="form-group">
                <label>Address</label>
                <input type="text" value="<?=$event['address1']?>" name="event[address1]" class="form-control" />
            </fieldset>

            <fieldset class="form-group">
                <label>City</label>
                <input type="text" value="<?=$event['city']?>" name="event[city]" class="form-control" />
            </fieldset>

            <fieldset class="form-group">
                <label>State</label>
                <select name="event[state]" class="custom_control form-control">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?=$state_code?>" <?php if ($state_code == $event['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset class="form-group">
                <label>Zip</label>
                <input type="text" value="<?=$event['zip']?>" name="event[zip]" class="form-control" />
            </fieldset>

            <fieldset class="form-group">
                <label>Phone</label>
                <input type="text" value="<?=$event['phone']?>" name="event[phone]" class="form-control" />
            </fieldset>

            <fieldset class="form-group">
                <label>Email</label>
                <input type="text" value="<?=$event['email']?>" name="event[email]" class="form-control" />
            </fieldset>

            <fieldset class="form-group">
                <label>Link</label>
                <input type="text" value="<?=$event['url']?>" name="event[url]" placeholder="www.domain.com" class="form-control" />
            </fieldset>

            <div class="contain">
                <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
                <div class="contain uccms_toggle-seo" style="display: none;">

                    <fieldset class="form-group">
                        <label>URL</label>
                        <input type="text" name="event[slug]" value="<?php echo stripslashes($event['slug']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Meta Title</label>
                        <input type="text" name="event[meta_title]" value="<?php echo stripslashes($event['meta_title']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Meta Description</label>
                        <textarea name="event[meta_description]" class="form-control" style="height: 64px;"><?php echo stripslashes($event['meta_description']); ?></textarea>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Meta Keywords</label>
                        <textarea name="event[meta_keywords]" class="form-control" style="height: 64px;"><?php echo stripslashes($event['meta_keywords']); ?></textarea>
                    </fieldset>

                </div>
            </div>

        </div>

        <div id="col_right" class="col-xl-4">
            <div class="contain">

                <fieldset class="form-group">
                    <label>Status</label>
                    <select name="event[status]" class="custom_control form-control">
                        <?php foreach ($_uccms_events->statuses as $status_id => $status_name) { ?>
                            <option value="<?php echo $status_id; ?>" <?php if ($status_id == $event['status']) { ?>selected="selected"<?php } ?>><?php echo $status_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="form-group">
                    <label>Account</label>
                    <select name="event[account_id]" class="custom_control form-control">
                        <option value="0">None</option>
                        <?php foreach ($accta as $account_id => $account) { ?>
                            <option value="<?php echo $account_id; ?>" <?php if ($account_id == $event['account_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($account['email']); ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="event_date form-group">
                    <label>Starts<span style="color: #ff0000;">*</span></label>
                    <div class="row">
                        <div class="col-md-6">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'event_start_date',
                                'key'       => 'event[start_date]',
                                'value'     => $event['dt_start'],
                                'required'  => true,
                                'options'   => array(
                                    'default_today' => false
                                )
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/date.php'));

                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'event_start_time',
                                'key'       => 'event[start_time]',
                                'value'     => $event['dt_start'],
                                'required'  => true
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/time.php'));

                            ?>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="event_date form-group">
                    <label>Ends</label>
                    <div class="row">
                        <div class="col-md-6">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'event_end_date',
                                'key'       => 'event[end_date]',
                                'value'     => (substr($event['dt_end'], 0, 10) == '0000-00-00' ? '' : $event['dt_end']),
                                'required'  => false,
                                'options'   => array(
                                    'default_today' => false
                                )
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/date.php'));

                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'event_end_time',
                                'key'       => 'event[end_time]',
                                'value'     => $event['dt_end'],
                                'required'  => false
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/time.php'));

                            ?>
                        </div>
                    </div>
                </fieldset>

                <div class="repeats">

                    <fieldset class="form-group">
                        <input id="repeats_enabled" type="checkbox" name="event[recurring]" value="1" <?php if ($event['recurring']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Repeats</label>
                    </fieldset>

                    <div class="repeats_enabled_content" style="<?php if (!$event['recurring']) { echo ' display: none;'; } ?>">

                        <fieldset class="form-group">
                            <select name="event[recurring_freq]" class="custom_control form-control">
                                <option value="day" <?php if ($event['recurring_freq'] == 'day') { ?>selected="selected"<?php } ?>>Daily</option>
                                <option value="week" <?php if ($event['recurring_freq'] == 'week') { ?>selected="selected"<?php } ?>>Weekly</option>
                                <option value="month" <?php if ($event['recurring_freq'] == 'month') { ?>selected="selected"<?php } ?>>Monthly</option>
                                <option value="year" <?php if ($event['recurring_freq'] == 'year') { ?>selected="selected"<?php } ?>>Yearly</option>
                            </select>
                        </fieldset>

                        <fieldset class="repeat_every">
                            <label>Repeat Every</label>
                            <select name="event[recurring_interval]" class="custom_control form-control">
                                <?php for ($i=1; $i<=30; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php if ($i == $event['recurring_interval']) { echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select> <span class="re day" style="<?php if (($event['recurring_freq'] != 'day') && ($event['recurring_freq'])) { ?>display: none;<?php } ?>">Days</span><span class="re week" style="<?php if ($event['recurring_freq'] != 'week') { ?>display: none;<?php } ?>">Weeks</span><span class="re month" style="<?php if ($event['recurring_freq'] != 'month') { ?>display: none;<?php } ?>">Months</span><span class="re year" style="<?php if ($event['recurring_freq'] != 'year') { ?>display: none;<?php } ?>">Years</span>
                        </fieldset>

                        <div class="freq_content_container">

                            <fieldset class="freq_content week form-group" style="<?php if ($event['recurring_freq'] != 'week') { echo 'display: none;'; } ?>">
                                <label>Repeat On</label>
                                <div class="contain">
                                    <?php
                                    $seldaya = array();
                                    if ($event['recurring_freq'] == 'week') {
                                        $seldaya = explode(',', stripslashes($event['recurring_byday']));
                                    }
                                    foreach ($_uccms_events->daysOfWeek() as $day_id => $day) {
                                        ?>
                                        <div style="float: left; <?php if ($day['code'] != 'SA') { ?>padding-right: 15px;<?php } ?>">
                                            <input type="checkbox" name="recurring_days[]" value="<?php echo $day['code']; ?>" <?php if (in_array($day['code'], $seldaya)) { ?>checked="checked"<?php } ?> /> <?php echo substr($day['title'], 0, 1); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div style="padding-top: 4px; font-size: .8em; opacity: .7;">(Start day must be selected.)</div>
                            </fieldset>

                            <fieldset class="freq_content month form-group" style="<?php if ($event['recurring_freq'] != 'month') { echo 'display: none;'; } ?>">
                                <label>Repeat By</label>
                                <div class="contain">
                                    <div style="float: left;">
                                        <input type="radio" name="event[recurring_by]" value="dotw" <?php if ($event['recurring_byday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Week</label>
                                    </div>
                                    <div style="float: left;">
                                        <input type="radio" name="event[recurring_by]" value="dotm" <?php if ($event['recurring_bymonthday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Month</label>
                                    </div>
                                </div>
                            </fieldset>

                        </div>

                        <fieldset class="repeats_end form-group">
                            <label>Ends</label>
                            <div>
                                <input type="radio" name="event[recurring_end]" value="0" <?php if (!$event['recurring_count']) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">On end date</label>
                            </div>
                            <div class="num-occurances">
                                <input type="radio" name="event[recurring_end]" value="1" <?php if ($event['recurring_count'] > 0) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">After <input type="text" name="event[recurring_count]" value="<?php if ($event['recurring_count'] > 0) { echo $event['recurring_count']; } ?>" class="form-control" /> occurances</label>
                            </div>
                            <div>
                                <input type="radio" name="event[recurring_end]" value="-1" <?php if ($event['recurring_count'] == -1) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">Never</label>
                            </div>
                        </fieldset>

                    </div>

                </div>

                <?php

                // HAVE E-COMMERCE EXTENSION
                if (class_exists('uccms_Ecommerce')) {

                    // GET EVENTS E-COMMERCE ITEMS
                    $eeia = $_uccms_events->event_getEcommerceItems($event['id']);

                    // ECOMMERCE ITEM ARRAY
                    $eia = array();

                    // GET ALL E-COMMERCE ITEMS
                    $eitem_query = "SELECT `id`, `title` FROM `uccms_ecommerce_items` ORDER BY `title` ASC";
                    $eitem_q = sqlquery($eitem_query);
                    while ($eitem = sqlfetch($eitem_q)) {
                        $eia[$eitem['id']] = $eitem;
                    }

                    ?>

                    <style type="text/css">

                        #ecommerce_items .add select {
                            display: inline-block;
                            width: calc(100% - 60px);
                        }
                        #ecommerce_items .add .button {
                            display: inline-block;
                            margin-left: 8px;
                        }
                        #ecommerce_items .items {
                            margin: 10px 0 0;
                        }
                        #ecommerce_items .items .item {
                            margin-bottom: 10px;
                            padding: 5px 10px 5px 2px;
                            border: 1px solid #ccc;
                            background-color: #fff;
                        }
                        #ecommerce_items .items .item:last-child {
                            margin-bottom: 0px;
                        }
                        #ecommerce_items .items .item .sort {
                            float: left;
                        }
                        #ecommerce_items .items .item .sort .icon_sort {
                            margin: 0px;
                        }
                        #ecommerce_items .items .item .title {
                            float: left;
                            margin-left: 5px;
                            line-height: 24px;
                            max-width: 150px;
                            height: 24px;
                            overflow: hidden;
                        }
                        #ecommerce_items .items .item .remove {
                            float: right;
                            font-size: 1.2em;
                            line-height: 24px;
                        }

                        #ecommerce_items .items .item.template {
                            display: none;
                        }

                    </style>

                    <script type="text/javascript">

                        $(document).ready(function() {

                            // SORTABLE ECOMMERCE ITEMS
                            ecommerceItemsSort();

                            // ADD ECOMMERCE ITEM
                            $('#ecommerce_items .add .button').click(function(e) {
                                e.preventDefault();
                                var eitem_id = $('#ecommerce_items .add select').val();
                                if (eitem_id) {
                                    var clone = $('#ecommerce_items .items .item.template').clone();
                                    clone.removeClass('template');
                                    clone.find('input[type="hidden"]').val(eitem_id);
                                    clone.find('.title').text($('#ecommerce_items .add select option:selected').text());
                                    clone.appendTo('#ecommerce_items .items').show();
                                    ecommerceItemsSort();
                                    $('#ecommerce_items .items').show();
                                }
                            });

                            // REMOVE ECOMMERCE ITEM
                            $('#ecommerce_items .items').on('click', '.remove a', function(e) {
                                e.preventDefault();
                                var parent = $(this).closest('.item');
                                parent.fadeOut(400, function() {
                                    parent.remove();
                                });
                            });

                        });

                        // SORTABLE ECOMMERCE ITEMS
                        function ecommerceItemsSort() {

                            // SORTABLE
                            $('#ecommerce_items .items').sortable({
                                handle: '.icon_sort',
                                axis: 'y',
                                containment: 'parent',
                                items: '.item',
                                //placeholder: 'ui-sortable-placeholder',
                                update: function() {
                                }
                            });

                        }

                    </script>

                    <div id="ecommerce_items">

                        <fieldset class="form-group">
                            <label>E-Commerce Items</label>
                            <div class="add">
                                <select class="custom_control form-control">
                                    <option value="">Select</option>
                                    <?php foreach ($eia as $eitem) { ?>
                                        <option value="<?php echo $eitem['id']; ?>"><?php echo stripslashes($eitem['title']); ?></option>
                                    <?php } ?>
                                </select> <a href="#" class="btn btn-secondary">Add</a>
                            </div>
                            <div class="items" style="<?php if (count($eeia) == 0) { echo 'display: none; '; } ?>">

                                <div class="item template clearfix">
                                    <input type="hidden" name="event[ecommerce][item_id][]" value="" />
                                    <div class="sort"><span class="icon_sort ui-sortable-handle"></span></div>
                                    <div class="title"></div>
                                    <div class="remove"><a href="#" title="Remove"><i class="fa fa-times"></i></a></div>
                                </div>

                                <?php foreach ($eeia as $eeitem) { ?>
                                    <div class="item clearfix">
                                        <input type="hidden" name="event[ecommerce][item_id][]" value="<?php echo $eeitem['ecommerce_item_id']; ?>" />
                                        <div class="sort"><span class="icon_sort ui-sortable-handle"></span></div>
                                        <div class="title"><?php echo stripslashes($eia[$eeitem['ecommerce_item_id']]['title']); ?></div>
                                        <div class="remove"><a href="#" title="Remove"><i class="fa fa-times"></i></a></div>
                                    </div>
                                <?php } ?>

                            </div>
                        </fieldset>

                    </div>

                    <?php

                }

                ?>

                <button type="submit" class="btn btn-primary" style="display: block; width: 100%; margin-top: 25px;">Save</button>

                <?php if ($event['id']) { ?>

                    <div class="images">
                        <div class="contain">
                            <h4 class="h6" style="float: left; margin: 7px 0 0;">Images</h4>
                            <a class="add btn btn-secondary" href="#" style="float: right; padding-top: 3px;"><span></span>Add Image</a>
                        </div>

                        <?php

                        // GET IMAGES
                        $image_query = "SELECT * FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`event_id`=" .$event['id']. ") ORDER BY `sort` ASC, `id` ASC";
                        $image_q = sqlquery($image_query);

                        // NUMBER OF IMAGES
                        $num_images = sqlrows($image_q);

                        // HAVE IMAGES
                        if ($num_images > 0) {

                            ?>

                            <div class="items contain">

                                <?php

                                $ii = 1;

                                // LOOP
                                while ($image = sqlfetch($image_q)) {
                                    if ($image['image']) {

                                        if ($ii == 2) {
                                            ?>
                                            </div>
                                            <div class="items thumbs contain" style="padding-top: 10px;">
                                            <?php
                                        }

                                        ?>
                                        <div class="item <?php if ($ii == 1) { echo 'main'; } ?>">
                                            <img src="<?php echo $_uccms_events->imageBridgeOut($image['image'], 'events'); ?>" alt="<?php echo $image['alt']; ?>" />
                                            <div class="buttons">
                                                <a href="#" class="edit" data-id="<?php echo $image['id']; ?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href="#" class="delete" data-id="<?php echo $image['id']; ?>"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                        <?php
                                        $ii++;
                                    }
                                }

                                ?>

                            </div>

                            <?php

                        }

                        ?>
                    </div>
                <?php } ?>

                <div id="cats_tags">
                    <h4 class="h6"><a href="#" class="active" data-what="categories">Category</a>&nbsp;&nbsp;<span style="font-weight: normal;">|</span>&nbsp;&nbsp;<a href="#" data-what="tags">Tag</a>&nbsp;&nbsp;<span style="font-weight: normal;">|</span>&nbsp;&nbsp;<a href="#" data-what="locations">Location</a></h4>
                    <div style="background-color: #fff;">

                        <div class="categories switch contain">
                            <div class="items">
                                <?php

                                // ARRAY OF CATEGORIES
                                $cata = array();

                                // GET CURRENT RELATIONS
                                $query = "SELECT `category_id` FROM `" .$_uccms_events->tables['event_categories']. "` WHERE (`event_id`=" .$id. ")";
                                $q = sqlquery($query);
                                while ($row = sqlfetch($q)) {
                                    $cata[$row['category_id']] = $row['category_id'];
                                }

                                // GET CATEGORIES
                                $query = "SELECT `id`, `title` FROM `" .$_uccms_events->tables['categories']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                                $q = sqlquery($query);
                                while ($cat = sqlfetch($q)) {
                                    ?>
                                    <div class="item contain">
                                        <input type="checkbox" name="category[<?php echo $cat['id']; ?>]" value="1" <?php if ($cata[$cat['id']]) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($cat['title']); ?>
                                    </div>
                                    <?php
                                }

                                ?>
                            </div>
                            <div class="add">
                                <div class="add_link"><a href="#" data-what="categories">Add Category</a></div>
                                <div class="add_form" style="display: none;">
                                    <input type="text" name="add_title" value="" placeholder="Category Name" class="form-control" />
                                    <button data-what="categories" class="btn btn-secondary">Add Category</button>
                                </div>
                            </div>
                        </div>

                        <div class="tags switch contain" style="display: none;">
                            <div class="items">
                                <?php

                                // ARRAY OF TAGS
                                $taga = array();

                                // GET CURRENT RELATIONS
                                $query = "SELECT `tag_id` FROM `" .$_uccms_events->tables['event_tags']. "` WHERE (`event_id`=" .$id. ")";
                                $q = sqlquery($query);
                                while ($row = sqlfetch($q)) {
                                    $taga[$row['tag_id']] = $row['tag_id'];
                                }

                                // GET TAGS
                                $query = "SELECT `id`, `title` FROM `" .$_uccms_events->tables['tags']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                                $q = sqlquery($query);
                                while ($tag = sqlfetch($q)) {
                                    ?>
                                    <div class="item contain">
                                        <input type="checkbox" name="tag[<?php echo $tag['id']; ?>]" value="1" <?php if ($taga[$tag['id']]) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($tag['title']); ?>
                                    </div>
                                    <?php
                                }

                                ?>
                            </div>
                            <div class="add">
                                <div class="add_link"><a href="#" data-what="tags">Add Tag</a></div>
                                <div class="add_form" style="display: none;">
                                    <input type="text" name="add_title" value="" placeholder="Tag" class="form-control" />
                                    <button data-what="tags" class="btn btn-secondary">Add Tag</button>
                                </div>
                            </div>
                        </div>

                        <div class="locations switch contain" style="display: none;">
                            <div class="items">
                                <?php

                                // ARRAY OF LOCATIONS
                                $locationa = array();

                                // GET CURRENT RELATIONS
                                $query = "SELECT `location_id` FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`event_id`=" .$id. ")";
                                $q = sqlquery($query);
                                while ($row = sqlfetch($q)) {
                                    $locationa[$row['location_id']] = $row['location_id'];
                                }

                                // GET LOCATIONS
                                $query = "SELECT `id`, `title` FROM `" .$_uccms_events->tables['locations']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                                $q = sqlquery($query);
                                while ($location = sqlfetch($q)) {
                                    ?>
                                    <div class="item contain">
                                        <input type="checkbox" name="location[<?php echo $location['id']; ?>]" value="1" <?php if ($locationa[$location['id']]) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($location['title']); ?>
                                    </div>
                                    <?php
                                }

                                ?>
                            </div>
                            <div class="add">
                                <div class="add_link"><a href="#" data-what="locations">Add Location</a></div>
                                <div class="add_form" style="display: none;">
                                    <input type="text" name="add_title" value="" placeholder="Location" class="form-control"/>
                                    <button data-what="locations" class="btn btn-secondary">Add Location</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

</div>

</form>

<?php if ($event['id']) { ?>

    <style type="text/css">

        #images .add_container {
            padding: 10px;
            background-color: #fafafa;
            border-bottom: 1px solid #ccc;
        }

        #images .add_container td {
            vertical-align: top;
        }

        #images .table section a, #categories.table section a {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #images .table > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }

        #images .table > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }

        #images .table > ul li section:first-child {
            padding-left: 0px;
        }

        #images .items .item .image {
            width: 510px;
            padding: 0 25px;
        }
        #images .items .item .image .image_field {
            margin-top: 5px;
        }
        #images .items .item .title {
            width: 50%;
        }
        #images .items .item .view_action {
            margin-top: 7px;
        }

        #images .table .currently .remove_resource {
            margin-top: -3px;
            margin-bottom: -10px;
        }

        #images .items .item .title fieldset label {
            margin-bottom: 2px;
            text-align: left;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function() {

            // IMAGES - SORTABLE
            $('#images ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('id').replace('row_', ''));
                    });
                    $('#images input[name="sort"]').val(sort.join());
                }
            });

            // IMAGES - ADD CLICK
            $('#images .add_resource').click(function(e) {
                e.preventDefault();
                $('#add_image_container').toggle();
            });

            // IMAGES - REMOVE CLICK
            $('#images .icon_delete').click(function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var del = $('#images input[name="delete"]').val();
                if (del) del += ',';
                del += id;
                $('#images input[name="delete"]').val(del);
                $('#images li#image-' +id).fadeOut(500);
            });

            // IMAGES - NEW FORM SUBMIT
            $('#add_image_container form').submit(function(e) {
                var c = confirm('Are you sure you want to add this image?\nDoing so will reload the page, so make sure your event is saved first.');
                return c;
            });

            // IMAGES - FORM SUBMIT
            $('#images form').submit(function(e) {
                var c = confirm('Are you sure you want to save the images?\nDoing so will reload the page, so make sure your event is saved first.');
                return c;
            });

        });

    </script>

    <div style="height: 20px; overflow: hidden;"></div>

    <a name="images"></a>

    <div id="add_image_container" style="display: none;">
        <b>Add Image</b>
        <div class="container legacy">

            <form enctype="multipart/form-data" action="./images/process/" method="post">
            <input type="hidden" name="event[id]" value="<?php echo $event['id']; ?>" />

            <table style="margin-bottom: 0px; border: 0px;">
                <tr>
                    <td style="width: 470px;">
                        <?php
                        $field = array(
                            'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                            'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                            'key'       => 'new', // The value you should use for the "name" attribute of your form field
                            'value'     => '', // The existing value for this form field
                            'id'        => 'image-new', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'image' => true
                            ),
                            'required'  => false // A boolean value of whether this form field is required or not
                        );
                        include(BigTree::path('admin/form-field-types/draw/upload.php'));
                        ?>
                    </td>
                    <td>
                        <input type="text" name="new-caption" value="" placeholder="Caption / Alt Text" class="form-control" />
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </td>
                </tr>
            </table>

            </form>

        </div>
    </div>

    <div id="images" class="container" style="margin-bottom: 0px; border: 0px none;">

        <form enctype="multipart/form-data" action="./images/process/" method="post">
        <input type="hidden" name="event[id]" value="<?php echo $event['id']; ?>" />

        <div class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Images</h2>
                <a class="add_resource add" href="#"><span></span>Add Image</a>
            </summary>

            <?php

            // GET IMAGES
            $image_query = "SELECT * FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`event_id`=" .$event['id']. ") ORDER BY `sort` ASC, `id` ASC";
            $image_q = sqlquery($image_query);

            // NUMBER OF IMAGES
            $num_images = sqlrows($image_q);

            // HAVE IMAGES
            if ($num_images > 0) {

                ?>

                <ul class="items ui-sortable">

                    <?php

                    // LOOP
                    while ($image = sqlfetch($image_q)) {

                        ?>

                        <li id="image-<?php echo $image['id']; ?>" class="item contain">
                            <section class="sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="image">
                                <?php
                                $field = array(
                                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                    'key'       => 'image-' .$image['id'], // The value you should use for the "name" attribute of your form field
                                    'value'     => ($image['image'] ? $_uccms_events->imageBridgeOut($image['image'], 'events') : ''), // The existing value for this form field
                                    'id'        => 'image-' .$image['id'], // A unique ID you can assign to your form field for use in JavaScript
                                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                    'options'   => array(
                                        'image' => true
                                    ),
                                    'required'  => false // A boolean value of whether this form field is required or not
                                );
                                include(BigTree::path('admin/form-field-types/draw/upload.php'));
                                ?>
                            </section>
                            <section class="title">
                                <input type="text" name="image-<?php echo $image['id']; ?>-caption" value="<?php echo stripslashes($image['caption']); ?>" placeholder="Caption / Alt Text" class="form-control" />
                            </section>
                            <section class="view_action">
                                <a class="icon_delete" href="#" data-id="<?php echo $image['id']; ?>"></a>
                            </section>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <footer>
                    <input class="blue" type="submit" value="Save" />
                </footer>

                <?php

            } else {

                ?>

                <div style="padding: 10px; text-align: center;">
                    No item images added yet.
                </div>

                <?php

            }

            ?>

        </div>

        </form>

    </div>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>