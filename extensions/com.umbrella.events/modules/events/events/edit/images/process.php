<?php

/*
echo print_r($_FILES);
echo print_r($_POST);
exit;
*/

// CLEAN UP
$id = (int)$_POST['event']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE EVENT ID
    if ($id) {

        // HAVE IMAGES
        if (count($_FILES) > 0) {

            // GET EVENT INFO
            $event_query = "SELECT * FROM `" .$_uccms_events->tables['events']. "` WHERE (`id`=" .$id. ")";
            $event_q = sqlquery($event_query);
            $event = sqlfetch($event_q);

            // LOOP THROUGH IMAGES
            foreach ($_FILES as $image_field => $values) {

                // NOT NEW
                if ($image_field != 'new') {

                    // CLEAN UP
                    $image_id = (int)str_replace('image-', '', $image_field);

                    // HAVE IMAGE ID
                    if ($image_id) {

                        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
                        if (($_FILES[$image_field]['name']) || ((!$_POST[$image_field]) || (substr($_POST[$image_field], 0, 11) == 'resource://'))) {

                            // GET CURRENT IMAGE
                            $ex_query = "SELECT `image` FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`id`=" .$image_id. ")";
                            $ex = sqlfetch(sqlquery($ex_query));

                            // THERE'S AN EXISTING IMAGE
                            if ($ex['image']) {

                                // REMOVE IMAGES
                                @unlink($_uccms_events->imageBridgeOut($ex['image'], 'events', true));
                                @unlink(BigTree::prefixFile($_uccms_events->imageBridgeOut($ex['image'], 'events', true), 't_'));

                                // UPDATE DATABASE
                                $query = "UPDATE `" .$_uccms_events->tables['event_images']. "` SET `image`='' WHERE (`id`=" .$image_id. ")";
                                sqlquery($query);

                            }

                        }

                    }

                }

                // FILE UPLOADED / SELECTED
                if (($_FILES[$image_field]['name']) || ($_POST[$image_field])) {

                    // BIGTREE UPLOAD FIELD INFO
                    $field = array(
                        'type'          => 'upload',
                        'title'         => 'Image',
                        'key'           => $image_field,
                        'options'       => array(
                            'directory' => 'extensions/' .$_uccms_events->Extension. '/files/events',
                            'image' => true,
                            'thumbs' => array(
                                array(
                                    'width'     => '1024', // MATTD: make controlled through settings
                                    'height'    => '1024' // MATTD: make controlled through settings
                                ),
                                array(
                                    'prefix'    => 't_',
                                    'width'     => '480', // MATTD: make controlled through settings
                                    'height'    => '480' // MATTD: make controlled through settings
                                )
                            ),
                            'crops' => array()
                        )
                    );

                    // UPLOADED FILE
                    if ($_FILES[$image_field]['name']) {
                        $field['file_input'] = $_FILES[$image_field];

                    // FILE FROM MEDIA BROWSER
                    } else if ($_POST[$image_field]) {
                        $field['input'] = $_POST[$image_field];
                    }

                    // DIGITAL ASSET MANAGER VARS
                    $field['dam_vars'] = array(
                        'website_extension'         => $_uccms_events->Extension,
                        'website_extension_item'    => 'event',
                        'website_extension_item_id' => $id,
                        'title'                     => stripslashes($event['title'])
                    );

                    if ($_POST[$image_field. '-caption']) {
                        $field['dam_vars']['caption'] = $_POST[$image_field. '-caption'];
                    }

                    // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
                    $file_path = BigTreeAdmin::processField($field);

                    // UPLOAD SUCCESSFUL
                    if ($file_path) {

                        // DB COLUMNS
                        $columns = array(
                            'event_id'      => $id,
                            'sort'          => 999,
                            'image'         => $_uccms_events->imageBridgeIn($file_path),
                            'caption'       => $_POST[$image_field. '-caption']
                        );

                        // NEW IMAGE
                        if ($image_field == 'new') {

                            // INSERT INTO DB
                            $query = "INSERT INTO `" .$_uccms_events->tables['event_images']. "` SET " .uccms_createSet($columns). "";

                        // EXISTING IMAGE
                        } else if ($image_id) {

                            // UPDATE DB
                            $query = "UPDATE `" .$_uccms_events->tables['event_images']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$image_id. ")";

                        }

                        // RUN QUERY
                        if ($query) {

                            sqlquery($query);

                            $admin->growl('Image', 'Image added!');

                        }

                    // UPLOAD FAILED
                    } else {

                        echo print_r($bigtree['errors']);
                        exit;

                        $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

                    }

                }



            }

        }

        // SORT ARRAY
        $sorta = explode(',', $_POST['sort']);

        // ARE DELETING
        if ($_POST['delete']) {

            // GET ID'S TO DELETE
            $ida = explode(',', $_POST['delete']);

            // HAVE ID'S
            if (count($ida) > 0) {

                // LOOP
                foreach ($ida as $image_id) {

                    // CLEAN UP
                    $image_id = (int)$image_id;

                    // HAVE VALID ID
                    if ($image_id) {

                        // GET CURRENT IMAGE
                        $ex_query = "SELECT `image` FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`id`=" .$image_id. ")";
                        $ex = sqlfetch(sqlquery($ex_query));

                        // THERE'S AN EXISTING IMAGE
                        if ($ex['image']) {

                            // REMOVE IMAGES
                            @unlink($_uccms_events->imageBridgeOut($ex['image'], 'events', true));
                            @unlink(BigTree::prefixFile($_uccms_events->imageBridgeOut($ex['image'], 'events', true), 't_'));

                        }

                        // REMOVE FROM DATABASE
                        $query = "DELETE FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`id`=" .$image_id. ")";
                        sqlquery($query);

                        // REMOVE FROM SORT ARRAY
                        if (($key = array_search($image_id, $sorta)) !== false) {
                            unset($sorta[$key]);
                        }


                    }

                }

            }

        }

        // HAVE IMAGES TO SORT
        if (count($sorta) > 0) {

            $i = 0;

            // LOOP
            foreach ($sorta as $image_id) {

                // CLEAN UP
                $image_id = (int)str_replace('image-', '', $image_id);

                // HAVE VALID ID
                if ($image_id) {

                    // UPDATE DATABASE
                    $query = "UPDATE `" .$_uccms_events->tables['event_images']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$image_id. ")";
                    sqlquery($query);

                    $i++;

                }

            }

        }

    // EVENT ID NOT SPECIFIED
    } else {
        $admin->growl('Event', 'Event ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'events/edit/?id=' .$id. '#images');

?>