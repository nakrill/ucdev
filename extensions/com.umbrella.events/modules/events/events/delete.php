<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE BUSINESS
    $query = "UPDATE `" .$_uccms_events->tables['events']. "` SET " .uccms_createSet($columns). ", `dt_deleted`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Event', 'Event deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Event', 'Failed to delete event.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Event', 'No event specified.');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT.'events/');
}

?>