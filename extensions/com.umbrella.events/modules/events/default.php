<?php

// INCLUDE COMPOSER
include_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

use When\When;

?>

<style type="text/css">

    #events_dashboard .col_left {
        width: 100%;
        padding-top: 0px;
        padding-left: 0px;
        vertical-align: top;
    }

    #events_dashboard .col_right {
        margin-left: 30px;
        border-left: 1px solid #ccc;
        background-color: #f5f5f5;
    }
    #events_dashboard .col_right .size {
        width: 200px;
        min-height: 400px;
        padding: 0 15px;
    }

    #events_dashboard .toggle_bar {
        padding: 15px 8px;
        background-color: #f8f8f8;
    }
    #events_dashboard .toggle_bar ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #events_dashboard .toggle_bar ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 10px;
        font-weight: bold;
        text-transform: uppercase;
    }
    #events_dashboard .toggle_bar ul li a {
        color: #aaa;
    }
    #events_dashboard .toggle_bar ul li a:hover, #events_dashboard .toggle_bar ul li a.active {
        color: #333;
    }
    #events_dashboard .toggle_bar ul li a .num {
        font-size: .9em;
        font-weight: normal;
        color: #aaa;
    }

    #events_dashboard .toggle_content_container {
    }
    #events_dashboard .toggle_content_container .toggle {
        display: none;
    }
    #events_dashboard .toggle_content_container .toggle:first-child {
        display: block;
    }
    #events_dashboard .toggle_content_container .toggle .none {
        padding: 15px;
        text-align: center;
    }

    #events_dashboard .toggle_content_container .item {
        padding: 15px 15px 15px 0;
        line-height: 1em;
    }
    #events_dashboard .toggle_content_container .item:hover {
        background-color: #f5faff;
    }
    #events_dashboard .toggle_content_container .item table {
        width: 100%;
        margin: 0px;
        padding: 0px;
        border: 0px none;
    }
    #events_dashboard .toggle_content_container .item table td {
        padding: 0px;
        vertical-align: top;
    }
    #events_dashboard .toggle_content_container .item table td table td {
        font-size: 1em;
        color: #999;
        text-transform: uppercase;
    }
    #events_dashboard .toggle_content_container .item td.icon {
        padding: 0 15px;
        white-space: nowrap;
        font-size: 2em;
        opacity: .6;
    }
    #events_dashboard .toggle_content_container .item .icon i.fa {
        /*margin-top: -3px;*/
    }
    #events_dashboard .toggle_content_container .item td.content {
        width: 100%;
    }
    #events_dashboard .toggle_content_container .item .content .title {
        padding-bottom: 6px;
        font-size: 1.2em;
        font-weight: bold;
    }
    #events_dashboard .toggle_content_container .item .content .title a {
        color: #555;
    }
    #events_dashboard .toggle_content_container .item .content .status {
        width: 25%;
    }
    #events_dashboard .toggle_content_container .item .content .status .published {
        color: #6BD873;
    }
    #events_dashboard .toggle_content_container .item .content .date {
        width: 25%;
    }
    #events_dashboard .toggle_content_container .item .content .actions {
        width: 50%;
        text-align: right;
    }
    #events_dashboard .toggle_content_container .item .content .actions ul {
        opacity: 0;
    }
    #events_dashboard .toggle_content_container .item:hover .content .actions ul {
        opacity: 1;
    }
    #events_dashboard .toggle_content_container .item ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #events_dashboard .toggle_content_container .item ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 0 0 8px;
        line-height: 1em;
    }
    #events_dashboard .toggle_content_container .more {
        padding-top: 15px;
        text-align: center;
    }

    #events_dashboard .activity_container {
        border-left: 2px solid #bbb;
    }

    #events_dashboard .activity_container .day {
        position: relative;
        margin-top: 20px;
        padding-bottom: 20px;
    }

    #events_dashboard .activity_container .day:first-child {
        margin-top: 0px;
    }

    #events_dashboard .activity_container .day .icon {
        position: absolute;
        top: 5px;
        left: -7px;
        width: 0.9em;
        height: 0.9em;
        background-color: #bbb;
        border-radius: 0.9em;
    }

    #events_dashboard .activity_container .day .date {
        margin-left: 20px;
        padding: 5px 10px;
        background-color: #e0e0e0;
        border-radius: 10px;
        text-align: center;
        font-weight: bold;
        color: #666;
    }

    #events_dashboard .activity_container .item {
        position: relative;
    }

    #events_dashboard .activity_container .item .icon {
        position: absolute;
        top: -3px;
        left: -10px;
        width: 1.6em;
        height: 1.6em;
        background-color: #aaa;
        text-align: center;
        font-size: 1em;
        line-height: 1.6em;
        color: #fff;
        border-radius: 1.6em;
    }

    #events_dashboard .activity_container .item .main {
        padding-left: 20px;
    }

    #events_dashboard .activity_container .item .main .title, #events_dashboard .activity_container .item .main .title a {
        padding-bottom: 3px;
        font-size: 1.05em;
        font-weight: bold;
        color: #555;
    }

    #events_dashboard .activity_container .item .main .time {
        padding-bottom: 3px;
        font-size: .9em;
        color: #777;
    }

    #events_dashboard .activity_container .item .main .details {
        padding-top: 1px;
        padding-bottom: 3px;
        font-size: .9em;
        color: #999;
    }

    #events_dashboard .activity_container .item .main .status {
        padding-bottom: 2px;
    }
    #events_dashboard .activity_container .item .main .action {
        color: #777;
    }

    #events_dashboard .activity_container .split {
        height: 1px;
        margin: 15px 0;
        border-bottom: 1px dashed #ddd;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        $('#events_dashboard .toggle_bar a').click(function(e) {
            e.preventDefault();
            var set = $(this).closest('.toggle_bar').attr('data-set');
            var what = $(this).attr('data-what');
            $('#events_dashboard .toggle_bar a').removeClass('active');
            $(this).addClass('active');
            $('#events_dashboard .toggle_content_container.set-' +set+ ' .toggle').hide();
            $('#events_dashboard .toggle_content_container.set-' +set+ ' .toggle.' +what).show();

        });

    });

</script>


<div id="events_dashboard">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 0px;">
        <tr>

            <td class="col_left">

                <?php

                // GET ALL UPCOMING EVENTS
                $all_events = $_uccms_events->getEvents('NOW', date('Y-m-d H:i:s', strtotime('+2 Months')), array('status'=>'all'));
                $num_all = count($all_events);

                // GET ACTIVE UPCOMING EVENTS
                $active_events = $_uccms_events->getEvents('NOW', date('Y-m-d H:i:s', strtotime('+2 Months')), array('status'=>'1'));
                $num_active = count($active_events);

                // GET INACTIVE UPCOMING EVENTS
                $inactive_events = $_uccms_events->getEvents('NOW', date('Y-m-d H:i:s', strtotime('+2 Months')), array('status'=>'0'));
                $num_inactive = count($inactive_events);

                ?>

                <div class="toggle_bar" data-set="events">
                    <ul>
                        <li><a href="#" class="active" data-what="all">All Events <span class="num">(<?php echo number_format($num_all, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="active">Active <span class="num">(<?php echo number_format($num_active, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="inactive">Inactive <span class="num">(<?php echo number_format($num_inactive, 0); ?>)</span></a></li>
                    </ul>
                </div>

                <div class="toggle_content_container set-events">

                    <div class="toggle all">
                        <?php if ($num_all > 0) { ?>
                            <div class="items">
                                <?php
                                $i = 1;
                                foreach ($all_events as $event) {
                                    echo this_item_event($event);
                                    if ($i == 12) {
                                        break;
                                    } else {
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./events/" class="button">All Events</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No events.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle active">
                        <?php if ($num_active > 0) { ?>
                            <div class="items">
                                <?php
                                $i = 1;
                                foreach ($active_events as $event) {
                                    echo this_item_event($event);
                                    if ($i == 12) {
                                        break;
                                    } else {
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./events/?status=active" class="button">All Active Events</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No active events.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle inactive">
                        <?php if ($num_inactive > 0) { ?>
                            <div class="items">
                                <?php
                                $i = 1;
                                foreach ($inactive_events as $event) {
                                    echo this_item_event($event);
                                    if ($i == 12) {
                                        break;
                                    } else {
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./events/?status=inactive" class="button">Inactive Events</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No inactive events.
                            </div>
                        <?php } ?>
                    </div>

                </div>

            </td>

            <td class="col_right" valign="top">
                <div class="size">

                    <h3 style="text-align: center;">Recent Activity</h3>

                    <div class="activity_container">

                        <?php

                        // ITEM ARRAY
                        $itema = array();

                        // LAST CREATED
                        $lcreated_query = "SELECT *, `dt_created` AS `sort_by`, 'created' AS `item_what` FROM `" .$_uccms_events->tables['events']. "` WHERE (`dt_created`!='0000-00-00 00:00:00') ORDER BY `dt_created` DESC LIMIT 10";
                        $lcreated_q = sqlquery($lcreated_query);
                        while ($lcreated = sqlfetch($lcreated_q)) {
                            $itema[] = $lcreated;
                        }

                        // LAST SAVED
                        $lsaved_query = "SELECT *, `dt_saved` AS `sort_by`, 'saved' AS `item_what` FROM `" .$_uccms_events->tables['events']. "` WHERE (`dt_saved`!='0000-00-00 00:00:00') ORDER BY `dt_saved` DESC LIMIT 10";
                        $lsaved_q = sqlquery($lsaved_query);
                        while ($lsaved = sqlfetch($lsaved_q)) {
                            $itema[] = $lsaved;
                        }

                        // LAST DELETED
                        $ldeleted_query = "SELECT *, `dt_deleted` AS `sort_by`, 'deleted' AS `item_what` FROM `" .$_uccms_events->tables['events']. "` WHERE (`dt_deleted`!='0000-00-00 00:00:00') ORDER BY `dt_deleted` DESC LIMIT 10";
                        $ldeleted_q = sqlquery($ldeleted_query);
                        while ($ldeleted = sqlfetch($ldeleted_q)) {
                            $itema[] = $ldeleted;
                        }

                        // HAVE ITEMS
                        if (count($itema) > 0) {

                            // SORT BY DATE/TIME ASCENDING
                            usort($itema, function($a, $b) {
                                return strtotime($b['sort_by']) - strtotime($a['sort_by']);
                            });

                            // STATUSES
                            $statusa = array(
                                'created'   => array(
                                    'title'     => 'Created',
                                    'icon'      => 'fa-plus-circle',
                                    'color'     => '#85eeb8',
                                    'actions'   => '<a href="./posts/edit/?id={id}">Edit</a>'
                                ),
                                'saved'     => array(
                                    'title'     => 'Saved',
                                    'icon'      => 'fa-floppy-o',
                                    'color'     => '#4dd0e1',
                                    'actions'   => '<a href="./posts/edit/?id={id}">Edit</a>'
                                ),
                                'deleted'   => array(
                                    'title'     => 'Deleted',
                                    'icon'      => 'fa-times-circle',
                                    'color'     => '#e53935'
                                )
                            );

                            $i = 1;

                            // LOOP
                            foreach ($itema as $item) {
                                $actiona = array(
                                    $item['id']
                                );
                                ?>
                                <div class="item contain">
                                    <?php if ($statusa[$item['item_what']]['icon']) { ?>
                                        <div class="icon" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'background-color: ' .$statusa[$item['item_what']]['color']; } ?>"><i class="fa fa-fw <?php echo $statusa[$item['item_what']]['icon']; ?>"></i></div>
                                    <?php } ?>
                                    <div class="main">
                                        <?php if ($statusa[$item['item_what']]['title']) { ?>
                                            <div class="status" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'color: ' .$statusa[$item['item_what']]['color']; } ?>">
                                                <?php echo $statusa[$item['item_what']]['title']; ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($item['title']) { ?>
                                            <div class="title">
                                                <?php if ($item['status'] != 9) { ?>
                                                    <a href="./posts/edit/?id=<?php echo $item['id']; ?>">
                                                <?php } ?>
                                                <?php echo stripslashes($item['title']); ?>
                                                <?php if ($item['status'] != 9) { ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="time"><?php echo date('j M g:i A', strtotime($item['sort_by'])); ?></div>
                                        <?php if ($statusa[$item['item_what']]['actions']) { ?>
                                            <div class="actions">
                                                <i class="fa fa-caret-right"></i> <?php echo str_replace(array('{id}'), $actiona, $statusa[$item['item_what']]['actions']); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if ($i == 10) {
                                    break;
                                } else {
                                    ?>
                                    <div class="split"></div>
                                    <?php
                                    $i++;
                                }
                            }

                            unset($itema);

                        }

                        ?>

                    </div>

                </div>
            </td>

        </tr>
    </table>

</div>

<?php

function this_item_event($event) {
    global $_uccms_events;
    if ($event['id']) {
        $out = '
        <div class="item">
            <table>
                <tr>
                    <td class="icon"><i class="fa fa-calendar"></i></td>
                    <td class="content">
                        <div class="title"><a href="./events/edit/?id=' .$event['id']. '">' .stripslashes($event['title']). '</a></div>
                        <table>
                            <tr>
                                <td class="status">
                                    ';
                                    if ($event['status'] == 1) {
                                        $out .= '<span class="active">Active</span>';
                                    } else  if ($event['status'] == 9) {
                                        $out .= '<span class="deleted">Deleted</span>';
                                    } else {
                                        $out .= '<span class="inactive">Inactive</span>';
                                    }
                                    $out .= '
                                </td>
                                <td class="date">
                                    ';
                                    if ($event['status'] == 9) {
                                        $out .= date('j M', strtotime($event['dt_deleted']));
                                    } else {
                                        $out .= date('n/j/Y g:i A', strtotime($event['date']));
                                    }
                                    $out .= '
                                </td>
                                <td class="actions">
                                    <ul>
                                        <li><a href="./events/edit/?id=' .$event['id']. '">Edit</a></li>
                                        <li><a href="' .$_uccms_events->eventURL($event['id'], 0, $event). '">View</a></li>
                                        <li><a href="./events/delete/?id=' .$event['id']. '&from=dashboard" onclick="return confirmPrompt(this.href, \'Are you sure you want to delete this this?\');">Delete</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        ';
    }
    return $out;
}

?>