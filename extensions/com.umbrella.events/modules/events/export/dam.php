<?php

//echo 'Memory Limit: ' .ini_get('memory_limit'). "\n";
/*
    - If limit is too low, edit /site/php.ini and set memory_limit = 512M
    - Running from command line: php -d memory_limit=512M manual.php
*/

##########################################################

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.events/modules/events/export/dam.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INIT CLASSES
$_uccms_events  = new uccms_Events;
$_dam           = new DigitalAssetManager();

// GET ALL EVENTS
$events_query = "SELECT * FROM `" .$_uccms_events->tables['events']. "`";
$events_q = sqlquery($events_query);

echo 'Business ID: ' .$_GLOB['REVSOCIAL']['business_id'];
echo ($cli ? "\n" : '<br />');
echo sqlrows($events_q). ' events found.';
echo ($cli ? "\n" : '<br />');

$num_success    = 0;
$num_failed     = 0;

$site = parse_url(WWW_ROOT);

// LOOP
while ($event = sqlfetch($events_q)) {

    /*
    // HAVE CONTENT
    if ($event['content']) {

        // GET HTML FROM CONTENT
        $dom = new DOMDocument('1.0');
        @$dom->loadHTML(stripslashes($event['content']));
        $xpath = new DOMXpath($dom);

        //$html = iconv('UTF-8', 'UTF-8//IGNORE', $dom->saveHTML());

        // GET IMAGES
        $images = $xpath->query("//img[@src]");

        // HAVE IMAGES
        if ($images->length > 0) {

            // LOOP THROUGH IMAGES
            foreach ($images as $image) {

                // SOURCE
                $src = $image->getAttribute('src');

                // HAVE INITIAL SLASH
                if (substr($src, 0, 1) == '/') {

                    // REMOVE IT
                    $src = substr($src, 1);

                }

                // IS NOT FULL URL
                if (substr($src, 0, 4) != 'http') {

                    // PREPEND SITE URL
                    $src = WWW_ROOT . $src;

                }

                $alt = $image->getAttribute('alt');

                // DIGITAL ASSET MANAGER VARS
                $dam_vars = array(
                    'website_extension'         => $_uccms_events->Extension,
                    'website_extension_item'    => 'post',
                    'website_extension_item_id' => $event['id'],
                    'image_url'                 => $src
                );

                if ($alt) {
                    $dam_vars['alt'] = stripslashes($alt);
                }

                // ADD MEDIA
                $dam_result = $_dam->addMedia($dam_vars);

                //echo print_r($dam_result);
                //exit;

                // SUCCESS
                if ($dam_result['success']) {

                    $num_success++;

                // FAILED
                } else {

                    $error      = print_r($dam_vars, true). "<br />" .print_r($dam_result, true). "<br /><br />";
                    $error_text = str_replace('<br />', "\n", $error);

                    echo ($cli ? $error_text : $error);

                    error_log($error_text);

                }

            }

        }

    }
    */

    //echo print_r($event);
    //exit;

    /*
    // HAVE IMAGE
    if ($event['image']) {

        // DIGITAL ASSET MANAGER VARS
        $dam_vars = array(
            'website_extension'         => $_uccms_events->Extension,
            'website_extension_item'    => 'ad',
            'website_extension_item_id' => $event['id'],
            'image_url'                 => $_uccms_events->imageBridgeOut($event['image'], 'media'),
            'title'                     => stripslashes($event['title']),
            'description'               => stripslashes($event['description'])
        );

        //echo print_r($dam_vars);
        //exit;

        // ADD MEDIA
        $dam_result = $_dam->addMedia($dam_vars);

        //echo print_r($dam_result);
        //exit;

        // SUCCESS
        if ($dam_result['success']) {

            $num_success++;

        // FAILED
        } else {

            $num_failed++;

            $error      = print_r($image, true). "<br />" .print_r($dam_vars, true). "<br />" .print_r($dam_result, true). "<br /><br />";
            $error_text = str_replace('<br />', "\n", $error);

            echo ($cli ? $error_text : $error);

            error_log($error_text);

        }

    }
    */

    //echo print_r($event);
    //exit;

    // GET IMAGES
    $images_query = "SELECT * FROM `" .$_uccms_events->tables['event_images']. "` WHERE (`event_id`=" .$event['id']. ") ORDER BY `sort` ASC, `id` ASC";
    $images_q = sqlquery($images_query);
    while ($image = sqlfetch($images_q)) {

        // DIGITAL ASSET MANAGER VARS
        $dam_vars = array(
            'website_extension'         => $_uccms_events->Extension,
            'website_extension_item'    => 'event',
            'website_extension_item_id' => $event['id'],
            'image_url'                 => $_uccms_events->imageBridgeOut($image['image'], 'events'),
            'title'                     => stripslashes($event['title'])
        );

        if ($image['caption']) {
            $dam_vars['caption'] = stripslashes($image['caption']);
        }

        //echo print_r($dam_vars);
        //exit;

        // ADD MEDIA
        $dam_result = $_dam->addMedia($dam_vars);

        //echo print_r($dam_result);
        //exit;

        // SUCCESS
        if ($dam_result['success']) {

            $num_success++;

        // FAILED
        } else {

            $num_failed++;

            $error      = print_r($image, true). "<br />" .print_r($dam_vars, true). "<br />" .print_r($dam_result, true). "<br /><br />";
            $error_text = str_replace('<br />', "\n", $error);

            echo ($cli ? $error_text : $error);

            error_log($error_text);

        }

    }

}

//echo ($cli ? "\n" : '<br />');

echo ($cli ? "\n" : '<br />');
echo '<b>All done. (' .$num_success. ' Success. ' .$num_failed. ' Failed.)</b>';

?>