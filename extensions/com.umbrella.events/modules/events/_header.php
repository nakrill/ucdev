<?php

// BIGTREE
$bigtree['css'][]   = 'master.css';
$bigtree['js'][]    = 'master.js';

// MODULE CLASS
$_uccms_events = new uccms_Events;

// BREADCRUMBS
$bigtree['breadcrumb'] = [
    [ 'title' => 'Events', 'link' => $bigtree['path'][1] ],
];

// MODULE MAIN NAV SELECTED ARRAY
if ($bigtree['path'][2]) {
    $mmnsa[$bigtree['path'][2]] = 'active';
} else {
    $mmnsa['dashboard'] = 'active';
}

?>

<nav class="main">
    <section>
        <ul>
            <li class="<?php echo $mmnsa['dashboard']; ?>">
                <a href="<?=MODULE_ROOT;?>"><span class="dashboard"></span>Dashboard</a>
                <?php /*
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>calendar/">Calendar</a></li>
                </ul>
                */ ?>
            </li>
            <li class="<?php echo $mmnsa['categories']; ?>">
                <a href="<?=MODULE_ROOT;?>categories/"><span class="pages"></span>Categories</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>categories/">All</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>categories/?do=add">New Category</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['tags']; ?>">
                <a href="<?=MODULE_ROOT;?>tags/"><span class="pages"></span>Tags</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>tags/">All</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>tags/?do=add">New Tag</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['locations']; ?>">
                <a href="<?=MODULE_ROOT;?>locations/"><span class="pages"></span>Locations</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>locations/">All</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>locations/?do=add">New Location</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['events']; ?>">
                <a href="<?=MODULE_ROOT;?>events/"><span class="developer"></span>Events</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>events/">All</a></li>
                    <? /*
                    <li><a href="<?=MODULE_ROOT;?>posts/?status=published">Published</a></li>
                    <li><a href="<?=MODULE_ROOT;?>posts/?status=draft">Drafts</a></li>
                    */ ?>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>events/edit/?id=new">New Event</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['settings']; ?>">
                <a href="<?=MODULE_ROOT;?>settings/"><span class="settings"></span>Settings</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                </ul>
            </li>
        </ul>
    </section>
</nav>
