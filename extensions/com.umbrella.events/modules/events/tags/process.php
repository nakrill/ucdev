<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['tag']['id'];

    // USE tag TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['tag']['slug']) {
        $_POST['tag']['slug'] = $_POST['tag']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_events->makeRewrite($_POST['tag']['slug']),
        'active'            => (int)$_POST['tag']['active'],
        'title'             => $_POST['tag']['title'],
        'meta_title'        => $_POST['tag']['meta_title'],
        'meta_description'  => $_POST['tag']['meta_description'],
        'meta_keywords'     => $_POST['tag']['meta_keywords']
    );

    // HAVE tag ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_events->tables['tags']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO tag ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_events->tables['tags']. "` SET " .uccms_createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Tag', 'Tag added!');
        } else {
            $admin->growl('Tag', 'Tag updated!');
        }

    // QUERY FAILED
    } else {
        $admin->growl('Tag', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'tags/edit/?id=' .$id);

?>