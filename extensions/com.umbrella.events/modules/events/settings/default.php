<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET SETTINGS
$settings = $_uccms_events->getSettings();

// TIMEZONE
$timezone = $_uccms_events->timezone();

?>

<style type="text/css">

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // EVENTS - COMMENTS ENABLED TOGGLE
        $('#event_comments_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('.event_comments_enabled_content').show();
            } else {
                $('.event_comments_enabled_content').hide();
            }
        });

        // EVENTS - COMMENTS SOURCE CHANGE
        $('#event_comments_source').change(function(e) {
            var what = $(this).val();
            $('.event_comments_source_content').hide();
            if (what) {
                $('.event_comments_source_content.' +what).show();
            }
        });

        /*
        // SIDEBAR WIDGETS - SORTABLE
        $('form .sidebar_widgets table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });
        */

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>General</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset class="form-group">
                    <label>Timezone</label>
                    <select name="setting[timezone]">
                        <?php foreach ($_uccms_events->timezoneArray() as $tz => $display) { ?>
                            <option value="<?php echo $tz; ?>" <?php if ($tz == $timezone) { ?>selected="selected"<?php } ?>><?php echo $display; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="form-group">
                    <label>Events Per Page</label>
                    <input type="text" name="setting[events_per_page]" value="<?php echo stripslashes($settings['events_per_page']); ?>" placeholder="<?php echo $cms->getSetting('bigtree-internal-per-page'); ?>" class="smaller_60" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="setting[search_disabled]" value="1" <?php if ($settings['search_disabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Search disabled <small>Don't allow searching.</small></label>
                </fieldset>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<? /*
<div class="container">

    <header>
        <h2>Home</h2>
    </header>

    <section>

        <fieldset class="form-group">
            <label>Content</label>
            <div>
                <?php
                $field = array(
                    'key'       => 'setting[home_content]', // The value you should use for the "name" attribute of your form field
                    'value'     => stripslashes($settings['home_content']), // The existing value for this form field
                    'id'        => 'home_content', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'simple' => false
                    )
                );
                include(BigTree::path('admin/form-field-types/draw/html.php'));
                ?>
            </div>
        </fieldset>

        <div class="contain" style="padding-bottom: 20px;">

            <div class="left last">

                <fieldset class="form-group">
                    <label>Number of Featured Events</label>
                    <input type="text" name="setting[home_featured_num]" value="<?php echo (int)$settings['home_featured_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Featured Events Title</label>
                    <input type="text" name="setting[home_featured_title]" value="<?php echo stripslashes($settings['home_featured_title']); ?>" placeholder="Featured Items" />
                </fieldset>

            </div>

            <div class="right last">

                <fieldset class="form-group">
                    <label>Number of Upcoming Events</label>
                    <input type="text" name="setting[home_upcoming_num]" value="<?php echo (int)$settings['home_upcoming_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Upcoming Events Title</label>
                    <input type="text" name="setting[home_upcoming_title]" value="<?php echo stripslashes($settings['home_upcoming_title']); ?>" placeholder="Upcoming Events" />
                </fieldset>

            </div>

        </div>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="home_seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-home_seo" style="display: none;">

                <fieldset class="form-group">
                    <label>Meta Title</label>
                    <input type="text" name="setting[home_meta_title]" value="<?php echo stripslashes($settings['home_meta_title']); ?>" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Description</label>
                    <textarea name="setting[home_meta_description]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_description']); ?></textarea>
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Keywords</label>
                    <textarea name="setting[home_meta_keywords]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>
*/ ?>

<div class="container legacy">

    <header>
        <h2>Events</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset class="last">
                    <input id="event_comments_enabled" type="checkbox" name="setting[event_comments_enabled]" value="1" <?php if ($settings['event_comments_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Comments enabled <small>Display comments on event detail pages.</small></label>
                </fieldset>

                <div class="event_comments_enabled_content" style="<?php if (!$settings['event_comments_enabled']) { ?>display: none; <?php } ?> padding-top: 20px;">

                    <fieldset class="form-group">
                        <label>Source</label>
                        <select id="event_comments_source" name="setting[event_comments_source]">
                            <option value="">Select</option>
                            <option value="facebook" <?php if ($settings['event_comments_source'] == 'facebook') { ?>selected="selected"<?php } ?>>Facebook</option>
                            <option value="disqus" <?php if ($settings['event_comments_source'] == 'disqus') { ?>selected="selected"<?php } ?>>Disqus</option>
                        </select>
                    </fieldset>

                    <div class="event_comments_source_content facebook" style="<?php if ($settings['event_comments_source'] != 'facebook') { ?>display: none; <?php } ?>">

                        <fieldset class="form-group">
                            <label>App ID <small>(<a href="https://developers.facebook.com/docs/plugins/comments" target="_blank">Create app</a>)</small></label>
                            <input type="text" name="setting[event_comments_facebook_appid]" value="<?php echo stripslashes($settings['event_comments_facebook_appid']); ?>" />
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Number of Comments to Display</label>
                            <input type="text" name="setting[event_comments_facebook_num]" value="<?php echo stripslashes($settings['event_comments_facebook_num']); ?>" placeholder="10" class="smaller_60" />
                        </fieldset>

                    </div>

                    <div class="event_comments_source_content disqus" style="<?php if ($settings['event_comments_source'] != 'disqus') { ?>display: none; <?php } ?>">

                        <fieldset class="form-group">
                            <label>Shortname <small>(<a href="http://disqus.com/register" target="_blank">Register site</a>)</small></label>
                            <input type="text" name="setting[event_comments_disqus_shortname]" value="<?php echo stripslashes($settings['event_comments_disqus_shortname']); ?>" />
                        </fieldset>

                    </div>

                </div>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>

</form>