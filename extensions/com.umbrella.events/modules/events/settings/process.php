<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SETTINGS ARRAY
    $seta = array(
        'timezone',
        'events_per_page',
        'search_disabled',
        //'home_content',
        //'home_upcoming_num',
        //'home_featured_num',
        //'home_meta_title',
        //'home_meta_description',
        //'home_meta_keywords',
        'event_comments_enabled',
        'event_comments_source',
        'event_comments_facebook_appid',
        'event_comments_facebook_num',
        'event_comments_disqus_shortname'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_events->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('General Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/');

?>