<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

    #categories .category_title {
        width: 530px;
    }
    #categories .category_events {
        width: 160px;
    }
    #categories .category_status {
        width: 140px;
    }
    #categories .category_edit {
        width: 55px;
    }
    #categories .category_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#categories .status select').change(function() {
            $('#form_categories').submit();
        });

    });

</script>

<div id="categories">

    <form id="form_categories">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <div class="status contain">
        <div style="float: right;">
            <select name="status">
                <option value="">All</option>
                <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
            </select>
        </div>
    </div>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Categories</h2>
            <a class="add_resource add" href="./edit/"><span></span>Add Category</a>
        </summary>
        <header style="clear: both;">
            <span class="category_title">Title</span>
            <span class="category_events">Events</span>
            <span class="category_status">Status</span>
            <span class="category_edit">Edit</span>
            <span class="category_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/categories/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_events->Extension?>/ajax/admin/categories/get-page/?page=1&status=" +escape($('#categories .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_events->Extension?>/ajax/admin/categories/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=" +escape($('#categories .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>");
        return false;
    });
</script>