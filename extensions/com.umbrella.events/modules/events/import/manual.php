<?php

/*
[0] => Test Event 1
    [1] => 1
    [2] => 11/25/2015 0:00
    [3] => 9:00:00 AM
    [4] =>
    [5] => 23:00
    [6] => <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In facilisis lectus in erat porttitor aliquam. Nulla ornare faucibus dui eget mollis. Vestibulum quis velit odio. In pre
tium finibus mauris non lacinia.</p><p>Sed orci tortor, fringilla et ante non, sodales efficitur urna. Ut non orci ut libero dictum mollis. Nunc convallis sem et consectetur euismod.</p><p>Qu
isque eu molestie velit. In tellus tellus, tincidunt quis eros in, condimentum sagittis ipsum. Curabitur eros ante, eleifend sit amet tincidunt eu, fringilla eu ex. In hac habitasse platea di
ctumst. Quisque et tortor nibh. Sed efficitur lorem tortor, quis facilisis sapien fermentum vitae. Proin sit amet massa eget dolor vestibulum ultrices. In ipsum quam, lacinia nec enim quis, s
celerisque fermentum arcu. Quisque ultrices diam quis risus blandit accumsan.</p>
    [7] => Matt DeWyer
    [8] => 323 S Nevada St
    [9] => Oceanside
    [10] => CA
    [11] => 92054
    [12] => 555-555-5555
    [13] => there@nowhere.com
    [14] => http://www.domain.com
    [15] => Category One
*/

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.events/modules/events/import/manual.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INIT EVENTS CLASS
$_uccms_events = new uccms_Events;

$row = 0;

// GET DATA
if (($handle = fopen(dirname(__FILE__). '/Import_Template.csv', 'r')) !== FALSE) {

    $cata = array();
    $taga = array();
    $locationa = array();

    // LOOP THROUGH DATA
    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {

        // IS HEADINGS - SKIP
        if ($row == 0) {
            $row++;
            continue;
        }

        // HAVE DATA
        if (count($data) > 0) {

            // CLEAN UP
            $event_title    = trim(sqlescape($data[0]));
            $start_date     = trim(str_replace(' 0:00', '', $data[2]));
            if ((!$data[4]) && ($start_date)) {
                $end_date   = $start_date;
            } else {
                $end_date   = trim(str_replace(' 0:00', '', $data[4]));
            }
            $dt_start       = date('Y-m-d H:i:s', strtotime($start_date. ' ' .$data[3]));
            $dt_end         = date('Y-m-d H:i:s', strtotime($end_date. ' ' .$data[5]));

            // CHECK FOR EXISTING EVENT
            $event_query = "SELECT * FROM `" .$_uccms_events->tables['events']. "` WHERE (`title`='" .$event_title. "') AND (`dt_start`='" .$dt_start. "') ORDER BY `id` DESC LIMIT 1";
            $event_q = sqlquery($event_query);
            $event = sqlfetch($event_q);

            // DB COLUMNS
            $columns = array(
                'dt_start'                  => $dt_start,
                'dt_end'                    => $dt_end,
                'title'                     => $event_title,
                'description'               => $data[6],
                'name_contact'              => $data[7],
                'address1'                  => $data[8],
                'city'                      => $data[9],
                'state'                     => $data[10],
                'zip'                       => $data[11],
                'phone'                     => $data[12],
                'email'                     => $data[13],
                'url'                       => $data[14]
            );

            // STATUS
            if ($data[1] == 1) {
                $columns['status'] = 1;
            } else if ($data[1] === 0) {
                $columns['status'] = 0;
            }

            // REMOVE EMPTY COLUMNS
            foreach ($columns as $col_name => $col_val) {
                if ($col_val === 0) {
                    continue;
                } else if ($col_val) {
                    continue;
                } else {
                    unset($columns[$col_name]);
                }
            }

            // HAVE ID - IS UPDATING
            if ($event['id']) {

                // DB QUERY
                $query = "UPDATE `" .$_uccms_events->tables['events']. "` SET " .uccms_createSet($columns). ", `dt_saved`=NOW() WHERE (`id`=" .$event['id']. ")";

            // NO ID - IS NEW
            } else {

                // SLUG
                $columns['slug'] = $_uccms_events->makeRewrite($event_title);

                // DB QUERY
                $query = "INSERT INTO `" .$_uccms_events->tables['events']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW(), `dt_saved`=NOW()";

            }

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {

                // UPDATED
                if ($event['id']) {
                    echo $data[0]. ' - Updated';

                // ADDED
                } else {

                    echo $data[0]. ' - Added';

                    // NEW EVENT ID
                    $event['id'] = sqlid();

                }

                ###############################
                # CATEGORIES
                ###############################

                $create_relation = false;
                unset($category_del);

                // GET CURRENT RELATIONS
                $rel_query = "SELECT `category_id` FROM `" .$_uccms_events->tables['event_categories']. "` WHERE (`event_id`=" .$event['id']. ")";
                $rel_q = sqlquery($rel_query);
                while ($rel = sqlfetch($rel_q)) {
                    $category_del[$rel['category_id']] = $rel['category_id']; // SAVE TO DELETE ARRAY
                }

                // CATEGORIES
                $categorya = explode('|', $data[15]);

                // LOOP THROUGH CATEGORIES
                foreach ($categorya as $category_title) {

                    // CLEAN UP
                    $category_title = trim(sqlescape($category_title));

                    // IS A VALUE
                    if ($category_title) {

                        // NOT IN CATEGORY ARRAY
                        if (!$cata[$category_title]) {

                            // LOOK FOR MATCH
                            $category_query = "SELECT `id` FROM `" .$_uccms_events->tables['categories']. "` WHERE (`title`='" .$category_title. "')";
                            $category_q = sqlquery($category_query);
                            $category = sqlfetch($category_q);

                            // ADD TO CATEGORY ARRAY
                            $cata[$category_title] = $category['id'];

                        }

                        // CATEGORY FOUND
                        if ($cata[$category_title]) {

                            // RELATION ALREADY EXISTS
                            if ($category_del[$cata[$category_title]]) {
                                unset($category_del[$cata[$category_title]]);

                            // CREATE RELATION
                            } else {
                                $create_relation = true;
                            }

                        // NO CATEGORY FOUND
                        } else {

                            // CREATE NEW CATEGORY
                            $ncat_query = "INSERT INTO `" .$_uccms_events->tables['categories']. "` SET `title`='" .$category_title. "', `slug`='" .$_uccms_events->makeRewrite($category_title). "'";
                            $ncat_q = sqlquery($ncat_query);

                            // NEW CATEGORY ID
                            $cata[$category_title] = sqlid();

                            $create_relation = true;

                        }

                        // CREATE RELATION
                        if (($create_relation) && ($cata[$category_title])) {

                            $rel_query = "INSERT INTO `" .$_uccms_events->tables['event_categories']. "` SET `category_id`=" .$cata[$category_title]. ", `event_id`=" .$event['id'];
                            sqlquery($rel_query);

                        }

                        // HAVE ANY TO DELETE
                        if (count($category_del) > 0) {
                            foreach ($category_del as $category_id) {
                                $del_query = "DELETE FROM `" .$_uccms_events->tables['event_categories']. "` WHERE (`event_id`=" .$event['id']. ") AND (`category_id`=" .$category_id. ")";
                                sqlquery($del_query);
                            }
                        }

                        unset($category);

                    }

                }

                ###############################
                # TAGS
                ###############################

                $create_relation = false;
                unset($tag_del);

                // GET CURRENT RELATIONS
                $rel_query = "SELECT `tag_id` FROM `" .$_uccms_events->tables['event_tags']. "` WHERE (`event_id`=" .$event['id']. ")";
                $rel_q = sqlquery($rel_query);
                while ($rel = sqlfetch($rel_q)) {
                    $tag_del[$rel['tag_id']] = $rel['tag_id']; // SAVE TO DELETE ARRAY
                }

                // TAGS
                $tagsa = explode('|', $data[16]);

                // LOOP THROUGH TAGS
                foreach ($tagsa as $tag_title) {

                    // CLEAN UP
                    $tag_title = trim(sqlescape($tag_title));

                    // IS A VALUE
                    if ($tag_title) {

                        // NOT IN TAG ARRAY
                        if (!$taga[$tag_title]) {

                            // LOOK FOR MATCH
                            $tag_query = "SELECT `id` FROM `" .$_uccms_events->tables['tags']. "` WHERE (`title`='" .$tag_title. "')";
                            $tag_q = sqlquery($tag_query);
                            $tag = sqlfetch($tag_q);

                            // ADD TO TAG ARRAY
                            $taga[$tag_title] = $tag['id'];

                        }

                        // TAG FOUND
                        if ($taga[$tag_title]) {

                            // RELATION ALREADY EXISTS
                            if ($tag_del[$taga[$tag_title]]) {
                                unset($tag_del[$taga[$tag_title]]);

                            // CREATE RELATION
                            } else {
                                $create_relation = true;
                            }

                        // NO TAG FOUND
                        } else {

                            // CREATE NEW TAG
                            $ncat_query = "INSERT INTO `" .$_uccms_events->tables['tags']. "` SET `title`='" .$tag_title. "', `slug`='" .$_uccms_events->makeRewrite($tag_title). "'";
                            $ncat_q = sqlquery($ncat_query);

                            // NEW TAG ID
                            $taga[$tag_title] = sqlid();

                            $create_relation = true;

                        }

                        // CREATE RELATION
                        if (($create_relation) && ($taga[$tag_title])) {

                            $rel_query = "INSERT INTO `" .$_uccms_events->tables['event_tags']. "` SET `tag_id`=" .$taga[$tag_title]. ", `event_id`=" .$event['id'];
                            sqlquery($rel_query);

                        }

                        // HAVE ANY TO DELETE
                        if (count($tag_del) > 0) {
                            foreach ($tag_del as $tag_id) {
                                $del_query = "DELETE FROM `" .$_uccms_events->tables['event_tags']. "` WHERE (`event_id`=" .$event['id']. ") AND (`tag_id`=" .$tag_id. ")";
                                sqlquery($del_query);
                            }
                        }

                        unset($tag);

                    }

                }

                ###############################
                # LOCATIONS
                ###############################

                $create_relation = false;
                unset($location_del);

                // GET CURRENT RELATIONS
                $rel_query = "SELECT `location_id` FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`event_id`=" .$event['id']. ")";
                $rel_q = sqlquery($rel_query);
                while ($rel = sqlfetch($rel_q)) {
                    $location_del[$rel['location_id']] = $rel['location_id']; // SAVE TO DELETE ARRAY
                }

                // LOCATIONS
                $locationsa = explode('|', $data[17]);

                // LOOP THROUGH LOCATIONS
                foreach ($locationsa as $location_title) {

                    // CLEAN UP
                    $location_title = trim(sqlescape($location_title));

                    // IS A VALUE
                    if ($location_title) {

                        // NOT IN LOCATION ARRAY
                        if (!$locationa[$location_title]) {

                            // LOOK FOR MATCH
                            $location_query = "SELECT `id` FROM `" .$_uccms_events->tables['locations']. "` WHERE (`title`='" .$location_title. "')";
                            $location_q = sqlquery($location_query);
                            $location = sqlfetch($location_q);

                            // ADD TO LOCATION ARRAY
                            $locationa[$location_title] = $location['id'];

                        }

                        // LOCATION FOUND
                        if ($locationa[$location_title]) {

                            // RELATION ALREADY EXISTS
                            if ($location_del[$locationa[$location_title]]) {
                                unset($location_del[$locationa[$location_title]]);

                            // CREATE RELATION
                            } else {
                                $create_relation = true;
                            }

                        // NO LOCATION FOUND
                        } else {

                            // CREATE NEW LOCATION
                            $ncat_query = "INSERT INTO `" .$_uccms_events->tables['locations']. "` SET `title`='" .$location_title. "', `slug`='" .$_uccms_events->makeRewrite($location_title). "'";
                            $ncat_q = sqlquery($ncat_query);

                            // NEW location ID
                            $locationa[$location_title] = sqlid();

                            $create_relation = true;

                        }

                        // CREATE RELATION
                        if (($create_relation) && ($locationa[$location_title])) {

                            $rel_query = "INSERT INTO `" .$_uccms_events->tables['event_locations']. "` SET `location_id`=" .$locationa[$location_title]. ", `event_id`=" .$event['id'];
                            sqlquery($rel_query);

                        }

                        // HAVE ANY TO DELETE
                        if (count($location_del) > 0) {
                            foreach ($location_del as $location_id) {
                                $del_query = "DELETE FROM `" .$_uccms_events->tables['event_locations']. "` WHERE (`event_id`=" .$event['id']. ") AND (`location_id`=" .$location_id. ")";
                                sqlquery($del_query);
                            }
                        }

                        unset($location);

                    }

                }

                ###############################

            // QUERY FAILED
            } else {

                echo $data[0]. ' - Failed';

            }

            // CLEAR ARRAYS
            unset($event);
            unset($columns);

        }

        echo ($cli ? "\n" : '<br />');

        $row++;

    }

    // CLOSE DATA
    fclose($handle);

    unset($cata);

}

echo ($cli ? "\n" : '<br /><b>');
echo 'All done.';
echo ($cli ? "\n" : '</b><br />');

?>