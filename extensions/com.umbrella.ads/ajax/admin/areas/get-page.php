<?php

$admin->requireLevel(1);

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

// MODULE CLASS
if (!$_uccms_ads) $_uccms_ads = new uccms_Ads;

//$_uccms_ads->setPerPage(5);

// WHERE ARRAY
$wa[] = "a.id>0";

// STATUSES
$statusa = array(
    'inactive'  => 0,
    'active'    => 1,
);
if (isset($statusa[$_REQUEST['status']])) {
    $wa[] = "a.active=" .$statusa[$_REQUEST['status']];
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(a.title) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED AREAS
$area_query = "
SELECT a.*
FROM `" .$_uccms_ads->tables['areas']. "` AS `a`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY a.title ASC, a.id ASC
LIMIT " .(($page - 1) * $_uccms_ads->perPage()). "," .$_uccms_ads->perPage();
$area_q = sqlquery($area_query);

// NUMBER OF PAGED AREAS
$num_areas = sqlrows($area_q);

// TOTAL NUMBER OF AREAS
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ads->tables['areas']. "` AS `a` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_ads->pageCount($total_results['total']);

// HAVE AREAS
if ($num_areas > 0) {

    // LOOP
    while ($area = sqlfetch($area_q)) {

        ?>

        <li class="item">
            <section class="area_title">
                <a href="./edit/?id=<?php echo $area['id']; ?>"><?php echo stripslashes($area['title']); ?></a>
            </section>
            <section class="area_ads">
                <?php
                $aa_query = "SELECT `id` FROM `" .$_uccms_ads->tables['ads_areas']. "` WHERE (`area_id`=" .$area['id']. ")";
                $aa_q = sqlquery($aa_query);
                ?>
                <a href="../ads/?area_id=<?php echo $area['id']; ?>"><?php echo number_format(sqlrows($aa_q), 0); ?></a>
            </section>
            <section class="area_status status_<?php if ($area['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                <?php if ($area['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
            </section>
            <section class="area_edit">
                <a class="icon_edit" title="Edit area" href="./edit/?id=<?php echo $area['id']; ?>"></a>
            </section>
            <section class="area_delete">
                <a href="./delete/?id=<?php echo $area['id']; ?>" class="icon_delete" title="Delete Area" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
            </section>
        </li>

        <?php

    }

// NO CATEGORIES
} else {
    ?>
    <li style="text-align: center;">
        No areas.
    </li>
    <?php
}

?>

<script>
	BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>