<?php

// MODULE CLASS
if (!$_uccms_ads) $_uccms_ads = new uccms_Ads;

// HAS ACCESS
if ($_uccms_ads->adminModulePermission()) {

    // CLEAN UP
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
    $area_id        = (int)$_GET['area_id'];
    $tag_id         = (int)$_GET['tag_id'];
    $limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_ads->perPage();

    if (isset($_GET['base_url'])) {
        $base_url = $_GET['base_url'];
    } else if (defined('MODULE_ROOT')) {
        $base_url = MODULE_ROOT. 'ads';
    } else {
        $base_url = '.';
    }

    //$_uccms_ads->setPerPage(5);

    // WHERE ARRAY
    unset($wa);
    $wa[] = "a.id>0";

    // STATUSES
    $statusa = array(
        'inactive'  => 0,
        'active'    => 1,
        'scheduled' => 2,
        'expired'   => 7,
        'deleted'   => 9
    );
    if (isset($statusa[$_REQUEST['status']])) {
        $wa[] = "a.status=" .$statusa[$_REQUEST['status']];
    } else {
        $wa[] = "(a.status!=7) AND (a.status!=9)";
    }

    // AREA SPECIFIED
    if ($area_id > 0) {
        $inner_join_area = "INNER JOIN `" .$_uccms_ads->tables['ads_areas']. "` AS `aa` ON a.id=aa.ad_id";
        $wa[] = "aa.area_id=" .$area_id;
    }

    /*
    // TAG SPECIFIED
    if ($tag_id > 0) {
        $inner_join_tag = "INNER JOIN `" .$_uccms_ads->tables['area_tags']. "` AS `tc` ON a.id=tc.ad_id";
        $wa[] = "tc.category_id=" .$tag_id;
    }
    */

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(a.title) LIKE '%$part%') OR (LOWER(a.description) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED ADS
    $ad_query = "
    SELECT a.*
    FROM `" .$_uccms_ads->tables['ads']. "` AS `a`
    " .$inner_join_area. " " .$inner_join_tag. "
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY a.dt_created DESC, a.id DESC
    LIMIT " .(($page - 1) * $limit). "," .$limit;
    $ad_q = sqlquery($ad_query);

    // NUMBER OF PAGED ADS
    $num_ads = sqlrows($ad_q);

    // TOTAL NUMBER OF ADS
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ads->tables['ads']. "` AS `a` " .$inner_join_area. " " .$inner_join_tag. " WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_ads->pageCount($total_results['total']);

    // HAVE ADS
    if ($num_ads > 0) {

        // LOOP
        while ($ad = sqlfetch($ad_q)) {

            // AREA RELATIONS ARRAY
            $rela = array();

            // GET AREA RELATIONS
            $rel_query = "
            SELECT aa.*, a.title
            FROM `" .$_uccms_ads->tables['ads_areas']. "` AS `aa`
            INNER JOIN `" .$_uccms_ads->tables['areas']. "` AS `a` ON aa.area_id=a.id
            WHERE (aa.ad_id=" .$ad['id']. ") AND (aa.active=1)
            ORDER BY a.title ASC
            ";
            $rel_q = sqlquery($rel_query);
            while ($rel = sqlfetch($rel_q)) {

                /*
                $dt_active = ($rel['dt_active'] != '0000-00-00 00:00:00' ? strtotime($rel['dt_active']) : '');
                $dt_expire = ($rel['dt_expire'] != '0000-00-00 00:00:00' ? strtotime($rel['dt_expire']) : '');

                if (($dt_active) && ($dt_expire)) {
                    if ((time() > $dt_active) && (time() < $dt_expire)) {
                        $rel_status = 'active';
                    } else if (time() < $dt_active) {
                        $rel_status = 'scheduled';
                    } else if (time() > $dt_expire) {
                        $rel_status = 'expired';
                    }
                } else if ($dt_active) {
                    if (time() > $dt_active) {
                        $rel_status = 'active';
                    } else {
                        $rel_status = 'scheduled';
                    }
                } else if ($dt_expire) {
                    if (time() < $dt_expire) {
                        $rel_status = 'active';
                    } else {
                        $rel_status = 'expired';
                    }
                }
                */

                $rela[] = '<a href="' .$base_url. '/?area_id=' .$rel['area_id']. '" class="area_status status-' .strtolower($_uccms_ads->statuses[(int)$rel['status']]). '" title="' .$_uccms_ads->statuses[(int)$rel['status']]. '">' .stripslashes($rel['title']). '</a>';
            }

            ?>

            <li class="item">
                <section class="ad_titlex">
                    <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $ad['id']; ?>"><?php echo stripslashes($ad['title']); ?></a>
                </section>
                <section class="ad_area">
                    <?php echo implode(', ', $rela); ?>
                </section>
                <section class="ad_status status_<?php if ($ad['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php echo $_uccms_ads->statuses[$ad['status']]; ?>
                </section>
                <section class="ad_edit">
                    <a class="icon_edit" title="Edit Ad" href="<?php echo $base_url; ?>/edit/?id=<?php echo $ad['id']; ?>"></a>
                </section>
                <section class="ad_delete">
                    <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $ad['id']; ?>" class="icon_delete" title="Delete Ad" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                </section>
            </li>

            <?php

        }

        unset($accta);

    // NO ads
    } else {
        ?>
        <li style="text-align: center;">
            No ads.
        </li>
        <?php
    }

    ?>

    <script>
	    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>