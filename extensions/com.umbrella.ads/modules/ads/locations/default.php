<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Locations', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #items header span, #items .item section {
        text-align: left;
    }

    #items .city_title {
        width: 530px;
    }
    #items .city_neighborhoods {
        width: 150px;
    }
    #items .city_ads {
        width: 150px;
    }
    #items .city_status {
        width: 100px;
    }
    #items .city_edit {
        width: 55px;
    }
    #items .city_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#cities .status select').change(function() {
            $('#form_cities').submit();
        });

    });

</script>

<div id="cities">

    <form id="form_cities">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <? /*
    <div class="status contain">
        <div style="float: right;">
            <select name="status">
                <option value="">All</option>
                <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
                <option value="submitted" <?php if ($_REQUEST['status'] == 'submitted') { ?>selected="selected"<?php } ?>>Submitted</option>
            </select>
        </div>
    </div>
    */ ?>

    <div class="search_paging contain">
        <? /*
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        */ ?>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Cities</h2>
            <a class="add_resource add" href="./city/"><span></span>Add City</a>
        </summary>
        <header style="clear: both;">
            <span class="city_title">Name</span>
            <span class="city_neighborhoods">Neighborhoods</span>
            <span class="city_ads">Ads</span>
            <span class="city_edit">Edit</span>
            <span class="city_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/locations/get-page-cities.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_ads->Extension?>/ajax/admin/locations/get-page-cities/?page=1&status=<?php echo $_REQUEST['status']; ?>&query=" +escape($("#query").val()));
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_ads->Extension?>/ajax/admin/locations/get-page-cities/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=<?php echo $_REQUEST['status']; ?>&query=" +escape($("#query").val()));
        return false;
    });
</script>