<?php

exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // SETTINGS ARRAY
    $seta = array(
        'posts_per_page',
        'sidebar_enabled',
        'sidebar_position',
        'search_disabled',
        'post_hide_date',
        'post_hide_author',
        'post_comments_enabled',
        'post_comments_source',
        'post_comments_facebook_appid',
        'post_comments_facebook_num',
        'post_comments_disqus_shortname',
        'post_ping_enabled',
        'post_ping_from_links',
        'post_ping_list'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_blog->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    // CODE FOR SIDEBARS
    if (is_array($_POST['setting']['sidebar_widgets'])) {
        foreach ($_POST['setting']['sidebar_widgets'] as $widget_id => $widget) {
            if ($widget['code']) {
                $_uccms_blog->setSetting('sidebar_widgets_' .$widget_id. '_code', $widget['code']);
                unset($_POST['setting']['sidebar_widgets'][$widget_id]['code']);
            }
        }
    }

    // SIDEBAR WIDGETS ORDER
    $_uccms_blog->setSetting('sidebar_widgets', $_POST['setting']['sidebar_widgets']);

    $admin->growl('General Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/');

?>