<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET SETTINGS
$settings = $_uccms_ads->getSettings();

?>

<style type="text/css">

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // POSTS - COMMENTS ENABLED TOGGLE
        $('#post_comments_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('.post_comments_enabled_content').show();
            } else {
                $('.post_comments_enabled_content').hide();
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>General</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <? /*
                <fieldset>
                    <label>Posts Per Page</label>
                    <input type="text" name="setting[posts_per_page]" value="<?php echo stripslashes($settings['posts_per_page']); ?>" placeholder="<?php echo $cms->getSetting('bigtree-internal-per-page'); ?>" class="smaller_60" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="setting[search_disabled]" value="1" <?php if ($settings['search_disabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Search disabled <small>Don't allow searching.</small></label>
                </fieldset>
                */ ?>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Ads</h2>
    </header>

    <section>

        <div class="contain">

            <? /*
            <div class="left">

                <fieldset>
                    <input type="checkbox" name="setting[post_hide_date]" value="1" <?php if ($settings['post_hide_date']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Hide Date</label>
                </fieldset>

                <fieldset>
                    <input type="checkbox" name="setting[post_hide_author]" value="1" <?php if ($settings['post_hide_author']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Hide Author</label>
                </fieldset>

            </div>

            <div class="right">

                <fieldset>
                    <input id="post_comments_enabled" type="checkbox" name="setting[post_comments_enabled]" value="1" <?php if ($settings['post_comments_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Comments enabled <small>Display comments on blog posts.</small></label>
                </fieldset>

                <div class="post_comments_enabled_content" style="<?php if (!$settings['post_comments_enabled']) { ?>display: none; <?php } ?> padding: 0 0 20px 0;">

                    <fieldset>
                        <label>Source</label>
                        <select id="post_comments_source" name="setting[post_comments_source]">
                            <option value="">Select</option>
                            <option value="facebook" <?php if ($settings['post_comments_source'] == 'facebook') { ?>selected="selected"<?php } ?>>Facebook</option>
                            <option value="disqus" <?php if ($settings['post_comments_source'] == 'disqus') { ?>selected="selected"<?php } ?>>Disqus</option>
                        </select>
                    </fieldset>

                    <div class="post_comments_source_content facebook" style="<?php if ($settings['post_comments_source'] != 'facebook') { ?>display: none; <?php } ?>">

                        <fieldset>
                            <label>App ID <small>(<a href="https://developers.facebook.com/docs/plugins/comments" target="_blank">Create app</a>)</small></label>
                            <input type="text" name="setting[post_comments_facebook_appid]" value="<?php echo stripslashes($settings['post_comments_facebook_appid']); ?>" />
                        </fieldset>

                        <fieldset>
                            <label>Number of Comments to Display</label>
                            <input type="text" name="setting[post_comments_facebook_num]" value="<?php echo stripslashes($settings['post_comments_facebook_num']); ?>" placeholder="10" class="smaller_60" />
                        </fieldset>

                    </div>

                    <div class="post_comments_source_content disqus" style="<?php if ($settings['post_comments_source'] != 'disqus') { ?>display: none; <?php } ?>">

                        <fieldset>
                            <label>Shortname <small>(<a href="http://disqus.com/register" target="_blank">Register site</a>)</small></label>
                            <input type="text" name="setting[post_comments_disqus_shortname]" value="<?php echo stripslashes($settings['post_comments_disqus_shortname']); ?>" />
                        </fieldset>

                    </div>

                </div>

                <fieldset class="last">
                    <input id="post_ping_enabled" type="checkbox" name="setting[post_ping_enabled]" value="1" <?php if ($settings['post_ping_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Pingbacks / Trackbacks enabled</label>
                </fieldset>

            </div>
            */ ?>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>

</form>