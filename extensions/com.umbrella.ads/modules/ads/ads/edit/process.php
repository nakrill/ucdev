<?php

//echo print_r($_POST);
//exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['ad']['id'];

    // USE ad TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['ad']['slug']) {
        $_POST['ad']['slug'] = $_POST['ad']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_ads->makeRewrite($_POST['ad']['slug']),
        'status'            => (int)$_POST['ad']['active'],
        'account_id'        => (int)$_POST['ad']['account_id'],
        'title'             => $_POST['ad']['title'],
        'description'       => $_POST['ad']['description'],
        'keywords'          => $_POST['ad']['keywords'],
        'link'              => $_POST['ad']['link'],
        'target'            => $_POST['ad']['target'],
        'rs_event'          => $_POST['ad']['rs_event']
    );

    // HAVE AD ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ads->tables['ads']. "` SET " .uccms_createSet($columns). ", `dt_saved`=NOW() WHERE (`id`=" .$id. ")";

    // NO AD ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ads->tables['ads']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Ad', 'Ad added!');
        } else {
            $admin->growl('Ad', 'Ad updated!');
        }

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['image']['name']) || ((!$_POST['image']) || (substr($_POST['image'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `image` FROM `" .$_uccms_ads->tables['ads']. "` WHERE (`id`=" .$id. ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['image']) {

                // REMOVE IMAGE
                @unlink($_uccms_ads->imageBridgeOut($ex['image'], 'media', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_ads->tables['ads']. "` SET `image`='' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['image']['name']) || ($_POST['image'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'Ad Image',
                'key'           => 'image',
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_ads->Extension. '/files/media',
                    'image' => true,
                    'thumbs' => array(
                        /*array(
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )*/
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES['image']['name']) {
                $field['file_input'] = $_FILES['image'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['image']) {
                $field['input'] = $_POST['image'];
            }

            // DIGITAL ASSET MANAGER VARS
            $field['dam_vars'] = array(
                'website_extension'         => $_uccms_ads->Extension,
                'website_extension_item'    => 'ad',
                'website_extension_item_id' => $id,
                'title'                     => $_POST['ad']['title'],
                'description'               => $_POST['ad']['description'],
                'link'                      => $_POST['ad']['link']
            );

            $keywords = array();
            if ($_POST['ad']['keywords']) {
                $ka = explode(',', $_POST['ad']['keywords']);
                foreach ($ka as $kw) {
                    $kwc = trim($kw);
                    if ($kwc) {
                        $keywords[] = $kwc;
                    }
                }
            }
            if (count($keywords) > 0) {
                $field['dam_vars']['keywords'] = $keywords;
            }

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_ads->tables['ads']. "` SET `image`='" .sqlescape($_uccms_ads->imageBridgeIn($file_path)). "' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                echo print_r($bigtree['errors']);
                exit;

                $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

            }

        }

        ###########################
        # CITIES
        ###########################

        // HAVE CITIES
        if (count($_POST['city_id']) > 0) {

            $cata = array();

            // GET CURRENT RELATIONS
            $query = "SELECT `city_id` FROM `" .$_uccms_ads->tables['ads_cities']. "` WHERE (`ad_id`=" .$id. ")";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $cata[$row['city_id']] = $row['city_id'];
            }

            // LOOP
            foreach ($_POST['city_id'] as $city_id) {

                // CLEAN UP
                $city_id = (int)$city_id;

                // HAVE CITY ID
                if ($city_id) {

                    // NOT IN EXISTING CITY RELATIONS
                    if (!$cata[$city_id]) {

                        // DB COLUMNS
                        $columns = array(
                            'ad_id'         => $id,
                            'city_id'       => $city_id
                        );

                        // ADD RELATIONSHIP TO DB
                        $query = "INSERT INTO `" .$_uccms_ads->tables['ads_cities']. "` SET " .uccms_createSet($columns). "";
                        sqlquery($query);

                    }

                    // REMOVE FROM RELATIONS
                    unset($cata[$city_id]);

                }

            }

            // HAVE LEFT OVER (OLD) RELATIONS
            if (count($cata) > 0) {

                // LOOP
                foreach ($cata as $city_id) {

                    // REMOVE RELATIONSHIP FROM DB
                    $query = "DELETE FROM `" .$_uccms_ads->tables['ads_cities']. "` WHERE (`ad_id`=" .$id. ") AND (`city_id`=" .$city_id. ")";
                    sqlquery($query);

                }

            }

        // NO CITIES
        } else {

            // REMOVE CITY RELATIONS
            $drel_query = "DELETE FROM `" .$_uccms_ads->tables['ads_cities']. "` WHERE (`ad_id`=" .$id. ")";
            sqlquery($drel_query);

            // REMOVE NEIGHBORHOOD RELATIONS
            $drel_query = "DELETE FROM `" .$_uccms_ads->tables['ads_neighborhoods']. "` WHERE (`ad_id`=" .$id. ")";
            sqlquery($drel_query);

            unset($_POST['neighborhood_id']);

        }


        ###########################
        # NEIGHBORHOODS
        ###########################

        // HAVE NEIGHBORHOODS
        if (count($_POST['neighborhood_id']) > 0) {

            $cata = array();

            // GET CURRENT RELATIONS
            $query = "SELECT `neighborhood_id` FROM `" .$_uccms_ads->tables['ads_neighborhoods']. "` WHERE (`ad_id`=" .$id. ")";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $cata[$row['neighborhood_id']] = $row['neighborhood_id'];
            }

            // LOOP
            foreach ($_POST['neighborhood_id'] as $neighborhood_id) {

                // CLEAN UP
                $neighborhood_id = (int)$neighborhood_id;

                // HAVE NEIGHBORHOOD ID
                if ($neighborhood_id) {

                    // NOT IN EXISTING NEIGHBORHOOD RELATIONS
                    if (!$cata[$neighborhood_id]) {

                        // DB COLUMNS
                        $columns = array(
                            'ad_id'             => $id,
                            'neighborhood_id'   => $neighborhood_id
                        );

                        // ADD RELATIONSHIP TO DB
                        $query = "INSERT INTO `" .$_uccms_ads->tables['ads_neighborhoods']. "` SET " .uccms_createSet($columns). "";
                        sqlquery($query);

                    }

                    // REMOVE FROM RELATIONS
                    unset($cata[$neighborhood_id]);

                }

            }

            // HAVE LEFT OVER (OLD) RELATIONS
            if (count($cata) > 0) {

                // LOOP
                foreach ($cata as $neighborhood_id) {

                    // REMOVE RELATIONSHIP FROM DB
                    $query = "DELETE FROM `" .$_uccms_ads->tables['ads_neighborhoods']. "` WHERE (`ad_id`=" .$id. ") AND (`neighborhood_id`=" .$neighborhood_id. ")";
                    sqlquery($query);

                }

            }

        // NO NEIGHBORHOODS
        } else {

            // REMOVE NEIGHBORHOOD RELATIONS
            $drel_query = "DELETE FROM `" .$_uccms_ads->tables['ads_neighborhoods']. "` WHERE (`ad_id`=" .$id. ")";
            sqlquery($drel_query);

        }


    // QUERY FAILED
    } else {
        $admin->growl('Ad', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'ads/edit/?id=' .$id);

?>