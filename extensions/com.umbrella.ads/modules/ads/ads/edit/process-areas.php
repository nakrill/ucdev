<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['ad']['id'];

    // GET AREAS
    $areas_query = "SELECT * FROM `" .$_uccms_ads->tables['areas']. "`";
    $areas_q = sqlquery($areas_query);
    while ($area = sqlfetch($areas_q)) {

        // DB COLUMNS
        $columns = array(
            'active'                => (int)$_POST['area'][$area['id']]['active'],
            'dt_active'             => ($_POST['area'][$area['id']]['dt_active'] ? date('Y-m-d H:i:s', strtotime($_POST['area'][$area['id']]['dt_active'])) : '0000-00-00 00:00:00'),
            'expire_type'           => $_POST['area'][$area['id']]['expire_type'],
            'expire_dt'             => ($_POST['area'][$area['id']]['expire_dt'] ? date('Y-m-d H:i:s', strtotime($_POST['area'][$area['id']]['expire_dt'])) : '0000-00-00 00:00:00'),
            'expire_impressions'    => (int)$_POST['area'][$area['id']]['expire_impressions'],
            'expire_clicks'         => (int)$_POST['area'][$area['id']]['expire_clicks'],
            'weight'                => (int)$_POST['area'][$area['id']]['weight']
        );

        // GET CURRENT RELATION
        $rel_query = "SELECT * FROM `" .$_uccms_ads->tables['ads_areas']. "` WHERE (`area_id`=" .$area['id']. ") AND (`ad_id`=" .$id. ")";
        $rel_q = sqlquery($rel_query);
        $rel = sqlfetch($rel_q);

        // HAVE RELATION
        if ($rel['id']) {

            // UPDATE RECORD
            $query = "UPDATE `" .$_uccms_ads->tables['ads_areas']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$rel['id']. ")";
            sqlquery($query);

        // NO RELATION
        } else {

            $columns['area_id'] = $area['id'];
            $columns['ad_id']   = $id;

            // INSERT RECORD
            $query = "INSERT INTO `" .$_uccms_ads->tables['ads_areas']. "` SET " .uccms_createSet($columns). "";
            sqlquery($query);

            $rel['id'] = sqlid();

        }

        // HANDLE UPDATING STATUS
        $_uccms_ads->ad_relStatus($rel['id']);

    }

    $admin->growl('Ad', 'Areas saved.');

// NO POST FIELDS
} else {
    $admin->growl('Ad', 'Form not submitted.');
}

BigTree::redirect(MODULE_ROOT.'ads/edit/?id=' .$id. '#areas');

?>