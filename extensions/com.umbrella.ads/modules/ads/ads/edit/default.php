<?php

$ad_cities = array();
$ad_neighborhoods = array();

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET AD INFO
    $ad_query = "SELECT * FROM `" .$_uccms_ads->tables['ads']. "` WHERE (`id`=" .$id. ")";
    $ad_q = sqlquery($ad_query);
    $ad = sqlfetch($ad_q);

    // HAVE AD ID
    if ($ad['id']) {

        /*
        // REVSOCIAL INFO
        if ($ad['revsocial']) {
            $ad['revsocial'] = json_decode(stripslashes($ad['revsocial']), true);
        }
        */

        // GET CITIES
        $cities_query = "SELECT * FROM `" .$_uccms_ads->tables['ads_cities']. "` WHERE (`ad_id`=" .$ad['id']. ")";
        $cities_q = sqlquery($cities_query);
        while ($city = sqlfetch($cities_q)) {
            $ad_cities[$city['city_id']] = $city;
        }

        // GET NEIGHBORHOODS
        $neighborhoods_query = "SELECT * FROM `" .$_uccms_ads->tables['ads_neighborhoods']. "` WHERE (`ad_id`=" .$ad['id']. ")";
        $neighborhoods_q = sqlquery($neighborhoods_query);
        while ($neighborhood = sqlfetch($neighborhoods_q)) {
            $ad_neighborhoods[$neighborhood['neighborhood_id']] = $neighborhood;
        }

    }

}

?>

<style type="text/css">

    #top_stats .left, #stats .totals .left {
        float: left;
        width: 50%;
        text-align: right;
    }
    #top_stats .left .padding, #stats .totals .left .padding {
        margin-right: 20px;
    }
    #top_stats .right, #stats .totals .right {
        float: left;
        width: 50%;
        text-align: left;
    }
    #top_stats .right .padding, #stats .totals .right .padding {
        margin-left: 20px;
    }
    #top_stats .stat .padding, #stats .totals .stat .padding {
        display: inline-block;
        min-width: 150px;
        margin-bottom: 20px;
        padding: 20px 20px 15px;
        background-color: #eee;
        text-align: center;
        border-radius: 5px;
    }
    #top_stats .stat .padding {
        cursor: pointer;
    }
    #top_stats .stat .num, #stats .totals .stat .num {
        display: block;
        margin-bottom: 5px;
        font-size: 2em;
    }
    #top_stats .stat .title, #stats .totals .stat .title {
        display: block;
    }

</style>

<script type="text/javascript">
    $(function() {

        // STATS CLICK
        $('#top_stats .stat .padding').click(function(e) {
            e.preventDefault();
            window.location.href = '#stats';
        });

        // CITY CHECK
        $('input[name="city_id[]"]').click(function() {
            var any_checked = false;
            $('input[name="city_id[]"]').each(function() {
                if ($(this).is(':checked')) {
                    $('#neighborhoods .city-' +$(this).val()).show();
                    any_checked = true;
                } else {
                    $('#neighborhoods .city-' +$(this).val()).hide();
                    $('#neighborhoods .city-' +$(this).val()+ ' input:checkbox').prop('checked', false);
                    $('#neighborhoods .city-' +$(this).val()+ ' .checkbox a').removeClass('checked');
                }
            });
        });

    });
</script>

<?php

// AD EXISTS
if ($ad['id']) {

    // STATS - IMPRESSIONS
    $stat_impressions_sql = "SELECT COUNT(`id`) AS `total` FROM `" .$_uccms_ads->tables['impressions']. "` WHERE (`ad_id`=" .$ad['id']. ")";
    $stat_impressions_query = sqlquery($stat_impressions_sql);
    $stat_impressions = sqlfetch($stat_impressions_query);

    // STATS - CLICKS
    $stat_clicks_sql = "SELECT COUNT(`id`) AS `total` FROM `" .$_uccms_ads->tables['clicks']. "` WHERE (`ad_id`=" .$ad['id']. ")";
    $stat_clicks_query = sqlquery($stat_clicks_sql);
    $stat_clicks = sqlfetch($stat_clicks_query);

    ?>
    <div id="top_stats" class="contain">
        <div class="stat left">
            <div class="padding">
                <span class="num"><?php echo number_format((int)$stat_impressions['total'], 0); ?></span>
                <span class="title">Impression<?php if ($stat_impressions['total'] != 1) { ?>s<?php } ?></span>
            </div>
        </div>
        <div class="stat right">
            <div class="padding">
                <span class="num"><?php echo number_format((int)$stat_clicks['total'], 0); ?></span>
                <span class="title">Click<?php if ($stat_clicks['total'] != 1) { ?>s<?php } ?></span>
            </div>
        </div>
    </div>
    <?php
}
?>

<div class="container">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="ad[id]" value="<?php echo $ad['id']; ?>" />

    <header>
        <h2><?php if ($ad['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Ad</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Title</label>
                    <input type="text" name="ad[title]" value="<?php echo stripslashes($ad['title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Description</label>
                    <textarea name="ad[description]" style="height: 80px;"><?php echo stripslashes($ad['description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Keywords</label>
                    <textarea name="ad[keywords]" style="height: 80px;"><?php echo stripslashes($ad['keywords']); ?></textarea>
                    <label class="note">
                        Separate with commas.
                    </label>
                </fieldset>

                <?php if ($_uccms['_account']->isEnabled()) { ?>

                    <div style="margin-bottom: 15px; padding: 10px; background-color: #f9f9f9;">
                        <fieldset>
                            <label>Account</label>
                            <select name="ad[account_id]">
                                <option value="">None</option>
                                <optgroup label="Accounts">
                                    <?php
                                    $accounts_query = "
                                    SELECT a.id, a.username, a.email, CONCAT(ad.firstname, ' ', ad.lastname) AS `fullname`
                                    FROM `uccms_accounts` AS `a`
                                    INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                                    ORDER BY `fullname` ASC, a.username ASC
                                    ";
                                    $accounts_q = sqlquery($accounts_query);
                                    while ($acct = sqlfetch($accounts_q)) {
                                        ?>
                                        <option value="<?php echo $acct['id']; ?>" <?php if ($acct['id'] == $ad['account_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($acct['fullname']); ?> (<?php echo $acct['email']; ?>)</option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                            </select>
                        </fieldset>
                    </div>

                <?php } ?>

                <fieldset>
                    <label>Cities</label>
                    <div class="items checkboxes">
                        <?php
                        $city_query = "SELECT * FROM `" .$_uccms_ads->tables['cities']. "` ORDER BY `title` ASC, `id` ASC";
                        $city_q = sqlquery($city_query);
                        while ($city = sqlfetch($city_q)) {
                            ?>
                            <div class="item"><input type="checkbox" name="city_id[]" value="<?php echo $city['id']; ?>" <?php if ($ad_cities[$city['id']]['id']) { ?>checked="checked"<?php } ?> /> <label class="for-checkbox"><?php echo stripslashes($city['title']); ?></label></div>
                            <?php
                        }
                        ?>
                    </div>
                </fieldset>

                <fieldset id="neighborhoods">
                    <label>Neighborhoods</label>
                    <div class="items checkboxes">
                        <?php
                        $neighborhood_query = "SELECT * FROM `" .$_uccms_ads->tables['neighborhoods']. "` ORDER BY `title` ASC, `id` ASC";
                        $neighborhood_q = sqlquery($neighborhood_query);
                        while ($neighborhood = sqlfetch($neighborhood_q)) {
                            ?>
                            <div class="item city-<?php echo $neighborhood['city_id']; ?>" style="<?php if (!$ad_cities[$neighborhood['city_id']]['id']) { ?>display: none;<?php } ?>">
                                <input type="checkbox" name="neighborhood_id[]" value="<?php echo $neighborhood['id']; ?>" <?php if ($ad_neighborhoods[$neighborhood['id']]['id']) { ?>checked="checked"<?php } ?> /> <label class="for-checkbox"><?php echo stripslashes($neighborhood['title']); ?></label>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </fieldset>

            </div>

            <div class="right">

                <fieldset>
                    <input type="checkbox" name="ad[active]" value="1" <?php if ($ad['status'] == 1) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

                <fieldset>
                    <label>Image</label>
                    <div>
                        <?php
                        $bigtree['form']['embedded'] = true;
                        $field = array(
                            'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                            'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                            'key'       => 'image', // The value you should use for the "name" attribute of your form field
                            'value'     => ($ad['image'] ? $_uccms_ads->imageBridgeOut($ad['image'], 'media') : ''), // The existing value for this form field
                            'id'        => 'ad_image', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'image' => true
                            ),
                            'required'  => false // A boolean value of whether this form field is required or not
                        );
                        include(BigTree::path('admin/form-field-types/draw/upload.php'));
                        ?>
                    </div>
                </fieldset>

                <fieldset>
                    <label>Link</label>
                    <input type="text" name="ad[link]" value="<?php echo stripslashes($ad['link']); ?>" placeholder="http://" />
                </fieldset>

                <fieldset>
                    <label>Link Target</label>
                    <select name="ad[target]">
                        <option value="n" <?php if ($ad['target'] == 'n') { ?>selected="selected"<?php } ?>>New Window</option>
                        <option value="s" <?php if ($ad['target'] == 's') { ?>selected="selected"<?php } ?>>Same Window</option>
                    </select>
                </fieldset>

            </div>

        </div>

        <? /*
        <div class="contain">
            <h3 class="uccms_toggle" data-what="revsocial"><span>RevSocial</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-revsocial" style="display: none;">

                <fieldset>
                    <label>RevSocial Event</label>
                    <input type="text" name="ad[rs_event]" value="<?php echo stripslashes($ad['rs_event']); ?>" placeholder="ad-impression" />
                </fieldset>

            </div>
        </div>
        */ ?>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>

<?php

// HAVE AD ID
if ($ad['id']) {

    // AREAS ARRAY
    $areaa = array();

    // GET AREAS
    $areas_query = "
    SELECT a.id, a.title
    FROM `" .$_uccms_ads->tables['areas']. "` AS `a`
    WHERE (a.active=1)
    ORDER BY a.title ASC
    ";
    $areas_q = sqlquery($areas_query);
    while ($area = sqlfetch($areas_q)) {
        $areaa[$area['id']] = $area;
    }

    ?>

    <style type="text/css">

        #areas header span {
            text-align: left;
        }
        #areas .area_title {
            width: 330px;
        }
        #areas .area_title .status-active {
            color: #81c784;
        }
        #areas .area_title .status-inactive {
            color: #bbb;
        }
        #areas .area_title .status-scheduled {
            color: #81d4fa;
        }
        #areas .area_title .status-expired {
            color: #ffcc80;
        }
        #areas .area_weight {
            width: 95px;
        }
        #areas .area_active {
            width: 190px;
        }
        #areas .area_expire_type {
            width: 140px;
        }
        #areas .area_expire {
            width: 180px;
        }
        #areas ul li {
            height: 43px;
        }
        #areas ul li section {
            height: auto;
            padding-top: 5px;
            text-align: left;
        }
        #areas .checkbox {
            margin-top: 4px;
        }
        #areas label.for_checkbox {
            line-height: 35px;
        }

        #areas input.date_picker, #areas input.time_picker, #areas input.date_time_picker {
            width: 150px !important;
        }
        #areas .icon_small_calendar {
            margin: 8px 0 0 -24px;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            $('#areas .item .area_expire_type select').change(function() {
                $('#areas .item .area_expire .type').hide();
                $('#areas .item .area_expire .type.' +$(this).val()).show();
            });

        });

    </script>

    <a name="areas"></a>

    <form enctype="multipart/form-data" action="./process-areas/" method="post">
    <input type="hidden" name="ad[id]" value="<?php echo $ad['id']; ?>" />

    <div class="container" style="margin-bottom: 0px; border: 0px none;">

        <div id="areas" class="table">

            <summary>
                <h2>Areas</h2>
            </summary>

            <header style="clear: both;">
                <span class="area_title">Area</span>
                <span class="area_weight">Weight</span>
                <span class="area_active">Active</span>
                <span class="area_expire_type">Expire</span>
                <span class="area_expire"></span>
            </header>

            <ul class="items clearfix">
                <?php

                // LOOP THROUGH AREAS
                foreach ($areaa as $area) {

                    unset($rel);

                    // GET RELATION
                    $rel_query = "SELECT * FROM `" .$_uccms_ads->tables['ads_areas']. "` WHERE (`area_id`=" .$area['id']. ") AND (`ad_id`=" .$ad['id']. ") LIMIT 1";
                    $rel_q = sqlquery($rel_query);
                    $rel = sqlfetch($rel_q);

                    ?>

                    <li class="item">
                        <section class="area_title">
                            <fieldset class="last">
                                <input type="checkbox" name="area[<?php echo $area['id']; ?>][active]" value="1" <?php if ($rel['active'] == 1) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox status-<?php echo strtolower($_uccms_ads->statuses[(int)$rel['active']]); ?>" title="<?php echo $_uccms_ads->statuses[(int)$rel['status']]; ?>"><?php echo stripslashes($area['title']); ?></label>
                            </fieldset>
                        </section>
                        <section class="area_weight">
                            <select name="area[<?php echo $area['id']; ?>][weight]">
                                <?php for ($i=1; $i<=10; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php if ($rel['weight'] == $i) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </section>
                        <section class="area_active">
                            <div>
                                <?php

                                // FIELD VALUES
                                $field = array(
                                    'id'        => 'area-' .$area['id']. '-active',
                                    'key'       => 'area[' .$area['id']. '][dt_active]',
                                    'value'     => (($rel['dt_active'] != '0000-00-00 00:00:00') ? $rel['dt_active'] : ''),
                                    'required'  => true,
                                    'options'   => array(
                                        'default_today' => false
                                    )
                                );

                                // INCLUDE FIELD
                                include(BigTree::path('admin/form-field-types/draw/datetime.php'));

                                ?>
                            </div>
                        </section>
                        <section class="area_expire_type">
                            <select name="area[<?php echo $area['id']; ?>][expire_type]">
                                <option value="">Never</option>
                                <option value="d" <?php if ($rel['expire_type'] == 'd') { ?>selected="selected"<?php } ?>>Date & Time</option>
                                <option value="i" <?php if ($rel['expire_type'] == 'i') { ?>selected="selected"<?php } ?>>Impressions</option>
                                <option value="c" <?php if ($rel['expire_type'] == 'c') { ?>selected="selected"<?php } ?>>Clicks</option>
                            </select>
                        </section>
                        <section class="area_expire">
                            <div class="type d" style="<?php if ($rel['expire_type'] != 'd') { ?>display: none;<?php } ?>">
                                <?php

                                // FIELD VALUES
                                $field = array(
                                    'id'        => 'area-' .$area['id']. '-expire',
                                    'key'       => 'area[' .$area['id']. '][expire_dt]',
                                    'value'     => (($rel['expire_dt'] != '0000-00-00 00:00:00') ? $rel['expire_dt'] : ''),
                                    'required'  => true,
                                    'options'   => array(
                                        'default_today' => false
                                    )
                                );

                                // INCLUDE FIELD
                                include(BigTree::path('admin/form-field-types/draw/datetime.php'));

                                ?>
                            </div>
                            <div class="type i" style="<?php if ($rel['expire_type'] != 'i') { ?>display: none;<?php } ?>">
                                <input type="text" name="area[<?php echo $area['id']; ?>][expire_impressions]" value="<?php echo (int)$rel['expire_impressions']; ?>" style="width: 80px;" />
                            </div>
                            <div class="type c" style="<?php if ($rel['expire_type'] != 'c') { ?>display: none;<?php } ?>">
                                <input type="text" name="area[<?php echo $area['id']; ?>][expire_clicks]" value="<?php echo (int)$rel['expire_clicks']; ?>" style="width: 80px;" />
                            </div>
                        </section>
                    </li>

                    <?php

                }

                ?>
            </ul>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

        </div>

    </div>

    </form>

    <style type="text/css">

        #stats header .select {
            float: right;
            margin: 6px 6px 0 0;
        }

    </style>

    <script src="/js/highcharts.js"></script>

    <script type="text/javascript">
        $(function() {

            // AREA SELECT CHANGE
            $('#stats header select').change(function() {
                $('#stats .area-container').hide();
                $('#stats .area-' +$(this).val()).show();
            });

        });
    </script>

    <a name="stats"></a>
    <div id="stats" class="container">

        <header>
            <h2>Stats (past 30 days)</h2>
            <select name="area" style="float: right;">
                <option value="all">All Areas</option>
                <?php foreach ($areaa as $area) { ?>
                    <option value="<?php echo $area['id']; ?>"><?php echo stripslashes($area['title']); ?></option>
                <?php } ?>
            </select>
        </header>

        <section>

            <?php

            unset($tfa);
            $tfa = array();

            // PAST 30 DAYS
            for ($i=30; $i>=0; $i--) {
                $day = strtotime('-' .$i. ' Days');
                $tfa[$i]['from'] = date('Y-m-d 00:00:00', $day);
                $tfa[$i]['to'] = date('Y-m-d 23:59:59', $day);
            }

            unset($tvals);
            $tvals = array();

            // LOOP
            foreach ($tfa as $tf) {

                // COLUMN TITLE
                $col = strtotime($tf['from']);

                // GET TOTAL STATS - IMPRESSIONS
                $impressions_query = "
                SELECT `id` FROM `" .$_uccms_ads->tables['impressions']. "`
                WHERE (`ad_id`=" .$ad['id']. ") AND ((`dt`>='" .$tf['from']. "') AND (`dt`<='" .$tf['to']. "'))
                ";
                $impressions_q = sqlquery($impressions_query);

                $tvals['all'][$col]['impressions'] = sqlrows($impressions_q);

                // GET TOTAL STATS - CLICKS
                $clicks_query = "
                SELECT `id` FROM `" .$_uccms_ads->tables['clicks']. "`
                WHERE (`ad_id`=" .$ad['id']. ") AND ((`dt`>='" .$tf['from']. "') AND (`dt`<='" .$tf['to']. "'))
                ";
                $clicks_q = sqlquery($clicks_query);

                $tvals['all'][$col]['clicks'] = sqlrows($clicks_q);

                // LOOP THROUGH AREAS
                foreach ($areaa as $area) {

                    // GET AREA STATS - IMPRESSIONS
                    $impressions_query = "
                    SELECT `id` FROM `" .$_uccms_ads->tables['impressions']. "`
                    WHERE (`ad_id`=" .$ad['id']. ") AND (`area_id`=" .$area['id']. ") AND ((`dt`>='" .$tf['from']. "') AND (`dt`<='" .$tf['to']. "'))
                    ";
                    $impressions_q = sqlquery($impressions_query);

                    $tvals[$area['id']][$col]['impressions'] = sqlrows($impressions_q);

                    // GET AREA STATS - CLICKS
                    $clicks_query = "
                    SELECT `id` FROM `" .$_uccms_ads->tables['clicks']. "`
                    WHERE (`ad_id`=" .$ad['id']. ") AND (`area_id`=" .$area['id']. ") AND ((`dt`>='" .$tf['from']. "') AND (`dt`<='" .$tf['to']. "'))
                    ";
                    $clicks_q = sqlquery($clicks_query);

                    $tvals[$area['id']][$col]['clicks'] = sqlrows($clicks_q);

                }

            }

            // LOOP THROUGH RESULTS
            foreach ($tvals as $area_id => $cols) {

                $total_impressions = 0;
                $total_clicks = 0;

                // LOOP THROUGH VALUES
                foreach ($cols as $col_name => $val) {
                    $g_cola[] = date('j', $col_name);
                    $g_impressionsa[] = (int)$val['impressions'];
                    $g_clicksa[] = (int)$val['clicks'];
                    $total_impressions += (int)$val['impressions'];
                    $total_clicks += (int)$val['clicks'];
                }

                unset($tvals);

                // PUT GRAPH COLUMNS TOGETHER
                $g_cols = "'" .implode("','", $g_cola). "'";

                // PUT GRAPH IMPRESSION VALUES TOGETHER
                $g_impressions = implode(',', $g_impressionsa);

                // PUT GRAPH CLICKS VALUES TOGETHER
                $g_clicks = implode(',', $g_clicksa);

                unset($g_cola);
                unset($g_impressionsa);
                unset($g_clicksa);

                ?>

                <script type="text/javascript">
                    $(function() {
                        $('#stats .area-<?php echo $area_id; ?> .graph-container .graph').highcharts({
                            chart: {
                                margin: [15, 50, 30, 55],
                                backgroundColor: '#ffffff'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: [{
                                categories: [<?php echo $g_cols; ?>]
                            }],
                            yAxis: [{ // Left yAxis
                                title: {
                                    text: 'Impressions',
                                    style: {
                                        color: '#89A54E'
                                    }
                                },
                                labels: {
                                    format: '{value}',
                                    style: {
                                        color: '#89A54E'
                                    }
                                },
                                min: 0
                            }, { // Right yAxis
                                title: {
                                    text: 'Clicks',
                                    style: {
                                        color: '#4572A7'
                                    }
                                },
                                labels: {
                                    format: '{value}',
                                    style: {
                                        color: '#4572A7'
                                    }
                                },
                                min: 0,
                                opposite: true
                            }],
                            tooltip: {
                                shared: true
                            },
                            legend: {
                                enabled: false,
                                layout: 'vertical',
                                align: 'left',
                                x: 120,
                                verticalAlign: 'top',
                                y: 100,
                                floating: true,
                                backgroundColor: '#FFFFFF'
                            },
                            series: [
                                {
                                    name: 'Impressions',
                                    color: '#89A54E',
                                    type: 'spline',
                                    data: [<?php echo $g_impressions; ?>],
                                    tooltip: {
                                    }
                                },{
                                    name: 'Clicks',
                                    color: '#4572A7',
                                    type: 'column',
                                    yAxis: 1,
                                    data: [<?php echo $g_clicks; ?>],
                                    tooltip: {
                                    }
                                }
                            ],
                            credits: {
                                enabled: false
                            }
                        });
                    });
                </script>

                <div class="area-container area-<?php echo $area_id; ?>" <?php if ($area_id != 'all') { ?>style="display: none;"<?php } ?>>
                    <div class="totals" class="contain">
                        <div class="stat left">
                            <div class="padding">
                                <span class="num"><?php echo number_format($total_impressions, 0); ?></span>
                                <span class="title">Impression<?php if ($total_impressions != 1) { ?>s<?php } ?></span>
                            </div>
                        </div>
                        <div class="stat right">
                            <div class="padding">
                                <span class="num"><?php echo number_format($total_clicks, 0); ?></span>
                                <span class="title">Click<?php if ($total_clicks != 1) { ?>s<?php } ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="graph-container">
                        <div class="graph"></div>
                    </div>
                </div>

                <?php

            }

            ?>

        </section>

    </div>



<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>