<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE AD
    $query = "UPDATE `" .$_uccms_ads->tables['ads']. "` SET " .uccms_createSet($columns). ", `dt_deleted`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Ad', 'Ad deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Ad', 'Failed to delete ad.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Ad', 'No ad specified.');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT.'ads/');
}

?>