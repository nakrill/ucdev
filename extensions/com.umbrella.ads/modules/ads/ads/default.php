<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Ads', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #items header span, #items .item section {
        text-align: left;
    }

    #items .ad_titlex {
        width: 340px;
    }
    #items .ad_area {
        width: 400px;
    }
    #items .ad_tags {
        width: 200px;
    }
    #items .ad_date {
        width: 130px;
    }
    #items .ad_status {
        width: 90px;
    }
    #items .ad_edit {
        width: 55px;
    }
    #items .ad_delete {
        width: 55px;
    }

    #results .ad_area .area_status {
        font-size: 1em !important;
    }
    #results .ad_area .area_status.status-active {
        color: #81c784;
    }
    #results .ad_area .area_status.status-inactive {
        color: #bbb;
    }
    #results .ad_area .area_status.status-scheduled {
        color: #81d4fa;
    }
    #results .ad_area .area_status.status-expired {
        color: #ffcc80;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#ads .status select').change(function() {
            $('#form_ads').submit();
        });

    });

</script>

<div id="ads">

    <form id="form_ads">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <div class="status contain">
        <div style="float: right; padding-left: 10px;">
            <select name="status">
                <option value="">Status (All)</option>
                <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
                <? /*
                <option value="scheduled" <?php if ($_REQUEST['status'] == 'scheduled') { ?>selected="selected"<?php } ?>>Secheduled</option>
                <option value="expired" <?php if ($_REQUEST['status'] == 'expired') { ?>selected="selected"<?php } ?>>Expired</option>
                */ ?>
            </select>
        </div>
        <div style="float: right; padding-left: 10px;">
            <select name="area_id">
                <option value="">Area (All)</option>
                <?php
                $area_query = "SELECT `id`, `title` FROM `" .$_uccms_ads->tables['areas']. "` WHERE (`active`=1) ORDER BY `title` ASC";
                $area_q = sqlquery($area_query);
                while ($area = sqlfetch($area_q)) {
                    ?>
                    <option value="<?php echo $area['id']; ?>" <?php if ($area['id'] == $_REQUEST['area_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($area['title']); ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Ads</h2>
            <a class="add_resource add" href="./edit/"><span></span>Add Ad</a>
        </summary>
        <header style="clear: both;">
            <span class="ad_titlex">Title</span>
            <span class="ad_area">Areas</span>
            <span class="ad_status">Status</span>
            <span class="ad_edit">Edit</span>
            <span class="ad_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/ads/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_ads->Extension?>/ajax/admin/ads/get-page/?page=1&status=" +escape($('#ads .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>&tag_id=<?=$_REQUEST['tag_id']?>&author_id=<?=$_REQUEST['author_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_ads->Extension?>/ajax/admin/ads/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=" +escape($('#ads .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>&tag_id=<?=$_REQUEST['tag_id']?>&author_id=<?=$_REQUEST['author_id']?>");
        return false;
    });
</script>