<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_ads->tables['post_areas']. "` WHERE (`area_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_ads->tables['areas']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Area', 'Area deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Area', 'Failed to delete area.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Area', 'No area specified.');
}

BigTree::redirect(MODULE_ROOT.'areas/');

?>