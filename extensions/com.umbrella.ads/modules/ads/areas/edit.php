<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET AREA INFO
    $area_query = "SELECT * FROM `" .$_uccms_ads->tables['areas']. "` WHERE (`id`=" .$id. ")";
    $area_q = sqlquery($area_query);
    $area = sqlfetch($area_q);

}

?>

<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

</style>

<div class="container">

    <form enctype="multipart/form-data" action="../process/" method="post">
    <input type="hidden" name="area[id]" value="<?php echo $area['id']; ?>" />

    <header>
        <h2><?php if ($area['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Area</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Title</label>
                    <input type="text" name="area[title]" value="<?php echo stripslashes($area['title']); ?>" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="area[active]" value="1" <?php if ($area['active']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

            </div>

            <div class="right">

            </div>

        </div>

        <?php if ($area['id']) { ?>

            <?php $unique_id = time(); ?>

            <div class="contain">
                <h3 class="uccms_toggle" data-what="code"><span>Code</span><span class="icon_small icon_small_caret_down"></span></h3>
                <div class="contain uccms_toggle-code" style="display: none;">

                    <fieldset>
                        <label>Javascript Embed Code</label>

<textarea>
<script src="/extensions/<?=$_uccms_ads->Extension;?>/js/embed.js"></script>
<script type="text/javascript" id="ae-area-<?php echo $unique_id; ?>">
    $(function() {
        displayAEMedia(<?php echo $area['id']; ?>, 'ae-area-<?php echo $unique_id; ?>', {});
    });
</script></textarea>

                    </fieldset>

                    <fieldset>
                        <label>HTML Embed Code</label>

<textarea>
<?php echo '<?php'. "\n"; ?>
$ad_area_id = <?php echo $area['id']; ?>;
$ad_area_params = array();
include(SERVER_ROOT. 'extensions/com.umbrella.ads/embed.php');
?></textarea>

                    </fieldset>

                </div>
            </div>

        <?php } ?>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>