<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Areas', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

    #areas .area_title {
        width: 530px;
    }
    #areas .area_ads {
        width: 160px;
    }
    #areas .area_status {
        width: 140px;
    }
    #areas .area_edit {
        width: 55px;
    }
    #areas .area_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#areas .status select').change(function() {
            $('#form_areas').submit();
        });

    });

</script>

<div id="areas">

    <form id="form_areas">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <div class="status contain">
        <div style="float: right;">
            <select name="status">
                <option value="">All</option>
                <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
            </select>
        </div>
    </div>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Areas</h2>
            <a class="add_resource add" href="./edit/"><span></span>Add Area</a>
        </summary>
        <header style="clear: both;">
            <span class="area_title">Title</span>
            <span class="area_ads">Ads</span>
            <span class="area_status">Status</span>
            <span class="area_edit">Edit</span>
            <span class="area_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/areas/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_ads->Extension?>/ajax/admin/areas/get-page/?page=1&status=" +escape($('#areas .status select').val())+ "&query=" +escape($("#query").val())+ "&area_id=<?=$_REQUEST['area_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_ads->Extension?>/ajax/admin/areas/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=" +escape($('#areas .status select').val())+ "&query=" +escape($("#query").val())+ "&area_id=<?=$_REQUEST['area_id']?>");
        return false;
    });
</script>