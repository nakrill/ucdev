<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['area']['id'];

    // USE AREA TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['area']['slug']) {
        $_POST['area']['slug'] = $_POST['area']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_ads->makeRewrite($_POST['area']['slug']),
        'active'            => (int)$_POST['area']['active'],
        'title'             => $_POST['area']['title']
    );

    // HAVE AREA ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ads->tables['areas']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO area ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ads->tables['areas']. "` SET " .uccms_createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Area', 'Area added!');
        } else {
            $admin->growl('Area', 'Area updated!');
        }

    // QUERY FAILED
    } else {
        $admin->growl('Area', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'areas/edit/?id=' .$id);

?>