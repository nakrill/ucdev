<?php

// BIGTREE
$bigtree['css'][]   = 'master.css';
$bigtree['js'][]    = 'master.js';

// MODULE CLASS
$_uccms_ads = new uccms_Ads;

// BREADCRUMBS
$bigtree['breadcrumb'] = [
    [ 'title' => 'Ads', 'link' => $bigtree['path'][1] ],
];

// MODULE MAIN NAV SELECTED ARRAY
if ($bigtree['path'][2]) {
    $mmnsa[$bigtree['path'][2]] = 'active';
} else {
    $mmnsa['dashboard'] = 'active';
}

?>

<nav class="main">
    <section>
        <ul>
            <li class="<?php echo $mmnsa['dashboard']; ?>">
                <a href="<?=MODULE_ROOT;?>"><span class="dashboard"></span>Dashboard</a>
                <?php /*
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>calendar/">Calendar</a></li>
                </ul>
                */ ?>
            </li>
            <li class="<?php echo $mmnsa['locations']; ?>">
                <a href="<?=MODULE_ROOT;?>locations/"><span class="developer"></span>Locations</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>locations/">Cities & Neighborhoods</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['areas']; ?>">
                <a href="<?=MODULE_ROOT;?>areas/"><span class="pages"></span>Areas</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>areas/">All</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>areas/edit/">New Area</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['ads']; ?>">
                <a href="<?=MODULE_ROOT;?>ads/"><span class="developer"></span>Ads</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>ads/">All</a></li>
                    <li><a href="<?=MODULE_ROOT;?>ads/?status=active">Active</a></li>
                    <li><a href="<?=MODULE_ROOT;?>ads/?status=inactive">Inactive</a></li>
                    <? /*
                    <li><a href="<?=MODULE_ROOT;?>ads/?status=active">Published</a></li>
                    <li><a href="<?=MODULE_ROOT;?>ads/?status=scheduled">Scheduled</a></li>
                    <li><a href="<?=MODULE_ROOT;?>ads/?status=inactive">Inactive</a></li>
                    <li><a href="<?=MODULE_ROOT;?>ads/?status=expired">Expired</a></li>
                    */ ?>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>ads/edit/?id=new">New Ad</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['settings']; ?>">
                <a href="<?=MODULE_ROOT;?>settings/"><span class="settings"></span>Settings</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                </ul>
            </li>
        </ul>
    </section>
</nav>
