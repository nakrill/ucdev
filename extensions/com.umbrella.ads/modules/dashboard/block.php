<?php

// MODULE CLASS
$_uccms_ads = new uccms_Ads;

// HAS ACCESS
if ($_uccms_ads->adminModulePermission()) {

    ?>

    <style type="text/css">

        #block_ads #latest_ads header span, #block_ads #latest_ads .item section {
            text-align: left;
        }

        #block_ads #latest_ads .ad_titlex {
            width: 240px;
        }
        #block_ads #latest_ads .ad_area {
            width: 400px;
        }
        #block_ads #latest_ads .ad_tags {
            width: 200px;
        }
        #block_ads #latest_ads .ad_date {
            width: 130px;
        }
        #block_ads #latest_ads .ad_status {
            width: 190px;
        }
        #block_ads #latest_ads .ad_edit {
            width: 55px;
        }
        #block_ads #latest_ads .ad_delete {
            width: 55px;
        }

        #block_ads #latest_ads .items .ad_area .area_status {
            font-size: 1em !important;
        }
        #block_ads #latest_ads .items .ad_area .area_status.status-active {
            color: #81c784;
        }
        #block_ads #latest_ads .items .ad_area .area_status.status-inactive {
            color: #bbb;
        }
        #block_ads #latest_ads .items .ad_area .area_status.status-scheduled {
            color: #81d4fa;
        }
        #block_ads #latest_ads .items .ad_area .area_status.status-expired {
            color: #ffcc80;
        }

    </style>

    <div id="block_ads" class="block">
        <h2><a href="../<?php echo $extension; ?>*ads/">Ads</a></h2>
        <div id="latest_ads" class="table">
            <summary>
                <h2>Latest Ads</h2>
                <a class="add_resource add" href="../<?php echo $extension; ?>*ads/ads/">
                    View All
                </a>
            </summary>
            <header style="clear: both;">
                <span class="ad_titlex">Title</span>
                <span class="ad_area">Areas</span>
                <span class="ad_status">Status</span>
                <span class="ad_edit">Edit</span>
                <span class="ad_delete">Delete</span>
            </header>
            <ul class="items">
                <?php
                $_GET['base_url'] = '../' .$extension. '*ads/ads';
                $_GET['limit'] = 5;
                include($extension_dir. '/ajax/admin/ads/get-page.php');
                ?>
            </ul>
        </div>
    </div>

    <?php

}

unset($_uccms_ads);

?>