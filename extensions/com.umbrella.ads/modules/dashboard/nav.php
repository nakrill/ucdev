<?php

$extension_nav =
["link" => "com.umbrella.ads*ads", "title" => "Ads", "access" => 1, "children" => [
    ["link" => ".", "title" => "Dashboard", "access" => 1],
    ["link" => "locations", "title" => "Locations", "access" => 1],
    ["link" => "areas", "title" => "Areas", "access" => 1],
    ["link" => "ads", "title" => "Ads", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>