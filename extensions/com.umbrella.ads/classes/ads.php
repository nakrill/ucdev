<?
    class uccms_Ads extends BigTreeModule {

        var $Table = "";

        var $Extension = "com.umbrella.ads";

        // DB TABLES
        var $tables = array(
            'ads'                   => 'uccms_ads_ads',
            'ads_areas'             => 'uccms_ads_ad_area_rel',
            'ads_cities'            => 'uccms_ads_ad_city_rel',
            'ads_neighborhoods'     => 'uccms_ads_ad_neighborhood_rel',
            'areas'                 => 'uccms_ads_areas',
            'cities'                => 'uccms_ads_cities',
            'clicks'                => 'uccms_ads_log_clicks',
            'impressions'           => 'uccms_ads_log_impressions',
            'neighborhoods'         => 'uccms_ads_neighborhoods',
            'settings'              => 'uccms_blog_settings'
        );

        // POST STATUSES
        var $statuses = array(
            0 => 'Inactive',
            1 => 'Active',
            2 => 'Scheduled',
            7 => 'Expired',
            9 => 'Deleted'
        );

        var $settings, $misc;

        // START AS 0, WILL ADJUST LATER
        static $PerPage = 0;


        function __construct() {

        }


        #############################
        # GENERAL - MISC
        #############################


        // MODULE ID
        public function moduleID() {
            if (!isset($this->settings['module_id'])) {
                $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
            }
            return $this->settings['module_id'];
        }


        // ADMIN USER ID
        public function adminID() {
            return $_SESSION['bigtree_admin']['id'];
        }


        // ADMIN ACCESS LEVEL
        public function adminModulePermission() {
            global $admin;
            if (!isset($this->settings['admin_module_permission'])) {
                $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
            }
            return $this->settings['admin_module_permission'];
        }


        // STRING IS JSON
        public function isJson($string) {
            if (substr($string, 0, 2) == '{"') {
                return true;
            }
        }


        // CONVERTS A STRING INTO A REWRITE-ABLE URL
        public function makeRewrite($string) {

            $string = strtolower(trim($string));

            // REMOVE ALL QUOTES
            $string = str_replace("'", '', $string);
            $string = str_replace('"', '', $string);

            // STRIP ALL NON WORD CHARS
            $string = preg_replace('/\W/', ' ', $string);

            // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
            $string = preg_replace('/\ +/', '-', $string);

            // TRIM DASHES
            $string = preg_replace('/\-$/', '', $string);
            $string = preg_replace('/^\-/', '', $string);

            return $string;

        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
        public function imageBridgeIn($string) {
            $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $dpa = explode('/', $string);
                return array_pop($dpa);
                //$what = array_pop($dpa);
            }
        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
        public function imageBridgeOut($string, $what, $file=false) {
            $string = stripslashes($string);
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $base = ($file ? SITE_ROOT : STATIC_ROOT);
                return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
            }
        }


        // CONVERT BIGTREE FILE LOCATION TO FULL URL
        public function bigtreeFileURL($file) {
            return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
        }


        // SET NUMBER PER PAGE
        public function setPerPage($num) {
            static::$PerPage = (int)$num;
        }


        // NUMBER PER PAGE
        public function perPage() {
            global $cms;
            if (!static::$PerPage) {
                $pp = $this->getSetting('ads_per_page');
                if ($pp) {
                    $this->setPerPage($pp);
                }
            }
            if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
            if (!static::$PerPage) $this->setPerPage(15);
            return static::$PerPage;
        }


        // GET NUMBER OF PAGES FROM QUERY
        public function pageCount($num) {
            return ceil($num / $this->perPage());
        }


        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            );
        }


        // TRIM URL
        public function trimURL($string) {
            return trim(preg_replace('#^https?://#', '', $string));
        }


        // CONVERT ARRAY TO CSV
        public function arrayToCSV($array=array()) {
            if (is_array($array)) {
                return implode(',', $array);
            }
        }


        #############################
        # GENERAL - SETTINGS
        #############################


        // GET SETTING
        public function getSetting($id='') {
            $id = sqlescape($id);
            if ($id) {
                if (isset($this->settings[$id])) {
                    return $this->settings[$id];
                } else {
                    $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tables['settings']. "` WHERE `id`='" .$id. "'"));
                    if ($this->isJson($f['value'])) {
                        $val = json_decode($f['value'], true);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$id] = $val;
                }
                return $val;
            }
        }


        // GET SETTINGS
        public function getSettings($array='') {
            $out = array();
            if (is_array($array)) {
                $oa = array();
                foreach ($array as $id) {
                    $id = sqlescape($id);
                    if ($id) {
                        $oa[] = "`id`='" .$id. "'";
                    }
                }
                if (count($oa) > 0) {
                    $or = "(" .implode(") OR (", $oa). ")";
                }
                if ($or) {
                    $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` WHERE " .$or;
                }
            } else {
                $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` ORDER BY `id` ASC";
            }
            if ($query) {
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    if ($this->isJson($string)) {
                        $val = json_decode($f['value']);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$f['id']] = $val;
                    $out[$f['id']] = $val;
                }
            }
            return $out;
        }


        // SET SETTING
        public function setSetting($id='', $value='') {

            // CLEAN UP
            $id = sqlescape($id);

            // HAVE ID
            if ($id) {

                // VALUE IS ARRAY
                if (is_array($value)) {
                    $value = json_encode($value);

                // VALUE IS STRING
                } else {
                    $value = sqlescape($value);
                }

                $query = "
                INSERT INTO `" .$this->tables['settings']. "`
                SET `id`='" .$id. "', `value`='" .$value. "'
                ON DUPLICATE KEY UPDATE `value`='" .$value. "'
                ";
                if (sqlquery($query)) {
                    return true;
                }

            }

        }


        // FRONTEND PATH
        public function frontendPath() {
            if (!isset($this->settings['frontend_path'])) {
                $this->settings['frontend_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*ads')"))['path']);
            }
            if (!$this->settings['frontend_path']) $this->settings['frontend_path'] = 'blog';
            return $this->settings['frontend_path'];
        }


        /*
        #############################
        # CITY
        #############################


        // GENERATE CITY URL
        public function cityURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            return '/' .$this->frontendPath(). '/' .$data['slug']. '/';
        }


        #############################
        # NEIGHBORHOOD
        #############################


        // GENERATE NEIGHBORHOOD URL
        public function neighborhoodURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            $city_query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .$data['city_id']. ")";
            $city_data = sqlfetch(sqlquery($city_query));
            return '/' .$this->frontendPath(). '/' .$city_data['slug']. '/' .$data['slug']. '/';
        }
        */


        #############################
        # AREA
        #############################


        // GET ADS IN AREA
        public function areaGetAds($id, $params=array()) {

            $out = array();

            $id = (int)$id;

            // HAVE AREA ID
            if ($id) {

                // GET AREA
                $area_sql = "SELECT * FROM `" .$this->tables['areas']. "` WHERE (`id`=" .$id. ") AND (`active`=1)";
                $area_query = sqlquery($area_sql);
                $area = sqlfetch($area_query);

                // AREA FOUND
                if ($area['id']) {

                    $ija = array();
                    $wa = array();
                    $taa = array();

                    // INNER JOIN
                    $ija[] = "INNER JOIN `" .$this->tables['ads']. "` AS `a` ON aa.ad_id=a.id";

                    // WHERE ARRAY
                    $wa[] = "aa.area_id=" .$id;
                    $wa[] = "aa.active=1";
                    $wa[] = "aa.status=1";
                    $wa[] = "(a.status=1) OR (a.status=2)";

                    // KEYWORDS
                    if ($params['keywords']) {
                        $keywords = sqlescape($params['keywords']);
                        $wa[] = "MATCH(a.keywords) AGAINST ('" .$keywords. "' IN BOOLEAN MODE)";
                    }

                    // CITIES SPECIFIED
                    if (is_array($params['cities'])) {
                        unset($ida);
                        $ija[] = "LEFT JOIN `" .$this->tables['ads_cities']. "` AS `ac` ON a.id=ac.ad_id";
                        foreach ($params['cities'] as $city_id) {
                            $ida[] = (int)$city_id;
                        }
                        $wa[] = "ac.city_id IN (" .implode(',', $ida). ")";
                    }

                    // NEIGHBORHOODS SPECIFIED
                    if (is_array($params['neighborhoods'])) {
                        unset($ida);
                        $ija[] = "LEFT JOIN `" .$this->tables['ads_neighborhoods']. "` AS `an` ON a.id=an.ad_id";
                        foreach ($params['neighborhoods'] as $neighborhood_id) {
                            $ida[] = (int)$neighborhood_id;
                        }
                        $wa[] = "ac.neighborhood_id IN (" .implode(',', $ida). ")";
                    }
                    // WHERE
                    $where_sql = "(" .implode(") AND ( ", $wa). ")";

                    // GET ADS
                    $ads_sql = "
                    SELECT a.*, aa.*
                    FROM `" .$this->tables['ads_areas']. "` AS `aa`
                    " .implode(" ", $ija). "
                    WHERE " .$where_sql. "
                    ";

                    $ads_query = sqlquery($ads_sql);
                    while ($ad = sqlfetch($ads_query)) {

                        // GET AD STATUS
                        $status = $this->ad_relStatus($ad['id'], $ad);

                        // IS ACTIVE
                        if ($status['status'] == 1) {

                            $ad['rel_id'] = $ad['id'];

                            // ADD TO TEMP AD ARRAY
                            $taa[$ad['rel_id']] = $ad;

                        }

                    }

                    // HAVE ADS
                    if (count($taa) > 0) {

                        if (!$params['limit']) $params['limit'] = 1;

                        // SORT BY WEIGHT
                        usort($taa, function($a, $b) {
                              $aw = $a['weight'];
                              $bw = $b['weight'];
                              return mt_rand(0, ($aw + $bw)) > $aw ? 1 : -1;
                        });

                        // SORT
                        $taa = array_reverse($taa);

                        // LIMIT
                        $taa = array_slice($taa, 0, $params['limit']);

                        // HAVE ADS
                        if (count($taa) > 0) {
                            foreach ($taa as $ad) {
                                $ad['image_url'] = $this->imageBridgeOut($ad['image'], 'media');
                                $out['ads'][] = $ad;
                            }
                        }

                        unset($taa);

                    }

                // AREA NOT FOUND
                } else {
                    $out['error'] = 'Area not found / not active.';
                }

            // AREA ID NOT SPECIFIED
            } else {
                $out['error'] = 'Area ID not specified.';
            }

            return $out;

        }


        // OUTPUT AD HTML FOR AREA
        public function areaAdHTML($id, $params=array()) {
            global $cms;

            $out    = '';
            $out_js = '';

            // GET ADS IN AREA
            $ads = $this->areaGetAds($id, $params);

            // HAVE ADS
            if (count($ads['ads']) > 0) {

                if (!$param['link_class']) $param['link_class'] = 'ae-link';
                if (!$param['media_class']) $param['media_class'] = 'ae-media';

                // HAVE REVSOCIAL WEB TRACKING
                if ($cms->getSetting('rs_web-tracking-id')) {
                    $rst = true;
                }

                // LOOP
                foreach ($ads['ads'] as $ad) {

                    // LOG IMPRESSION
                    $this->adLogImpression($ad['rel_id'], $ad);

                    // IS REVSOCIAL WEB TRACKING
                    if ($rst) {

                        if ($ad['title']) {
                            $tracking_title = stripslashes($ad['title']). ' (#' .$ad['ad_id']. ')';
                        } else {
                            $tracking_title = '#' .$ad['ad_id'];
                        }

                        $out_js .= "
                        _rst.push(['event', 'ad-impression', '" .$tracking_title. "']);
                        ";

                    }

                    // HAS LINK
                    if ($ad['link']) {

                        // CLICKTRACKER PATH
                        if (!isset($this->settings['clicktracker_path'])) {
                            $this->settings['clicktracker_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*ads') AND (`route`='redirect')"))['path']);
                        }

                        // HAVE CLICKTRACKER PATH
                        if ($this->settings['clicktracker_path']) {
                            $link = '/' .$this->settings['clicktracker_path']. '/?id=' .$ad['rel_id']. '&url=' .urlencode($ad['link']);
                        } else {
                            $link = $ad['link'];
                        }

                        // TARGET
                        switch ($ad['target']) {
                            case 'n':
                                $target = '_blank';
                                break;
                            case 's':
                                $target = '_self';
                                break;
                            case 'p':
                                $target = '_parent';
                                break;
                            case 't':
                                $target = '_top';
                                break;
                        }

                        /*
                        // IS REVSOCIAL WEB TRACKING
                        if ($rst) {
                            $onclick = "_rst.push(['event', 'ad-click', '" .$tracking_title. "']);";
                        } else {
                            $onclick = '';
                        }
                        */
                        $onclick = '';

                        $out .= '<a href="' .$link. '" target="' .$target. '" onclick="' .$onclick. '" class="' .$param['link_class']. '"><img src="' .$ad['image_url']. '" alt="" class="' .$param['media_class']. ' image" /></a>';

                    // NO LINK
                    } else {
                        $out .= '<img src="' .$ad['image_url']. '" alt="" class="' .$param['media_class']. ' image" />';
                    }

                }

                // HAVE JS
                if ($out_js) {
                    $out .= '
                    <script type="text/javascript" language="javascript">
                    // <![CDATA[
                        ' .$out_js. '
                    // ]]>
                    </script>
                    ';
                }

            }

            return $out;

        }


        #############################
        # AD
        #############################


        // GET AD'S INFO
        public function adRelInfo($rel_id) {

            // GET AD INFO
            $data_sql = "
            SELECT a.*, aa.*, aa.id AS `rel_id`
            FROM `" .$this->tables['ads_areas']. "` AS `aa`
            INNER JOIN `" .$this->tables['ads']. "` AS `a` ON aa.ad_id=a.id
            WHERE aa.id=" .(int)$rel_id. "
            ";
            $data_query = sqlquery($data_sql);
            $data = sqlfetch($data_query);

            return $data;

        }


        // HANDLE AD'S STATUS
        public function ad_relStatus($id, $rel=array(), $update=true) {

            $out = array();

            $id = (int)$id;

            // HAVE ID
            if ($id) {

                // NO RELATIONSHIP DATA
                if (count($rel) == 0) {

                    // GET RELATION
                    $rel_query = "SELECT * FROM `" .$this->tables['ads_areas']. "` WHERE (`id`=" .$id. ")";
                    $rel_q = sqlquery($rel_query);
                    $rel = sqlfetch($rel_q);

                }

                // HAVE RELATIONSHIP
                if ($rel['id']) {

                    $status = 0;
                    $start_active = false;

                    // ACTIVE DATE & TIME
                    $dt_active = ($rel['dt_active'] != '0000-00-00 00:00:00' ? strtotime($rel['dt_active']) : '');

                    // HAVE ACTIVE DATE & TIME
                    if ($dt_active) {
                        if (time() > $dt_active) {
                            $start_active = true;
                        } else {
                            $status = 2; // scheduled
                        }
                    } else {
                        $start_active = true;
                    }

                    // START IS ACTIVE
                    if ($start_active) {

                        // EXPIRES BY DATE & TIME
                        if ($rel['expire_type'] == 'd') {

                            // EXPIRE DATE & TIME
                            $dt_expire = ($rel['expire_dt'] != '0000-00-00 00:00:00' ? strtotime($rel['expire_dt']) : '');

                            // HAVE EXPIRE DATE & TIME
                            if ($dt_expire) {
                                if (time() < $dt_expire) {
                                    $status = 1; // active
                                } else {
                                    $status = 7; // expired
                                }
                            } else {
                                $status = 1; // active
                            }

                        // EXPIRES BY IMPRESSIONS
                        } else if ($rel['expire_type'] == 'i') {
                            if ($rel['expire_impressions'] > 0) {
                                if ($rel['expire_impressions'] > $rel['impressions']) {
                                    $status = 1; // active
                                } else {
                                    $status = 7; // expired
                                }
                            } else {
                                $status = 1; // active
                            }

                        // EXPIRES BY CLICKS
                        } else if ($rel['expire_type'] == 'c') {
                            if ($rel['expire_clicks'] > 0) {
                                if ($rel['expire_clicks'] > $rel['clicks']) {
                                    $status = 1; // active
                                } else {
                                    $status = 7; // expired
                                }
                            } else {
                                $status = 1; // expired
                            }

                        // NEVER EXPIRES
                        } else if (!$rel['expire_type']) {
                            $status = 1; // active
                        }

                    }

                    // ARE UPDATING
                    if ($update) {

                        // UPDATE DATABASE
                        $sql = "UPDATE `" .$this->tables['ads_areas']. "` SET `status`=" .$status. " WHERE (`id`=" .$rel['id']. ")";
                        sqlquery($sql);

                    }

                    $out['status'] = $status;

                }

            }

            return $out;

        }


        // LOG AD IMPRESSION
        public function adLogImpression($rel_id, $data=array()) {

            $rel_id = (int)$rel_id;

            // DON'T HAVE DATA
            if (!$data['id']) {
                $data = $_uccms_ads->adRelInfo($rel_id);
            }

            // DB COLUMNS
            $columns = array(
                'area_id'           => $data['area_id'],
                'ad_id'             => $data['ad_id'],
                'ad_area_rel_id'    => $rel_id,
                'url'               => $_SERVER['REQUEST_URI'],
                'ip'                => ip2long($_SERVER['REMOTE_ADDR'])
            );

            // ADD LOG
            $sql = "INSERT INTO `" .$this->tables['impressions']. "` SET " .uccms_createSet($columns);
            sqlquery($sql);

            // UPDATE RELATIONSHIP
            $sql = "UPDATE `" .$this->tables['ads_areas']. "` SET `impressions`=`impressions`+1 WHERE (`id`=" .$rel_id. ")";
            sqlquery($sql);

        }


        // LOG AD CLICK
        public function adLogClick($rel_id, $data=array()) {

            $rel_id = (int)$rel_id;

            // DON'T HAVE DATA
            if (!$data['id']) {
                $data = $_uccms_ads->adRelInfo($rel_id);
            }

            // DB COLUMNS
            $columns = array(
                'area_id'           => $data['area_id'],
                'ad_id'             => $data['ad_id'],
                'ad_area_rel_id'    => $rel_id,
                'referer'           => $_SERVER['HTTP_REFERER'],
                'ip'                => ip2long($_SERVER['REMOTE_ADDR'])
            );

            // ADD LOG
            $sql = "INSERT INTO `" .$this->tables['clicks']. "` SET " .uccms_createSet($columns);
            sqlquery($sql);

            // UPDATE RELATIONSHIP
            $sql = "UPDATE `" .$this->tables['ads_areas']. "` SET `clicks`=`clicks`+1 WHERE (`id`=" .$rel_id. ")";
            sqlquery($sql);

        }


        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # CRON
        #############################

        // RUN THE MAIN CRON
        public function runCron() {

            return;

            $out = array();

            // DATE & TIME
            $publish_dt_from = date('Y-m-d H:i:s', strtotime('-10 Minutes'));
            $publish_dt_to = date('Y-m-d H:i:s');

            // GET POSTS PUBLISHING IN PAST 10 MINUTES
            $publishing_query = "SELECT * FROM `" .$this->tables['posts']. "` WHERE (`status`=2) AND ((`dt_publish`>'" .$publish_dt_from. "') AND (`dt_publish`<='" .$publish_dt_to. "'))";
            $publishing_q = sqlquery($publishing_query);

            $out[] = sqlrows($publishing_q). ' posts to publish.';

            // LOOP THROUGH POSTS
            while ($publishing = sqlfetch($publishing_q)) {

                // STRIPSLASHES
                foreach ($publishing as $name => $value) {
                    $data['post'][$name] = stripslashes($value);
                }

                // MARK TO PUBLISH
                $data['publish'] = true;

                // GET CURRENT CATEGORIES
                $cat_query = "SELECT `category_id` FROM `" .$this->tables['post_categories']. "` WHERE (`post_id`=" .$publishing['id']. ")";
                $cat_q = sqlquery($cat_query);
                while ($cat = sqlfetch($cat_q)) {
                    $data['category'][$cat['category_id']] = $cat['category_id'];
                }

                // GET CURRENT TAGS
                $tag_query = "SELECT `tag_id` FROM `" .$this->tables['post_tags']. "` WHERE (`post_id`=" .$publishing['id']. ")";
                $tag_q = sqlquery($tag_query);
                while ($tag = sqlfetch($tag_q)) {
                    $data['tag'][$tag['tag_id']] = $tag['tag_id'];
                }

                // DECODE REVSOCIAL DATA
                $data['revsocial'] = json_decode($publishing['revsocial'], true);

                //echo print_r($data);
                //exit;

                // PUBLISH POST
                $result = $this->post_addUpdate($data);

                $out[] = 'ID: ' .$publishing['id']. ' - ' .$result['message'];

            }

            return implode("\n", $out). "\n";

        }


    }

?>