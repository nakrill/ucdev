<?php

class global_uccms_Ads extends uccms_Ads {


    /*
    #############################
    # GENERAL: Sitemap
    #############################

    public function general_sitemap($page=array()) {
        global $bigtree;

        $out = array();

        // CATEGORIES
        $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->categoryURL($r['id'], $r)
            );
        }

        return $out;

    }
    */


    /*
    #############################
    # GENERAL: Search
    #############################

    public function general_search($q='', $params=array()) {

        $results = array();

        // GET MATCHING BUSINESS LISTINGS
        $results_query = "SELECT `id`, `slug`, `name` FROM `" .$this->tables['businesses']. "` WHERE (`name` LIKE '%" .$q. "%') ORDER BY `updated_dt` DESC, `created_dt` DESC LIMIT " .(int)$params['limit'];
        $results_q = sqlquery($results_query);
        while ($row = sqlfetch($results_q)) {
            $results[] = array(
                'title' => stripslashes($row['name']),
                'url'   => $this->businessURL($row['id'], 0, $row),
                'type'  => 'business'
            );
        }

        return $results;

    }
    */


    /*
    #############################
    # GENERAL: Cron
    #############################

    // RUN THE MAIN CRON
    public function runCron() {


    }
    */


    /*
    #############################
    # PAYMENT - Transactions
    #############################

    public function payment_transactions($vars=array()) {

        $out = array();

        $wa = array();

        if ($vars['query']['orderby']['field'] == 'dt') {
            $orderby_sql = " ORDER BY `dt` " .($vars['query']['orderby']['direction'] == "desc" ? "DESC" : "ASC"). " ";
        }

        if ($vars['query']['limit']['num'] > 0) {
            $limit_sql = " LIMIT " .(int)$vars['query']['limit']['num']. " ";
        }

        $accounta = array();

        // GET TRANSACTION LOGS
        $trans_query = "SELECT * FROM `" .$this->tables['transaction_log']. "` " .$orderby_sql . $limit_sql;
        $trans_q = sqlquery($trans_query);
        while ($trans = sqlfetch($trans_q)) {

            // HAVE ACCOUNT ID
            if (($trans['account_id']) && (!$accounta[$trans['account_id']])) {

                // GET ACCOUNT INFO
                $account_query = "
                SELECT *
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$trans['account_id']. ")
                ";
                $account_q = sqlquery($account_query);
                $accounta[$trans['account_id']] = sqlfetch($account_q);

            }

            if ($accounta[$trans['account_id']]['id']) {
                $account = $accounta[$trans['account_id']];
            } else {
                $account = array();
            }

            $out[] = array(
                'number'        => $account['id'],
                'dt'            => $trans['dt'],
                'name'          => trim(stripslashes($account['firstname']. ' ' .$account['lastname'])),
                'method'        => $trans['method'],
                'description'   => 'Ad',
                'amount'        => $trans['amount'],
                'lastfour'      => $trans['lastfour'],
                'url'           => array(
                    'edit' => '/admin/' .$this->Extension. '*business-directory/businesses/edit/?id=' .$account['id']
                ),
            );

        }

        return $out;

    }
    */


}

?>