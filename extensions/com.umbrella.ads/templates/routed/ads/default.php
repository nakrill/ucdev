<?php

if (!$_uccms_ads) $_uccms_ads = new uccms_Ads;

// GET SETTING(S)
$ads['settings'] = $_uccms_ads->getSettings();

// SECTIONS DIRECTORY
$sections_dir = dirname(__FILE__). '/sections';

// DATA DIRECTORY
$data_dir = dirname(__FILE__). '/data';

// IS A REDIRECT
if ($bigtree['path'][count($bigtree['path'])-1]) {

    $bigtree['layout'] = 'blank';

    include($data_dir. '/redirect/clickthrough.php');

}

BigTree::redirect('/');

?>