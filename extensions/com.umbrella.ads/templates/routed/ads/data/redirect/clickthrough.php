<?php

$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET AD INFO
    $ad = $_uccms_ads->adRelInfo($id);

    // AD FOUND
    if ($ad['id']) {

        // LOG CLICK
        $_uccms_ads->adLogClick($ad['rel_id'], $ad);

        // REVSOCIAL WEB TRACKING
        if ($cms->getSetting('rs_web-tracking-id')) {

            if ($ad['title']) {
                $tracking_title = stripslashes($ad['title']). ' (#' .$ad['ad_id']. ')';
            } else {
                $tracking_title = '#' .$ad['ad_id'];
            }

            ?>
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <script type="text/javascript" language="javascript">
                        // <![CDATA[
                        var _rst = _rst || [];
                        _rst.push(['tid', '<?php echo $cms->getSetting('rs_web-tracking-id'); ?>']);
                        _rst.push(['event', 'ad-click', '<?php echo $tracking_title; ?>']);
                        (function() {
                        var rst = document.createElement('script'); rst.type = 'text/javascript'; rst.async = true; rst.src = '//rst.revsocial.com/rst.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(rst);
                        })();
                        // ]]>
                    </script>
                </head>
                <body></body>
            </html>
            <?php
        }

        // REDIRECT
        if ($ad['link']) {
            BigTree::redirect(stripslashes($ad['link']));
        }

    }

}

// HAVE URL
if ($_REQUEST['url']) {
    BigTree::redirect($_REQUEST['url']);
} else {
    $_uccms['_message']->set('err', 'No URL specified.');
    BigTree::redirect('/');
}

?>