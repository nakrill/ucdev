if (!$.isFunction(displayAEMedia)) {

    // DISPLAY AD MEDIA
    function displayAEMedia(id, unique_id, params) {
        $.get('/*/com.umbrella.ads/ajax/embed/media/', {
            id: id,
            params: params
        }, function(data) {
            $('#' +unique_id).replaceWith(data);
        }, 'html');
    }

}