<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_businessdir) $_uccms_businessdir = new uccms_BusinessDirectory();

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
//$city_id        = (int)$_GET['city_id'];
$limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_businessdir->perPage();

if (isset($_GET['base_url'])) {
    $base_url = $_GET['base_url'];
} else if (defined('MODULE_ROOT')) {
    $base_url = MODULE_ROOT. 'locations';
} else {
    $base_url = '.';
}

//$_uccms_businessdir->setPerPage(5);

// WHERE ARRAY
unset($wa);
$wa[] = "c.id>0";

/*
// CITY SPECIFIED
if ($city_id > 0) {
    $wa[] = "n.city_id=" .$city_id;
}
*/

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(c.title) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED
$city_query = "
SELECT c.*
FROM `" .$_uccms_businessdir->tables['cities']. "` AS `c`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY c.sort ASC, c.title ASC, c.id ASC
LIMIT " .(($page - 1) * $limit). "," .$limit;
$city_q = sqlquery($city_query);

// NUMBER OF PAGED CITIES
$num_cities = sqlrows($city_q);

// TOTAL NUMBER OF CITIES
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_businessdir->tables['cities']. "` AS `c` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_businessdir->pageCount($total_results['total']);

// HAVE CITIES
if ($num_cities > 0) {

    // LOOP
    while ($city = sqlfetch($city_q)) {

        // GET NEIGHBORHOODS
        $neighborhood_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` WHERE (`city_id`=" .$city['id']. ")";
        $neighborhood_q = sqlquery($neighborhood_query);
        $num_neighborhoods = sqlrows($neighborhood_q);

        // GET BUSINESSES
        $bus_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`city_id`=" .$city['id']. ") AND (`pending`=0)";
        $bus_q = sqlquery($bus_query);
        $num_businesses = sqlrows($bus_q);

        ?>

        <li class="item">
            <section class="city_title">
                <a href="<?php echo $base_url; ?>/city/?id=<?php echo $city['id']; ?>"><?php echo stripslashes($city['title']); ?></a>
            </section>
            <section class="city_neighborhoods">
                <a href="<?php echo $base_url; ?>/city/?id=<?php echo $city['id']; ?>#neighborhoods"><?php echo number_format($num_neighborhoods, 0); ?></a>
            </section>
            <section class="city_businesses">
                <?php echo number_format($num_businesses, 0); ?>
            </section>
            <section class="city_edit">
                <a class="icon_edit" title="Edit City" href="<?php echo $base_url; ?>/city/?id=<?php echo $city['id']; ?>"></a>
            </section>
            <section class="city_delete">
                <a href="<?php echo $base_url; ?>/delete-city/?id=<?php echo $city['id']; ?>" class="icon_delete" title="Delete City" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
            </section>
        </li>

        <?php

    }

// NO CITIES
} else {
    ?>
    <li style="text-align: center;">
        No cities.
    </li>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>