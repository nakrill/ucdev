<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_businessdir) $_uccms_businessdir = new uccms_BusinessDirectory();

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
$city_id        = ($_GET['city_id'] ? $_GET['city_id'] : $city['id']);
$limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_businessdir->perPage();

if (isset($_GET['base_url'])) {
    $base_url = $_GET['base_url'];
} else if (defined('MODULE_ROOT')) {
    $base_url = MODULE_ROOT. 'locations';
} else {
    $base_url = '.';
}

//$_uccms_businessdir->setPerPage(5);

// WHERE ARRAY
unset($wa);
$wa[] = "n.id>0";

// CITY SPECIFIED
if ($city_id > 0) {
    $wa[] = "n.city_id=" .$city_id;
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(n.title) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED
$neighborhood_query = "
SELECT n.*
FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` AS `n`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY n.sort ASC, n.title ASC, n.id ASC
LIMIT " .(($page - 1) * $limit). "," .$limit;
$neighborhood_q = sqlquery($neighborhood_query);

// NUMBER OF PAGED
$num_neighborhoods = sqlrows($neighborhood_q);

// TOTAL NUMBER
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` AS `n` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_businessdir->pageCount($total_results['total']);

// HAVE
if ($num_neighborhoods > 0) {

    // LOOP
    while ($neighborhood = sqlfetch($neighborhood_q)) {

        // GET BUSINESSES
        $bus_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`neighborhood_id`=" .$neighborhood['id']. ") AND (`pending`=0)";
        $bus_q = sqlquery($bus_query);
        $num_businesses = sqlrows($bus_q);

        ?>

        <li class="item">
            <section class="neighborhood_title">
                <a href="<?php echo $base_url; ?>/neighborhood/?id=<?php echo $neighborhood['id']; ?>"><?php echo stripslashes($neighborhood['title']); ?></a>
            </section>
            <section class="neighborhood_businesses">
                <?php echo number_format($num_businesses, 0); ?>
            </section>
            <section class="neighborhood_edit">
                <a class="icon_edit" title="Edit neighborhood" href="<?php echo $base_url; ?>/neighborhood/?id=<?php echo $neighborhood['id']; ?>"></a>
            </section>
            <section class="neighborhood_delete">
                <a href="<?php echo $base_url; ?>/city/delete-neighborhood/?id=<?php echo $neighborhood['id']; ?>&city_id=<?php echo $neighborhood['city_id']; ?>" class="icon_delete" title="Delete neighborhood" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
            </section>
        </li>

        <?php

    }

// NONE
} else {
    ?>
    <li style="text-align: center;">
        No neighborhoods.
    </li>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>