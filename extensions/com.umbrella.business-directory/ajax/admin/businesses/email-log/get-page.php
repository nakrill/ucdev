<?php

$admin->requireLevel(1);

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
$log_id         = (int)$_GET['business_id'];

// MODULE CLASS
if (!$_uccms_businessdir) $_uccms_businessdir = new uccms_BusinessDirectory;

//$_uccms_businessdir->setPerPage(5);

// WHERE ARRAY
$wa[] = "bel.deleted_dt='0000-00-00 00:00:00'";

// BUSINESS SPECIFIED
if ($log_id > 0) {
    $wa[] = "bel.business_id=" .$log_id;
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(bel.data) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED LOGS
$log_query = "
SELECT bel.*, b.name
FROM `" .$_uccms_businessdir->tables['business_email_log']. "` AS `bel`
INNER JOIN `" .$_uccms_businessdir->tables['businesses']. "` AS `b` ON bel.business_id=b.id
WHERE (" .implode(") AND (", $wa). ")
ORDER BY bel.dt DESC
LIMIT " .(($page - 1) * $_uccms_businessdir->perPage()). "," .$_uccms_businessdir->perPage();
$log_q = sqlquery($log_query);

// NUMBER OF PAGED LOGS
$num_logs = sqlrows($log_q);

// TOTAL NUMBER OF LOGS
$total_results = sqlfetch(sqlquery("
SELECT COUNT('x') AS `total`
FROM `" .$_uccms_businessdir->tables['business_email_log']. "` AS `bel`
INNER JOIN `" .$_uccms_businessdir->tables['businesses']. "` AS `b` ON bel.business_id=b.id
WHERE (" .implode(") AND (", $wa). ")
"));

// NUMBER OF PAGES
$pages = $_uccms_businessdir->pageCount($total_results['total']);

// HAVE LOGS
if ($num_logs > 0) {

    // LOOP
    while ($log = sqlfetch($log_q)) {

        unset($fielda);

        ?>

        <li class="item">
            <section class="log_date">
                <?php echo $log['dt']; ?>
            </section>
            <section class="log_business">
                <?php echo stripslashes($log['name']); ?>
            </section>
            <section class="log_data">
                <?php
                $data = json_decode(str_replace("\n", '<br />', stripslashes($log['data'])), true);
                if (count($data) > 0) {
                    foreach ($data as $field_name => $field_value) {
                        $fielda[] = $field_name. ': ' .$field_value;
                    }
                    echo implode(', ', $fielda);
                }
                ?>
            </section>
            <section class="log_view" style="text-align: center;">
                <a class="icon_edit" title="View Log" href="./view/?id=<?php echo $log['id']; ?>"></a>
            </section>
            <section class="log_delete" style="text-align: center;">
                <a href="./delete/?id=<?php echo $log['id']; ?>" class="icon_delete" title="Delete Log" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
            </section>
        </li>

        <?php

        unset($data);

    }

// NO LOGS
} else {
    ?>
    <li style="text-align: center;">
        No logs.
    </li>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>