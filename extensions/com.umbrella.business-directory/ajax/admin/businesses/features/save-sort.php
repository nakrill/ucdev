<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_businessdir) $_uccms_businessdir = new uccms_BusinessDirectory;

// HAVE SORT INFO
if (is_array($_POST['sort'])) {

    $si = 0;

    // LOOP
    foreach ($_POST['sort'] as $feature_id) {

        $update_query = "UPDATE `" .$_uccms_businessdir->tables['features']. "` SET `sort`=" .$si. " WHERE (`id`=" .(int)$feature_id. ")";
        sqlquery($update_query);

        $si++;

    }

}

?>