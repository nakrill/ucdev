<?php

// MODULE CLASS
if (!$_uccms_businessdir) $_uccms_businessdir = new uccms_BusinessDirectory;

// HAS ACCESS
if ($_uccms_businessdir->adminModulePermission()) {

    // CLEAN UP
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
    $category_id    = (int)$_GET['category_id'];
    $limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_businessdir->perPage();

    if (isset($_GET['base_url'])) {
        $base_url = $_GET['base_url'];
    } else if (defined('MODULE_ROOT')) {
        $base_url = MODULE_ROOT. 'businesses';
    } else {
        $base_url = '.';
    }

    //$_uccms_businessdir->setPerPage(5);

    // WHERE ARRAY
    unset($wa);
    $wa[] = "b.id>0";

    // STATUSES
    $statusa = array(
        'inactive'  => 0,
        'active'    => 1,
        'deleted'   => 9
    );
    if (isset($statusa[$_REQUEST['status']])) {
        $wa[] = "b.status=" .$statusa[$_REQUEST['status']];
    } else if ($_REQUEST['status'] == 'submitted') {
        $wa[] = "b.submitted=1";
    } else if ($_REQUEST['status'] == 'pending') {
        $wa[] = "b.pending>0";
    } else {
        $wa[] = "b.status!=9";
    }

    // CATEGORY SPECIFIED
    if ($category_id > 0) {
        $inner_join = "INNER JOIN `" .$_uccms_businessdir->tables['business_categories']. "` AS `bc` ON b.id=bc.business_id";
        $wa[] = "bc.category_id=" .$category_id;
    } else {
        $inner_join = "";
    }

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(b.name) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED BUSINESSES
    $business_query = "
    SELECT b.*
    FROM `" .$_uccms_businessdir->tables['businesses']. "` AS `b`
    " .$inner_join. "
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY b.name ASC, b.id ASC
    LIMIT " .(($page - 1) * $limit). "," .$limit;
    $business_q = sqlquery($business_query);

    // NUMBER OF PAGED BUSINESSES
    $num_businesses = sqlrows($business_q);

    // TOTAL NUMBER OF BUSINESSES
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_businessdir->tables['businesses']. "` AS `b` " .$inner_join. " WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_businessdir->pageCount($total_results['total']);

    // HAVE BUSINESSES
    if ($num_businesses > 0) {

        // ACCOUNTS ARRAY
        $accta = array();

        // LOOP
        while ($business = sqlfetch($business_q)) {

            // HAVE ACCOUNT ID
            if ($business['account_id'] > 0) {

                // NOT IN ACCOUNT ARRAY
                if (!$accta[$business['account_id']]) {

                    // GET ACCOUNT INFO
                    $acct_query = "
                    SELECT a.id, a.username, a.email, ad.firstname, ad.lastname
                    FROM `uccms_accounts` AS `a`
                    INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                    WHERE (a.id=" .$business['account_id']. ")
                    LIMIT 1
                    ";
                    $acct_q = sqlquery($acct_query);
                    $acct = sqlfetch($acct_q);
                    $accta[$business['account_id']] = $acct;

                }

            }

            ?>

            <li class="item">
                <section class="business_title">
                    <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $business['id']; ?>"><?php echo stripslashes($business['name']); ?></a>
                </section>
                <section class="business_phone">
                    <?php echo $_uccms_businessdir->prettyPhone(stripslashes($business['phone'])); ?>
                </section>
                <section class="business_account">
                    <?php
                    if (($accta[$business['account_id']]['firstname']) || ($accta[$business['account_id']]['lastname'])) {
                        echo trim(stripslashes($accta[$business['account_id']]['firstname']. ' ' .$accta[$business['account_id']]['lastname'])). ' (' .stripslashes($accta[$business['account_id']]['email']). ')';
                    } else {
                        echo stripslashes($accta[$business['account_id']]['email']);
                    }
                    ?>
                </section>
                <section class="business_status status_<?php if ($business['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php if ($business['status'] == 1) { echo 'Active' ; } elseif ($business['pending']) { echo 'Pending'; } else { echo 'Inactive'; } ?>
                </section>
                <section class="business_edit">
                    <a class="icon_edit" title="Edit Business" href="<?php echo $base_url; ?>/edit/?id=<?php echo $business['id']; ?>"></a>
                </section>
                <section class="business_delete">
                    <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $business['id']; ?>" class="icon_delete" title="Delete Business" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                </section>
            </li>

            <?php

        }

        unset($accta);

    // NO BUSINESSES
    } else {
        ?>
        <li style="text-align: center;">
            No businesses.
        </li>
        <?php
    }

    ?>

    <script>
        BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>