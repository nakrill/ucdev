<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['city']['id'];

    // USE TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['city']['slug']) {
        $_POST['city']['slug'] = $_POST['city']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_businessdir->makeRewrite($_POST['city']['slug']),
        'title'             => $_POST['city']['title'],
        'meta_title'        => $_POST['city']['meta_title'],
        'meta_description'  => $_POST['city']['meta_description'],
        'meta_keywords'     => $_POST['city']['meta_keywords']
    );

    // HAVE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_businessdir->tables['cities']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_businessdir->tables['cities']. "` SET " .uccms_createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('City', 'City added!');
        } else {
            $admin->growl('City', 'City updated!');
        }

    // QUERY FAILED
    } else {
        $admin->growl('City', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'/locations/city/?id=' .$id);

?>