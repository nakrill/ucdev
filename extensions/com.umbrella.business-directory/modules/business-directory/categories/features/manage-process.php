<?php

// CLEAN UP
$id = (int)$_POST['category']['id'];

// HAVE category ID
if ($id) {

    // ADDING
    if ($_POST['do'] == 'Add') {

        unset($columns);

        // ADDING NEW FEATURE
        if ($_POST['new_feature-what'] == 'feature') {

            // CLEAN UP
            $feature_id = (int)$_POST['new_feature'];

            // HAVE NEW FEATURE ID
            if ($feature_id) {

                // DB COLUMNS
                $columns = array(
                    'category_id'   => $id,
                    'sort'          => 999,
                    'feature_id'    => $feature_id,
                    'updated_by'    => $_uccms_businessdir->adminID()
                );

            // NO FEATURE ID
            } else {
                $admin->growl('Category Feature', 'No feature specified to add.');
            }

        // ADDING NEW GROUP
        } else if ($_POST['new_feature-what'] == 'group') {

            // CLEAN UP
            $group_id = (int)$_POST['new_feature-group'];

            // HAVE NEW GROUP ID
            if ($group_id) {

                // DB COLUMNS
                $columns = array(
                    'category_id'       => $id,
                    'sort'              => 999,
                    'group_id'          => $group_id,
                    'updated_by'        => $_uccms_businessdir->adminID()
                );

            // NO FEATURE ID
            } else {
                $admin->growl('Category Feature Group', 'No feature group specified to add.');
            }

        // ADDING CUSTOM
        } else if ($_POST['new_feature-what'] == 'custom') {

            // REDIRECT TO feature ADD / EDIT PAGE
            BigTree::redirect(MODULE_ROOT. 'categories/features/edit/?category_id=' .$id);

        }

        // HAVE DB COLUMNS
        if (is_array($columns)) {

            // DB QUERY
            $query = "INSERT INTO `" .$_uccms_businessdir->tables['category_features']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW()";

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {
                $admin->growl('Category Feature', 'Feature added!');

            // QUERY FAILED
            } else {
                $admin->growl('Category Feature', 'Failed to add.');
            }

        }

    // CUSTOM ADD / UPDATE
    } else if ($_POST['feature']) {

        // REQUIRED
        if ($_POST['feature']['required_checkbox']) {
            $required = $_POST['feature']['required'];
        } else {
            $required = '';
        }

        // FILE TYPES
        if ($_POST['feature']['file_types']) {
            $ffta = array();
            $tfta = explode(',', $_POST['feature']['file_types']);
            foreach ($tfta as $ft) {
                $ffta[] = trim(str_replace('.', '', $ft));
            }
            if (count($ffta) > 0) {
                $file_types = implode(',', $ffta);
            }
        }

        // DB COLUMNS
        $columns = array(
            'active'        => (int)$_POST['feature']['active'],
            'title'         => $_POST['feature']['title'],
            'description'   => $_POST['feature']['description'],
            'type'          => $_POST['feature']['type'],
            'limit'         => (int)$_POST['feature']['limit'],
            'required'      => $required,
            'file_types'    => $file_types,
            'updated_by'    => $_uccms_businessdir->adminID()
        );

        // DON'T INSERT BY DEFAULT
        $insert = false;

        // CLEAN UP
        $rel_id = (int)$_POST['rel']['id'];

        // HAVE RELATIONSHIP ID (EXISTING)
        if ($rel_id > 0) {

            // GET RELATIONSHIP INFO
            $rel_query = "SELECT * FROM `" .$_uccms_businessdir->tables['category_features']. "` WHERE (`id`=" .$rel_id. ")";
            $rel = sqlfetch(sqlquery($rel_query));

            // GET FEATURE INFO FROM DB
            $feature_query = "SELECT * FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`id`=" .$rel['feature_id']. ")";
            $feature_q = sqlquery($feature_query);
            $feature = sqlfetch($feature_q);

            // IS ALREADY CUSTOM
            if ($feature['category_id'] == $id) {

                // UPDATE CUSTOM FEATURE
                $query = "UPDATE `" .$_uccms_businessdir->tables['features']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$feature['id']. ")";

                // UPDATE SUCCESSFUL
                if (sqlquery($query)) {
                    $admin->growl('Category Feature', 'Updated custom feature.');
                } else {
                    $admin->growl('Category Feature', 'Failed to update custom feature.');
                }

            // IS GLOBAL
            } else {
                $insert = true;
            }

        // NO RELATIONSHIP ID (NEW)
        } else {
            $insert = true;
        }

        // IS INSERTING
        if ($insert) {

            // ADD TO DB COLUMNS
            $columns['category_id'] = $id;
            if ($rel_id > 0) $columns['options'] = $feature['options']; // COPY OPTIONS FROM GLOBAL

            // CREATE CUSTOM FEATURE
            $query = "INSERT INTO `" .$_uccms_businessdir->tables['features']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW()";

            // CUSTOM feature CREATED
            if (sqlquery($query)) {

                // NEW FEATURE ID
                $feature['id'] = sqlid();

                // DB COLUMNS
                $columns = array(
                    'category_id'   => $id,
                    'feature_id'    => $feature['id'],
                    'updated_by'    => $_uccms_businessdir->adminID()
                );

                // HAVE RELATIONSHIP ID - UPDATE
                if ($rel_id > 0) {

                    // UPDATE RELATIONSHIP
                    $query = "UPDATE `" .$_uccms_businessdir->tables['category_features']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$rel_id. ")";

                // NO RELATIONSHIP ID - CREATE
                } else {

                    $columns['sort'] = 999;

                    // CREATE RELATIONSHIP
                    $query = "INSERT INTO `" .$_uccms_businessdir->tables['category_features']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW()";

                }

                // RELATIONSHIP CREATE SUCCESSFUL
                if (sqlquery($query)) {
                    $admin->growl('Category Feature', 'Created custom feature.');

                    if (!$rel_id) $rel_id = sqlid();

                // CUSTOM FEATURE NOT CREATED
                } else {
                    $admin->growl('Category Feature', 'Failed to create custom feature relationship.');
                }

            // CUSTOM FEATURE CREATE FAILED
            } else {
                $admin->growl('Category Feature', 'Failed to create custom feature.');
            }

        }

        BigTree::redirect(MODULE_ROOT. 'categories/features/edit/?category_id=' .$id. '&id=' .$rel_id);
        exit;

    }

    // SORT ARRAY
    $sorta = explode(',', $_POST['sort']);

    // ARE DELETING
    if ($_POST['delete']) {

        // GET ID'S TO DELETE
        $ida = explode(',', $_POST['delete']);

        // HAVE ID'S
        if (count($ida) > 0) {

            // LOOP
            foreach ($ida as $rel_id) {

                // CLEAN UP
                $rel_id = (int)$rel_id;

                // HAVE VALID ID
                if ($rel_id) {

                    // GET RELATIONSHIP INFO
                    $rel_query = "SELECT * FROM `" .$_uccms_businessdir->tables['category_features']. "` WHERE (`id`=" .$rel_id. ")";
                    $rel = sqlfetch(sqlquery($rel_query));

                    // REMOVE RELATIONSHIP FROM DATABASE
                    $query = "DELETE FROM `" .$_uccms_businessdir->tables['category_features']. "` WHERE (`id`=" .$rel_id. ")";
                    sqlquery($query);

                    // REMOVE FROM SORT ARRAY
                    if (($key = array_search($rel_id, $sorta)) !== false) {
                        unset($sorta[$key]);
                    }

                    // IS NORMAL FEATURE (NOT A GROUP)
                    if ($rel['feature_id'] > 0) {

                        // DELETE FEATURE IF CUSTOM FOR CATEGORY
                        $query = "DELETE FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`id`=" .$rel['feature_id']. ") AND (`category_id`=" .$id. ")";
                        sqlquery($query);

                    }

                }

            }

        }

    }

    // HAVE SORT ORDER
    if (count($sorta) > 0) {

        $si = 0;

        // LOOP
        foreach ($sorta as $rel_id) {

            // CLEAN UP
            $rel_id = (int)$rel_id;

            // HAVE VALID ID
            if ($rel_id) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_businessdir->tables['category_features']. "` SET `sort`='" .$si. "' WHERE (`id`=" .$rel_id. ")";
                sqlquery($query);

                $si++;

            }

        }

    }

}

BigTree::redirect(MODULE_ROOT. 'categories/?id=' .$id. '#manage-features');

?>