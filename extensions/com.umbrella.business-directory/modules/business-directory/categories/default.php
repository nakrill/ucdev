<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Categories', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET CATEGORY INFO
    $category_query = "SELECT * FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`id`=" .$id. ")";
    $category_q = sqlquery($category_query);
    $category = sqlfetch($category_q);

// NO ID SPECIFIED
} else {

    // DEFAULTS
    $category['parent'] = (int)$_REQUEST['parent'];

}

// GET CATEGORIES
//$cata = $_uccms_businessdir->getCategoryTree();

// CATEGORY PARENT(S) ARRAY
$catpa = $_uccms_businessdir->getCategoryParents($id);

?>


<style type="text/css">

    .uccms_breadcrumbs {
        padding-bottom: 15px;
    }

    .file_wrapper .data {
        width: 211px;
    }

    #categories .category_title {
        width: 530px;
    }
    #categories .category_subs {
        width: 100px;
    }
    #categories .category_businesses {
        width: 100px;
    }
    #categories .category_status {
        width: 100px;
    }
    #categories .category_edit {
        width: 55px;
    }
    #categories .category_delete {
        width: 55px;
    }

</style>

<div class="uccms_breadcrumbs">
    <?php echo $_uccms_businessdir->generateBreadcrumbs($catpa); ?>
</div>

<?php if (($category['id']) || ($_REQUEST['do'] == 'add')) { ?>

    <div class="container legacy">

        <form enctype="multipart/form-data" action="./process/" method="post">
        <input type="hidden" name="category[id]" value="<?php echo $category['id']; ?>" />

        <header>
            <h2><?php if ($_REQUEST['do'] == 'add') { ?>Add<?php } else { ?>Edit<?php } ?> Category</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left">

                    <fieldset>
                        <label>Title</label>
                        <input type="text" name="category[title]" value="<?php echo stripslashes($category['title']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Description</label>
                        <textarea name="category[description]" style="height: 64px;"><?php echo stripslashes($category['description']); ?></textarea>
                    </fieldset>

                    <fieldset class="last">
                        <input type="checkbox" name="category[active]" value="1" <?php if ($category['active']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Active</label>
                    </fieldset>

                </div>

                <div class="right">

                    <fieldset>
                        <label>Parent Category</label>
                        <select name="category[parent]">
                            <option value="0">Top-Level</option>
                            <?php $_uccms_businessdir->printCategoryDropdown($_uccms_businessdir->getCategoryTree(), $category['parent'], $category['id']); ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Image</label>
                        <div>
                            <?php
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'image', // The value you should use for the "name" attribute of your form field
                                'value'     => ($category['image'] ? $_uccms_businessdir->imageBridgeOut($category['image'], 'categories') : ''), // The existing value for this form field
                                'id'        => 'category_image', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </div>
                    </fieldset>

                </div>

            </div>

            <div class="contain">
                <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
                <div class="contain uccms_toggle-seo" style="display: none;">

                    <fieldset>
                        <label>URL</label>
                        <input type="text" name="category[slug]" value="<?php echo stripslashes($category['slug']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Meta Title</label>
                        <input type="text" name="category[meta_title]" value="<?php echo stripslashes($category['meta_title']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Meta Description</label>
                        <textarea name="category[meta_description]" style="height: 64px;"><?php echo stripslashes($category['meta_description']); ?></textarea>
                    </fieldset>

                    <fieldset>
                        <label>Meta Keywords</label>
                        <textarea name="category[meta_keywords]" style="height: 64px;"><?php echo stripslashes($category['meta_keywords']); ?></textarea>
                    </fieldset>

                </div>
            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
            <a class="button back" href="./?id=<?php echo $category['parent']; ?>">&laquo; Back</a>
        </footer>

        </form>

    </div>

<?php } ?>

<?php if ($_REQUEST['do'] != 'add') { ?>

    <?php

    // HAVE CATEGORY ID
    if ($category['id']) {

        // FEATURE ARRAY
        $fa = array();

        // EXISTING FEATURE ARRAY
        $efa = array();

        // EXISTING GROUP ARRAY
        $ega = array();

        // GET CATEGORY FEATURES
        $cf_query = "
        SELECT cf.id AS `rel_id`, f.id, f.title, f.active, 'feature' AS `what`, cf.sort
        FROM `" .$_uccms_businessdir->tables['category_features']. "` AS `cf`
        INNER JOIN `" .$_uccms_businessdir->tables['features']. "` AS `f` ON cf.feature_id=f.id
        WHERE (cf.category_id=" .$category['id']. ") AND (cf.feature_id!=0)
        ORDER BY cf.sort ASC, f.sort ASC, f.title ASC, cf.id ASC
        ";
        $cf_q = sqlquery($cf_query);
        if (sqlrows($cf_q) > 0) {
            while ($cf = sqlfetch($cf_q)) {
                $fa[$cf['rel_id']] = $cf;
                $efa[$cf['id']] = $cf['id'];
            }
        }

        // GET CATEGORY FEATURE GROUPS
        $cfg_query = "
        SELECT cf.id AS `rel_id`, fg.id, fg.title, fg.active, 'group' AS `what`, cf.sort
        FROM `" .$_uccms_businessdir->tables['category_features']. "` AS `cf`
        INNER JOIN `" .$_uccms_businessdir->tables['feature_groups']. "` AS `fg` ON cf.group_id=fg.id
        WHERE (cf.category_id=" .$category['id']. ") AND (cf.group_id!=0)
        ORDER BY cf.sort ASC, fg.sort ASC, fg.title ASC, cf.id ASC
        ";
        $cfg_q = sqlquery($cfg_query);
        if (sqlrows($cfg_q) > 0) {
            while ($cfg = sqlfetch($cfg_q)) {
                $fa[$cfg['rel_id']] = $cfg;
                $ega[$cfg['id']] = $cfg['id'];
            }
        }

        // SORT FEATURE ARRAY
        usort($fa, function($a, $b) {
            return $a['sort'] - $b['sort'];
        });

        ?>

        <style type="text/css">

            #manage-features .add_container {
                padding: 6px 10px;
                background-color: #fafafa;
                border-bottom: 1px solid #ccc;
            }

            #manage-features .feature_sort {
                width: 55px;
            }
            #manage-features .feature_title {
                width: 730px;
                text-align: left;
            }
            #manage-features .feature_status {
                width: 100px;
            }
            #manage-features .feature_delete {
                width: 55px;
            }

        </style>

        <script type="text/javascript">

            $(document).ready(function() {

                // FEATURES - HIDE GROUP SELECT
                $('#select_feature-group').hide();

                // FEATURES - ADD CLICK
                $('#manage-features .add_resource').click(function(e) {
                    e.preventDefault();
                    $('#manage-features .add_container').toggle();
                });

                // NEW FEATURE - WHAT CHANGE
                $('#select_feature-what').change(function(e) {
                    var val = $(this).val();
                    $('#manage-features .whats .option').hide();
                    if (val == 'custom') {
                        window.location.href = './features/edit/?item_id=<?php echo $business['id']; ?>';
                    } else {
                        $('#manage-features .whats #select_feature-' +val).show();
                    }
                });

                // FEATURES - SORTABLE
                $('#manage-features ul.ui-sortable').sortable({
                    handle: '.icon_sort',
                    axis: 'y',
                    containment: 'parent',
                    items: 'li',
                    //placeholder: 'ui-sortable-placeholder',
                    update: function() {
                        var rows = $(this).find('li');
                        var sort = [];
                        rows.each(function() {
                            sort.push($(this).attr('data-id'));
                        });
                        $('#manage-features input[name="sort"]').val(sort.join());
                    }
                });

                // FEATURES - REMOVE CLICK
                $('#manage-features .items .item .icon_delete').click(function(e) {
                    e.preventDefault();
                    if (confirm('Are you sure you want to remove this?')) {
                        var id = $(this).attr('data-id');
                        var del = $('#manage-features input[name="delete"]').val();
                        if (del) del += ',';
                        del += id;
                        $('#manage-features input[name="delete"]').val(del);
                        $('#manage-features li[data-id=' +id+ ']').fadeOut(500);
                    }
                });

            });

        </script>

        <a name="manage-features"></a>

        <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

            <form id="form_mange-features" enctype="multipart/form-data" action="./features/manage-process/" method="post">
            <input type="hidden" name="category[id]" value="<?php echo $category['id']; ?>" />

            <div id="manage-features" class="table">

                <input type="hidden" name="sort" value="" />
                <input type="hidden" name="delete" value="" />

                <summary>
                    <h2>Manage Features</h2>
                    <a class="add_resource add" href="#"><span></span>Add Feature</a>
                </summary>

                <div class="add_container" style="display: none;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: auto; margin-bottom: 0px; border: 0px;">
                        <tr>
                            <td>
                                Add:&nbsp;
                            </td>
                            <td>
                                <select name="new_feature-what" id="select_feature-what">
                                    <option value="feature">Feature</option>
                                    <option value="group">Feature Group</option>
                                    <? /*<option value="custom">Custom Feature</option>*/ ?>
                                </select>
                            </td>
                            <td class="whats" style="padding-left: 15px;">

                                <?php

                                // AVAILABLE FEATURES ARRAY
                                $faa = array();

                                // GET ALL FEATURES
                                $a_query = "SELECT `id`, `title` FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`business_id`=0) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                                $a_q = sqlquery($a_query);
                                while ($a = sqlfetch($a_q)) {
                                    if (!in_array($a['id'], $efa)) {
                                        $faa[$a['id']] = $a;
                                    }
                                }

                                ?>

                                <div id="select_feature-feature" class="option">
                                    <?php if (count($faa) > 0) { ?>
                                        <select name="new_feature">
                                            <?php foreach ($faa as $a) { ?>
                                                <option value="<?php echo $a['id']; ?>"><?php echo stripslashes($a['title']); ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } else { ?>
                                        No available features.
                                    <?php } ?>
                                </div>

                                <?php

                                // AVAILABLE GROUPS ARRAY
                                $aga = array();

                                // GET ALL GROUPS
                                $ag_query = "SELECT `id`, `title` FROM `" .$_uccms_businessdir->tables['feature_groups']. "` ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                                $ag_q = sqlquery($ag_query);
                                while ($ag = sqlfetch($ag_q)) {
                                    if (!in_array($ag['id'], $ega)) {
                                        $aga[$ag['id']] = $ag;
                                    }
                                }

                                ?>

                                <div id="select_feature-group" class="option">
                                    <?php if (count($aga) > 0) { ?>
                                        <select name="new_feature-group">
                                            <?php foreach ($aga as $ag) { ?>
                                                <option value="<?php echo $ag['id']; ?>"><?php echo stripslashes($ag['title']); ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } else { ?>
                                        No available groups.
                                    <?php } ?>
                                </div>

                            </td>
                            <td>
                                <input class="blue" type="submit" name="do" value="Add" />
                            </td>
                        </tr>
                    </table>

                </div>

                <header style="clear: both;">
                    <span class="feature_sort"></span>
                    <span class="feature_title">Title</span>
                    <span class="feature_status">Status</span>
                    <span class="feature_delete">Remove</span>
                </header>

                <?php

                // HAVE FEATURES
                if (count($fa) > 0) {

                    ?>

                    <ul class="items ui-sortable">

                        <?php

                        // LOOP
                        foreach ($fa as $feature) {
                            ?>

                            <li class="item contain" data-id="<?php echo $feature['rel_id']; ?>">
                                <section class="feature_sort sort">
                                    <span class="icon_sort ui-sortable-handle"></span>
                                </section>
                                <section class="feature_title">
                                    <?php if ($feature['what'] == 'group') { ?>Group:&nbsp;<?php } ?><?php echo stripslashes($feature['title']); ?>
                                </section>
                                <section class="feature_status status_<?php if ($feature['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                                    <?php if ($feature['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                                </section>
                                <section class="feature_delete">
                                    <a href="#" class="icon_delete" title="Remove feature" data-id="<?php echo $feature['rel_id']; ?>"></a>
                                </section>
                            </li>

                            <?php
                        }

                        ?>

                    </ul>

                    <footer style="clear: both;">
                        <input class="blue" type="submit" value="Save" />
                    </footer>

                    <?php

                // NO FEATURES
                } else {
                    ?>
                    <div style="padding: 15px; text-align: center;">
                        No features added yet.
                    </div>
                    <?php
                }

                ?>

            </div>

            </form>

        </div>

        <?php

    }

    ?>

    <a name="subs"></a>

    <div id="categories" class="table">
        <summary>
            <h2><?php if ($category['id']) { ?>Sub-<?php } ?>Categories</h2>
            <a class="add_resource add" href="./?do=add&parent=<?php echo $category['id']; ?>"><span></span>Add Category</a>
        </summary>
        <header style="clear: both;">
            <span class="category_title">Title</span>
            <span class="category_subs">Sub-Categories</span>
            <span class="category_businesses">Businesses</span>
            <span class="category_status">Status</span>
            <span class="category_edit">Edit</span>
            <span class="category_delete">Delete</span>
        </header>

        <?php

        // GET DIRECT SUB-CATEGORIES
        $sub_query = "SELECT * FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`parent`=" .$id. ") ORDER BY `sort` ASC, `title` ASC, `id` ASC";
        $sub_q = sqlquery($sub_query);

        // NUMBER OF SUB CATEGORIES
        $num_sub = sqlrows($sub_q);

        // HAVE SUB CATEGORIES
        if ($num_sub > 0) {

            ?>

            <ul class="items">

                <?php

                // LOOP
                while ($sub = sqlfetch($sub_q)) {

                    // GET SUB-CATEGORIES OF THIS SUB-CATEGORY
                    $subsub_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`parent`=" .$sub['id']. ")";
                    $subsub_q = sqlquery($subsub_query);

                    // NUMBER OF SUB-SUB-CATEGORIES
                    $num_subsub = sqlrows($subsub_q);

                    // GET BUSINESSES IN CATEGORY
                    $businesses_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`category_id`=" .$sub['id']. ")";
                    $businesses_q = sqlquery($businesses_query);

                    // NUMBER OF BUSINESSES
                    $num_businesses = sqlrows($businesses_q);

                    ?>

                    <li id="row_<?php echo $sub['id']; ?>" class="item">
                        <section class="category_title">
                            <span class="icon_sort ui-sortable-handle"></span>
                            <a href="./?id=<?php echo $sub['id']; ?>"><?php echo stripslashes($sub['title']); ?></a>
                        </section>
                        <section class="category_subs">
                            <?php echo number_format($num_subsub, 0); ?>
                        </section>
                        <section class="category_businesses">
                            <?php if ($num_businesses > 0) { ?><a href="../businesses/?category_id=<?=$sub['id']?>"><?php } echo number_format($num_businesses, 0); ?></a>
                        </section>
                        <section class="category_status status_<?php if ($sub['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                            <?php if ($sub['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                        </section>
                        <section class="category_edit">
                            <a class="icon_edit" title="Edit Category" href="./?id=<?php echo $sub['id']; ?>"></a>
                        </section>
                        <section class="category_delete">
                            <a href="./delete/?id=<?php echo $sub['id']; ?>" class="icon_delete" title="Delete Category" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?\nBusinesses in it will not be deleted, just removed from the category.');"></a>
                        </section>
                    </li>

                    <?php

                }

                ?>

            </ul>

            <?php

        }

        ?>

    </div>

    <script>
        $('#categories .items').sortable({
            axis: 'y',
            containment: 'parent',
            handle: '.icon_sort',
            items: 'li',
            placeholder: 'ui-sortable-placeholder',
            tolerance: 'pointer',
            update: function() {
                $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_businessdir->Extension?>/ajax/admin/categories/order/', {
                    sort: $('#categories .items').sortable('serialize')
                });
            }
        });
    </script>

<?php } ?>