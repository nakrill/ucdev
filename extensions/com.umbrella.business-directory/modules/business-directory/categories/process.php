<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['category']['id'];

    // USE CATEGORY TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['category']['slug']) {
        $_POST['category']['slug'] = $_POST['category']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_businessdir->makeRewrite($_POST['category']['slug']),
        'parent'            => (int)$_POST['category']['parent'],
        'active'            => (int)$_POST['category']['active'],
        'title'             => $_POST['category']['title'],
        'description'       => $_POST['category']['description'],
        'meta_title'        => $_POST['category']['meta_title'],
        'meta_description'  => $_POST['category']['meta_description'],
        'meta_keywords'     => $_POST['category']['meta_keywords']
    );

    // HAVE CATEGORY ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_businessdir->tables['categories']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO CATEGORY ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_businessdir->tables['categories']. "` SET " .uccms_createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Category', 'Category added!');
        } else {
            $admin->growl('Category', 'Category updated!');
        }

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['image']['name']) || ((!$_POST['image']) || (substr($_POST['image'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `image` FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`id`=" .$id. ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['image']) {

                // REMOVE IMAGE
                @unlink($_uccms_businessdir->imageBridgeOut($ex['image'], 'categories', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_businessdir->tables['categories']. "` SET `image`='' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['image']['name']) || ($_POST['image'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'Category Image',
                'key'           => 'image',
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_businessdir->Extension. '/files/categories',
                    'image' => true,
                    'thumbs' => array(
                        array(
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES['image']['name']) {
                $field['file_input'] = $_FILES['image'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['image']) {
                $field['input'] = $_POST['image'];
            }

            // DIGITAL ASSET MANAGER VARS
            $field['dam_vars'] = array(
                'website_extension'         => $_uccms_businessdir->Extension,
                'website_extension_item'    => 'category',
                'website_extension_item_id' => $id
            );

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_businessdir->tables['categories']. "` SET `image`='" .sqlescape($_uccms_businessdir->imageBridgeIn($file_path)). "' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                echo print_r($bigtree['errors']);
                exit;

                $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Category', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'categories/?id=' .$id);

?>