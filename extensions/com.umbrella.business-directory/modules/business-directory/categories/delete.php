<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`category_id`=" .$id. ")";
    sqlquery($query);

    // GET CATEGORY INFO
    $category_query = "SELECT `parent`, `image` FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`id`=" .$id. ")";
    $category_q = sqlquery($category_query);
    $category = sqlfetch($category_q);

    // THERE'S AN EXISTING IMAGE
    if ($category['image']) {

        // REMOVE IMAGE
        @unlink($_uccms_businessdir->imageBridgeOut($category['image'], 'categories', true));

    }

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Category', 'Category deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Category', 'Failed to delete category.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Category', 'No category specified.');
}

BigTree::redirect(MODULE_ROOT.'categories/?id=' .$category['parent']. '#subs');

?>