<style type="text/css">

    #businessdir_dashboard .col_left {
        width: 100%;
        padding-top: 0px;
        padding-left: 0px;
        vertical-align: top;
    }

    #businessdir_dashboard .col_right {
        margin-left: 30px;
        border-left: 1px solid #ccc;
        background-color: #f5f5f5;
    }
    #businessdir_dashboard .col_right .size {
        width: 200px;
        min-height: 400px;
        padding: 0 15px;
    }

    #businessdir_dashboard .toggle_bar {
        padding: 15px 8px;
        background-color: #f8f8f8;
    }
    #businessdir_dashboard .toggle_bar ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #businessdir_dashboard .toggle_bar ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 10px;
        font-weight: bold;
        text-transform: uppercase;
    }
    #businessdir_dashboard .toggle_bar ul li a {
        color: #aaa;
    }
    #businessdir_dashboard .toggle_bar ul li a:hover, #businessdir_dashboard .toggle_bar ul li a.active {
        color: #333;
    }
    #businessdir_dashboard .toggle_bar ul li a .num {
        font-size: .9em;
        font-weight: normal;
        color: #aaa;
    }

    #businessdir_dashboard .toggle_content_container {
    }
    #businessdir_dashboard .toggle_content_container .toggle {
        display: none;
    }
    #businessdir_dashboard .toggle_content_container .toggle:first-child {
        display: block;
    }
    #businessdir_dashboard .toggle_content_container .toggle .none {
        padding: 15px;
        text-align: center;
    }

    #businessdir_dashboard .toggle_content_container .item {
        padding: 15px 15px 15px 0;
        line-height: 1em;
    }
    #businessdir_dashboard .toggle_content_container .item:hover {
        background-color: #f5faff;
    }
    #businessdir_dashboard .toggle_content_container .item table {
        width: 100%;
        margin: 0px;
        padding: 0px;
        border: 0px none;
    }
    #businessdir_dashboard .toggle_content_container .item table td {
        padding: 0px;
        vertical-align: top;
    }
    #businessdir_dashboard .toggle_content_container .item table td table td {
        font-size: 1em;
        color: #999;
        text-transform: uppercase;
    }
    #businessdir_dashboard .toggle_content_container .item td.icon {
        padding: 0 15px;
        white-space: nowrap;
        font-size: 2em;
        opacity: .6;
    }
    #businessdir_dashboard .toggle_content_container .item .icon i.fa {
        /*margin-top: -3px;*/
    }
    #businessdir_dashboard .toggle_content_container .item td.content {
        width: 100%;
    }
    #businessdir_dashboard .toggle_content_container .item .content .title {
        padding-bottom: 6px;
        font-size: 1.2em;
        font-weight: bold;
    }
    #businessdir_dashboard .toggle_content_container .item .content .title a {
        color: #555;
    }
    #businessdir_dashboard .toggle_content_container .item .content .status {
        width: 25%;
    }
    #businessdir_dashboard .toggle_content_container .item .content .status .active {
        color: #6BD873;
    }
    #businessdir_dashboard .toggle_content_container .item .content .status .submitted {
        color: #4dd0e1;
    }
    #businessdir_dashboard .toggle_content_container .item .content .date {
        width: 25%;
    }
    #businessdir_dashboard .toggle_content_container .item .content .actions {
        width: 50%;
        text-align: right;
    }
    #businessdir_dashboard .toggle_content_container .item .content .actions ul {
        opacity: 0;
    }
    #businessdir_dashboard .toggle_content_container .item:hover .content .actions ul {
        opacity: 1;
    }
    #businessdir_dashboard .toggle_content_container .item ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #businessdir_dashboard .toggle_content_container .item ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 0 0 8px;
        line-height: 1em;
    }
    #businessdir_dashboard .toggle_content_container .more {
        padding-top: 15px;
        text-align: center;
    }

    #businessdir_dashboard .activity_container {
        border-left: 2px solid #bbb;
    }

    #businessdir_dashboard .activity_container .day {
        position: relative;
        margin-top: 20px;
        padding-bottom: 20px;
    }

    #businessdir_dashboard .activity_container .day:first-child {
        margin-top: 0px;
    }

    #businessdir_dashboard .activity_container .day .icon {
        position: absolute;
        top: 5px;
        left: -7px;
        width: 0.9em;
        height: 0.9em;
        background-color: #bbb;
        border-radius: 0.9em;
    }

    #businessdir_dashboard .activity_container .day .date {
        margin-left: 20px;
        padding: 5px 10px;
        background-color: #e0e0e0;
        border-radius: 10px;
        text-align: center;
        font-weight: bold;
        color: #666;
    }

    #businessdir_dashboard .activity_container .item {
        position: relative;
    }

    #businessdir_dashboard .activity_container .item .icon {
        position: absolute;
        top: -3px;
        left: -10px;
        width: 1.6em;
        height: 1.6em;
        background-color: #aaa;
        text-align: center;
        font-size: 1em;
        line-height: 1.6em;
        color: #fff;
        border-radius: 1.6em;
    }

    #businessdir_dashboard .activity_container .item .main {
        padding-left: 20px;
    }

    #businessdir_dashboard .activity_container .item .main .title, #businessdir_dashboard .activity_container .item .main .title a {
        padding-bottom: 3px;
        font-size: 1.05em;
        font-weight: bold;
        color: #555;
    }

    #businessdir_dashboard .activity_container .item .main .time {
        padding-bottom: 3px;
        font-size: .9em;
        color: #777;
    }

    #businessdir_dashboard .activity_container .item .main .details {
        padding-top: 1px;
        padding-bottom: 3px;
        font-size: .9em;
        color: #999;
    }

    #businessdir_dashboard .activity_container .item .main .status {
        padding-bottom: 2px;
    }
    #businessdir_dashboard .activity_container .item .main .action {
        color: #777;
    }

    #businessdir_dashboard .activity_container .split {
        height: 1px;
        margin: 15px 0;
        border-bottom: 1px dashed #ddd;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        $('#businessdir_dashboard .toggle_bar a').click(function(e) {
            e.preventDefault();
            var set = $(this).closest('.toggle_bar').attr('data-set');
            var what = $(this).attr('data-what');
            $('#businessdir_dashboard .toggle_bar a').removeClass('active');
            $(this).addClass('active');
            $('#businessdir_dashboard .toggle_content_container.set-' +set+ ' .toggle').hide();
            $('#businessdir_dashboard .toggle_content_container.set-' +set+ ' .toggle.' +what).show();

        });

    });

</script>


<div id="businessdir_dashboard">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 0px;">
        <tr>

            <td class="col_left">

                <?php

                // GET ALL BUSINESSES
                $all_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`status`!=9)";
                $all_q = sqlquery($all_query);
                $num_all = sqlrows($all_q);

                /*
                // RATINGS & REVIEWS ENABLED
                if ($_uccms_businessdir->getSetting('business_ratings_reviews_enabled')) {

                    // CLASS
                    include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
                    if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

                    while ($business = sqlfetch($all_q)) {

                        $result = $_uccms_ratings_reviews->calculate([
                            'vars'  => [
                                'source'    => $_uccms_businessdir->Extension,
                                'what'      => 'business',
                                'item_id'   => $business['id'],
                            ]
                        ]);

                        // UPDATE DB RECORD

                    }

                }
                */

                $all_query = $all_query. " ORDER BY `created_dt` DESC, `id` DESC LIMIT 12";
                $all_q = sqlquery($all_query);

                // GET ACTIVE BUSINESSES
                $active_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`status`=1)";
                $active_q = sqlquery($active_query);
                $num_active = sqlrows($active_q);
                $active_query = $active_query. " ORDER BY `created_dt` DESC, `id` DESC LIMIT 12";
                $active_q = sqlquery($active_query);

                // GET SUBMITTED BUSINESSES
                $submitted_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`submitted`=1) AND (`status`!=9)";
                $submitted_q = sqlquery($submitted_query);
                $num_submitted = sqlrows($submitted_q);
                $submitted_query = $submitted_query. " ORDER BY `created_dt` DESC, `id` DESC LIMIT 12";
                $submitted_q = sqlquery($submitted_query);

                // GET PENDING BUSINESSES
                $pending_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`pending`>0)";
                $pending_q = sqlquery($pending_query);
                $num_pending = sqlrows($pending_q);
                $pending_query = $pending_query. " ORDER BY `created_dt` DESC, `id` DESC LIMIT 12";
                $pending_q = sqlquery($pending_query);

                ?>

                <div class="toggle_bar" data-set="businesses">
                    <ul>
                        <li><a href="#" class="active" data-what="all">All Businesses <span class="num">(<?php echo number_format($num_all, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="active">Active <span class="num">(<?php echo number_format($num_active, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="submitted">Submitted <span class="num">(<?php echo number_format($num_submitted, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="pending">Pending <span class="num">(<?php echo number_format($num_pending, 0); ?>)</span></a></li>
                    </ul>
                </div>

                <div class="toggle_content_container set-businesses">

                    <div class="toggle all">
                        <?php if ($num_all > 0) { ?>
                            <div class="items">
                                <?php
                                while ($business = sqlfetch($all_q)) {
                                    echo this_item_business($business);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./businesses/" class="button">All Businesses</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No businesses.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle active">
                        <?php if ($num_active > 0) { ?>
                            <div class="items">
                                <?php
                                while ($business = sqlfetch($active_q)) {
                                    echo this_item_business($business);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./businesses/?status=active" class="button">All Active Businesses</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No active businesses.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle submitted">
                        <?php if ($num_submitted > 0) { ?>
                            <div class="items">
                                <?php
                                while ($business = sqlfetch($submitted_q)) {
                                    echo this_item_business($business);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./businesses/?status=submitted" class="button">All Submitted Businesses</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No submitted businesses.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle pending">
                        <?php if ($num_pending > 0) { ?>
                            <div class="items">
                                <?php
                                while ($business = sqlfetch($pending_q)) {
                                    echo this_item_business($business);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./businesses/?status=pending" class="button">All Pending Businesses</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No pending businesses.
                            </div>
                        <?php } ?>
                    </div>

                </div>

            </td>

            <td class="col_right" valign="top">
                <div class="size">

                    <h3 style="text-align: center;">Recent Activity</h3>

                    <div class="activity_container">

                        <?php

                        // ITEM ARRAY
                        $itema = array();

                        // LAST CREATED
                        $lcreated_query = "SELECT *, `created_dt` AS `sort_by`, 'created' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`created_dt`!='0000-00-00 00:00:00') AND (`pending`=0) ORDER BY `created_dt` DESC LIMIT 10";
                        $lcreated_q = sqlquery($lcreated_query);
                        while ($lcreated = sqlfetch($lcreated_q)) {
                            $itema[] = $lcreated;
                        }

                        // LAST SUBMITTED
                        $lsubmitted_query = "SELECT *, `submitted_dt` AS `sort_by`, 'submitted' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`submitted_dt`!='0000-00-00 00:00:00') ORDER BY `submitted_dt` DESC LIMIT 10";
                        $lsubmitted_q = sqlquery($lsubmitted_query);
                        while ($lsubmitted = sqlfetch($lsubmitted_q)) {
                            $itema[] = $lsubmitted;
                        }

                        // LAST UPDATED
                        $lupdated_query = "SELECT *, `updated_dt` AS `sort_by`, 'updated' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`updated_dt`!='0000-00-00 00:00:00') ORDER BY `updated_dt` DESC LIMIT 10";
                        $lupdated_q = sqlquery($lupdated_query);
                        while ($lupdated = sqlfetch($lupdated_q)) {
                            $itema[] = $lupdated;
                        }

                        // LAST DELETED
                        $ldeleted_query = "SELECT *, `deleted_dt` AS `sort_by`, 'deleted' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`deleted_dt`!='0000-00-00 00:00:00') ORDER BY `deleted_dt` DESC LIMIT 10";
                        $ldeleted_q = sqlquery($ldeleted_query);
                        while ($ldeleted = sqlfetch($ldeleted_q)) {
                            $itema[] = $ldeleted;
                        }

                        // HAVE ITEMS
                        if (count($itema) > 0) {

                            // SORT BY DATE/TIME ASCENDING
                            usort($itema, function($a, $b) {
                                return strtotime($b['sort_by']) - strtotime($a['sort_by']);
                            });

                            // STATUSES
                            $statusa = array(
                                'created'   => array(
                                    'title'     => 'Created',
                                    'icon'      => 'fa-plus-circle',
                                    'color'     => '#85eeb8',
                                    'actions'   => '<a href="./businesses/edit/?id={id}">Edit</a>'
                                ),
                                'submitted'     => array(
                                    'title'     => 'Submitted',
                                    'icon'      => 'check-circle',
                                    'color'     => '#4dd0e1',
                                    'actions'   => '<a href="./businesses/edit/?id={id}">Edit</a>'
                                ),
                                'updated' => array(
                                    'title'     => 'Updated',
                                    'icon'      => 'fa-floppy-o',
                                    'color'     => '#00c05d',
                                    'actions'   => '<a href="./businesses/edit/?id={id}">Edit</a>'
                                ),
                                'deleted'   => array(
                                    'title'     => 'Deleted',
                                    'icon'      => 'fa-times-circle',
                                    'color'     => '#e53935'
                                )
                            );

                            $i = 1;

                            // LOOP
                            foreach ($itema as $item) {
                                $actiona = array(
                                    $item['id']
                                );
                                ?>
                                <div class="item contain">
                                    <?php if ($statusa[$item['item_what']]['icon']) { ?>
                                        <div class="icon" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'background-color: ' .$statusa[$item['item_what']]['color']; } ?>"><i class="fa fa-fw <?php echo $statusa[$item['item_what']]['icon']; ?>"></i></div>
                                    <?php } ?>
                                    <div class="main">
                                        <?php if ($statusa[$item['item_what']]['title']) { ?>
                                            <div class="status" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'color: ' .$statusa[$item['item_what']]['color']; } ?>">
                                                <?php echo $statusa[$item['item_what']]['title']; if ($item['pending']) { echo ' <span style="color: #ff8a80;">(pending)</span>'; } ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($item['name']) { ?>
                                            <div class="title">
                                                <?php if ($item['status'] != 9) { ?>
                                                    <a href="./businesses/edit/?id=<?php echo $item['id']; ?>">
                                                <?php } ?>
                                                <?php echo stripslashes($item['name']); ?>
                                                <?php if ($item['status'] != 9) { ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="time"><?php echo date('j M g:i A', strtotime($item['sort_by'])); ?></div>
                                        <?php if ($statusa[$item['item_what']]['actions']) { ?>
                                            <div class="actions">
                                                <i class="fa fa-caret-right"></i> <?php echo str_replace(array('{id}'), $actiona, $statusa[$item['item_what']]['actions']); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if ($i == 10) {
                                    break;
                                } else {
                                    ?>
                                    <div class="split"></div>
                                    <?php
                                    $i++;
                                }
                            }

                            unset($itema);

                        }

                        ?>

                    </div>

                </div>
            </td>

        </tr>
    </table>

</div>

<?php

function this_item_business($business) {
    global $_uccms_businessdir;
    if ($business['id']) {
        $out = '
        <div class="item">
            <table>
                <tr>
                    <td class="icon"><i class="fa fa-building-o"></i></td>
                    <td class="content">
                        <div class="title"><a href="./businesses/edit/?id=' .$business['id']. '">' .stripslashes($business['name']). '</a></div>
                        <table>
                            <tr>
                                <td class="status">
                                    ';
                                    if ($business['submitted'] == 1) {
                                        $out .= '<span class="submitted">Submitted</span>';
                                    } else {
                                        if ($business['status'] == 1) {
                                            $out .= '<span class="active">Active</span>';
                                        } else if ($business['pending']) {
                                            $out .= '<span class="pending">Pending</span>';
                                        } else {
                                            $out .= '<span class="inactive">Inactive</span>';
                                        }
                                    }
                                    $out .= '
                                </td>
                                <td class="date">
                                    ';
                                    if ($business['submitted'] == 1) {
                                        $out .= date('j M', strtotime($business['submitted_dt']));
                                    } else {
                                        if ($business['updated_dt'] != '0000-00-00 00:00:00') {
                                            $out .= date('j M', strtotime($business['updated_dt']));
                                        } else {
                                            $out .= date('j M', strtotime($business['created_dt']));
                                        }
                                    }
                                    $out .= '
                                </td>
                                <td class="actions">
                                    <ul>
                                        <li><a href="./businesses/edit/?id=' .$business['id']. '">Edit</a></li>
                                        ';
                                        if ($business['submitted'] != 1) {
                                            $out .= '<li><a href="' .$_uccms_businessdir->businessURL($business['id'], 0, $business). '">View</a></li>';
                                        }
                                        $out .= '
                                        <li><a href="./businesses/delete/?id=' .$business['id']. '&from=dashboard" onclick="return confirmPrompt(this.href, \'Are you sure you want to delete this this?\');">Delete</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        ';
    }
    return $out;
}

?>