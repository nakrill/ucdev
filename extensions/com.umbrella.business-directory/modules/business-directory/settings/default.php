<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET SETTINGS
$settings = $_uccms_businessdir->getSettings();

?>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>General</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <input id="submissions_enabled" type="checkbox" name="setting[submissions_enabled]" value="1" <?php if ($settings['submissions_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Submissions enabled <small>Allow business submissions.</small></label>
                </fieldset>

                <fieldset>
                    <input id="claiming_enabled" type="checkbox" name="setting[claiming_enabled]" value="1" <?php if ($settings['claiming_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Claiming enabled <small>Allow business to claim a listing.</small></label>
                </fieldset>

                <?php if ($admin->Level == 2) { ?>
                    <fieldset>
                        <input type="checkbox" name="setting[localeze_enabled]" value="1" <?php if ($settings['localeze_enabled']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Enable Localeze integration.</label>
                    </fieldset>
                <?php } ?>

                <fieldset>
                    <input type="checkbox" name="setting[search_disabled]" value="1" <?php if ($settings['search_disabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Search disabled. <small>Don't allow people to search for businesses.</small></label>
                </fieldset>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Home</h2>
    </header>

    <section>

        <fieldset>
            <label>Content</label>
            <div>
                <?php
                $field = array(
                    'key'       => 'setting[home_content]', // The value you should use for the "name" attribute of your form field
                    'value'     => stripslashes($settings['home_content']), // The existing value for this form field
                    'id'        => 'home_content', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'simple' => false
                    )
                );
                include(BigTree::path('admin/form-field-types/draw/html.php'));
                ?>
            </div>
        </fieldset>

        <div class="contain" style="padding-bottom: 20px;">

            <div class="left last">

                <fieldset>
                    <label>Number of Featured Businesses</label>
                    <input type="text" name="setting[home_featured_num]" value="<?php echo (int)$settings['home_featured_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset>
                    <label>Featured Businesses Title</label>
                    <input type="text" name="setting[home_featured_title]" value="<?php echo stripslashes($settings['home_featured_title']); ?>" placeholder="Featured Businesses" />
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <label>Number of Recent Businesses</label>
                    <input type="text" name="setting[home_recent_num]" value="<?php echo (int)$settings['home_recent_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset>
                    <label>Recent Businesses Title</label>
                    <input type="text" name="setting[home_recent_title]" value="<?php echo stripslashes($settings['home_recent_title']); ?>" placeholder="Recent Businesses" />
                </fieldset>

            </div>

        </div>

        <fieldset>
            <input type="checkbox" name="setting[home_hide_categories]" value="1" <?php if ($settings['home_hide_categories']) { ?>checked="checked"<?php } ?> />
            <label class="for_checkbox">Hide the categories section.</small></label>
        </fieldset>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="home_seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-home_seo" style="display: none;">

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="setting[home_meta_title]" value="<?php echo stripslashes($settings['home_meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="setting[home_meta_description]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="setting[home_meta_keywords]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Categories</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Number of Listings Per Page</label>
                    <input type="text" name="setting[listings_per_page]" value="<?php echo (int)$settings['listings_per_page']; ?>" class="smaller_60" />
                </fieldset>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<script type="text/javascript">

    $(document).ready(function() {

        // CONTACT FIELDS - SORTABLE
        $('form .listing_contact table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // CONTACT FIELDS - ADD
        $('form .listing_contact .add').click(function(e) {
            e.preventDefault();
            $('#add_listing_contact_field').toggle();
        });

        // CONACT FIELDS - REMOVE
        $('form .listing_contact .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

        // CONTACT FIELDS TOGGLE
        $('#listing_contact_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_listing_contact').show();
            } else {
                $('#toggle_listing_contact').hide();
            }
        });

        // CHANGE CATEGORY ENABLED TOGGLE
        $('#listing_change_category_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_listing_change_category').show();
            } else {
                $('#toggle_listing_change_category').hide();
            }
        });

        // BUSINESS - REVIEW CRITERIA TOGGLE
        $('#business_ratings_reviews_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_business_reviews').show();
            } else {
                $('#toggle_business_reviews').hide();
            }
        });

        // BUSINESS - REVIEW CRITERIA - SORTABLE
        $('form .business_review-criteria table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // BUSINESS - REVIEW CRITERIA - ADD
        $('form .business_review-criteria .add').click(function(e) {
            e.preventDefault();
            $('#add_business_review-criteria').toggle();
        });

        // BUSINESS - REVIEW CRITERIA - REMOVE
        $('form .business_review-criteria .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

    });

</script>

<a name="business"></a>
<div class="container legacy">

    <header>
        <h2>Business Listing</h2>
    </header>

    <section>

        <div class="contain">

            <fieldset>
                <input id="listing_contact_enabled" type="checkbox" name="setting[listing_contact_enabled]" value="1" <?php if ($settings['listing_contact_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Contact Form Enabled</label>
            </fieldset>

            <div id="toggle_listing_contact" class="contain" style="<?php if (!$settings['listing_contact_enabled']) { ?>display: none;<?php } ?> padding-top: 10px;">

                <div class="left">

                    <fieldset class="listing_contact">
                        <div class="contain">
                            <div class="left inner_quarter last">
                                <label>Fields</label>
                            </div>
                            <div class="left inner_quarter last" style="margin: 0px; text-align: right;">
                                <a href="#" class="add button_small">Add</a>
                            </div>
                        </div>
                        <div id="add_listing_contact_field" style="display: none; padding: 5px 0 10px 0;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px;">
                                <tr>
                                    <td style="padding-right: 0px;">
                                        <input type="text" name="listing_contact_field_title[0]" value="" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td style="padding-right: 0px;">
                                        <input type="checkbox" name="listing_contact_field_required[0]" value="1" /> Req
                                    </td>
                                    <td>
                                        <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                            <?php foreach ($_uccms_businessdir->listingContactFields() as $field_id => $field) { ?>
                                <tr id="pg-<?=$field_id?>" class="item">
                                    <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                                    <td width="60%" style="padding-right: 0px;">
                                        <input type="text" name="listing_contact_field_title[<?=$field_id?>]" value="<?=$field['title']?>" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td width="40%" style="padding-right: 0px; white-space: nowrap;">
                                         <input type="checkbox" name="listing_contact_field_required[<?=$field_id?>]" value="1" <?php if ($field['required'] == 1) { ?>checked="checked"<?php } ?> /> Req
                                    </td>
                                    <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </fieldset>

                </div>

                <div class="right">

                    <fieldset>
                        <label>Success Message</label>
                        <div>
                            <?php
                            $field = array(
                                'key'       => 'setting[listing_contact_success]', // The value you should use for the "name" attribute of your form field
                                'value'     => stripslashes($settings['listing_contact_success']), // The existing value for this form field
                                'id'        => 'listing_contact_success', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'simple' => false
                                )
                            );
                            include(BigTree::path('admin/form-field-types/draw/html.php'));
                            ?>
                        </div>
                    </fieldset>

                </div>

            </div>

            <fieldset>
                <label>Max Categories per Listing</label>
                <input type="text" name="setting[listing_max_categories]" value="<?php echo (int)$settings['listing_max_categories']; ?>" class="smaller_60" />
                <small>0 = none allowed, blank = no limit</small>
            </fieldset>

            <fieldset>
                <label>Max Images per Listing</label>
                <input type="text" name="setting[listing_max_images]" value="<?php echo $settings['listing_max_images']; ?>" class="smaller_60" />
                <small>0 = none allowed, blank = no limit</small>
            </fieldset>

            <fieldset>
                <label>Max Videos per Listing</label>
                <input type="text" name="setting[listing_max_videos]" value="<?php echo $settings['listing_max_videos']; ?>" class="smaller_60" />
                <small>0 = none allowed, blank = no limit</small>
            </fieldset>

        </div>

        <div class="contain" style="margin-top: 15px;">

            <fieldset>
                <input id="business_ratings_reviews_enabled" type="checkbox" name="setting[business_ratings_reviews_enabled]" value="1" <?php if ($settings['business_ratings_reviews_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Ratings & Reviews Enabled</label>
            </fieldset>

            <div id="toggle_business_reviews" class="contain" style="<?php if (!$settings['business_ratings_reviews_enabled']) { ?>display: none;<?php } ?>">

                <div class="left">

                    <fieldset class="business_review-criteria">
                        <div class="contain">
                            <div class="left inner_quarter last">
                                <label>Review Criteria</label>
                            </div>
                            <div class="left inner_quarter last" style="margin: 0px; text-align: right;">
                                <a href="#" class="add button_small">Add</a>
                            </div>
                        </div>
                        <div id="add_business_review-criteria" style="display: none; padding: 5px 0 10px 0;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px;">
                                <tr>
                                    <td style="padding-right: 0px;">
                                        <input type="text" name="business_review-criteria_title[0]" value="" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td style="padding-right: 0px;">
                                        <input type="checkbox" name="business_review-criteria_required[0]" value="1" /> Req
                                    </td>
                                    <td>
                                        <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                            <?php foreach ($_uccms_businessdir->review_criteria('business') as $criteria_id => $criteria) { ?>
                                <tr id="prc-<?=$criteria_id?>" class="item">
                                    <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                                    <td width="60%" style="padding-right: 0px;">
                                        <input type="text" name="business_review-criteria_title[<?=$criteria_id?>]" value="<?=$criteria['title']?>" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td width="40%" style="padding-right: 0px; white-space: nowrap;">
                                         <input type="checkbox" name="business_review-criteria_required[<?=$criteria_id?>]" value="1" <?php if ($criteria['required'] == 1) { ?>checked="checked"<?php } ?> /> Req
                                    </td>
                                    <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </fieldset>

                </div>

                <div class="right">

                </div>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<a name="claiming"></a>
<div id="claiming" class="container legacy">

    <header>
        <h2>Claiming</h2>
    </header>

    <section>

        <div class="contain">

            <fieldset>
                <label>Content <small>Displayed above form.</small></label>
                <div>
                    <?php
                    $field = array(
                        'key'       => 'setting[claiming_content]', // The value you should use for the "name" attribute of your form field
                        'value'     => stripslashes($settings['claiming_content']), // The existing value for this form field
                        'id'        => 'claiming_content', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'simple' => false
                        )
                    );
                    include(BigTree::path('admin/form-field-types/draw/html.php'));
                    ?>
                </div>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<script type="text/javascript">

    $(document).ready(function() {

        // UPDATE APPROVAL ENABLED
        $('#update_approval_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#update_approval_content').show();
            } else {
                $('#update_approval_content').hide();
            }
        });

    });

</script>

<a name="submissions"></a>
<div id="submissions" class="container legacy">

    <header>
        <h2>Submissions</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Price <small>Blank field = option not available.</small></label>
                    $<input type="text" name="setting[listing_price_mo]" value="<?php echo $settings['listing_price_mo']; ?>" class="smaller_60" style="display: inline-block;" /> /mo
                    &nbsp;&nbsp;
                    $<input type="text" name="setting[listing_price_yr]" value="<?php echo $settings['listing_price_yr']; ?>" class="smaller_60" style="display: inline-block;" /> /yr
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <input id="update_approval_enabled" type="checkbox" name="setting[approve_listing_update]" value="1" <?php if ($settings['approve_listing_update']) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox">Listing updates must be approved.</label>
                </fieldset>

                <div id="update_approval_content" style="<?php if (!$settings['approve_listing_update']) { ?>display: none;<?php } ?>">
                    <fieldset>
                        <label>Notify these email addresses on updates.</label>
                        <input type="text" name="setting[approve_listing_update_emails]" value="<?php echo stripslashes($settings['approve_listing_update_emails']); ?>" placeholder="Separate multiple addresses by comma" />
                    </fieldset>
                </div>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>

</form>