<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SETTINGS ARRAY
    $seta = array(
        'email_from_name',
        'email_from_email',
        'email_submission_copy'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_businessdir->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('Email Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/email/');

?>