<?php

// GET SETTINGS
$settings = $_uccms_businessdir->getSettings();

?>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>Email Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>From Name</label>
                <input type="text" name="setting[email_from_name]" value="<?php echo $settings['email_from_name']; ?>" />
            </fieldset>

            <fieldset>
                <label>From Email</label>
                <input type="text" name="setting[email_from_email]" value="<?php echo $settings['email_from_email']; ?>" />
            </fieldset>

        </div>

        <div class="right last">

            <?php if ($settings['submissions_enabled']) { ?>

                <fieldset>
                    <label>Send new business submission notification emails to:</label>
                    <input type="text" name="setting[email_submission_copy]" value="<?php echo $settings['email_submission_copy']; ?>" />
                    <label><small>(Seperate email addresses by comma.)</small></label>
                </fieldset>

            <?php } ?>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

</form>