<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // FORMAT
    if ($_POST['setting']['listing_price_mo']) $_POST['setting']['listing_price_mo'] = number_format($_POST['setting']['listing_price_mo'], 2);
    if ($_POST['setting']['listing_price_yr']) $_POST['setting']['listing_price_yr'] = number_format($_POST['setting']['listing_price_yr'], 2);

    // SETTINGS ARRAY
    $seta = array(
        'submissions_enabled',
        'localeze_enabled',
        'search_disabled',
        'home_content',
        'home_featured_num',
        'home_featured_title',
        'home_recent_num',
        'home_recent_title',
        'home_meta_title',
        'home_meta_description',
        'home_meta_keywords',
        'home_hide_categories',
        'listings_per_page',
        'listing_max_categories',
        'listing_max_images',
        'listing_max_videos',
        'claiming_enabled',
        'claiming_content',
        'listing_price_mo',
        'listing_price_yr',
        'listing_contact_enabled',
        'listing_contact_success',
        'approve_listing_update',
        'approve_listing_update_emails',
        'business_ratings_reviews_enabled'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_businessdir->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('General Settings', 'Settings saved!');

    ########################
    # LISTING CONTACT FIELDS
    ########################

    // NEW FIELD
    $new_field['title']     = $_POST['listing_contact_field_title'][0];
    $new_field['required']  = $_POST['listing_contact_field_required'][0];
    unset($_POST['listing_contact_field_title'][0]);
    unset($_POST['listing_contact_field_required'][0]);

    // CONTACT FIELD ARRAY
    $cfa = array();

    // TOP FIELD ID
    $top_field_id = 0;

    // CONTACT FIELDS SPECIFIED
    if (count($_POST['listing_contact_field_title']) > 0) {

        // LOOP
        foreach ($_POST['listing_contact_field_title'] as $field_id => $field_title) {
            $cfa[$field_id] = array(
                'title'     => $field_title,
                'required'  => (int)$_POST['listing_contact_field_required'][$field_id]
            );
            if ($field_id > $top_field_id) $top_field_id = $field_id;
        }

    // NO FIELDS SPECIFIED
    } else {

        // USE DEFAULTS
        foreach ($_uccms_businessdir->listingContactFields() as $field_id => $field) {
            $cfa[$field_id] = $field;
            if ($field_id > $top_field_id) $top_field_id = $field_id;
        }

    }

    // HAVE NEW FIELD
    if ($new_field['title']) {
        $cfa[$top_field_id+1] = array(
            'title'     => $new_field['title'],
            'required'  => $new_field['required']
        );
    }

    // SAVE PRICE GROUPS
    $_uccms_businessdir->setSetting('listing_contact_fields', $cfa);

    ########################
    # BUSINESS - REVIEW CRITERIA
    ########################

    // NEW FIELD
    $new_field['title']     = $_POST['business_review-criteria_title'][0];
    $new_field['required']  = $_POST['business_review-criteria_required'][0];
    unset($_POST['business_review-criteria_title'][0]);
    unset($_POST['business_review-criteria_required'][0]);

    // REVIEW CRITERIA FIELD ARRAY
    $prca = array();

    // TOP FIELD ID
    $top_field_id = 0;

    // REVIEW CRITERIA FIELDS SPECIFIED
    if (count($_POST['business_review-criteria_title']) > 0) {

        // LOOP
        foreach ($_POST['business_review-criteria_title'] as $field_id => $field_title) {
            $prca[$field_id] = array(
                'title'     => $field_title,
                'required'  => (int)$_POST['business_review-criteria_required'][$field_id]
            );
            if ($field_id > $top_field_id) $top_field_id = $field_id;
        }

    }

    // HAVE NEW FIELD
    if ($new_field['title']) {
        $prca[$top_field_id+1] = array(
            'title'     => $new_field['title'],
            'required'  => $new_field['required']
        );
    }

    // SAVE REVIEW CRITERIA FIELDS
    $_uccms_businessdir->setSetting('review_criteria-business', $prca);

    ########################

}

BigTree::redirect(MODULE_ROOT.'settings/');

?>