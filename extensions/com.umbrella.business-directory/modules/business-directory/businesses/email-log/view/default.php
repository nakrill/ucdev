<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET LOG INFO
    $log_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_email_log']. "` WHERE (`id`=" .$id. ")";
    $log_q = sqlquery($log_query);
    $log = sqlfetch($log_q);

    // LOG FOUND
    if ($log['id']) {

        // GET BUSINESS INFO
        $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$log['business_id']. ")";
        $business_q = sqlquery($business_query);
        $business = sqlfetch($business_q);

    }

}

// LOG NOT FOUND
if (!$log['id']) {
    $admin->growl('Email Log', 'Could not find log.');
    BigTree::redirect(MODULE_ROOT. 'businesses/email-log/');
}

?>

<div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

    <div class="container legacy">

        <header>
            <h2>Email Log</h2>
        </header>

        <section>

            <div class="contain">

                <table style="width: auto;">
                    <tr>
                        <td>Business: </td>
                        <td><a href="../../edit/?id=<?php echo $business['id']; ?>"><?php echo stripslashes($business['name']); ?></a></td>
                    </tr>
                    <tr>
                        <td>Date: </td>
                        <td><?php echo date('n/j/Y g:i A', strtotime($log['dt'])); ?></td>
                    </tr>
                    <tr>
                        <td>Logs: </td>
                        <td><a href="../?business_id=<?php echo $business['id']; ?>">all logs for business</a></td>
                    </tr>
                </table>

            </div>

            <div class="contain">

                <?php

                // GET SUBMITTED DATA
                $data = json_decode(str_replace("\n", '<br />', stripslashes($log['data'])), true);

                // HAVE DATA
                if (count($data) > 0) {

                    ?>

                    <table>

                        <?php

                        foreach ($data as $field_name => $field_value) {
                            ?>
                            <tr>
                                <td style="white-space: nowrap; vertical-align: top;"><?php echo $field_name; ?>: </td>
                                <td style="width: 100%;"><?php echo $field_value; ?></td>
                            </tr>
                            <?php
                        }

                        ?>

                    </table>

                    <?php

                // NO DATA
                } else {
                    echo 'No data submitted.';
                }

                ?>

            </div>

        </section>

        <footer>
            <a class="button back" href="../">&laquo; Back</a>
        </footer>

    </div>

</div>