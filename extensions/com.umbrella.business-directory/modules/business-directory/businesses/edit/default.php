<?php

function business_fieldChanged($field, $combine=', ') {
    global $business, $master;
    if ($business['pending']) {
        $diff = false;
        if (is_array($field)) {
            $tda = array();
            foreach ($field as $field) {
                if ($business[$field] != $master[$field]) {
                    if ($master[$field]) {
                        $tda[] = $master[$field];
                    } else {
                        $tda[] = '<span class="null">Null</span>';
                    }
                }
            }
            if (count($tda) > 0) {
                $diff = implode($combine, $tda);
            }
        } else {
            if ($business[$field] != $master[$field]) {
                if ($master[$field]) {
                    $diff = $master[$field];
                } else {
                    $diff = '<span class="null">Null</span>';
                }
            }
        }
        return fieldChanged_output($diff);
    }
}

function extra_fieldChanged($val_curr, $val_orig, $combine=', ') {
    if (is_array($val_curr)) {

    } else {
        if ($val_curr != $val_orig) {
            if ($val_orig) {
                $diff = $val_orig;
            } else {
                $diff = '<span class="null">Null</span>';
            }
        }
    }
    return fieldChanged_output($diff);
}

function fieldChanged_output($diff) {
    if ($diff) {
        return '
        <div class="fieldChanged">
            <div class="head contain">
                <span class="orig">Original:</span>
                <a href="#" class="toggle">Changed</a>
            </div>
            <div class="orig">
                ' .$diff. '
            </div>
        </div>
        ';
    }
}

###########################################################




// CLEAN UP
$id = (int)$_REQUEST['id'];

// PAYMENT METHODS
$pma = array();

// LANGUAGES
$la = array();

// ID SPECIFIED
if ($id) {

    // GET BUSINESS INFO
    $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$id. ")";
    $business_q = sqlquery($business_query);
    $business = sqlfetch($business_q);

    // HAVE BUSINESS ID
    if ($business['id']) {

        // GET HOURS
        $hours_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$business['id']. ")";
        $hours_q = sqlquery($hours_query);
        while ($hours = sqlfetch($hours_q)) {
            $business['hours'][$hours['day']] = $hours;
        }

        // PAYMENT METHODS
        $pma = explode(',', stripslashes($business['payment_methods']));

        // LANGUAGES
        $la = explode(',', stripslashes($business['languages']));

        // SEE IF THERE'S A PENDING VERSION
        $pending_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`pending`=" .$business['id']. ")";
        $pending_q = sqlquery($pending_query);
        $pending = sqlfetch($pending_q);

        // IS PENDING VERSION
        if ($business['pending']) {

            // GET MASTER
            $master_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$business['pending']. ")";
            $master_q = sqlquery($master_query);
            $master = sqlfetch($master_q);

        }

    }

// NO ID SPECIFIED
} else {

    // USE URL VARS FOR BUSINESS INFO
    $business = $_REQUEST;

    // HAVE BUSINESS NAME
    if ($business['name']) {
        $business['slug'] = $_uccms_businessdir->makeRewrite($business['name']); // CREATE SLUG
    }

}

// GET FRONTEND ACCOUNTS
$account_query = "
SELECT CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, a.*, ad.*
FROM `uccms_accounts` AS `a`
INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
ORDER BY `fullname` ASC, `email` ASC, a.id ASC
";
$account_q = sqlquery($account_query);
while ($account = sqlfetch($account_q)) {
    $accta[$account['id']] = $account;
}

// ARRAY OF CITIES
$citiesa = array();

// GET CITIES
$cities_query = "
SELECT c.*
FROM `" .$_uccms_businessdir->tables['cities']. "` AS `c`
ORDER BY c.title ASC, c.id ASC
";
$cities_q = sqlquery($cities_query);
while ($agency = sqlfetch($cities_q)) {
    $citiesa[$agency['id']] = $agency;
}

// ARRAY OF NEIGHBORHOODS
$neighborhoodsa = array();

// GET NEIGHBORHOODS
$neighborhoods_query = "
SELECT n.*
FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` AS `n`
ORDER BY n.title ASC, n.id ASC
";
$neighborhoods_q = sqlquery($neighborhoods_query);
while ($neighborhood = sqlfetch($neighborhoods_q)) {
    $neighborhoodsa[$neighborhood['city_id']][$neighborhood['id']] = $neighborhood;
}

// ARRAYS
if (!is_array($accta)) $accta = array();
if (!is_array($business['languages'])) $business['languages'] = array();
if (!is_array($business['hours'])) $business['hours'] = array();

?>

<style type="text/css">

    #page .section {
        margin-bottom: 30px;
    }
    #page .section .table {
        margin: 0px;
        background-color: #fff;
    }
    #page .section .table footer {
        background: #eee;
        border-radius: 0 0 2px 2px;
        -webkit-border-radius: 0 0 2px 2px;
        -moz-border-radius: 0 0 2px 2px;
        -ms-border-radius: 0 0 2px 2px;
        -o-border-radius: 0 0 2px 2px;
        border-top: 1px solid #ccc;
        border-top: 1px solid #ccc;
        overflow: hidden;
        padding: 30px;
    }

    .fieldChanged {
        clear: both;
        margin-top: 2px;
    }
    .fieldChanged .orig {
        display: none;
    }
    .fieldChanged .head {
        margin-top: 4px;
    }
    .fieldChanged .head .orig {
        float: left;
    }
    .fieldChanged .head .toggle {
        float: right;
        color: #ff8a80;
    }
    .fieldChanged > .orig {
        margin-top: 2px;
        padding: 4px 0 0;
        border-top: 1px solid #e5e5e5;
        text-align: left;
        color: #333;
    }
    .fieldChanged > .orig .null {
        font-style: italic;
        opacity: .7;
    }

    .new_changed {
        clear: both;
        margin-top: 3px;
        text-align: left;
        color: #ff8a80;
    }
    .new_changed a {
        color: #ff8a80;
    }

    #images .fieldChanged, #videos .fieldChanged {
        margin-right: 48px;
    }

    #business_container .top_stats {
        margin-bottom: 15px;
    }

    #business_container .top_stats .stat {
        float: left;
        width: 25%;
        text-align: center;
    }
    #business_container .top_stats .stat .padding {
        display: inline-block;
        min-width: 150px;
        margin: 10px;
        padding: 20px 20px 15px;
        background-color: #fff;
        text-align: center;
        border-radius: 5px;
    }
    #business_container .top_stats .stat .num {
        display: block;
        font-size: 2em;
    }
    #business_container .top_stats .stat .title {
        display: block;
        margin-top: 5px;
        font-size: 1.2em;
        font-weight: bold;
    }
    #business_container .top_stats .stat .period {
        display: block;
        margin-top: 2px;
        opacity: .8;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        $('.fieldChanged .head .toggle').click(function(e) {
            e.preventDefault();
            $(this).closest('.fieldChanged').find('.orig').toggle();
        });

        // CITY CHANGE
        $('select[name="business[city_id]"]').change(function() {
            $('#neighborhoods .city').hide();
            $('#neighborhoods .city-' +$(this).val()).show();
        });

    });

</script>


<?php

// HAVE PENDING VERSION
if ($pending['id']) {
    ?>
    <div style="margin-bottom: 10px; padding: 8px; border: 1px solid #ff8a80; border-radius: 4px; font-weight: bold; color: #ff8a80;">
        There is a <a href="?id=<?php echo $pending['id']; ?>">pending version</a> of this listing.
    </div>
    <?php
}

// IS PENDING VERSION
if ($business['pending']) {
    ?>
    <div style="margin-bottom: 10px; padding: 8px; border: 1px solid #ff8a80; border-radius: 4px; font-weight: bold; color: #ff8a80;">
        This is the pending version of <a href="?id=<?php echo $business['pending']; ?>">this listing</a>.
    </div>
    <?php
}

?>

<div id="business_container" class="contain" style="margin-bottom: 0px; border: 0px none;">

    <?php

    // BUSINESS EXISTS AND ISN'T PENDING
    if (($business['id']) && (!$business['pending'])) {

        // GET TOTAL STATS
        $stats_total = $_uccms_businessdir->business_getStats($business['id']);

        // GET TIMEFRAME STATS
        $stats_timeframe = $_uccms_businessdir->business_getStats($business['id'], array(
            'date_from' => date('Y-m-d H:i:s', strtotime('-7 Days')),
            'date_to'   => date('Y-m-d H:i:s')
        ));

        ?>

        <div id="top_stats" class="top_stats contain">
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_timeframe['view'], 0); ?></span>
                    <span class="title">View<?php if ($stats_timeframe['view'] != 1) { ?>s<?php } ?></span>
                    <span class="period">7 Days</span>
                </div>
            </div>
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_total['view'], 0); ?></span>
                    <span class="title">View<?php if ($stats_total['view'] != 1) { ?>s<?php } ?></span>
                    <span class="period">Total</span>
                </div>
            </div>
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_timeframe['email'], 0); ?></span>
                    <span class="title">Email<?php if ($stats_timeframe['email'] != 1) { ?>s<?php } ?></span>
                    <span class="period">7 Days</span>
                </div>
            </div>
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_total['email'], 0); ?></span>
                    <span class="title">Email<?php if ($stats_total['email'] != 1) { ?>s<?php } ?></span>
                    <span class="period">Total</span>
                </div>
            </div>
        </div>

        <?php

    }

    ?>

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

    <div class="container legacy">

        <header>
            <h2><?php if ($business['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Business</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left">

                    <fieldset>
                        <label>Business Name <span class="required">*</span></label>
                        <input type="text" value="<?=$business['name']?>" name="business[name]" />
                        <?php echo business_fieldChanged('name'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Department</label>
                        <input type="text" value="<?=$business['department']?>" name="business[department]" />
                        <?php echo business_fieldChanged('department'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Owner Name</label>
                        <input type="text" value="<?=$business['name_owner']?>" name="business[name_owner]" />
                        <?php echo business_fieldChanged('name_owner'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Contact Name</label>
                        <input type="text" value="<?=$business['name_contact']?>" name="business[name_contact]" />
                        <?php echo business_fieldChanged('name_contact'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Address <span class="required">*</span></label>
                        <input type="text" value="<?=$business['address1']?>" name="business[address1]" />
                        <?php echo business_fieldChanged('address1'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Address Cont.</label>
                        <input type="text" value="<?=$business['address2']?>" name="business[address2]" />
                        <?php echo business_fieldChanged('address2'); ?>
                    </fieldset>

                    <? /*
                    <fieldset>
                        <label>City <span class="required">*</span></label>
                        <input type="text" value="<?=$business['city']?>" name="business[city]" />
                        <?php echo business_fieldChanged('city'); ?>
                    </fieldset>
                    */ ?>

                    <fieldset>
                        <label>City</label>
                        <select name="business[city_id]">
                            <option value="0">None</option>
                            <?php foreach ($citiesa as $city) { ?>
                                <option value="<?php echo $city['id']; ?>" <?php if (($city['id'] == $business['city_id']) || (stripslashes($city['title']) == stripslashes($business['city']))) { ?>selected="selected"<?php } ?>><?php echo stripslashes($city['title']); ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset id="neighborhoods">
                        <label>Neighborhood</label>
                        <?php foreach ($neighborhoodsa as $city_id => $neighborhoods) { ?>
                            <div class="city city-<?php echo $city_id; ?>" <?php if ($city_id != $business['city_id']) { ?>style="display: none;"<?php } ?>>
                                <select name="business[neighborhood][<?php echo $city_id; ?>][neighborhood_id]">
                                    <option value="0">None</option>
                                    <?php foreach ($neighborhoods as $neighborhood) { ?>
                                        <option value="<?php echo $neighborhood['id']; ?>" <?php if ($neighborhood['id'] == $business['neighborhood_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($neighborhood['title']); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                    </fieldset>

                    <fieldset>
                        <label>State <span class="required">*</span></label>
                        <select name="business[state]">
                            <option value="">Select</option>
                            <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                <option value="<?=$state_code?>" <?php if ($state_code == $business['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                            <?php } ?>
                        </select>
                        <?php echo business_fieldChanged('state'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Zip</label>
                        <input type="text" value="<?=$business['zip']?>" name="business[zip]" style="display: inline; width: 50px;" /><span class="required">*</span> - <input type="text" value="<?=$business['zip4']?>" name="business[zip4]" style="display: inline; width: 40px;" />
                        <?php echo business_fieldChanged(array('zip','zip4'), ' - '); ?>
                    </fieldset>

                    <fieldset>
                        <label>Phone <span class="required">*</span></label>
                        <input type="text" value="<?=$business['phone']?>" name="business[phone]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$business['phone_ext']?>" name="business[phone_ext]" style="display: inline-block; width: 30px;" />
                        <?php echo business_fieldChanged(array('phone','phone_ext'), ' Ext: '); ?>
                    </fieldset>

                    <fieldset>
                        <label>Toll-Free Number</label>
                        <input type="text" value="<?=$business['phone_tollfree']?>" name="business[phone_tollfree]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$business['phone_tollfree_ext']?>" name="business[phone_tollfree_ext]" style="display: inline-block; width: 30px;" />
                        <?php echo business_fieldChanged(array('phone_tollfree','phone_tollfree_ext'), ' Ext: '); ?>
                    </fieldset>

                    <fieldset>
                        <label>Alternate Phone Number</label>
                        <input type="text" value="<?=$business['phone_alt']?>" name="business[phone_alt]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$business['phone_alt_ext']?>" name="business[phone_alt_ext]" style="display: inline-block; width: 30px;" />
                        <?php echo business_fieldChanged(array('phone_alt','phone_alt_ext'), ' Ext: '); ?>
                    </fieldset>

                    <fieldset>
                        <label>Mobile</label>
                        <input type="text" value="<?=$business['phone_mobile']?>" name="business[phone_mobile]" style="display: inline-block; width: 100px;" />
                        <?php echo business_fieldChanged('mobile'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Fax</label>
                        <input type="text" value="<?=$business['fax']?>" name="business[fax]" style="display: inline-block; width: 100px;" />
                        <?php echo business_fieldChanged('fax'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Email</label>
                        <input type="text" value="<?=$business['email']?>" name="business[email]" />
                        <?php echo business_fieldChanged('email'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Private Email</label>
                        <input type="text" value="<?=$business['email_private']?>" name="business[email_private]" />
                        <?php echo business_fieldChanged('email_private'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Lead Email</label>
                        <input type="text" value="<?=$business['email_lead']?>" name="business[email_lead]" />
                        <?php echo business_fieldChanged('email_lead'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Website URL</label>
                        <input type="text" value="<?=$business['url']?>" name="business[url]" placeholder="www.domain.com" />
                        <?php echo business_fieldChanged('url'); ?>
                    </fieldset>

                </div>

                <div class="right">

                    <div style="margin-bottom: 15px; padding: 20px; background-color: #f5f5f5;">

                        <fieldset>
                            <label>Status <span class="required">*</span></label>
                            <select name="business[status]">
                                <option value="submitted" <?php if ($business['submitted']) { ?>selected="selected"<?php } ?>>Submitted</option>
                                <?php foreach ($_uccms_businessdir->statuses as $status_id => $status_title) { ?>
                                    <?php
                                    if ($status_id == 5) {
                                        if (!$business['pending']) {
                                            continue;
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $status_id; ?>" <?php if ($status_id == $business['status']) { ?>selected="selected"<?php } ?>><?php echo $status_title; ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>

                        <fieldset>
                            <label>Account</label>
                            <select name="business[account_id]">
                                <option value="0">None</option>
                                <?php foreach ($accta as $account_id => $account) { ?>
                                    <option value="<?php echo $account_id; ?>" <?php if ($account_id == $business['account_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($account['fullname']). ' (' .stripslashes($account['email']). ')'; ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>

                    </div>

                    <?php

                    // HAVE E-COMMERCE EXTENSION
                    if (class_exists('uccms_Ecommerce')) {

                        // GET BUSINESS'S E-COMMERCE ITEMS
                        $beia = $_uccms_businessdir->business_getEcommerceItems($business['id']);

                        // ECOMMERCE ITEM ARRAY
                        $eia = array();

                        // GET ALL E-COMMERCE ITEMS
                        $eitem_query = "SELECT `id`, `title` FROM `uccms_ecommerce_items` ORDER BY `title` ASC";
                        $eitem_q = sqlquery($eitem_query);
                        while ($eitem = sqlfetch($eitem_q)) {
                            $eia[$eitem['id']] = $eitem;
                        }

                        ?>

                        <style type="text/css">

                            #ecommerce_items {
                                margin-bottom: 15px;
                                padding: 20px;
                                background-color: #f5f5f5;
                            }
                            #ecommerce_items .add .button {
                                height: 30px;
                                margin-left: 6px;
                                line-height: 30px;
                            }
                            #ecommerce_items .items {
                                margin: 10px 0 0;
                            }
                            #ecommerce_items .items .item {
                                margin-bottom: 10px;
                                padding: 5px 10px 5px 2px;
                                border: 1px solid #ccc;
                                background-color: #fff;
                            }
                            #ecommerce_items .items .item:last-child {
                                margin-bottom: 0px;
                            }
                            #ecommerce_items .items .item .sort {
                                float: left;
                            }
                            #ecommerce_items .items .item .sort .icon_sort {
                                margin: 0px;
                            }
                            #ecommerce_items .items .item .title {
                                float: left;
                                margin-left: 5px;
                                line-height: 24px;
                            }
                            #ecommerce_items .items .item .remove {
                                float: right;
                                font-size: 1.2em;
                                line-height: 24px;
                            }

                            #ecommerce_items .items .item.template {
                                display: none;
                            }

                        </style>

                        <script type="text/javascript">

                            $(document).ready(function() {

                                // SORTABLE ECOMMERCE ITEMS
                                ecommerceItemsSort();

                                // ADD ECOMMERCE ITEM
                                $('#ecommerce_items .add .button').click(function(e) {
                                    e.preventDefault();
                                    var eitem_id = $('#ecommerce_items .add select').val();
                                    if (eitem_id) {
                                        var clone = $('#ecommerce_items .items .item.template').clone();
                                        clone.removeClass('template');
                                        clone.find('input[type="hidden"]').val(eitem_id);
                                        clone.find('.title').text($('#ecommerce_items .add select option:selected').text());
                                        clone.appendTo('#ecommerce_items .items').show();
                                        ecommerceItemsSort();
                                        $('#ecommerce_items .items').show();
                                    }
                                });

                                // REMOVE ECOMMERCE ITEM
                                $('#ecommerce_items .items').on('click', '.remove a', function(e) {
                                    e.preventDefault();
                                    var parent = $(this).closest('.item');
                                    parent.fadeOut(400, function() {
                                        parent.remove();
                                    });
                                });

                            });

                            // SORTABLE ECOMMERCE ITEMS
                            function ecommerceItemsSort() {

                                // SORTABLE
                                $('#ecommerce_items .items').sortable({
                                    handle: '.icon_sort',
                                    axis: 'y',
                                    containment: 'parent',
                                    items: '.item',
                                    //placeholder: 'ui-sortable-placeholder',
                                    update: function() {
                                    }
                                });

                            }

                        </script>

                        <div id="ecommerce_items">

                            <fieldset>
                                <div class="contain">
                                    <div class="left inner_quarter last">
                                        <label>E-Commerce Items</label>
                                    </div>
                                </div>
                                <div class="add">
                                    <select>
                                        <option value="">Select</option>
                                        <?php foreach ($eia as $eitem) { ?>
                                            <option value="<?php echo $eitem['id']; ?>"><?php echo stripslashes($eitem['title']); ?></option>
                                        <?php } ?>
                                    </select> <a href="#" class="button">Add</a>
                                </div>
                                <div class="items" style="<?php if (count($beia) == 0) { echo 'display: none; '; } ?>">

                                    <div class="item template clearfix">
                                        <input type="hidden" name="business[ecommerce][item_id][]" value="" />
                                        <div class="sort"><span class="icon_sort ui-sortable-handle"></span></div>
                                        <div class="title"></div>
                                        <div class="remove"><a href="#" title="Remove"><i class="fa fa-times"></i></a></div>
                                    </div>

                                    <?php foreach ($beia as $beitem) { ?>
                                        <div class="item clearfix">
                                            <input type="hidden" name="business[ecommerce][item_id][]" value="<?php echo $beitem['ecommerce_item_id']; ?>" />
                                            <div class="sort"><span class="icon_sort ui-sortable-handle"></span></div>
                                            <div class="title"><?php echo stripslashes($eia[$beitem['ecommerce_item_id']]['title']); ?></div>
                                            <div class="remove"><a href="#" title="Remove"><i class="fa fa-times"></i></a></div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </fieldset>

                        </div>

                        <?php

                    }

                    ?>

                    <fieldset>
                        <label>Listing Slug</label>
                        /<input type="text" value="<?=$business['slug']?>" name="business[slug]" placeholder="business-name" style="display: inline-block; width: 400px;" />/
                        <label style="margin-bottom: 0px;"><small>The URL this listing is available at on this site.</small></label>
                    </fieldset>

                    <fieldset>
                        <input type="checkbox" name="business[not_recent]" value="1" <?php if ($business['not_recent']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Exclude from recent businesses list</label>
                    </fieldset>

                    <fieldset>
                        <label>Logo</label>
                        <div>
                            <?php
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'logo', // The value you should use for the "name" attribute of your form field
                                'value'     => ($business['logo'] ? $_uccms_businessdir->imageBridgeOut($business['logo'], 'businesses') : ''), // The existing value for this form field
                                'id'        => 'blogo', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </div>
                        <?php echo business_fieldChanged('logo'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Description</label>
                        <textarea name="business[description]" style="height: 60px;"><?=$business['description']?></textarea>
                        <?php echo business_fieldChanged('description'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Tagline</label>
                        <input type="text" value="<?=$business['tagline']?>" name="business[tagline]" />
                        <?php echo business_fieldChanged('tagline'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Keywords <small>Separate by comma.</small></label>
                        <textarea name="business[keywords]" style="height: 60px;"><?=$business['keywords']?></textarea>
                        <?php echo business_fieldChanged('keywords'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Year Opened</label>
                        <select name="business[year_opened]">
                            <option value="">N/A</option>
                            <?php for ($i=date('Y'); $i>=1700; $i--) { ?>
                                <option value="<?=$i?>" <?php if ($i == $business['year_opened']) { ?>selected="selected"<?php } ?>><?=$i?></option>
                            <?php } ?>
                        </select>
                        <?php echo business_fieldChanged('year_opened'); ?>
                    </fieldset>

                    <fieldset>
                        <label style="padding-bottom: 8px;">Payment Methods Accepted</label>
                        <div class="clearfix">
                            <?php foreach ($_uccms_businessdir->paymentMethods() as $pmid => $name) { ?>
                                <div style="float: left; width: 50%; padding-bottom: 8px;">
                                    <input type="checkbox" name="business[payment_methods][<?=$pmid?>]" value="<?=$pmid?>" <?php if (in_array($pmid, $pma)) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                                </div>
                            <?php } ?>
                        </div>
                        <?php echo business_fieldChanged('payment_methods'); ?>
                    </fieldset>

                    <fieldset>
                        <label style="padding-bottom: 8px;">Languages Spoken</label>
                        <div class="clearfix">
                            <?php foreach ($_uccms_businessdir->languages() as $lid => $name) { ?>
                                <div style="float: left; width: 50%; padding-bottom: 8px;">
                                    <input type="checkbox" name="business[languages][<?=$lid?>]" value="<?=$lid?>" <?php if (in_array($lid, $la)) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                                </div>
                            <?php } ?>
                        </div>
                        <?php echo business_fieldChanged('languages'); ?>
                    </fieldset>

                    <fieldset>
                        <label>Booking Widget Embed Code</label>
                        <textarea name="business[booking_widget_code]" style="height: 45px;"><?=$business['booking_widget_code']?></textarea>
                        <label style="margin-bottom: 0px;"><small>Code for embedded booking widget.</small></label>
                    </fieldset>

                </div>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
            <?php if ($business['pending']) { ?><input class="blue" type="submit" name="approve" value="Save & Approve" /><?php } ?>
            <a class="button back" href="../">&laquo; Back</a>
        </footer>

    </div>

    <div class="container legacy">

        <header>
            <h2>Social</h2>
        </header>

        <section>

            <div class="contain">

                <fieldset>
                    <label>Facebook URL</label>
                    <input type="text" value="<?=$business['social_facebook']?>" name="business[social][facebook]" placeholder="www.facebook.com/pagename" />
                    <?php echo business_fieldChanged('social_facebook'); ?>
                </fieldset>

                <fieldset>
                    <label>Foursquare URL</label>
                    <input type="text" value="<?=$business['social_foursquare']?>" name="business[social][foursquare]" placeholder="www.foursquare.com/v/name/value" />
                    <?php echo business_fieldChanged('social_foursquare'); ?>
                </fieldset>

                <fieldset>
                    <label>Google+ URL</label>
                    <input type="text" value="<?=$business['social_googleplus']?>" name="business[social][googleplus]" placeholder="plus.google.com/+name" />
                    <?php echo business_fieldChanged('social_googleplus'); ?>
                </fieldset>

                <fieldset>
                    <label>Instagram URL</label>
                    <input type="text" value="<?=$business['social_instagram']?>" name="business[social][instagram]" placeholder="www.instagram.com/username" />
                    <?php echo business_fieldChanged('social_instagram'); ?>
                </fieldset>

                <fieldset>
                    <label>LinkedIn URL</label>
                    <input type="text" value="<?=$business['social_linkedin']?>" name="business[social][linkedin]" placeholder="www.linkedin.com/company/companyname" />
                    <?php echo business_fieldChanged('social_linkedin'); ?>
                </fieldset>

                <fieldset>
                    <label>Pinterest URL</label>
                    <input type="text" value="<?=$business['social_pinterest']?>" name="business[social][pinterest]" placeholder="www.pinterest.com/username" />
                    <?php echo business_fieldChanged('social_pinterest'); ?>
                </fieldset>

                <fieldset>
                    <label>TripAdvisor URL</label>
                    <input type="text" value="<?=$business['social_tripadvisor']?>" name="business[social][tripadvisor]" placeholder="www.tripadvisor.com/URL" />
                    <?php echo business_fieldChanged('social_tripadvisor'); ?>
                </fieldset>

                <fieldset>
                    <label>Twitter URL</label>
                    <input type="text" value="<?=$business['social_twitter']?>" name="business[social][twitter]" placeholder="www.twitter.com/username" />
                    <?php echo business_fieldChanged('social_twitter'); ?>
                </fieldset>

                <fieldset>
                    <label>Yelp URL</label>
                    <input type="text" value="<?=$business['social_yelp']?>" name="business[social][yelp]" placeholder="www.yelp.com/biz/name" />
                    <?php echo business_fieldChanged('social_yelp'); ?>
                </fieldset>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>

    </div>

    </form>

</div>


<?php if ($business['id']) { ?>

    <style type="text/css">

        #images .add_container, #videos .add_container {
            padding: 10px;
            background-color: #fafafa;
            border-bottom: 1px solid #ccc;
        }

        #images .add_container td, #videos .add_container td {
            vertical-align: top;
        }

        #images.table section a, #categories.table section a, #videos.table section a, #videos.table section a {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #images.table > ul li, #videos.table > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }

        #images.table > ul li section, #videos.table > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }

        #images.table > ul li section:first-child, #videos.table > ul li section:first-child {
            padding-left: 0px;
        }

        #images .items .item .image, #videos .items .item .video {
            width: 460px;
            padding: 0 25px;
        }
        #images .items .item .title, #videos .items .item .title {
            width: 370px;
        }

        #images.table .currently .remove_resource, #videos.table .currently .remove_resource {
            margin-top: -3px;
            margin-bottom: -10px;
        }

        #images .items .item .title fieldset label, #videos .items .item .title fieldset label {
            margin-bottom: 2px;
            text-align: left;
        }

        #categories.table > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }

        #categories.table > ul li li {
            padding-bottom: 0px;
            padding-left: 20px;
            border: 0px none;
            background-color: transparent;
        }

        #categories.table > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }

        #categories.table > ul li section:first-child {
            padding-left: 0px;
        }

        #categories ul .checkbox {
            margin-top: -1px;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function() {

            // IMAGES - SORTABLE
            $('#images ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('id').replace('row_', ''));
                    });
                    $('#images input[name="sort"]').val(sort.join());
                }
            });

            // IMAGES - ADD CLICK
            $('#images .add_resource').click(function(e) {
                e.preventDefault();
                $('#images .add_container').toggle();
            });

            // IMAGES - REMOVE CLICK
            $('#images .icon_delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var id = $(this).attr('data-id');
                    var del = $('#images input[name="delete"]').val();
                    if (del) del += ',';
                    del += id;
                    $('#images input[name="delete"]').val(del);
                    $('#images li#row_' +id).fadeOut(500);
                }
            });

            // VIDEOS - SORTABLE
            $('#videos ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('id').replace('row_', ''));
                    });
                    $('#videos input[name="sort"]').val(sort.join());
                }
            });

            // VIDEOS - ADD CLICK
            $('#videos .add_resource').click(function(e) {
                e.preventDefault();
                $('#videos .add_container').toggle();
            });

            // VIDEOS - REMOVE CLICK
            $('#videos .icon_delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var id = $(this).attr('data-id');
                    var del = $('#videos input[name="delete"]').val();
                    if (del) del += ',';
                    del += id;
                    $('#videos input[name="delete"]').val(del);
                    $('#videos li#row_' +id).fadeOut(500);
                }
            });

            // LOCALEZE CATEGORY SELECT
            $('#categories_localeze select.category').change(function() {
                this_getSubCategories($(this).attr('data-what'), $(this).find('option:selected').text());
            });

            // LOCALEZE CATEGORY - ADD ATTRIBUTE
            $('#categories_localeze .sub_select').on('click', '.add', function(e) {
                e.preventDefault();
                var what = $(this).attr('data-what');
                var id = $(this).prev('select').val();
                var selected = $(this).prev('select').find('option:selected');
                var name = selected.text();
                var group_id = selected.closest('optgroup').attr('data-id');
                var group_name = selected.closest('optgroup').attr('label');
                $('#categories_localeze .category.' +what+ '  .subs_selected').append('<div class="item"><input type="checkbox" name="business[attributes][' +what+ '][' +group_id+ '##' +group_name+ '][' +id+ ']" value="' +name+ '" checked="checked" /><label class="for_checkbox">' +group_name+ ' / ' +name.replace(/\-/g, '').trim()+ '</label></div>');
                $('#categories_localeze .category.' +what+ '  .subs_selected').show();
            });

            // HOURS - CLOSED CLICK
            $('#hours .day input.closed').click(function() {
                $(this).closest('.day').find('select').prop('disabled', $(this).prop('checked'));
            });

        });

    </script>

    <div class="section images">

        <a name="images"></a>

        <form enctype="multipart/form-data" action="./images/process/" method="post">
        <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

        <div id="images" class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Images</h2>
                <a class="add_resource add" href="#"><span></span>Add Image</a>
            </summary>

            <div class="add_container" style="display: none;">
                <table style="margin-bottom: 0px; border: 0px;">
                    <tr>
                        <td width="460">
                            <?php
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'new', // The value you should use for the "name" attribute of your form field
                                'value'     => '', // The existing value for this form field
                                'id'        => 'image-new', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </td>
                        <td>
                            <fieldset>
                                <input type="text" name="new-caption" value="" placeholder="Caption / Alt Text" style="width: 300px;" />
                            </fieldset>
                        </td>
                        <td>
                            <input class="blue" type="submit" value="Add" />
                        </td>
                    </tr>
                </table>
            </div>

            <?php

            // GET IMAGES
            $image_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_images']. "` WHERE (`business_id`=" .$business['id']. ") ORDER BY `sort` ASC, `id` ASC";
            $image_q = sqlquery($image_query);

            // NUMBER OF IMAGES
            $num_images = sqlrows($image_q);

            // HAVE IMAGES
            if ($num_images > 0) {

                ?>

                <ul class="items ui-sortable">

                    <?php

                    // LOOP
                    while ($image = sqlfetch($image_q)) {

                        $match_found = false;

                        // LISTING IS PENDING
                        if ($business['pending']) {

                            // LOOK FOR MATCHING ITEM
                            $match_sql = "SELECT * FROM `" .$_uccms_businessdir->tables['business_images']. "` WHERE (`business_id`=" .$master['id']. ") AND (`image`='" .$image['image']. "')";
                            $match_q = sqlquery($match_sql);
                            if (sqlrows($match_q) > 0) {
                                $match_found = true;
                                $match = sqlfetch($match_q);
                            }

                        }

                        ?>

                        <li id="row_<?php echo $image['id']; ?>" class="item contain">
                            <section class="sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="image">
                                <?php

                                $field = array(
                                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                    'key'       => 'image-' .$image['id'], // The value you should use for the "name" attribute of your form field
                                    'value'     => ($image['image'] ? $_uccms_businessdir->imageBridgeOut($image['image'], 'businesses') : ''), // The existing value for this form field
                                    'id'        => 'image-' .$image['id'], // A unique ID you can assign to your form field for use in JavaScript
                                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                    'options'   => array(
                                        'image' => true
                                    ),
                                    'required'  => false // A boolean value of whether this form field is required or not
                                );
                                include(BigTree::path('admin/form-field-types/draw/upload.php'));

                                // LISTING IS PENDING & NO MATCH FOUND
                                if (($business['pending']) && (!$match_found)) {
                                    ?>
                                    <div class="new_changed">
                                        <a href="?id=<?php echo $business['pending']; ?>#images" target="_blank">New / Changed</a>
                                    </div>
                                    <?php
                                }

                                ?>
                            </section>
                            <section class="title">
                                <fieldset>
                                    <input type="text" name="image-<?php echo $image['id']; ?>-caption" value="<?php echo stripslashes($image['caption']); ?>" placeholder="Caption / Alt Text" style="width: 300px;" />
                                    <?php
                                    if ($match_found) {
                                        echo extra_fieldChanged($image['caption'], $match['caption']);
                                    }
                                    ?>
                                </fieldset>
                            </section>
                            <section class="view_action">
                                <a class="icon_delete" href="#" data-id="<?php echo $image['id']; ?>"></a>
                            </section>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <footer>
                    <input class="blue" type="submit" value="Save" />
                </footer>

                <?php

            } else {

                ?>

                <div style="padding: 10px; text-align: center;">
                    No images added yet.
                </div>

                <?php

            }

            ?>

        </div>

        </form>

    </div>

    <div class="section videos">

        <a name="videos"></a>

        <form enctype="multipart/form-data" action="./videos/process/" method="post">
        <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

        <div id="videos" class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Videos</h2>
                <a class="add_resource add" href="#"><span></span>Add Video</a>
            </summary>

            <div class="add_container" style="display: none;">
                <table style="margin-bottom: 0px; border: 0px;">
                    <tr>
                        <td width="460">
                            <input id="video-new" type="text" name="video[new][url]" value="" placeholder="YouTube / Vimeo URL" style="width: 400px;" />
                        </td>
                        <td>
                            <fieldset>
                                <input type="text" name="video[new][caption]" value="" placeholder="Title" style="width: 300px;" />
                            </fieldset>
                        </td>
                        <td>
                            <input class="blue" type="submit" value="Add" />
                        </td>
                    </tr>
                </table>
            </div>

            <?php

            // GET VIDEOS
            $video_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_videos']. "` WHERE (`business_id`=" .$business['id']. ") ORDER BY `sort` ASC, `id` ASC";
            $video_q = sqlquery($video_query);

            // NUMBER OF VIDEOS
            $num_videos = sqlrows($video_q);

            // HAVE VIDEOS
            if ($num_videos > 0) {

                ?>

                <ul class="items ui-sortable">

                    <?php

                    // LOOP
                    while ($video = sqlfetch($video_q)) {

                        // GET VIDEO INFO
                        $vi = videoInfo(stripslashes($video['video']));

                        $match_found = false;

                        // LISTING IS PENDING
                        if ($business['pending']) {

                            // LOOK FOR MATCHING ITEM
                            $match_sql = "SELECT * FROM `" .$_uccms_businessdir->tables['business_videos']. "` WHERE (`business_id`=" .$master['id']. ") AND (`video`='" .$video['video']. "')";
                            $match_q = sqlquery($match_sql);
                            if (sqlrows($match_q) > 0) {
                                $match_found = true;
                                $match = sqlfetch($match_q);
                            }

                        }

                        ?>

                        <li id="row_<?php echo $video['id']; ?>" class="item contain">
                            <section class="sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="video">
                                <input type="text" name="video[<?php echo $video['id']; ?>][url]" value="<?php echo stripslashes($video['video']); ?>" placeholder="YouTube / Vimeo URL" style="width: 400px;" />
                            </section>
                            <section class="title">
                                <fieldset>
                                    <input type="text" name="video[<?php echo $video['id']; ?>][caption]" value="<?php echo stripslashes($video['caption']); ?>" placeholder="Title" style="width: 300px;" />
                                    <?php
                                    if ($match_found) {
                                        echo extra_fieldChanged($video['caption'], $video['caption']);
                                    }
                                    ?>
                                </fieldset>
                            </section>
                            <section class="view_action">
                                <a class="icon_delete" href="#" data-id="<?php echo $video['id']; ?>"></a>
                            </section>
                            <?php if (($vi['thumb']) || (($business['pending']) && (!$match_found))) { ?>
                                <div style="clear: both; padding: 20px 0 0 42px;">
                                    <?php if ($vi['thumb']) { ?>
                                        <a href="<?php echo $video['video']; ?>" target="_blank"><img src="<?php echo $vi['thumb']; ?>" alt="" style="max-width: 150px;" /></a>
                                    <?php } ?>
                                    <?php if (($business['pending']) && (!$match_found)) { ?>
                                        <div class="new_changed">
                                            <a href="?id=<?php echo $business['pending']; ?>#videos" target="_blank">New / Changed</a>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <footer>
                    <input class="blue" type="submit" value="Save" />
                </footer>

                <?php

            } else {

                ?>

                <div style="padding: 10px; text-align: center;">
                    No videos added yet.
                </div>

                <?php

            }

            ?>

        </div>

        </form>

    </div>

    <a name="categories"></a>

    <?php

    // ARRAY OF CATEGORIES
    $cata = array();

    // GET CURRENT RELATIONS
    $query = "SELECT `category_id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$id. ")";
    $q = sqlquery($query);
    while ($row = sqlfetch($q)) {
        $cata[$row['category_id']] = $row['category_id'];
    }

    // LISTING IS PENDING
    if ($business['pending']) {

        $cats_changed = false;

        // GET ORIGINAL RELATIONS
        $query = "SELECT `category_id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['pending']. ")";
        $q = sqlquery($query);
        while ($row = sqlfetch($q)) {
            $ocata[$row['category_id']] = $row['category_id'];
            if (!in_array($row['category_id'], $cata)) {
                $cats_changed = true;
            }
        }

    }

    ?>

    <div class="section categories">

        <form enctype="multipart/form-data" action="./categories/process/" method="post">
        <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

        <div id="categories" class="table">

            <summary>
                <h2>Categories</h2>
                <?php if ($cats_changed) { ?>
                    <div class="new_changed" style="clear: none; float: right; margin: 0px;">
                        <a href="?id=<?php echo $business['pending']; ?>#categories" target="_blank">Changed</a>
                    </div>
                <?php } ?>
            </summary>

            <?php

            // OUTPUT LIST
            function this_listElements($array) {
                global $cata;
                if (count($array) > 0) {
                    echo '<ul>';
                    foreach ($array as $element) {
                        if ($cata[$element['id']]) {
                            $checked = 'checked="checked"';
                        } else {
                            $checked = '';
                        }
                        echo '<li><input type="checkbox" name="category[' .$element['id']. ']" value="1" ' .$checked. ' /> ' .stripslashes($element['title']);
                        if (!empty($element['children'])) {
                            this_listElements($element['children']);
                        }
                        echo '</li>';
                    }
                    echo '</ul>';
                }
            }

            // BUILD LIST OFF ALL CATEGORIES
            echo this_listElements($_uccms_businessdir->getCategoryTree());

            ?>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

        </div>

        </form>

    </div>

    <?php

    // FEATURE ARRAY
    $fa = array();

    // EXISTING FEATURE ARRAY
    $efa = array();

    // EXISTING GROUP ARRAY
    $ega = array();

    // GET ITEM FEATURES
    $bf_query = "
    SELECT bf.id AS `rel_id`, f.id, f.title, f.active, 'feature' AS `what`, bf.sort
    FROM `" .$_uccms_businessdir->tables['business_features']. "` AS `bf`
    INNER JOIN `" .$_uccms_businessdir->tables['features']. "` AS `f` ON bf.feature_id=f.id
    WHERE (bf.business_id=" .$business['id']. ") AND (bf.feature_id!=0)
    ORDER BY bf.sort ASC, f.sort ASC, f.title ASC, bf.id ASC
    ";
    $bf_q = sqlquery($bf_query);
    if (sqlrows($bf_q) > 0) {
        while ($bf = sqlfetch($bf_q)) {
            $fa[$bf['rel_id']] = $bf;
            $efa[$bf['id']] = $bf['id'];
        }
    }

    // GET BUSINESS FEATURE GROUPS
    $bfg_query = "
    SELECT bf.id AS `rel_id`, fg.id, fg.title, fg.active, 'group' AS `what`, bf.sort
    FROM `" .$_uccms_businessdir->tables['business_features']. "` AS `bf`
    INNER JOIN `" .$_uccms_businessdir->tables['feature_groups']. "` AS `fg` ON bf.group_id=fg.id
    WHERE (bf.business_id=" .$business['id']. ") AND (bf.group_id!=0)
    ORDER BY bf.sort ASC, fg.sort ASC, fg.title ASC, bf.id ASC
    ";
    $bfg_q = sqlquery($bfg_query);
    if (sqlrows($bfg_q) > 0) {
        while ($bfg = sqlfetch($bfg_q)) {
            $fa[$bfg['rel_id']] = $bfg;
            $ega[$bfg['id']] = $bfg['id'];
        }
    }

    // SORT FEATURE ARRAY
    usort($fa, function($a, $b) {
        return $a['sort'] - $b['sort'];
    });

    ?>

    <style type="text/css">

        #manage-features .add_container {
            padding: 6px 10px;
            background-color: #fafafa;
            border-bottom: 1px solid #ccc;
        }

        #manage-features .feature_sort {
            width: 55px;
        }
        #manage-features .feature_title {
            width: 730px;
            text-align: left;
        }
        #manage-features .feature_status {
            width: 100px;
        }
        #manage-features .feature_delete {
            width: 55px;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // FEATURES - HIDE GROUP SELECT
            $('#select_feature-group').hide();

            // FEATURES - ADD CLICK
            $('#manage-features .add_resource').click(function(e) {
                e.preventDefault();
                $('#manage-features .add_container').toggle();
            });

            // NEW FEATURE - WHAT CHANGE
            $('#select_feature-what').change(function(e) {
                var val = $(this).val();
                $('#manage-features .whats .option').hide();
                if (val == 'custom') {
                    window.location.href = './features/edit/?item_id=<?php echo $business['id']; ?>';
                } else {
                    $('#manage-features .whats #select_feature-' +val).show();
                }
            });

            // FEATURES - SORTABLE
            $('#manage-features ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('data-id'));
                    });
                    $('#manage-features input[name="sort"]').val(sort.join());
                }
            });

            // FEATURES - REMOVE CLICK
            $('#manage-features .items .item .icon_delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to remove this?')) {
                    var id = $(this).attr('data-id');
                    var del = $('#manage-features input[name="delete"]').val();
                    if (del) del += ',';
                    del += id;
                    $('#manage-features input[name="delete"]').val(del);
                    $('#manage-features li[data-id=' +id+ ']').fadeOut(500);
                }
            });

        });

    </script>

    <div class="section manage-features">

        <a name="manage-features"></a>

        <form id="form_mange-features" enctype="multipart/form-data" action="./features/manage-process/" method="post">
        <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

        <div id="manage-features" class="table" style="margin-bottom: 0px;">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Manage Features</h2>
                <a class="add_resource add" href="#"><span></span>Add Feature</a>
            </summary>

            <div class="add_container" style="display: none;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: auto; margin-bottom: 0px; border: 0px;">
                    <tr>
                        <td>
                            Add:&nbsp;
                        </td>
                        <td>
                            <select name="new_feature-what" id="select_feature-what">
                                <option value="feature">Feature</option>
                                <option value="group">Feature Group</option>
                                <? /*<option value="custom">Custom Feature</option>*/ ?>
                            </select>
                        </td>
                        <td class="whats" style="padding-left: 15px;">

                            <?php

                            // AVAILABLE FEATURES ARRAY
                            $faa = array();

                            // GET ALL FEATURES
                            $a_query = "SELECT `id`, `title` FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`business_id`=0) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                            $a_q = sqlquery($a_query);
                            while ($a = sqlfetch($a_q)) {
                                if (!in_array($a['id'], $efa)) {
                                    $faa[$a['id']] = $a;
                                }
                            }

                            ?>

                            <div id="select_feature-feature" class="option">
                                <?php if (count($faa) > 0) { ?>
                                    <select name="new_feature">
                                        <?php foreach ($faa as $a) { ?>
                                            <option value="<?php echo $a['id']; ?>"><?php echo stripslashes($a['title']); ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } else { ?>
                                    No available features.
                                <?php } ?>
                            </div>

                            <?php

                            // AVAILABLE GROUPS ARRAY
                            $aga = array();

                            // GET ALL GROUPS
                            $ag_query = "SELECT `id`, `title` FROM `" .$_uccms_businessdir->tables['feature_groups']. "` ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                            $ag_q = sqlquery($ag_query);
                            while ($ag = sqlfetch($ag_q)) {
                                if (!in_array($ag['id'], $ega)) {
                                    $aga[$ag['id']] = $ag;
                                }
                            }

                            ?>

                            <div id="select_feature-group" class="option">
                                <?php if (count($aga) > 0) { ?>
                                    <select name="new_feature-group">
                                        <?php foreach ($aga as $ag) { ?>
                                            <option value="<?php echo $ag['id']; ?>"><?php echo stripslashes($ag['title']); ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } else { ?>
                                    No available groups.
                                <?php } ?>
                            </div>

                        </td>
                        <td>
                            <input class="blue" type="submit" name="do" value="Add" />
                        </td>
                    </tr>
                </table>

            </div>

            <header style="clear: both;">
                <span class="feature_sort"></span>
                <span class="feature_title">Title</span>
                <span class="feature_status">Status</span>
                <span class="feature_delete">Remove</span>
            </header>

            <?php

            // HAVE FEATURES
            if (count($fa) > 0) {

                ?>

                <ul class="items ui-sortable">

                    <?php

                    // LOOP
                    foreach ($fa as $feature) {
                        ?>

                        <li class="item contain" data-id="<?php echo $feature['rel_id']; ?>">
                            <section class="feature_sort sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="feature_title">
                                <?php if ($feature['what'] == 'group') { ?>Group:&nbsp;<?php } ?><?php echo stripslashes($feature['title']); ?>
                            </section>
                            <section class="feature_status status_<?php if ($feature['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                                <?php if ($feature['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                            </section>
                            <section class="feature_delete">
                                <a href="#" class="icon_delete" title="Remove feature" data-id="<?php echo $feature['rel_id']; ?>"></a>
                            </section>
                        </li>

                        <?php
                    }

                    ?>

                </ul>

                <footer style="clear: both;">
                    <input class="blue" type="submit" value="Save" />
                </footer>

                <?php

            // NO FEATURES
            } else {
                ?>
                <div style="padding: 15px; text-align: center;">
                    No features added yet.
                </div>
                <?php
            }

            ?>

        </div>

        </form>

    </div>

    <?php

    // GET BUSINESS FEATURES
    $feata = $_uccms_businessdir->business_features($business['id']);

    //echo print_r($feata);
    //exit;

    // HAVE FEATURE INFO
    if (count($feata) > 0) {

        ?>

        <style type="text/css">

            #features .add_container {
                padding: 6px 10px;
                background-color: #fafafa;
                border-bottom: 1px solid #ccc;
            }

            #features .feature_sort {
                width: 55px;
            }
            #features .feature_title {
                width: 730px;
                text-align: left;
            }
            #features .feature_status {
                width: 100px;
            }
            #features .feature_delete {
                width: 55px;
            }

            #features .options .option {
                margin-bottom: 10px;
            }
            #features .options .option:last-child {
                margin-bottom: 0px;
            }
            #features .options .option .checkbox {
                margin-top: -3px;
            }

        </style>

        <? /*
        <script type="text/javascript">

            $(document).ready(function() {

            });

        </script>
        */ ?>

        <div class="section">

            <a name="features"></a>

            <form id="form_features" enctype="multipart/form-data" action="./features/process/" method="post">
            <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

            <div id="features" class="table">

                <summary>
                    <h2>Features</h2>
                </summary>

                <div id="business_features" style="padding: 15px;">
                    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/features/edit/main.php'); ?>
                </div>

                <footer style="clear: both;">
                    <input class="blue" type="submit" value="Save" />
                </footer>

            </div>

            </form>

        </div>

        <?php

    }

    ?>

    <? /*
    // if localeze is enabled

    <a name="localeze-categories"></a>

    <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

        <form enctype="multipart/form-data" action="./localeze/categories/" method="post">
        <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

        <div id="localeze_categories" class="section localeze_categories">

            <h4>Categories</h4>

            <div style="padding: 20px; background-color: #fff; border-radius: 5px;">

                <?php

                $catha = array(
                    0 => array(
                        'name'  => 'primary',
                        'title' => 'Primary Category'
                    ),
                    1 => array(
                        'name'  => 'alt1',
                        'title' => 'Alternate Category 1'
                    ),
                    2 => array(
                        'name'  => 'alt2',
                        'title' => 'Alternate Category 2'
                    )
                );

                foreach ($catha as $cath_id => $cath) {

                    // HAVE KEYWORDS
                    if ((is_array($business['categories'][$cath['name']]['Attributes'])) && (count($business['categories'][$cath['name']]['Attributes']) > 0)) {
                        $have_keywords = true;
                    } else {
                        $have_keywords = false;
                    }

                    ?>

                    <div class="category <?=$cath['name']?> contain" style="padding: 15px; background-color: #fff;">

                        <h3><?=$cath['title']?><?php if ($cath_id == 0) { ?> <span class="required">*</span><?php } ?></h3>

                        <select name="business[categories][<?=$cath['name']?>]" class="custom_control category" data-what="<?=$cath['name']?>">
                            <option value=""><?php if ($cath['name'] == 'primary') { echo 'Required'; } else { echo 'Select'; } ?></option>
                            <?php foreach ($cata as $cat_id => $cat_title) { ?>
                                <option value="<?=$cat_id?>##<?=$cat_title?>" <?php if (strtoupper($cat_title) == strtoupper($business['categories'][$cath['name']]['Name'])) { echo 'selected="selected"'; } ?>><?=ucwords(strtolower($cat_title))?></option>
                            <?php } ?>
                        </select>

                        <div class="sub_select" style="<?php if (!$business['categories'][$cath['name']]['Name']) { ?>display: none;<?php } ?> padding-top: 15px;">
                            <?php if ($business['categories'][$cath['name']]['Name']) { ?>
                                Loading..
                                <script type="text/javascript">
                                    $(function() {
                                        this_getSubCategories('<?=$cath['name']?>', '<?=$business['categories'][$cath['name']]['Name']?>');
                                    });
                                </script>
                            <?php } ?>
                        </div>

                        <div class="subs_selected" style="<?php if (!$have_keywords) { ?>display: none;<?php } ?> padding-top: 15px;">

                            <?php
                            if ($have_keywords) {
                                unset($keywords);
                                if ($business['categories'][$cath['name']]['Attributes']['Attribute'][0]['ID']) {
                                    $keywords = $business['categories'][$cath['name']]['Attributes']['Attribute'];
                                } else {
                                    $keywords[] = $business['categories'][$cath['name']]['Attributes']['Attribute'];
                                }
                                foreach ($keywords as $keyword) {
                                    ?>
                                    <div class="item"><input type="checkbox" name="business[attributes][<?=$cath['name']?>][<?=$keyword['Group']['ID']?>##<?=$keyword['Group']['Name']?>][<?=$keyword['ID']?>]" value="<?=$keyword['Name']?>" checked="checked" /><label class="for_checkbox"><?=$keyword['Group']['Name']?> / <?=$keyword['Name']?></label></div>
                                    <?php
                                }
                                unset($keywords);
                            }
                            ?>

                        </div>

                    </div>

                    <?php

                }

                ?>

            </div>

        </div>

        </form>

    </div>
    */ ?>

    <a name="hours"></a>

    <?php

    $hours_changed = false;

    // LISTING IS PENDING
    if ($business['pending']) {

        // HAVE HOURS
        if (count($business['hours']) > 0) {

            // LOOP THROUGH DAYS
            foreach ($business['hours'] as $day => $hours) {

                // LOOK FOR MATCH
                $orig_hours_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$business['pending']. ") AND (`day`=" .$day. ")";
                $orig_hours_q = sqlquery($orig_hours_query);
                $orig_hours = sqlfetch($orig_hours_q);

                if ($hours['open'] != $orig_hours['open']) {
                    $hours_changed = true;
                }

                if ($hours['close'] != $orig_hours['close']) {
                    $hours_changed = true;
                }

                if ($hours['closed'] != $orig_hours['closed']) {
                    $hours_changed = true;
                }

            }

        }

    }

    ?>

    <div class="section hours">

        <form enctype="multipart/form-data" action="./hours/process/" method="post">
        <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

        <div id="hours" class="table">

            <summary>
                <h2>Hours</h2>
                <?php if ($hours_changed) { ?>
                    <div class="new_changed" style="clear: none; float: right; margin: 0px;">
                        <a href="?id=<?php echo $business['pending']; ?>#hours" target="_blank">Changed</a>
                    </div>
                <?php } ?>
            </summary>

            <div class="clearfix">

                <table style="margin: 0px; border: 0px;">
                    <?php

                    // LOOP THROUGH DAYS OF WEEK
                    foreach ($_uccms_businessdir->daysOfWeek() as $day_code => $day_name) {

                        // THIS DAY
                        $this_day = $business['hours'][$day_code];

                        // CLOSED OR NOT
                        $closed = $this_day['closed'];

                        ?>
                        <tr class="day">
                            <td style="white-space: nowrap; font-weight: bold;"><?=$day_name?></td>
                            <td style="white-space: nowrap;">
                                <select name="business[hours][<?=$day_code?>][open]" <?php if ($closed) { ?>disabled="disabled"<?php } ?>class="custom_control">
                                    <?php
                                    for ($i=1; $i<=24; $i++) {
                                        for ($j=0; $j<=45; $j+=15) {
                                            if ($closed) {
                                                $selected = false;
                                            } else {
                                                if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['open']) {
                                                    $selected = true;
                                                } else {
                                                    $selected = false;
                                                }
                                            }
                                            ?>
                                            <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                &nbsp;-&nbsp;
                                <select name="business[hours][<?=$day_code?>][close]" <?php if ($closed) { ?>disabled="disabled"<?php } ?> class="custom_control">
                                    <?php
                                    for ($i=1; $i<=24; $i++) {
                                        for ($j=0; $j<=45; $j+=15) {
                                            if ($closed) {
                                                $selected = false;
                                            } else {
                                                if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['close']) {
                                                    $selected = true;
                                                } else {
                                                    $selected = false;
                                                }
                                            }
                                            ?>
                                            <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td style="width: 100%; padding-top: 3px;"><input type="checkbox" name="business[hours][<?=$day_code?>][closed]" value="1" <?php if ($closed) { ?>checked="checked"<?php } ?>class="closed" /> Closed</td>
                        </tr>
                    <?php } ?>
                </table>

            </div>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

        </div>

        </form>

    </div>

    <?php if (!$business['pending']) { ?>

        <script src="/js/lib/ratings-reviews/jquery.barrating.min.js"></script>

        <style type="text/css">

            .uccms_ratings-reviews .pages.top {
                display: block;
                margin-bottom: 10px;
                text-align: right;
            }

            .uccms_ratings-reviews .results .items .item .criteria .select, .uccms_ratings-reviews .results .items .item .stars .select {
                display: none;
            }
            .uccms_ratings-reviews .results .items .item .criteria label, .uccms_ratings-reviews .results .items .item .stars label {
                display: inline-block;
            }

        </style>

        <div id="reviews" class="section reviews">

            <a name="reviews"></a>

            <div class="contain">

                <header>
                    <h2>Reviews</h2>
                </header>

                <?php

                // ENABLED
                if ($_uccms_businessdir->getSetting('business_ratings_reviews_enabled')) {

                    // RATINGS / REVIEWS
                    include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
                    if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

                    // VARS FOR REVIEWS
                    $reviews_vars = array(
                        'source'    => $_uccms_businessdir->Extension,
                        'what'      => 'business',
                        'item_id'   => $business['id']
                    );

                    // IS OWNER
                    $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'reply', true);
                    $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'status', true);
                    $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'delete', true);

                    ?>

                    <section>
                        <div class="contain">
                            <div class="section reviews uccms_ratings-reviews">
                                <div class="results">
                                    <?php include(SERVER_ROOT. 'templates/ajax/ratings-reviews/paged-results-admin.php'); ?>
                                </div>
                            </div>
                        </div>
                    </section>

                    <?php

                } else {
                    ?>
                    <div style="padding: 15px; text-align: center;">
                        Ratings / reviews not enabled.
                    </div>
                    <?php
                }

                ?>

            </div>

        </div>

    <?php } ?>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>