<?php

//echo print_r($_POST);
//exit;

// CLEAN UP
$id = (int)$_POST['business']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE BUSINESS ID
    if ($id) {

        // GET BUSINESS FEATURES
        $feata = $_uccms_businessdir->business_features($id);

        // HAVE FEATURES
        if (count($_POST['feature']) > 0) {

            // LOOP
            foreach ($_POST['feature'] as $feature_id => $options) {

                // CLEAN UP
                //$feature_id = (int)$feature_id;

                // HAS VALUE RECORD
                if ($feata[$feature_id]['value']['id']) {

                    $columns = array(
                        'option_id'     => 0,
                        'value'         => implode(',', $options)
                    );

                    // UPDATE VALUE
                    $query = "UPDATE `" .$_uccms_businessdir->tables['business_feature_values']. "` SET " .$_uccms_businessdir->createSet($columns). " WHERE (`id`=" .$feata[$feature_id]['value']['id']. ")";

                // NO VALUE RECORD
                } else {

                    $columns = array(
                        'business_id'   => $id,
                        'feature_id'    => (int)$feature_id,
                        'option_id'     => 0,
                        'value'         => implode(',', $options)
                    );

                    // RECORD VALUE
                    $query = "INSERT INTO `" .$_uccms_businessdir->tables['business_feature_values']. "` SET " .$_uccms_businessdir->createSet($columns);

                }

                //echo $query. '<br />';

                // QUERY SUCCESSFUL
                if (sqlquery($query)) {
                    unset($feata[$feature_id]);
                }

            }

        }

        // HAVE LEFTOVER FEATURES
        if (count($feata) > 0) {

            // LOOP
            foreach ($feata as $feature) {

                // DELETE FEATURE VALUE
                $delete_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_feature_values']. "` WHERE (`business_id`=" .$id. ") AND (`feature_id`=" .$feature['id']. ")";
                sqlquery($delete_query);

            }

        }

        $admin->growl('Business', 'Features updated.');

    // BUSINESS ID NOT SPECIFIED
    } else {
        $admin->growl('Business', 'Business ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'businesses/edit/?id=' .$id. '#features');

?>