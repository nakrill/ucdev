<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['business']['id'];

    // USE BUSINESS TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['business']['slug']) {
        $_POST['business']['slug'] = $_POST['business']['name'];
    }

    $city_id = (int)$_POST['business']['city_id'];

    // HAVE CITY
    if ($city_id) {

        // GET CITY INFO
        $city_query = "SELECT * FROM `" .$_uccms_businessdir->tables['cities']. "` WHERE (`id`=" .$city_id. ")";
        $city_q = sqlquery($city_query);
        $city = sqlfetch($city_q);

        if ($city['title']) {
            $_POST['business']['city'] = stripslashes($city['title']);
        }

    }

    if ($_POST['business']['year_opened']) {
        $year_opened = date('Y', strtotime('01/01/' .$_POST['business']['year_opened']));
    } else {
        $year_opened = '0000';
    }

    // DB COLUMNS
    $columns = array(
        'slug'                      => $_uccms_businessdir->makeRewrite($_POST['business']['slug']),
        'status'                    => (int)$_POST['business']['status'],
        'account_id'                => (int)$_POST['business']['account_id'],
        'not_recent'                => (int)$_POST['business']['not_recent'],
        'city_id'                   => $city_id,
        'neighborhood_id'           => (int)$_POST['business']['neighborhood'][$_POST['business']['city_id']]['neighborhood_id'],
        'name'                      => $_POST['business']['name'],
        'department'                => $_POST['business']['department'],
        'name_owner'                => $_POST['business']['name_owner'],
        'name_contact'              => $_POST['business']['name_contact'],
        'phone'                     => preg_replace('/\D/', '', $_POST['business']['phone']),
        'phone_ext'                 => $_POST['business']['phone_ext'],
        'phone_tollfree'            => preg_replace('/\D/', '', $_POST['business']['phone_tollfree']),
        'phone_tollfree_ext'        => $_POST['business']['phone_tollfree_ext'],
        'phone_alt'                 => preg_replace('/\D/', '', $_POST['business']['phone_alt']),
        'phone_alt_ext'             => $_POST['business']['phone_alt_ext'],
        'phone_mobile'              => preg_replace('/\D/', '', $_POST['business']['phone_mobile']),
        'fax'                       => preg_replace('/\D/', '', $_POST['business']['fax']),
        'address1'                  => $_POST['business']['address1'],
        'address2'                  => $_POST['business']['address2'],
        'city'                      => $_POST['business']['city'],
        'state'                     => $_POST['business']['state'],
        'zip'                       => preg_replace('/\D/', '', $_POST['business']['zip']),
        'zip4'                      => preg_replace('/\D/', '', $_POST['business']['zip4']),
        'url'                       => $_uccms_businessdir->trimURL($_POST['business']['url']),
        'email'                     => $_POST['business']['email'],
        'email_private'             => $_POST['business']['email_private'],
        'email_lead'                => $_POST['business']['email_lead'],
        'year_opened'               => $year_opened,
        'payment_methods'           => $_uccms_businessdir->arrayToCSV($_POST['business']['payment_methods']),
        'languages'                 => $_uccms_businessdir->arrayToCSV($_POST['business']['languages']),
        'description'               => $_POST['business']['description'],
        'tagline'                   => $_POST['business']['tagline'],
        'keywords'                  => $_POST['business']['keywords'],
        'social_facebook'           => $_uccms_businessdir->trimURL($_POST['business']['social']['facebook']),
        'social_foursquare'         => $_uccms_businessdir->trimURL($_POST['business']['social']['foursquare']),
        'social_googleplus'         => $_uccms_businessdir->trimURL($_POST['business']['social']['googleplus']),
        'social_instagram'          => $_uccms_businessdir->trimURL($_POST['business']['social']['instagram']),
        'social_linkedin'           => $_uccms_businessdir->trimURL($_POST['business']['social']['linkedin']),
        'social_pinterest'          => $_uccms_businessdir->trimURL($_POST['business']['social']['pinterest']),
        'social_tripadvisor'        => $_uccms_businessdir->trimURL($_POST['business']['social']['tripadvisor']),
        'social_twitter'            => $_uccms_businessdir->trimURL($_POST['business']['social']['twitter']),
        'social_yelp'               => $_uccms_businessdir->trimURL($_POST['business']['social']['yelp']),
        'booking_widget_code'       => $_POST['business']['booking_widget_code']
    );

    // NOT INACTIVE - NOT SUBMITTED
    if ($_POST['business']['status'] != 0) {
        $columns['submitted'] = 0;
    }

    // HAVE BUSINESS ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO ITEM ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `created_dt`=NOW(), `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Business', 'Business added!');
        } else {
            $admin->growl('Business', 'Business updated!');
        }

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['logo']['name']) || ((!$_POST['logo']) || (substr($_POST['logo'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `logo` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$id. ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['logo']) {

                // REMOVE IMAGE
                @unlink($_uccms_businessdir->imageBridgeOut($ex['logo'], 'businesses', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET `logo`='' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['logo']['name']) || ($_POST['logo'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'Business Logo',
                'key'           => 'image',
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_businessdir->Extension. '/files/businesses',
                    'image' => true,
                    'thumbs' => array(
                        array(
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES['logo']['name']) {
                $field['file_input'] = $_FILES['logo'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['logo']) {
                $field['input'] = $_POST['logo'];
            }

            // DIGITAL ASSET MANAGER VARS
            $field['dam_vars'] = array(
                'website_extension'         => $_uccms_businessdir->Extension,
                'website_extension_item'    => 'business',
                'website_extension_item_id' => $id,
                'title'                     => $_POST['business']['name'],
                'tags'                      => array('logo')
            );

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET `logo`='" .sqlescape($_uccms_businessdir->imageBridgeIn($file_path)). "' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

            }

        }

        // GET BUSINESS'S EXISTING E-COMMERCE ITEMS
        $beia = $_uccms_businessdir->business_getEcommerceItems($id);

        // LEFTOVER BUSINESS E-COMMERCE ITEMS
        $leia = $beia;

        // HAVE SPECIFIED
        if (count($_POST['business']['ecommerce']['item_id']) > 0) {

            $si = 0;

            // LOOP
            foreach ($_POST['business']['ecommerce']['item_id'] as $item_id) {

                $item_id = (int)$item_id;

                // HAVE ITEM ID
                if ($item_id) {

                    // BUSINESS ALREADY HAS ITEM
                    if ($beia[$item_id]) {

                        $ucolumns = array(
                            'sort'  => $si
                        );

                        // UPDATE
                        $update_query = "UPDATE `" .$_uccms_businessdir->tables['business_ecommerce_items']. "` SET " .uccms_createSet($ucolumns). " WHERE (`id`=" .$beia[$item_id]['id']. ")";
                        sqlquery($update_query);

                        unset($leia[$item_id]);

                    // NEW ITEM FOR BUSINESS
                    } else {

                        $ncolumns = array(
                            'business_id'       => $id,
                            'ecommerce_item_id' => $item_id,
                            'sort'              => $si
                        );

                        // ADD TO DB
                        $new_query = "INSERT INTO `" .$_uccms_businessdir->tables['business_ecommerce_items']. "` SET " .uccms_createSet($ncolumns). "";
                        sqlquery($new_query);

                    }

                    $si++;

                }

            }

        }

        // HAVE LEFTOVERS
        if (count($leia) > 0) {

            // LOOP
            foreach ($leia as $beitem) {

                // DELETE FROM DB
                $delete_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_ecommerce_items']. "` WHERE (`id`=" .$beitem['id']. ")";
                sqlquery($delete_query);

            }

        }

        unset($beia, $leia);

        // IF APPROVING
        if ($_POST['approve']) {

            // GET BUSINESS
            $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$id. ")";
            $business_q = sqlquery($business_query);
            $business = sqlfetch($business_q);

            // BUSINESS FOUND
            if ($business['id']) {

                $parent = $business['pending'];

                unset($business['id']);
                unset($business['pending']);
                unset($business['status']);
                unset($business['updated_dt']);

                // UPDATE MASTER BUSINESS
                $business_query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($business). ", `updated_dt`=NOW() WHERE (`id`=" .$parent. ")";
                if (sqlquery($business_query)) {

                    $admin->growl('Business', 'Saved and approved.');

                    // DELETE PENDING
                    $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$id. ")";
                    sqlquery($del_query);

                    ##############################
                    # CATEGORIES
                    ##############################

                    // GET PENDING CATEGORIES
                    $cats_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$id. ")";
                    $cats_q = sqlquery($cats_query);

                    // HAVE PENDING CATEGORIES
                    if (sqlrows($cats_q) > 0) {

                        // DELETE MASTER CATEGORIES
                        $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$parent. ")";
                        sqlquery($del_query);

                        // UPDATE PENDING TO MASTER
                        $update_query = "UPDATE `" .$_uccms_businessdir->tables['business_categories']. "` SET `business_id`=" .$parent. " WHERE (`business_id`=" .$id. ")";
                        sqlquery($update_query);

                    }

                    ##############################
                    # FEATURES
                    ##############################

                    // GET PENDING FEATURE RELATIONS
                    $features_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_features']. "` WHERE (`business_id`=" .$id. ")";
                    $features_q = sqlquery($features_query);

                    // HAVE PENDING FEATURE RELATIONS
                    if (sqlrows($features_q) > 0) {

                        // DELETE MASTER FEATURES
                        $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_features']. "` WHERE (`business_id`=" .$parent. ")";
                        sqlquery($del_query);

                        // UPDATE PENDING TO MASTER
                        $update_query = "UPDATE `" .$_uccms_businessdir->tables['business_features']. "` SET `business_id`=" .$parent. " WHERE (`business_id`=" .$id. ")";
                        sqlquery($update_query);

                    }

                    // GET PENDING FEATURE VALUES
                    $featurev_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_feature_values']. "` WHERE (`business_id`=" .$id. ")";
                    $featurev_q = sqlquery($featurev_query);

                    // HAVE PENDING FEATURE VALUES
                    if (sqlrows($featurev_q) > 0) {

                        // DELETE MASTER FEATURES
                        $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_feature_values']. "` WHERE (`business_id`=" .$parent. ")";
                        sqlquery($del_query);

                        // UPDATE PENDING TO MASTER
                        $update_query = "UPDATE `" .$_uccms_businessdir->tables['business_feature_values']. "` SET `business_id`=" .$parent. " WHERE (`business_id`=" .$id. ")";
                        sqlquery($update_query);

                    }

                    ##############################
                    # HOURS
                    ##############################

                    // LOOP THROUGH EACH WEEKDAY
                    for ($i=1; $i<=7; $i++) {

                        // GET PENDING HOURS FOR DAY
                        $hours_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$id. ") AND (`day`=" .$i. ")";
                        $hours_q = sqlquery($hours_query);

                        // HAVE PENDING HOURS
                        if (sqlrows($hours_q) > 0) {

                            $hours = sqlfetch($hours_q);

                            // DELETE MASTER RECORD FOR DAY
                            $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$parent. ") AND (`day`=" .$i. ")";
                            sqlquery($del_query);

                            // UPDATE PENDING TO MASTER FOR DAY
                            $update_query = "UPDATE `" .$_uccms_businessdir->tables['business_hours']. "` SET `business_id`=" .$parent. " WHERE (`id`=" .$hours['id']. ")";
                            sqlquery($update_query);

                        }

                    }

                    ##############################
                    # IMAGES
                    ##############################

                    // DELETE OLD
                    $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_images']. "` WHERE (`business_id`=" .$parent. ")";
                    sqlquery($del_query);

                    // UPDATE PENDING TO MASTER
                    $update_query = "UPDATE `" .$_uccms_businessdir->tables['business_images']. "` SET `business_id`=" .$parent. " WHERE (`business_id`=" .$id. ")";
                    sqlquery($update_query);

                    ##############################
                    # VIDEOS
                    ##############################

                    // DELETE OLD
                    $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_videos']. "` WHERE (`business_id`=" .$parent. ")";
                    sqlquery($del_query);

                    // UPDATE PENDING TO MASTER
                    $update_query = "UPDATE `" .$_uccms_businessdir->tables['business_videos']. "` SET `business_id`=" .$parent. " WHERE (`business_id`=" .$id. ")";
                    sqlquery($update_query);

                    ##############################

                    $id = $parent;

                }

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Business', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT. 'businesses/edit/?id=' .$id);

?>