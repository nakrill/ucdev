<?php

//echo print_r($_POST);
//exit;

// CLEAN UP
$id = (int)$_POST['business']['id'];

// HAVE BUSINESS ID
if ($id) {

    // HAVE HOURS
    if (is_array($_POST['business']['hours'])) {

        // LOOP
        foreach ($_POST['business']['hours'] as $day => $hours) {

            // DB COLUMNS
            $columns = array(
                'day'       => (int)$day,
                'open'      => date('H:i', strtotime($hours['open'])),
                'close'     => date('H:i', strtotime($hours['close'])),
                'closed'    => (int)$hours['closed']
            );

            // SEE IF RECORD EXISTS
            $hc_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$id. ") AND (`day`=" .(int)$day. ")";
            $hc_q = sqlquery($hc_query);
            $hc = sqlfetch($hc_q);

            // RECORD EXISTS
            if ($hc['id']) {

                $query = "UPDATE `" .$_uccms_businessdir->tables['business_hours']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$hc['id']. ")";

            // NO RECORD
            } else {

                $columns['business_id'] = $id;

                $query = "INSERT INTO `" .$_uccms_businessdir->tables['business_hours']. "` SET " .uccms_createSet($columns). "";

            }

            sqlquery($query);

            unset($columns);

        }

        $admin->growl('Business', 'Hours updated.');

    // NO HOURS
    } else {
        $admin->growl('Business', 'Business hours not specified.');
    }

// NO BUSINESS ID
} else {
    $admin->growl('Business', 'Business ID not specified.');
}

BigTree::redirect(MODULE_ROOT. 'businesses/edit/?id=' .$id. '#hours');

?>