<style type="text/css">

    #features .feature_sort {
        width: 40px;
    }
    #features .feature_title {
        width: 490px;
        text-align: left;
    }
    #features .feature_type {
        width: 120px;
        text-align: left;
    }
    #features .feature_required {
        width: 80px;
    }
    #features .feature_status {
        width: 100px;
    }
    #features .feature_edit {
        width: 55px;
    }
    #features .feature_delete {
        width: 55px;
    }

    #groups .group_title {
        width: 650px;
    }
    #groups .group_features {
        width: 80px;
    }
    #groups .group_status {
        width: 100px;
    }
    #groups .group_edit {
        width: 55px;
    }
    #groups .group_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // FEATURES - SORTABLE
        $('#features ul.ui-sortable').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'li',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
                $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_businessdir->Extension?>/ajax/admin/businesses/features/save-sort/', $('#form_features').serialize(), function(data) {
                    console.log(data);
                });
            }
        });

    });

</script>

<form id="form_features" action="" method="post">

<div id="features" class="table">

    <summary>
        <h2>Features</h2>
        <a class="add_resource add" href="./edit/"><span></span>Add Feature</a>
    </summary>
    <header style="clear: both;">
        <span class="feature_sort"></span>
        <span class="feature_title">Title</span>
        <span class="feature_type">Type</span>
        <span class="feature_required">Required</span>
        <span class="feature_status">Status</span>
        <span class="feature_edit">Edit</span>
        <span class="feature_delete">Delete</span>
    </header>

    <?php

    // GET FEATURES
    $feature_query = "SELECT * FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`business_id`=0) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
    $feature_q = sqlquery($feature_query);

    // HAVE FEATURES
    if (sqlrows($feature_q)) {

        ?>

        <ul class="items ui-sortable">

            <?php

            // LOOP
            while ($feature = sqlfetch($feature_q)) {

                if ($feature['type'] == 'dt') {
                    $type = 'Date & Time';
                } else {
                    $type = ucwords(str_replace('_', ' ', stripslashes($feature['type'])));
                }

                ?>

                <li class="item">
                    <section class="feature_sort sort">
                        <input type="hidden" name="sort[]" value="<?php echo $feature['id']; ?>" />
                        <span class="icon_sort ui-sortable-handle"></span>
                    </section>
                    <section class="feature_title">
                        <a href="./edit/?id=<?php echo $feature['id']; ?>"><?php echo stripslashes($feature['title']); ?></a>
                    </section>
                    <section class="feature_type">
                        <?php echo $type; ?>
                    </section>
                    <section class="feature_required">
                        <?php if ($feature['required']) { echo 'Yes'; } ?>
                    </section>
                    <section class="feature_status status_<?php if ($feature['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                        <?php if ($feature['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                    </section>
                    <section class="feature_edit">
                        <a class="icon_edit" title="Edit feature" href="./edit/?id=<?php echo $feature['id']; ?>"></a>
                    </section>
                    <section class="feature_delete">
                        <a href="./delete/?id=<?php echo $feature['id']; ?>" class="icon_delete" title="Delete feature" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                    </section>
                </li>

                <?php

            }

            ?>

            </ul>

        <?php

    // NO FEATURES
    } else {
        ?>
        <div style="padding: 10px; text-align: center;">
            No features created.
        </div>
        <?php
    }

    ?>

</div>

</form>

<div id="groups" class="table">
    <summary>
        <h2>Groups</h2>
        <a class="add_resource add" href="./group/"><span></span>Add Group</a>
    </summary>
    <header style="clear: both;">
        <span class="group_title">Title</span>
        <span class="group_features">Features</span>
        <span class="group_status">Status</span>
        <span class="group_edit">Edit</span>
        <span class="group_delete">Delete</span>
    </header>

    <?php

    // GET GROUPS
    $group_query = "SELECT * FROM `" .$_uccms_businessdir->tables['feature_groups']. "` ORDER BY `sort`, `title` ASC, `id` ASC";
    $group_q = sqlquery($group_query);

    if (sqlrows($group_q) > 0) {

        ?>

        <ul class="items">

            <?php

            // LOOP
            while ($group = sqlfetch($group_q)) {

                // GET NUMBER OF featureS
                $num_attr = count(explode(',', stripslashes($group['features'])));

                ?>

                <li class="item">
                    <section class="group_title">
                        <a href="./group/?id=<?php echo $group['id']; ?>"><?php echo stripslashes($group['title']); ?></a>
                    </section>
                    <section class="group_features">
                        <?php echo number_format($num_attr, 0); ?>
                    </section>
                    <section class="group_status status_<?php if ($group['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                        <?php if ($group['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                    </section>
                    <section class="group_edit">
                        <a class="icon_edit" title="Edit Group" href="./group/?id=<?php echo $group['id']; ?>"></a>
                    </section>
                    <section class="group_delete">
                        <a href="./group/delete/?id=<?php echo $group['id']; ?>" class="icon_delete" title="Delete Group" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?\nfeatures in this group will not be deleted.');"></a>
                    </section>
                </li>

                <?php

            }

            ?>

        </ul>

        <?php

    // NO FEATURES
    } else {
        ?>
        <div style="padding: 10px; text-align: center;">
            No groups created.
        </div>
        <?php
    }

    ?>

</div>