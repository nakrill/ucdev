<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_businessdir->tables['business_features']. "` WHERE (`feature_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Feature', 'Feature deleted.');

        // DELETE BUSINESS RELATIONS
        $delete_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_features']. "` WHERE (`feature_id`=" .$id. ")";
        sqlquery($delete_query);

        // DELETE FEATURE OPTIONS
        $delete_query = "DELETE FROM `" .$_uccms_businessdir->tables['feature_options']. "` WHERE (`feature_id`=" .$id. ")";
        sqlquery($delete_query);

        // DELETE BUSINESS VALUES
        $delete_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_feature_values']. "` WHERE (`feature_id`=" .$id. ")";
        sqlquery($delete_query);


    // QUERY FAILED
    } else {
        $admin->growl('Delete Feature', 'Failed to delete feature.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Feature', 'No feature specified.');
}

BigTree::redirect(MODULE_ROOT.'businesses/features/');

?>