<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['group']['id'];

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['group']['active'],
        'title'         => $_POST['group']['title'],
        'features'      => implode(',', array_keys($_POST['feature'])),
        'updated_by'    => $_uccms_businessdir->adminID()
    );

    // HAVE FEATURE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_businessdir->tables['feature_groups']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO FEATURE ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_businessdir->tables['feature_groups']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Feature Group', 'Saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Feature Group', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'businesses/features/group/?id=' .$id);

?>