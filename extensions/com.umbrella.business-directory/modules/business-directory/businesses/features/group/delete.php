<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_businessdir->tables['business_features']. "` WHERE (`group_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_businessdir->tables['feature_groups']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Feature Group', 'Feature group deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Feature Group', 'Failed to delete feature group.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Feature Group', 'No feature group specified.');
}

BigTree::redirect(MODULE_ROOT.'businesses/features/');

?>