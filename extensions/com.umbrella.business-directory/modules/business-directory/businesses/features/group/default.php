<?php

// GROUP FEATURE ARRAY
$gfa = array();

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $group_query = "SELECT * FROM `" .$_uccms_businessdir->tables['feature_groups']. "` WHERE (`id`=" .$id. ")";
    $group_q = sqlquery($group_query);
    $group = sqlfetch($group_q);

    // GROUP FEATURE ARRAY
    $gfa = explode(',', stripslashes($group['features']));

}

?>


<style type="text/css">

    #features .contain .contain {
        padding-bottom: 10px;
    }

    #features .contain .contain:last-child {
        padding-bottom: 0px;
    }

    #features .contain .contain .checkbox {
        margin-top: -2px;
    }

</style>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="group[id]" value="<?php echo $group['id']; ?>" />

<div class="container legacy">

    <header>
        <h2><?php if ($group['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Group</h2>
    </header>

    <section>

        <div class="left">

            <fieldset>
                <label>Title</label>
                <input type="text" name="group[title]" value="<?php echo stripslashes($group['title']); ?>" />
            </fieldset>

            <fieldset>
                <input type="checkbox" name="group[active]" value="1" <?php if ($group['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

        </div>

        <div class="right">

        </div>

        <div id="features" class="contain" style="clear: both;">
            <h3><span>features</span></h3>
            <div class="contain">
                <?php

                // GET FEATURES
                $feature_query = "SELECT `id`, `title` FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`business_id`=0) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                $feature_q = sqlquery($feature_query);

                // LOOP
                while ($feature = sqlfetch($feature_q)) {
                    ?>
                    <div class="contain">
                        <input type="checkbox" name="feature[<?php echo $feature['id']; ?>]" value="1" <?php if (in_array($feature['id'], $gfa)) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($feature['title']); ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

</div>

</form>