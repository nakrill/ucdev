<?php

// CLEAN UP
$id = (int)$_POST['feature']['id'];

// HAVE FEATURE ID
if ($id) {

    // EXISTING OPTIONS
    $eo = $_uccms_businessdir->feature_getOptions($id);

    // HAVE SUBMITTED OPTIONS
    if (count($_POST['option']) > 0) {

        $si = 0;

        // LOOP
        foreach ($_POST['option'] as $option_id => $option) {

            $columns = array(
                'feature_id'    => $id,
                'sort'          => $si,
                'title'         => $option['title'],
                'default'       => ($option_id == $_POST['option_default'] ? 1 : 0)
            );

            // ALREADY IN DB
            if ($eo[$option_id]) {

                // UPDATE
                $query = "UPDATE `" .$_uccms_businessdir->tables['feature_options']. "` SET " .$_uccms_businessdir->createSet($columns). " WHERE (`id`=" .$option_id. ")";

                unset($eo[$option_id]);

            // NEW
            } else {

                // INSERT
                $query = "INSERT INTO `" .$_uccms_businessdir->tables['feature_options']. "` SET " .$_uccms_businessdir->createSet($columns). "";

            }

            sqlquery($query);

            $si++;

        }

    }

    // HAVE LEFTOVERS
    if (count($eo) > 0) {

        // LOOP
        foreach ($eo as $o) {

            // DELETE RECORD
            $delete_query = "DELETE FROM `" .$_uccms_businessdir->tables['feature_options']. "` WHERE (`id`=" .$o['id']. ")";
            if (sqlquery($delete_query)) {

                // DELETE BUSINESS VALUES

            }

        }

    }

    $admin->growl('Feature Options', 'Saved!');

}

BigTree::redirect(MODULE_ROOT.'businesses/features/edit/?id=' .$id. '#options');

?>