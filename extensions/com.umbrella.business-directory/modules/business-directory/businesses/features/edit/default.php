<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $feature_query = "SELECT * FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`id`=" .$id. ")";
    $feature_q = sqlquery($feature_query);
    $feature = sqlfetch($feature_q);

    // FEATURE FOUND
    if ($feature['id']) {

        // EXISTING OPTIONS
        $oa = $_uccms_businessdir->feature_getOptions($feature['id']);

    }

}

// OPTIONS ARRAY
if (!is_array($oa)) $oa = array();

?>

<script type="text/javascript">

    $(document).ready(function() {

        // TYPE SELECT
        $('#select_type').change(function(e) {
            if ($(this).val() == 'checkbox') {
                $('#feature .limit').show();
            } else {
                $('#feature .limit').hide();
            }
            if ($(this).val() == 'file') {
                $('#feature .file_types').show();
            } else {
                $('#feature .file_types').hide();
            }
        });

        // REQUIRED TOGGLE
        $('#required').change(function(e) {
            if ($(this).prop('checked')) {
                if ($('#select_type').val() != 'file') {
                    $('#toggle_required').show();
                }
            } else {
                $('#toggle_required').hide();
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="feature[id]" value="<?php echo $feature['id']; ?>" />

<div id="feature" class="container legacy">

    <header>
        <h2><?php if ($feature['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Feature</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>Title</label>
                <input type="text" name="feature[title]" value="<?php echo stripslashes($feature['title']); ?>" />
            </fieldset>

            <fieldset>
                <label>Description</label>
                <textarea name="feature[description]" style="height: 60px;"><?php echo stripslashes($feature['description']); ?></textarea>
            </fieldset>

        </div>

        <div class="right last">

            <div class="contain">

                <div class="left inner_quarter type">
                    <fieldset>
                        <label>Type</label>
                        <select id="select_type" name="feature[type]">
                            <option value="text" <?php if ($feature['type'] == 'text') { ?>selected="selected"<?php } ?>>Text</option>
                            <option value="textarea" <?php if ($feature['type'] == 'textarea') { ?>selected="selected"<?php } ?>>Textarea</option>
                            <option value="select_one" <?php if ($feature['type'] == 'select_one') { ?>selected="selected"<?php } ?>>Select One</option>
                            <option value="select_multi" <?php if ($feature['type'] == 'select_multi') { ?>selected="selected"<?php } ?>>Select Multiple</option>
                            <? /*
                            <option value="file" <?php if ($feature['type'] == 'file') { ?>selected="selected"<?php } ?>>File</option>
                            <option value="select_box" <?php if ($feature['type'] == 'select_box') { ?>selected="selected"<?php } ?>>Select Box(s)</option>
                            <option value="date" <?php if ($feature['type'] == 'date') { ?>selected="selected"<?php } ?>>Date</option>
                            <option value="time" <?php if ($feature['type'] == 'time') { ?>selected="selected"<?php } ?>>Time</option>
                            <option value="width_height" <?php if ($feature['type'] == 'width_height') { ?>selected="selected"<?php } ?>>Width x Height</option>
                            */ ?>
                        </select>
                    </fieldset>
                </div>

                <div class="right inner_quarter limit" style="<?php if ($feature['type'] != 'checkbox') { ?>display: none;<?php } ?>">
                    <fieldset>
                        <label>Limit</label>
                        <input type="text" name="feature[limit]" value="<?php echo (int)$feature['limit']; ?>" class="smaller_60" />
                        <label><small>The number of options a person may select. 0 for no limit.</small></label>
                    </fieldset>
                </div>

                <div class="right inner_quarter file_types" style="<?php if ($feature['type'] != 'file') { ?>display: none;<?php } ?>">
                    <fieldset>
                        <label>Allowed File Types</label>
                        <input type="text" name="feature[file_types]" value="<?php echo $feature['file_types']; ?>" style="width: 180px;" />
                        <label><small>Separate by comma. Ex: jpg, png, pdf</small></label>
                    </fieldset>
                </div>

            </div>

            <fieldset>
                <input type="checkbox" name="feature[active]" value="1" <?php if ($feature['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

            <fieldset>
                <input id="required" type="checkbox" name="feature[required_checkbox]" value="1" <?php if ($feature['required']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Required</label>
            </fieldset>

            <fieldset id="toggle_required" style="<?php if ((!$feature['required']) || ($feature['type'] == 'file')) { ?>display: none;<?php } ?>">
                <label>Required Type</label>
                <select id="select_type" name="feature[required]">
                    <option value="general" <?php if ($feature['required'] == 'general') { ?>selected="selected"<?php } ?>>General</option>
                    <option value="number" <?php if ($feature['required'] == 'number') { ?>selected="selected"<?php } ?>>Number</option>
                    <? /*
                    <option value="email" <?php if ($feature['required'] == 'email') { ?>selected="selected"<?php } ?>>Email</option>
                    <option value="url" <?php if ($feature['required'] == 'url') { ?>selected="selected"<?php } ?>>URL</option>
                    */ ?>
                </select>
            </fieldset>

            <fieldset>
                <input id="required" type="checkbox" name="feature[search_hidden]" value="1" <?php if ($feature['search_hidden']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Hide from search</label>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

</div>

</form>

<?php

// CAN HAVE OPTIONS
if (($feature['id']) && (($feature['type'] == 'select_one') || ($feature['type'] == 'select_multi'))) {

    ?>

    <style type="text/css">

        #options header {
            clear: both;
        }

        #options.table > ul li {
            height: auto;
            padding: 5px 0;
            line-height: inherit;
        }
        #options.table > ul li section {
            height: auto;
            overflow: visible;
        }

        #options .option_sort {
            width: 55px;
        }
        #options .option_title {
            width: 680px;
            text-align: left;
        }
        /*
        #options .option_markup {
            width: 200px;
            text-align: left;
        }
        */
        #options .option_default {
            width: 150px;
            text-align: left;
        }
        #options .option_delete {
            width: 55px;
        }

        #options .option_default .radio_button {
            margin-top: 3px;
            margin-left: 12px;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            var newOptionKey = 1;

            // OPTIONS - SORTABLE
            $('#options ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                }
            });

            // OPTIONS - ADD
            $('#options .add_resource').click(function(e) {
                e.preventDefault();
                var key = 'new-' +newOptionKey;
                newOptionKey++;
                var el = $('#option-new').clone();
                el.html(el.html().replace(/###/g, key)).removeAttr('id').appendTo('#options ul.ui-sortable').show();
                el.find('.option_default').html('<input type="radio" name="option_default" value="' +key+ '" />');
                el.find("input[type=radio]").get(0).customControl = new BigTreeRadioButton(el.find("input[type=radio]").get(0));
            });

            // OPTIONS - DELETE
            $('#options .items').on('click', '.item .icon_delete', function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    $(this).closest('.item').fadeOut(400, function() {
                        $(this).remove();
                    });
                }
            });

        });

    </script>

    <a name="options"></a>

    <div class="container legacy" style="border: 0px none;">

        <form enctype="multipart/form-data" action="./process-options/" method="post">
        <input type="hidden" name="feature[id]" value="<?php echo $feature['id']; ?>" />

        <div id="options" class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Options</h2>
                <a class="add_resource add" href="#"><span></span>Add Option</a>
            </summary>

            <header>
                <span class="option_sort"></span>
                <span class="option_title">Name</span>
                <span class="option_default">Default</span>
                <span class="option_delete">Delete</span>
            </header>

            <ul class="items ui-sortable">

                <?php

                $i = 1;

                // HAVE OPTIONS
                if (count($oa) > 0) {

                    foreach ($oa as $option) {

                        if ($option['markup']) {
                            number_format($option['markup'], 2);
                        } else {
                            $option['markup'] = '';
                        }

                        ?>

                        <li class="item contain">
                            <section class="option_sort sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="option_title">
                                <input type="text" name="option[<?php echo $option['id']; ?>][title]" value="<?php echo stripslashes($option['title']); ?>" placeholder="Name" style="width: 300px;" />
                            </section>
                            <section class="option_default">
                                <input type="radio" name="option_default" value="<?php echo $option['id']; ?>" data-key="<?php echo $key; ?>" <?php if ($option['default']) { ?>checked="checked"<?php } ?> />
                            </section>
                            <section class="option_delete view_action">
                                <a class="icon_delete" href="#"></a>
                            </section>
                        </li>

                        <?php

                        $i++;

                    }

                }

                ?>

            </ul>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

        </div>

        </form>

        <li id="option-new" class="item contain" style="display: none;">
            <section class="option_sort sort">
                <span class="icon_sort ui-sortable-handle"></span>
            </section>
            <section class="option_title">
                <input type="text" name="option[###][title]" value="" placeholder="Name" style="width: 300px;" />
            </section>
            <section class="option_default">

            </section>
            <section class="option_delete view_action">
                <a class="icon_delete" href="#"></a>
            </section>
        </li>

    </div>

    <?php

}

?>