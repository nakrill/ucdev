<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['feature']['id'];

    // REQUIRED
    if ($_POST['feature']['required_checkbox']) {
        $required = $_POST['feature']['required'];
    } else {
        $required = '';
    }

    // FILE TYPES
    if ($_POST['feature']['file_types']) {
        $ffta = array();
        $tfta = explode(',', $_POST['feature']['file_types']);
        foreach ($tfta as $ft) {
            $ffta[] = trim(str_replace('.', '', $ft));
        }
        if (count($ffta) > 0) {
            $file_types = implode(',', $ffta);
        }
    }

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['feature']['active'],
        'title'         => $_POST['feature']['title'],
        'description'   => $_POST['feature']['description'],
        'type'          => $_POST['feature']['type'],
        'limit'         => (int)$_POST['feature']['limit'],
        'required'      => $required,
        'file_types'    => $file_types,
        'search_hidden' => (int)$_POST['feature']['search_hidden'],
        'updated_by'    => $_uccms_businessdir->adminID()
    );

    // HAVE FEATURE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_businessdir->tables['features']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO FEATURE ID - IS NEW
    } else {

        $columns['sort'] = 999;

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_businessdir->tables['features']. "` SET " .$_uccms_businessdir->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Feature', 'Saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Feature', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'businesses/features/edit/?id=' .$id);

?>