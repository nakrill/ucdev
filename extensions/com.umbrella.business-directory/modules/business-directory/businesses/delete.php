<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE BUSINESS
    $query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `deleted_dt`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Business', 'Business deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Business', 'Failed to delete business.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Business', 'No business specified.');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT.'businesses/');
}

?>