<?php

// BIGTREE
$bigtree['css'][]   = 'master.css';
$bigtree['js'][]    = 'master.js';

// MODULE CLASS
$_uccms_businessdir = new uccms_BusinessDirectory;

// BREADCRUMBS
$bigtree['breadcrumb'] = [
    [ 'title' => 'Business Directory', 'link' => $bigtree['path'][1] ],
];

// MODULE MAIN NAV SELECTED ARRAY
if ($bigtree['path'][2]) {
    $mmnsa[$bigtree['path'][2]] = 'active';
} else {
    $mmnsa['dashboard'] = 'active';
}

?>

<nav class="main">
    <section>
        <ul>
            <li class="<?php echo $mmnsa['dashboard']; ?>">
                <a href="<?=MODULE_ROOT;?>"><span class="dashboard"></span>Dashboard</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>seo/">SEO</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['locations']; ?>">
                <a href="<?=MODULE_ROOT;?>locations/"><span class="developer"></span>Locations</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>locations/">Cities & Neighborhoods</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['categories']; ?>">
                <a href="<?=MODULE_ROOT;?>categories/"><span class="pages"></span>Categories</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>categories/">All</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>categories/?do=add">New Category</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['businesses']; ?>">
                <a href="<?=MODULE_ROOT;?>businesses/"><span class="developer"></span>Businesses</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>businesses/">All</a></li>
                    <li><a href="<?=MODULE_ROOT;?>businesses/?status=active">Active</a></li>
                    <li><a href="<?=MODULE_ROOT;?>businesses/?status=inactive">Inactive</a></li>
                    <li><a href="<?=MODULE_ROOT;?>businesses/?status=submitted">Submitted</a></li>
                    <?php if ($_uccms_businessdir->getSetting('approve_listing_update')) { ?><li><a href="<?=MODULE_ROOT;?>businesses/?status=pending">Pending</a></li><?php } ?>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>businesses/edit/?id=new">New Business</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>businesses/features/">Features</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>businesses/email-log/">Email Logs</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['settings']; ?>">
                <a href="<?=MODULE_ROOT;?>settings/"><span class="settings"></span>Settings</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                    <li><a href="<?=MODULE_ROOT;?>settings/payment/">Payment</a></li>
                    <li><a href="<?=MODULE_ROOT;?>settings/email/">Email</a></li>
                </ul>
            </li>
        </ul>
    </section>
</nav>
