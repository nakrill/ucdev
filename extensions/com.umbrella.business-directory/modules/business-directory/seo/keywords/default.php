<?php

// GET LOGGED SEARCHES WITH KEYWORD(S)
$keyword_query = "SELECT * FROM `" .$_uccms_businessdir->tables['search_log']. "` WHERE (`keywords`!='') ORDER BY `dt` DESC LIMIT 25";
$keyword_q = sqlquery($keyword_query);

?>

<style type="text/css">

    #p_keywords .table .keyword_date {
        width: 150px;
        text-align: left;
    }
    #p_keywords .table .keyword_title {
        width: 600px;
        text-align: left;
    }
    #p_keywords .table .keyword_results {
        width: 100px;
        text-align: left;
    }
    #p_keywords .table .keyword_view {
        width: 70px;
        text-align: right;
    }

    #p_keywords .table li section:first-child {
        padding-left: 15px;
    }


</style>

<div class="uccms_breadcrumbs">
    <a href="../">SEO</a>
    <span class="sep">&rsaquo;</span>
    <a href="./">Searched Keywords</a>
</div>

<div id="p_keywords">

    <?php

    // HAVE KEYWORD SEARCHES
    if (sqlrows($keyword_q) > 0) {

        ?>

        <div class="table">
            <summary>
                <h2>Latest Searched Keyword(s)</h2>
            </summary>
            <header>
                <span class="keyword_date">Date</span>
                <span class="keyword_title">Word / Phrase</span>
                <span class="keyword_results"># Results</span>
                <span class="keyword_view">View</span>
            </header>
            <ul id="keywords">
                <?php while ($keyword = sqlfetch($keyword_q)) { ?>
                    <li class="published">
                        <section class="keyword_date">
                            <?php echo date('n/j/Y G:i A', strtotime($keyword['dt'])); ?>
                        </section>
                        <section class="keyword_title">
                            <?php echo stripslashes($keyword['keywords']); ?>
                        </section>
                        <section class="keyword_results">
                            <?php echo number_format($keyword['num_results'], 0); ?>
                        </section>
                        <section class="keyword_view">
                            <a href="/<?php echo $_uccms_businessdir->frontendPath(); ?>/search/?q=<?php echo stripslashes($keyword['keywords']); ?>" target="_blank" class="icon_restore"></a>
                        </section>
                    </li>
                <?php } ?>
            </ul>

        </div>

        <?php

    // NO KEYWORDS
    } else {
        ?>
        <div class="container legacy" style="padding: 15px;">
            <div style="text-align: center;">No keyword searches found!</div>
        </div>
        <?php
    }

    ?>

</div>