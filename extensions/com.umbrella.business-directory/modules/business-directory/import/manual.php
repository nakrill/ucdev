<?php

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.business-directory/modules/business-directory/import/manual.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INIT BUSINESS DIRECTORY CLASS
$_uccms_businessdir = new uccms_BusinessDirectory;

$row = 0;

// GET DATA
if (($handle = fopen(dirname(__FILE__). '/Import_Template.csv', 'r')) !== FALSE) {

    $cata = array();

    // LOOP THROUGH DATA
    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {

        // IS HEADINGS - SKIP
        if ($row == 0) {
            $row++;
            continue;
        }

        // HAVE DATA
        if (count($data) > 0) {

            // CLEAN UP
            $business_name  = trim(sqlescape($data[0]));
            $business_phone = $_uccms_businessdir->cleanPhone($data[5]);

            // CHECK FOR EXISTING BUSINESS
            $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`name`='" .$business_name. "') AND (`phone`='" .$business_phone. "') ORDER BY `id` DESC LIMIT 1";
            $business_q = sqlquery($business_query);
            $business = sqlfetch($business_q);

            // DB COLUMNS
            $columns = array(
                'name'                      => $data[0],
                'department'                => $data[2],
                'name_owner'                => $data[3],
                'name_contact'              => $data[4],
                'phone'                     => $_uccms_businessdir->cleanPhone($data[5]),
                'phone_ext'                 => $data[6],
                'phone_tollfree'            => $_uccms_businessdir->cleanPhone($data[7]),
                'phone_tollfree_ext'        => $data[8],
                'phone_alt'                 => $_uccms_businessdir->cleanPhone($data[9]),
                'phone_alt_ext'             => $data[10],
                'phone_mobile'              => $_uccms_businessdir->cleanPhone($data[11]),
                'fax'                       => $_uccms_businessdir->cleanPhone($data[12]),
                'address1'                  => $data[13],
                'address2'                  => $data[14],
                'city'                      => $data[15],
                'state'                     => $data[16],
                'zip'                       => $_uccms_businessdir->cleanPhone($data[17]),
                'zip4'                      => $_uccms_businessdir->cleanPhone($data[18]),
                'url'                       => $_uccms_businessdir->trimURL($data[19]),
                'email'                     => $data[20],
                'email_private'             => $data[21],
                'email_lead'                => $data[22],
                'description'               => $data[24],
                'tagline'                   => $data[25],
                'keywords'                  => $data[26],
                'social_facebook'           => $_uccms_businessdir->trimURL($data[27]),
                'social_foursquare'         => $_uccms_businessdir->trimURL($data[28]),
                'social_googleplus'         => $_uccms_businessdir->trimURL($data[29]),
                'social_instagram'          => $_uccms_businessdir->trimURL($data[30]),
                'social_linkedin'           => $_uccms_businessdir->trimURL($data[31]),
                'social_pinterest'          => $_uccms_businessdir->trimURL($data[32]),
                'social_tripadvisor'        => $_uccms_businessdir->trimURL($data[33]),
                'social_twitter'            => $_uccms_businessdir->trimURL($data[34]),
                'social_yelp'               => $_uccms_businessdir->trimURL($data[35])
            );

            // STATUS
            if ($data[1] == 1) {
                $columns['status'] = 1;
            } else if ($data[1] === 0) {
                $columns['status'] = 0;
            }

            // YEAR OPENED
            if ($data[23]) {
                $columns['year_opened'] = date('Y', strtotime('01/01/' .$data[23]));
            }

            /*
            'payment_methods'           => $_uccms_businessdir->arrayToCSV($_POST['business']['payment_methods']), // 37
            'languages'                 => $_uccms_businessdir->arrayToCSV($_POST['business']['languages']), // 38
            'account_id'                => (int)$_POST['business']['account_id'],
            'not_recent'                => (int)$_POST['business']['not_recent'],
            */

            // REMOVE EMPTY COLUMNS
            foreach ($columns as $col_name => $col_val) {
                if ($col_val === 0) {
                    continue;
                } else if ($col_val) {
                    continue;
                } else {
                    unset($columns[$col_name]);
                }
            }

            // HAVE ID - IS UPDATING
            if ($business['id']) {

                // DB QUERY
                $query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$business['id']. ")";

            // NO ID - IS NEW
            } else {

                // SLUG
                $columns['slug'] = $_uccms_businessdir->makeRewrite($data[0]);

                // DB QUERY
                $query = "INSERT INTO `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `created_dt`=NOW(), `updated_dt`=NOW()";

            }

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {

                // UPDATED
                if ($business['id']) {
                    echo $data[0]. ' - Updated';

                // ADDED
                } else {

                    echo $data[0]. ' - Added';

                    // NEW BUSINESS ID
                    $business['id'] = sqlid();

                }

                ###############################
                # CATEGORIES
                ###############################

                $create_relation = false;
                unset($category_del);

                // GET CURRENT RELATIONS
                $rel_query = "SELECT `category_id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ")";
                $rel_q = sqlquery($rel_query);
                while ($rel = sqlfetch($rel_q)) {
                    $category_del[$rel['category_id']] = $rel['category_id']; // SAVE TO DELETE ARRAY
                }

                // CATEGORIES
                $categorya = explode('|', $data[36]);

                // LOOP THROUGH CATEGORIES
                foreach ($categorya as $category_title) {

                    // CLEAN UP
                    $category_title = trim(sqlescape($category_title));

                    // IS A VALUE
                    if ($category_title) {

                        // NOT IN CATEGORY ARRAY
                        if (!$cata[$category_title]) {

                            // LOOK FOR MATCH
                            $category_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`title`='" .$category_title. "')";
                            $category_q = sqlquery($category_query);
                            $category = sqlfetch($category_q);

                            // ADD TO CATEGORY ARRAY
                            $cata[$category_title] = $category['id'];

                        }

                        // CATEGORY FOUND
                        if ($cata[$category_title]) {

                            // RELATION ALREADY EXISTS
                            if ($category_del[$cata[$category_title]]) {
                                unset($category_del[$cata[$category_title]]);

                            // CREATE RELATION
                            } else {
                                $create_relation = true;
                            }

                        // NO CATEGORY FOUND
                        } else {

                            // CREATE NEW CATEGORY
                            $ncat_query = "INSERT INTO `" .$_uccms_businessdir->tables['categories']. "` SET `title`='" .$category_title. "', `slug`='" .$_uccms_businessdir->makeRewrite($category_title). "'";
                            $ncat_q = sqlquery($ncat_query);

                            // NEW CATEGORY ID
                            $cata[$category_title] = sqlid();

                            $create_relation = true;

                        }

                        // CREATE RELATION
                        if (($create_relation) && ($cata[$category_title])) {

                            $rel_query = "INSERT INTO `" .$_uccms_businessdir->tables['business_categories']. "` SET `category_id`=" .$cata[$category_title]. ", `business_id`=" .$business['id'];
                            sqlquery($rel_query);

                        }

                        // HAVE ANY TO DELETE
                        if (count($category_del) > 0) {
                            foreach ($category_del as $category_id) {
                                $del_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ") AND (`category_id`=" .$category_id. ")";
                                sqlquery($del_query);
                            }
                        }

                        unset($category);

                    }

                }

                ###############################

            // QUERY FAILED
            } else {

                echo $data[0]. ' - Failed';

            }

            // CLEAR ARRAYS
            unset($business);
            unset($columns);

        }

        echo ($cli ? "\n" : '<br />');

        $row++;

    }

    // CLOSE DATA
    fclose($handle);

    unset($cata);

}

echo ($cli ? "\n" : '<br /><b>');
echo 'All done.';
echo ($cli ? "\n" : '</b><br />');

?>