<?php

// MODULE CLASS
$_uccms_businessdir = new uccms_BusinessDirectory;

// HAS ACCESS
if ($_uccms_businessdir->adminModulePermission()) {

    ?>

    <style type="text/css">

        #block_businessdir #latest_businesses header span, #block_businessdir #latest_businesses .item section {
            text-align: left;
        }

        #block_businessdir #latest_businesses .business_title {
            width: 300px;
        }
        #block_businessdir #latest_businesses .business_phone {
            width: 150px;
        }
        #block_businessdir #latest_businesses .business_account {
            width: 280px;
        }
        #block_businessdir #latest_businesses .business_status {
            width: 100px;
        }
        #block_businessdir #latest_businesses .business_edit {
            width: 55px;
        }
        #block_businessdir #latest_businesses .business_delete {
            width: 55px;
        }

    </style>

    <div id="block_businessdir" class="card">
        <div class="card-header">
            <a href="../<?php echo $extension; ?>*business-directory/">Latest Businesses</a>
            <a class="btn btn-secondary float-right" href="../<?php echo $extension; ?>*business-dir/businesses/">
                View All
            </a>
        </div>
        <div class="card-body">
            <header style="clear: both;">
                <span class="business_title">Name</span>
                <span class="business_phone">Phone</span>
                <span class="business_account">Account</span>
                <span class="business_status">Status</span>
                <span class="business_edit">Edit</span>
                <span class="business_delete">Delete</span>
            </header>
            <ul id="results" class="items">
                <?php
                $_GET['base_url'] = '../' .$extension. '*business-directory/businesses';
                $_GET['limit'] = 5;
                include($extension_dir. '/ajax/admin/businesses/get-page.php');
                ?>
            </ul>
        </div>
    </div>

    <?php

}

unset($_uccms_businessdir);

?>