<?php

$extension_nav =
["link" => "com.umbrella.business-directory*business-directory", "title" => "Business Directory", "access" => 1, "children" => [
    ["link" => ".", "title" => "Dashboard", "access" => 1],
    ["link" => "locations", "title" => "Locations", "access" => 1],
    ["link" => "categories", "title" => "Categories", "access" => 1],
    ["link" => "businesses", "title" => "Businesses", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>