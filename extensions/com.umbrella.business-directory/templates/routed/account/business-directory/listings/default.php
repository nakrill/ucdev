<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/master/listings.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/custom/listings.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/master/listings.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/custom/listings.js"></script>

<?php

// STATS TOTALS
$stat_total_nvtf = 0;
$stat_total_nvt = 0;
$stat_total_netf = 0;
$stat_total_net = 0;

// STATS - GET ALL BUSINESSES
$business_query = "SELECT b.id FROM `" .$_uccms_businessdir->tables['businesses']. "` AS `b` WHERE (b.account_id=" .$_uccms['_account']->userID(). ") AND (b.pending=0) AND (b.status!=9)";
$business_q = sqlquery($business_query);
while ($business = sqlfetch($business_q)) {

    // GET TOTAL STATS
    $stats_total = $_uccms_businessdir->business_getStats($business['id']);

    $stat_total_nvt += $stats_total['view'];
    $stat_total_net += $stats_total['email'];

    // GET TIMEFRAME STATS
    $stats_timeframe = $_uccms_businessdir->business_getStats($business['id'], array(
        'date_from' => date('Y-m-d H:i:s', strtotime('-7 Days')),
        'date_to'   => date('Y-m-d H:i:s')
    ));

    $stat_total_nvtf += $stats_timeframe['view'];
    $stat_total_netf += $stats_timeframe['email'];

}

?>

<div class="section top_stats clearfix">
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_nvtf, 0); ?></span>
            <span class="title">View<?php if ($stat_total_nvtf != 1) { ?>s<?php } ?></span>
            <span class="period">7 Days</span>
        </div>
    </div>
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_nvt, 0); ?></span>
            <span class="title">View<?php if ($stat_total_nvt != 1) { ?>s<?php } ?></span>
            <span class="period">Total</span>
        </div>
    </div>
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_netf, 0); ?></span>
            <span class="title">Email<?php if ($stat_total_netf != 1) { ?>s<?php } ?></span>
            <span class="period">7 Days</span>
        </div>
    </div>
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_net, 0); ?></span>
            <span class="title">Email<?php if ($stat_total_net != 1) { ?>s<?php } ?></span>
            <span class="period">Total</span>
        </div>
    </div>
</div>

<?php

// BUSINESS ARRAY
$busa = array();

// GET MATCHING BUSINESSES
$business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` AS `b` WHERE (b.account_id=" .$_uccms['_account']->userID(). ") AND (b.status!=9) ORDER BY b.name ASC, b.id ASC";
$business_q = sqlquery($business_query);
while ($business = sqlfetch($business_q)) {
    $busa[$business['id']] = $business;
}

unset($business);

// HAVE BUSINESSES
if (count($busa) > 0) {
    foreach ($busa as $tbus) {
        if ($tbus['pending']) {
            unset($busa[$tbus['pending']]);
        }
    }
}

// NUMBER OF BUSINESSES
$num_business = count($busa);

?>

<div class="section listings">

    <div class="heading-main clearfix">
        <h4>Business Listings (<?php echo number_format($num_business, 0); ?>)</h4>
        <a href="/<?php echo $_uccms_businessdir->frontendPath(); ?>/submit/" class="add"><i class="fa fa-plus" aria-hidden="true"></i> Add Business</a>
    </div>

    <?php

    // HAVE BUSINESSES
    if ($num_business > 0) {

        ?>
        <div class="businesses clearfix">
            <table id="listings_list" width="100%" border="0">

                <?php

                // LOOP
                foreach ($busa as $business) {

                    // GET TOTAL STATS
                    $stats_total = $_uccms_businessdir->business_getStats(($business['pending'] ? $business['pending'] : $business['id']));

                    // GET TIMEFRAME STATS
                    $stats_timeframe = $_uccms_businessdir->business_getStats(($business['pending'] ? $business['pending'] : $business['id']), array(
                        'date_from' => date('Y-m-d H:i:s', strtotime('-7 Days')),
                        'date_to'   => date('Y-m-d H:i:s')
                    ));

                    ?>
                    <tr>
                        <td>
                            <div style="width: 100px; padding: 10px; overflow: hidden; text-align: center;">
                                <?php if ($business['logo']) { ?>
                                    <img src="<?php echo $_uccms_businessdir->imageBridgeOut($business['logo'], 'businesses'); ?>" alt="" style="width: 100%; height: auto;" />
                                <?php } ?>
                            </div>
                        </td>
                        <td style="width: 50%; padding: 15px;">
                            <div style="font-size: 1.2em; font-weight: bold;">
                                <?=$business['name']?>
                            </div>
                            <div>
                                <?=$business['address1']?>
                            </div>
                            <div>
                                <?=$business['city']?>, <?=$business['state']?> <?=$business['zip']?>
                            </div>
                        </td>
                        <td style="width: 50%; padding: 15px;">
                            <div style="font-size: 1.2em;">
                                <?=$_uccms_businessdir->prettyPhone($business['phone'])?>
                            </div>
                            <?php if ($business['phone_tollfree']) { ?>
                                <div>
                                    <?=$_uccms_businessdir->prettyPhone($business['phone_tollfree'])?>
                                </div>
                            <?php } ?>
                            <?php if ($business['url']) { ?>
                                <div>
                                    <a href="<?=$business['url']?>" target="_blank"><?=$business['url']?></a>
                                </div>
                            <?php } ?>
                            <div class="stats clearfix">
                                <div class="group">
                                    <h4>7 Days</h4>
                                    <div class="clearfix">
                                        <div class="stat" title="Views in the past 7 days">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_timeframe['view'], 0); ?></span>
                                            </div>
                                        </div>
                                        <div class="stat" title="Emails in the past 7 days">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_timeframe['email'], 0); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="group">
                                    <h4>Total</h4>
                                    <div class="clearfix">
                                        <div class="stat" title="Total views">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_total['view'], 0); ?></span>
                                            </div>
                                        </div>
                                        <div class="stat" title="Total emails">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_total['email'], 0); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td style="padding: 15px; text-align: center; white-space: nowrap;">
                            <? /*<a href="/business-listing/review/?id=<?=$listing['id']?>" class="button"><i class="fa fa-eye"></i> View Listing</a>*/ ?>
                            <?php if ($business['pending']) { ?>
                                <div class="pending">Pending</div>
                            <?php } ?>
                            <a href="../business/?id=<?=$business['id']?>&clear=true" class="button"><i class="fa fa-pencil"></i> Edit</a>
                        </td>
                    </tr>
                    <?php

                }

                ?>

            </table>
        </div>
        <?php

    // NO BUSINESSES
    } else {
        ?>
        <div class="no_listings">
            It doesn't look like you have any business listings yet.
        </div>
        <?php
    }

    ?>

</div>