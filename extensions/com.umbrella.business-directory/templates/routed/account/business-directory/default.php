<?php

// MODULE CLASS
if (!$_uccms_businessdir) $_uccms_businessdir = new uccms_BusinessDirectory();

// GET SETTING(S)
$_businessdir['settings'] = $_uccms_businessdir->getSettings();

#########################################################

// RATINGS / REVIEWS
include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

// GET BUSINESSES
$business_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`account_id`=" .$_uccms['_account']->userID(). ") AND (`pending`=0) AND (`status`!=9) ORDER BY `created_dt` DESC";
$business_q = sqlquery($business_query);

// NUMBER OF BUSINESSES
$num_businesses = sqlrows($business_q);

// NUMBER OF REVIEWS
$num_reviews = 0;

// REVIEWS ENABLED
if ($_uccms_businessdir->getSetting('business_ratings_reviews_enabled')) {

    // LOOP
    while ($business = sqlfetch($business_q)) {

        // GET REVIEWS FOR BUSINESS
        $reviewsnum_query = "SELECT `id` FROM `uccms_ratings_reviews` WHERE (`source`='" .$_uccms_businessdir->Extension. "') AND (`what`='business') AND (`item_id`=" .$business['id']. ") AND (`status`=1) AND (`reply_to_id`=0)";
        $reviewsnum_q = sqlquery($reviewsnum_query);

        $num_reviews += sqlrows($reviewsnum_q);

    }

}

#########################################################

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/master/master.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/custom/master.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/master/master.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/custom/master.js"></script>

<h3>Business Directory</h3>

<div class="dashboard clearfix">

    <div class="colSmall">

        <? /*
        <div class="infobox">

            <div class="name">
            </div>

            <img src="<?php echo $_uccms_businessdir->imageBridgeOut($_properties_agent['image'], 'agents'); ?>" alt="<?php echo stripslashes($_properties_agent['name']); ?>" class="main" />

        </div>
        */ ?>

        <div class="nav">
            <ul>
                <li class="<?php if ($bigtree['path'][2] == 'listings') { echo 'active'; } ?>"><a href="/<?php echo $bigtree['path'][0]; ?>/<?php echo $bigtree['path'][1]; ?>/listings/">Listings<span class="num"><?php echo number_format($num_businesses, 0); ?></span></a></li>
                <?php if ($_uccms_businessdir->getSetting('business_ratings_reviews_enabled')) { ?>
                    <li class="<?php if ($bigtree['path'][2] == 'reviews') { echo 'active'; } ?>"><a href="/<?php echo $bigtree['path'][0]; ?>/<?php echo $bigtree['path'][1]; ?>/reviews/">Reviews<span class="num"><?php echo number_format($num_reviews, 0); ?></span></a></li>
                <?php } ?>
            </ul>
        </div>

    </div>

    <div class="colLarge-container">

        <div class="colLarge">

            <?php

            // INCLUDE FILE
            $include_file = dirname(__FILE__). '/dashboard/default.php';

            // HAVE COMMANDS
            if (count($bigtree['commands']) > 1) {

                $base = array_shift($bigtree['commands']);

                // GET BIGTREE ROUTING
                list($include, $commands) = BigTree::route(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/account/' .$base. '/', $bigtree['commands']);

                // FILE TO INCLUDE
                if ($include) {
                    $include_file = $include;
                }

            }

            // INCLUDE ROUTED FILE
            include($include_file);

            ?>

        </div>

    </div>

</div>