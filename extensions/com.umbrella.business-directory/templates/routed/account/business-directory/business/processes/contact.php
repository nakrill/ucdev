<?php

// REQUIRED FIELDS
$reqa = array(
    'address1'  => 'Address',
    'state'     => 'State',
    'zip'       => 'Zip',
    'phone'     => 'Phone Number'
);

// MISSING FIELDS
$missing = false;

// LOOP THROUGH REQUIRED FIELDS
foreach ($reqa as $field_name => $field_title) {
    $val = trim($_POST['business'][$field_name]);
    if (!$val) {
        $missing = true;
        $_uccms['_site-message']->set('error', 'Missing required field: ' .$field_title);
    }
}

// HAVE MISSING REQUIRED FIELDS
if ($missing) {
    BigTree::redirect('../?id=' .$business['id']. '&tab=' .$_REQUEST['goto_tab']);
}

$city_id = (int)$_POST['business']['city_id'];

// HAVE CITY
if ($city_id) {

    // GET CITY INFO
    $city_query = "SELECT * FROM `" .$_uccms_businessdir->tables['cities']. "` WHERE (`id`=" .$city_id. ")";
    $city_q = sqlquery($city_query);
    $city = sqlfetch($city_q);

    if ($city['title']) {
        $_POST['business']['city'] = stripslashes($city['title']);
    }

}

// DB COLUMNS
$columns = array(
    'city_id'                   => $city_id,
    'neighborhood_id'           => (int)$_POST['business']['neighborhood'][$_POST['business']['city_id']]['neighborhood_id'],
    'name_contact'              => $_POST['business']['name_contact'],
    'phone'                     => preg_replace('/\D/', '', $_POST['business']['phone']),
    'phone_ext'                 => $_POST['business']['phone_ext'],
    'phone_tollfree'            => preg_replace('/\D/', '', $_POST['business']['phone_tollfree']),
    'phone_tollfree_ext'        => $_POST['business']['phone_tollfree_ext'],
    'phone_alt'                 => preg_replace('/\D/', '', $_POST['business']['phone_alt']),
    'phone_alt_ext'             => $_POST['business']['phone_alt_ext'],
    'phone_mobile'              => preg_replace('/\D/', '', $_POST['business']['phone_mobile']),
    'fax'                       => preg_replace('/\D/', '', $_POST['business']['fax']),
    'address1'                  => $_POST['business']['address1'],
    'address2'                  => $_POST['business']['address2'],
    'city'                      => $_POST['business']['city'],
    'state'                     => $_POST['business']['state'],
    'zip'                       => preg_replace('/\D/', '', $_POST['business']['zip']),
    'zip4'                      => preg_replace('/\D/', '', $_POST['business']['zip4']),
    'url'                       => $_uccms_businessdir->trimURL($_POST['business']['url']),
    'email'                     => $_POST['business']['email'],
    'email_private'             => $_POST['business']['email_private'],
    'email_lead'                => $_POST['business']['email_lead'],
    'social_facebook'           => $_uccms_businessdir->trimURL($_POST['business']['social']['facebook']),
    'social_foursquare'         => $_uccms_businessdir->trimURL($_POST['business']['social']['foursquare']),
    'social_googleplus'         => $_uccms_businessdir->trimURL($_POST['business']['social']['googleplus']),
    'social_instagram'          => $_uccms_businessdir->trimURL($_POST['business']['social']['instagram']),
    'social_linkedin'           => $_uccms_businessdir->trimURL($_POST['business']['social']['linkedin']),
    'social_pinterest'          => $_uccms_businessdir->trimURL($_POST['business']['social']['pinterest']),
    'social_tripadvisor'        => $_uccms_businessdir->trimURL($_POST['business']['social']['pinterest']),
    'social_twitter'            => $_uccms_businessdir->trimURL($_POST['business']['social']['twitter']),
    'social_yelp'               => $_uccms_businessdir->trimURL($_POST['business']['social']['yelp'])
);

// DB QUERY
$query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$business['id']. ")";
if (sqlquery($query)) {
    $_uccms['_site-message']->set('success', 'Contact information saved.');
} else {
    $_uccms['_site-message']->set('error', 'Failed to save contact information.');
}

?>