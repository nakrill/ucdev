<?php

$cata = array();

// GET CURRENT RELATIONS
$query = "SELECT `category_id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ")";
$q = sqlquery($query);
while ($row = sqlfetch($q)) {
    $cata[$row['category_id']] = $row['category_id'];
}

// HAVE CATEGORIES
if (count($_POST['category']) > 0) {

    // GET MAXIMUM NUMBER OF CATEGORIES ALLOWED
    $max_categories = (int)$_uccms_businessdir->getSetting('listing_max_categories');

    $i = 1;

    // LOOP
    foreach ($_POST['category'] as $category_id => $selected) {

        // MAX CATEGORIES
        if ($max_categories > 0) {
            if ($i > $max_categories) {
                $_uccms['_site-message']->set('warning', 'Selected categories more than maximum (' .$max_categories. '). Used first ' .$max_categories. ' categories.');
                break;
            }
        }

        // CLEAN UP
        $category_id = (int)$category_id;

        // HAVE CATEGORY ID & WAS SELECTED
        if (($category_id) && ($selected)) {

            // NOT IN EXISTING CATEGORY RELATIONS
            if (!$cata[$category_id]) {

                // DB COLUMNS
                $columns = array(
                    'business_id'   => $business['id'],
                    'category_id'   => $category_id
                );

                // ADD RELATIONSHIP TO DB
                $query = "INSERT INTO `" .$_uccms_businessdir->tables['business_categories']. "` SET " .uccms_createSet($columns). "";
                sqlquery($query);

            }

            // REMOVE FROM RELATIONS
            unset($cata[$category_id]);

        }

        $i++;

    }

}

// HAVE LEFT OVER (OLD) RELATIONS
if (count($cata) > 0) {

    // LOOP
    foreach ($cata as $category_id) {

        // REMOVE RELATIONSHIP FROM DB
        $query = "DELETE FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ") AND (`category_id`=" .$category_id. ")";
        sqlquery($query);

    }

}

$_uccms['_site-message']->set('success', 'Category information saved.');

?>