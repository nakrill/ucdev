<?php

// REQUIRED FIELDS
$reqa = array(
    'name'      => 'Business Name'
);

// MISSING FIELDS
$missing = false;

// LOOP THROUGH REQUIRED FIELDS
foreach ($reqa as $field_name => $field_title) {
    $val = trim($_POST['business'][$field_name]);
    if (!$val) {
        $missing = true;
        $_uccms['_site-message']->set('error', 'Missing required field: ' .$field_title);
    }
}

// HAVE MISSING REQUIRED FIELDS
if ($missing) {
    BigTree::redirect('../?id=' .$business['id']. '&tab=' .$_REQUEST['goto_tab']);
}

$city_id = (int)$_POST['business']['city_id'];

// HAVE CITY
if ($city_id) {

    // GET CITY INFO
    $city_query = "SELECT * FROM `" .$_uccms_businessdir->tables['cities']. "` WHERE (`id`=" .$city_id. ")";
    $city_q = sqlquery($city_query);
    $city = sqlfetch($city_q);

    if ($city['title']) {
        $_POST['business']['city'] = stripslashes($city['title']);
    }

}

// DB COLUMNS
$columns = array(
    'name'                      => $_POST['business']['name'],
    'department'                => $_POST['business']['department'],
    'name_owner'                => $_POST['business']['name_owner'],
    'description'               => $_POST['business']['description'],
    'tagline'                   => $_POST['business']['tagline'],
    'keywords'                  => $_POST['business']['keywords'],
    'year_opened'               => date('Y', strtotime('01/01/' .$_POST['business']['year_opened'])),
    'payment_methods'           => $_uccms_businessdir->arrayToCSV($_POST['business']['payment_methods']),
    'languages'                 => $_uccms_businessdir->arrayToCSV($_POST['business']['languages']),
);

if ($_POST['business']['name']) {
    $columns['slug'] = $_uccms_businessdir->makeRewrite($_POST['business']['name']);
}

// DB QUERY
$query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$business['id']. ")";
if (sqlquery($query)) {
    $_uccms['_site-message']->set('success', 'Basic information saved.');
} else {
    $_uccms['_site-message']->set('error', 'Failed to save basic information.');
}

// FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
if (($_FILES['logo']['name']) || ((!$_POST['logo']) || (substr($_POST['logo'], 0, 11) == 'resource://'))) {

    // GET CURRENT IMAGE
    $ex_query = "SELECT `logo` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$business['id']. ")";
    $ex = sqlfetch(sqlquery($ex_query));

    // THERE'S AN EXISTING IMAGE
    if ($ex['logo']) {

        // REMOVE IMAGE
        @unlink($_uccms_businessdir->imageBridgeOut($ex['logo'], 'businesses', true));

        // UPDATE DATABASE
        $query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET `logo`='' WHERE (`id`=" .$business['id']. ")";
        sqlquery($query);

    }

}

// FILE UPLOADED / SELECTED
if (($_FILES['logo']['name']) || ($_POST['logo'])) {

    // BIGTREE UPLOAD FIELD INFO
    $field = array(
        'type'          => 'upload',
        'title'         => 'Business Logo',
        'key'           => 'image',
        'options'       => array(
            'directory' => 'extensions/' .$_uccms_businessdir->Extension. '/files/businesses',
            'image' => true,
            'thumbs' => array(
                array(
                    'width'     => '640', // MATTD: make controlled through settings
                    'height'    => '640' // MATTD: make controlled through settings
                )
            ),
            'crops' => array()
        )
    );

    // UPLOADED FILE
    if ($_FILES['logo']['name']) {
        $field['file_input'] = $_FILES['logo'];

    // FILE FROM MEDIA BROWSER
    } else if ($_POST['logo']) {
        $field['input'] = $_POST['logo'];
    }

    // INIT BIGTREE ADMIN
    $admin = new BigTreeAdmin;

    // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
    $file_path = BigTreeAdmin::processField($field);

    // DESTROY BIGTREE ADMIN
    unset($admin);

    // UPLOAD SUCCESSFUL
    if ($file_path) {

        // UPDATE DATABASE
        $query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET `logo`='" .sqlescape($_uccms_businessdir->imageBridgeIn($file_path)). "' WHERE (`id`=" .$business['id']. ")";
        sqlquery($query);

    // UPLOAD FAILED
    } else {

        // $bigtree['errors'][0]['field']
        $_uccms['_site-message']->set('error', 'Failed to upload logo. (' .$bigtree['errors'][0]['error']. ')');

    }

}

// HAVE HOURS
if (is_array($_POST['business']['hours'])) {

    // LOOP
    foreach ($_POST['business']['hours'] as $day => $hours) {

        // DB COLUMNS
        $columns = array(
            'day'       => (int)$day,
            'open'      => date('H:i', strtotime($hours['open'])),
            'close'     => date('H:i', strtotime($hours['close'])),
            'closed'    => (int)$hours['closed']
        );

        // SEE IF RECORD EXISTS
        $hc_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$business['id']. ") AND (`day`=" .(int)$day. ")";
        $hc_q = sqlquery($hc_query);
        $hc = sqlfetch($hc_q);

        // RECORD EXISTS
        if ($hc['id']) {

            $query = "UPDATE `" .$_uccms_businessdir->tables['business_hours']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$hc['id']. ")";

        // NO RECORD
        } else {

            $columns['business_id'] = $business['id'];

            $query = "INSERT INTO `" .$_uccms_businessdir->tables['business_hours']. "` SET " .uccms_createSet($columns). "";

        }

        sqlquery($query);

    }

}

?>