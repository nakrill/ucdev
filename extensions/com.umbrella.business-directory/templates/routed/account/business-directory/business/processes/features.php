<?php

// HAVE FEATURES
if (count($_POST['feature']) > 0) {

    // HAVE FEATURES
    if (count($feata) > 0) {

        // MISSING FIELDS
        $missing = false;

        // LOOP
        foreach ($feata as $feature) {

            // IS REQUIRED
            if ($feature['required']) {

                // CLEAN UP VALUE
                $val = trim($_POST['feature'][$feature['id']][0]);

                if (!$val) {
                    $missing = true;
                    $_uccms['_site-message']->set('error', 'Missing required field: ' .stripslashes($feature['title']));
                }

            }

        }

        // HAVE MISSING REQUIRED FIELDS
        if ($missing) {
            BigTree::redirect('../?id=' .$business['id']. '&tab=' .$_REQUEST['goto_tab']);
        }

    }

    // LOOP
    foreach ($_POST['feature'] as $feature_id => $options) {

        // HAS VALUE RECORD
        if ($feata[$feature_id]['value']['id']) {

            $columns = array(
                'option_id'     => 0,
                'value'         => implode(',', $options)
            );

            // UPDATE VALUE
            $query = "UPDATE `" .$_uccms_businessdir->tables['business_feature_values']. "` SET " .$_uccms_businessdir->createSet($columns). " WHERE (`id`=" .$feata[$feature_id]['value']['id']. ")";

        // NO VALUE RECORD
        } else {

            $columns = array(
                'business_id'   => $id,
                'feature_id'    => (int)$feature_id,
                'option_id'     => 0,
                'value'         => implode(',', $options)
            );

            // RECORD VALUE
            $query = "INSERT INTO `" .$_uccms_businessdir->tables['business_feature_values']. "` SET " .$_uccms_businessdir->createSet($columns);

        }

        // QUERY SUCCESSFUL
        if (sqlquery($query)) {
            unset($feata[$feature_id]);
        }

    }

}

// HAVE LEFTOVER FEATURES
if (count($feata) > 0) {

    // LOOP
    foreach ($feata as $feature) {

        // DELETE FEATURE VALUE
        $delete_query = "DELETE FROM `" .$_uccms_businessdir->tables['business_feature_values']. "` WHERE (`business_id`=" .$id. ") AND (`feature_id`=" .$feature['id']. ")";
        sqlquery($delete_query);

    }

}

$_uccms['_site-message']->set('success', 'Feature information saved.');

?>