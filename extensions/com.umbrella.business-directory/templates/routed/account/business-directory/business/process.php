<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SAVE SUBMITTED DATA TO SESSION
    if (!is_array($_SESSION['_uccms']['business-directory']['business']['post'])) $_SESSION['_uccms']['business-directory']['business']['post'] = array();
    $_SESSION['_uccms']['business-directory']['business']['post'] = array_merge($_SESSION['_uccms']['business-directory']['business']['post'], $_POST);

    // CLEAN UP
    $id = (int)$_POST['business']['id'];

    // ID SPECIFIED
    if ($id > 0) {

        // GET BUSINESS
        $business = $_uccms_businessdir->getBusiness($id, $_uccms_businessdir->getSetting('approve_listing_update'), array(
            'account_id' => $_uccms['_account']->userID()
        ), array(
            'create' => true
        ));

        // BUSINESS FOUND
        if ($business['id']) {

            // TABS
            include(dirname(__FILE__). '/tabs.php');

            // INCLUDE PROCESSING FILE
            include(dirname(__FILE__). '/processes/' .$tab. '.php');

            // APPROVAL REQUIRED
            if ($_uccms_businessdir->getSetting('approve_listing_update')) {

                $_uccms['_site-message']->set('info', 'Changes will be reflected on the site once approved.');

                // GET EMAILS
                $emaila = explode(',', $_uccms_businessdir->getSetting('approve_listing_update_emails'));

                // HAVE EMAIL ADDRESS(S)
                if (count($emaila) > 0) {

                    ##########################
                    # EMAIL
                    ##########################

                    // GET EMAIL RELATED SETTINGS
                    $esitesetta = $_uccms_businessdir->getSettings(array('email_from_name','email_from_email'));

                    // EMAIL SETTINGS
                    $esettings = array(
                        'custom_template'   => SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/email/updated.html',
                        'subject'           => 'Business Listing Updated (Approval Required)',
                        'listing_id'        => $business['id']
                    );

                    if ($esitesetta['email_from_name']) $esettings['from_name'] = $esitesetta['email_from_name'];
                    if ($esitesetta['email_from_email']) $esettings['from_email'] = $esitesetta['email_from_email'];

                    // EMAIL VARIABLES
                    $evars = array(
                        'heading'               => 'Business Listing Updated',
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'listing_id'            => $business['id'],
                        'business_name'         => $_POST['business']['name'],
                        'admin_url'             => WWW_ROOT. '/admin/' .$_uccms_businessdir->Extension. '*business-directory/businesses/edit/?id=' .$business['id']
                    );

                    // LOOP THROUGH EMAILS
                    foreach ($emaila as $email) {

                        // WHO TO SEND TO
                        $esettings['to_email'] = $email;

                        // SEND EMAIL
                        $result = $_uccms['_account']->sendEmail($esettings, $evars);

                    }

                }

            }

            unset($_SESSION['_uccms']['business-directory']['business']['post']);

        // BUSINESS NOT FOUND
        } else {

            $_uccms['_site-message']->set('error', 'Business not found or is not yours.');
            BigTree::redirect('../../');

        }

    // ID NOT SPECIFIED
    } else {
        $_uccms['_site-message']->set('error', 'Business not specified.');
        BigTree::redirect('../../');
    }

// FORM NOT SUBMITTED
} else {
    $_uccms['_site-message']->set('error', 'Form not submitted.');
    BigTree::redirect('../../');
}

// REDIRECT
if ($taba[$_REQUEST['goto_tab']]) {
    BigTree::redirect('../?id=' .$business['id']. '&tab=' .$_REQUEST['goto_tab']);
} else {
    BigTree::redirect('../?id=' .$business['id']);
}

?>