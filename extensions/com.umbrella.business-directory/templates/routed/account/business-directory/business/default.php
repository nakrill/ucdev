<?php

// CLEAR SESSION
if ($_REQUEST['clear']) {
    unset($_SESSION['_uccms']['business-directory']['business']['post']);
}

// BUSINESS
$business = array();

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id > 0) {

    // GET BUSINESS
    $business = $_uccms_businessdir->getBusiness($id, true, array(
        'account_id' => $_uccms['_account']->userID()
    ));

}

// USE INFO FROM SESSION
if (is_array($_SESSION['_uccms']['business-directory']['business']['post']['business'])) {
    unset($_SESSION['_uccms']['business-directory']['business']['post']['business']['id']);
    $business = array_merge($business, $_SESSION['_uccms']['business-directory']['business']['post']['business']);
}

if (!$business['id']) {
    $_uccms['_site-message']->set('error', 'Business not specified.');
    BigTree::redirect('../');
}

// TABS
include(dirname(__FILE__). '/tabs.php');

// NEW BUSINESS
if (!$business['id']) {

    // ONLY FIRST TAB
    $taba = array_slice($taba, 0, 1);

}

// TREAT FORM AS EMBEDDED
$bigtree['form']['embedded'] = true;

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/master/business/edit/main.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/custom/business/edit/main.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/master/business/edit/main.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/custom/business/edit/main.js"></script>

<style type="text/css">

</style>

<script type="text/javascript">

    $(document).ready(function() {

        /*
        // LISTING TYPE
        $('#myAccount .section.business form.main #listing_type').change(function() {
            if ($(this).val() == 1) {
                $('#myAccount .section.business .lt-rental').hide();
                $('#myAccount .section.business .lt-sale').hide();
            } else if ($(this).val() == 2) {
                $('#myAccount .section.business .lt-sale').hide();
                $('#myAccount .section.business .lt-rental').show();
            }
        });

        // DATE / TIME PICKER
        $('#myAccount .section.business form.main .date_picker').datepicker({ dateFormat: 'mm/dd/yy', duration: 200, showAnim: "slideDown" });
        $('#myAccount .section.business form.main .time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
        $('#myAccount .section.business form.main .date_time_picker').datetimepicker({ dateFormat: 'mm/dd/yy', timeFormat: "hh:mm tt", duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10 });
        */

    });

</script>

<div class="section business">

    <h3><?php if ($business['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Business</h3>

    <form action="./process/" method="post" enctype="multipart/form-data" class="main">
    <input type="hidden" name="tab" value="<?php echo $tab; ?>" />
    <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />
    <input type="hidden" name="goto_tab" value="<?php echo $tab; ?>" />

    <div class="tabs1 main">
        <ul class="nav">
            <?php foreach ($taba as $tab_id => $tab_title) { ?>
                <li><a href="?id=<?php echo $business['id']; ?>&tab=<?php echo $tab_id; ?>" <?php if ($tab_id == $tab) { ?>class="active"<?php } ?> data-tab="<?php echo $tab_id; ?>"><?php echo $tab_title; ?></a></li>
            <?php } ?>
        </ul>
        <div class="content">
            <div id="tab-<?php echo $tab; ?>" class="item">
                <?php include(dirname(__FILE__). '/tabs/' .$tab. '.php'); ?>
            </div>
        </div>
    </div>

    <?php if (($tab != 'status') && ($tab != 'reviews')) { ?>
        <div class="main_buttons">
            <input type="submit" value="Save" class="button save" />
            <?php if ($tab_next) { ?>
                <input type="button" value="Save & Continue" class="button save-continue" data-next="<?php echo $tab_next; ?>" />
            <?php } ?>
        </div>
    <?php } ?>

    </form>

</div>

<?php include(BigTree::path('admin/layouts/_html-field-loader.php')); ?>