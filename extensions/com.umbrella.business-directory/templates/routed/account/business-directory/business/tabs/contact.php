<?php

// ARRAY OF CITIES
$citiesa = array();

// GET CITIES
$cities_query = "
SELECT c.*
FROM `" .$_uccms_businessdir->tables['cities']. "` AS `c`
ORDER BY c.title ASC, c.id ASC
";
$cities_q = sqlquery($cities_query);
while ($agency = sqlfetch($cities_q)) {
    $citiesa[$agency['id']] = $agency;
}

// ARRAY OF NEIGHBORHOODS
$neighborhoodsa = array();

// GET NEIGHBORHOODS
$neighborhoods_query = "
SELECT n.*
FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` AS `n`
ORDER BY n.title ASC, n.id ASC
";
$neighborhoods_q = sqlquery($neighborhoods_query);
while ($neighborhood = sqlfetch($neighborhoods_q)) {
    $neighborhoodsa[$neighborhood['city_id']][$neighborhood['id']] = $neighborhood;
}

?>

<script type="text/javascript">

    $(document).ready(function() {

        // CITY CHANGE
        $('#tab-contact select[name="business[city_id]"]').change(function() {
            $('#tab-contact .neighborhoods .city').hide();
            $('#tab-contact .neighborhoods .city-' +$(this).val()).show();
        });

    });

</script>

<div class="fields">

    <div class="group">

        <fieldset>
            <label>Contact Name</label>
            <input type="text" value="<?php echo stripslashes($business['name_contact']); ?>" name="business[name_contact]" />
        </fieldset>

    </div>

    <div class="group clearfix">

        <div class="half left">

            <fieldset>
                <label>Address <span class="required">*</span></label>
                <input type="text" value="<?php echo stripslashes($business['address1']); ?>" name="business[address1]" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Address Cont.</label>
                <input type="text" value="<?php echo stripslashes($business['address2']); ?>" name="business[address2]" />
            </fieldset>

        </div>

        <? /*
        <fieldset>
            <label>City <span class="required">*</span></label>
            <input type="text" value="<?php echo $business['city']?>" name="business[city]" />
        </fieldset>
        */ ?>

        <div class="half left">

            <?php if (count($citiesa) > 0) { ?>
                <fieldset class="cities">
                    <label>City</label>
                    <select name="business[city_id]">
                        <option value="0">None</option>
                        <?php foreach ($citiesa as $city) { ?>
                            <option value="<?php echo $city['id']; ?>" <?php if ($city['id'] == $business['city_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($city['title']); ?></option>
                        <?php } ?>
                    </select>
                </fieldset>
            <?php } ?>

        </div>

        <div class="half right">
            <fieldset class="neighborhoods">
                <?php if (count($neighborhoodsa) > 0) { ?>
                    <label>Neighborhood</label>
                    <?php foreach ($neighborhoodsa as $city_id => $neighborhoods) { ?>
                        <div class="city city-<?php echo $city_id; ?>" <?php if ($city_id != $business['city_id']) { ?>style="display: none;"<?php } ?>>
                            <select name="business[neighborhood][<?php echo $city_id; ?>][neighborhood_id]">
                                <option value="0">None</option>
                                <?php foreach ($neighborhoods as $neighborhood) { ?>
                                    <option value="<?php echo $neighborhood['id']; ?>" <?php if ($neighborhood['id'] == $business['neighborhood_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($neighborhood['title']); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    <?php } ?>
                <?php } ?>
            </fieldset>
        </div>

        <div class="half left">

            <fieldset>
                <label>State <span class="required">*</span></label>
                <select name="business[state]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?php echo $state_code?>" <?php if ($state_code == $business['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name?></option>
                    <?php } ?>
                </select>
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Zip</label>
                <input type="text" value="<?php echo $business['zip']; ?>" name="business[zip]" style="display: inline; width: 50px;" /><span class="required">*</span> - <input type="text" value="<?php echo $business['zip4']?>" name="business[zip4]; " style="display: inline; width: 40px;" />
            </fieldset>

        </div>

    </div>

    <div class="group clearfix">

        <div class="half left">

            <fieldset>
                <label>Phone <span class="required">*</span></label>
                <input type="text" value="<?php echo $business['phone']; ?>" name="business[phone]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?php echo $business['phone_ext']; ?>" name="business[phone_ext]" style="display: inline-block; width: 30px;" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Toll-Free Number</label>
                <input type="text" value="<?php echo $business['phone_tollfree']; ?>" name="business[phone_tollfree]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?php echo $business['phone_tollfree_ext']; ?>" name="business[phone_tollfree_ext]" style="display: inline-block; width: 30px;" />
            </fieldset>

        </div>

        <div class="half left">

            <fieldset>
                <label>Alternate Phone Number</label>
                <input type="text" value="<?php echo $business['phone_alt']; ?>" name="business[phone_alt]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?php echo $business['phone_alt_ext']; ?>" name="business[phone_alt_ext]" style="display: inline-block; width: 30px;" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Mobile</label>
                <input type="text" value="<?php echo $business['phone_mobile']; ?>" name="business[phone_mobile]" style="display: inline-block; width: 100px;" />
            </fieldset>

        </div>

        <div class="half left">

            <fieldset>
                <label>Fax</label>
                <input type="text" value="<?php echo $business['fax']; ?>" name="business[fax]" style="display: inline-block; width: 100px;" />
            </fieldset>

        </div>

    </div>

    <div class="group clearfix">

        <div class="half left">

            <fieldset>
                <label>Email</label>
                <input type="text" value="<?php echo stripslashes($business['email']); ?>" name="business[email]" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Private Email</label>
                <input type="text" value="<?php echo stripslashes($business['email_private']); ?>" name="business[email_private]" />
            </fieldset>

        </div>

        <div class="half left">

            <fieldset>
                <label>Lead Email</label>
                <input type="text" value="<?php echo stripslashes($business['email_lead']); ?>" name="business[email_lead]" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Website URL</label>
                <input type="text" value="<?php echo stripslashes($business['url']); ?>" name="business[url]" placeholder="www.domain.com" />
            </fieldset>

        </div>

    </div>

    <div class="group clearfix">

        <div class="half left">

            <fieldset>
                <label>Facebook URL</label>
                <input type="text" value="<?=$business['social_facebook']?>" name="business[social][facebook]" placeholder="www.facebook.com/pagename" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Foursquare URL</label>
                <input type="text" value="<?=$business['social_foursquare']?>" name="business[social][foursquare]" placeholder="www.foursquare.com/v/name/value" />
            </fieldset>

        </div>

        <div class="half left">

            <fieldset>
                <label>Google+ URL</label>
                <input type="text" value="<?=$business['social_googleplus']?>" name="business[social][googleplus]" placeholder="plus.google.com/+name" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Instagram URL</label>
                <input type="text" value="<?=$business['social_instagram']?>" name="business[social][instagram]" placeholder="www.instagram.com/username" />
            </fieldset>

        </div>

        <div class="half left">

            <fieldset>
                <label>LinkedIn URL</label>
                <input type="text" value="<?=$business['social_linkedin']?>" name="business[social][linkedin]" placeholder="www.linkedin.com/company/companyname" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Pinterest URL</label>
                <input type="text" value="<?=$business['social_pinterest']?>" name="business[social][pinterest]" placeholder="www.pinterest.com/username" />
            </fieldset>

        </div>

        <div class="half left">

            <fieldset>
                <label>TripAdvisor URL</label>
                <input type="text" value="<?=$business['social_tripadvisor']?>" name="business[social][tripadvisor]" placeholder="www.tripadvisor.com/URL" />
            </fieldset>

        </div>

        <div class="half right">

            <fieldset>
                <label>Twitter URL</label>
                <input type="text" value="<?=$business['social_twitter']?>" name="business[social][twitter]" placeholder="www.twitter.com/username" />
            </fieldset>

        </div>

        <div class="half left">

            <fieldset>
                <label>Yelp URL</label>
                <input type="text" value="<?=$business['social_yelp']?>" name="business[social][yelp]" placeholder="www.yelp.com/biz/name" />
            </fieldset>

        </div>

    </div>

</div>