<div class="fields">

    <div class="group">

        <?php

        // HAVE FEATURE VALUES SAVED TO SESSION
        if (is_array($_SESSION['_uccms']['business-directory']['business']['post']['feature'])) {

            // LOOP
            foreach ($_SESSION['_uccms']['business-directory']['business']['post']['feature'] as $feat_id => $feat) {
                if (count($feat) > 1) {
                    $feata[$feat_id]['value']['value'] = $feat;
                } else {
                    $feata[$feat_id]['value']['value'] = $feat[0];
                }
            }

        }

        // DISPLAY FEATURES
        include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/features/edit/main.php');

        ?>

    </div>

</div>