<?php

// ARRAY OF CATEGORIES
$cata = array();

// GET CURRENT RELATIONS
$query = "SELECT `category_id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$id. ")";
$q = sqlquery($query);
while ($row = sqlfetch($q)) {
    $cata[$row['category_id']] = $row['category_id'];
}

// OUTPUT LIST
function this_listElements($array) {
    global $cata;
    if (count($array) > 0) {
        echo '<ul>';
        foreach ($array as $element) {
            if ($cata[$element['id']]) {
                $checked = 'checked="checked"';
            } else {
                $checked = '';
            }
            echo '<li><input type="checkbox" name="category[' .$element['id']. ']" value="1" ' .$checked. ' /> ' .stripslashes($element['title']);
            if (!empty($element['children'])) {
                this_listElements($element['children']);
            }
            echo '</li>';
        }
        echo '</ul>';
    }
}

?>

<style type="text/css">

    #tab-categories .limit {
        margin-bottom: 10px;
        font-size: .9em;
        color: #C0362F;
        opacity: .8;
    }

    #tab-categories ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }

    #myAccount #tab-categories ul li {
        margin: 8px 0 0 0;
    }
    #myAccount #tab-categories ul li ul {
        margin: 0 0 0 15px;
        padding: 0px;
    }

</style>

<?php if ($category_limit !== '') { ?>
    <div class="limit">Limit: <?php echo (int)$category_limit; ?></div>
<?php } ?>

<div class="fields">

    <div class="group">

        <?php

        // BUILD LIST OFF ALL CATEGORIES
        echo this_listElements($_uccms_businessdir->getCategoryTree());

        ?>

    </div>

</div>