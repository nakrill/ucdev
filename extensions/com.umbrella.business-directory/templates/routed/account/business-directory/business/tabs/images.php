<style type="text/css">

    #tab-images .add {
        padding-bottom: 10px;
        border-bottom: 1px solid #ccc;
        text-align: right;
    }
    #tab-images .add_item {
        display: none;
        padding: 10px 0;
        text-align: left;
        border-bottom: 1px solid #ccc;
    }
    #tab-images .add_item .add_container .upload {
        float: left;
        margin-right: 20px;
    }
    #tab-images .add_item .add_container .title {
        float: left;
        margin-right: 20px;
    }
    #tab-images .add_item .add_container .buttons {
        float: left;
    }

    #tab-images .items .item {
        padding: 10px;
    }
    #tab-images .items .item:nth-child(odd) {
        background-color: #f5f5f5;
    }
    #tab-images .items .item .sort {
        float: left;
        margin-right: 20px;
        font-size: 1.2em;
        cursor: move;
    }
    #tab-images .items .item .image {
        margin-right: 20px;
        float: left;
    }
    #tab-images .items .item .image img {
        max-width: 250px;
    }
    #tab-images .items .item .title {
        float: left;
        width: 370px;
        margin-right: 20px;
    }
    #tab-images .items .item .title .upload {
        margin-top: 15px;
    }
    #tab-images .items .item .buttons {
        float: right;
        font-size: 1.2em;
    }
    #tab-images .items .item .buttons .delete {
        color: #e53935;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {

        // IMAGES - SORTABLE
        $('#tab-images .items').sortable({
            handle: '.sort',
            axis: 'y',
            containment: 'parent',
            items: '.item',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
                var rows = $(this).find('div.item');
                var sort = [];
                rows.each(function() {
                    sort.push($(this).attr('id').replace('row_', ''));
                });
                $('#tab-images input[name="sort"]').val(sort.join());
            }
        });

        // IMAGES - ADD CLICK
        $('#tab-images .add a').click(function(e) {
            e.preventDefault();
            $('#tab-images .add_item').toggle();
        });

        // IMAGES - REMOVE CLICK
        $('#tab-images .items .item .delete').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to delete this?')) {
                var id = $(this).attr('data-id');
                var del = $('#tab-images input[name="delete"]').val();
                if (del) del += ',';
                del += id;
                $('#tab-images input[name="delete"]').val(del);
                $('#tab-images #row_' +id).fadeOut(500);
            }
        });

    });

</script>

<input type="hidden" name="sort" value="" />
<input type="hidden" name="delete" value="" />

<div class="add">
    <a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Add Image</a>
</div>

<div class="add_item">
    <div class="add_container clearfix">
        <div class="upload">
            <input type="file" name="new" data-min-width="0" data-min-height="0" accept="image/*" />
        </div>
        <div class="title">
            <fieldset>
                <input type="text" name="new-caption" value="" placeholder="Caption / Alt Text" style="width: 300px;" />
            </fieldset>
        </div>
        <div class="buttons">
            <input type="submit" value="Add" class="button" />
        </div>
    </div>
</div>

<?php

// GET IMAGES
$image_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_images']. "` WHERE (`business_id`=" .$business['id']. ") ORDER BY `sort` ASC, `id` ASC";
$image_q = sqlquery($image_query);

// NUMBER OF IMAGES
$num_images = sqlrows($image_q);

// HAVE IMAGES
if ($num_images > 0) {

    ?>

    <div class="items ui-sortable">

        <?php

        // LOOP
        while ($image = sqlfetch($image_q)) {

            // IMAGE URL
            $image_url = $_uccms_businessdir->imageBridgeOut($image['image'], 'businesses');

            ?>

            <div id="row_<?php echo $image['id']; ?>" class="item clearfix">
                <div class="sort">
                    <i class="fa fa-sort" aria-hidden="true"></i>
                </div>
                <div class="image">
                    <?php if ($image_url) { ?>
                        <img src="<?php echo $image_url ?>" alt="IMAGE" />
                    <?php } ?>
                </div>
                <div class="title">
                    <input type="text" name="image-<?php echo $image['id']; ?>-caption" value="<?php echo stripslashes($image['caption']); ?>" placeholder="Caption / Alt Text" style="width: 300px;" />
                    <div class="upload">
                        <input type="file" name="image-<?php echo $image['id']; ?>" data-min-width="0" data-min-height="0" accept="image/*" />
                    </div>
                </div>
                <div class="buttons">
                    <a href="#" data-id="<?php echo $image['id']; ?>" class="delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                </div>
            </div>

            <?php

        }

        ?>

    </div>

    <?php

} else {

    ?>

    <div style="padding: 10px; text-align: center;">
        No images added yet.
    </div>

    <?php

}

?>