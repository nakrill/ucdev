<style type="text/css">

    #tab-videos .add {
        padding-bottom: 10px;
        border-bottom: 1px solid #ccc;
        text-align: right;
    }
    #tab-videos .add_item {
        display: none;
        padding: 10px 0;
        text-align: left;
        border-bottom: 1px solid #ccc;
    }
    #tab-videos .add_item .add_container input[type="text"] {
        width: 300px;
    }
    #tab-videos .add_item .add_container .upload {
        float: left;
        margin-right: 20px;
    }
    #tab-videos .add_item .add_container .title {
        float: left;
        margin-right: 20px;
    }
    #tab-videos .add_item .add_container .buttons {
        float: left;
    }

    #tab-videos .items .item {
        padding: 10px;
    }
    #tab-videos .items .item:nth-child(odd) {
        background-color: #f5f5f5;
    }
    #tab-videos .items .item input[type="text"] {
        width: 300px;
    }
    #tab-videos .items .item .sort {
        float: left;
        margin-right: 20px;
        font-size: 1.2em;
        cursor: move;
    }
    #tab-videos .items .item .video {
        margin-right: 20px;
        float: left;
    }
    #tab-videos .items .item .video img {
        max-width: 250px;
    }
    #tab-videos .items .item .title {
        float: left;
        width: 370px;
        margin-right: 20px;
    }
    #tab-videos .items .item .title .upload {
        margin-top: 15px;
    }
    #tab-videos .items .item .buttons {
        float: right;
        font-size: 1.2em;
    }
    #tab-videos .items .item .buttons .delete {
        font-size: 1.2em;
        color: #e53935;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {

        // VIDEOS - SORTABLE
        $('#tab-videos .items').sortable({
            handle: '.sort',
            axis: 'y',
            containment: 'parent',
            items: '.item',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
                var rows = $(this).find('div.item');
                var sort = [];
                rows.each(function() {
                    sort.push($(this).attr('id').replace('row_', ''));
                });
                $('#tab-videos input[name="sort"]').val(sort.join());
            }
        });

        // VIDEOS - ADD CLICK
        $('#tab-videos .add a').click(function(e) {
            e.preventDefault();
            $('#tab-videos .add_item').toggle();
        });

        // VIDEOS - REMOVE CLICK
        $('#tab-videos .items .item .delete').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to delete this?')) {
                var id = $(this).attr('data-id');
                var del = $('#tab-videos input[name="delete"]').val();
                if (del) del += ',';
                del += id;
                $('#tab-videos input[name="delete"]').val(del);
                $('#tab-videos #row_' +id).fadeOut(500);
            }
        });

    });

</script>

<input type="hidden" name="sort" value="" />
<input type="hidden" name="delete" value="" />

<div class="add">
    <a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Add Video</a>
</div>

<div class="add_item">
    <div class="add_container clearfix">
        <div class="upload">
            <input type="text" name="video[new][url]" value="" placeholder="YouTube / Vimeo URL" style="width: 300px;" />
        </div>
        <div class="title">
            <fieldset>
                <input type="text" name="video[new][caption]" value="" placeholder="Caption / Alt Text" style="width: 300px;" />
            </fieldset>
        </div>
        <div class="buttons">
            <input type="submit" value="Add" class="button" />
        </div>
    </div>
</div>

<?php

// GET VIDEOS
$video_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_videos']. "` WHERE (`business_id`=" .$business['id']. ") ORDER BY `sort` ASC, `id` ASC";
$video_q = sqlquery($video_query);

// NUMBER OF VIDEOS
$num_videos = sqlrows($video_q);

// HAVE VIDEOS
if ($num_videos > 0) {

    ?>

    <div class="items ui-sortable">

        <?php

        // LOOP
        while ($video = sqlfetch($video_q)) {

            // GET VIDEO INFO
            $vi = videoInfo(stripslashes($video['video']));

            ?>

            <div id="row_<?php echo $video['id']; ?>" class="item clearfix">
                <div class="sort">
                    <i class="fa fa-sort" aria-hidden="true"></i>
                </div>
                <div class="video">
                    <?php if ($vi['thumb']) { ?>
                        <a href="<?php echo $video['video']; ?>" target="_blank"><img src="<?php echo $vi['thumb']; ?>" alt="VIDEO" /></a>
                    <?php } ?>
                </div>
                <div class="title">
                    <input type="text" name="video[<?php echo $video['id']; ?>][caption]" value="<?php echo stripslashes($video['caption']); ?>" placeholder="Title" />
                    <div class="upload">
                        <input type="text" name="video[<?php echo $video['id']; ?>][url]" value="<?php echo stripslashes($video['video']); ?>" placeholder="YouTube / Vimeo URL" />
                    </div>
                </div>
                <div class="buttons">
                    <a href="#" data-id="<?php echo $video['id']; ?>" class="delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                </div>
            </div>

            <?php

        }

        ?>

    </div>

    <?php

} else {

    ?>

    <div style="padding: 10px; text-align: center;">
        No videos added yet.
    </div>

    <?php

}

?>