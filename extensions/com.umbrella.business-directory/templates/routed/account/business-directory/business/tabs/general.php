<?php

// PAYMENT METHODS
$pma = array();

// LANGUAGES
$la = array();

// GET HOURS
$hours_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$business['id']. ")";
$hours_q = sqlquery($hours_query);
while ($hours = sqlfetch($hours_q)) {
    $business['hours'][$hours['day']] = $hours;
}

// PAYMENT METHODS
if (is_array($business['payment_methods'])) {
    $pma = $business['payment_methods'];
} else {
    $pma = explode(',', stripslashes($business['payment_methods']));
}

// LANGUAGES
if (is_array($business['languages'])) {
    $la = $business['languages'];
} else {
    $la = explode(',', stripslashes($business['languages']));
}

// ARRAYS
if (!is_array($business['languages'])) $business['languages'] = array();
if (!is_array($business['hours'])) $business['hours'] = array();

?>

<style type="text/css">

    #tab-general .top_todo {
        margin: 0 0 15px 0;
    }
    #tab-general .top_todo .items {
        background-color: #f5f5f5;
    }
    #tab-general .top_todo .items .item {
        border-bottom: 1px solid #ddd;
    }
    #tab-general .top_todo .items .item:last-child {
        border-bottom: 0px none;
    }
    #tab-general .top_todo .items .item .num {
        float: left;
        padding: 10px;
        font-weight: bold;
    }
    #tab-general .top_todo .items .item .title {
        float: left;
        padding: 10px;
    }
    #tab-general .top_todo .items .item .link {
        float: right;
        padding: 10px;
    }

    #tab-general .group.hours table {
        margin: 0px;
        padding: 0px;
        border: 0px none;
    }
    #tab-general .group.hours .day td {
        padding: 4px 5px 4px 0;
        white-space: nowrap;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // HOURS - CLOSED CLICK
        $('#tab-general .group.hours .day input.closed').click(function() {
            $(this).closest('.day').find('select').prop('disabled', $(this).prop('checked'));
        });

    });

</script>

<?php

$todos = array();

// GET BUSINESS FEATURES
$feata = $_uccms_businessdir->business_features($business['id']);

// SHOULD HAVE FEATURES
if (count($feata) > 0) {

    // GET FEATURES
    $fcheck_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_feature_values']. "` WHERE (`business_id`=" .$business['id']. ")";
    $fcheck_q = sqlquery($fcheck_query);
    if (sqlrows($fcheck_q) == 0) {
        $todos[] = array(
            'title' => 'Features',
            'tab'   => 'features',
        );
    }

}

// GET IMAGES
$icheck_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_images']. "` WHERE (`business_id`=" .$business['id']. ")";
$icheck_q = sqlquery($icheck_query);
if (sqlrows($icheck_q) == 0) {
    $todos[] = array(
        'title' => 'Images',
        'tab'   => 'images',
    );
}

// GET VIDEOS
$vcheck_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_videos']. "` WHERE (`business_id`=" .$business['id']. ")";
$vcheck_q = sqlquery($vcheck_query);
if (sqlrows($vcheck_q) == 0) {
    $todos[] = array(
        'title' => 'Videos',
        'tab'   => 'videos',
    );
}

// HAVE TODOS
if (count($todos) > 0) {

    $ti = 1;

    ?>

    <div class="top_todo">

        <h3>Todo</h3>

        <div class="items">

            <?php

            // LOOP
            foreach ($todos as $todo) {

                ?>
                <div class="item clearfix">
                    <div class="num"><?php echo $ti; ?></div>
                    <div class="title"><?php echo $todo['title']; ?></div>
                    <div class="link"><a href="./?id=<?php echo $business['id']; ?>&tab=<?php echo $todo['tab']; ?>">Fix</a></div>
                </div>
                <?php

                $ti++;

            }

            ?>

        </div>

    </div>

    <?php

}

?>

<div class="fields">

    <div class="group">

        <fieldset>
            <label>Business Name <span class="required">*</span></label>
            <input type="text" value="<?php echo stripslashes($business['name']); ?>" name="business[name]" />
        </fieldset>

        <div class="clearfix">

            <div class="half left">

                <fieldset>
                    <label>Department</label>
                    <input type="text" value="<?php echo stripslashes($business['department']); ?>" name="business[department]" />
                </fieldset>

            </div>

            <div class="half right">

                <fieldset>
                    <label>Owner Name</label>
                    <input type="text" value="<?php echo stripslashes($business['name_owner']); ?>" name="business[name_owner]" />
                </fieldset>

            </div>

        </div>

    </div>

    <div class="group">

        <fieldset>
            <label>Logo</label>
            <div>
                <?php
                $field = array(
                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                    'key'       => 'logo', // The value you should use for the "name" attribute of your form field
                    'value'     => ($business['logo'] ? $_uccms_businessdir->imageBridgeOut($business['logo'], 'businesses') : ''), // The existing value for this form field
                    'id'        => 'blogo', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'image' => true
                    ),
                    'required'  => false // A boolean value of whether this form field is required or not
                );
                include(BigTree::path('admin/form-field-types/draw/upload.php'));
                ?>
            </div>
        </fieldset>

    </div>

    <div class="group">

        <fieldset>
            <label>Description</label>
            <textarea name="business[description]"><?=$business['description']?></textarea>
        </fieldset>

        <fieldset>
            <label>Tagline</label>
            <input type="text" value="<?=$business['tagline']?>" name="business[tagline]" />
        </fieldset>

        <fieldset>
            <label>Keywords <small>Separate by comma.</small></label>
            <textarea name="business[keywords]"><?=$business['keywords']?></textarea>
        </fieldset>

    </div>

    <div class="group hours">

        <h4>Hours</h4>

        <table>

            <?php

            // LOOP THROUGH DAYS OF WEEK
            foreach ($_uccms_businessdir->daysOfWeek() as $day_code => $day_name) {

                // THIS DAY
                $this_day = $business['hours'][$day_code];

                // CLOSED OR NOT
                $closed = $this_day['closed'];

                ?>
                <tr class="day">
                    <td class="name"><?=$day_name?></td>
                    <td class="open_close">
                        <select name="business[hours][<?=$day_code?>][open]" <?php if ($closed) { ?>disabled="disabled"<?php } ?>class="custom_control">
                            <?php
                            for ($i=1; $i<=24; $i++) {
                                for ($j=0; $j<=45; $j+=15) {
                                    if ($closed) {
                                        $selected = false;
                                    } else {
                                        if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['open']) {
                                            $selected = true;
                                        } else {
                                            $selected = false;
                                        }
                                    }
                                    ?>
                                    <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        &nbsp;-&nbsp;
                        <select name="business[hours][<?=$day_code?>][close]" <?php if ($closed) { ?>disabled="disabled"<?php } ?> class="custom_control">
                            <?php
                            for ($i=1; $i<=24; $i++) {
                                for ($j=0; $j<=45; $j+=15) {
                                    if ($closed) {
                                        $selected = false;
                                    } else {
                                        if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['close']) {
                                            $selected = true;
                                        } else {
                                            $selected = false;
                                        }
                                    }
                                    ?>
                                    <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td style="width: 100%; padding-top: 3px;"><input type="checkbox" name="business[hours][<?=$day_code?>][closed]" value="1" <?php if ($closed) { ?>checked="checked"<?php } ?>class="closed" /> Closed</td>
                </tr>

                <?php

            }

            ?>

        </table>

    </div>

    <div class="group">

        <fieldset>
            <label>Year Opened</label>
            <select name="business[year_opened]">
                <?php for ($i=date('Y'); $i>=1700; $i--) { ?>
                    <option value="<?=$i?>" <?php if ($i == $business['year_opened']) { ?>selected="selected"<?php } ?>><?=$i?></option>
                <?php } ?>
            </select>
        </fieldset>

        <fieldset>
            <label>Payment Methods Accepted</label>
            <div class="checkboxes clearfix">
                <?php foreach ($_uccms_businessdir->paymentMethods() as $pmid => $name) { ?>
                    <div class="checkbox_item">
                        <input type="checkbox" name="business[payment_methods][<?=$pmid?>]" value="<?=$pmid?>" <?php if (in_array($pmid, $pma)) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                    </div>
                <?php } ?>
            </div>
        </fieldset>

        <fieldset>
            <label>Languages Spoken</label>
            <div class="checkboxes clearfix">
                <?php foreach ($_uccms_businessdir->languages() as $lid => $name) { ?>
                    <div class="checkbox_item">
                        <input type="checkbox" name="business[languages][<?=$lid?>]" value="<?=$lid?>" <?php if (in_array($lid, $la)) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                    </div>
                <?php } ?>
            </div>
        </fieldset>

    </div>

</div>