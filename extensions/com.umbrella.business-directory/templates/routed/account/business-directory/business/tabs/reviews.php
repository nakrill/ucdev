<?php

require_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

// VARS FOR REVIEWS
$reviews_vars = array(
    'source'    => $_uccms_businessdir->Extension,
    'what'      => 'business',
    'item_id'   => ($business['pending'] ? $business['pending'] : $business['id'])
);

// IS OWNER
$_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'reply', true);

?>

<style type="text/css">

    #myAccount .section.business .tabs1 .uccms_ratings-reviews .pages.top {
        display: block;
        margin-bottom: 10px;
        text-align: right;
    }

    #myAccount .section.business .tabs1 .uccms_ratings-reviews .content {
        padding: 0px;
    }

</style>

<div class="section reviews uccms_ratings-reviews">
    <div class="results">
        <?php include(SERVER_ROOT. 'templates/ajax/ratings-reviews/paged-results-admin.php'); ?>
    </div>
</div>