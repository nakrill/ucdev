<?php

$tab = '';

// TABS
$taba = array(
    'general'       => 'General',
    'contact'       => 'Contact',
    'categories'    => 'Categories',
    'features'      => 'Features',
    'images'        => 'Images',
    'videos'        => 'Videos'
);

// GET BUSINESS FEATURES
$feata = $_uccms_businessdir->business_features($business['id']);

// NO FEATURES
if (count($feata) == 0) {
    unset($taba['features']);
}

// CATEGORY LIMIT
$category_limit = $_uccms_businessdir->getSetting('listing_max_categories');

// CAN'T DO CATEGORIES
if ($category_limit === '0') {
    unset($taba['categories']);
}

// IMAGE LIMIT
$image_limit = $_uccms_businessdir->getSetting('listing_max_images');

// CAN'T DO IMAGES
if ($image_limit === '0') {
    unset($taba['images']);
}

// VIDEO LIMIT
$video_limit = $_uccms_businessdir->getSetting('listing_max_videos');

// CAN'T DO VIDEOS
if ($video_limit === '0') {
    unset($taba['videos']);
}

// RATINGS / REVIEWS ENABLED
if ($_uccms_businessdir->getSetting('ratings_reviews_enabled')) {
    $taba['reviews'] = 'Reviews';
}

if ($taba[$_REQUEST['tab']]) {
    $tab = $_REQUEST['tab'];
} else {
    $tab = 'general';
}

// NEXT
$tab_keys = array_keys($taba);
$tab_position = array_search($tab, $tab_keys);
if (isset($tab_keys[$tab_position + 1])) {
    $tab_next = $tab_keys[$tab_position + 1];
}

?>