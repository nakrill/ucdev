<?php

BigTree::redirect('../listings/');
exit;

// BUSINESS ARRAY
$busa = array();

// GET BUSINESSES
$business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`account_id`=" .$_uccms['_account']->userID(). ") AND (`status`!=9) ORDER BY `created_dt` DESC";
$business_q = sqlquery($business_query);
while ($business = sqlfetch($business_q)) {
    $busa[$business['id']] = $business;
}

unset($business);

// HAVE BUSINESSES
if (count($busa) > 0) {
    foreach ($busa as $tbus) {
        if ($tbus['pending']) {
            unset($busa[$tbus['pending']]);
        }
    }
}

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/master/my-listings.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/custom/my-listings.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/master/my-listings.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/custom/my-listings.js"></script>

<h3 style="margin-bottom: 5px;">My Businesses</h3>

<?php

// HAVE BUSINESSES
if (count($busa) > 0 ) {

    ?>

    <table id="listings_list" width="100%" border="0">

        <?php

        // LOOP
        foreach ($busa as $business) {

            ?>
            <tr>
                <td>
                    <div style="width: 100px; padding: 10px; overflow: hidden; text-align: center;">
                        <?php if ($business['logo']) { ?>
                            <img src="<?php echo $_uccms_businessdir->imageBridgeOut($business['logo'], 'businesses'); ?>" alt="" style="width: 100%; height: auto;" />
                        <?php } ?>
                    </div>
                </td>
                <td style="width: 50%; padding: 15px;">
                    <div style="font-size: 1.2em; font-weight: bold;">
                        <?=$business['name']?>
                    </div>
                    <div>
                        <?=$business['address1']?>
                    </div>
                    <div>
                        <?=$business['city']?>, <?=$business['state']?> <?=$business['zip']?>
                    </div>
                </td>
                <td style="width: 50%; padding: 15px;">
                    <div style="font-size: 1.2em;">
                        <?=$_uccms_businessdir->prettyPhone($business['phone'])?>
                    </div>
                    <?php if ($business['phone_tollfree']) { ?>
                        <div>
                            <?=$_uccms_businessdir->prettyPhone($business['phone_tollfree'])?>
                        </div>
                    <?php } ?>
                    <?php if ($business['url']) { ?>
                        <div>
                            <a href="<?=$business['url']?>" target="_blank"><?=$business['url']?></a>
                        </div>
                    <?php } ?>
                </td>
                <td style="<?php if ($business['pending']) { ?>padding: 5px 15px 5px 11px;<?php } else { ?>padding: 5px 15px 0 15px;<?php } ?> text-align: right; white-space: nowrap;">
                    <? /*<a href="/business-listing/review/?id=<?=$listing['id']?>" class="button"><i class="fa fa-eye"></i> View Listing</a>*/ ?>
                    <?php if ($business['pending']) { ?>
                        <div class="pending">Pending</div>
                    <?php } ?>
                    <a href="./edit/?id=<?=$business['id']?>&clear=true" class="button"><i class="fa fa-pencil"></i> Edit</a>
                </td>
            </tr>
            <?php

        }

        ?>

    </table>

    <?php

// NO BUSINESSES
} else {
    ?>
    <div style="margin-top: 10px;">
        It doesn't look like you have any listings yet. <a href="/business-listing/">Add your business.</a>
    </div>
    <?php
}

?>