<link rel="stylesheet" type="text/css" href="/css/lib/ratings-reviews/review-admin.css" />
<script src="/js/lib/ratings-reviews/review-admin.js"></script>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/master/reviews.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/account/custom/reviews.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/master/reviews.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/account/custom/reviews.js"></script>

<?php

// PAGING CLASS
require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

// INIT PAGING CLASS
$_paging = new paging();

// OPTIONAL - URL VARIABLES TO IGNORE
$_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

// HOW MANY PER PAGE
$limit = 15;

// CALCULATE OFFSET
if ($_GET['page']) {
    $offset = ((int)$_GET['page'] - 1) * $limit;
} else {
    $offset = 0;
}

// PREPARE PAGING
$_paging->prepare($limit, 10, $_GET['page'], $_GET);

// MAIN QUERY
$query_main = "
FROM `uccms_ratings_reviews` AS `rr`
INNER JOIN `" .$_uccms_businessdir->tables['businesses']. "` AS `b` ON rr.item_id=b.id
WHERE (b.account_id=" .$_uccms['_account']->userID(). ") AND (rr.reply_to_id=0) AND (rr.status=1) AND (rr.source='" .$_uccms_businessdir->Extension. "') AND (rr.what='business') AND (b.status!=9)
GROUP BY rr.id
";

// GET PAGED REVIEWS
$reviews_query = "
SELECT rr.*
" .$query_main. "
ORDER BY `dt_created` DESC
LIMIT " .$offset. ", " .$limit. "
";
$reviews_q = sqlquery($reviews_query);

// NUMBER OF REVIEWS ON THIS PAGE
$num_reviews = sqlrows($reviews_q);

// GET ALL MATCHING REVIEWS
$reviews_total_query = "
SELECT rr.id
" .$query_main. "
";
$reviews_total_q = sqlquery($reviews_total_query);

// NUMBER OF REVIEWS ON THIS PAGE
$num_reviews_total = sqlrows($reviews_total_q);

// GENERATE PAGING
$pages = $_paging->output($num_reviews, $num_reviews_total);

?>

<div class="section reviews uccms_ratings-reviews">

    <div class="heading-main clearfix">
        <h4>Reviews</h4>
        <?php echo $pages; ?>
    </div>

    <?php

    // HAVE REVIEWS
    if ($num_reviews > 0) {

        // REVIEW SETTINGS
        $review_admin = array(
            'source'        => $_uccms_businessdir->Extension,
            'what'          => 'property',
            'hide_replies'  => false
        );

        ?>

        <div class="results">
            <div class="reviews items clearfix">
                <?php

                // LOOP
                while ($review = sqlfetch($reviews_q)) {

                    // IS OWNER
                    $_uccms_ratings_reviews->setReviewAccessSession($review, 'reply', true);

                    // DISPLAY
                    include(SERVER_ROOT. 'templates/elements/ratings-reviews/review-admin.php');

                }

                ?>
            </div>
        </div>

        <div class="pages bottom">
            <?php echo $pages; ?>
        </div>

        <?php

    // NO PROPERTIES
    } else {
        ?>
        <div class="no_reviews">
            <?php if ($num_reviews_total > 0) { ?>
                No reviews on this page.
            <?php } else { ?>
                It doesn't look like you have any reviews yet.
            <?php } ?>
        </div>
        <?php
    }

    ?>

</div>