<?php

// HAVE FEATURE ID
if ($feature['id']) {

    // FIELD VALUES ARRAY
    $fva = array();

    // ALREADY SELECTED
    if (is_array($field_values)) {
        $fva = $field_values;
    } else {
        $fva[] = $field_values;
    }

    ?>

    <fieldset id="ff-<?php echo $key; ?>" class="feature <?php echo $feature['type']; ?>">

        <?php

        // HAVE TITLE
        if ($feature['title']) {
            ?>
            <label class="title"><?php echo stripslashes($feature['title']); ?><?php if ($feature['required']) { ?><span class="reqd">*</span><?php } ?></label>
            <?php
        }

        // HAVE DESCRIPTION
        if ($feature['description']) {
            ?>
            <label class="description"><?php echo stripslashes($feature['description']); ?></label>
            <?php
        }

        // HAVE LIMIT
        if ($feature['limit'] > 0) {
            ?>
            <label class="limit">Limit: <?php echo number_format($feature['limit'], 0); ?></label>
            <?php
        }

        ?>

        <div class="options">
            <?php include(dirname(__FILE__). '/types/' .stripslashes($feature['type']). '.php'); ?>
        </div>

    </fieldset>

    <?php

}

?>