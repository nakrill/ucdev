<?php

// FEATURE USE COUNT ARRAY
$fuca = array();

// LOOP
foreach ($feata as $key => $feature) {

    unset($field_values);

    // HAVE VALUE(S)
    if (is_array($feature['value'])) {
        if (!$fuca[$feature['id']]) $fuca[$feature['id']] = 0;
        $field_values = $feature['value']['value'];

    // USE DEFAULTS
    } else {

        // FEATURE HAS OPTIONS
        if ($feature['options']) {
            foreach ($feature['options'] as $option) {
                if ($option['default']) {
                    $field_values = $option['id'];
                }
            }
        }

    }

    $field_name = 'feature[' .$feature['id']. '][]';

    // FEATURE ELEMENT
    include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/features/edit/base.php');

    $fuca[$feature['id']]++;

}

?>