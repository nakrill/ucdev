<div class="options">

    <?php

    // HAVE OPTIONS
    if ((is_array($feature['options'])) && (count($feature['options']) > 0)) {

        // LOOP THROUGH OPTIONS
        foreach ($feature['options'] as $option) {
            ?>
            <div class="option clearfix"><input type="checkbox" name="<?php echo $field_name; ?>" value="<?php echo $option['id']; ?>" <?php if (in_array($option['id'], $fva)) { ?>checked="checked"<?php } ?> />&nbsp;<?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?><?php if ($option['markup']) { ?> (+$<?php echo number_format(stripslashes($option['markup']), 2); ?>)<?php } ?></div>
            <?php
        }

    }

    ?>

</div>