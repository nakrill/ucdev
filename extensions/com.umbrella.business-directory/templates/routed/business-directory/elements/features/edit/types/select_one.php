<div class="options">

    <select name="<?php echo $field_name; ?>">
        <option value="0">Select</option>
        <?php

        // HAVE OPTIONS
        if ((is_array($feature['options'])) && (count($feature['options']) > 0)) {

            // LOOP
            foreach ($feature['options'] as $option) {
                ?>
                <option value="<?php echo $option['id']; ?>" <?php if (in_array($option['id'], $fva)) { ?>selected="selected"<?php } ?>><?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?><?php if ($option['markup']) { ?> (+$<?php echo number_format(stripslashes($option['markup']), 2); ?>)<?php } ?></option>
                <?php
            }

        }

        ?>
    </select>

</div>