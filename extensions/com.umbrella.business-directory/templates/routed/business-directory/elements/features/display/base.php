<?php

// HAVE FEATURE ID
if ($feature['id']) {

    ?>

    <div id="bf-<?php echo $feature['id']; ?>" class="feature <?php echo $feature['type']; ?> clearfix">

        <?php

        // HAVE TITLE
        if ($feature['title']) {
            ?>
            <div class="title"><?php echo stripslashes($feature['title']); ?></div>
            <?php
        }

        // HAVE DESCRIPTION
        if ($feature['description']) {
            ?>
            <div class="description"><?php echo stripslashes($feature['description']); ?></div>
            <?php
        }

        ?>

        <div class="values">
            <?php include(dirname(__FILE__). '/types/' .stripslashes($feature['type']). '.php'); ?>
        </div>

    </div>

    <?php

}

?>