<?php

if (!$ecomm['settings']['home_hide_categories']) {

    // GET MASTER CATEGORIES
    $cats = $_uccms_businessdir->subCategories(0, true, true);

    // HAVE MASTER CATEGORIES
    if (count($cats) > 0) {

        $cnuv = '';

        $cnga = $_GET;
        unset($cnga['bigtree_htaccess_url']);
        unset($cnga['category']);
        if (count($cnga) > 0) {
            $cnuv .= '?' .http_build_query($cnga);
        }
        unset($cnga);

        ?>

        <div class="category_nav">
            <h3>Categories</h3>
            <ul>
                <?php foreach ($cats as $cat) { ?>
                    <li class="<?php if ($cat['id'] == $category_master_parent_id) { ?>active<?php } ?>">
                        <a href="<?php echo $_uccms_businessdir->categoryURL($cat['id'], $cat) . $cnuv; ?>"><?php echo stripslashes($cat['title']); ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <?php

    }

}

?>