<?php

// LIMIT
$recent_limit = ($businessdir['settings']['home_recent_num'] ? (int)$businessdir['settings']['home_recent_num'] : 3);

// GET RECENT LISTINGS
$recent_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`status`=1) AND (`not_recent`=0) ORDER BY `id` ASC LIMIT " .$recent_limit;
$recent_q = sqlquery($recent_query);

// HAVE RECENT LISTINGS
if (sqlrows($recent_q) > 0) {

    // HEADING
    if (!$businessdir['settings']['home_recent_title']) $businessdir['settings']['home_recent_title'] = 'Recent Businesses';

    ?>

    <div class="recent">

        <h3><?php echo stripslashes($businessdir['settings']['home_recent_title']); ?></h3>

        <div class="item_container contain">

            <?php

            // LOOP
            while ($business = sqlfetch($recent_q)) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/listing.php');
            }

            ?>

        </div>

    </div>

    <?php

}

?>