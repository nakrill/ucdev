<?php

// HAVE BUSINESS ID
if ($business['id']) {

    $url = $_uccms_businessdir->businessURL($business['id'], (int)$business['category_id'], $business);

    // GET IMAGE
    $image_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_images']. "` WHERE (`business_id`=" .$business['id']. ") ORDER BY `sort` ASC LIMIT 1";
    $image = sqlfetch(sqlquery($image_query));

    // HAVE IMAGE
    if ($image['image']) {
        $image_url = BigTree::prefixFile($_uccms_businessdir->imageBridgeOut($image['image'], 'businesses'), 't_');
    } else {
        $image_url = WWW_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/images/business_no-image.jpg';
    }

    ?>

    <div class="item col-1-4 serif">
        <div class="image" style="background-image: url('<?php echo $image_url; ?>');">
            <a href="<?php echo $url; ?>"></a>
        </div>
        <div class="title">
            <a href="<?php echo $url; ?>"><?php echo stripslashes($business['name']); ?></a>
        </div>

        <?php if ($_uccms_businessdir->getSetting('business_ratings_reviews_enabled')) { ?>
            <div class="rating_stars">
                <?php
                if ($business['rating'] > 0) {
                    ?>
                    <div class="stars" title="<?php echo $business['rating']; ?> from <?php echo number_format($business['reviews'], 0); ?> reviews">
                        <?php
                        $star_vars = array(
                            'el_id'     => 'business-stars_' .$business['id']. '-' .rand(1111,9999),
                            'value'     => $business['rating'],
                            'readonly'  => true
                        );
                        include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        <?php } ?>

    </div>

    <?php

}

?>