<?php

// SEARCH NOT DISABLED
if (!$_uccms_businessdir->getSetting('search_disabled')) {

    // SMALL COLUMN VARIABLE ARRAY
    $scva = $_GET;

    // REQUIRED $scva ARRAY
    $rga = array(
        'city_id',
        'neighborhood_id',
    );
    foreach ($rga as $rga_name) {
        if (!is_array($scva[$rga_name])) $scva[$rga_name] = array();
    }

    ?>
    <div class="search_filter">
        <?php if (count($_GET) > 2) { ?><span class="clear"><a href="./">Clear</a></span><?php } ?>
        <h3>Search & Filter</h3>

        <form action="./" method="get" class="search">

            <div class="group text keyword">
                <input type="text" name="q" placeholder="Keyword" value="<?php echo $scva['q']; ?>" />
            </div>

            <?php
            $cities_query = "
            SELECT c.*
            FROM `" .$_uccms_businessdir->tables['cities']. "` AS `c`
            ORDER BY c.title ASC, c.id ASC
            ";
            $cities_q = sqlquery($cities_query);
            if (sqlrows($cities_q) > 0) {
                ?>
                <div class="group checkboxes city_id">
                    <div class="title">City</div>
                    <div class="options">
                        <?php while ($tcity = sqlfetch($cities_q)) { ?>
                            <div class="option">
                                <input type="checkbox" name="city_id[]" value="<?php echo $tcity['id']; ?>" <?php if (in_array($tcity['id'], $scva['city_id'])) { ?>checked="checked"<?php } ?>><?php echo stripslashes($tcity['title']); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php
            $neighborhoods_query = "
            SELECT n.*
            FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` AS `n`
            ORDER BY n.title ASC, n.id ASC
            ";
            $neighborhoods_q = sqlquery($neighborhoods_query);
            if (sqlrows($neighborhoods_q) > 0) {
                ?>
                <div class="group checkboxes neighborhood_id">
                    <div class="title">Neighborhood</div>
                    <div class="options">
                        <?php while ($thood = sqlfetch($neighborhoods_q)) { ?>
                            <div class="option">
                                <input type="checkbox" name="neighborhood_id[]" value="<?php echo $thood['id']; ?>" <?php if (in_array($thood['id'], $scva['neighborhood_id'])) { ?>checked="checked"<?php } ?>><?php echo stripslashes($thood['title']); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="group rating_stars">
                <div class="title">Min Rating</div>
                <div class="stars">
                    <?php
                    $star_vars = array(
                        'el_name'       => 'rating_min',
                        'value'         => (int)$scva['rating_min'],
                        'allow_empty'   => true
                    );
                    include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                    ?>
                </div>
            </div>

            <?php

            // FEATURES
            $feata = array();

            // GET FEATURES
            $feature_query = "SELECT * FROM `" .$_uccms_businessdir->tables['features']. "` WHERE (`active`=1) AND (`search_hidden`=0) ORDER BY `sort` ASC, `title` ASC, `id` DESC";
            $feature_q = sqlquery($feature_query);
            while ($feature = sqlfetch($feature_q)) {
                if (($feature['type'] == 'select_one') || ($feature['type'] == 'select_multi')) {
                    $options_query = "SELECT * FROM `" .$_uccms_businessdir->tables['feature_options']. "` WHERE (`feature_id`=" .$feature['id']. ") ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                    $options_q = sqlquery($options_query);
                    while ($option = sqlfetch($options_q)) {
                        $feature['options'][$option['id']] = $option;
                    }
                }
                $feature['value']['value'] = $scva['feature'][$feature['id']];
                $feata[$feature['id']] = $feature;
            }

            // HAVE FEATURES
            if (count($feata) > 0) {
                ?>
                <div class="features">
                    <?php
                    include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/features/edit/main.php');
                    ?>
                </div>
                <?php
            }

            ?>

            <div class="submit">
                <input type="submit" value="Find" />
            </div>

        </form>

    </div>

    <?php

    unset($scva);

}

?>