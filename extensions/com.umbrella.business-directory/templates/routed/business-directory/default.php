<?php

// MODULE CLASS
$_uccms_businessdir = new uccms_BusinessDirectory;

// GET SETTING(S)
$businessdir['settings'] = $_uccms_businessdir->getSettings();

// NUMBER OF LEVELS IN URL PATH
$path_num = count($bigtree['path']);

// RATINGS / REVIEWS
include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/master/master.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/custom/master.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/master/master.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/custom/master.js"></script>

<div class="site-width block_center">
    <div class="page">

        <div id="businessdirContainer" class="clearfix">

            <?php

            // SECTIONS DIRECTORY
            $sections_dir = dirname(__FILE__). '/sections';

            // DATA DIRECTORY
            $data_dir = dirname(__FILE__). '/data';

            // HOME
            if ($path_num == 1) {
                include($data_dir. '/home.php');
                include($sections_dir. '/home/default.php');

            // ONE LEVEL DEEP
            } else if ($path_num == 2) {

                // SEARCH SECTION
                if ($bigtree['path'][1] == 'search') {
                    include($data_dir. '/search.php');
                    //include($sections_dir. '/search/default.php');

                // SUBMIT SECTION
                } else if ($bigtree['path'][1] == 'submit') {
                    //include($data_dir. '/submit.php');
                    include($sections_dir. '/submit/default.php');

                } else {

                    // GET LAST PATH PARTS
                    $ppa = explode('-', $bigtree['path'][1]);

                    // HAS NUMBER ON END - IS CATEGORY
                    if (is_numeric($ppa[count($ppa) - 1])) {
                        $category_id = $ppa[count($ppa) - 1];
                        include($data_dir. '/home.php');
                        include($sections_dir. '/home/default.php');

                    // CITY
                    } else {
                        include($data_dir. '/city-neighborhood.php');
                        include($sections_dir. '/city-neighborhood/default.php');
                    }

                }

            // TWO LEVELS DEEP
            } else if ($path_num == 3) {

                // GET LAST PATH PARTS
                $ppa = explode('-', $bigtree['path'][2]);

                // HAS NUMBER ON END - IS ITEM
                if (is_numeric($ppa[count($ppa) - 1])) {
                    $business_id = $ppa[count($ppa) - 1];
                    include($data_dir. '/business.php');
                    include($sections_dir. '/business/default.php');

                // SUBMIT SECTION
                } else if ($bigtree['path'][1] == 'submit') {

                    // REVIEW
                    if ($bigtree['path'][2] == 'review') {
                        include($data_dir. '/submit/review.php');
                        include($sections_dir. '/submit/review.php');

                    // PROCESS
                    } else if ($bigtree['path'][2] == 'process') {
                        include($sections_dir. '/submit/process.php');
                    }

                // NEIGHBORHOOD
                } else {
                    include($data_dir. '/city-neighborhood.php');
                    include($sections_dir. '/city-neighborhood/default.php');
                }

            }

            ?>

        </div>

    </div>
</div>