<?php

unset($_GET['bigtree_htaccess_url']);

$url = '/' .$_uccms_businessdir->frontendPath(). '/';

if (count($_GET) > 0) {
    $url .= '?' .http_build_query($_GET);
}

header('Location: ' .$url, true, 301);
exit;

// SEARCH NOT DISABLED
if (!$_uccms_businessdir->getSetting('search_disabled')) {

    // PAGE LIMIT
    $page_limit = $_uccms_businessdir->perPage();

    // OFFSET
    if ($_GET['page']) {
        $offset = ((int)$_GET['page'] - 1) * $page_limit;
    } else {
        $offset = 0;
    }

    // SORT
    if ($_REQUEST['sort']) {
        list($order_field, $order_direction) = explode('.', $_REQUEST['sort']);
    }

    //$_GET['feature'][3] = 'acme'; // SINGLE VALUE

    // MULTIPLE VALUES
    //$_GET['feature'][5] = array(2,4);

    // MIN / MAX
    //$_GET['feature'][1]['min'] = 20;
    //$_GET['feature'][1]['max'] = 40;

    // VARIABLES TO FIND BY
    $business_vars = array(
        'vars'              => $_GET,
        'order_field'       => $order_field,
        'order_direction'   => $order_direction,
        'limit'             => $page_limit,
        'offset'            => $offset,
        'log'               => true
    );

    //echo print_r($business_vars);
    //echo '<br /><br />';

    // GET MATCHING BUSINESSES
    $results = $_uccms_businessdir->getBusinesses($business_vars);

    // LISTINGS
    $listings = $results['results'];

    // NUMBER OF LISTINGS
    $num_listings = count($results['num_results']);

    // HAVE RESULTS
    if ($num_listings > 0) {

        // PAGING CLASS
        require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

        // INIT PAGING CLASS
        $_paging = new paging();

        // OPTIONAL - URL VARIABLES TO IGNORE
        $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

        // PREPARE PAGING
        $_paging->prepare($page_limit, 10, $_GET['page'], $_GET);

        // GENERATE PAGING
        $pages = $_paging->output($results['num_results'], $results['num_total']);

    }

}

?>