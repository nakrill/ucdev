<?php

// IS CITY
if ($path_num == 2) {

    // GET CITY INFO
    $city_query = "SELECT * FROM `" .$_uccms_businessdir->tables['cities']. "` WHERE (`slug`='" .stripslashes($bigtree['path'][1]). "')";
    $city_q = sqlquery($city_query);
    $city = sqlfetch($city_q);

    // META
    if ($city['meta_title']) $bigtree['page']['title'] = stripslashes($city['meta_title']);
    if ($city['meta_description']) $bigtree['page']['meta_description'] = stripslashes($city['meta_description']);
    if ($city['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($city['meta_keywords']);

// IS NEIGHBORHOOD
} else if ($path_num == 3) {

    // GET NEIGHBORHOOD INFO
    $neighborhood_query = "SELECT * FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` WHERE (`slug`='" .stripslashes($bigtree['path'][2]). "')";
    $neighborhood_q = sqlquery($neighborhood_query);
    $neighborhood = sqlfetch($neighborhood_q);

    // HAVE CITY ID
    if ($neighborhood['city_id']) {

        // META
        if ($neighborhood['meta_title']) $bigtree['page']['title'] = stripslashes($neighborhood['meta_title']);
        if ($neighborhood['meta_description']) $bigtree['page']['meta_description'] = stripslashes($neighborhood['meta_description']);
        if ($neighborhood['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($neighborhood['meta_keywords']);

        // GET CITY
        $city_query = "SELECT * FROM `" .$_uccms_businessdir->tables['cities']. "` WHERE (`id`=" .$neighborhood['city_id']. ")";
        $city_q = sqlquery($city_query);
        $city = sqlfetch($city_q);

    }

}

// PAGING CLASS
require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

// INIT PAGING CLASS
$_paging = new paging();

// OPTIONAL - URL VARIABLES TO IGNORE
$_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url,paging,limit';

// USE PAGING
$_REQUEST['paging'] = true;
//$_REQUEST['limit'] = 16;

$search_vars = array(
    'paging'    => true,
    'page'      => $_GET['page']
);

if ($neighborhood['id']) {
    $search_vars['neighborhood_id'] = $neighborhood['id'];
} else if ($city['id']) {
    $search_vars['city_id'] = $city['id'];
}

// GET RESULTS
$results = $_uccms_businessdir->doSearch($search_vars);

// LISTINGS
$listings = $results['results'];

// NUMBER OF PAGED LISTINGS
$num_listings = count($listings);

// PREPARE PAGING (RESULTS PER PAGE, NUMBER OF PAGE NUMBERS, CURRENT PAGE, REQUEST VARIABLES (_GET, _POST), USE MOD_REWRITE)
$_paging->prepare($results['limit'], 5, $results['page'], $_GET, false);

// GENERATE PAGING
$pages = $_paging->output($num_listings, $results['total']);

?>