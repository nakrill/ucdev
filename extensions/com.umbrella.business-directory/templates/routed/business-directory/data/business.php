<?php

// https://developers.google.com/structured-data/rich-snippets/products?hl=en&rd=1
// https://developers.google.com/structured-data/testing-tool/

// CLEAN UP
$business_id = (int)$business_id;

// HAVE BUSINESS ID
if ($business_id > 0) {

    // GET BUSINESS
    $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$business_id. ") AND (`status`=1)";
    $business_q = sqlquery($business_query);
    $business = sqlfetch($business_q);

}

// HAVE BUSINESS
if ($business['id']) {

    // TRACK VIEW
    $_SESSION['uccmsAcct_webTracking']['events']['business-view'] = trim(stripslashes($business['name']). ' (#' .$business['id']. ')');

    // LOG VIEW STAT
    $_uccms_businessdir->business_logStat($business['id'], 'view');

    // CUSTOM EDIT LINK
    $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_businessdir->Extension. '*business-directory/businesses/edit/?id=' .$business['id'];

    // BUSINESS SLUG
    if ($business['slug']) {
        $slug = stripslashes($business['slug']);
    } else {
        $slug = $_uccms_businessdir->makeRewrite(stripslashes($business['title']));
    }

    // SLUGS DON'T MATCH - 301 TO CURRENT
    if ($bigtree['path'][2] != $slug. '-' .$business['id']) {

        $catgory_id = 0;

        // IS IN CATEGORY
        if ($bigtree['path'][1] != 'business') {

            // GET LAST PATH PARTS
            $ppa = explode('-', $bigtree['path'][1]);

            // HAS NUMBER ON END - IS CATEGORY
            if (is_numeric($ppa[count($ppa) - 1])) {
                $category_id = $ppa[count($ppa) - 1];
            }
        }

        // 301 REDIRECT TO CURRENT BUSINESS URL
        header('Location: ' .$_uccms_businessdir->businessURL($business['id'], $category_id, $business), true, 301);
        exit;

    }

    // META
    if ($business['meta_title']) {
        $bigtree['page']['title'] = stripslashes($business['meta_title']);
    } else {
        $bigtree['page']['title'] = stripslashes($business['name']);
    }
    if ($business['meta_description']) {
        $bigtree['page']['meta_description'] = stripslashes($business['meta_description']);
    } else if ($business['description']) {
        $bigtree['page']['meta_description'] = substrWord(stripslashes($business['description']), 160, '..');
    }
    if ($business['meta_keywords']) {
        $bigtree['page']['meta_keywords'] = stripslashes($business['meta_keywords']);
    } else if ($business['keywords']) {
        $bigtree['page']['meta_keywords'] = stripslashes($business['keywords']);
    }

    // CANONICAL URL
    $bigtree['page']['canonical'] = $_uccms_businessdir->businessURL($business['id'], 0, $business);

    // CATEGORY PARENT(S) ARRAY
    $catpa = array();

    // CATEGORY MASTER PARENT ID
    $category_master_parent_id = 0;

    // IS IN CATEGORY
    if ($bigtree['path'][1] != 'business') {

        // GET LAST PATH PARTS
        $ppa = explode('-', $bigtree['path'][1]);

        // HAS NUMBER ON END - IS CATEGORY
        if (is_numeric($ppa[count($ppa) - 1])) {
            $category_id = $ppa[count($ppa) - 1];
        }

        // CATEGORY PARENT(S) ARRAY
        $catpa = $_uccms_businessdir->getCategoryParents($category_id);

        // CATEGORY MASTER PARENT ID
        $category_master_parent_id = $catpa[count($catpa) - 1]['id'];

    }

    // IMAGE ARRAY
    $imagea = array();

    $ii = 0;

    // GET IMAGES
    $image_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_images']. "` WHERE (`business_id`=" .$business['id']. ") ORDER BY `sort` ASC";
    $image_q = sqlquery($image_query);
    while ($image = sqlfetch($image_q)) {
        if ($image['image']) {
            $imagea[$ii] = $image;
            $imagea[$ii]['url'] = $_uccms_businessdir->imageBridgeOut($image['image'], 'businesses');
            $imagea[$ii]['url_thumb'] = BigTree::prefixFile($_uccms_businessdir->imageBridgeOut($image['image'], 'businesses'), 't_');
            $ii++;
        }
    }

    // VIDEO ARRAY
    $videoa = array();

    $ii = 0;

    // GET VIDEOS
    $video_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_videos']. "` WHERE (`business_id`=" .$business['id']. ") ORDER BY `sort` ASC";
    $video_q = sqlquery($video_query);
    while ($video = sqlfetch($video_q)) {
        if ($video['video']) {
            $videoa[$ii] = $video;
            $videoa[$ii]['info'] = videoInfo(stripslashes($video['video']));
            $videoa[$ii]['url'] = stripslashes($video['video']);
            $videoa[$ii]['url_thumb'] = $videoa[$ii]['info']['thumb'];
            $ii++;
        }
    }

    // GET BUSINESS FEATURES
    $feata = $_uccms_businessdir->business_features($business['id']);

// BUSINESS NOT FOUND
} else {
    header('HTTP/1.0 404 Not Found');
    $bigtree['page']['title'] = 'Business Not Found';
    $bigtree['page']['meta_description'] = '';
    $bigtree['page']['meta_keywords'] = '';
}

?>