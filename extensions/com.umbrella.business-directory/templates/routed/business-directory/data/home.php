<?php

// CATEGORY SPECIFIED
if ($_REQUEST['category']) $category_id = $_REQUEST['category'];

// CLEAN UP
$category_id = (int)$category_id;

// HAVE CATEGORY ID
if ($category_id > 0) {

    // GET CATEGORY
    $category_query = "SELECT * FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`id`=" .$category_id. ") AND (`active`=1)";
    $category_q = sqlquery($category_query);
    $category = sqlfetch($category_q);

    // HAVE CATEGORY
    if ($category['id']) {

        $_GET['category'] = $category['id'];

        // CUSTOM EDIT LINK
        $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_businessdir->Extension. '*business-directory/categories/edit/?id=' .$category['id'];

        // CATEGORY SLUG
        if ($category['slug']) {
            $slug = stripslashes($category['slug']);
        } else {
            $slug = $_uccms_businessdir->makeRewrite(stripslashes($category['title']));
        }

        // SLUGS DON'T MATCH - 301 TO CURRENT
        if ($bigtree['path'][1] != $slug. '-' .$category['id']) {

            // 301 REDIRECT TO CURRENT CATEGORY URL
            header('Location: ' .$_uccms_businessdir->categoryURL($category['id'], $category), true, 301);
            exit;

        }

        // META
        if ($category['meta_title']) {
            $bigtree['page']['title'] = stripslashes($category['meta_title']);
        } else {
            $bigtree['page']['title'] = stripslashes($category['title']);
        }
        if ($category['meta_description']) {
            $bigtree['page']['meta_description'] = stripslashes($category['meta_description']);
        } else if ($category['description']) {
            $bigtree['page']['description'] = stripslashes($category['description']);
        }
        if ($category['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($category['meta_keywords']);

        // CANONICAL URL
        $bigtree['page']['canonical'] = $_uccms_businessdir->categoryURL($category['id'], $category);

        // CATEGORY PARENT(S) ARRAY
        $catpa = $_uccms_businessdir->getCategoryParents($category['id']);

        // CATEGORY MASTER PARENT ID
        $category_master_parent_id = $catpa[count($catpa) - 1]['id'];

        // GET SUB-CATEGORIES
        $sub_categories = $_uccms_businessdir->subCategories($category['id'], true, true);

    // CATEGORY NOT FOUND
    } else {
        header('HTTP/1.0 404 Not Found');
        $bigtree['page']['title'] = 'Category Not Found';
        $bigtree['page']['meta_description'] = '';
        $bigtree['page']['meta_keywords'] = '';
    }

}

// PAGE LIMIT
$page_limit = $_uccms_businessdir->perPage();

// OFFSET
if ($_GET['page']) {
    $offset = ((int)$_GET['page'] - 1) * $page_limit;
} else {
    $offset = 0;
}

// SORT
if ($_GET['sort']) {
    list($order_field, $order_direction) = explode('.', $_GET['sort']);
}

// VARIABLES TO FIND BY
$business_vars = array(
    'vars'              => $_GET,
    'order_field'       => $order_field,
    'order_direction'   => $order_direction,
    'limit'             => $page_limit,
    'offset'            => $offset
);

if (count($_GET) > 1) {
    $business_vars['log'] = true;
}

// GET MATCHING BUSINESSES
$businesses = $_uccms_businessdir->getBusinesses($business_vars);

// HAVE RESULTS
if (count($businesses['results']) > 0) {

    // PAGING CLASS
    require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

    // INIT PAGING CLASS
    $_paging = new paging();

    // OPTIONAL - URL VARIABLES TO IGNORE
    $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

    // PREPARE PAGING
    $_paging->prepare($page_limit, 10, $_GET['page'], $_GET);

    // GENERATE PAGING
    $pages = $_paging->output($businesses['num_results'], $businesses['num_total']);

}

?>