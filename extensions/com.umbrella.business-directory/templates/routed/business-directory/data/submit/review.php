<?php

// USER OWNS LISTING
$owns = false;

// PAYMENT IS DUE
$payment_due = true;

// INFO POSTED
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // USE POSTED BUSINESS INFO
    $business = $_POST['business'];

    // SAVE INFO TO SESSION
    $_SESSION['POST']['BUSINESS-DIRECTORY'] = $_POST;

// USE FROM SESSION
} else {
    $business = $_SESSION['POST']['BUSINESS-DIRECTORY']['business'];
}

// SUBMISSIONS ENABLED
if ($_uccms_businessdir->getSetting('submissions_enabled')) {

    // PAYMENT METHODS
    $pma = array();

    // LANGUAGES
    $la = array();

    // HAVE BUSINESS ID
    if ($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['id']) {

        // GET BUSINESS INFO
        $businessi_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .(int)$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['id']. ")";
        $businessi_q = sqlquery($businessi_query);
        $businessi = sqlfetch($businessi_q);

        // FOUND
        if ($businessi['id']) {

            // INFO NOT POSTED
            if ($_SERVER['REQUEST_METHOD'] != 'POST') {

                // USE INFO FROM DATABASE
                $business = $businessi;

                // SAVE DB INFO TO SESSION
                $_SESSION['POST']['BUSINESS-DIRECTORY']['business'] = $businessi;

                // GET HOURS
                $hours_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$business['id']. ")";
                $hours_q = sqlquery($hours_query);
                while ($hours = sqlfetch($hours_q)) {
                    $business['hours'][$hours['day']] = $hours;
                }
                $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['hours'] = $business['hours'];

                // PAYMENT METHODS
                if (!is_array($business['payment_methods'])) {
                     $tpma = explode(',', stripslashes($business['payment_methods']));
                     if (count($tpma) > 0) {
                         unset($business['payment_methods']);
                         foreach ($tpma as $pm) {
                            $business['payment_methods'][$pm] = $pm;
                         }
                         $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['payment_methods'] = $business['payment_methods'];
                     }
                     unset($tpma);
                }

                // LANGUAGES
                if (!is_array($business['languages'])) {
                     $tla = explode(',', stripslashes($business['languages']));
                     if (count($tla) > 0) {
                         unset($business['languages']);
                         foreach ($tla as $lc) {
                            $business['languages'][$lc] = $lc;
                         }
                         $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['languages'] = $business['languages'];
                     }
                     unset($tla);
                }

            }

            // BUSINESS HAS ACCOUNT ID
            if ($businessi['account_id'] > 0) {

                // USER IS LOGGED IN
                if ($_uccms['_account']->loggedIn()) {

                    // USER IS OWNER
                    if ($businessi['account_id'] == $_uccms['_account']->userID()) {

                        $owns = true;

                        // GET DATE LAST CHARGED
                        $charge_query = "SELECT * FROM `" .$_uccms_businessdir->tables['transaction_log']. "` WHERE (`business_id`=" .$businessi['id']. ") AND (`status`='charged') ORDER BY `dt` DESC LIMIT 1";
                        $charge_q = sqlquery($charge_query);
                        $charge = sqlfetch($charge_q);

                        // HAVE CHARGED DATE
                        if (($charge['dt']) && ($charge['dt'] != '0000-00-00 00:00:00')) {

                            // PAYING MONTHLY
                            if ($charge['term'] == 'mo') {
                                if (strtotime($charge['dt']) > strtotime('-1 Month')) {
                                    $payment_due = false;
                                }

                            // PAYING ANNUALLY
                            } else {
                                if (strtotime($charge['dt']) > strtotime('-1 Year')) {
                                    $payment_due = false;
                                }
                            }

                        }

                    // USER IS NOT OWNER
                    } else {
                        $_uccms['_site-message']->set('error', 'Listing is owned by someone else.');
                        BigTree::redirect('../');
                    }

                // USER IS NOT LOGGED IN
                } else {
                    $_uccms['_site-message']->set('error', 'Listing is owned by someone else.');
                    BigTree::redirect('../');
                }

            // NO ACCOUNT ID
            } else {

                // CLAIMING NOT ENABLED
                if (!$_uccms_businessdir->getSetting('claiming_enabled')) {
                    $_uccms['_site-message']->set('error', 'Claiming of business listing is disabled.');
                    BigTree::redirect('../');
                }

            }

        // NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Business listing not found.');
            BigTree::redirect('../');
        }

    }

// SUBMISSIONS NOT ENABLED
} else {
    $_uccms['_site-message']->set('error', 'Submitting business listing is disabled.');
    BigTree::redirect('../');
}

// GET MAXIMUM NUMBER OF CATEGORIES ALLOWED
$max_categories = (int)$_uccms_businessdir->getSetting('listing_max_categories');

// MAX CATEGORIES
if ($max_categories > 0) {

    $i = 1;

    $message = false;

    // LOOP
    if (is_array($_SESSION['POST']['BUSINESS-DIRECTORY']['category'])) {
        foreach ($_SESSION['POST']['BUSINESS-DIRECTORY']['category'] as $category_id => $selected) {
            if ($i > $max_categories) {
                if (!$message) {
                    $_uccms['_site-message']->set('warning', 'Selected categories more than maximum (' .$max_categories. '). Used first ' .($i - 1). ' categories.');
                    $message = true;
                }
                unset($_SESSION['POST']['BUSINESS-DIRECTORY']['category'][$category_id]);
            }
            $i++;
        }
    }

}

// REQUIRED FIELDS ARRAY
$reqfa = array(
    'name',
    'address1',
    //'city',
    'state',
    'zip',
    'phone'
);

// MISSING FIELDS
$missa = array();

// CHECK REQUIRED FIELDS
foreach ($reqfa as $field) {
    if (!$business[$field]) {
        $missa[] = $field;
    }
}

// HAVE MISSING FIELDS
if (count($missa) > 0) {
    foreach ($missa as $field) {
        $_uccms['_site-message']->set('error', 'Required field: ' .ucwords($field));
    }
    BigTree::redirect('../');
}

// ARRAYS
if (!is_array($business['payment_methods'])) $business['payment_methods'] = array();
if (!is_array($business['languages'])) $business['languages'] = array();
if (!is_array($business['hours'])) $business['hours'] = array();

// FILE UPLOADED / SELECTED
if (($_FILES['logo']['name']) || ($_POST['logo'])) {

    // BIGTREE UPLOAD FIELD INFO
    $field = array(
        'type'          => 'upload',
        'title'         => 'Logo',
        'key'           => 'logo',
        'options'       => array(
            'directory' => 'extensions/' .$_uccms_businessdir->Extension. '/files/temp',
            'image' => true,
            'thumbs' => array(
                array(
                    'width'     => '480', // MATTD: make controlled through settings
                    'height'    => '480' // MATTD: make controlled through settings
                )
            ),
            'crops' => array()
        )
    );

    // UPLOADED FILE
    if ($_FILES['logo']['name']) {
        $field['file_input'] = $_FILES['logo'];

    // FILE FROM MEDIA BROWSER
    } else if ($_POST['logo']) {
        $field['input'] = $_POST['logo'];
    }

    // INIT BIGTREE ADMIN
    $admin = new BigTreeAdmin;

    // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
    $file_path = BigTreeAdmin::processField($field);

    // DESTROY BIGTREE ADMIN
    unset($admin);

    // UPLOAD SUCCESSFUL
    if ($file_path) {

        // SAVE IMAGE TO SESSION
        $business['temp_logo'] = $_uccms_businessdir->imageBridgeIn($file_path);
        $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo'] = $business['temp_logo'];

    // UPLOAD FAILED
    } else {
        $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo'] = '';
        $_uccms['_site-message']->set('error', $bigtree['errors'][0]['error']);
    }

}

// GET PRICING
$price_mo = $_uccms_businessdir->getSetting('listing_price_mo');
$price_yr = $_uccms_businessdir->getSetting('listing_price_yr');

// HAVE CITY
if ($business['city_id']) {

    // GET CITY INFO
    $city_query = "SELECT * FROM `" .$_uccms_businessdir->tables['cities']. "` WHERE (`id`=" .$business['city_id']. ")";
    $city_q = sqlquery($city_query);
    $city = sqlfetch($city_q);

    if ($city['title']) {
        $business['city'] = stripslashes($city['title']);
    }

}

// NEIGHBORHOOD ID
$business['neighborhood_id'] = (int)$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['neighborhood'][$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['city_id']]['neighborhood_id'];

// HAVE NEIGHBORHOOD
if ($business['neighborhood_id']) {

    // GET NEIGHBORHOOD INFO
    $neighborhood_query = "SELECT * FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` WHERE (`id`=" .$business['neighborhood_id']. ")";
    $neighborhood_q = sqlquery($neighborhood_query);
    $neighborhood = sqlfetch($neighborhood_q);

}

?>