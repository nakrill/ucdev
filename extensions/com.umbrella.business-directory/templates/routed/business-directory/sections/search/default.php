<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/master/search.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/custom/search.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/master/search.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/custom/search.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="smallColumn">

    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/category_nav.php'); ?>

</div>

<div class="largeColumn withSmallColumn item_container">

    <?php if (!$_uccms_businessdir->getSetting('search_disabled')) { ?>
        <form action="/<?php echo $_uccms_businessdir->frontendPath(); ?>/search/" method="get" class="search">
            <input type="text" name="q" placeholder="Search businesses" value="<?php echo $_GET['q']; ?>" />
        </form>
    <?php } ?>

    <h1>Search</h1>

    <?php if ($_GET['q']) { ?>
        <div class="bold_heading">
            Showing results for "<?php echo $_GET['q']; ?>"
        </div>
    <?php } ?>

    <?php

    // HAVE LISTINGS
    if ($results['num_total'] > 0) {

        // HAVE PAGES
        if ($pages) {
            ?>

            <div class="paging top">
                <?php echo $pages; ?>
            </div>
            <?php

        }

        // HAVE PAGED LISTINGS
        if ($num_listings > 0) {

            ?>

            <div class="items">

                <div class="item_container contain">

                    <?php

                    // LOOP
                    foreach ($listings as $business) {
                        include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/listing.php');
                    }

                    ?>

                </div>

            </div>

            <?php if ($pages) { ?>
                <div class="paging bottom">
                    <?php echo $pages; ?>
                </div>
            <?php } ?>

            <?php

        // NO RESULTS ON THIS PAGE
        } else {

            ?>

            <div class="noresults">
                No listings on this page.
            </div>

            <?php

        }

    // NO RESULTS FOUND
    } else {

        ?>

        <div class="noresults">
            No matches found.
        </div>

        <?php

    }

    ?>

</div>