<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/master/category.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/custom/category.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/master/category.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/custom/category.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="smallColumn">

    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/category_nav.php'); ?>

</div>

<div class="largeColumn withSmallColumn item_container">

    <?php

    // HAVE CATEGORY
    if ($category['id']) {

        // SEARCH
        if (!$_uccms_businessdir->getSetting('search_disabled')) {
            ?>
            <form action="/<?php echo $_uccms_businessdir->frontendPath(); ?>/search/" method="get" class="search">
                <input type="hidden" name="category" value="<?php echo $category['id']; ?>" />
                <input type="text" name="q" placeholder="Search businesses" value="<?php echo $_GET['q']; ?>" />
            </form>
            <?php
        }

        // BREADCRUMB OPTIONS
        $bcoptions = array(
            'base'      => WWW_ROOT . $bigtree['path'][0]. '/',
            'rewrite'   => true
        );

        // BREADCRUMBS
        include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/breadcrumbs.php');

        // HAVE INFO FOR CATEGORY HEAD
        if (($category['image']) || ($category['title']) || ($category['description'])) {

            ?>

            <div class="category_head contain">

                <?php

                // HAVE IMAGE
                if ($category['image']) {
                    ?>
                    <img src="<?php echo $_uccms_businessdir->imageBridgeOut($category['image'], 'categories'); ?>" alt="<?php echo stripslashes($category['title']); ?>" />
                    <?php
                }

                // HAVE TITLE
                if ($category['title']) {
                    ?>
                    <h1><?php echo stripslashes($category['title']); ?></h1>
                    <?php
                }

                // HAVE DESCRIPTION
                if ($category['description']) {
                    ?>
                    <div class="description">
                        <?php echo nl2br(stripslashes($category['description'])); ?>
                    </div>
                    <?php
                }

                ?>

            </div>

            <?php

        }

        // HAVE SUB-CATEGORIES
        if (count($sub_categories) > 0) {

            ?>

            <div class="sub_categories">
                <h3>Sub-Categories</h3>
                <div class="contain">
                    <?php foreach ($sub_categories as $sub) { ?>
                        <div class="sub">
                            <a href="<?php echo $_uccms_businessdir->categoryURL($sub['id'], $sub); ?>"><?php echo stripslashes($sub['title']); ?></a>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <?php

        }

        // HAVE LISTINGS
        if ($num_listings > 0) {

            // HAVE PAGES
            if ($pages) {
                ?>

                <div class="paging sort">
                    <form>
                    <table style="width: 100%; border: 0px;">
                        <tr>
                            <td width="50%" style="vertical-align: middle;">
                                <?php /*
                                Sort by: <select name="sort">
                                    <option value="">Default</option>
                                    <option value="name_asc" <?php if ($_REQUEST['sort'] == 'name_asc') { ?>selected="selected"<?php } ?>>Name</option>
                                </select>
                                */ ?>
                            </td>
                            <td width="50%" style="text-align: right;">
                                <?php echo $pages; ?>
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
                <?php

            }
            ?>

            <div class="items">

                <div class="item_container contain">

                    <?php

                    // LOOP
                    foreach ($category_listings as $business) {
                        $business['category_id']    = $category['id'];
                        $business['category_slug']  = stripslashes($category['slug']);
                        include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/listing.php');
                    }

                    ?>

                </div>

            </div>

            <?php if ($pages) { ?>
                <div class="paging bottom">
                    <?php echo $pages; ?>
                </div>
            <?php } ?>

            <?php

        }

    // CATEGORY NOT FOUND
    } else {
        ?>
        Category not found.
        <?php
    }

    ?>

</div>