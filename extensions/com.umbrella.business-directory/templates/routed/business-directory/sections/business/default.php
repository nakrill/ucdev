<?php /*
http://schema.org/LocalBusiness
*/ ?>

<link rel="stylesheet" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/master/swipebox.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.1/js/jquery.swipebox.min.js"></script>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/master/business.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/custom/business.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/master/business.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/custom/business.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

// HAVE BUSINESS
if ($business['id']) {

    // GET ALL CATEGORIES BUSINESS IS IN
    $cats_query = "
    SELECT c.*
    FROM `" .$_uccms_businessdir->tables['business_categories']. "` AS `bc`
    INNER JOIN `" .$_uccms_businessdir->tables['categories']. "` AS `c` ON bc.category_id = c.id
    WHERE (bc.business_id=" .$business['id']. ")
    ";
    $cats_q = sqlquery($cats_query);
    if (sqlrows($cats_q) > 0) {
        ?>
        <ol itemscope itemtype="http://schema.org/BreadcrumbList" style="display: none;">
            <?php
            while ($cat = sqlfetch($cats_q)) {

                // GET CATEGORY PARENTS
                $tcatpa = $_uccms_businessdir->getCategoryParents($cat['id']);

                // HAVE CATEGORY PARENTS
                if (count($tcatpa) > 0) {

                    // REVERSE ARRAY FOR EASIER OUTPUT
                    $tcatpa = array_reverse($tcatpa);

                    // LOOP
                    foreach ($tcatpa as $tcat) {
                        ?>
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="<?php echo $_uccms_businessdir->categoryURL($tcat['id'], $tcat); ?>"><span itemprop="name"><?php echo stripslashes($tcat['title']); ?></span></a>
                        </li>
                        <?php
                    }

                }

                unset($tcatpa);

            }
            ?>
        </ol>
        <?php
    }

    ?>

    <div class="item_container" itemscope itemtype="http://schema.org/LocalBusiness">

        <div class="smallColumn">

            <?php if ($business['logo']) { ?>
                <div class="logo">
                    <img src="<?php echo $_uccms_businessdir->imageBridgeOut($business['logo'], 'businesses'); ?>" itemprop="logo" />
                </div>
            <?php } ?>

            <?php

            // GET HOURS
            $hours = $_uccms_businessdir->getHours($business['id']);

            // HAVE HOURS
            if (count($hours) > 0) {
                $dow    = $_uccms_businessdir->daysOfWeek();
                $dows   = $_uccms_businessdir->daysOfWeek_Schema();
                ?>
                <div class="hours pad-top">
                    <h3>Hours</h3>
                    <table>
                        <?php
                        foreach ($hours as $day) {
                            if (!$day['closed']) { ?>
                                <meta itemprop="openingHours" content="<?php echo $dows[$day['day']]; ?> <?php echo date('H:i', strtotime($day['open'])). '-' .date('H:i', strtotime($day['close'])); ?>" />
                            <?php } ?>
                            <tr>
                                <td class="day"><?php echo substr($dow[$day['day']], 0, 3); ?></td>
                                <td class="time">
                                    <?php
                                    if ($day['closed']) {
                                        echo 'Closed';
                                    } else {
                                        echo date('g:i A', strtotime($day['open'])). ' - ' .date('g:i A', strtotime($day['close']));
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <?php
            }

            // HAVE IMAGES OR VIDEOS
            if ((count($imagea) > 0) || (count($videoa) > 0)) {

                ?>

                <div class="images_container pad-top">

                    <?php if ($imagea[0]['url']) { ?>
                        <div class="main">
                            <a href="<?php echo $imagea[0]['url']; ?>" target="blank" class="swipebox <?php if ($i == 0) { ?>active<?php } ?>" rel="gallery-1"><img src="<?php echo $imagea[0]['url']; ?>" alt="<?php if ($imagea[0]['caption']) { echo stripslashes($imagea[0]['caption']); } else { echo stripslashes($business['name']); } ?>" itemprop="image" /></a>
                        </div>
                        <?php
                        unset($imagea[0]);
                    } ?>

                    <?php

                    $ii = 0;
                    ?>
                    <div class="thumbs contain">
                        <?php

                        // LOOP THROUGH IMAGES
                        if (count($imagea > 0)) {
                            foreach ($imagea as $image) {
                                if ($image['image']) {
                                    if ($image['caption']) {
                                        $caption = stripslashes($image['caption']);
                                    } else {
                                        $caption = stripslashes($item['title']);
                                    }
                                    ?>
                                    <div class="thumb image <?php if ($ii == 0) { ?>active<?php } ?>">
                                        <a href="<?php echo $image['url']; ?>" title="<?php echo $caption; ?>" target="_blank" class="swipebox" rel="gallery-1"><img src="<?php echo $image['url_thumb']; ?>" alt="<?php echo $caption; ?>" itemprop="image" /></a>
                                    </div>
                                    <?php
                                    $ii++;
                                }
                            }
                        }

                        // LOOP THROUGH VIDEOS
                        if (count($videoa > 0)) {
                            foreach ($videoa as $video) {
                                if (($video['video']) && ($video['info']['thumb']))  {
                                    if ($video['caption']) {
                                        $caption = stripslashes($video['caption']);
                                    } else {
                                        $caption = stripslashes($item['title']);
                                    }
                                    ?>
                                    <div class="thumb video <?php if ($ii == 0) { ?>active<?php } ?>">
                                        <a href="<?php echo $video['video']; ?>" title="<?php echo $caption; ?>" target="_blank" class="swipebox-video" rel="gallery-1"><img src="<?php echo $video['url_thumb']; ?>" alt="<?php echo $caption; ?>" /></a>
                                    </div>
                                    <?php
                                    $ii++;
                                }
                            }
                        }

                        ?>
                    </div>

                </div>

                <?php
            }

            ?>

        </div>

        <div class="largeColumn withSmallColumn">

            <?php

            // BREADCRUMB OPTIONS
            $bcoptions = array(
                'base'      => WWW_ROOT . $bigtree['path'][0]. '/',
                'rewrite'   => true,
                'link_last' => true
            );

            // BREADCRUMBS
            include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/breadcrumbs.php');

            ?>

            <div class="contain">

                <div class="clearfix">

                    <h1 itemprop="name"><?php echo stripslashes($business['name']); ?></h1>

                    <?php if ($_uccms_businessdir->getSetting('business_ratings_reviews_enabled')) { ?>
                        <div class="rating_stars" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                            <?php
                            if ($business['rating'] > 0) {
                                ?>
                                <div class="stars" title="<?php echo $business['rating']; ?> from <?php echo number_format($business['reviews'], 0); ?> reviews">
                                    <?php
                                    $star_vars = array(
                                        'el_id'     => 'business-stars-' .$business['id'],
                                        'value'     => $business['rating'],
                                        'readonly'  => true
                                    );
                                    include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                                    ?>
                                </div>
                                <span itemprop="ratingValue"><?php echo $business['rating']; ?></span>
                                <span itemprop="reviewCount"><?php echo $business['reviews']; ?></span>
                                <?php
                            } else {
                                ?>
                                <a href="#reviews">No reviews</a>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } ?>

                </div>

                <?php if ($business['department']) { ?>
                    <div class="department" itemprop="department" itemscope itemtype="http://schema.org/Organization">
                        <span itemprop="name"><?php echo stripslashes($business['department']); ?></span>
                    </div>
                <?php } ?>

                <div class="grid info_cols">

                    <div class="col-1-2 left last">

                        <div class="address_container" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                            <?php if (($business['address1']) || ($business['address2'])) { ?>
                                <div class="address" itemprop="streetAddress">
                                    <?php if ($business['address1']) { ?>
                                        <span><?php echo stripslashes($business['address1']); ?></span>
                                    <?php } ?>
                                    <?php if ($business['address2']) { ?>
                                        <span><?php echo stripslashes($business['address2']); ?></span>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if (($business['city']) || ($business['state']) || ($business['zip'])) { ?>
                                <div class="csz">
                                    <?php if ($business['city']) { ?>
                                        <span itemprop="addressLocality"><?php echo stripslashes($business['city']); ?><?php if ($business['state']) { ?>, <?php } ?></span>
                                    <?php } ?>
                                    <?php if ($business['state']) { ?>
                                        <span itemprop="addressRegion"><?php echo stripslashes($business['state']); ?></span>
                                    <?php } ?>
                                    <?php if ($business['zip']) { ?>
                                        <span itemprop="postalCode"><?php echo $business['zip']; ?><?php if ($business['zip4']) { echo '-' .$business['zip4']; } ?></span>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>

                        <table>

                            <?php if ($business['phone']) { ?>
                                <tr class="phone">
                                    <td><i class="fa fa-fw fa-phone"></i> Phone: </td>
                                    <td>
                                        <div itemprop="telephone">
                                            <?php echo $_uccms_businessdir->prettyPhone($business['phone']); if ($business['phone_ext']) { echo ' ' .stripslashes($business['phone_ext']); } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ($business['phone_tollfree']) { ?>
                                <tr class="phone tollfree">
                                    <td><i class="fa fa-fw fa-phone"></i> Tollfree: </td>
                                    <td>
                                        <div>
                                            <?php echo $_uccms_businessdir->prettyPhone($business['phone_tollfree']); if ($business['phone_tollfree_ext']) { echo ' ' .stripslashes($business['phone_tollfree_ext']); } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ($business['phone_alt']) { ?>
                                <tr class="phone alt">
                                    <td><i class="fa fa-fw fa-phone"></i> Alternate: </td>
                                    <td>
                                        <div>
                                            <?php echo $_uccms_businessdir->prettyPhone($business['phone_alt']); if ($business['phone_alt_ext']) { echo ' ' .stripslashes($business['phone_alt_ext']); } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ($business['phone_mobile']) { ?>
                                <tr class="phone mobile">
                                    <td><i class="fa fa-fw fa-mobile"></i> Mobile: </td>
                                    <td>
                                        <div>
                                            <?php echo $_uccms_businessdir->prettyPhone($business['phone_mobile']); if ($business['phone_mobile_ext']) { echo ' ' .stripslashes($business['phone_mobile_ext']); } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ($business['fax']) { ?>
                                <tr class="phone mobile">
                                    <td><i class="fa fa-fw fa-fax"></i> Fax: </td>
                                    <td>
                                        <div itemprop="faxNumber">
                                            <?php echo $_uccms_businessdir->prettyPhone($business['fax']); ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                        </table>

                        <?php if ($business['email']) { ?>
                            <div class="email pad-top">
                                <i class="fa fa-fw fa-envelope-o"></i> <a href="mailto:<?php echo $business['email']; ?>" target="blank"><span itemprop="email"><?php echo $business['email']; ?></span></a>
                            </div>
                        <?php } ?>

                        <?php if ($business['url']) { ?>
                            <div class="url pad-top">
                                <i class="fa fa-fw fa-globe"></i> <a itemprop="url" href="http://<?php echo $business['url']; ?>" target="_blank"><?php echo $business['url']; ?></a>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="col-1-2 right last">
                        <?php if (($business['address1']) || ($business['address2']) || ($business['city']) || ($business['state']) || ($business['zip'])) { ?>
                            <iframe width="100%" height="100%" frameborder="0" style="margin: 0; border: 0;" src="https://www.google.com/maps/embed/v1/place?q=<?php echo stripslashes($business['address1']); if ($business['address2']) { echo ',' .stripslashes($business['address2']); } if ($business['city']) { echo ',' .stripslashes($business['city']); } if ($business['state']) { echo ',' .stripslashes($business['state']); } if ($business['zip']) { echo ',' .stripslashes($business['zip']); } ?>&key=AIzaSyDb9IKaXNmMAc8bMuRtOmmEx35cOW1HZEo" allowfullscreen></iframe>
                        <?php } ?>
                    </div>

                </div>

                <?php if ($business['description']) { ?>
                    <div class="description pad-top" itemprop="description">
                        <?php echo nl2br(stripslashes($business['description'])); ?>
                    </div>
                <?php } ?>

                <?php if (($business['name_owner']) || ($business['name_contact'])) { ?>
                    <div class="owner_contact pad-top">
                        <table>

                            <?php if ($business['name_owner']) { ?>
                                <tr class="owner">
                                    <td>Owner: </td>
                                    <td>
                                        <div itemprop="founder" itemscope itemtype="http://schema.org/Person">
                                            <span itemprop="name"><?php echo stripslashes($business['name_owner']); ?></span>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ($business['name_contact']) { ?>
                                <tr class="contact">
                                    <td>Contact: </td>
                                    <td>
                                        <div itemprop="contactPoint" itemscope itemtype="http://schema.org/ContactPoint">
                                            <span itemprop="name"><?php echo stripslashes($business['name_contact']); ?></span>
                                            <meta itemprop="contactType" content="Customer Service" />
                                            <meta itemprop="telephone" content="+1 <?php echo $_uccms_businessdir->prettyPhone($business['phone']); ?>" />
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                        </table>
                    </div>
                <?php } ?>

                <?php if (($business['year_opened']) && ($business['year_opened'] != '0000')) { ?>
                    <div class="year_opened pad-top">
                        <h3>Year Opened</h3>
                        <span itemprop="foundingDate"><?php echo $business['year_opened']; ?></span>
                    </div>
                <?php } ?>

                <?php if ($business['payment_methods']) { ?>
                    <div class="payment_methods pad-top">
                        <h3>Payment Methods Accepted</h3>
                        <div itemprop="paymentAccepted">
                            <?php
                            $pma = $_uccms_businessdir->paymentMethods();
                            $bpma = explode(',', stripslashes($business['payment_methods']));
                            foreach ($bpma as $pm_id) {
                                $pmta[] = $pma[$pm_id];
                            }
                            if (count($pmta) > 0) {
                                echo implode(', ', $pmta);
                            }
                            ?>
                         </div>
                    </div>
                <?php } ?>

                <?php if ($business['languages']) { ?>
                    <div class="languages pad-top">
                        <h3>Languages</h3>
                        <div>
                            <?php
                            $la = $_uccms_businessdir->languages();
                            $bla = explode(',', stripslashes($business['languages']));
                            foreach ($bla as $lid) {
                                $lta[] = $la[$lid];
                            }
                            if (count($lta) > 0) {
                                echo implode(', ', $lta);
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($business['tagline']) { ?>
                    <div class="tagline pad-top">
                        <h3>Tagline</h3>
                        <div>
                            <?php echo stripslashes($business['tagline']); ?>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($business['keywords']) { ?>
                    <div class="keywords pad-top">
                        <h3>Keywords</h3>
                        <div>
                            <?php echo stripslashes($business['keywords']); ?>
                        </div>
                    </div>
                <?php } ?>

                <?php

                // HAVE FEATURES
                if (count($feata) > 0) {
                    ?>
                    <div class="features pad-top">
                        <?php

                        // FEATURES
                        include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/features/display/main.php');

                        ?>
                    </div>
                    <?php
                }

                // SEE IF THEY HAVE SOCIAL LINKS
                $have_social = false;
                foreach ($_uccms_businessdir->socialNetworks() as $sn_name => $null) {
                    if ($business['social_' .$sn_name]) {
                        $have_social = true;
                        break;
                    }
                }

                // HAVE SOCIAL
                if ($have_social) {

                    ?>

                    <div class="social pad-top">
                        <h3>Social</h3>
                        <div>
                            <table>

                                <?php if ($business['social_facebook']) { ?>
                                    <tr class="facebook">
                                        <td><i class="fa fa-fw fa-facebook"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_facebook']); ?>" target="_blank"><?php echo stripslashes($business['social_facebook']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_foursquare']) { ?>
                                    <tr class="foursquare">
                                        <td><i class="fa fa-fw fa-foursquare"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_foursquare']); ?>" target="_blank"><?php echo stripslashes($business['social_foursquare']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_googleplus']) { ?>
                                    <tr class="googleplus">
                                        <td><i class="fa fa-fw fa-google-plus"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_googleplus']); ?>" target="_blank"><?php echo stripslashes($business['social_googleplus']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_instagram']) { ?>
                                    <tr class="instagram">
                                        <td><i class="fa fa-fw fa-instagram"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_instagram']); ?>" target="_blank"><?php echo stripslashes($business['social_instagram']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_linkedin']) { ?>
                                    <tr class="linkedin">
                                        <td><i class="fa fa-fw fa-linkedin"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_linkedin']); ?>" target="_blank"><?php echo stripslashes($business['social_linkedin']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_pinterest']) { ?>
                                    <tr class="pinterest">
                                        <td><i class="fa fa-fw fa-pinterest"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_pinterest']); ?>" target="_blank"><?php echo stripslashes($business['social_pinterest']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_tripadvisor']) { ?>
                                    <tr class="tripadvisor">
                                        <td><i class="fa fa-fw fa-tripadvisor"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_tripadvisor']); ?>" target="_blank"><?php echo stripslashes($business['social_tripadvisor']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_twitter']) { ?>
                                    <tr class="twitter">
                                        <td><i class="fa fa-fw fa-twitter"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_twitter']); ?>" target="_blank"><?php echo stripslashes($business['social_twitter']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ($business['social_yelp']) { ?>
                                    <tr class="yelp">
                                        <td><i class="fa fa-fw fa-yelp"></i> </td>
                                        <td>
                                            <a href="https://<?php echo stripslashes($business['social_yelp']); ?>" target="_blank"><?php echo stripslashes($business['social_yelp']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </table>
                        </div>
                    </div>

                <?php

                }

                // BOOKING WIDGET EMBED CODE SPECIFIED
                if ($business['booking_widget_code']) {

                    ?>

                    <div class="booking pad-top" style="margin-top: 15px; padding: 15px; border: 1px solid #ccc;">

                        <h3>Booking</h3>

                        <div style="width: 350px;">
                            <?php echo stripslashes($business['booking_widget_code']); ?>
                        </div>

                    </div>

                    <?php

                // HAVE E-COMMERCE EXTENSION
                } else if (class_exists('uccms_Ecommerce')) {

                    // GET BUSINESS'S E-COMMERCE ITEMS
                    $beia = $_uccms_businessdir->business_getEcommerceItems($business['id']);

                    // HAVE E-COMMERCE ITEMS
                    if (count($beia) > 0) {

                        // INIT ECOMMERCE CLASS
                        if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce();

                        ?>

                        <div class="booking pad-top" style="margin-top: 15px; padding: 15px; border: 1px solid #ccc;">

                            <h3>Booking</h3>

                            <?php

                            // LOOP THROUGH ITEMS
                            foreach ($beia as $beitem) {

                                // GET E-COMMERCE ITEM
                                $eitem = $_uccms_ecomm->getItem($beitem['ecommerce_item_id']);

                                // IS BOOKING
                                if ($eitem['type'] == 2) {

                                    ?>

                                    <div style="width: 350px;">
                                        <script src="/<?php echo $_uccms_ecomm->storePath(); ?>/widget/embed.js?store_type=booking&what=booking-calendar&vars[item_id]=<?php echo $eitem['id']; ?>" type="text/javascript"></script>
                                        <div id="uccms-widget-container"></div>
                                    </div>

                                    <?php

                                }

                            }

                            ?>

                        </div>
                        <?php

                    }

                }

                // CONTACT FORM ENABLED
                if (($_uccms_businessdir->getSetting('listing_contact_enabled')) && (($business['email']) || ($business['email_lead'])) && (count($_uccms_businessdir->listingContactFields()) > 0)) {
                    ?>
                    <div class="contact pad-top">
                        <h3>Contact</h3>
                        <div class="site-message type-success"></div>
                        <div class="site-message type-error"></div>
                        <div>
                            <form action="" method="post">
                            <input type="hidden" name="business_id" value="<?php echo $business['id']; ?>" />
                            <?php foreach ($_uccms_businessdir->listingContactFields() as $field_id => $field) { ?>
                                <div class="field">
                                    <input type="text" name="field[<?php echo $field_id; ?>]" value="" placeholder="<?php echo $field['title']; ?>" />
                                </div>
                            <?php } ?>
                            <?php
                            $captcha_el = '#businessdirContainer .contact form .captcha .recaptcha';
                            include(SERVER_ROOT. 'templates/elements/captcha.php');
                            ?>
                            <div class="buttons">
                                <input type="submit" value="Submit" class="button" />
                            </div>
                            </form>
                        </div>
                    </div>
                    <?php
                }

                // RATINGS / REVIEWS
                if ($_uccms_businessdir->getSetting('business_ratings_reviews_enabled')) {

                    // VARS FOR REVIEW
                    $reviews_vars = array(
                        'source'    => $_uccms_businessdir->Extension,
                        'what'      => 'business',
                        'item_id'   => $business['id']
                    );

                    // IS OWNER
                    $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'reply', ($business['account_id'] == $_uccms['_account']->userID()));

                    ?>
                    <div class="reviews pad-top">
                        <h3>Reviews</h3>
                        <div class="content">
                            <?php include(SERVER_ROOT. 'templates/elements/ratings-reviews/reviews.php'); ?>
                        </div>
                    </div>

                    <?php

                }

                // HAVE BLOG EXTENSION
                if (is_dir(SERVER_ROOT. 'extensions/com.umbrella.blog')) {

                    if (!$_uccms_blog) $_uccms_blog = new uccms_Blog();

                    // GET BLOG POSTS
                    $posts_query = "SELECT `id` FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`business_id`=" .$business['id']. ") AND (`status`=1) ORDER BY dt_published DESC, id DESC LIMIT 2";
                    $posts_q = sqlquery($posts_query);

                    // HAVE BLOG POSTS
                    if (sqlrows($posts_q) > 0) {

                        ?>
                        <div class="blog-posts pad-top">
                            <h3>Blog Posts</h3>
                            <div class="posts clearfix">
                                <?php
                                while ($post = sqlfetch($posts_q)) {
                                    $post = $_uccms_blog->getPostInfo($post['id']);
                                    ?>
                                    <div class="post">
                                        <div class="margin">
                                            <?php if ($post['images'][0]['image']) { ?>
                                                <div class="image main">
                                                    <a href="<?php echo $_uccms_blog->postURL($post['id']); ?>"><img src="<?php echo $_uccms_blog->imageBridgeOut($post['images'][0]['image'], 'posts'); ?>" alt="<?php echo $post['images'][0]['caption']; ?>" /></a>
                                                </div>
                                            <?php } ?>
                                            <?php if ($post['title']) { ?>
                                                <h3><a href="<?php echo $_uccms_blog->postURL($post['id']); ?>"><?php echo stripslashes($post['title']); ?></a></h3>
                                            <?php } ?>
                                            <?php if ($post['content']) { ?>
                                                <div class="content">
                                                    <?php echo substrWord(str_replace('<p></p>', '', str_replace('[gallery]', '', stripslashes($post['content']))), ($post['images'][0]['image'] ? 350 : 700), '..'); ?>
                                                </div>
                                            <?php } ?>
                                            <div class="read-more">
                                                <a href="<?php echo $_uccms_blog->postURL($post['id']); ?>">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                    }

                }

                ?>

            </div>

        </div>

    </div>

    <?php

// BUSINESS NOT FOUND
} else {
    ?>

    <div class="smallColumn">

        <?php include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/category_nav.php'); ?>

    </div>

    <div class="largeColumn withSmallColumn item_container">

        Business not found.

    </div>

    <?php
}

?>