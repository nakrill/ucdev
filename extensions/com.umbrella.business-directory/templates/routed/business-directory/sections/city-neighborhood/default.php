<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/master/home.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/custom/home.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/master/home.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/custom/home.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="smallColumn">

    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/category_nav.php'); ?>

</div>

<div class="largeColumn withSmallColumn item_container">

    <?php

    // HAVE CITY ID
    if ($city['id']) {

        // HAVE CITY TITLE
        if ($city['title']) {
            ?>
            <h2 class="location">
                <?php if ($neighborhood['id']) { ?>
                    <a href="../"><?php echo stripslashes($city['title']); ?></a> <i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo stripslashes($neighborhood['title']); ?>
                <?php } else { ?>
                    <?php echo stripslashes($city['title']); ?>
                <?php } ?>
            </h2>
            <?php
        }

        // HAVE LISTINGS
        if ($results['total'] > 0) {

            // HAVE PAGES
            if ($pages) {
                ?>

                <div class="paging top">
                    <?php echo $pages; ?>
                </div>
                <?php

            }

            // HAVE PAGED LISTINGS
            if ($num_listings > 0) {

                ?>

                <div class="items">

                    <div class="item_container contain">

                        <?php

                        // LOOP
                        foreach ($listings as $business) {
                            include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/listing.php');
                        }

                        ?>

                    </div>

                </div>

                <?php if ($pages) { ?>
                    <div class="paging bottom">
                        <?php echo $pages; ?>
                    </div>
                <?php } ?>

                <?php

            // NO RESULTS ON THIS PAGE
            } else {

                ?>

                <div class="noresults">
                    No listings on this page.
                </div>

                <?php

            }

        // NO RESULTS FOUND
        } else {

            ?>

            <div class="noresults">
                No matches found.
            </div>

            <?php

        }

    } else {

        ?>

        <div class="noresults">
            City not found.
        </div>

        <?php

    }

    ?>

</div>