<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/master/home.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_businessdir->Extension;?>/css/custom/home.css" />
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/master/home.js"></script>
<script src="/extensions/<?=$_uccms_businessdir->Extension;?>/js/custom/home.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="smallColumn">

    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/category_nav.php'); ?>

    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/search_filter-vertical.php'); ?>

</div>

<div class="largeColumn withSmallColumn">

    <?php

    // HAVE CATEGORY
    if ($category['id']) {

        // BREADCRUMB OPTIONS
        $bcoptions = array(
            'base'      => WWW_ROOT . $bigtree['path'][0]. '/',
            'rewrite'   => true
        );

        // BREADCRUMBS
        include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/breadcrumbs.php');

        // HAVE INFO FOR CATEGORY HEAD
        if (($category['image']) || ($category['title']) || ($category['description'])) {

            ?>

            <div class="category_head contain">

                <?php

                // HAVE IMAGE
                if ($category['image']) {
                    ?>
                    <img src="<?php echo $_uccms_businessdir->imageBridgeOut($category['image'], 'categories'); ?>" alt="<?php echo stripslashes($category['title']); ?>" />
                    <?php
                }

                // HAVE TITLE
                if ($category['title']) {
                    ?>
                    <h1><?php echo stripslashes($category['title']); ?></h1>
                    <?php
                }

                // HAVE DESCRIPTION
                if ($category['description']) {
                    ?>
                    <div class="description">
                        <?php echo nl2br(stripslashes($category['description'])); ?>
                    </div>
                    <?php
                }

                ?>

            </div>

            <?php

        }

        // HAVE SUB-CATEGORIES
        if (count($sub_categories) > 0) {

            $huv = '';

            $hga = $_GET;
            unset($hga['bigtree_htaccess_url']);
            unset($hga['category']);
            if (count($hga) > 0) {
                $huv .= '?' .http_build_query($hga);
            }
            unset($hga);

            ?>

            <div class="sub_categories">
                <h3>Sub-Categories</h3>
                <div class="contain">
                    <?php foreach ($sub_categories as $sub) { ?>
                        <div class="sub">
                            <a href="<?php echo $_uccms_businessdir->categoryURL($sub['id'], $sub) . $huv; ?>"><?php echo stripslashes($sub['title']); ?></a>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <?php

        }

    // MAIN HOME
    } else if (count($_GET) <= 1) {

        // HAVE CONTENT
        if ($businessdir['settings']['home_content']) {
            ?>
            <div class="content">
                <?php echo stripslashes($businessdir['settings']['home_content']); ?>
            </div>
            <?php
        }

        // DISPLAYING FEATURED
        if ((int)$businessdir['settings']['home_featured_num'] > 0) {
            include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/featured_listings.php');
        }

        // DISPLAYING RECENT
        if ((int)$businessdir['settings']['home_recent_num'] > 0) {
            include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/recent_listings.php');
        }

    }

    // HAVE BUSINESSES
    if (count($businesses['results']) > 0) {

        // HAVE PAGES
        if ($pages) {
            ?>

            <div class="paging top">
                <?php echo $pages; ?>
            </div>
            <?php

        }

        ?>

        <div class="items">

            <div class="item_container contain">

                <?php

                // LOOP
                foreach ($businesses['results'] as $business) {
                    include(SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/routed/business-directory/elements/listing.php');
                }

                ?>

            </div>

        </div>

        <?php if ($pages) { ?>
            <div class="paging bottom">
                <?php echo $pages; ?>
            </div>
        <?php } ?>

        <?php

    // NO RESULTS FOUND
    } else {

        ?>

        <div class="noresults">
            No matches found.
        </div>

        <?php

    }

    ?>

</div>