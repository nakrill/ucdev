<style type="text/css">

    #listing .ditem {
        margin-bottom: 8px;
    }
    #listing .ditem2 {
        margin-bottom: 15px;
    }

    #listing .logo img {
        width: 100%;
        max-width: 375px;
        height: auto;
    }
    #listing h2 {
        margin: 0;
        font-size: 1.6em;
        opacity: .8;
    }
    #listing .department {
        font-size: 1.1em;
    }
    #listing .phone.main {
        font-size: 1.1em;
    }
    #listing .tagline {
        font-style: italic;
    }

    #listing h5 {
        margin: 0 0 2px 0;
        border-bottom: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
    }

    #listing .categories .category {
        margin: 8px 0 0 8px;
    }
    #listing .categories .category strong {
        color: #4f4f4f;
    }
    #listing .categories .attrs {
        padding: 0 0 0 10px;
    }

    #listing .right_section {
        margin-top: 25px;
    }
    #listing .right_section h4 {
        margin: 0px;
    }
    #listing .right_section fieldset {
        margin-top: 10px;
    }
    #listing .right_section fieldset label {
        cursor: default;
    }
    #listing .right_section input {
        display: block;
        width: 250px;
        margin: 0px auto;
    }
    #listing .right_section input[type="text"] {
        border: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
    }

    #listing .account input[type="text"], #listing .account input[type="password"] {
        border: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
    }

    #listing .right_section.account {
        margin-bottom: 10px;
    }

    #listing .right_section.total input[type="radio"] {
        display: inline-block;
        width: auto;
    }

    #listing .right_section .button.submit {
        width: 100%;
        padding: 8px 14px;
        font-size: 1.2em;
    }

</style>

<?php include(__DIR__. '/_header.php'); ?>

<form action="../process/" method="post">
<?php if ($business['id']) { ?>
    <input type="hidden" name="id" value="<?php echo $business['id']; ?>" />
<?php } ?>

<h3 class="cl" style="margin-bottom: 0px;">Review Business Listing</h3>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>

        <td width="70%" valign="top">

            <div style="background-color: #fff; border-bottom: 1px solid #fff;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr style="height: 100%;">

                        <td width="55%" valign="top" style="padding: 20px;">

                            <?php if ($business['temp_logo']) { ?>
                                <div class="logo ditem">
                                    <img src="<?php echo $_uccms_businessdir->imageBridgeOut($business['temp_logo'], 'temp'); ?>" alt="Invalid logo URL" />
                                </div>
                            <?php } else if ($business['logo']) { ?>
                                <div class="logo ditem">
                                    <img src="<?php echo $_uccms_businessdir->imageBridgeOut($business['logo'], 'businesses'); ?>" alt="Invalid logo URL" />
                                </div>
                            <?php } ?>

                            <h2 class="name" style="color: inherit;"><?php echo $business['name']; ?></h2>

                            <?php if ($business['department']) { ?>
                                <div class="department ditem"><?php echo $business['department']; ?></div>
                            <?php } ?>

                            <div class="phone main ditem"><?php echo $_uccms_businessdir->prettyPhone($business['phone']); ?></div>

                            <?php if ($business['tagline']) { ?>
                                <div class="tagline ditem"><?php echo $business['tagline']; ?></div>
                            <?php } ?>

                            <?php if ($neighborhood['title']) { ?>
                                <div class="neighborhood ditem"><?php echo stripslashes($neighborhood['title']); ?></div>
                            <?php } ?>

                            <div class="address ditem">
                                <div class="street"><?php echo $business['address1']; ?></div>
                                <?php if ($business['address2']) { ?>
                                    <div class="street2"><?php echo $business['address2']; ?></div>
                                <?php } ?>
                                <div class="csz"><?php echo $business['city']; ?>, <?php echo $business['state']; ?>&nbsp;&nbsp;<?php echo $business['zip']; ?><?php if ($business['zip_4']) { echo '-' .$business['zip_4']; } ?></div>
                            </div>

                            <?php if ($business['url']) { ?>
                                <div class="url ditem"><a href="<?php echo $business['url']; ?>" target="_blank"><?php echo $business['url']; ?></a></div>
                            <?php } ?>

                            <?php if ($business['email']) { ?>
                                <div class="email"><?php echo $business['email']; ?></div>
                            <?php } ?>
                            <?php if ($business['email_private']) { ?>
                                <div class="email"><?php echo $business['email_private']; ?> (Private)</div>
                            <?php } ?>
                            <?php if ($business['email_lead']) { ?>
                                <div class="email"><?php echo $business['email_lead']; ?> (Lead)</div>
                            <?php } ?>

                        </td>

                        <td width="45%" height="100%" valign="top">
                            <iframe width="312" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo stripslashes($business['address1']); if ($business['address2']) { echo ',' .stripslashes($business['address2']); } if ($business['city']) { echo ',' .stripslashes($business['city']); } if ($business['state']) { echo ',' .stripslashes($business['state']); } if ($business['zip']) { echo ',' .stripslashes($business['zip']); } ?>&key=AIzaSyDb9IKaXNmMAc8bMuRtOmmEx35cOW1HZEo" allowfullscreen></iframe>
                        </td>

                    </tr>
                </table>
            </div>

            <div style="padding: 20px; background-color: #f6f6f6; border-radius: 0px 0px 5px 5px; -moz-border-radius: 0px 0px 5px 5px; -webkit-border-radius: 0px 0px 5px 5px;">

                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="ditem2">
                    <tr>
                        <td width="55%" valign="top" style="padding-right: 30px;">

                            <?php if (($business['phone_tollfree']) || ($business['phone_mobile']) || ($business['phone_alt']) || ($business['fax'])) { ?>
                                <div class="phone_numbers ditem2">
                                    <h5>Other Numbers</h5>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <?php if ($business['phone_tollfree']) { ?>
                                            <tr class="phone tollfree">
                                                <td>Tollfree:&nbsp;</td>
                                                <td><?php echo $_uccms_businessdir->prettyPhone($business['phone_tollfree']); ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($business['phone_mobile']) { ?>
                                            <tr class="phone mobile">
                                                <td>Mobile:&nbsp;</td>
                                                <td><?php echo $_uccms_businessdir->prettyPhone($business['phone']['mobile']); ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($business['phone_alt']) { ?>
                                            <tr class="phone alt">
                                                <td>Alternate:&nbsp;</td>
                                                <td><?php echo $_uccms_businessdir->prettyPhone($business['phone']['alt']); ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($business['fax']) { ?>
                                            <tr class="phone fax">
                                                <td>Fax:&nbsp;</td>
                                                <td><?php echo $_uccms_businessdir->prettyPhone($business['fax']); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            <?php } ?>

                            <?php if ($business['description']) { ?>
                                <div class="description ditem2"><?php echo nl2br($business['description']); ?></div>
                            <?php } ?>

                            <div class="hours">
                                <h5>Hours</h5>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <?php
                                    $dowa = $_uccms_businessdir->daysOfWeek();
                                    foreach ($dowa as $day_code => $day_name) {
                                        ?>
                                        <tr>
                                            <td><?php echo $day_name; ?>:&nbsp;</td>
                                            <td>
                                                <?php
                                                if ($business['hours'][$day_code]['closed'] == 1) {
                                                    echo 'Closed';
                                                } else {
                                                    echo date('g:i A', strtotime($business['hours'][$day_code]['open'])). ' - ' .date('g:i A', strtotime($business['hours'][$day_code]['close']));
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>

                        </td>
                        <td width="45%" valign="top">

                            <?php if ($business['year_opened']) { ?>
                                <div class="year_opened ditem2">
                                    <h5>Year Opened</h5>
                                    <?php echo $business['year_opened']; ?>
                                </div>
                            <?php } ?>

                            <?php if (count($business['languages']) > 0) { ?>
                                <div class="keywords ditem2">
                                    <h5>Languages Spoken</h5>
                                    <?php
                                    $langa = $_uccms_businessdir->languages();
                                    foreach ($business['languages'] as $lang_code => $selected) {
                                        if ($selected) {
                                            ?>
                                            <div><?php echo $langa[$lang_code]; ?></div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            <?php } ?>

                            <?php if (count($business['payment_methods']) > 0) { ?>
                                <div class="payment_methods">
                                    <h5>Payment Methods Accepted</h5>
                                    <?php
                                    $pma = $_uccms_businessdir->paymentMethods();
                                    foreach ($business['payment_methods'] as $pmid => $selected) {
                                        if ($selected) {
                                            ?>
                                            <div><?php echo $pma[$pmid]; ?></div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            <?php } ?>

                        </td>
                    </tr>
                </table>

                <?php

                // ARRAY OF CATEGORIES
                $cata = array();

                // HAVE ARRAY FROM SESSION
                if (is_array($_SESSION['POST']['BUSINESS-DIRECTORY']['category'])) {
                    $cata = $_SESSION['POST']['BUSINESS-DIRECTORY']['category'];

                // HAVE BUSINESS ID
                } else if ($business['id']) {

                    // GET CURRENT RELATIONS
                    $query = "SELECT `category_id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ")";
                    $q = sqlquery($query);
                    while ($row = sqlfetch($q)) {
                        $cata[$row['category_id']] = $row['category_id'];
                    }

                }

                // HAVE CATEGORIES
                if (count($cata) > 0) {
                    ?>
                    <div class="categories ditem2">
                        <h5>Category(s)</h5>
                        <?php
                        foreach ($cata as $category_id => $category) {
                            $catpa = $_uccms_businessdir->getCategoryParents($category_id); // CATEGORY PARENT(S) ARRAY
                            ?>
                            <div class="item">
                                <?php echo $_uccms_businessdir->generateBreadcrumbs($catpa, array(
                                    'no_home'   => true
                                )); ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                }

                // HAVE SOCIAL LINKS
                if (count($business['social']) > 0) { ?>
                    <div class="social_links ditem2">
                        <h5>Social</h5>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <?php if ($business['social']['facebook']) { ?>
                                <tr class="social facebook">
                                    <td>Facebook:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['facebook']; ?>" target="_blank"><?php echo $business['social']['facebook']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['foursquare']) { ?>
                                <tr class="social foursquare">
                                    <td>Foursquare:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['foursquare']; ?>" target="_blank"><?php echo $business['social']['foursquare']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['googleplus']) { ?>
                                <tr class="social googleplus">
                                    <td>Google+:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['googleplus']; ?>" target="_blank"><?php echo $business['social']['googleplus']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['instagram']) { ?>
                                <tr class="social instagram">
                                    <td>Instagram:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['instagram']; ?>" target="_blank"><?php echo $business['social']['instagram']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['linkedin']) { ?>
                                <tr class="social linkedin">
                                    <td>LinkedIn:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['linkedin']; ?>" target="_blank"><?php echo $business['social']['linkedin']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['pinterest']) { ?>
                                <tr class="social pinterest">
                                    <td>Pinterest:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['pinterest']; ?>" target="_blank"><?php echo $business['social']['pinterest']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['tripadvisor']) { ?>
                                <tr class="social tripadvisor">
                                    <td>TripAdvisor:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['tripadvisor']; ?>" target="_blank"><?php echo $business['social']['tripadvisor']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['twitter']) { ?>
                                <tr class="social twitter">
                                    <td>Twitter:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['twitter']; ?>" target="_blank"><?php echo $business['social']['twitter']; ?></a></td>
                                </tr>
                            <?php } ?>
                            <?php if ($business['social']['yelp']) { ?>
                                <tr class="social yelp">
                                    <td>Yelp:&nbsp;</td>
                                    <td><a href="<?php echo $business['social']['yelp']; ?>" target="_blank"><?php echo $business['social']['yelp']; ?></a></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                <?php } ?>

                <?php if ($business['keywords']) { ?>
                    <div class="keywords">
                        <h5>Keywords</h5>
                        <?php echo $business['keywords']; ?>
                    </div>
                <?php } ?>

            </div>

            <div style="padding-top: 10px;">
                <a href="../" class="button"><i class="fa fa-pencil"></i> Edit Business Details</a>
            </div>

        </td>

        <td width="30%" valign="top">
            <div style="padding: 0 0 20px 30px;">

                <div class="right_section edit_business_details">
                    <a href="../"><i class="fa fa-pencil"></i> Edit Business Details</a>
                </div>

                <?php
                if (!$_uccms['_account']->loggedIn()) {
                    if ($_SESSION['POST']['BUSINESS-DIRECTORY']['account']['email']) {
                        $account_email = $_SESSION['POST']['BUSINESS-DIRECTORY']['account']['email'];
                    } else {
                        $account_email = $business['email'];
                    }
                    ?>
                    <div class="right_section account">
                        <h4>Create Your Umbrella Account</h4>
                        <div class="note">
                            Your account enables you to update your listing(s) whenever you need. (Required)
                        </div>
                        <fieldset>
                            <input type="text" name="account[email]" value="<?php echo $account_email; ?>" placeholder="email" />
                        </fieldset>
                        <fieldset>
                            <input type="password" name="account[password]" value="" placeholder="password" />
                        </fieldset>
                    </div>
                <?php } ?>

                <?php

                if ($payment_due) { ?>

                    <div class="right_section total">
                        <table width="100%">
                            <?php if ($price_mo) { ?>
                                <tr>
                                    <td width="50%"><input type="radio" name="payment[term]" value="mo" <?php if (($_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['term'] == 'mo') || (!$price_yr)) { ?>checked="checked"<?php } ?> /> <strong>Monthly Listing:</strong></td>
                                    <td width="50%" style="text-align: right;"><h4>$<?php echo $price_mo; ?></h4></td>
                                </tr>
                            <?php } ?>
                            <?php if ($price_yr) { ?>
                                <tr>
                                    <td width="50%"><input type="radio" name="payment[term]" value="yr" <?php if (($_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['term'] == 'yr') || (!$_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['term'])) { ?>checked="checked"<?php } ?> /> <strong>Yearly Listing:</strong></td>
                                    <td width="50%" style="text-align: right;"><h4>$<?php echo $price_yr; ?></h4></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>

                    <div class="right_section payment">
                        <h4>Enter Your Payment Info</h4>
                        <div class="note">
                            All fields required.
                        </div>
                        <fieldset>
                            <label>Name On Card</label>
                            <input type="text" name="payment[name]" value="<?php echo $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['name']; ?>" />
                        </fieldset>
                        <fieldset>
                            <label>Card Number</label>
                            <input type="text" name="payment[number]" value="<?php echo $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['number']; ?>" />
                        </fieldset>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%" valign="top">
                                    <fieldset>
                                        <label>Card Expiration</label>
                                        <input type="text" name="payment[expiration]" value="<?php echo $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['expiration']; ?>" placeholder="mm/yy" style="width: 114px;" />
                                    </fieldset>
                                </td>
                                <td width="50%" valign="top" style="padding-left: 10px;">
                                    <fieldset>
                                        <label style="padding-left: 2px;">Card Zip</label>
                                        <input type="text" name="payment[zip]" value="<?php echo $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['zip']; ?>" style="width: 100px;" />
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                        <fieldset>
                            <label>Card CVV</label>
                            <input type="text" name="payment[cvv]" value="<?php echo $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['cvv']; ?>" />
                        </fieldset>
                    </div>

                <?php } ?>

                <div class="right_section submit">
                    <button type="submit" class="button submit">
                        <?php
                        if ($business['id']) {
                            if ($business['status'] == 8) {
                                echo 'Renew Listing';
                            } else {
                                echo 'Save Listing';
                            }
                        } else {
                            echo 'Submit My Business';
                        }
                        ?>
                    </button>
                </div>

                <? /*
                <div class="right_section payment" style="padding: 15px; background-color: #fff; border-radius: 5px; opacity: .5;">

                    <?php if ($business['id']) { ?>

                        <strong>After you've submitted your business,</strong>

                        <p>
                           we will begin processing your listing and send you a confirmation email.
                        </p>

                        <p style="padding-top: 8px;">
                            Many listings go live within minutes<br />and you can update your listing(s)<br />as needed from your account.
                        </p>

                    <?php } else { ?>

                        <strong>After you've submitted your business,</strong>

                        <p>
                           we will begin processing your listing and send you a confirmation email.
                        </p>

                        <p style="padding-top: 8px;">
                            Many listings go live within minutes<br />and you can update your listing(s)<br />as needed from your account.
                        </p>

                    <?php } ?>

                </div>
                */ ?>

            </div>
        </td>

    </tr>
</table>

</form>

<?php include(__DIR__. '/_footer.php'); ?>