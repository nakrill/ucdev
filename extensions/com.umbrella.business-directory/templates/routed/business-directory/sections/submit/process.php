<?php

/*
// FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
if (($_FILES['logo']['name']) || ((!$_SESSION['POST']['BUSINESS-DIRECTORY']['image']) || (substr($_SESSION['POST']['BUSINESS-DIRECTORY']['image'], 0, 11) == 'resource://'))) {

    // GET CURRENT IMAGE
    $ex_query = "SELECT `image` FROM `" .$_uccms_businessdir->tables['categories']. "` WHERE (`id`=" .$id. ")";
    $ex = sqlfetch(sqlquery($ex_query));

    // THERE'S AN EXISTING IMAGE
    if ($ex['image']) {

        // REMOVE IMAGE
        @unlink($_uccms_businessdir->imageBridgeOut($ex['image'], 'categories', true));

        // UPDATE DATABASE
        $query = "UPDATE `" .$_uccms_businessdir->tables['categories']. "` SET `image`='' WHERE (`id`=" .$id. ")";
        sqlquery($query);

    }

}
*/

// USER OWNS LISTING
$owns = false;

// PAYMENT IS DUE
$payment_due = true;

// SAVE MORE INFO TO SESSION
$_SESSION['POST']['BUSINESS-DIRECTORY']['account'] = $_POST['account'];
$_SESSION['POST']['BUSINESS-DIRECTORY']['payment'] = $_POST['payment'];

// LOGGED IN
if ($_uccms['_account']->loggedIn()) {

    // USER ID
    $uid = $_uccms['_account']->userID();

// NOT LOGGED IN
} else {

    // CLEAN UP
    $email      = trim($_SESSION['POST']['BUSINESS-DIRECTORY']['account']['email']);
    $password   = trim($_SESSION['POST']['BUSINESS-DIRECTORY']['account']['password']);

    // HAVE EMAIL AND PASSWORD
    if (($email) && ($password)) {

        // ACCOUNT DETAILS
        $details = array(
            'firstname' => '',
            'lastname'  => ''
        );

        // CREATE ACCOUNT
        $create = $_uccms['_account']->register($email, $password, $email, $details, true);

        // LOGIN
        $login = $_uccms['_account']->login($email, $password, false, false);

        // USER ID
        $uid = $_uccms['_account']->userID();

        // HAVE USER ID
        if ($uid) {

            $_uccms['_site-message']->clear('error');

        // NO USER ID
        } else {

            // HAVE CREATE ERROR
            if ($create['error']) {
                $_uccms['_site-message']->set('error', $create['message']);
            }

        }

    // MISSING EMAIL AND/OR PASSWORD
    } else {
        $_uccms['_site-message']->set('error', 'Account email and password must be specified.');
        BigTree::redirect('../review/');
    }

}

// SUBMISSIONS ENABLED
if ($_uccms_businessdir->getSetting('submissions_enabled')) {

    // HAVE BUSINESS ID
    if ($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['id']) {

        // GET BUSINESS INFO
        $businessi_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .(int)$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['id']. ")";
        $businessi_q = sqlquery($businessi_query);
        $businessi = sqlfetch($businessi_q);

        // FOUND
        if ($businessi['id']) {

            // BUSINESS HAS ACCOUNT ID
            if ($businessi['account_id'] > 0) {

                // USER IS LOGGED IN
                if ($_uccms['_account']->loggedIn()) {

                    // USER IS OWNER
                    if ($businessi['account_id'] == $_uccms['_account']->userID()) {
                        $owns = true;

                    // USER IS NOT OWNER
                    } else {
                        $_uccms['_site-message']->set('error', 'Listing is owned by someone else.');
                        BigTree::redirect('../review/');
                    }

                // USER IS NOT LOGGED IN
                } else {
                    $_uccms['_site-message']->set('error', 'Listing is owned by someone else.');
                    BigTree::redirect('../review/');
                }

            // NO ACCOUNT ID
            } else {

                // CLAIMING NOT ENABLED
                if (!$_uccms_businessdir->getSetting('claiming_enabled')) {
                    $_uccms['_site-message']->set('error', 'Claiming of business listing is disabled.');
                    BigTree::redirect('../review/');
                }

            }

        // NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Business listing not found.');
            BigTree::redirect('../review/');
        }

    }

// SUBMISSIONS NOT ENABLED
} else {
    $_uccms['_site-message']->set('error', 'Submitting business listing is disabled.');
    BigTree::redirect('../review/');
}

// REQUIRED FIELDS ARRAY
$reqfa = array(
    'name',
    'address1',
    //'city',
    'state',
    'zip',
    'phone'
);

// MISSING FIELDS
$missa = array();

// CHECK REQUIRED FIELDS
foreach ($reqfa as $field) {
    if (!$_SESSION['POST']['BUSINESS-DIRECTORY']['business'][$field]) {
        $missa[] = $field;
    }
}

// HAVE MISSING FIELDS
if (count($missa) > 0) {
    foreach ($missa as $field) {
        $_uccms['_site-message']->set('error', 'Required field: ' .ucwords($field));
    }
    BigTree::redirect('../review/');
}

// HAVE USER ID
if ($uid) {

    // BUSINESS LISTING RECORD ID
    $record_id = 0;

    // WE HAVE AN ID
    if ($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['id']) {

        // GET LISTING
        $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .(int)$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['id']. ")";
        $business_q = sqlquery($business_query);
        $business = sqlfetch($business_q);

    }

    $city_id = (int)$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['city_id'];

    // HAVE CITY
    if ($city_id) {

        // GET CITY INFO
        $city_query = "SELECT * FROM `" .$_uccms_businessdir->tables['cities']. "` WHERE (`id`=" .$city_id. ")";
        $city_q = sqlquery($city_query);
        $city = sqlfetch($city_q);

        if ($city['title']) {
            $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['city'] = stripslashes($city['title']);
        }

    }

    // DB COLUMNS
    $columns = array(
        'slug'                      => $_uccms_businessdir->makeRewrite($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['name']),
        'account_id'                => $uid,
        'city_id'                   => $city_id,
        'neighborhood_id'           => (int)$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['neighborhood'][$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['city_id']]['neighborhood_id'],
        'name'                      => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['name'],
        'department'                => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['department'],
        'name_owner'                => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['name_owner'],
        'name_contact'              => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['name_contact'],
        'phone'                     => preg_replace('/\D/', '', $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone']),
        'phone_ext'                 => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone_ext'],
        'phone_tollfree'            => preg_replace('/\D/', '', $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone_tollfree']),
        'phone_tollfree_ext'        => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone_tollfree_ext'],
        'phone_alt'                 => preg_replace('/\D/', '', $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone_alt']),
        'phone_alt_ext'             => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone_alt_ext'],
        'phone_mobile'              => preg_replace('/\D/', '', $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone_mobile']),
        'fax'                       => preg_replace('/\D/', '', $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['fax']),
        'address1'                  => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['address1'],
        'address2'                  => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['address2'],
        'city'                      => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['city'],
        'state'                     => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['state'],
        'zip'                       => preg_replace('/\D/', '', $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['zip']),
        'zip4'                      => preg_replace('/\D/', '', $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['zip4']),
        'url'                       => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['url']),
        'email'                     => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['email'],
        'email_private'             => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['email_private'],
        'email_lead'                => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['email_lead'],
        'year_opened'               => date('Y', strtotime('01/01/' .$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['year_opened'])),
        'payment_methods'           => $_uccms_businessdir->arrayToCSV($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['payment_methods']),
        'languages'                 => $_uccms_businessdir->arrayToCSV($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['languages']),
        'description'               => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['description'],
        'tagline'                   => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['tagline'],
        'keywords'                  => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['keywords'],
        'social_facebook'           => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['facebook']),
        'social_foursquare'         => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['foursquare']),
        'social_googleplus'         => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['googleplus']),
        'social_instagram'          => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['instagram']),
        'social_linkedin'           => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['linkedin']),
        'social_pinterest'          => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['pinterest']),
        'social_tripadvisor'        => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['tripadvisor']),
        'social_twitter'            => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['twitter']),
        'social_yelp'               => $_uccms_businessdir->trimURL($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['social']['yelp'])
    );

    // HAVE LISTING
    if ($business['id']) {

        // UPDATE
        $business_query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET " .uccms_createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$business['id']. ")";

    // NO RECORD
    } else {

        // CREATE
        $business_query = "INSERT INTO `" .$_uccms_businessdir->tables['businesses']. "` SET `submitted`=1, " .uccms_createSet($columns). ", `updated_dt`=NOW(), `created_dt`=NOW(), `submitted_dt`=NOW()";

    }

    // BUSINESS SAVED
    if (sqlquery($business_query)) {

        // GET NEW RECORD ID
        if (!$business['id']) {
            $business['id'] = sqlid();
            $new_business = true;
        } else {
            $new_business = false;
        }

        // HAVE RECORD ID
        if ($business['id']) {

            // SAVE TO SESSION
            $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['id'] = $business['id'];

            // HAVE HOURS
            if (is_array($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['hours'])) {

                // LOOP
                foreach ($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['hours'] as $day => $hours) {

                    // DB COLUMNS
                    $columns = array(
                        'day'       => (int)$day,
                        'open'      => date('H:i', strtotime($hours['open'])),
                        'close'     => date('H:i', strtotime($hours['close'])),
                        'closed'    => (int)$hours['closed']
                    );

                    // SEE IF RECORD EXISTS
                    $hc_query = "SELECT `id` FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$business['id']. ") AND (`day`=" .(int)$day. ")";
                    $hc_q = sqlquery($hc_query);
                    $hc = sqlfetch($hc_q);

                    // RECORD EXISTS
                    if ($hc['id']) {

                        $query = "UPDATE `" .$_uccms_businessdir->tables['business_hours']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$hc['id']. ")";

                    // NO RECORD
                    } else {

                        $columns['business_id'] = $business['id'];

                        $query = "INSERT INTO `" .$_uccms_businessdir->tables['business_hours']. "` SET " .uccms_createSet($columns). "";

                    }

                    sqlquery($query);

                    unset($columns);

                }

            }

            $cata = array();

            // GET CURRENT RELATIONS
            $query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ")";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $cata[$row['category_id']] = $row['category_id'];
            }

            // HAVE CATEGORIES
            if (count($_SESSION['POST']['BUSINESS-DIRECTORY']['category']) > 0) {

                // GET MAXIMUM NUMBER OF CATEGORIES ALLOWED
                $max_categories = (int)$_uccms_businessdir->getSetting('listing_max_categories');

                $i = 1;

                // LOOP
                foreach ($_SESSION['POST']['BUSINESS-DIRECTORY']['category'] as $category_id) {

                    // MAX CATEGORIES
                    if ($max_categories > 0) {
                        if ($i > $max_categories) {
                            $_uccms['_site-message']->set('warning', 'Selected categories more than maximum (' .$max_categories. '). Used first ' .$i. ' categories.');
                            break;
                        }
                    }

                    // CLEAN UP
                    $category_id = (int)$category_id;

                    // HAVE CATEGORY ID
                    if ($category_id) {

                        // NOT IN EXISTING CATEGORY RELATIONS
                        if (!$cata[$category_id]) {

                            // DB COLUMNS
                            $columns = array(
                                'business_id'   => $business['id'],
                                'category_id'   => $category_id
                            );

                            // ADD RELATIONSHIP TO DB
                            $query = "INSERT INTO `" .$_uccms_businessdir->tables['business_categories']. "` SET " .uccms_createSet($columns). "";
                            sqlquery($query);

                        }

                        // REMOVE FROM RELATIONS
                        unset($cata[$category_id]);

                    }

                    $i++;

                }

            }

            // HAVE LEFT OVER (OLD) RELATIONS
            if (count($cata) > 0) {

                // LOOP
                foreach ($cata as $category_id) {

                    // REMOVE RELATIONSHIP FROM DB
                    $query = "DELETE FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ") AND (`category_id`=" .$category_id. ")";
                    sqlquery($query);

                }

            }

            // HAVE TEMP LOGO
            if ($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo']) {

                // LOCATIONS
                $temp_loc = SERVER_ROOT. 'site/extensions/' .$_uccms_businessdir->Extension. '/files/temp/' .$_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo'];
                $final_loc = 'extensions/' .$_uccms_businessdir->Extension. '/files/businesses';

                // MOVE FILE
                $storage = new BigTreeStorage;
                $logo = $storage->store($temp_loc, $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo'], $final_loc, true);

                // HAVE LOGO
                if ($logo) {

                    $lpa = explode('/', $logo);
                    $logo = array_pop($lpa);

                    // UPDATE BUSINESS
                    $logo_query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET `logo`='" .$logo. "' WHERE (`id`=" .$business['id']. ")";
                    if (sqlquery($logo_query)) {
                        unset($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo']);
                        $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['logo'] = $logo;
                    }

                }

            }

            // GET ACCOUNT EMAIL
            $account = $_uccms['_account']->getAccount('email');

            $payment_success = false;
            $payment_due = true;

            // GET DATE LAST CHARGED
            $charge_query = "SELECT * FROM `" .$_uccms_businessdir->tables['transaction_log']. "` WHERE (`business_id`=" .$business['id']. ") AND (`status`='charged') ORDER BY `dt` DESC LIMIT 1";
            $charge_q = sqlquery($charge_query);
            $charge = sqlfetch($charge_q);

            // HAVE CHARGED DATE
            if (($charge['dt']) && ($charge['dt'] != '0000-00-00 00:00:00')) {

                // PAYING MONTHLY
                if ($_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['term'] == 'mo') {
                    if (strtotime($charge['dt']) > strtotime('-1 Month')) {
                        $payment_due = false;
                    }

                // PAYING ANNUALLY
                } else {
                    if (strtotime($charge['dt']) > strtotime('-1 Year')) {
                        $payment_due = false;
                    }
                }

            }

            // PAYMENT REQUIRED
            if ($payment_due) {

                // GET PRICING
                if ($_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['term'] == 'mo') {
                    $price = $_uccms_businessdir->getSetting('listing_price_mo');
                } else {
                    $price = $_uccms_businessdir->getSetting('listing_price_yr');
                }

                // FREE
                if (strtolower($_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['number']) == 'toby') {
                    $price = 0;
                }

                // HAVE PRICE TO CHARGE
                if ($price) {

                    $missing = false;

                    // REQUIRE PAYMENT FIELDS
                    $reqfa = array(
                        'name'          => 'Name On Card',
                        'number'        => 'Card Number',
                        'expiration'    => 'Card Expiration',
                        'zip'           => 'Card Zip',
                        'cvv'           => 'Card CVV'
                    );

                    // CHECK REQUIRED
                    foreach ($reqfa as $name => $title) {
                        if (!$_SESSION['POST']['BUSINESS-DIRECTORY']['payment'][$name]) {
                            $missing = true;
                            $_uccms['_site-message']->set('error', 'Missing / Incorrect: Payment - ' .$title);
                        }
                    }

                    // HAVE MISSING FIELDS
                    if ($missing) {
                        BigTree::redirect('../review/');
                    }

                    // NEW PAYMENT GATEWAY
                    $gateway = new BigTreePaymentGateway();

                    // ADDRESS ARRAY
                    $address = array(
                        'street'    => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['address'],
                        'city'      => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['city'],
                        'state'     => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['state'],
                        'zip'       => $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['zip']
                    );

                    // PAYMENT EXPIRATION DATE FORMATTING
                    if ($_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['expiration']) {
                        $payment_expiration = date('Y-m-d', strtotime(str_replace('/', '/01/', $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['expiration'])));
                    } else {
                        $payment_expiration = '0000-00-00';
                    }

                    // PAYMENT LAST FOUR
                    $payment_lastfour = substr(preg_replace("/[^0-9]/", '', $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['number']), -4);

                    // CHARGE CARD (https://www.bigtreecms.org/docs/dev-guide/advanced/payment-gateway/)
                    $transaction_id = $gateway->charge($price, 0, $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['name'], $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['number'], $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['expiration'], $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['cvv'], $address, 'Listing ID: ' .$business['id'], $account_email, $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['phone'], $uid);

                    // TRANSACTION MESSAGE
                    $transaction_message = $gateway->Message;

                // FREE
                } else {
                    $transaction_id = 'free';
                }

                // LOG TRANSACTION
                $_uccms_businessdir->logTransaction(array(
                    'business_id'       => $business['id'],
                    'status'            => ($transaction_id ? 'charged' : 'failed'),
                    'amount'            => $price,
                    'term'              => $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['term'],
                    'method'            => 'credit-card',
                    'name'              => $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['name'],
                    'lastfour'          => $payment_lastfour,
                    'expiration'        => $payment_expiration,
                    'transaction_id'    => $transaction_id,
                    'message'           => $transaction_message,
                    'ip'                => ip2long($_SERVER['REMOTE_ADDR'])
                ));

                // CHARGE SUCCESSFUL
                if ($transaction_id) {

                    $payment_success = true;

                // CHARGE FAILED
                } else {

                    // UPDATE LISTING
                    $update_query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET `status`='6' WHERE (`id`=" .$business['id']. ")";
                    sqlquery($update_query);

                    // SEND BACK TO CHECKOUT
                    $_uccms['_site-message']->set('error', 'Credit Card Error (' .$gateway->Message. ')');
                    BigTree::redirect('../review/');

                }

            // NO PAYMENT REQUIRED
            } else {
                $payment_success = true;
            }

            // PAYMENT WAS SUCCESSFUL (OR NO PAYMENT DUE)
            if ($payment_success) {

                // UPDATE LISTING
                //$update_query = "UPDATE `" .$_uccms_businessdir->tables['businesses']. "` SET `status`='1' WHERE (`id`=" .$business['id']. ")";
                //sqlquery($update_query);

                $new_business = true;

                // NEW LISTING
                if ($new_business) {

                    ##########################
                    # EMAIL
                    ##########################

                    // GET EMAIL RELATED SETTINGS
                    $esitesetta = $_uccms_businessdir->getSettings(array('email_from_name','email_from_email','email_submission_copy'));

                    // EMAIL SETTINGS
                    $esettings = array(
                        'custom_template'   => SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/email/added.html',
                        'subject'           => 'Your Business Listing',
                        'listing_id'        => $business['id']
                    );

                    if ($esitesetta['email_from_name']) $esettings['from_name'] = $esitesetta['email_from_name'];
                    if ($esitesetta['email_from_email']) $esettings['from_email'] = $esitesetta['email_from_email'];

                    // EMAIL VARIABLES
                    $evars = array(
                        'heading'               => 'Business Listing Added',
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'listing_id'            => $business['id'],
                        'business_name'         => $_SESSION['POST']['BUSINESS-DIRECTORY']['business']['name'],
                        'total'                 => '$' .$price,
                        'listing_url'           => WWW_ROOT. 'account/business-directory/edit/?id=' .$business['id']
                    );

                    // WHO TO SEND EMAILS TO
                    $emailtoa = array(
                        $account['email'] => $_SESSION['POST']['BUSINESS-DIRECTORY']['payment']['name']
                    );

                    // COPY OF ORDER TO ADMIN(S)
                    if ($esitesetta['email_submission_copy']) {
                        $emails = explode(',', stripslashes($esitesetta['email_submission_copy']));
                        foreach ($emails as $email) {
                            $emailtoa[$email] = $email;
                        }
                    }

                    // HAVE EMAILS TO SEND TO
                    if (count($emailtoa) > 0) {

                        // LOOP
                        foreach ($emailtoa as $to_email => $to_name) {

                            // WHO TO SEND TO
                            $esettings['to_email'] = $to_email;

                            // SEND EMAIL
                            $result = $_uccms['_account']->sendEmail($esettings, $evars);

                            echo print_r($result);

                        }

                    }

                    $_uccms['_site-message']->set('success', 'Business listing added to your account.');

                // NOT NEW LISTING
                } else {
                    $_uccms['_site-message']->set('success', 'Business listing updated.');
                }

                // CLEAR SESSION DATA
                unset($_SESSION['POST']['BUSINESS-DIRECTORY']);

                BigTree::redirect('/account/business-directory/');

            // $payment_success = false
            } else {
                $_uccms['_site-message']->set('error', 'Failed to process your payment. Please contact us if this continues to happen.');
                BigTree::redirect('../review/');
            }

        // NO RECORD ID
        } else {
            $_uccms['_site-message']->set('error', 'Failed to save business data.');
            BigTree::redirect('../review/');
        }

    }

// NO USER ID
} else {
    $_uccms['_site-message']->set('error', 'Failed to create account.');
    BigTree::redirect('../review/');
}

?>