<?php

// CLEAR SESSION DATA
if ($_REQUEST['clear'] == true) {
    unset($_SESSION['POST']['BUSINESS-DIRECTORY']);
}

// PAYMENT METHODS
$pma = array();

// LANGUAGES
$la = array();

// HAVE SESSION INFO
if (is_array($_SESSION['POST']['BUSINESS-DIRECTORY']['business'])) {

    // USE SESSION INFO
    $business = $_SESSION['POST']['BUSINESS-DIRECTORY']['business'];

    // PAYMENT METHODS
    if (is_array($business['payment_methods'])) {
        $pma = $business['payment_methods'];
    }

    // LANGUAGES
    if (is_array($business['languages'])) {
        $la = $business['languages'];
    }

    // SOCIAL
    if (is_array($business['social'])) {
        foreach ($business['social'] as $what => $value) {
            $business['social_' .$what] = $value;
        }
    }

// CLAIMING
} else if ($_REQUEST['claim_id']) {

    // CLAIMING ENABLED
    if ($_uccms_businessdir->getSetting('claiming_enabled')) {

        // GET BUSINESS
        $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .(int)$_REQUEST['claim_id']. ")";
        $business_q = sqlquery($business_query);
        $business = sqlfetch($business_q);

        // FOUND
        if ($business['id']) {

            // NOT CLAIMED
            if ($business['account_id'] == 0) {

                // CLAIM MESSAGE
                $claim_message = $_uccms_businessdir->getSetting('claiming_content');

                // GET HOURS
                $hours_query = "SELECT * FROM `" .$_uccms_businessdir->tables['business_hours']. "` WHERE (`business_id`=" .$business['id']. ")";
                $hours_q = sqlquery($hours_query);
                while ($hours = sqlfetch($hours_q)) {
                    $business['hours'][$hours['day']] = $hours;
                }

                // PAYMENT METHODS
                $pma = explode(',', stripslashes($business['payment_methods']));

                // LANGUAGES
                $la = explode(',', stripslashes($business['languages']));

            // CLAIMED
            } else {
                $_uccms['_site-message']->set('error', 'Business listing is already claimed.');
                BigTree::redirect('../');
            }

        // NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Business listing not found.');
            BigTree::redirect('../');
        }

    // CLAIMING NOT ENABLED
    } else {
        $_uccms['_site-message']->set('error', 'Claiming of business listing is disabled.');
        BigTree::redirect('../');
    }

}

// SUBMISSIONS ENABLED
if ($_uccms_businessdir->getSetting('submissions_enabled')) {

    // ARRAY OF CITIES
    $citiesa = array();

    // GET CITIES
    $cities_query = "
    SELECT c.*
    FROM `" .$_uccms_businessdir->tables['cities']. "` AS `c`
    ORDER BY c.title ASC, c.id ASC
    ";
    $cities_q = sqlquery($cities_query);
    while ($agency = sqlfetch($cities_q)) {
        $citiesa[$agency['id']] = $agency;
    }

    // ARRAY OF NEIGHBORHOODS
    $neighborhoodsa = array();

    // GET NEIGHBORHOODS
    $neighborhoods_query = "
    SELECT n.*
    FROM `" .$_uccms_businessdir->tables['neighborhoods']. "` AS `n`
    ORDER BY n.title ASC, n.id ASC
    ";
    $neighborhoods_q = sqlquery($neighborhoods_query);
    while ($neighborhood = sqlfetch($neighborhoods_q)) {
        $neighborhoodsa[$neighborhood['city_id']][$neighborhood['id']] = $neighborhood;
    }

    // TREAT FORM AS EMBEDDED
    $bigtree['form']['embedded'] = true;

    ?>

    <style type="text/css">

        #listing input[type="text"], textarea {
            width: 85%;
        }
        #listing label.for_checkbox {
            display: inline;
        }

        #listing .section {
            padding-bottom: 30px;
        }
        #listing .section h4 {
            margin: 0px;
            padding: 0 0 3px 0;
            border-bottom: 1px solid <?php echo $bigtree['config']['css']['vars']['sitePrimary']; ?>;
        }
        #listing .section .content {
            padding: 15px;
            background-color: <?php echo $bigtree['config']['css']['vars']['siteThird']; ?>;
        }
        #listing .section .buttons {
            clear: both;
            margin-top: 10px;
        }

        .icon_sort {
            background: rgba(0, 0, 0, 0) url('/admin/images/icon-sprite.svg') repeat scroll -432px -80px;
            cursor: move;
            display: block;
            float: left;
            height: 24px;
            margin: 0 0 0 -7px;
            width: 24px;
        }
        .icon_delete, .icon_dialog_delete {
            background: rgba(0, 0, 0, 0) url('/admin/images/icon-sprite.svg') repeat scroll -312px -80px;
            display: inline-block;
            height: 24px;
            width: 24px;
        }

        .currently {
            height: 100px;
            margin: 0;
            padding: 0;
            width: 150px;
        }
        .currently .remove_resource {
            background: rgba(0, 0, 0, 0) url("/admin/images/icon-sprite.svg") repeat scroll -336px -140px;
            display: block;
            height: 16px;
            margin: -3px 0 -10px 139px;
            position: relative;
            width: 16px;
        }
        .currently_wrapper {
            height: 100px;
            overflow: hidden;
            width: 150px;
        }
        .currently label {
            background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
            color: #eee;
            display: block;
            font-size: 10px;
            height: 20px;
            line-height: 20px;
            margin: -20px 0 0;
            padding: 0 5px;
            position: relative;
            width: 100%;
        }
        .currently img {
            width: 150px;
        }

        #images .add_container {
            padding: 0 0 15px 0;
        }
        #images .add_container td {
            vertical-align: top;
        }
        #images .add_container fieldset {
            margin: 0px;
            padding: 0px;
        }
        #images section a, #categories section a {
            margin-top: 0px;
            margin-bottom: 0px;
        }
        #images .content > ul {
            margin: 0px;
            padding: 0px;
        }
        #images .content > ul li {
            position: relative;
            height: auto;
            margin-bottom: 15px;
            padding: 10px 10px 15px;
            background-color: <?php echo $bigtree['config']['css']['vars']['siteBG']; ?>;
            line-height: 1.2em;
        }
        #images .content > ul li:last-child {
            margin-bottom: 0px;
        }
        #images .content > ul li section {
            float: left;
            height: auto;
            overflow: visible;
            white-space: normal;
        }
        #images .content > ul li section:first-child {
            padding-left: 0px;
        }
        #images .items .item .image {
            padding: 0 25px;
        }
        #images .items .item .title {
        }
        #images .currently {
            margin-top: 8px;
        }
        #images .currently .remove_resource {
            margin-top: -3px;
            margin-bottom: -10px;
        }
        #images .items .item .title fieldset label {
            margin-bottom: 2px;
            text-align: left;
        }
        #images li .view_action {
            position: absolute;
            top: 10px;
            right: 10px;
        }

        #categories .max {
            margin-bottom: 10px;
            font-size: .9em;
            opacity: .8;
        }
        #categories ul {
            margin: 0px;
            padding: 0px;
            list-style: none;
        }
        #categories .content > ul {
            padding-left: 0px;
        }
        #categories .content > ul li {
            height: auto;
            padding: 0 0 10px 0;
            line-height: 1.2em;
        }
        #categories .content > ul li:last-child {
            padding-bottom: 0px;
        }
        #categories .content > ul li li {
            padding-bottom: 0px;
            padding-left: 20px;
            border: 0px none;
            background-color: transparent;
        }
        #categories .content > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }
        #categories .content > ul li section:first-child {
            padding-left: 0px;
        }
        #categories .content ul .checkbox {
            margin-top: -1px;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#listing').on('click', '.remove_resource', function(e) {
                e.preventDefault();
                var p = $(this).parent();
                if (p.hasClass('currently_file')) {
                    p.remove();
                } else {
                    p.hide().find('input, img').remove();
                }
                return false;
            });

            // CITY CHANGE
            $('#listing select[name="business[city_id]"]').change(function() {
                $('#listing .neighborhoods .city').hide();
                $('#listing .neighborhoods .city-' +$(this).val()).show();
            });

            // IMAGES - SORTABLE
            $('#images ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('id').replace('row_', ''));
                    });
                    $('#images input[name="sort"]').val(sort.join());
                }
            });

            // IMAGES - ADD CLICK
            $('#images .add_resource').click(function(e) {
                e.preventDefault();
                $('#images .add_container').toggle();
            });

            // IMAGES - REMOVE CLICK
            $('#images .icon_delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var id = $(this).attr('data-id');
                    var del = $('#images input[name="delete"]').val();
                    if (del) del += ',';
                    del += id;
                    $('#images input[name="delete"]').val(del);
                    $('#images li#row_' +id).fadeOut(500);
                }
            });

            // HOURS - CLOSED CLICK
            $('#hours .day input.closed').click(function() {
                $(this).closest('.day').find('select').prop('disabled', $(this).prop('checked'));
            });

        });

    </script>

    <?php include(__DIR__. '/_header.php'); ?>

    <h3 class="cl">Business Information</h3>

    <form enctype="multipart/form-data" action="./review/" method="post">
    <input type="hidden" name="business[id]" value="<?php echo $business['id']; ?>" />

    <div class="section grid">

        <div class="col-1-2">

            <fieldset>
                <label>Business Name <span class="required">*</span></label>
                <input type="text" value="<?=$business['name']?>" name="business[name]" />
            </fieldset>

            <fieldset>
                <label>Department</label>
                <input type="text" value="<?=$business['department']?>" name="business[department]" />
            </fieldset>

            <fieldset>
                <label>Owner Name</label>
                <input type="text" value="<?=$business['name_owner']?>" name="business[name_owner]" />
            </fieldset>

            <fieldset>
                <label>Contact Name</label>
                <input type="text" value="<?=$business['name_contact']?>" name="business[name_contact]" />
            </fieldset>

            <fieldset>
                <label>Address <span class="required">*</span></label>
                <input type="text" value="<?=$business['address1']?>" name="business[address1]" />
            </fieldset>

            <fieldset>
                <label>Address Cont.</label>
                <input type="text" value="<?=$business['address2']?>" name="business[address2]" />
            </fieldset>

            <? /*
            <fieldset>
                <label>City <span class="required">*</span></label>
                <input type="text" value="<?=$business['city']?>" name="business[city]" />
            </fieldset>
            */ ?>

            <?php if (count($citiesa) > 0) { ?>
                <fieldset>
                    <label>City</label>
                    <select name="business[city_id]">
                        <option value="0">None</option>
                        <?php foreach ($citiesa as $city) { ?>
                            <option value="<?php echo $city['id']; ?>" <?php if ($city['id'] == $business['city_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($city['title']); ?></option>
                        <?php } ?>
                    </select>
                </fieldset>
            <?php } ?>

            <?php if (count($neighborhoodsa) > 0) { ?>
            <fieldset class="neighborhoods">
                <label>Neighborhood</label>
                <?php foreach ($neighborhoodsa as $city_id => $neighborhoods) { ?>
                    <div class="city city-<?php echo $city_id; ?>" <?php if ($city_id != $business['city_id']) { ?>style="display: none;"<?php } ?>>
                        <select name="business[neighborhood][<?php echo $city_id; ?>][neighborhood_id]">
                            <option value="0">None</option>
                            <?php foreach ($neighborhoods as $neighborhood) { ?>
                                <option value="<?php echo $neighborhood['id']; ?>" <?php if ($neighborhood['id'] == $business['neighborhood_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($neighborhood['title']); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
            </fieldset>
            <?php } ?>

            <fieldset>
                <label>State <span class="required">*</span></label>
                <select name="business[state]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?=$state_code?>" <?php if ($state_code == $business['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset>
                <label>Zip</label>
                <input type="text" value="<?=$business['zip']?>" name="business[zip]" style="display: inline; width: 50px;" /><span class="required">*</span> - <input type="text" value="<?=$business['zip4']?>" name="business[zip4]" style="display: inline; width: 40px;" />
            </fieldset>

            <fieldset>
                <label>Phone <span class="required">*</span></label>
                <input type="text" value="<?=$business['phone']?>" name="business[phone]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$business['phone_ext']?>" name="business[phone_ext]" style="display: inline-block; width: 30px;" />
            </fieldset>

            <fieldset>
                <label>Toll-Free Number</label>
                <input type="text" value="<?=$business['phone_tollfree']?>" name="business[phone_tollfree]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$business['phone_tollfree_ext']?>" name="business[phone_tollfree_ext]" style="display: inline-block; width: 30px;" />
            </fieldset>

            <fieldset>
                <label>Alternate Phone Number</label>
                <input type="text" value="<?=$business['phone_alt']?>" name="business[phone_alt]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$business['phone_alt_ext']?>" name="business[phone_alt_ext]" style="display: inline-block; width: 30px;" />
            </fieldset>

            <fieldset>
                <label>Mobile</label>
                <input type="text" value="<?=$business['phone_mobile']?>" name="business[phone_mobile]" style="display: inline-block; width: 100px;" />
            </fieldset>

            <fieldset>
                <label>Fax</label>
                <input type="text" value="<?=$business['fax']?>" name="business[fax]" style="display: inline-block; width: 100px;" />
            </fieldset>

            <fieldset>
                <label>Email</label>
                <input type="text" value="<?=$business['email']?>" name="business[email]" />
            </fieldset>

            <fieldset>
                <label>Private Email</label>
                <input type="text" value="<?=$business['email_private']?>" name="business[email_private]" />
            </fieldset>

            <fieldset>
                <label>Lead Email</label>
                <input type="text" value="<?=$business['email_lead']?>" name="business[email_lead]" />
            </fieldset>

            <fieldset>
                <label>Website URL</label>
                <input type="text" value="<?=$business['url']?>" name="business[url]" placeholder="www.domain.com" />
            </fieldset>

        </div>

        <div class="col-1-2">

            <fieldset>
                <label>Logo</label>
                <div>
                    <?php
                    if ($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo']) {
                        $logo = $_uccms_businessdir->imageBridgeOut($_SESSION['POST']['BUSINESS-DIRECTORY']['business']['temp_logo'], 'temp');
                    } else if ($business['logo']) {
                        $logo = $_uccms_businessdir->imageBridgeOut($business['logo'], 'businesses');
                    } else {
                        $logo = '';
                    }
                    $field = array(
                        'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                        'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                        'key'       => 'logo', // The value you should use for the "name" attribute of your form field
                        'value'     => $logo, // The existing value for this form field
                        'id'        => 'blogo', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'image' => true
                        ),
                        'required'  => false // A boolean value of whether this form field is required or not
                    );
                    include(BigTree::path('admin/form-field-types/draw/upload.php'));
                    ?>
                </div>
            </fieldset>

            <fieldset>
                <label>Description</label>
                <textarea name="business[description]" style="height: 60px;"><?=$business['description']?></textarea>
            </fieldset>

            <fieldset>
                <label>Tagline</label>
                <input type="text" value="<?=$business['tagline']?>" name="business[tagline]" />
            </fieldset>

            <fieldset>
                <label>Keywords <small>Separate by comma.</small></label>
                <textarea name="business[keywords]" style="height: 60px;"><?=$business['keywords']?></textarea>
            </fieldset>

            <fieldset>
                <label>Year Opened</label>
                <select name="business[year_opened]">
                    <?php for ($i=date('Y'); $i>=1700; $i--) { ?>
                        <option value="<?=$i?>" <?php if ($i == $business['year_opened']) { ?>selected="selected"<?php } ?>><?=$i?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset>
                <label style="padding-bottom: 8px;">Payment Methods Accepted</label>
                <div class="clearfix">
                    <?php foreach ($_uccms_businessdir->paymentMethods() as $pmid => $name) { ?>
                        <div style="float: left; width: 50%; padding-bottom: 8px;">
                            <input type="checkbox" name="business[payment_methods][<?=$pmid?>]" value="<?=$pmid?>" <?php if (in_array($pmid, $pma)) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                        </div>
                    <?php } ?>
                </div>
            </fieldset>

            <fieldset>
                <label style="padding-bottom: 8px;">Languages Spoken</label>
                <div class="clearfix">
                    <?php foreach ($_uccms_businessdir->languages() as $lid => $name) { ?>
                        <div style="float: left; width: 50%; padding-bottom: 8px;">
                            <input type="checkbox" name="business[languages][<?=$lid?>]" value="<?=$lid?>" <?php if (in_array($lid, $la)) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                        </div>
                    <?php } ?>
                </div>
            </fieldset>

        </div>

    </div>

    <div class="section grid">

        <h4>Social</h4>

        <div class="content">

            <fieldset>
                <label>Facebook URL</label>
                <input type="text" value="<?=$business['social_facebook']?>" name="business[social][facebook]" placeholder="www.facebook.com/pagename" />
            </fieldset>

            <fieldset>
                <label>Foursquare URL</label>
                <input type="text" value="<?=$business['social_foursquare']?>" name="business[social][foursquare]" placeholder="www.foursquare.com/v/name/value" />
            </fieldset>

            <fieldset>
                <label>Google+ URL</label>
                <input type="text" value="<?=$business['social_googleplus']?>" name="business[social][googleplus]" placeholder="plus.google.com/+name" />
            </fieldset>

            <fieldset>
                <label>Instagram URL</label>
                <input type="text" value="<?=$business['social_instagram']?>" name="business[social][instagram]" placeholder="www.instagram.com/username" />
            </fieldset>

            <fieldset>
                <label>LinkedIn URL</label>
                <input type="text" value="<?=$business['social_linkedin']?>" name="business[social][linkedin]" placeholder="www.linkedin.com/company/companyname" />
            </fieldset>

            <fieldset>
                <label>Pinterest URL</label>
                <input type="text" value="<?=$business['social_pinterest']?>" name="business[social][pinterest]" placeholder="www.pinterest.com/username" />
            </fieldset>

            <fieldset>
                <label>TripAdvisor URL</label>
                <input type="text" value="<?=$business['social_tripadvisor']?>" name="business[social][tripadvisor]" placeholder="www.tripadvisor.com/URL" />
            </fieldset>

            <fieldset>
                <label>Twitter URL</label>
                <input type="text" value="<?=$business['social_twitter']?>" name="business[social][twitter]" placeholder="www.twitter.com/username" />
            </fieldset>

            <fieldset>
                <label>Yelp URL</label>
                <input type="text" value="<?=$business['social_yelp']?>" name="business[social][yelp]" placeholder="www.yelp.com/biz/name" />
            </fieldset>

        </div>

    </div>

    <a name="categories"></a>
    <div id="categories" class="section">

        <h4>Categories</h4>

        <div class="content">

            <?php
            $max_categories = (int)$_uccms_businessdir->getSetting('listing_max_categories');
            if ($max_categories > 0) {
                ?>
                <div class="max">
                    Maximum: <?php echo (int)$max_categories; ?>
                </div>
                <?php
            }

            // ARRAY OF CATEGORIES
            $cata = array();

            // HAVE ARRAY FROM SESSION
            if (is_array($_SESSION['POST']['BUSINESS-DIRECTORY']['category'])) {
                $cata = $_SESSION['POST']['BUSINESS-DIRECTORY']['category'];

            // HAVE BUSINESS ID
            } else if ($business['id']) {

                // GET CURRENT RELATIONS
                $query = "SELECT `category_id` FROM `" .$_uccms_businessdir->tables['business_categories']. "` WHERE (`business_id`=" .$business['id']. ")";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $cata[$row['category_id']] = $row['category_id'];
                }

            }

            // OUTPUT LIST
            function this_listElements($array) {
                global $cata;
                if (count($array) > 0) {
                    echo '<ul>';
                    foreach ($array as $element) {
                        if ($cata[$element['id']]) {
                            $checked = 'checked="checked"';
                        } else {
                            $checked = '';
                        }
                        echo '<li><input type="checkbox" name="category[' .$element['id']. ']" value="1" ' .$checked. ' /> ' .stripslashes($element['title']);
                        if (!empty($element['children'])) {
                            this_listElements($element['children']);
                        }
                        echo '</li>';
                    }
                    echo '</ul>';
                }
            }

            // BUILD LIST OFF ALL CATEGORIES
            echo this_listElements($_uccms_businessdir->getCategoryTree());

            ?>

        </div>

    </div>

    <a name="hours"></a>
    <div id="hours" class="section">

        <h4>Hours</h4>

        <div class="content">

            <table style="margin: 0px; border: 0px;">
                <?php

                // LOOP THROUGH DAYS OF WEEK
                foreach ($_uccms_businessdir->daysOfWeek() as $day_code => $day_name) {

                    // THIS DAY
                    $this_day = $business['hours'][$day_code];

                    // CLOSED OR NOT
                    $closed = $this_day['closed'];

                    ?>
                    <tr class="day">
                        <td style="white-space: nowrap; font-weight: bold;"><?=$day_name?></td>
                        <td style="white-space: nowrap;">
                            <select name="business[hours][<?=$day_code?>][open]" <?php if ($closed) { ?>disabled="disabled"<?php } ?>class="custom_control">
                                <?php
                                for ($i=1; $i<=24; $i++) {
                                    for ($j=0; $j<=45; $j+=15) {
                                        if ($closed) {
                                            $selected = false;
                                        } else {
                                            if (date('H:i:s', strtotime($i. ':' .$j)) == date('H:i:s', strtotime($this_day['open']))) {
                                                $selected = true;
                                            } else {
                                                $selected = false;
                                            }
                                        }
                                        ?>
                                        <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            &nbsp;-&nbsp;
                            <select name="business[hours][<?=$day_code?>][close]" <?php if ($closed) { ?>disabled="disabled"<?php } ?> class="custom_control">
                                <?php
                                for ($i=1; $i<=24; $i++) {
                                    for ($j=0; $j<=45; $j+=15) {
                                        if ($closed) {
                                            $selected = false;
                                        } else {
                                            if (date('H:i:s', strtotime($i. ':' .$j)) == date('H:i:s', strtotime($this_day['close']))) {
                                                $selected = true;
                                            } else {
                                                $selected = false;
                                            }
                                        }
                                        ?>
                                        <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td style="width: 100%; padding-top: 3px;"><input type="checkbox" name="business[hours][<?=$day_code?>][closed]" value="1" <?php if ($closed) { ?>checked="checked"<?php } ?>class="closed" /> Closed</td>
                    </tr>
                <?php } ?>
            </table>

        </div>

    </div>

    <div class="buttons">
        <input type="submit" value="Review" />
    </div>

    </form>

    <?php include(__DIR__. '/_footer.php'); ?>

    <?php

// SUBMISSIONS NOT ENABLED
} else {
    $_uccms['_site-message']->set('error', 'Submitting business listing is disabled.');
    BigTree::redirect('../');
}

?>