<style type="text/css">

    #blSteps {
        margin: 0 0 40px;
        text-align: center;
    }
    #blSteps ul {
        display: inline-block;
        padding: 0px;
        border: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
        list-style: none;
        text-align: center;
        border-radius: 6px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
    }
    #blSteps ul li {
        display: inline-block;
        width: 150px;
        margin: 0 -4px 0 0;
        padding: 15px 15px 12px;
        border-right: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
        background-color: <?php echo $bigtree['config']['css']['vars']['sitePrimary']; ?>;
        opacity: 0.8;
    }
    #blSteps ul li.active {
        background-color: #fff;
        opacity: 1;
    }
    #blSteps ul li:first-child {
        border-radius: 6px 0px 0px 6px;
        -moz-border-radius: 6px 0px 0px 6px;
        -webkit-border-radius: 6px 0px 0px 6px;
    }
    #blSteps ul li:last-child {
        margin-right: 0px;
        border-right: 0px none;
        border-radius: 0px 6px 6px 0px;
        -moz-border-radius: 0px 6px 6px 0px;
        -webkit-border-radius: 0px 6px 6px 0px;
    }
    #blSteps ul li i {
        display: block;
        margin-top: 0px;
        margin-bottom: 7px;
        font-size: 2em;
    }
    #blSteps ul li strong {
        display: block;
    }

    #listing {
        margin-bottom: 30px;
    }

    #listing h3.cl {
        margin: 0px 0px 15px 0;
        padding: 0px 0px 4px 0;
        border-bottom: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
        text-align: center;
        font-size: 2.2em;
        font-weight: normal;
        letter-spacing: 1px;
    }

    #listing input[type="text"], #listing input[type="password"] {
        margin: 0px;
        padding: 4px 8px;
        border: 1px solid #888;
        background-color: #fff;
        font-size: 1.2em;
        color: #444;
        border-radius: 4px;
    }
    #listing input[type="text"]:focus {
        border: 1px solid #000;
    }
    #listing textarea {
        padding: 5px;
        border: 1px solid #888;
        border-radius: 4px;
    }

    #listing span.required {
        color: red;
    }

</style>

<?php

// ACTIVE STEP
if ($bigtree['commands'][1]) {
    $bnsa[$bigtree['commands'][1]] = 'active';
} else {
    $bnsa['info'] = 'active';
}

?>

<div id="blSteps">

    <ul>
        <li class="<?=$bnsa['info']?>">
            <? /*<i class="fa fa-map"></i>*/ ?>
            <strong>Step 1:</strong> Business Info
        </li>
        <li class="<?=$bnsa['review']?>">
            <? /*<i class="fa fa-check"></i>*/ ?>
            <strong>Step 2:</strong> Review & Submit
        </li>
    </ul>

</div>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div id="listing">

