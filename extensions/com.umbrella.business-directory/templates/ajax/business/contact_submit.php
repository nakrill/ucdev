<?php

$out = array();

// BUSINESS ID
$business_id = (int)$_REQUEST['business_id'];

// HAVE ID
if ($business_id) {

    // MODULE CLASS
    $_uccms_businessdir = new uccms_BusinessDirectory;

    // GET BUSINESS
    $business_query = "SELECT * FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`id`=" .$business_id. ") AND (`status`=1)";
    $business_q = sqlquery($business_query);
    $business = sqlfetch($business_q);

    // BUSINESS FOUND
    if ($business['id']) {

        // CONTACT FORM ENABLED
        if (($_uccms_businessdir->getSetting('listing_contact_enabled')) && (($business['email']) || ($business['email_lead'])) && (count($_uccms_businessdir->listingContactFields()) > 0)) {

            $missing = array();
            $efields = array();
            $jfields = array();

            // LOOP THROUGH FIELDS
            foreach ($_uccms_businessdir->listingContactFields() as $field_id => $field) {
                $efields[] = $field['title']. ': ' .$_REQUEST['field'][$field_id];
                $jfields[$field['title']] = $_REQUEST['field'][$field_id];
                if ($field['required']) {
                    if (!$_REQUEST['field'][$field_id]) {
                        $missing[] = $field['title'];
                    }
                }
            }

            // GET reCAPTCHA SETTINGS
            $recaptcha_sitekey      = $_uccms_businessdir->getSetting('recaptcha_sitekey');
            $recaptcha_secretkey    = $_uccms_businessdir->getSetting('recaptcha_secretkey');

            // reCAPTCHA ENABLED
            if (($recaptcha_sitekey) && ($recaptcha_secretkey)) {
                $response = json_decode(BigTree::cURL('https://www.google.com/recaptcha/api/siteverify?secret=' .$recaptcha_secretkey. '&response=' .urlencode($_REQUEST['g-recaptcha-response']). '&remoteip=' .$_SERVER['REMOTE_ADDR']));
                if (!$response->success) {
                    $missing[] = 'Human verification';
                }
            }

            // HAVE MISSING
            if (count($missing) > 0) {
                $out['err'] = 'Required fields are missing.';
                $out['missing_fields'] = $missing;

            // GOOD TO GO
            } else {

                if ($_uccms_businessdir->getSetting('listing_contact_success')) {
                    $out['success'] = stripslashes($_uccms_businessdir->getSetting('listing_contact_success'));
                } else {
                    $out['success'] = 'Business contacted!';
                }

                ##########################
                # EMAIL
                ##########################

                // GET EMAIL RELATED SETTINGS
                //$esitesetta = $_uccms_businessdir->getSettings(array('email_from_name','email_from_email','email_submission_copy'));

                // EMAIL SETTINGS
                $esettings = array(
                    'custom_template'   => SERVER_ROOT. 'extensions/' .$_uccms_businessdir->Extension. '/templates/email/business/contact.html',
                    'subject'           => 'Business Contact',
                    'listing_id'        => $business['id']
                );

                //if ($esitesetta['email_from_name']) $esettings['from_name'] = $esitesetta['email_from_name'];
                //if ($esitesetta['email_from_email']) $esettings['from_email'] = $esitesetta['email_from_email'];

                // EMAIL VARIABLES
                $evars = array(
                    'heading'               => 'Business Contact',
                    'date'                  => date('n/j/Y'),
                    'time'                  => date('g:i A T'),
                    'listing_id'            => $business['id'],
                    'business_name'         => stripslashes($business['name']),
                    'page_url'              => '',
                    'fields'                => implode('<br />', $efields)
                );

                // WHO TO SEND TO
                if ($business['email_lead']) {
                    $esettings['to_email'] = stripslashes($business['email_lead']);
                } else {
                    $esettings['to_email'] = stripslashes($business['email']);
                }

                // SEND EMAIL
                $result = $_uccms['_account']->sendEmail($esettings, $evars);

                //$out['email_result'] = $result;

                ##########################

                // RECORD EMAIL
                $email_log_id = $_uccms_businessdir->logBusinessEmail($business['id'], $jfields);

                // LOG EMAIL STAT
                $_uccms_businessdir->business_logStat($business['id'], 'email', $email_log_id);

            }

        // CONTACT FORM NOT ENABLED
        } else {
            $out['err'] = 'Contact form not available.';
        }

    // BUSINESS NOT FOUND
    } else {
        $out['err'] = 'Business not found.';
    }

} else {
    $out['err'] = 'Business ID not specified.';
}

echo json_encode($out);

?>