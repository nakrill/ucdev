$(document).ready(function() {

    // RATING STARS CLICK
    $('#businessdirContainer .item_container .rating_stars .stars, #businessdirContainer .item_container .rating_stars a').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('#businessdirContainer .item_container .reviews').offset().top
        }, 1500);
    });

    // SWIPEBOX
    $('#businessdirContainer .swipebox, #businessdirContainer .swipebox-video').swipebox({
        removeBarsOnMobile: false
    });

    // EQUAL COLUMN HEIGHT
    $('#businessdirContainer .item_container .info_cols .right').height($('#businessdirContainer .item_container .info_cols .left').height());

    // CONTACT FORM SUBMIT
    $('#businessdirContainer .contact form').submit(function(e) {
        e.preventDefault();
        $('#businessdirContainer .contact .site-message').hide().html('');
        $.post('/*/com.umbrella.business-directory/ajax/business/contact_submit/', $(this).serialize(), function(data) {
            if (data['success']) {
                $('#businessdirContainer .contact .site-message.type-success').html(data['success']).show();
            } else {
                if (data['err']) {
                    $('#businessdirContainer .contact .site-message.type-error').html(data['err']).show();
                    if (data['missing_fields']) {
                        var missing_html = '';
                        $.each(data['missing_fields'], function(index, value) {
                            missing_html += '<li>' +value+ '</li>';
                        });
                        $('#businessdirContainer .contact .site-message.type-error').append('<ul>' +missing_html+ '</ul>');
                    }
                } else {
                    alert('Failed to submit contact form.');
                }
            }
        }, 'json');
    });

});