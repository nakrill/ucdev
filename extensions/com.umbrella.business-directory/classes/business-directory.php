<?
    class uccms_BusinessDirectory extends BigTreeModule {

        var $Table = "";

        var $Extension = "com.umbrella.business-directory";

        // DB TABLES
        var $tables = array(
            'businesses'                => 'uccms_businessdirectory_businesses',
            'business_categories'       => 'uccms_businessdirectory_business_category_rel',
            'business_ecommerce_items'  => 'uccms_businessdirectory_business_ecommerce_item_rel',
            'business_email_log'        => 'uccms_businessdirectory_email_log',
            'business_features'         => 'uccms_businessdirectory_business_features_rel',
            'business_feature_values'   => 'uccms_businessdirectory_business_feature_values',
            'business_hours'            => 'uccms_businessdirectory_business_hours',
            'business_images'           => 'uccms_businessdirectory_business_images',
            'business_stats_log'        => 'uccms_businessdirectory_business_stats_log',
            'business_videos'           => 'uccms_businessdirectory_business_videos',
            'categories'                => 'uccms_businessdirectory_categories',
            'category_features'         => 'uccms_businessdirectory_category_features_rel',
            'cities'                    => 'uccms_businessdirectory_cities',
            'features'                  => 'uccms_businessdirectory_features',
            'feature_groups'            => 'uccms_businessdirectory_feature_groups',
            'feature_options'           => 'uccms_businessdirectory_feature_options',
            'neighborhoods'             => 'uccms_businessdirectory_neighborhoods',
            'payment_methods'           => 'uccms_businessdirectory_settings_payment_methods',
            'search_log'                => 'uccms_businessdirectory_search_log',
            'settings'                  => 'uccms_businessdirectory_settings',
            'transaction_log'           => 'uccms_businessdirectory_transaction_log'
        );

        // BUSINESS LISTING STATUSES
        var $statuses = array(
            0 => 'Inactive',
            1 => 'Active',
            5 => 'Pending',
            9 => 'Deleted'
        );

        var $settings, $misc;

        // START AS 0, WILL ADJUST LATER
        static $PerPage = 0;


        function __construct() {

        }


        #############################
        # GENERAL - MISC
        #############################


        // FRONTEND PATH
        public function frontendPath() {
            if (!isset($this->settings['frontend_path'])) {
                $this->settings['frontend_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*business-directory')"))['path']);
            }
            if (!$this->settings['frontend_path']) $this->settings['frontend_path'] = 'directory';
            return $this->settings['frontend_path'];
        }


        // MODULE ID
        public function moduleID() {
            if (!isset($this->settings['module_id'])) {
                $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
            }
            return $this->settings['module_id'];
        }


        // ADMIN USER ID
        public function adminID() {
            return $_SESSION['bigtree_admin']['id'];
        }


        // ADMIN ACCESS LEVEL
        public function adminModulePermission() {
            global $admin;
            if (!isset($this->settings['admin_module_permission'])) {
                $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
            }
            return $this->settings['admin_module_permission'];
        }


        // STRING IS JSON
        public function isJson($string) {
            if (substr($string, 0, 2) == '{"') {
                return true;
            }
        }


        // CREATE A MYSQL SET
        public function createSet($fields=array()) {
            return uccms_createSet($fields);
        }


        // CONVERTS A STRING INTO A REWRITE-ABLE URL
        public function makeRewrite($string) {

            $string = strtolower(trim($string));

            // REMOVE ALL QUOTES
            $string = str_replace("'", '', $string);
            $string = str_replace('"', '', $string);

            // STRIP ALL NON WORD CHARS
            $string = preg_replace('/\W/', ' ', $string);

            // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
            $string = preg_replace('/\ +/', '-', $string);

            // TRIM DASHES
            $string = preg_replace('/\-$/', '', $string);
            $string = preg_replace('/^\-/', '', $string);

            return $string;

        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
        public function imageBridgeIn($string) {
            $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $dpa = explode('/', $string);
                return array_pop($dpa);
                //$what = array_pop($dpa);
            }
        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
        public function imageBridgeOut($string, $what, $file=false) {
            $string = stripslashes($string);
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $base = ($file ? SITE_ROOT : STATIC_ROOT);
                return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
            }
        }


        // CONVERT BIGTREE FILE LOCATION TO FULL URL
        public function bigtreeFileURL($file) {
            return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
        }


        // SET NUMBER PER PAGE
        public function setPerPage($num) {
            static::$PerPage = (int)$num;
        }


        // NUMBER PER PAGE
        public function perPage() {
            global $cms;
            if (!static::$PerPage) $this->setPerPage($this->getSetting('listings_per_page'));
            if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
            if (!static::$PerPage) $this->setPerPage(15);
            return static::$PerPage;
        }


        // GET NUMBER OF PAGES FROM QUERY
        public function pageCount($num) {
            return ceil($num / $this->perPage());
        }


        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            );
        }


        // DAYS OF WEEK FOR SCHEMA.ORG
        public function daysOfWeek_Schema() {
            return array(
                1 => 'Mo',
                2 => 'Tu',
                3 => 'We',
                4 => 'Th',
                5 => 'Fr',
                6 => 'Sa',
                7 => 'Su'
            );
        }


        // LANGUAGES
        public function languages() {
            return array(
                'EN' => 'English',
                'ZH' => 'Chinese',
                'FR' => 'French',
                'DE' => 'German',
                'IT' => 'Italian',
                'JA' => 'Japanese',
                'KO' => 'Korean',
                'PT' => 'Portuguese',
                'RU' => 'Russian',
                'ES' => 'Spanish',
                'TH' => 'Thai'
            );
        }


        // PAYMENT METHODS
        public function paymentMethods() {
            return array(
                1 => 'All',
                2 => 'Visa',
                3 => 'Mastercard',
                4 => 'American Express',
                5 => 'Discover',
                6 => 'Diners',
                7 => 'Cash',
                8 => 'Check',
                9 => 'Debit'
            );
        }


        // SOCIAL NETWORKS
        public function socialNetworks() {
            return array(
                'facebook'      => array(
                    'title'     => 'Facebook',
                    'icon'      => 'fa-facebook'
                ),
                'foursquare'    => array(
                    'title'     => 'Foursquare',
                    'icon'      => 'fa-foursquare'
                ),
                'googleplus'    => array(
                    'title'     => 'Google+',
                    'icon'      => 'fa-google-plus'
                ),
                'instagram'     => array(
                    'title'     => 'Instagram',
                    'icon'      => 'fa-instagram'
                ),
                'linkedin'      => array(
                    'title'     => 'LinkedIn',
                    'icon'      => 'fa-linkedin'
                ),
                'tripadvisor'   => array(
                    'title'     => 'TripAdvisor',
                    'icon'      => 'fa-tripadvisor'
                ),
                'pinterest'     => array(
                    'title'     => 'Pinterest',
                    'icon'      => 'fa-pinterest'
                ),
                'twitter'       => array(
                    'title'     => 'Twitter',
                    'icon'      => 'fa-twitter'
                ),
                'yelp'          => array(
                    'title'     => 'Yelp',
                    'icon'      => 'fa-yelp'
                )
            );
        }


        // RETURNS ARRAY OF LISTING CONTACT FIELDS
        public function listingContactFields() {
            $out = $this->getSetting('listing_contact_fields');
            if (($out) && (!is_array($out))) {
                $out = json_decode($out, true);
            }
            if ((!$out) || ((is_array($out) && (count($out) == 0)))) {
                $out = array();
            }
            return $out;
        }


        // CLEAN A PHONE NUMBER (TO JUST NUMBERS)
        public function cleanPhone($number) {
            return trim(preg_replace('/\D/', '', $number));
        }


        // FORMAT PHONE NUMBER
        public function prettyPhone($number) {
            $number = preg_replace('/\D/', '', $number);
            return '(' .substr($number, 0, 3). ') ' .substr($number, 3, 3). '-' .substr($number, 6);
        }


        // TRIM URL
        public function trimURL($string) {
            return trim(preg_replace('#^https?://#', '', $string));
        }


        // CONVERT ARRAY TO CSV
        public function arrayToCSV($array=array()) {
            if (is_array($array)) {
                return implode(',', $array);
            }
        }


        // FORMAT ADDRESS
        public function formatAddress($business, $sep='<br />') {
            if ($business['address1']) $ba[] = stripslashes($business['address1']);
            if ($business['address2']) $ba[] = stripslashes($business['address2']);
            if ($business['unit']) $ba[] = stripslashes($business['unit']);
            if (($business['city']) && ($business['state'])) {
                $ba2[] = stripslashes($business['city']). ', ';
            } else if ($business['city']) {
                $ba2[] = stripslashes($business['city']);
            }
            if ($business['state']) $ba2[] = stripslashes($business['state']);
            if ($business['zip']) $ba2[] = stripslashes($business['zip']);
            if (count((array)$ba2) > 0) {
                $ba[] = implode(' ', $ba2);
            }
            if (count((array)$ba) > 0) {
                return implode($sep, $ba);
            }
        }


        #############################
        # GENERAL - SETTINGS
        #############################


        // GET SETTING
        public function getSetting($id='') {
            $id = sqlescape($id);
            if ($id) {
                if (isset($this->settings[$id])) {
                    return $this->settings[$id];
                } else {
                    $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tables['settings']. "` WHERE `id`='" .$id. "'"));
                    if ($this->isJson($f['value'])) {
                        $val = json_decode($f['value'], true);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$id] = $val;
                }
                return $val;
            }
        }


        // GET SETTINGS
        public function getSettings($array='') {
            $out = array();
            if (is_array($array)) {
                $oa = array();
                foreach ($array as $id) {
                    $id = sqlescape($id);
                    if ($id) {
                        $oa[] = "`id`='" .$id. "'";
                    }
                }
                if (count($oa) > 0) {
                    $or = "(" .implode(") OR (", $oa). ")";
                }
                if ($or) {
                    $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` WHERE " .$or;
                }
            } else {
                $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` ORDER BY `id` ASC";
            }
            if ($query) {
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    if ($this->isJson($string)) {
                        $val = json_decode($f['value']);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$f['id']] = $val;
                    $out[$f['id']] = $val;
                }
            }
            return $out;
        }


        // SET SETTING
        public function setSetting($id='', $value='') {

            // CLEAN UP
            $id = sqlescape($id);

            // HAVE ID
            if ($id) {

                // VALUE IS ARRAY
                if (is_array($value)) {
                    $value = json_encode($value);

                // VALUE IS STRING
                } else {
                    $value = sqlescape($value);
                }

                $query = "
                INSERT INTO `" .$this->tables['settings']. "`
                SET `id`='" .$id. "', `value`='" .$value. "'
                ON DUPLICATE KEY UPDATE `value`='" .$value. "'
                ";

                if (sqlquery($query)) {
                    return true;
                }

            }

        }


        #############################
        # GENERAL - CATEGORIES
        #############################


        public function getCategoryTree($parent=0, $active='', $depth=100) {

            $out = array();

            // CLEAN UP
            $parent = (int)$parent;

            if ($active === true) {
                $active_sql = "WHERE (active=1)";
            } else if ($active === false) {
                $active_sql = "WHERE (active=0)";
            } else {
                $active_sql = "";
            }

            // GET CATEGORIES
            $query = "SELECT * FROM `" .$this->tables['categories']. "` " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($item = sqlfetch($q)) {
                $ref = & $refs[$item['id']];
                foreach ($item as $name => $value) {
                    $ref[$name] = $value;
                }
                if ($item['parent'] == 0) {
                    $out[$item['id']] = & $ref;
                } else {
                    $refs[$item['parent']]['children'][$item['id']] = & $ref;
                }
            }

            unset($refs);

            // PARENT SPECIFIED
            if ($parent) {
                return $this->findArrayKey($out, $parent);
            // NO PARENT SPECIFIED
            } else {
                return $out;
            }

            return $out;

        }


        // OUTPUT THE CATEGORY TREE IN A DROPDOWN (TREE ARRAY, SELECTED ID,
        function printCategoryDropdown($tree, $selected=0, $id=0, $parent=0, $r=0) {
            foreach ($tree as $i => $t) {
                $dash = ($t['parent'] == 0) ? '' : str_repeat('-', $r) .' ';
                $sel = ($t['id'] == $selected ? ' selected="selected"' : '');
                $dis = ($t['id'] == $id ? ' disabled="disabled"' : '');
                echo "\t" . '<option value="' .$t['id']. '"' .$sel . $dis. '>' .$dash . stripslashes($t['title']). '</option>' . "\n";
                if (isset($t['children'])) {
                    $this->printCategoryDropdown($t['children'], $selected, $id, $t['parent'], $r+1);
                }
            }
        }


        // GET A SUB-ARRAY BY MATCHING KEY
        public function findArrayKey($array, $index) {
            if (!is_array($array)) return null;
            if (isset($array[$index])) return $array[$index];
            foreach ($array as $item) {
                $return = $this->findArrayKey($item, $index);
                if (!is_null($return)) {
                    return $return;
                }
            }
            return null;
        }


        // GET THE PARENT(S) OF A CATEGORY
        public function getCategoryParents($id=0) {
            $out = array();
            $id = (int)$id;
            if ($id > 0) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .$id. ")";
                $cat = sqlfetch(sqlquery($query));
                array_push($out, $cat);
                if ($cat['parent']) {
                    $out = array_merge($out, $this->getCategoryParents($cat['parent']));
                }
            }
            return $out;
        }


        // GET SUB-CATEGORIES
        public function subCategories($id, $active=null, $by_pricegroup=false) {
            $out = array();

            if ($active === true) {
                $active_sql = " AND (`active`=1)";
            } else if ($active === false) {
                $active_sql = " AND (`active`=0)";
            } else {
                $active_sql = "";
            }

            // GET SUB CATEGORIES FROM DB
            $sub_query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`parent`=" .(int)$id. ") " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $sub_q = sqlquery($sub_query);
            while ($sub = sqlfetch($sub_q)) {
                $out[] = $sub;
            }

            return $out;

        }


        // GET BUSINESSES IN CATEGORY WITH PAGING
        public function categoryBusinesses($id, $vars=array()) {
            global $_paging;

            $out = array();

            if ($id) {

                // SORT
                if ($vars['sort'] == 'name_asc') {
                    $sql_sort = "b.name ASC, b.id ASC";
                } else {
                    $sql_sort = "RAND()";
                }

                // HOW MANY LISTINGS PER PAGE
                if ($vars['limit']) $limit = (int)$vars['limit']; // SPECIFIED DIRECTLY
                if (!$limit) $limit = $this->getSetting('listings_per_page'); // USE EXTENSION SETTING
                if (!$limit) $limit = $this->perPage(); // USE BIGTREE / DEFAULT

                // CURRENT PAGE
                $page = isset($vars['page']) ? intval($vars['page']) : 1;

                // GET BUSINESSES
                $business_query = "
                SELECT b.*
                FROM `" .$this->tables['businesses']. "` AS `b`
                INNER JOIN `" .$this->tables['business_categories']. "` AS `bc` ON b.id=bc.business_id
                WHERE (bc.category_id=" .$id. ") AND (b.status=1)
                ORDER BY " .$sql_sort. "
                LIMIT " .(($page - 1) * $limit). "," .$limit. "
                ";
                $business_q = sqlquery($business_query);
                while ($business = sqlfetch($business_q)) {
                    $out[] = $business;
                }

                // HAVE PAGING CLASS
                if ($_paging) {

                    // PREPARE PAGING (RESULTS PER PAGE, NUMBER OF PAGE NUMBERS, CURRENT PAGE, REQUEST VARIABLES (_GET, _POST), USE MOD_REWRITE)
                    $_paging->prepare($limit, 5, $page, $_GET, false);

                }

            }

            return $out;

        }


        // GET TOTAL BUSINESSES IN CATEGORY
        public function categoryBusinessesTotal($id, $vars=array()) {

            $count = 0;

            if ($id) {

                // GET BUSINESSES
                $business_query = "
                SELECT b.id
                FROM `" .$this->tables['businesses']. "` AS `b`
                INNER JOIN `" .$this->tables['business_categories']. "` AS `bc` ON b.id=bc.business_id
                WHERE (bc.category_id=" .$id. ") AND (b.status=1)
                ";
                $business_q = sqlquery($business_query);
                $count = sqlrows($business_q);

            }

            return $count;

        }


        // GENERATE CATEGORY URL
        public function categoryURL($id, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/' .trim($slug). '-' .$id. '/';
        }


        // BREADCRUMB OUTPUT
        public function generateBreadcrumbs($array, $options=array(), $echo_string=true) {

            // OPTIONS
            if (!$options['base']) $options['base'] = './';
            if (!$options['home_title']) $options['home_title'] = 'Directory'; // MATTD: Make into a configurable setting
            if (!$options['sep']) $options['sep'] = ' / '; // MATTD: Make into a configurable setting

            // ELEMENTS
            $els = array();

            // SHOW HOME LINK
            if (!$options['no_home']) {
                $els[] = '<a href="' .$options['base']. '">' .$options['home_title']. '</a>';
            }

            // NUMBER OF PARENT CATEGORIES
            $num_cat = count((array)$array);

            // HAVE PARENT CATEGORIES
            if ($num_cat > 0) {

                // REVERSE ARRAY FOR EASIER OUTPUT
                $tcat = array_reverse($array);

                $i = 1;

                // LOOP
                foreach ($tcat as $cat) {
                    if ($options['rewrite']) {
                        $url = $this->categoryURL($cat['id'], $cat);
                    } else {
                        $url = $options['base']. '?id=' .$cat['id'];
                    }
                    $line = '';
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '<a href="' .$url. '">';
                    }
                    $line .= stripslashes($cat['title']);
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '</a>';
                    }
                    $els[] = $line;
                    $i++;
                }

            }

            if ($echo_string) {
                echo implode($options['sep'], $els);
            } else {
                return $els;
            }

        }


        #############################
        # CITY
        #############################


        // GENERATE CITY URL
        public function cityURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            return '/' .$this->frontendPath(). '/' .$data['slug']. '/';
        }


        #############################
        # NEIGHBORHOOD
        #############################


        // GENERATE NEIGHBORHOOD URL
        public function neighborhoodURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            $city_query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .$data['city_id']. ")";
            $city_data = sqlfetch(sqlquery($city_query));
            return '/' .$this->frontendPath(). '/' .$city_data['slug']. '/' .$data['slug']. '/';
        }


        #############################
        # SEARCH
        #############################


        // BUILD WHERE ARRAY FOR getBusinesses SEARCH METHOD (BELOW)
        private function gbBuildWA($var, $db_field, $type) {

            // HAVE VALUE FOR VAR
            if ($var) {

                // IS ARRAY
                if (is_array($var)) {

                    // IS STRING
                    if ($type == 'str') {

                        // APPLY TO ALL VALS
                        $vals = array_map(function($val) {
                            return sqlescape($val);
                        }, $var);

                        // QUERY STRING
                        $out = "(" .$db_field. "='" .implode("') OR (" .$db_field. "='", $vals). "')";

                    // IS STRING - MULTIPLE
                    } else if ($type == 'str_multi') {

                        // LOOP THROUGH VALUES
                        foreach ($var as $val) {
                            $pa[] = "(FIND_IN_SET('" .sqlescape($val). "', " .$db_field. "))";
                        }

                        // QUERY STRING
                        $out = "(" .implode(") OR (", $pa). ")";

                    // IS NUMBER
                    } else if ($type == 'num') {

                        // APPLY TO ALL VALS
                        $vals = array_map(function($val) {
                            return (float)$val;
                        }, $var);

                        // QUERY STRING
                        $out = "(" .$db_field. "=" .implode(") OR (" .$db_field. "=", $vals). ")";

                    // IS MIN/MAX
                    } else if ($type == 'min_max') {

                        // HAVE MIN AND MAX
                        if (($var[0]) && ($var[1])) {
                            $out = "(" .$db_field. ">=" .(float)$var[0]. ") AND (" .$db_field. "<=" .(int)$var[1]. ")";

                        // HAVE MIN
                        } else if ($var[0]) {
                            $out = "(" .$db_field. ">=" .(float)$var[0]. ")";

                        // HAVE MAX
                        } else if ($var[1]) {
                            $out = "(" .$db_field. "<=" .(float)$var[1]. ")";
                        }

                    }

                // IS SINGLE
                } else {

                    // IS STRING
                    if ($type == 'str') {
                        $out = $db_field. " LIKE '%" .sqlescape($var). "%'";

                    // IS STRING - MULTIPLE
                    } else if ($type == 'str_multi') {
                        $out = "(FIND_IN_SET('" .sqlescape($var). "', " .$db_field. "))";
                        // WHERE (CONCAT(",", `cooling_type_codes`, ",") REGEXP ",(F|W),")

                    // IS NUMBER
                    } else if ($type == 'num') {
                        $out = $db_field. "=" .(float)$var;

                    } else if ($type == 'has') {
                        $out = $db_field. "!=''";
                    }

                }

                return $out;

            }

        }


        // GET BUSINESSES
        public function getBusinesses($vars) {
            global $_uccms;

            $out = array();

            //echo print_r($vars);
            //exit;

            /*
            // STRING
            if ($vars['vars']['slug']) {
                if (is_array($vars['vars']['slug'])) {
                    $vals = array_map(function($val) {
                        return sqlescape($val);
                    }, $vars['vars']['slug']);
                    $wa[] = "(b.slug='" .implode("') OR (b.slug='", $vals). "')";
                } else {
                    $wa[] = "b.slug='" .sqlescape($vars['vars']['slug']). "'";
                }
            }

            // NUMBER
            if ($vars['vars']['id']) {
                if (is_array($vars['vars']['id'])) {
                    $vals = array_map(function($val) {
                        return (float)$val;
                    }, $vars['vars']['id']);
                    $wa[] = "(b.id=" .implode(") OR (b.id=", $vals). ")";
                } else {
                    $wa[] = "b.id=" .(float)$vars['vars']['slug'];
                }
            }
            */

            $wa = array();
            $ja = array();

            // BY ID
            if ($val = $this->gbBuildWA($vars['vars']['id'], 'b.id', 'num')) {
                $wa[] = $val;
                $lva['id'] = $vars['vars']['id'];
            }

            // BY SLUG
            if ($val = $this->gbBuildWA($vars['vars']['slug'], 'b.slug', 'str')) {
                $wa[] = $val;
                $lva['slug'] = $vars['vars']['slug'];
            }

            // BY NAME
            if ($val = $this->gbBuildWA($vars['vars']['name'], 'b.name', 'str')) {
                $wa[] = $val;
                $lva['name'] = $vars['vars']['name'];
            }

            // BY CITY
            if ($val = $this->gbBuildWA($vars['vars']['city_id'], 'b.city_id', 'num')) {
                $wa[] = $val;
                $lva['city_id'] = $vars['vars']['city_id'];
            }

            // BY NEIGHBORHOOD
            if ($val = $this->gbBuildWA($vars['vars']['neighborhood_id'], 'b.neighborhood_id', 'num')) {
                $wa[] = $val;
                $lva['neighborhood_id'] = $vars['vars']['neighborhood_id'];
            }

            // BY CITY
            if ($val = $this->gbBuildWA($vars['vars']['city'], 'b.city', 'str')) {
                $wa[] = $val;
                $lva['city'] = $vars['vars']['city'];
            }

            // BY STATE
            if ($val = $this->gbBuildWA($vars['vars']['state'], 'b.state', 'str')) {
                $wa[] = $val;
                $lva['state'] = $vars['vars']['state'];
            }

            // BY ZIP
            if ($val = $this->gbBuildWA($vars['vars']['zip'], 'b.zip', 'str')) {
                $wa[] = $val;
                $lva['zip'] = $vars['vars']['zip'];
            }

            // BY RATING
            if ($val = $this->gbBuildWA(array($vars['vars']['rating_min'], $vars['vars']['rating_max']), 'b.rating', 'min_max')) {
                $wa[] = $val;
                $lva['name'] = $vars['vars']['name'];
            }

            // BY REVIEWS
            if ($val = $this->gbBuildWA(array($vars['vars']['reviews_min'], $vars['vars']['reviews_max']), 'b.reviews', 'min_max')) {
                $wa[] = $val;
                if ($vars['vars']['reviews_min']) $lva['reviews_min'] = $vars['vars']['reviews_min'];
                if ($vars['vars']['reviews_max']) $lva['reviews_min'] = $vars['vars']['reviews_max'];
            }

            // QUERY
            if ($vars['vars']['q']) {
                $keywords = sqlescape($vars['vars']['q']);
                $wa[] = "(b.name LIKE '%" .$keywords. "%') OR (b.name_owner LIKE '%" .$keywords. "%') OR (b.name_contact LIKE '%" .$keywords. "%') OR (b.description LIKE '%" .$keywords. "%') OR (b.tagline LIKE '%" .$keywords. "%') OR (b.keywords LIKE '%" .$keywords. "%')";
            }

            // CATEGORY
            if ($vars['vars']['category']) {
                $ja['business_categories'] = "INNER JOIN `" .$this->tables['business_categories']. "` AS `bc` ON b.id=bc.business_id";
                $ja['categories'] = "INNER JOIN `" .$this->tables['categories']. "` AS `c` ON bc.category_id=c.id";
                if (is_array($vars['vars']['category'])) {
                    $wa[] = "(bc.category_id IN(" .implode(",", array_map('intval', $vars['vars']['category'])). ")) AND (c.active=1)";
                } else {
                    $wa[] = "(bc.category_id=" .(int)$vars['vars']['category']. ") AND (c.active=1)";
                }
                $lva['category_id'] = $vars['vars']['category'];
            }

            // FEATURES
            if (count((array)$vars['vars']['feature']) > 0) {

                // LOOP THROUGH FEATURE(S)
                foreach ($vars['vars']['feature'] as $feature_id => $value) {

                    if (($feature_id) && ($value)) {

                        $ja['business_feature_values'] = "LEFT JOIN `" .$this->tables['business_feature_values']. "` AS `bfv` ON b.id=bfv.business_id";

                        $lva['feature'][$feature_id] = $value;

                        // VALUE IS ARRAY
                        if (is_array($value)) {

                            // MIN / MAX
                            if (($value['min']) || ($value['max'])) {

                                // HAVE MIN AND MAX
                                if (($value['min']) && ($value['max'])) {
                                    $wa[] = "(bfv.feature_id=" .(int)$feature_id. ") AND (bfv.value>=" .(float)$value['min']. ") AND (bfv.value<=" .(float)$value['max']. ")";

                                // HAVE MIN
                                } else if ($value['min']) {
                                    $wa[] = "(bfv.feature_id=" .(int)$feature_id. ") AND (bfv.value>=" .(float)$value['min']. ")";

                                // HAVE MAX
                                } else if ($value['max']) {
                                    $wa[] = "(bfv.feature_id=" .(int)$feature_id. ") AND (bfv.value<=" .(float)$value['max']. ")";
                                }

                            // OPTION ID(S)
                            } else {

                                $uvals = array();

                                foreach ($value as $val) {
                                    $val = trim(sqlescape($val));
                                    if ($val) {
                                        $uvals[] = "FIND_IN_SET(" .(is_numeric($val) ? $val : "'" .$val. "'"). ", bfv.value)";
                                    }
                                }

                                if (count($uvals) > 0) {
                                    $wa[] = "(bfv.feature_id=" .(int)$feature_id. ") AND (" .implode(") AND ( ", $uvals). ")";
                                }

                            }

                        // SINGLE VALUE
                        } else {
                            $wa[] = "(bfv.feature_id=" .(int)$feature_id. ") AND (bfv.value LIKE '%" .sqlescape($value). "%')";
                        }

                    }

                }

            }

            // ACTIVE
            $wa[] = "b.status=1";

            // WHERE
            $where_sql = "(" .implode(") AND (", $wa). ")";

            // JOINS
            $join_sql = implode(" ", $ja);

            $order_direction = strtolower($vars['order_direction']);

            // RANDOM ORDER
            if ($order_direction == 'rand') {

                $order_sql = "RAND()";

            // SPECIFIC ORDER
            } else {

                // ORDER
                switch ($vars['order_field']) {
                    case 'name':
                        $order_field = "b.name";
                        break;
                    case 'city':
                        $order_field = "b.city";
                        break;
                    case 'state':
                        $order_field = "b.state";
                        break;
                    case 'zip':
                        $order_field = "b.zip";
                        break;
                    case 'rating':
                        $order_field = "b.rating";
                        break;
                    case 'reviews':
                        $order_field = "b.reviews";
                        break;
                    case 'new':
                        $order_field = "b.dt_created";
                        break;
                    case 'updated':
                        $order_field = "b.updated_dt";
                        break;
                }

                if (!$order_field) {
                    $order_field = "b.name";
                }

                // ORDER DIRECTION
                if (strtoupper($vars['order_direction']) == 'DESC') {
                    $order_direction = "DESC";
                } else {
                    $order_direction = "ASC";
                }

                // ORDER SQL
                $order_sql = trim($order_field. " " .$order_direction);

            }

            // LIMIT
            if ($vars['limit']) {
                $limit_limit = (int)$vars['limit'];
            } else {
                $limit_limit = $this->perPage();
            }

            // OFFSET
            if ($vars['offset']) {
                $limit_offset = (int)$vars['offset'];
            } else {
                $limit_offset = 0;
            }

            // LIMIT SQL
            $limit_sql = $limit_offset. "," .$limit_limit;

            // GET PAGED MATCHING BUSINESSES
            $query = "
            SELECT b.*
            FROM `" .$this->tables['businesses']. "` AS `b`
            " .$join_sql. "
            WHERE " .$where_sql. "
            GROUP BY b.id
            ORDER BY " .$order_sql. "
            LIMIT " .$limit_sql . "
            ";

            /*
            if ($_SERVER['REMOTE_ADDR'] == '24.28.79.50') {
                echo $query;
                exit;
            }
            */

            $q = sqlquery($query);

            // NUMBER OF RESULTS
            $out['num_results'] = sqlrows($q);

            // GET ALL MATCHING BUSINESSES
            $tquery = "
            SELECT b.id
            FROM `" .$this->tables['businesses']. "` AS `b`
            " .$join_sql. "
            WHERE " .$where_sql. "
            GROUP BY b.id
            ";
            $tq = sqlquery($tquery);

            // TOTAL RESULTS
            $out['num_total'] = sqlrows($tq);
            $out['total'] = $out['num_total'];

            // LOOP
            while ($business = sqlfetch($q)) {
                $out['results'][$business['id']] = $business;
            }

            // LOGGING SEARCH
            if ($vars['log']) {

                // LOG THE SEARCH
                $log = $this->search_log(array(
                    'keywords'      => $keywords,
                    'vars'          => $lva,
                    'num_results'   => $out['num_total'],
                    'account_id'    => $_uccms['_account']->userID(),
                    'ip'            => ip2long($_SERVER['REMOTE_ADDR'])
                ), $vars['log_params']);

                $_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['hash'] = $log['hash'];
                if ($log['id']) $_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['id'] = $log['id'];

            // NOT LOGGING SEARCH
            } else {
                //$_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['id'] = 0;
            }

            return $out;

        }


        // **OLD** MAIN SEARCH
        public function doSearch($vars=array()) {
            global $_uccms;

            $out = array();

            // LOG VARS ARRAY
            $lva = array();

            // CLEAN UP
            $q = trim(sqlescape($vars['q']));

            // IS PAGING
            if ($vars['paging']) {

                // HOW MANY LISTINGS PER PAGE
                if ($vars['limit']) $limit = (int)$vars['limit']; // SPECIFIED DIRECTLY
                if (!$limit) $limit = $this->getSetting('listings_per_page'); // USE EXTENSION SETTING
                if (!$limit) $limit = $this->perPage(); // USE BIGTREE / DEFAULT

                // CURRENT PAGE
                $page = isset($vars['page']) ? intval($vars['page']) : 1;

                $out['limit'] = $limit;
                $out['page'] = $page;

                $sql_limit = "LIMIT " .(($page - 1) * $limit). "," .$limit. "";

            }

            // JOIN ARRAY
            $ja = array();

            // WHERE ARRAY
            $wa = array();

            // BASE WHERE
            $wa[] = "b.status=1";

            // CITY
            if ($vars['city_id']) {
                $wa[] = "b.city_id=" .(int)$vars['city_id'];
            }

            // NEIGHBORHOOD
            if ($vars['neighborhood_id']) {
                $wa[] = "b.neighborhood_id=" .(int)$vars['neighborhood_id'];
                $lva['neighborhood_id'] = $vars['neighborhood_id'];
            }

            // RATING
            if (($vars['rating_min']) && ($vars['rating_max'])) {
                $wa[] = "(b.rating>=" .(int)$vars['rating_min']. ") AND (b.rating<=" .(int)$vars['rating_max']. ")";
                $lva['rating_min'] = $vars['rating_min'];
                $lva['rating_max'] = $vars['rating_max'];

            } else if ($vars['rating_min']) {
                $wa[] = "b.rating>=" .(int)$vars['rating_min'];
                $lva['rating_min'] = $vars['rating_min'];

            } else if ($vars['rating_max']) {
                $wa[] = "(b.rating<=" .(int)$vars['rating_max'];
                $lva['rating_max'] = $vars['rating_max'];
            }

            // REVIEWS
            if (($vars['reviews_min']) && ($vars['reviews_max'])) {
                $wa[] = "(b.reviews>=" .(int)$vars['reviews_min']. ") AND (b.reviews<=" .(int)$vars['reviews_max']. ")";
                $lva['reviews_min'] = $vars['reviews_min'];
                $lva['reviews_max'] = $vars['reviews_max'];

            } else if ($vars['reviews_min']) {
                $wa[] = "b.reviews>=" .(int)$vars['reviews_min'];
                $lva['reviews_min'] = $vars['reviews_min'];

            } else if ($vars['reviews_max']) {
                $wa[] = "(b.reviews<=" .(int)$vars['reviews_max'];
                $lva['reviews_max'] = $vars['reviews_max'];
            }

            // QUERY
            if ($q) {
                $wa[] = "(b.name LIKE '%" .$q. "%') OR (b.description LIKE '%" .$q. "%') OR (b.tagline LIKE '%" .$q. "%') OR (b.keywords LIKE '%" .$q. "%')";
            }

            // CATEGORY SPECIFIED
            if ($vars['category']) {
                $ja[] = "INNER JOIN `" .$this->tables['business_categories']. "` AS `bc` ON b.id=bc.business_id";
                $ja[] = "INNER JOIN `" .$this->tables['categories']. "` AS `c` ON bc.category_id=c.id";
                $wa[] = "(bc.category_id=" .(int)$vars['category']. ") AND (c.active=1)";
                $lva['category_id'] = $vars['category'];
            }

            if ($vars['order_by']) {
                $order_by = $vars['order_by'];
            } else {
                $order_by = "b.name ASC, b.id ASC";
            }

            // GET BUSINESSES
            $business_query = "
            SELECT b.*
            FROM `" .$this->tables['businesses']. "` AS `b`
            " .implode(" ", $ja). "
            WHERE ((" .implode(") AND (", $wa). "))
            GROUP BY b.id
            ORDER BY " .$order_by. "
            ";
            $business_q = sqlquery($business_query . $sql_limit);
            while ($business = sqlfetch($business_q)) {
                $out['results'][] = $business;
            }

            // IS PAGING
            if ($vars['paging']) {
                $business_q = sqlquery($business_query);
                $out['total'] = sqlrows($business_q);
            } else {
                $out['total'] = count((array)$out['results']);
            }

            // LOGGING SEARCH
            if ($vars['log']) {

                // LOG THE SEARCH
                $log = $this->search_log(array(
                    'keywords'      => $q,
                    'vars'          => $lva,
                    'num_results'   => $out['total'],
                    'account_id'    => $_uccms['_account']->userID(),
                    'ip'            => ip2long($_SERVER['REMOTE_ADDR'])
                ), $vars['log_params']);

                $_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['hash'] = $log['hash'];
                if ($log['id']) $_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['id'] = $log['id'];

            // NOT LOGGING SEARCH
            } else {
                //$_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['id'] = 0;
            }

            unset($lva);

            return $out;

        }


        // REMOVE EMPTY VALUES FROM ARRAY
        public function array_removeEmpty($haystack) {
            foreach ($haystack as $key => $value) {
                if (is_array($value)) {
                    $haystack[$key] = $this->array_removeEmpty($haystack[$key]);
                }

                if (empty($haystack[$key])) {
                    unset($haystack[$key]);
                }
            }
            return $haystack;
        }


        // LOG SEARCH
        public function search_log($vars=array(), $params=array()) {

            $out = array();

            // HAVE VARS
            if (is_array($vars['vars'])) {

                // NOT LOGGING EMPTY VARS
                if (!$params['log_empty_vars']) {
                    $vars['vars'] = $this->array_removeEmpty($vars['vars']);
                }

                if (count($vars['vars']) > 0) {
                    $vars['vars'] = json_encode($vars['vars']);
                } else {
                    $vars['vars'] = '';
                }

            }

            $out['hash'] = md5(serialize($vars));

            // IS NEW / DIFFERENT SEARCH
            if ($out['hash'] != $_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['hash']) {

                // RECORD
                $query = "INSERT INTO `" .$this->tables['search_log']. "` SET " .uccms_createSet($vars);
                if (sqlquery($query)) {
                    $out['id'] = sqlid();
                }

            }

            return $out;

        }


        #############################
        # BUSINESS
        #############################


        // GENERATE BUSINESS URL
        public function businessURL($id, $category_id=0, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['businesses']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            $category_slug = $data['category_slug']. '-' .$category_id;
            if (!$data['category_slug']) {
                if ($category_id > 0) {
                    $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$id. ")";
                    $data = sqlfetch(sqlquery($query));
                    if ($data['slug']) {
                        $category_slug = stripslashes($data['slug']). '-' .$data['id'];
                    } else {
                        $category_slug = $this->makeRewrite($data['title']). '-' .$id;
                    }
                } else {
                    $category_slug = 'business';
                }
            }
            return '/' .$this->frontendPath(). '/' .trim($category_slug). '/' .trim($slug). '-' .$id. '/';
        }


        // GET BUSINESS
        public function getBusiness($id, $pending=false, $vals=array(), $vars=array()) {

            $business = array();

            // LISTING ID SPECIFIED
            if ($id > 0) {

                // ACCOUNT SPECIFIED
                if ($vals['account_id'] > 0) {
                    $acct_sql = " AND (`account_id`=" .(int)$vals['account_id']. ")";
                }

                // PENDING
                if ($pending) {

                    // GET BUSINESS
                    $business_query = "SELECT * FROM `" .$this->tables['businesses']. "` WHERE (`pending`=" .(int)$id. ")" .$acct_sql;
                    $business_q = sqlquery($business_query);
                    $business = sqlfetch($business_q);

                    // BUSINESS NOT FOUND
                    if (!$business['id']) {

                        // GET MAIN BUSINESS
                        $business_query = "SELECT * FROM `" .$this->tables['businesses']. "` WHERE (`id`=" .(int)$id. ")" .$acct_sql;
                        $business_q = sqlquery($business_query);
                        $business = sqlfetch($business_q);

                        // BUSINESS FOUND
                        if ($business['id']) {

                            // PENDING BUSINESS NOT FOUND & CREATING
                            if ((!$business['pending']) && ($vars['create'])) {

                                $parent = $business['id'];

                                $business['pending'] = $business['id'];
                                $business['status'] = 5;
                                unset($business['id']);

                                // CREATE PENDING BUSINESS
                                $business_query = "INSERT INTO `" .$this->tables['businesses']. "` SET " .uccms_createSet($business). "";
                                if (sqlquery($business_query)) {

                                    $business['id'] = sqlid();

                                    // COPY CATEGORY RELATIONS
                                    $catrel_query = "SELECT * FROM `" .$this->tables['business_categories']. "` WHERE (`business_id`=" .$parent. ")";
                                    $catrel_q = sqlquery($catrel_query);
                                    while ($catrel = sqlfetch($catrel_q)) {

                                        unset($catrel['id']);
                                        unset($catrel['business_id']);
                                        $catrel['business_id'] = $business['id'];

                                        // CREATE NEW RELATION FOR PENDING BUSINESS
                                        $ncatrel_query = "INSERT INTO `" .$this->tables['business_categories']. "` SET " .uccms_createSet($catrel). "";
                                        sqlquery($ncatrel_query);

                                    }

                                    // COPY FEATURE RELATIONS
                                    $features_query = "SELECT * FROM `" .$this->tables['business_features']. "` WHERE (`business_id`=" .$parent. ")";
                                    $features_q = sqlquery($features_query);
                                    while ($feature = sqlfetch($features_q)) {

                                        unset($feature['id']);
                                        unset($feature['business_id']);
                                        $feature['business_id'] = $business['id'];

                                        // CREATE NEW FEATURES FOR PENDING BUSINESS
                                        $nfeatures_query = "INSERT INTO `" .$this->tables['business_features']. "` SET " .uccms_createSet($feature). "";
                                        sqlquery($nfeatures_query);

                                    }

                                    // COPY FEATURE VALUES
                                    $featurev_query = "SELECT * FROM `" .$this->tables['business_feature_values']. "` WHERE (`business_id`=" .$parent. ")";
                                    $featurev_q = sqlquery($featurev_query);
                                    while ($featurev = sqlfetch($featurev_q)) {

                                        unset($featurev['id']);
                                        unset($featurev['business_id']);
                                        $featurev['business_id'] = $business['id'];

                                        // CREATE NEW FEATURE VALUES FOR PENDING BUSINESS
                                        $nfeaturev_query = "INSERT INTO `" .$this->tables['business_feature_values']. "` SET " .uccms_createSet($featurev). "";
                                        sqlquery($nfeaturev_query);

                                    }

                                    // COPY HOURS
                                    $hours_query = "SELECT * FROM `" .$this->tables['business_hours']. "` WHERE (`business_id`=" .$parent. ")";
                                    $hours_q = sqlquery($hours_query);
                                    while ($hours = sqlfetch($hours_q)) {

                                        unset($hours['id']);
                                        unset($hours['business_id']);
                                        $hours['business_id'] = $business['id'];

                                        // CREATE NEW HOURS FOR PENDING BUSINESS
                                        $nhours_query = "INSERT INTO `" .$this->tables['business_hours']. "` SET " .uccms_createSet($hours). "";
                                        sqlquery($nhours_query);

                                    }

                                    // COPY IMAGE DB RECORDS
                                    $images_query = "SELECT * FROM `" .$this->tables['business_images']. "` WHERE (`business_id`=" .$parent. ")";
                                    $images_q = sqlquery($images_query);
                                    while ($image = sqlfetch($images_q)) {

                                        $iid = $image['id'];

                                        unset($image['id']);
                                        unset($image['business_id']);
                                        $image['business_id'] = $business['id'];

                                        // CREATE NEW IMAGE DB RECORDS FOR PENDING BUSINESS
                                        $nimages_query = "INSERT INTO `" .$this->tables['business_images']. "` SET " .uccms_createSet($image). "";
                                        sqlquery($nimages_query);

                                        $business['images']['map'][$iid] = sqlid();

                                    }

                                    // COPY VIDEO DB RECORDS
                                    $videos_query = "SELECT * FROM `" .$this->tables['business_videos']. "` WHERE (`business_id`=" .$parent. ")";
                                    $videos_q = sqlquery($videos_query);
                                    while ($video = sqlfetch($videos_q)) {

                                        $vid = $video['id'];

                                        unset($video['id']);
                                        unset($video['business_id']);
                                        $video['business_id'] = $business['id'];

                                        // CREATE NEW VIDEO DB RECORDS FOR PENDING BUSINESS
                                        $nvideos_query = "INSERT INTO `" .$this->tables['business_videos']. "` SET " .uccms_createSet($video). "";
                                        sqlquery($nvideos_query);

                                        $business['videos']['map'][$vid] = sqlid();

                                    }

                                } else {
                                    unset($business);
                                }

                            }

                        // BUSINESS NOT FOUND
                        } else {
                            unset($business);
                        }

                    }

                // NOT PENDING
                } else {

                    // GET BUSINESS
                    $business_query = "SELECT * FROM `" .$this->tables['businesses']. "` WHERE (`id`=" .(int)$id. ")" .$acct_sql;
                    $business_q = sqlquery($business_query);
                    $business = sqlfetch($business_q);

                }

            }

            return $business;

        }


        // GET BUSINESS'S HOURS
        public function getHours($business_id=0) {

            $out = array();

            if ($business_id) {

                // GET HOURS
                $hours_query = "SELECT * FROM `" .$this->tables['business_hours']. "` WHERE (`business_id`=" .(int)$business_id. ") ORDER BY `day` ASC";
                $hours_q = sqlquery($hours_query);
                while ($hours = sqlfetch($hours_q)) {
                    if (($hours['open'] == '01:00:00') && ($hours['close'] == '01:00:00')) {
                        // don't return day
                    } else {
                        $out[] = $hours;
                    }
                }

            }

            return $out;

        }


        // GET BUSINESS FEATURES (INCLUDING FROM WITHIN GROUPS)
        public function business_features($business_id, $category_id=0) {

            // FEATURE ARRAY
            $out = array();

            // CLEAN UP
            $business_id = (int)$business_id;

            // HAVE BUSINESS ID
            if ($business_id) {

                // FEATURE ARRAY
                $fa = array();

                // GET BUSINESS FEATURE RELATIONS
                $bf_query = "SELECT * FROM `" .$this->tables['business_features']. "` WHERE (`business_id`=" .$business_id. ") ORDER BY `sort` ASC, `id` ASC";
                $bf_q = sqlquery($bf_query);
                if (sqlrows($bf_q) > 0) {
                    while ($bf = sqlfetch($bf_q)) {
                        if ($bf['feature_id'] != 0) { // IS FEATURE
                            $fa[$bf['feature_id']] = $bf['feature_id'];
                        } else if ($bf['group_id'] != 0) { // IS GROUP
                            $feature_group_query = "SELECT * FROM `" .$this->tables['feature_groups']. "` WHERE (`id`=" .$bf['group_id']. ")";
                            $feature_group = sqlfetch(sqlquery($feature_group_query));
                            if ($feature_group['features']) {
                                foreach (explode(',', stripslashes($feature_group['features'])) as $feature) {
                                    $fa[$feature] = $feature;
                                }
                            }
                        }

                    }
                }

                // ARRAY OF CATEGORIES
                $cata = array();

                // CATEGORY SPECIFIED
                if ($category_id) {

                    $cata[] = $category_id;

                    // GET PARENTS OF CATEGORY
                    $cata += $this->getCategoryParents((int)$category_id);

                // GET ALL CATEGORIES BUSINESS IS IN
                } else {

                    // GET CURRENT RELATIONS
                    $query = "SELECT `category_id` FROM `" .$this->tables['business_categories']. "` WHERE (`business_id`=" .$business_id. ")";
                    $q = sqlquery($query);
                    while ($row = sqlfetch($q)) {
                        $cata[] = $row['category_id'];
                    }

                }

                // HAVE CATEGORIES
                if (count($cata) > 0) {

                    // LOOP
                    foreach ($cata as $category_id) {

                        // GET CATEGORY FEATURE RELATIONS
                        $cf_query = "SELECT * FROM `" .$this->tables['category_features']. "` WHERE (`category_id`=" .$category_id. ") ORDER BY `sort` ASC, `id` ASC";
                        $cf_q = sqlquery($cf_query);
                        if (sqlrows($cf_q) > 0) {
                            while ($cf = sqlfetch($cf_q)) {
                                if ($cf['feature_id'] != 0) { // IS FEATURE
                                    $fa[$cf['feature_id']] = $cf['feature_id'];
                                } else if ($cf['group_id'] != 0) { // IS GROUP
                                    $feature_group_query = "SELECT * FROM `" .$this->tables['feature_groups']. "` WHERE (`id`=" .$cf['group_id']. ")";
                                    $feature_group = sqlfetch(sqlquery($feature_group_query));
                                    if ($feature_group['features']) {
                                        foreach (explode(',', stripslashes($feature_group['features'])) as $feature) {
                                            $fa[$feature] = $feature;
                                        }
                                    }
                                }

                            }
                        }

                    }

                }

                // HAVE FEATURES
                if (count((array)$fa) > 0) {

                    $feature_query = "SELECT * FROM `" .$this->tables['features']. "` WHERE (`id` IN(" .implode(',', array_keys($fa)). ")) AND (`active`=1) ORDER BY `sort` ASC, `title` ASC, `id` DESC";
                    $feature_q = sqlquery($feature_query);
                    if (sqlrows($feature_q) > 0) {
                        while ($feature = sqlfetch($feature_q)) {
                            if (($feature['type'] == 'select_one') || ($feature['type'] == 'select_multi')) {
                                $options_query = "SELECT * FROM `" .$this->tables['feature_options']. "` WHERE (`feature_id`=" .$feature['id']. ") ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                                $options_q = sqlquery($options_query);
                                while ($option = sqlfetch($options_q)) {
                                    $feature['options'][$option['id']] = $option;
                                }
                            }
                            $value_query = "SELECT * FROM `" .$this->tables['business_feature_values']. "` WHERE (`business_id`=" .$business_id. ") AND (`feature_id`=" .$feature['id']. ")";
                            $value_q = sqlquery($value_query);
                            while ($value = sqlfetch($value_q)) {
                                if ($feature['type'] == 'select_multi') {
                                    $value['value'] = explode(',', $value['value']);
                                }
                                $feature['value'] = $value;
                            }
                            $out[$feature['id']] = $feature;
                        }
                    }
                }

            }

            return $out;

        }


        // LOG A PROPERTY STAT
        public function business_logStat($business_id, $action, $value='') {
            global $_uccms;
            $columns = array(
                'business_id'   => (int)$business_id,
                'action'        => $action,
                'value'         => $value,
                'search_log_id' => (int)$_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['id'],
                'account_id'    => $_uccms['_account']->userID(),
                'ip'            => ip2long($_SERVER['REMOTE_ADDR'])
            );
            $query = "INSERT INTO `" .$this->tables['business_stats_log']. "` SET " .uccms_createSet($columns). ", `dt`=NOW()";
            if (sqlquery($query)) {
                return sqlid();
            }
        }


        // GET A BUSINESS STAT
        public function business_getStats($business_id, $vars=array()) {

            $out = array();

            $business_id = (int)$business_id;

            if ($business_id) {

                $wa = array();

                $wa[] = "(`business_id`=" .$business_id. ")";

                if ($vars['action']) {
                    $wa[] = "(`action`='" .$vars['action']. "')";
                }

                if ($vars['value']) {
                    $wa[] = "(`value`='" .$vars['value']. "')";
                }

                if ($vars['date_from']) {
                    $wa[] = "(`dt`>='" .date('Y-m-d H:i:s', strtotime($vars['date_from'])). "')";
                }

                if ($vars['date_to']) {
                    $wa[] = "(`dt`<='" .date('Y-m-d H:i:s', strtotime($vars['date_to'])). "')";
                }

                // WHERE
                $where_sql = "(" .implode(") AND (", $wa). ")";

                // GET STATS
                $stat_query = "SELECT * FROM `" .$this->tables['business_stats_log']. "` WHERE " .$where_sql;
                $stat_q = sqlquery($stat_query);
                while ($stat = sqlfetch($stat_q)) {
                    $out[$stat['action']]++;
                }

            }

            return $out;

        }


        // LOG BUSINESS CONTACT EMAIL
        public function logBusinessEmail($business_id, $fields) {
            $data = '';
            if (is_array($fields)) {
                $da = array();
                foreach ($fields as $fid => $fval) {
                    $da[$fid] = str_replace(array("\n", "\r", '"', "'"), array('<br/>', '', '&quot;', '&#039;'), $fval);
                }
                $data = json_encode($da);
            }
            $query = "INSERT INTO `" .$this->tables['business_email_log']. "` SET `dt`=NOW(), `business_id`=" .(int)$business_id. ", `data`='" .$data. "'";
            if (sqlquery($query)) {
                return sqlid();
            }
        }


        // GET BUSINESS'S E-COMMERCE ITEMS
        public function business_getEcommerceItems($business_id, $param=array()) {

            $out = array();

            $business_id = (int)$business_id;

            if ($business_id) {

                // GET BUSINESS - ECOMM ITEM RELATIONS
                $query = "SELECT * FROM `" .$this->tables['business_ecommerce_items']. "` WHERE (`business_id`=" .$business_id. ") ORDER BY `sort` ASC";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['ecommerce_item_id']] = $row;
                }

            }

            return $out;

        }


        #############################
        # FEATURES
        #############################


        public function feature_getOptions($feature_id, $params=array()) {

            $out = array();

            $feature_id = (int)$feature_id;

            if ($feature_id) {

                // GET FEATURE OPTIONS
                $query = "SELECT * FROM `" .$this->tables['feature_options']. "` WHERE (`feature_id`=" .$feature_id. ") ORDER BY `sort` ASC";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['id']] = $row;
                }

            }

            return $out;

        }


        #############################
        # PAYMENT
        #############################


        // ALL AVAILABLE PAYMENT METHODS
        public function allPaymentMethods() {
            $out = array(
                'authorize'     => 'Authorize.Net',
                'paypal-rest'   => 'PayPal REST API',
                'paypal'        => 'PayPal Payments Pro',
                'payflow'       => 'PayPal Payflow Gateway',
                'linkpoint'     => 'First Data / LinkPoint',
                'stripe'        => 'Stripe',
                'instapay'      => 'InstaPay',
                'check_cash'    => 'Check / Cash'
            );
            return $out;
        }


        // GET PAYMENT METHODS FROM DB
        public function getPaymentMethods($active=false) {
            $out = array();
            if ($active) {
                $where = "WHERE (`active`=1)";
            }
            $query = "SELECT * FROM `" .$this->tables['payment_methods']. "` " .$where;
            $q = sqlquery($query);
            while ($f = sqlfetch($q)) {
                $out[$f['id']] = $f;
            }
            return $out;
        }


        // LOG TRANSACTION
        public function logTransaction($vars) {
            $query = "INSERT INTO `" .$this->tables['transaction_log']. "` SET " .uccms_createSet($vars). ", `dt`=NOW()";
            sqlquery($query);
        }


        #############################
        # REVIEWS
        #############################


        // CRITERIA
        public function review_criteria($what='') {
            if ($what) {
                $criteria = $this->getSetting('review_criteria-' .$what);
            } else {
                $criteria = $this->getSetting('review_criteria');
            }
            if (($criteria) && (!is_array($criteria))) {
                $criteria = json_decode($criteria, true);
            }
            if (!is_array($criteria)) $criteria = array();
            return $criteria;
        }


        // RECORD A REVIEW
        public function review_record($vars) {
            global $cms, $_uccms;

            // BUSINESS
            if ($vars['review']['what'] == 'business') {

                $id = (int)$vars['review']['item_id'];

                // HAVE ID
                if ($id) {

                    // GET BUSINESS
                    $business_query = "SELECT * FROM `" .$this->tables['businesses']. "` WHERE (`id`=" .$id. ")";
                    $business_q = sqlquery($business_query);
                    $business = sqlfetch($business_q);

                    // BUSINESS FOUND
                    if ($business['id']) {

                        // DB COLUMNS
                        $columns = array(
                            'rating'        => $vars['calc']['rating'],
                            'reviews'       => $vars['calc']['reviews'],
                            'review_data'   => json_encode($vars['calc']['criteria'])
                        );

                        // RECORD INFO - LIVE
                        $query = "UPDATE `" .$this->tables['businesses']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$business['id']. ")";
                        sqlquery($query);

                        // RECORD INFO - PENDING
                        $query = "UPDATE `" .$this->tables['businesses']. "` SET " .uccms_createSet($columns). " WHERE (`pending`=" .$business['id']. ")";
                        sqlquery($query);

                    }

                    // EMAIL SETTINGS
                    $esettings = array(
                        'business_id'   => $business['id']
                    );

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'item_id'               => $business['id'],
                        'item_title'            => stripslashes($business['name']),
                        'item_url'              => WWW_ROOT . substr($this->businessURL($business['id'], $business['category_id'], $business), 1),
                        'from_name'             => $vars['review']['name'],
                        'content'               => $vars['review']['content'],
                        'view_url'              => WWW_ROOT . substr($this->businessURL($business['id'], $business['category_id'], $business). '#reviews', 1)
                    );

                    // IS REPLY
                    if ($vars['review']['reply_to_id']) {

                        // GET ORIGINAL REVIEW
                        $orig_query = "SELECT * FROM `uccms_ratings_reviews` WHERE (`id`=" .(int)$vars['review']['reply_to_id']. ")";
                        $orig_q = sqlquery($orig_query);
                        $orig = sqlfetch($orig_q);

                        // HAVE ACCOUNT ID
                        if ($orig['account_id']) {

                            // GET ACCOUNT
                            $acct = $_uccms['_account']->getAccount('', true, $orig['account_id']);

                            // HAVE EMAIL ADDRESS
                            if ($acct['email']) {

                                $esettings['template']  = 'ratings-reviews/reply-received.html';
                                $esettings['subject']   = ($cms->getSetting('site_name') ? $cms->getSetting('site_name'). ' - ' : ''). 'Reply to Review Received';
                                $esettings['to_email']  = stripslashes($acct['email']);

                                $evars['heading']   = 'Reply to Review Received';
                                $evars['to_name']   = ($acct['firstname'] ? ' ' .stripslashes($acct['firstname']) : '');

                            }

                        }

                    // REVIEW
                    } else {

                        $esettings['template'] = 'ratings-reviews/review-submitted.html';
                        $esettings['subject']   = ($cms->getSetting('site_name') ? $cms->getSetting('site_name'). ' - ' : ''). 'Review Submitted';

                        // WHO TO SEND TO
                        if ($business['email_lead']) {
                            $esettings['to_email'] = stripslashes($business['email_lead']);
                        } else {
                            $esettings['to_email'] = stripslashes($business['email']);
                        }

                        $evars['heading']       = 'Review Submitted';
                        $evars['manage_url']    = WWW_ROOT . '/account/business-directory/edit/?id=' .$business['id']. '&clear=true';

                    }

                    // HAVE EMAIL
                    if ($esettings['to_email']) {

                        // SEND EMAIL
                        $result = $_uccms['_account']->sendEmail($esettings, $evars);

                    }

                }

            }

        }


        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # GLOBAL SEARCH
        #############################


        public function globalSearch($q='', $params=array()) {

            $results = array();

            // GET MATCHING BUSINESS LISTINGS
            $results_query = "SELECT `id`, `slug`, `name` FROM `" .$this->tables['businesses']. "` WHERE (`name` LIKE '%" .$q. "%') ORDER BY `updated_dt` DESC, `created_dt` DESC LIMIT " .(int)$params['limit'];
            $results_q = sqlquery($results_query);
            while ($row = sqlfetch($results_q)) {
                $results[] = array(
                    'title' => stripslashes($row['name']),
                    'url'   => $this->businessURL($row['id'], 0, $row),
                    'type'  => 'business'
                );
            }

            return $results;

        }


        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # SITEMAP
        #############################


        public function getSitemap($page=array()) {
            global $bigtree;

            $out = array();

            // CATEGORIES
            $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->categoryURL($r['id'], $r)
                );
            }

            // BUSINESSES
            $query = "SELECT * FROM `" .$this->tables['businesses']. "` WHERE (`pending`=0) AND (`status`=1) AND (`deleted_dt`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->businessURL($r['id'], 0, $r)
                );
            }

            // CITIES
            $query = "SELECT * FROM `" .$this->tables['cities']. "` ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->cityURL($r['id'], $r)
                );
            }

            // NEIGHBORHOODS
            $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->neighborhoodURL($r['id'], $r)
                );
            }

            return $out;

        }


    }

?>