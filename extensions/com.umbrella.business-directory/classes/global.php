<?php

class global_uccms_BusinessDirectory extends uccms_BusinessDirectory {


    #############################
    # GENERAL: Sitemap
    #############################

    public function general_sitemap($page=array()) {
        global $bigtree;

        $out = array();

        // CATEGORIES
        $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->categoryURL($r['id'], $r)
            );
        }

        // BUSINESSES
        $query = "SELECT * FROM `" .$this->tables['businesses']. "` WHERE (`pending`=0) AND (`status`=1) AND (`deleted_dt`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->businessURL($r['id'], 0, $r)
            );
        }

        // CITIES
        $query = "SELECT * FROM `" .$this->tables['cities']. "` ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->cityURL($r['id'], $r)
            );
        }

        // NEIGHBORHOODS
        $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->neighborhoodURL($r['id'], $r)
            );
        }

        return $out;

    }


    #############################
    # GENERAL: Search
    #############################

    public function general_search($q='', $params=array()) {

        $results = array();

        // GET MATCHING BUSINESS LISTINGS
        $results_query = "SELECT `id`, `slug`, `name` FROM `" .$this->tables['businesses']. "` WHERE (`name` LIKE '%" .$q. "%') ORDER BY `updated_dt` DESC, `created_dt` DESC LIMIT " .(int)$params['limit'];
        $results_q = sqlquery($results_query);
        while ($row = sqlfetch($results_q)) {
            $results[] = array(
                'title' => stripslashes($row['name']),
                'url'   => $this->businessURL($row['id'], 0, $row),
                'type'  => 'business'
            );
        }

        return $results;

    }


    /*
    #############################
    # GENERAL: Cron
    #############################

    // RUN THE MAIN CRON
    public function runCron() {

    }
    */


    #############################
    # PAYMENT - Transactions
    #############################

    public function payment_transactions($vars=array()) {

        $out = array();

        $wa = array();

        // DATE
        if (count($vars['date']) == 2) {
            $wa[] = "(`dt`>='" .date('Y-m-d', strtotime($vars['date']['from'])). " 00:00:00') AND (`dt`<='" .date('Y-m-d', strtotime($vars['date']['to'])). " 23:59:59')";
        } else if ($vars['date']['from']) {
            $wa[] = "`dt`>='" .date('Y-m-d', strtotime($vars['date']['from'])). " 00:00:00'";
        } else if ($vars['date']['to']) {
            $wa[] = "`dt`<='" .date('Y-m-d', strtotime($vars['date']['to'])). " 23:59:59'";
        }

        // KEYWORD
        if ($vars['q']) {
            $q = trim(sqlescape($vars['q']));
            $wa[] = "(`name` LIKE '%" .$q. "%') OR (`lastfour` LIKE '%" .$q. "%')";
        }

        // ORDERBY - Date
        if ($vars['query']['orderby']['field'] == 'dt') {
            $orderby_sql = " ORDER BY `dt` " .($vars['query']['orderby']['direction'] == "desc" ? "DESC" : "ASC"). " ";
        }

        // LIMIT
        if ($vars['query']['limit']['num'] > 0) {
            $limit_sql = " LIMIT " .(int)$vars['query']['limit']['num']. " ";
        }

        if (count($wa) > 0) {
            $where_sql = " WHERE (" .implode(" ) AND ( ", $wa). ")";
        } else {
            $where_sql = "";
        }

        $businessa = array();

        // GET TRANSACTION LOGS
        $trans_query = "SELECT * FROM `" .$this->tables['transaction_log']. "` " .$where_sql . $orderby_sql . $limit_sql;
        $trans_q = sqlquery($trans_query);
        while ($trans = sqlfetch($trans_q)) {

            // HAVE BUSINESS ID
            if (($trans['business_id']) && (!$businessa[$trans['business_id']])) {

                // GET BUSINESS INFO
                $business_query = "
                SELECT *
                FROM `" .$this->tables['businesses']. "` AS `b`
                WHERE (b.id=" .$trans['business_id']. ")
                ";
                $business_q = sqlquery($business_query);
                $businessa[$trans['business_id']] = sqlfetch($business_q);

            }

            $name = ($businessa[$trans['business_id']]['name'] ? $businessa[$trans['business_id']]['name'] : stripslashes($trans['name']));

            $out[] = array(
                'number'        => $trans['business_id'],
                'dt'            => $trans['dt'],
                'name'          => $name,
                'method'        => $trans['method'],
                'description'   => 'Business Listing',
                'amount'        => $trans['amount'],
                'lastfour'      => $trans['lastfour'],
                'success'       => ($trans['status'] == 'charged' ? true : false),
                'url'           => array(
                    'edit' => '/admin/' .$this->Extension. '*business-directory/businesses/edit/?id=' .$trans['business_id']
                ),
            );

        }

        return $out;

    }


}

?>