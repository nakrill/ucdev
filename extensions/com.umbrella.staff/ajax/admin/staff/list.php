<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    //$_REQUEST['limit'] = 1;

    // GET STAFF
    $staffs = $_uccms_staff->getStaffs($_REQUEST);

    ?>

    <?php if (count((array)$staffs['staff']) > 0) { ?>

        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th class="name" scope="col">Name</th>
                    <th class="status" scope="col">Status</th>
                    <th class="options" scope="col">Options</th>
                </tr>
            </thead>
            <tbody class="items">

                <?php foreach ($staffs['staff'] as $staff) { ?>

                    <tr class="item" data-id="<?php echo $staff['id']; ?>">
                        <td class="name">
                            <a href="./edit/?id=<?php echo $staff['id']; ?>" class="title main"><?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?></a>
                        </td>
                        <td class="status">
                            <span class="badge" style="background-color: <?php echo $_uccms_staff->staffStatuses($staff['status'])['color']; ?>;"><?php echo $_uccms_staff->staffStatuses($staff['status'])['title']; ?></span>
                        </td>
                        <td class="options">
                            <a href="./edit/?id=<?php echo $staff['id']; ?>" class="edit" title="Edit" data-id="<?php echo $staff['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                            <a href="#" class="delete" title="Delete"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>

                <?php } ?>

            </tbody>
        </table>

    <?php } else { ?>

        <div class="no-results">
            No staff found.
        </div>

    <?php } ?>

    <?php

}

?>

<script>
    BigTree.setPageCount('#staff .summary .view_paging' ,<?php echo (int)$staffs['pages']; ?>, <?php echo (int)$staffs['page']; ?>);
</script>