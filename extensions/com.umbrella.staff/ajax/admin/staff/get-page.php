<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // CLEAN UP
    if ($_GET['status'] == 'inactive') {
        $status = 0;
    } else {
        if ($_GET['status']) {
            $status = (int)$_GET['status'];
        } else {
            $status = 1;
        }
    }
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
    $area_id        = (int)$_GET['area_id'];
    $tag_id         = (int)$_GET['tag_id'];
    $limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_staff->perPage();

    if (isset($_GET['base_url'])) {
        $base_url = $_GET['base_url'];
    } else if (defined('MODULE_ROOT')) {
        $base_url = MODULE_ROOT. 'staff';
    } else {
        $base_url = '.';
    }

    //$_uccms_staff->setPerPage(5);

    // WHERE ARRAY
    unset($wa);
    $wa[] = "s.id>0";

    // STATUSES
    //if ($status) {
        $wa[] = "s.status=" .$status;
    //}
    //} else {
        //$wa[] = "(s.status!=7) AND (s.status!=9)";
    //}

    /*
    // AREA SPECIFIED
    if ($area_id > 0) {
        $inner_join_area = "INNER JOIN `" .$_uccms_staff->tables['ads_areas']. "` AS `aa` ON s.id=as.staff_id";
        $wa[] = "as.area_id=" .$area_id;
    }
    */

    /*
    // TAG SPECIFIED
    if ($tag_id > 0) {
        $inner_join_tag = "INNER JOIN `" .$_uccms_staff->tables['area_tags']. "` AS `tc` ON s.id=tc.staff_id";
        $wa[] = "tc.category_id=" .$tag_id;
    }
    */

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(s.firstname) LIKE '%$part%') OR (LOWER(s.lastname) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED STAFF
    $staff_query = "
    SELECT s.*
    FROM `" .$_uccms_staff->tables['staff']. "` AS `s`
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY s.dt_created DESC, s.id DESC
    LIMIT " .(($page - 1) * $limit). "," .$limit;
    $staff_q = sqlquery($staff_query);

    // NUMBER OF PAGED STAFF
    $num_staff = sqlrows($staff_q);

    // TOTAL NUMBER OF STAFF
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_staff->tables['staff']. "` AS `s` WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_staff->pageCount($total_results['total']);

    // HAVE STAFF
    if ($num_staff > 0) {

        // LOOP
        while ($staff = sqlfetch($staff_q)) {

            ?>

            <li class="item">
                <section class="staff_name">
                    <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $staff['id']; ?>"><?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?></a>
                </section>
                <? /*
                <section class="staff_area">
                    <?php echo implode(', ', $rela); ?>
                </section>
                */ ?>
                <section class="staff_status status_<?php if ($staff['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php echo $_uccms_staff->statuses[$staff['status']]; ?>
                </section>
                <section class="staff_edit">
                    <a class="icon_edit" title="Edit Staff" href="<?php echo $base_url; ?>/edit/?id=<?php echo $staff['id']; ?>"></a>
                </section>
                <section class="staff_delete">
                    <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $staff['id']; ?>" class="icon_delete" title="Delete Staff" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                </section>
            </li>

            <?php

        }

        unset($accta);

    // NO STAFF
    } else {
        ?>
        <li style="text-align: center;">
            No <?php echo strtolower($_uccms_staff->statuses()[$status]); ?> staff.
        </li>
        <?php
    }

    ?>

    <script>
	    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>