<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

if (isset($_REQUEST['base_url'])) {
    $base_url = $_REQUEST['base_url'];
} else if (defined('MODULE_ROOT')) {
    $base_url = MODULE_ROOT. 'staff';
} else {
    $base_url = '.';
}

/*
// CLEAN UP
if ($_REQUEST['status'] == 'inactive') {
    $status = 0;
} else {
    if ($_REQUEST['status']) {
        $status = (int)$_REQUEST['status'];
    } else {
        $status = 1;
    }
}
*/

$_REQUEST['limit'] = 10;

$query  = isset($_REQUEST["query"]) ? $_REQUEST["query"] : "";
$page   = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;

if ($_REQUEST['limit']) {
    $_uccms_staff->setPerPage((int)$_REQUEST['limit']);
}
$limit = $_uccms_staff->perPage();

//$_uccms_staff->setPerPage(5);

// WHERE ARRAY
unset($wa);
$wa[] = "p.id>0";

// STAFF ID
if ($_REQUEST['staff_id']) {
    $staff_id = (int)$_REQUEST['staff_id'];
    $wa[] = "p.staff_id=" .$staff_id;
}

/*
// AREA SPECIFIED
if ($area_id > 0) {
    $inner_join_area = "INNER JOIN `" .$_uccms_staff->tables['ads_areas']. "` AS `aa` ON s.id=as.tt_id";
    $wa[] = "as.area_id=" .$area_id;
}
*/

/*
// TAG SPECIFIED
if ($tag_id > 0) {
    $inner_join_tag = "INNER JOIN `" .$_uccms_staff->tables['area_tags']. "` AS `tc` ON s.id=tc.tt_id";
    $wa[] = "tc.category_id=" .$tag_id;
}
*/

/*
// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(s.name) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}
*/

// GET PAGED
$paged_query = "
SELECT p.*
FROM `" .$_uccms_staff->tables['photos']. "` AS `p`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY p.dt DESC, p.id DESC
LIMIT " .(($page - 1) * $limit). "," .$limit;
$paged_q = sqlquery($paged_query);

// NUMBER OF PAGED
$num_paged = sqlrows($paged_q);

// TOTAL NUMBER
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_staff->tables['photos']. "` AS `p` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_staff->pageCount($total_results['total']);

// HAVE PAGED
if ($num_paged > 0) {

    // GOOGLE MAPS KEY
    $google_maps_key = $cms->getSetting('google_maps_key');

    // HAVE GOOGLE MAPS KEY
    if ($google_maps_key) {

        ?>

        <script type="text/javascript">

            $(document).ready(function() {

                // MAP
                $('#photos .main_map .map_container').googleMap({
                    zoom: 10,
                    overviewMapControl: true,
                    streetViewControl: true,
                    scrollwheel: true,
                    mapTypeControl: true
                });

            });

        </script>

        <style type="text/css">

            .gm-style .gm-style-iw > div {
                color: #333;
            }
            .gm-style .gm-style-iw h1 {
                margin: 0 0 5px 0;
                font-size: 1.1em;
                line-height: 1em;
                font-weight: bold;
            }
            .gm-style .gm-style-iw .date {
                display: inline-block;
                margin-right: 5px;
            }
            .gm-style .gm-style-iw .view {
                display: block;
                margin-top: 5px;
            }
            .gm-style .gm-style-iw .image img {
                max-width: 230px;
                height: auto;
            }

        </style>

        <?php

    }

    // LOOP
    while ($photo = sqlfetch($paged_q)) {

        // GET STAFF INFO
        $staff_query = "SELECT * FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`id`=" .$photo['staff_id']. ")";
        $staff_q = sqlquery($staff_query);
        $staff = sqlfetch($staff_q);

        ?>

        <div id="photo-<?php echo $photo['id']; ?>" class="item photo">

            <div class="contain">

                <div class="head clearfix">

                    <div class="staff">
                        <a href="../staff/edit/?id=<?php echo $staff['id']; ?>">
                            <span class="avatar" <?php if ($staff['image']) { ?>style="background-image: url('<?php echo $_uccms_staff->imageBridgeOut($staff['image'], 'staff'); ?>');"<?php } ?>></span>
                            <?php echo $_uccms_staff->combineName($staff); ?>
                        </a>
                    </div>

                    <div class="date">
                        <?php echo date('n/j/Y g:i:s A', strtotime($photo['dt'])); ?>
                    </div>

                </div>

                <div class="image">
                    <img src="<?php echo $_uccms_staff->photoURL($photo['id'], $photo); ?>" alt="" />
                </div>

            </div>

        </div>

        <?php

        // HAVE GOOGLE MAPS KEY
        if ($google_maps_key) {

            $gps = array();

            // HAVE GPS INFO
            if (($photo['gps_latitude'] != 0) && ($photo['gps_longitude'] != 0)) {
                $gps = array($photo['gps_latitude'], $photo['gps_longitude']);
            }

            // HAVE GPS
            if (count($gps) == 2) {

                // FIRST LETTER OF NAME
                $fl = strtoupper(substr($_uccms_staff->combineName($staff), 0, 1));

                ?>

                <script type="text/javascript">

                    $(document).ready(function() {

                        $('#photos .main_map .map_container').addMarker({
                            id: '<?php echo $photo['id']; ?>-start',
                            coords: [<?php echo implode(', ', $gps); ?>],
                            icon: '/images/icons/map/marker_blue<?php echo $fl; ?>.png',
                            title: '<?php echo $_uccms_staff->combineName($staff); ?>',
                            text:  '<span class="date"><?php echo date('n/j/Y', strtotime($photo['dt'])); ?></span><span class="time"><?php echo date('g:i:s A', strtotime($photo['dt'])); ?></span><span class="image"><img src="<?php echo $_uccms_staff->photoURL($photo['id'], $photo); ?>" alt="" /></span>',
                            success: function(coords, obj) {
                                var gm = obj.data('googleMarker');
                                $('#photo-<?php echo $photo['id']; ?>').hover(function(e) {
                                    Object.keys(gm).forEach(function(key) {
                                        gm[key].setVisible(false);
                                    });
                                    gm['<?php echo $photo['id']; ?>-start'].setVisible(true);
                                    if (typeof gm['<?php echo $photo['id']; ?>-end'] == 'object') {
                                        gm['<?php echo $photo['id']; ?>-end'].setVisible(true);
                                    }
                                }, function(e) {
                                    Object.keys(gm).forEach(function(key) {
                                        gm[key].setVisible(true);
                                    });
                                });
                            },
                            mouseOver: function(data) {
                                $('#photo-<?php echo $photo['id']; ?>').addClass('active');
                            },
                            mouseOut: function(data) {
                                $('#photo-<?php echo $photo['id']; ?>').removeClass('active');
                            },
                        });

                    });

                </script>

                <?php

            }

        }

    }

    unset($accta);

// NO PAGED
} else {
    ?>
    <div style="text-align: center;">
        No results found.
    </div>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>