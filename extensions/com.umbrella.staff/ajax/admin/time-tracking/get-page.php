<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

if (isset($_REQUEST['base_url'])) {
    $base_url = $_REQUEST['base_url'];
} else if (defined('MODULE_ROOT')) {
    $base_url = MODULE_ROOT. 'time-tracking';
} else {
    $base_url = '.';
}

/*
// CLEAN UP
if ($_REQUEST['status'] == 'inactive') {
    $status = 0;
} else {
    if ($_REQUEST['status']) {
        $status = (int)$_REQUEST['status'];
    } else {
        $status = 1;
    }
}
*/

$_REQUEST['limit'] = 10;

$query  = isset($_REQUEST["query"]) ? $_REQUEST["query"] : "";
$page   = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;

if ($_REQUEST['limit']) {
    $_uccms_staff->setPerPage((int)$_REQUEST['limit']);
}
$limit = $_uccms_staff->perPage();

//$_uccms_staff->setPerPage(5);

// WHERE ARRAY
unset($wa);
$wa[] = "tt.id>0";

// STAFF ID
if ($_REQUEST['staff_id']) {
    $staff_id = (int)$_REQUEST['staff_id'];
    $wa[] = "tt.staff_id=" .$staff_id;
}

// STATUS
if ($_REQUEST['status'] == 'active') {
    $wa[] = "tt.dt_end='0000-00-00 00:00:00'";
} else if ($_REQUEST['status'] == 'complete') {
    $wa[] = "tt.dt_end!='0000-00-00 00:00:00'";
}

/*
// AREA SPECIFIED
if ($area_id > 0) {
    $inner_join_area = "INNER JOIN `" .$_uccms_staff->tables['ads_areas']. "` AS `aa` ON s.id=as.tt_id";
    $wa[] = "as.area_id=" .$area_id;
}
*/

/*
// TAG SPECIFIED
if ($tag_id > 0) {
    $inner_join_tag = "INNER JOIN `" .$_uccms_staff->tables['area_tags']. "` AS `tc` ON s.id=tc.tt_id";
    $wa[] = "tc.category_id=" .$tag_id;
}
*/

/*
// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(s.name) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}
*/

// GET PAGED
$paged_query = "
SELECT tt.*
FROM `" .$_uccms_staff->tables['time_tracking']. "` AS `tt`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY tt.dt_start DESC, tt.id DESC
LIMIT " .(($page - 1) * $limit). "," .$limit;
$paged_q = sqlquery($paged_query);

// NUMBER OF PAGED
$num_paged = sqlrows($paged_q);

// TOTAL NUMBER
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_staff->tables['time_tracking']. "` AS `tt` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_staff->pageCount($total_results['total']);

// HAVE PAGED
if ($num_paged > 0) {

    // GOOGLE MAPS KEY
    $google_maps_key = $cms->getSetting('google_maps_key');

    // HAVE GOOGLE MAPS KEY
    if ($google_maps_key) {

        ?>

        <script type="text/javascript">

            $(document).ready(function() {

                // MAP
                $('#time_tracking .main_map .map_container').googleMap({
                    zoom: 10,
                    overviewMapControl: true,
                    streetViewControl: true,
                    scrollwheel: true,
                    mapTypeControl: true
                });

            });

        </script>

        <style type="text/css">

            .gm-style .gm-style-iw > div {
                color: #333;
            }
            .gm-style .gm-style-iw h1 {
                margin: 0 0 5px 0;
                font-size: 1.1em;
                line-height: 1em;
                font-weight: bold;
            }
            .gm-style .gm-style-iw .date {
                display: inline-block;
                margin-right: 5px;
            }
            .gm-style .gm-style-iw .view {
                display: block;
                margin-top: 5px;
            }

        </style>

        <?php

    }

    // LOOP
    while ($time_tracking = sqlfetch($paged_q)) {

        // GET STAFF INFO
        $staff_query = "SELECT * FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`id`=" .$time_tracking['staff_id']. ")";
        $staff_q = sqlquery($staff_query);
        $staff = sqlfetch($staff_q);

        ?>

        <li id="tt-<?php echo $time_tracking['id']; ?>" class="item">

            <div class="contain">
                <section class="tt_staff">
                    <a href="../staff/edit/?id=<?php echo $staff['id']; ?>">
                        <span class="avatar" <?php if ($staff['image']) { ?>style="background-image: url('<?php echo $_uccms_staff->imageBridgeOut($staff['image'], 'staff'); ?>');"<?php } ?>></span>
                        <?php echo $_uccms_staff->combineName($staff); ?>
                    </a>
                </section>
                <section class="tt_dt">
                    <?php echo date('n/j/Y', strtotime($time_tracking['dt_start'])); ?>
                </section>
                <? /*
                <section class="tt_job">
                </section>
                */ ?>
                <section class="tt_clock_in">
                    <?php echo date('g:i:s A', strtotime($time_tracking['dt_start'])); ?>
                </section>
                <section class="tt_clock_out active">
                    <?php
                    if ($time_tracking['dt_end'] == '0000-00-00 00:00:00') {
                        ?>
                        <span class="circle"></span> <span class="title">Active</span>
                        <?php
                    } else {
                        echo date('g:i:s A', strtotime($time_tracking['dt_end']));
                    }
                    ?>
                </section>
                <section class="tt_total">
                    <?php


                    $diff = $_uccms_staff->dtDiff($time_tracking['dt_start'], $time_tracking['dt_end'], array(
                        'format' => '%H:%I:%S'
                    ));

                    echo $diff;

                    ?>
                </section>
                <? /*
                <section class="tt_edit">
                    <a href="#" title="Edit" class="icon_edit"></a>
                </section>
                <section class="tt_delete">
                    <a onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');" title="Delete" class="icon_delete" href="./processes/delete_time/?staff_id=<?php echo $staff['id']; ?>&id=<?php echo $time_tracking['id']; ?>"></a>
                </section>
                */ ?>
            </div>

            <? /*
            <div class="edit contain" style="display: none;">
                edit toggle
            </div>
            */ ?>

        </li>

        <?php

        // HAVE GOOGLE MAPS KEY
        if ($google_maps_key) {

            $gps = array();

            // HAVE START GPS
            if (($time_tracking['gps_start_latitude'] != 0) && ($time_tracking['gps_start_longitude'] != 0)) {
                $gps['start'] = array($time_tracking['gps_start_latitude'], $time_tracking['gps_start_longitude']);
            }

            // HAVE END GPS
            if (($time_tracking['gps_end_latitude'] != 0) && ($time_tracking['gps_end_longitude'] != 0)) {
                $gps['end'] = array($time_tracking['gps_end_latitude'], $time_tracking['gps_end_longitude']);
            }

            // HAVE GPS
            if (count($gps) > 0) {

                // FIRST LETTER OF NAME
                $fl = strtoupper(substr($_uccms_staff->combineName($staff), 0, 1));

                ?>

                <script type="text/javascript">

                    $(document).ready(function() {

                        // CLICK
                        $('#tt-<?php echo $time_tracking['id']; ?>').click(function(e) {
                            e.preventDefault();
                            window.location.href = '../daily-report/?staff_id=<?php echo $staff['id']; ?>&from=<?php echo date('m/d/Y', strtotime($time_tracking['dt_start'])); ?>&to=<?php if ($time_tracking['dt_end'] != '0000-00-00 00:00:00') { echo date('m/d/Y', strtotime($time_tracking['dt_end'])); } else { echo date('m/d/Y'); } ?>';
                        });

                        <?php if (count($gps['start']) == 2) { ?>
                            $('#time_tracking .main_map .map_container').addMarker({
                                id: '<?php echo $time_tracking['id']; ?>-start',
                                coords: [<?php echo implode(', ', $gps['start']); ?>],
                                icon: '/images/icons/map/marker_green<?php echo $fl; ?>.png',
                                title: '<?php echo $_uccms_staff->combineName($staff); ?>',
                                text:  '<span class="date"><?php echo date('n/j/Y', strtotime($time_tracking['dt_start'])); ?></span><span class="time"><?php echo date('g:i:s A', strtotime($time_tracking['dt_start'])); ?></span><span class="view"><a href="#" onclick="$(\'#tt-<?php echo $time_tracking['id']; ?>\').click();">View</a></span>',
                                success: function(coords, obj) {
                                    var gm = obj.data('googleMarker');
                                    $('#tt-<?php echo $time_tracking['id']; ?>').hover(function(e) {
                                        Object.keys(gm).forEach(function(key) {
                                            gm[key].setVisible(false);
                                        });
                                        gm['<?php echo $time_tracking['id']; ?>-start'].setVisible(true);
                                        if (typeof gm['<?php echo $time_tracking['id']; ?>-end'] == 'object') {
                                            gm['<?php echo $time_tracking['id']; ?>-end'].setVisible(true);
                                        }
                                    }, function(e) {
                                        Object.keys(gm).forEach(function(key) {
                                            gm[key].setVisible(true);
                                        });
                                    });
                                },
                                mouseOver: function(data) {
                                    $('#tt-<?php echo $time_tracking['id']; ?>').addClass('active');
                                },
                                mouseOut: function(data) {
                                    $('#tt-<?php echo $time_tracking['id']; ?>').removeClass('active');
                                },
                            });
                        <?php } ?>

                        <?php if (count($gps['end']) == 2) { ?>
                            $('#time_tracking .main_map .map_container').addMarker({
                                id: '<?php echo $time_tracking['id']; ?>-end',
                                coords: [<?php echo implode(', ', $gps['end']); ?>],
                                icon: '/images/icons/map/marker_red<?php echo $fl; ?>.png',
                                title: '<?php echo $_uccms_staff->combineName($staff); ?>',
                                text:  '<span class="date"><?php echo date('n/j/Y', strtotime($time_tracking['dt_end'])); ?></span><span class="time"><?php echo date('g:i:s A', strtotime($time_tracking['dt_end'])); ?></span><span class="view"><a href="#">View</a></span>',
                                success: function(coords, obj) {
                                    /*
                                    $('#tt-<?php echo $time_tracking['id']; ?>').hover(function(e) {
                                        $.each(obj.data('googleMarker'), function(i, mkr) {
                                            if (typeof mkr == 'object') {
                                                mkr.setVisible(false);
                                            }
                                        });
                                        obj.data('googleMarker')['<?php echo $time_tracking['id']; ?>-start'].setVisible(true);
                                        obj.data('googleMarker')['<?php echo $time_tracking['id']; ?>-end'].setVisible(true);
                                    }, function(e) {
                                        $.each(obj.data('googleMarker'), function(i, mkr) {
                                            if (typeof mkr == 'object') {
                                                mkr.setVisible(true);
                                            }
                                        });
                                    });
                                    */
                                },
                                mouseOver: function(data) {
                                    $('#tt-<?php echo $time_tracking['id']; ?>').addClass('active');
                                },
                                mouseOut: function(data) {
                                    $('#tt-<?php echo $time_tracking['id']; ?>').removeClass('active');
                                },
                            });
                        <?php } ?>

                    });

                </script>

                <?php

            }

        }

    }

    unset($accta);

// NO PAGED
} else {
    ?>
    <li style="text-align: center;">
        No results found.
    </li>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>