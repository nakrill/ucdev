<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

if (isset($_REQUEST['base_url'])) {
    $base_url = $_REQUEST['base_url'];
} else if (defined('MODULE_ROOT')) {
    $base_url = MODULE_ROOT. 'daily-report';
} else {
    $base_url = '.';
}

// STAFF ID
$staff_id = (int)$_REQUEST['staff_id'];

// HAVE STAFF ID
if ($staff_id) {

    ?>

    <script type="text/javascript">
        var mapMarkers = {};
    </script>

    <?php

    $query  = isset($_REQUEST["query"]) ? $_REQUEST["query"] : "";
    $page   = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;

    if ($_REQUEST['limit']) {
        $_uccms_staff->setPerPage((int)$_REQUEST['limit']);
    }
    $limit = $_uccms_staff->perPage();

    //$_uccms_staff->setPerPage(5);

    // DATES SPECIFIED
    if (($_REQUEST['from']) && ($_REQUEST['to'])) {
        $from = date('Y-m-d', strtotime($_REQUEST['from']));
        $to = date('Y-m-d', strtotime($_REQUEST['to']));
    } else {
        $from = date('Y-m-d');
        $to = date('Y-m-d');
    }

    $total_time = 0;

    // CURRENT STATUS
    $current_query = "
    SELECT *
    FROM `" .$_uccms_staff->tables['time_tracking']. "`
    WHERE (`staff_id`=" .$staff_id. ")
    ORDER BY `dt_start` DESC
    LIMIT 1
    ";
    $current_q = sqlquery($current_query);
    $current = sqlfetch($current_q);

    // GET DATES
    $dates = $_uccms_staff->date_range($from, $to, '+1 Day', 'Y-m-d');

    // NUMBER OF DATES
    $num_dates = count($dates);

    // LOOP
    foreach ($dates as $date) {

        $timeline = array();

        $dt_clock_in = '';
        $dt_clock_out = '';

        $cii = 1;
        $coi = 1;

        // GET TIME TRACKING
        $time_tracking_query = "
        SELECT *
        FROM `" .$_uccms_staff->tables['time_tracking']. "`
        WHERE
            (`staff_id`=" .$staff_id. ") AND
            ((`dt_start` BETWEEN '" .$date. " 00:00:00' AND '" .$date. " 23:59:59')
            OR
            (`dt_end` BETWEEN '" .$date. " 00:00:00' AND '" .$date. " 23:59:59')
            OR
            (('" .$date. " 00:00:00'>=`dt_start`) AND ('" .$date. " 23:59:59'<=`dt_end`)))
        ORDER BY `dt_start` ASC
        ";

        //echo $time_tracking_query;

        $time_tracking_q = sqlquery($time_tracking_query);
        while ($time_tracking = sqlfetch($time_tracking_q)) {

            // THIS DAY
            if (date('Y-m-d', strtotime($time_tracking['dt_start'])) == $date) {

                // TIMELINE ID
                $tlid = strtotime($time_tracking['dt_start']);

                // CLOCK IN TIME (FIRST OF DAY)
                if (!$dt_clock_in) {
                    $dt_clock_in = $time_tracking['dt_start'];
                }

                // TIMELINE
                $timeline[$tlid] = array(
                    'what'          => 'time_tracking',
                    'event'         => 'clock_in',
                    'dt'            => $time_tracking['dt_start'],
                    'color'         => '#8CCD77',
                    'icon'          => '<i class="fa fa-play-circle" aria-hidden="true" style="background-color: #8CCD77;"></i>',
                    'title'         => 'Clock In',
                    'map_marker'    => '/images/icons/map/marker_green' .$cii. '.png',
                );

                // HAVE START GPS
                if (($time_tracking['gps_start_latitude'] != 0) && ($time_tracking['gps_start_longitude'] != 0)) {
                    $timeline[$tlid]['gps'] = array($time_tracking['gps_start_latitude'], $time_tracking['gps_start_longitude']);
                }

                $cii++;

                // CLOCK OUT TIME
                $dt_clock_out = ($time_tracking['dt_end'] != '0000-00-00 00:00:00' ? $time_tracking['dt_end'] : '');

            }

            // HAVE END DATE / TIME
            if ($time_tracking['dt_end'] != '0000-00-00 00:00:00') {

                // THIS DAY
                if (date('Y-m-d', strtotime($time_tracking['dt_end'])) == $date) {

                    // TIMELINE ID
                    $tlid = strtotime($time_tracking['dt_end']);

                    // TIMELINE
                    $timeline[$tlid] = array(
                        'what'          => 'time_tracking',
                        'event'         => 'clock_out',
                        'dt'            => $time_tracking['dt_end'],
                        'color'         => '#FA9289',
                        'icon'          => '<i class="fa fa-stop-circle" aria-hidden="true" style="background-color: #FA9289;"></i>',
                        'title'         => 'Clock Out',
                        'map_marker'    => '/images/icons/map/marker_red' .$coi. '.png',
                    );

                    // HAVE END GPS
                    if (($time_tracking['gps_end_latitude'] != 0) && ($time_tracking['gps_end_longitude'] != 0)) {
                        $timeline[$tlid]['gps'] = array($time_tracking['gps_end_latitude'], $time_tracking['gps_end_longitude']);
                    }

                    //echo date('Y-m-d', strtotime($time_tracking['dt_end'])). ' - ' .$date. '<br />';

                    /*
                    // START AND END ARE ON SAME DAY
                    if (date('Y-m-d', strtotime($time_tracking['dt_start'])) == date('Y-m-d', strtotime($time_tracking['dt_end']))) {

                        // ADD TO TOTAL TIME
                        $total_time += strtotime($time_tracking['dt_end']) - strtotime($time_tracking['dt_start']);

                    // NOT ENDING ON THIS DAY
                    } else if (date('Y-m-d', strtotime($time_tracking['dt_end'])) != $date) {

                        // ADD TO TOTAL TIME
                        $total_time += strtotime($time_tracking['dt_end']) - strtotime($date. ' 23:59:59');

                    // NOT STARTING ON THIS DAY
                    } else if (date('Y-m-d', strtotime($time_tracking['dt_start'])) != $date) {

                        // ADD TO TOTAL TIME
                        $total_time += strtotime($time_tracking['dt_end']) - strtotime($date. ' 00:00:00');

                    } else {
                        echo 'here';
                    }
                    */

                    //echo $total_time. '<br />';

                    $coi++;

                }

            // NO END DATE / TIME
            } else {

                // ADD TO TOTAL TIME
                //$total_time += time() - strtotime($time_tracking['dt_start']);

            }

        }

        $pi = 1;

        // GET PHOTOS
        $photos_query = "
        SELECT *
        FROM `" .$_uccms_staff->tables['photos']. "`
        WHERE
            (`staff_id`=" .$staff_id. ") AND
            ((`dt` BETWEEN '" .$date. " 00:00:00' AND '" .$date. " 23:59:59')
            OR
            (('" .$date. " 00:00:00'>=`dt`) AND ('" .$date. " 23:59:59'<=`dt`)))
        ORDER BY `dt` ASC
        ";

        $photos_q = sqlquery($photos_query);
        while ($photo = sqlfetch($photos_q)) {

            //echo print_r($photo);

            // TIMELINE ID
            $tlid = strtotime($photo['dt']);

            // TIMELINE
            $timeline[$tlid] = array(
                'what'          => 'photo',
                'event'         => 'taken',
                'dt'            => $photo['dt'],
                'color'         => '#40B3FF',
                'icon'          => '<i class="fa fa-camera" aria-hidden="true" style="background-color: #40B3FF;"></i>',
                'title'         => 'Photo Taken',
                'map_marker'    => '/images/icons/map/marker_blue' .$pi. '.png',
                'photo'         => $_uccms_staff->photoURL($photo['id'], $photo),
            );

            // HAVE GPS
            if (($photo['gps_latitude'] != 0) && ($photo['gps_longitude'] != 0)) {
                $timeline[$tlid]['gps'] = array($photo['gps_latitude'], $photo['gps_longitude']);
            }

            $pi++;

        }

        // SORT BY KEY
        ksort($timeline);

        //echo print_r($timeline);

        ?>

        <li id="date_<?php echo $date; ?>" class="item report day">
            <div class="contain clearfix">
                <section class="r_dt">
                    <?php echo date('D, F d', strtotime($date)); ?>
                </section>
                <section class="r_clock_in">
                    <?php if (($dt_clock_in) && ($date != date('Y-m-d'))) { ?>
                        <?php echo date('h:i A', strtotime($dt_clock_in)); ?>
                    <?php } else { ?>
                        <span class="none">-</span>
                    <?php } ?>
                </section>
                <section class="r_clock_out <?php if (!$dt_clock_out) { ?>active<?php } ?>">
                    <?php if ($dt_clock_in) { ?>
                        <?php if ($dt_clock_out) { ?>
                            <?php if ($date == date('Y-m-d')) { ?>
                                <?php echo date('h:i A', strtotime($dt_clock_out)); ?>
                            <?php } else { ?>
                                <span class="none">-</span>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if ($date == date('Y-m-d')) { ?>
                                <span class="circle"></span>
                                <span class="title">Active</span>
                            <?php } else { ?>
                                <span class="none">-</span>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if ($date == date('Y-m-d')) { ?>
                            <?php if ($current['dt_end'] == '0000-00-00 00:00:00') { ?>
                                <span class="circle"></span>
                                <span class="title">Active</span>
                            <?php } else { ?>
                                <?php echo date('h:i A', strtotime($dt_clock_out)); ?>
                            <?php } ?>
                        <?php } else { ?>
                            <span class="none">-</span>
                        <?php } ?>
                    <?php } ?>
                </section>
                <section class="r_hours">
                    <?php

                    if (($dt_clock_in) && (date('Y-m-d', strtotime($dt_clock_in)) == $date)) {
                        $diff_ci = $dt_clock_in;
                    } else {
                        $diff_ci = $date. ' 00:00:00';
                    }

                    if (($dt_clock_out) && (date('Y-m-d', strtotime($dt_clock_out)) == $date)) {
                        $diff_co = $dt_clock_out;
                    } else {
                        if ($date == date('Y-m-d')) {
                            $diff_co = date('Y-m-d H:i:s');
                        } else {
                            $diff_co = $date. ' 23:59:59';
                        }
                    }

                    // SECONDS BETWEEN CLOCK IN AND CLOCK OUT
                    $seconds = strtotime($diff_co) - strtotime($diff_ci);

                    $total_time += $seconds;

                    // FORMAT SECONDS
                    echo $_uccms_staff->formatSeconds($seconds). ' hr';

                    /*
                    $diff = $_uccms_staff->dtDiff($diff_ci, $diff_co, array(
                        'format' => '%H:%I'
                    ));

                    echo $diff. ' hr';
                    */

                    ?>
                </section>
                <section class="r_expand">
                    <?php if (count($timeline) > 0) { ?>
                        <a href="#"><i class="fa fa-chevron-<?php if ($num_dates > 1) { echo 'down'; } else { echo 'up'; } ?>" aria-hidden="true"></i></a>
                    <?php } ?>
                </section>
            </div>

            <div class="toggle_content">

                <?php if ($google_maps_key) { ?>
                    <div class="map"></div>
                <?php } ?>

                <?php

                //echo print_r($timeline);

                // HAVE TIMELINE
                if (count($timeline) > 0) {

                    ?>

                    <div class="timeline">

                        <?php

                        $tli = 1;

                        // LOOP
                        foreach ($timeline as $tlid => $tl) {

                            ?>

                            <div id="e-<?php echo $tlid; ?>" class="event clearfix">

                                <div class="icon">
                                    <?php echo $tl['icon']; ?>
                                </div>

                                <div class="map_marker">
                                    <?php

                                    // HAVE GPS
                                    if (count($tl['gps']) == 2) {
                                        ?>
                                        <img src="<?php echo $tl['map_marker']; ?>" alt="" />
                                        <?php
                                    }

                                    ?>
                                </div>

                                <div class="dt">
                                    <?php echo date('h:i:s A', strtotime($tl['dt'])); ?>
                                </div>

                                <div class="title">
                                    <?php echo $tl['title']; ?>
                                </div>

                                <?php if ($tl['photo']) { ?>
                                    <div class="photo">
                                        <a class="thumb" style="background-image: url('<?php echo $tl['photo']; ?>');" data-image="<?php echo $tl['photo']; ?>"></a>
                                    </div>
                                <?php } ?>

                            </div>

                            <script type="text/javascript">

                                if (typeof mapMarkers['<?php echo $date; ?>'] != 'object') {
                                    mapMarkers['<?php echo $date; ?>'] = {};
                                }

                                mapMarkers['<?php echo $date; ?>']['<?php echo $tlid; ?>'] = {
                                    id: 'tle-<?php echo $tli; ?>',
                                    coords: [<?php echo implode(', ', $tl['gps']); ?>],
                                    icon: '<?php echo $tl['map_marker']; ?>',
                                    title: '<?php echo $tl['title']; ?>',
                                    text:  '<span class="date"><?php echo date('n/j/Y', strtotime($tl['dt'])); ?></span><span class="time"><?php echo date('g:i:s A', strtotime($tl['dt'])); ?></span></span>',
                                    success: function(coords, obj) {
                                        var gm = obj.data('googleMarker');
                                        $('#e-<?php echo $tlid; ?>').hover(function(e) {
                                            Object.keys(gm).forEach(function(key) {
                                                gm[key].setVisible(false);
                                            });
                                            gm['tle-<?php echo $tli; ?>'].setVisible(true);
                                        }, function(e) {
                                            Object.keys(gm).forEach(function(key) {
                                                gm[key].setVisible(true);
                                            });
                                        });
                                    },
                                    mouseOver: function(data) {
                                        $('#e-<?php echo $tlid; ?>').addClass('active');
                                    },
                                    mouseOut: function(data) {
                                        $('#e-<?php echo $tlid; ?>').removeClass('active');
                                    },
                                };

                            </script>

                            <?php

                            $tli++;

                        }

                        ?>

                    </div>

                    <?php

                }

                ?>

            </div>
        </li>

        <?php

    }

    // HAVE TOTAL TIME
    if ($total_time) {

        /*
        $total_time_formatted = $_uccms_staff->dtDiff('@0', "@$total_time", array(
            'format' => '%H:%I'
        ));
        */

        //echo $total_time;

        // TOTAL TIME
        $total_time_formatted = $_uccms_staff->formatSeconds($total_time);

    } else {
        $total_time_formatted = '00:00';
    }

    ?>

    <script type="text/javascript">

        $(document).ready(function() {

            var maps = {};

            // TOTALS
            $('#report .main_section .totals .item.total .value').html('<?php echo $total_time_formatted; ?> hr');
            $('#report .main_section .totals .item.total').show();

            // EXPAND CLICK
            $('#report .report .r_expand a').click(function(e) {
                e.preventDefault();

                var $parent = $(this).closest('.item');
                var day_id = $parent.attr('id');
                var date = day_id.replace('date_', '');

                $parent.find('.toggle_content').slideToggle(400, function() {

                    if ($(this).is(':visible')) {

                        $parent.find('.r_expand a i').removeClass('fa-chevron-down').addClass('fa-chevron-up');

                        $('html, body').animate({
                            scrollTop: $parent.offset().top
                        }, 600);

                        <?php

                        // HAVE GOOGLE MAPS KEY
                        if ($google_maps_key) {

                            ?>

                            if (!maps[day_id]) {

                                // MAP
                                maps[day_id] = $parent.find('.toggle_content .map').googleMap({
                                    zoom: 10,
                                    overviewMapControl: true,
                                    streetViewControl: true,
                                    scrollwheel: true,
                                    mapTypeControl: true
                                });

                                if (typeof mapMarkers[date] == 'object') {
                                    $.each(mapMarkers[date], function(i, val) {
                                        maps[day_id].addMarker(val);
                                    });
                                }

                            }

                            <?php

                        }

                        ?>

                    } else {
                        $parent.find('.r_expand a i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                    }

                });

            });


            <?php if ($num_dates == 1) { ?>
                $('#report .report .r_expand a').click();
            <?php } ?>

            // PHOTO LIGHTWINDOWS
            $('#report .report .toggle_content .timeline .event .photo .thumb').click(function(e) {
                e.preventDefault();
                var image_url = $(this).attr('data-image');
                $.featherlight(image_url);
            });

        });

    </script>

    <?php

// NO STAFF ID
} else {

    ?>

    <div style="padding: 15px; text-align: center;">
        Please select a staff.
    </div>

    <?php

}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=(int)$pages?>, <?=(int)$page?>);
</script>