<?php

$out = [];

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    $id = (int)$_REQUEST['id'];

    // START DATE / TIME
    if ($_REQUEST['start']) {
        $dt_start = date('Y-m-d H:i:s', strtotime($_REQUEST['start']));
    } else {
        $dt_start = '0000-00-00 00:00:00';
    }

    // END DATE / TIME
    if ($_REQUEST['end']) {
        $dt_end = date('Y-m-d H:i:s', strtotime($_REQUEST['end']));
    } else {
        $dt_end = '0000-00-00 00:00:00';
    }

    $recurring_byday = '';
    $recurring_bymonthday = 0;

    // FREQ - WEEKLY
    if ($_REQUEST['recurring_freq'] == 'week') {
        $recurring_byday = implode(',', $_REQUEST['recurring_days']);

    // FREQ - MONTHLY
    } else if ($_REQUEST['recurring_freq'] == 'month') {
        if ($_REQUEST['recurring_by'] == 'dotw') {
            $dowa = $_uccms_staff->daysOfWeek();
            $recurring_byday = $dowa[date('N', strtotime($dt_start))]['code'];
        } else {
            $recurring_bymonthday = date('j', strtotime($dt_start));
        }
    }

    // RECURRING END - ON END DATE
    if ($_REQUEST['recurring_end'] == 0) {
        if (!$_REQUEST['end']) {
            $dt_end = $dt_start;
        }
    }

    // RECURRING END - AFTER COUNT
    if ($_REQUEST['recurring_end'] == 1) {
        $dt_end = '0000-00-00 ' .date('H:i:s', strtotime($_REQUEST['end']));
    } else {
        $_REQUEST['recurring_count'] = 0;
    }

    // RECURRING END - NEVER
    if ($_REQUEST['recurring_end'] == -1) {
        $dt_end = '0000-00-00 ' .date('H:i:s', strtotime($_REQUEST['end']));
        $_REQUEST['recurring_count'] = -1;
    }

    $columns = [
        'active'                    => (int)$_REQUEST['active'],
        'title'                     => $_REQUEST['title'],
        'start'                     => $dt_start,
        'end'                       => $dt_end,
        'recurring'                 => (int)$_REQUEST['recurring'],
        'recurring_freq'            => $_REQUEST['recurring_freq'],
        'recurring_interval'        => (int)$_REQUEST['recurring_interval'],
        'recurring_count'           => (float)$_REQUEST['recurring_count'],
        'recurring_byday'           => $recurring_byday,
        'recurring_bymonthday'      => $recurring_bymonthday,
    ];

    // EXISTING
    if ($id) {

        $sql = "UPDATE `" .$_uccms_staff->tables['routes']. "` SET " .$_uccms_staff->createSet($columns). " WHERE (`id`=" .$id. ")";

    // NEW
    } else {

        $sql = "INSERT INTO `" .$_uccms_staff->tables['routes']. "` SET " .$_uccms_staff->createSet($columns);

    }

    if (sqlquery($sql)) {

        if (!$id) $id = sqlid();

        $out['success'] = true;

    } else {
        $out['error'] = 'Failed to save route.';
    }

}

echo json_encode($out);

?>