<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // GET ROUTES
    $routes = $_uccms_staff->getRoutes(false);

    ?>

    <style type="text/css">

    </style>

    <script type="text/javascript">

        $(function() {

            // SORTABLE
            $('#routes .items').sortable({
                axis: 'y',
                containment: 'parent',
                handle: '.sortable-handle',
                items: 'tr',
                placeholder: 'ui-sortable-placeholder',
                tolerance: 'pointer',
                update: function() {
                    $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/routes/order/', $('#routes .items').sortable('serialize', {
                        attribute: 'data-id',
                        key: 'sort[]',
                        expression: /(.+)/
                    }));
                }
            });

            // EDIT
            $('#routes .item .edit').click(function(e) {
                editRoute({
                    id: $(this).closest('.item').data('id')
                });
            });

            // DELETE
            $('#routes .item .delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var $item = $(this).closest('.item');
                    $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/routes/delete/', {
                        id: $item.data('id'),
                    }, function(data) {
                        $item.fadeOut(400, function() {
                            getRouteList();
                        });
                    }, 'html');
                }
            });

        });

    </script>

    <?php if (count($routes) > 0) { ?>

        <table class="table sortable">
            <thead class="thead-light">
                <tr>
                    <th class="name" scope="col">Name</th>
                    <th class="depart" scope="col">Depart</th>
                    <th class="status" scope="col">Status</th>
                    <th class="options" scope="col">Options</th>
                </tr>
            </thead>
            <tbody class="items">

                <?php foreach ($routes as $route) { ?>

                    <tr class="item" data-id="<?php echo $route['id']; ?>">
                        <td class="name">
                            <i title="Re-order" class="sortable-handle fas fa-sort"></i>
                            <span class="title"><?php echo stripslashes($route['title']); ?></span>
                        </td>
                        <td class="depart">
                            <?php echo ($route['depart'] ? date('g:i a', strtotime($route['depart'])) : '&nbsp;'); ?>
                        </td>
                        <td class="status">
                            <?php if ($route['active']) { ?>
                                <span class="badge badge-success">Active</span>
                            <?php } else { ?>
                                <span class="badge badge-secondary">Inactive</span>
                            <?php } ?>
                        </td>
                        <td class="options">
                            <a href="#" class="edit" title="Edit" <? /*data-toggle="modal" data-target="#modal-editRoute"*/ ?> data-id="<?php echo $route['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                            <a href="#" class="delete" title="Delete"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>

                <?php } ?>

            </tbody>
        </table>

    <?php } else { ?>

        <div class="no-results">
            No routes created yet.
        </div>

    <?php } ?>

    <?php

}

?>