<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // GET ROUTE INFO
    $route = $_uccms_staff->getRoute((int)$_REQUEST['id']);

    ?>

    <style type="text/css">

        #modal-editRoute .repeats_enabled_content .repeat_every label {
            font-weight: 500;
        }
        #modal-editRoute .repeats_enabled_content .repeat_every select {
            display: inline-block;
            width: auto;
        }
        #modal-editRoute .repeats_enabled_content .repeat_every span {
            padding-left: 3px;
            font-size: 14px;
        }

        #modal-editRoute .repeats_enabled_content .freq_content .contain {
            display: flex;
            justify-content: space-between;
        }
        #modal-editRoute .repeats_enabled_content .freq_content .dayofweek {

        }

        #modal-editRoute .repeats_enabled_content .repeats_end {
            margin: 0px;
        }
        #modal-editRoute .repeats_enabled_content .repeats_end label {
            font-weight: 500;
        }
        #modal-editRoute .repeats_enabled_content .repeats_end input[type="text"] {
            display: inline-block;
            width: 72px;
        }

    </style>

    <script type="text/javascript">

        $(function() {

            $('#modal-editRoute .modal-title').text($('#modal-editRoute input[name="id"]').val() ? 'Edit Route' : 'Add Route');

            <?php if ($route['title']) { ?>
                $('#modal-editRoute .btn.save').prop('disabled', '');
            <?php } ?>

            // TIME PICKER
            $('#modal-editRoute .time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
            $('#modal-editRoute .date_time_picker').datetimepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });

            // NAME CHANGE
            $('#modal-editRoute input[name="title"]').keyup(function(e) {
                if ($(this).val().trim()) {
                    $('#modal-editRoute .btn.save').prop('disabled', '');
                } else {
                    $('#modal-editRoute .btn.save').prop('disabled', 'disabled');
                }
            });

            // REPEATS TOGGLE
            $('#modal-editRoute input[name="recurring"]').change(function(e) {
                if ($(this).prop('checked')) {
                    $('#modal-editRoute .repeats_enabled_content').show();
                } else {
                    $('#modal-editRoute .repeats_enabled_content').hide();
                }
            });

            // REPEATS FREQUENCY CHANGE
            $('#modal-editRoute .repeats_enabled_content select[name="recurring_freq"]').change(function() {
                var freq = $(this).val();
                $('#modal-editRoute .freq_content_container .freq_content').hide();
                $('#modal-editRoute .freq_content_container .freq_content.' +freq).show();
                $('#modal-editRoute .repeats_enabled_content .repeat_every .re').hide();
                $('#modal-editRoute .repeats_enabled_content .repeat_every .re.' +freq).show();
            });

        });

    </script>

    <form>

    <input type="hidden" name="id" value="<?php echo $route['id']; ?>" />

    <div class="form-group">
        <label for="modal-editRoute-title">Name</label>
        <input type="text" name="title" value="<?php echo stripslashes($route['title']); ?>" class="form-control" id="modal-editRoute-title" placeholder="Enter title">
    </div>

    <div class="form-group">
        <label for="modal-editRoute-active">Active</label>
        <input type="checkbox" name="active" value="1" id="modal-editRoute-active" <?php if ($route['active']) { ?>checked="checked"<?php } ?>>
    </div>

    <div class="form-group" style="position: relative;">
        <label for="modal-editRoute-depart">Depart</label>
        <?php
        $field = array(
            'key'       => 'depart', // The value you should use for the "name" attribute of your form field
            'value'     => ($route['depart'] ? $route['depart'] : ''), // The existing value for this form field
            'id'        => 'modal-editRoute-depart', // A unique ID you can assign to your form field for use in JavaScript
            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
            'options'   => []
        );
        include(BigTree::path('admin/form-field-types/draw/time.php'));
        ?>
    </div>

    <div class="form-group" style="position: relative;">
        <label for="modal-editRoute-start">Start</label>
        <?php
        $field = array(
            'key'       => 'start', // The value you should use for the "name" attribute of your form field
            'value'     => ($route['start'] ? $route['start'] : ''), // The existing value for this form field
            'id'        => 'modal-editRoute-start', // A unique ID you can assign to your form field for use in JavaScript
            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
            'options'   => []
        );
        include(BigTree::path('admin/form-field-types/draw/datetime.php'));
        ?>
    </div>

    <div class="form-group" style="position: relative;">
        <label for="modal-editRoute-start">End</label>
        <?php
        $field = array(
            'key'       => 'end', // The value you should use for the "name" attribute of your form field
            'value'     => ($route['end'] ? $route['end'] : ''), // The existing value for this form field
            'id'        => 'modal-editRoute-end', // A unique ID you can assign to your form field for use in JavaScript
            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
            'options'   => []
        );
        include(BigTree::path('admin/form-field-types/draw/datetime.php'));
        ?>
    </div>

    <div class="card recurring">
        <div class="card-body">
            <h5 class="card-title">Recurring</h5>

            <div class="form-group" style="margin-bottom: 0px;">
                <label><input id="repeats_enabled" type="checkbox" name="recurring" value="1" <?php if ($route['recurring']) { ?>checked="checked"<?php } ?> /> Repeats</label>
            </div>

            <div class="repeats_enabled_content" style="padding-top: 5px;<?php if (!$route['recurring']) { echo ' display: none;'; } ?>">

                <div class="row">

                    <div class="col-md-6">

                        <fieldset class="form-group">
                            <select name="recurring_freq" class="form-control">
                                <option value="day" <?php if ($route['recurring_freq'] == 'day') { ?>selected="selected"<?php } ?>>Daily</option>
                                <option value="week" <?php if ($route['recurring_freq'] == 'week') { ?>selected="selected"<?php } ?>>Weekly</option>
                                <option value="month" <?php if ($route['recurring_freq'] == 'month') { ?>selected="selected"<?php } ?>>Monthly</option>
                                <option value="year" <?php if ($route['recurring_freq'] == 'year') { ?>selected="selected"<?php } ?>>Yearly</option>
                            </select>
                        </fieldset>

                    </div>

                    <div class="col-md-6">

                        <fieldset class="repeat_every form-group">
                            <label>Every</label>
                            <select name="recurring_interval" class="form-control">
                                <?php for ($i=1; $i<=31; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php if ($i == $route['recurring_interval']) { echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select> <span class="re day" style="<?php if (($route['recurring_freq'] != 'day') && ($route['recurring_freq'])) { ?>display: none;<?php } ?>">days</span><span class="re week" style="<?php if ($route['recurring_freq'] != 'week') { ?>display: none;<?php } ?>">weeks</span><span class="re month" style="<?php if ($route['recurring_freq'] != 'month') { ?>display: none;<?php } ?>">months</span><span class="re year" style="<?php if ($route['recurring_freq'] != 'year') { ?>display: none;<?php } ?>">years</span>
                        </fieldset>

                    </div>

                </div>

                <div class="freq_content_container">

                    <div class="freq_content week form-group" style="<?php if ($route['recurring_freq'] != 'week') { echo 'display: none;'; } ?>">
                        <label>Repeat On</label>
                        <div class="contain">
                            <?php
                            $seldaya = [];
                            if ($route['recurring_freq'] == 'week') {
                                $seldaya = explode(',', stripslashes($route['recurring_byday']));
                            }
                            foreach ($_uccms_staff->daysOfWeek() as $day_id => $day) {
                                ?>
                                <div class="dayofweek">
                                    <input type="checkbox" name="recurring_days[]" value="<?php echo $day['code']; ?>" <?php if (in_array($day['code'], $seldaya)) { ?>checked="checked"<?php } ?> /> <?php echo substr($day['title'], 0, 1); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="freq_content month form-group" style="<?php if ($route['recurring_freq'] != 'month') { echo 'display: none;'; } ?>">
                        <label>Repeat By</label>
                        <div class="contain">
                            <div style="float: left;">
                                <input type="radio" name="recurring_by" value="dotw" <?php if ($route['recurring_byday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Week</label>
                            </div>
                            <div style="float: left;">
                                <input type="radio" name="recurring_by" value="dotm" <?php if ($route['recurring_bymonthday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Month</label>
                            </div>
                        </div>
                    </div>

                </div>

                <fieldset class="repeats_end form-group">
                    <label>Ends</label>
                    <div>
                        <fieldset class="last">
                            <input type="radio" name="recurring_end" value="0" <?php if (!$route['recurring_count']) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">On end date</label>
                        </fieldset>
                        <fieldset class="last radio_padding">
                            <input type="radio" name="recurring_end" value="1" <?php if ($route['recurring_count'] > 0) { echo 'checked="checked"'; } ?> style="margin-top: 3px;" /><label class="for_checkbox">After <input type="text" name="route[recurring_count]" value="<?php if ($route['recurring_count'] > 0) { echo $route['recurring_count']; } ?>" class="form-control" /> occurances</label>
                        </fieldset>
                        <fieldset>
                            <input type="radio" name="recurring_end" value="-1" <?php if ($route['recurring_count'] == -1) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">Never</label>
                        </fieldset>
                    </div>
                </fieldset>

            </div>

        </div>
    </div>

    </form>

    <?php

}

?>