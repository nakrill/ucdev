<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // GET ROLES
    $roles = $_uccms_staff->getRoles(false);

    ?>

    <style type="text/css">

    </style>

    <script type="text/javascript">

        $(function() {

            // SORTABLE
            $('#roles .items').sortable({
                axis: 'y',
                containment: 'parent',
                handle: '.sortable-handle',
                items: 'tr',
                placeholder: 'ui-sortable-placeholder',
                tolerance: 'pointer',
                update: function() {
                    $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/roles/order/', $('#roles .items').sortable('serialize', {
                        attribute: 'data-id',
                        key: 'sort[]',
                        expression: /(.+)/
                    }));
                }
            });

            // EDIT
            $('#roles .item .edit').click(function(e) {
                editRole({
                    id: $(this).closest('.item').data('id')
                });
            });

            // DELETE
            $('#roles .item .delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var $item = $(this).closest('.item');
                    $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/roles/delete/', {
                        id: $item.data('id'),
                    }, function(data) {
                        $item.fadeOut(400, function() {
                            getRoleList();
                        });
                    }, 'html');
                }
            });

        });

    </script>

    <?php if (count($roles) > 0) { ?>

        <table class="table sortable">
            <thead class="thead-light">
                <tr>
                    <th class="name" scope="col">Name</th>
                    <th class="status" scope="col">Status</th>
                    <th class="options" scope="col">Options</th>
                </tr>
            </thead>
            <tbody class="items">

                <?php foreach ($roles as $role) { ?>

                    <tr class="item" data-id="<?php echo $role['id']; ?>">
                        <td class="name">
                            <i title="Re-order" class="sortable-handle fas fa-sort"></i>
                            <span class="title"><?php echo stripslashes($role['title']); ?></span>
                        </td>
                        <td class="status">
                            <?php if ($role['active']) { ?>
                                <span class="badge badge-success">Active</span>
                            <?php } else { ?>
                                <span class="badge badge-secondary">Inactive</span>
                            <?php } ?>
                        </td>
                        <td class="options">
                            <a href="#" class="edit" title="Edit" <? /*data-toggle="modal" data-target="#modal-editRole"*/ ?> data-id="<?php echo $role['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                            <a href="#" class="delete" title="Delete"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>

                <?php } ?>

            </tbody>
        </table>

    <?php } else { ?>

        <div class="no-results">
            No roles created yet.
        </div>

    <?php } ?>

    <?php

}

?>