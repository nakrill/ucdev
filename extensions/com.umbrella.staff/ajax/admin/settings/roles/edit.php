<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // GET ROLE INFO
    $role = $_uccms_staff->getRole((int)$_REQUEST['id']);

    ?>

    <script type="text/javascript">

        $(function() {

            // FOCUS ON NAME
            //$('#modal-editRole input[name="title"]').focus();

            <?php if ($role['title']) { ?>
                $('#modal-editRole .btn.save').prop('disabled', '');
            <?php } ?>

            // NAME CHANGE
            $('#modal-editRole input[name="title"]').keyup(function(e) {
                if ($(this).val().trim()) {
                    $('#modal-editRole .btn.save').prop('disabled', '');
                } else {
                    $('#modal-editRole .btn.save').prop('disabled', 'disabled');
                }
            });

        });

    </script>

    <form>

    <input type="hidden" name="id" value="<?php echo $role['id']; ?>" />

    <div class="form-group">
        <label for="modal-editRole-title">Name</label>
        <input type="text" name="title" value="<?php echo stripslashes($role['title']); ?>" class="form-control" id="modal-editRole-title" placeholder="Enter title">
    </div>

    <div class="form-group">
        <label for="modal-editRole-active">Active</label>
        <input type="checkbox" name="active" value="1" id="modal-editRole-active" <?php if ($role['active']) { ?>checked="checked"<?php } ?>>
    </div>

    </form>

    <?php

}

?>