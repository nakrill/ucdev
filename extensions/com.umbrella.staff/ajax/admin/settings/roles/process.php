<?php

$out = [];

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    $id = (int)$_REQUEST['id'];

    $columns = [
        'active'    => (int)$_REQUEST['active'],
        'title'     => $_REQUEST['title']
    ];

    // EXISTING
    if ($id) {

        $sql = "UPDATE `" .$_uccms_staff->tables['roles']. "` SET " .$_uccms_staff->createSet($columns). " WHERE (`id`=" .$id. ")";

    // NEW
    } else {

        $sql = "INSERT INTO `" .$_uccms_staff->tables['roles']. "` SET " .$_uccms_staff->createSet($columns);

    }

    if (sqlquery($sql)) {

        if (!$id) $id = sqlid();

        $out['success'] = true;

    } else {
        $out['error'] = 'Failed to save role.';
    }

}

echo json_encode($out);

?>