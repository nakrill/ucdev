<?php

$out = [];

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // HAVE DATA
    if ($_POST['sort']) {

        // GET VARIABLES
        //parse_str($_POST['sort']);

        // LOOP
        foreach ($_POST['sort'] as $pos => $id) {

            // UPDATE
            $query = "UPDATE `" .$_uccms_staff->tables['roles']. "` SET `sort`=" .(int)$pos. " WHERE (`id`=" .(int)$id. ")";
            sqlquery($query);

        }

        $out['success'] = true;

    }

}

echo json_encode($out);

?>