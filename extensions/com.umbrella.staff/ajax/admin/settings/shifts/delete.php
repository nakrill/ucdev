<?php

$out = [];

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // HAVE DATA
    if ($id) {

        // DELETE
        $query = "DELETE FROM `" .$_uccms_staff->tables['shifts']. "` WHERE (`id`=" .(int)$id. ")";
        if (sqlquery($query)) {
            $out['success'] = true;
        }

    }

}

echo json_encode($out);

?>