<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // GET SHIFTS
    $shifts = $_uccms_staff->getShifts(false);

    ?>

    <style type="text/css">

    </style>

    <script type="text/javascript">

        $(function() {

            // SORTABLE
            $('#shifts .items').sortable({
                axis: 'y',
                containment: 'parent',
                handle: '.sortable-handle',
                items: 'tr',
                placeholder: 'ui-sortable-placeholder',
                tolerance: 'pointer',
                update: function() {
                    $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/shifts/order/', $('#shifts .items').sortable('serialize', {
                        attribute: 'data-id',
                        key: 'sort[]',
                        expression: /(.+)/
                    }));
                }
            });

            // EDIT
            $('#shifts .item .edit').click(function(e) {
                editShift({
                    id: $(this).closest('.item').data('id')
                });
            });

            // DELETE
            $('#shifts .item .delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var $item = $(this).closest('.item');
                    $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/shifts/delete/', {
                        id: $item.data('id'),
                    }, function(data) {
                        $item.fadeOut(400, function() {
                            getShiftList();
                        });
                    }, 'html');
                }
            });

        });

    </script>

    <?php if (count($shifts) > 0) { ?>

        <table class="table sortable">
            <thead class="thead-light">
                <tr>
                    <th class="name" scope="col">Name</th>
                    <th class="start" scope="col">Start time</th>
                    <th class="end" scope="col">End time</th>
                    <th class="status" scope="col">Status</th>
                    <th class="options" scope="col">Options</th>
                </tr>
            </thead>
            <tbody class="items">

                <?php foreach ($shifts as $shift) { ?>

                    <tr class="item" data-id="<?php echo $shift['id']; ?>">
                        <td class="name">
                            <i title="Re-order" class="sortable-handle fas fa-sort"></i>
                            <span class="title"><?php echo stripslashes($shift['title']); ?></span>
                        </td>
                        <td class="start">
                            <?php echo ($shift['start'] ? date('g:i a', strtotime($shift['start'])) : '&nbsp;'); ?>
                        </td>
                        <td class="end">
                            <?php echo ($shift['end'] ? date('g:i a', strtotime($shift['end'])) : '&nbsp;'); ?>
                        </td>
                        <td class="status">
                            <?php if ($shift['active']) { ?>
                                <span class="badge badge-success">Active</span>
                            <?php } else { ?>
                                <span class="badge badge-secondary">Inactive</span>
                            <?php } ?>
                        </td>
                        <td class="options">
                            <a href="#" class="edit" title="Edit" <? /*data-toggle="modal" data-target="#modal-editShift"*/ ?> data-id="<?php echo $shift['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                            <a href="#" class="delete" title="Delete"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>

                <?php } ?>

            </tbody>
        </table>

    <?php } else { ?>

        <div class="no-results">
            No shifts created yet.
        </div>

    <?php } ?>

    <?php

}

?>