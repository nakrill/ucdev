<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // GET SHIFT INFO
    $shift = $_uccms_staff->getShift((int)$_REQUEST['id']);

    ?>

    <script type="text/javascript">

        $(function() {

            // FOCUS ON NAME
            //$('#modal-editShift input[name="title"]').focus();

            <?php if ($shift['title']) { ?>
                $('#modal-editShift .btn.save').prop('disabled', '');
            <?php } ?>

            // TIME PICKER
            $('#modal-editShift .time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });

            // NAME CHANGE
            $('#modal-editShift input[name="title"]').keyup(function(e) {
                if ($(this).val().trim()) {
                    $('#modal-editShift .btn.save').prop('disabled', '');
                } else {
                    $('#modal-editShift .btn.save').prop('disabled', 'disabled');
                }
            });

        });

    </script>

    <form>

    <input type="hidden" name="id" value="<?php echo $shift['id']; ?>" />

    <div class="form-group">
        <label for="modal-editShift-title">Name</label>
        <input type="text" name="title" value="<?php echo stripslashes($shift['title']); ?>" class="form-control" id="modal-editShift-title" placeholder="Enter title">
    </div>

    <div class="form-group">
        <label for="modal-editShift-active">Active</label>
        <input type="checkbox" name="active" value="1" id="modal-editShift-active" <?php if ($shift['active']) { ?>checked="checked"<?php } ?>>
    </div>

    <div class="form-group" style="position: relative;">
        <label for="modal-editShift-start">Start time</label>
        <?php
        $field = array(
            'key'       => 'start', // The value you should use for the "name" attribute of your form field
            'value'     => ($shift['start'] ? $shift['start'] : ''), // The existing value for this form field
            'id'        => 'modal-editShift-start', // A unique ID you can assign to your form field for use in JavaScript
            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
            'options'   => []
        );
        include(BigTree::path('admin/form-field-types/draw/time.php'));
        ?>
    </div>

    <div class="form-group" style="position: relative;">
        <label for="modal-editShift-start">End time</label>
        <?php
        $field = array(
            'key'       => 'end', // The value you should use for the "name" attribute of your form field
            'value'     => ($shift['end'] ? $shift['end'] : ''), // The existing value for this form field
            'id'        => 'modal-editShift-end', // A unique ID you can assign to your form field for use in JavaScript
            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
            'options'   => []
        );
        include(BigTree::path('admin/form-field-types/draw/time.php'));
        ?>
    </div>

    </form>

    <?php

}

?>