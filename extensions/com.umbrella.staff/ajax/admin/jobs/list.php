<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    //$_REQUEST['limit'] = 1;

    // GET JOBS
    $jobs = $_uccms_staff->getJobs($_REQUEST);

    // HAVE JOBS
    if (count((array)$jobs['jobs']) > 0) {

        // GET STAFF
        $staffs = $_uccms_staff->getStaffs();

        ?>

        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th class="name" scope="col">Name</th>
                    <th class="status" scope="col">Status</th>
                    <th class="date" scope="col">Date</th>
                    <th class="status" scope="col">Staff</th>
                    <th class="options" scope="col">Options</th>
                </tr>
            </thead>
            <tbody class="items">

                <?php

                // LOOP THROUGH JOBS
                foreach ($jobs['jobs'] as $job) {

                    // JOB STAFF
                    $jstaff = $_uccms_staff->jobStaff($job['id']);

                    ?>

                    <tr class="item" data-id="<?php echo $job['id']; ?>">
                        <td class="name">
                            <a href="./edit/?id=<?php echo $job['id']; ?>" class="title main"><?php echo stripslashes($job['title']); ?></a>
                        </td>
                        <td class="status">
                            <span class="badge" style="border-color: <?php echo $_uccms_staff->jobStatuses($job['status'])['color']; ?>; color: <?php echo $_uccms_staff->jobStatuses($job['status'])['color']; ?>;"><?php echo $_uccms_staff->jobStatuses($job['status'])['title']; ?></span>
                        </td>
                        <td class="date">
                            <?php
                            $da = [];
                            if (($job['start']) && ($job['start'] != '0000-00-00 00:00:00')) $da[] = date('n/j/Y g:i a', strtotime($job['start']));
                            if (($job['end']) && ($job['end'] != '0000-00-00 00:00:00')) $da[] = date('n/j/Y g:i a', strtotime($job['end']));
                            echo implode(' - ', $da);
                            ?>
                        </td>
                        <td class="staff">
                            <?php foreach ((array)$jstaff as $staff) { ?>
                                <span class="badge"><?php echo trim(stripslashes($staffs['staff'][$staff['staff_id']]['firstname']. ' ' .$staffs['staff'][$staff['staff_id']]['lastname']{0})); ?></span>
                            <?php } ?>
                        </td>
                        <td class="options">
                            <a href="#" class="edit job modalLink-editJob" data-jobid="<?php echo $job['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                            <? /*<a href="./edit/?id=<?php echo $job['id']; ?>" class="edit" title="Edit" data-id="<?php echo $job['id']; ?>"><i class="fas fa-pencil-alt"></i></a>*/ ?>
                            <a href="#" class="delete" title="Delete"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>

                    <?php

                }

                ?>

            </tbody>
        </table>

    <?php } else { ?>

        <div class="no-results">
            No jobs found.
        </div>

    <?php } ?>

    <?php

}

?>

<script>
    BigTree.setPageCount('#jobs .summary .view_paging' ,<?php echo (int)$jobs['pages']; ?>, <?php echo (int)$jobs['page']; ?>);
</script>