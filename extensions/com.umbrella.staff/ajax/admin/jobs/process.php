<?php

$out = [];

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // SAVE JOB
    $result = $_uccms_staff->saveJob($_POST['job']);

    if ($result['success']) {
        $out['success'] = true;
        $out['id'] = $result['id'];
    } else {
        $out['error'] = ($result['error'] ? $result['error'] : 'Failed to save job.');
    }

}

echo json_encode($out);

?>