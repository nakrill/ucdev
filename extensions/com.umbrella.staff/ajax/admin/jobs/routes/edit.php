<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    // DATE
    $date = date('Y-m-d H:i:s', ($_REQUEST['date'] ? strtotime($_REQUEST['date']) : ''));

    // GET ROUTE INFO
    $route = $_uccms_staff->getRoute((int)$_REQUEST['id'], $date);

    //print_r($route);
    //exit;

    // ROUTE FOUND
    if ($route['id']) {

        // ROUTE STAFF
        $rstaffs = $_uccms_staff->routeStaffs($route['id'], $date);

        // GET ROUTE JOBS
        $rjobs = $_uccms_staff->routeJobs($route['id'], $date);

        $num_jobs = count((array)$rjobs['jobs']);

        // GET ROUTE CUSTOMERS
        $rcustomers = $_uccms_staff->routeCustomers($route['id'], [
            'jobs' => $rjobs['jobs']
        ]);

        //print_r($rcustomers);
        //exit;

        // CUSTOMERS BY JOB
        $jcustomers = [];

        // HAVE E-COMMERCE
        if (class_exists('uccms_Ecommerce')) {

            $_uccms_ecomm = new uccms_Ecommerce();

            // CONVERT ROUTE CUSTOMERS TO JOB CUSTOMERS
            foreach ((array)$rcustomers as $customer_id => $contacts) {
                foreach ($contacts as $contact) {
                    foreach ((array)$contact['jobs'] as $job_id) {
                        $jcustomers[$job_id] = $_uccms_ecomm->customerContact($customer_id, $contact['contact_id']);
                    }
                }
            }

        }

        //print_r($jcustomers);

        // JOB - ADD / EDIT MODAL CODE
        include_once(SERVER_ROOT. 'extensions/' .$_uccms_staff->Extension. '/modules/elements/jobs/modal_add-edit.php');

        ?>

        <style type="text/css">

            #modal-editRoute form .alert {
                display: none;
            }

            #modal-editRoute .info .date {
                font-size: 1.1em;
                font-weight: 600;
            }
            #modal-editRoute .info .customers {
                margin-top: 5px;
            }
            #modal-editRoute .info .depart {
                margin-top: 5px;
            }
            #modal-editRoute .info i {
                opacity: .8;
            }

            #modal-editRoute .jobs-list {
                margin-top: 15px;
            }


        </style>

        <script type="text/javascript">

            $(function() {

                $('#modal-editRoute .modal-title').text('<?php echo stripslashes($route['title']); ?>');

                // ENABLE SAVE
                $('#modal-editRoute input').change(function(e) {
                    $('#modal-editRoute .btn.save').prop('disabled', '');
                });

                // SORTABLE
                $('#modal-editRoute .jobs-list .items').sortable({
                    axis: 'y',
                    containment: 'parent',
                    handle: '.sortable-handle',
                    items: 'tr',
                    placeholder: 'ui-sortable-placeholder',
                    tolerance: 'pointer',
                    update: function() {
                        $('#modal-editRoute .btn.save').prop('disabled', '');
                        //saveRoute();
                    }
                });

                // ALERT CLOSE
                $('#modal-editRoute form .alert .close').click(function(e) {
                    $(this).closest('.alert').fadeOut(300);
                });

            });

        </script>

        <form>

        <input type="hidden" name="id" value="<?php echo $route['id']; ?>" />
        <input type="hidden" name="date" value="<?php echo $date; ?>" />

        <div class="alert alert-dismissible fade show" role="alert">
            <strong></strong>
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="info row">

            <div class="col-md-6">

                <div class="date"><?php echo date('n/j/Y', strtotime($date)); ?></div>

                <div class="customers">
                    <i class="fas fa-users fa-fw"></i> <?php echo number_format((int)count((array)$rcustomers), 0); ?> Customers
                </div>

                <div class="jobs">
                    <i class="fas fa-briefcase fa-fw"></i> <?php echo number_format($num_jobs, 0); ?> Jobs
                </div>

                <div class="depart">
                    Departs: <?php echo date('g:i a', strtotime($route['depart'])); ?>
                </div>

            </div>

            <div class="col-md-6">

                <div class="card staff">
                    <div class="card-body">
                        <h5 class="card-title">Staff</h5>
                        <div>
                            <?php foreach ($_uccms_staff->getStaffs()['staff'] as $staff) { ?>
                                <div class="form-check">
                                    <input class="form-check-input custom_control" type="checkbox" name="staff[]" value="<?php echo $staff['id']; ?>" id="routeStaff-<?php echo $staff['id']; ?>" <?php if ($rstaffs[$staff['id']]) { ?>checked="checked"<?php } ?> />
                                    <label class="form-check-label" for="routeStaff-<?php echo $staff['id']; ?>">
                                        <?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?>
                                    </label>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <?php if ($num_jobs > 0) ?>

        <div class="jobs-list">

            <table class="table sortable">
                <thead class="thead-light">
                    <tr>
                        <th class="title" scope="col">Job</th>
                        <th class="customer" scope="col">Customer</th>
                        <th class="address" scope="col">Address</th>
                        <th class="phone" scope="col">Phone</th>
                        <th class="options" scope="col">Options</th>
                    </tr>
                </thead>
                <tbody class="items">

                    <?php

                    // ROUTE JOBS
                    foreach ($rjobs['jobs'] as $job) {

                        // JOB CUSTOMER
                        $jcustomer = $jcustomers[$job['id']];

                        ?>

                        <tr class="job item" data-id="<?php echo $job['id']; ?>">
                            <td class="title">
                                <i title="Re-order" class="sortable-handle fas fa-sort"></i>
                                <span class="title"><?php echo stripslashes($job['title']); ?></span>
                            </td>
                            <td class="customer">
                                <?php echo $jcustomer['name']; ?>
                            </td>
                            <td class="address">
                                <?php echo uccms_prettyAddress([
                                    $jcustomer['address1'],
                                    $jcustomer['address2'],
                                    implode(' ', [$jcustomer['city'], $jcustomer['state'], $jcustomer['zip']])
                                ]); ?>
                            </td>
                            <td class="phone">
                                <?php echo $_uccms_staff->prettyPhone($jcustomer['phone']); ?>
                            </td>
                            <td class="options">
                                <a href="#" class="edit modalLink-editJob" title="Edit" data-jobid="<?php echo $job['id']; ?>" data-jobdata="<?php echo base64_encode(json_encode([])); ?>"><i class="fas fa-pencil-alt"></i></a>
                            </td>
                        </tr>

                        <?php

                    }

                    ?>

                </tbody>
            </table>

        </div>

        </form>

        <?php

    }

}

?>