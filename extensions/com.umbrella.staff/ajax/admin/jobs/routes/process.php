<?php

$out = [];

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    $route_id = (int)$_REQUEST['id'];
    $date = date('Y-m-d H:i:s', strtotime($_REQUEST['date']));

    // HAVE ROUTE ID
    if ($route_id) {

        $override = $_uccms_staff->routeOverrideSave($route_id, $date, [
            'options' => [
                'staff' => $_REQUEST['staff'],
                'jobSort' => $_REQUEST['sort'],
            ],
        ]);

        $out = $override;

    }

}

echo json_encode($out);

?>