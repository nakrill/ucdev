<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    $cols = [];

    $date = ($_REQUEST['date'] ? date('Y-m-d', strtotime($_REQUEST['date'])) : date('Y-m-d'));
    $view = ($_REQUEST['view'] == 'day' ? 'day' : 'week');

    // DAY VIEW
    if ($view == 'day') {

        // GET COLUMNS (HOURS)
        $cols = $_uccms_staff->dayHours($date);

        $prev_date = date('Y-m-d', strtotime('-1 Day', strtotime($cols[0])));
        $next_date = date('Y-m-d', strtotime('+1 Day', strtotime($cols[0])));

    // WEEK VIEW
    } else {

        // GET COLUMNS (DAYS)
        $cols = $_uccms_staff->weekDays($date);

        $prev_date = date('Y-m-d', strtotime('-1 Week', strtotime($cols[0])));
        $next_date = date('Y-m-d', strtotime('+1 Week', strtotime($cols[0])));

    }

    // STAFF
    $staffs = $_uccms_staff->getStaffs();

    // ROLES
    $roles = $_uccms_staff->getRoles();

    // JOBS
    $jobs = $_uccms_staff->getJobs([
        'start' => date('Y-m-d 00:00:00', strtotime($cols[0])),
        'end'   => date('Y-m-d 23:59:59', strtotime($cols[count($cols)-1])),
    ]);

    //print_r($jobs);
    //exit;

    // STAFF - TIME - JOB MATRIX
    $stj = [];
    foreach ((array)$jobs['jobs'] as $job) {

        $time = $job['start'];
        // break into hours / days (to span)

        if ($view == 'day') {
            $time = date('H', strtotime($job['start']));
        } else {
            $time = date('Y-m-d', strtotime($job['start']));
        }

        foreach ((array)$job['staff'] as $jstaff) {
            $stj[$jstaff['staff_id']][$time][$job['id']] = $job;
        }

    }

    //print_r($stj);
    //exit;

    ?>

    <style type="text/css">

        #schedule .colSchedule .heading h2 {
            float: left;
            margin: 0px;
            font-size: 1.4rem;
            line-height: 26px;
        }
        #schedule .colSchedule .heading .options {
            float: right;
        }
        #schedule .colSchedule .heading .option {
            margin-left: 10px;
        }
        #schedule .colSchedule .heading .options .btn {
            border-color: #d3d9df;
            font-size: 13px;
            line-height: 13px;
            text-transform: uppercase;
        }
        #schedule .colSchedule .heading .options .btn.disabled {
            background: transparent;
        }

        #schedule .colSchedule .interface {
            margin-top: 15px;
        }

        #schedule .colSchedule .interface th {
            font-weight: 600;
            color: #212529;
        }

        #schedule .colSchedule .interface thead th {
            text-transform: uppercase;
            color: #212529c7;
        }
        #schedule .colSchedule .interface thead th:not(:first-child) {
            text-align: center;
        }
        #schedule .colSchedule .interface thead th.current {
            background-color: #e8e5c4;
        }

        #schedule .colSchedule .interface tbody th {
            background-color: #eeeeee75;
            color: #212529;
        }
        #schedule .colSchedule .interface .table.week tbody th {
            width: 16%;
        }
        #schedule .colSchedule .interface tbody tr.open th {
            color: #212529c7;
        }
        #schedule .colSchedule .interface tbody th span {
            display: inline-block;
        }
        #schedule .colSchedule .interface tbody th .avatar {
            margin-right: 7px;
        }
        #schedule .colSchedule .interface tbody th .avatar img {
            width: 40px;
            height: 40px;
        }
        #schedule .colSchedule .interface tbody td {
            position: relative;
            padding: 0;
            background-color: #fff;
        }
        #schedule .colSchedule .interface .table.week tbody td {
            width: 12%;
        }
        #schedule .colSchedule .interface tbody td.current {
            background-color: #FFFDE7;
        }
        #schedule .colSchedule .interface tbody td .jobs {
            position: absolute;
            width: calc(100% - 10px);
            height: calc(100% - 10px);
            margin: 5px;
            padding-bottom: 10px;
            background-color: #57BDB0;
        }
        #schedule .colSchedule .interface tbody td .jobs .job {
            padding: 10px;
            font-size: 13px;
            line-height: 14px;
            font-weight: 500;
            color: #fff;
        }
        #schedule .colSchedule .interface tbody td .jobs .job .time {
            display: inline-block;
            margin-right: 5px;
        }
        #schedule .colSchedule .interface tbody td .jobs .job .role {
            display: inline-block;
            padding: 4px 5px 3px;
            background-color: #3F887F;
            border-radius: 3px;
            font-size: 13px;
            font-weight: normal;
            text-transform: uppercase;
        }

    </style>

    <script type="text/javascript">

        $(function() {

            // OPTION - TODAY
            $('#schedule .colSchedule .heading .option.today').click(function(e) {
                e.preventDefault();
                $('#schedule input[name="date"]').val($(this).data('date'));
                getSchedule();
            });

            // OPTION - NEXT / PREV
            $('#schedule .colSchedule .heading .option.prev-next .btn').click(function(e) {
                e.preventDefault();
                $('#schedule input[name="date"]').val($(this).data('date'));
                getSchedule();
            });

            // OPTION - VIEW
            $('#schedule .colSchedule .heading .option.view .btn').click(function(e) {
                e.preventDefault();
                $('#schedule input[name="view"]').val($(this).data('view'));
                getSchedule();
            });

        });

    </script>

    <div class="heading clearfix">

        <h2>
            <?php
            if ($view == 'day') {
                echo date('F d', strtotime($cols[0]));
            } else {
                echo date('F d', strtotime($cols[0])). ' - ' .date('F d', strtotime($cols[6]));
            }
            ?>
        </h2>

        <div class="options">

            <a href="#" class="option today btn btn-light" data-date="<?php echo date('Y-m-d'); ?>">Today</a>

            <div class="option prev-next btn-group" role="group">
                <button type="button" class="btn btn-light" title="Previous <?php echo $view; ?>" data-date="<?php echo $prev_date; ?>"><i class="fas fa-angle-left"></i></button>
                <button type="button" class="btn btn-light disabled"><i class="far fa-calendar-alt"></i></button>
                <button type="button" class="btn btn-light" title="Next <?php echo $view; ?>" data-date="<?php echo $next_date; ?>"><i class="fas fa-angle-right"></i></button>
            </div>

            <div class="option view btn-group" role="group">
                <button type="button" class="btn btn-light <?php echo ($view == 'day' ? 'active' : ''); ?>" data-view="day">Day</button>
                <button type="button" class="btn btn-light <?php echo ($view == 'week' ? 'active' : ''); ?>" data-view="week">Week</button>
            </div>

        </div>

    </div>

    <div class="interface">
        <div class="table-responsive">
            <table class="table table-light table-bordered <?php echo $view; ?>">
                <thead>
                    <tr>

                        <th scope="col">Staff</th>

                        <?php

                        foreach ($cols as $col) {

                            $current = false;
                            if ($view == 'day') {
                                if (date('Y-m-d H:i:s', strtotime($col)) == date('Y-m-d H:00:00')) {
                                    $current = true;
                                }
                            } else {
                                if (date('Y-m-d', strtotime($col)) == date('Y-m-d')) {
                                    $current = true;
                                }
                            }

                            ?>
                            <th scope="col" class="<?php echo ($current ? 'current' : ''); ?>">
                                <?php
                                if ($view == 'day') {
                                    echo date('g:i a', strtotime($col));
                                } else {
                                    echo date('D d', strtotime($col));
                                }
                                ?>
                            </th>
                            <?php

                        }

                        ?>

                    </tr>
                </thead>
                <tbody>

                    <? /*
                    <tr class="open">
                        <th scope="row">Open</th>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    */ ?>

                    <?php

                    /*
                    // ROLES
                    foreach ($roles as $role) {

                        ?>

                        <tr class="heading">
                            <td colspan="8">
                                <h3><?php echo stripslashes($role['title']); ?></h3>
                            </td>
                        </tr>

                        <?php

                    }
                    */

                    // LOOP THROUGH STAFF
                    foreach ((array)$staffs['staff'] as $staff) {

                        ?>

                        <tr class="staff">
                            <th scope="row">
                                <span class="avatar"><img src="<?php echo $staff['image']['avatar']; ?>" alt="" /></span>
                                <span class="name">
                                    <?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?>
                                </span>
                            </th>
                            <?php

                            // LOOP THROUGH DAYS / HOURS
                            foreach ($cols as $col) {

                                // CURRENT
                                $current = false;
                                if ($view == 'day') {
                                    if (date('Y-m-d H:i:s', strtotime($col)) == date('Y-m-d H:00:00')) {
                                        $current = true;
                                    }
                                } else {
                                    if (date('Y-m-d', strtotime($col)) == date('Y-m-d')) {
                                        $current = true;
                                    }
                                }

                                if ($view == 'day') {
                                    $sja = $stj[$staff['id']][date('H', strtotime($col))];
                                } else {
                                    $sja = $stj[$staff['id']][date('Y-m-d', strtotime($col))];
                                }
                                $num_jobs = count((array)$sja);

                                ?>
                                <td class="<?php echo ($current ? ' current' : ''); ?><?php echo ($num_jobs ? ' jobs' : ''); ?>">
                                    <?php if ($num_jobs > 0) { ?>
                                        <div class="jobs">
                                            <?php
                                            foreach ($sja as $job) {
                                                $role_id = $job['staff'][$staff['id']]['role_id'];
                                                ?>
                                                <div class="job">
                                                    <span class="time">
                                                        <?php echo substr(date('ga', strtotime($job['start'])), 0, -1); ?> - <?php echo substr(date('ga', strtotime($job['end'])), 0, -1); ?>
                                                    </span>
                                                    <?php if ($role_id) { ?>
                                                        <span class="role">
                                                            <?php echo stripslashes($roles[$role_id]['title']); ?>
                                                        </span>
                                                    <?php } ?>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    <?php } ?>
                                </td>
                                <?php

                            }

                            ?>
                        </tr>

                        <?php

                    }

                    ?>

                </tbody>
            </table>
        </div>
    </div>

    <?php

}

?>
