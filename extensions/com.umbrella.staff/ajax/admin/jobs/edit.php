<?php

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    $element = trim($_REQUEST['element']);

    $job = [];
    $top_info = [];

    // CLEAN
    $id = (int)$_REQUEST['id'];

    // ID SPECIFIED
    if ($id) {

        // GET JOB
        $job = $_uccms_staff->getJob($id);

        // JOB FOUND
        if ($job['id']) {

            // GET JOB STAFF
            $job['staff'] = $_uccms_staff->jobStaff($job['id']);

            // GET JOB CHANNELS
            $job['channels'] = $_uccms_staff->jobChannels($job['id']);

        }

    }

    // GET ROLES
    $roles = $_uccms_staff->getRoles();

    ?>

    <style type="text/css">

        #modal_editJob .modal-top {
            margin: 0 -15px 15px;
            padding: 0 15px 15px;
            border-bottom: 1px solid #e9ecef;
        }

        #modal_editJob form .alert {
            display: none;
        }

        #modal_editJob .staff .form-check {
            line-height: 26px;
        }
        #modal_editJob .staff select {
            display: inline-block;
            width: auto;
            height: auto;
            margin-left: 10px;
            padding: 0 5px;
            vertical-align: middle;
            line-height: unset;
            font-size: 14px;
        }

    </style>

    <script type="text/javascript">

        $(function() {

            // TIME PICKER
            $('#modal_editJob .date_time_picker').datetimepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });

            // ALERT CLOSE
            $('#modal_editJob form .alert .close').click(function(e) {
                $(this).closest('.alert').fadeOut(300);
            });

        });

    </script>

    <form>

    <input type="hidden" name="job[id]" value="<?php echo $job['id']; ?>" />

    <?php foreach ((array)$_REQUEST['channels'] as $ci => $channel) { ?>
        <input type="hidden" name="job[channels][<?php echo $ci; ?>][channel]" value="<?php echo $channel['channel']; ?>" />
        <input type="hidden" name="job[channels][<?php echo $ci; ?>][what]" value="<?php echo $channel['what']; ?>" />
        <input type="hidden" name="job[channels][<?php echo $ci; ?>][item_id]" value="<?php echo $channel['item_id']; ?>" />
    <?php } ?>

    <?php

    // LOOP THROUGH CHANNELS
    foreach ((array)$job['channels'] as $channel) {

        // ECOMMERCE
        if ($channel['channel'] == 'com.umbrella.ecommerce') {

            // HAS ECOMMERCE EXTENSION
            if (class_exists('uccms_Ecommerce')) {

                if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce();

                // HAS ACCESS
                if ($_uccms_ecomm->adminModulePermission()) {

                    // ORDER
                    if ($channel['what'] == 'order') {

                        // GET ORDER INFO
                        $order = $_uccms_ecomm->orderInfo($channel['item_id']);

                        $customer = [];

                        $name = trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname']));
                        if ($name) {
                            $customer['name'] = $name;
                        }

                        $ap = [];
                        if ($order['billing_address1']) $ap[] = stripslashes($order['billing_address1']);
                        if ($order['billing_address2']) $ap[] = stripslashes($order['billing_address2']);
                        $csz = [];
                        if ($order['billing_city']) $csz[] = stripslashes($order['billing_city']);
                        if ($order['billing_state']) $csz[] = stripslashes($order['billing_state']);
                        if ($order['billing_zip']) $csz[] = stripslashes($order['billing_zip']);
                        if (count($csz) > 0) $ap[] = implode(' ', $csz);
                        if (count($ap) > 0) {
                            $customer['address'] = implode('<br />', $ap);
                        }

                        if ($order['billing_phone']) {
                            $customer['phone'] = $_uccms_ecomm->prettyPhone($order['billing_phone']);
                        }

                        ?>

                        <style type="text/css">

                            #modal_editJob .customer {
                                font-size: .8em;
                            }
                            #modal_editJob .map .mapContainer iframe {
                                display: block;
                                width: 100%;
                                height: 100px;
                            }

                        </style>

                        <div class="modal-top">
                            <div class="row">

                                <div class="customer col-md-6">
                                    <?php echo implode('<br />', $customer); ?>
                                </div>

                                <div class="map col-md-6">
                                    <div class="mapContainer">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d51603.23636467399!2d-75.74274325569507!3d36.0641701449723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89a4e02e4f1acf99%3A0xbe6f5aea12b3aff8!2sKitty+Hawk%2C+NC+27949!5e0!3m2!1sen!2sus!4v1538074216697" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php

                    }

                }

            }

        }

    }

    ?>

    <div class="alert alert-dismissible fade show" role="alert">
        <strong></strong>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="row">

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>Title</label>
                <input type="text" name="job[title]" value="<?php echo stripslashes($job['title']); ?>" class="form-control" />
            </fieldset>

        </div>

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>Status</label>
                <select name="job[status]" class="custom_control form-control">
                    <?php
                    foreach ($_uccms_staff->jobStatuses() as $status_id => $status) {
                        if ($status_id != 9) {
                            ?>
                            <option value="<?php echo $status_id; ?>" <?php if ($status_id == $job['status']) { ?>selected="selected"<?php } ?>><?php echo $status['title']; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </fieldset>

        </div>

    </div>

    <div class="row">

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>Start time</label>
                <?php
                $field = array(
                    'key'       => 'job[start]', // The value you should use for the "name" attribute of your form field
                    'value'     => ($job['start'] ? $job['start'] : ''), // The existing value for this form field
                    'id'        => 'jobStart', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => []
                );
                include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                ?>
            </fieldset>

        </div>

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>End time</label>
                <?php
                $field = array(
                    'key'       => 'job[end]', // The value you should use for the "name" attribute of your form field
                    'value'     => ($job['end'] ? $job['end'] : ''), // The existing value for this form field
                    'id'        => 'jobEnd', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => []
                );
                include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                ?>
            </fieldset>

        </div>

    </div>

    <fieldset class="form-group">
        <label>Route</label>
        <select name="job[route_id]" class="custom_control form-control">
            <option value="0">None</option>
            <?php foreach ($_uccms_staff->getRoutes() as $route) { ?>
                <option value="<?php echo $route['id']; ?>" <?php if ($route['id'] == $job['route_id']) { ?>selected="selected"<?php } ?>><?php echo $route['title']; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <div class="card staff">
        <div class="card-body">
            <h5 class="card-title">Staff</h5>
            <div>
                <?php foreach ($_uccms_staff->getStaffs()['staff'] as $staff) { ?>
                    <div class="form-check">
                        <input class="form-check-input custom_control" type="checkbox" name="job[staff][]" value="<?php echo $staff['id']; ?>" id="jobStaff-<?php echo $staff['id']; ?>" <?php if ($job['staff'][$staff['id']]) { ?>checked="checked"<?php } ?> />
                        <label class="form-check-label" for="jobStaff-<?php echo $staff['id']; ?>">
                            <?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?>
                            <?php if (count((array)$staff['roles']) > 0) { ?>
                                <select name="job[staff_roles][<?php echo $staff['id']; ?>]" class="custom_control form-control">
                                    <?php foreach ($staff['roles'] as $role) { ?>
                                        <option value="<?php echo $role['role_id']; ?>" <?php if ($role['role_id'] == $job['staff'][$staff['id']]['role_id']) { ?>selected="selected" <?php } ?>><?php echo stripslashes($roles[$role['role_id']]['title']); ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    </form>

    <?php

}

?>