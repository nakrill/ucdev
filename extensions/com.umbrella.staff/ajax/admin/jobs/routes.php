<?php

// INCLUDE COMPOSER
include_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

use When\When;

// MODULE CLASS
if (!$_uccms_staff) $_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    $cols = [];

    $date = ($_REQUEST['date'] ? date('Y-m-d', strtotime($_REQUEST['date'])) : date('Y-m-d'));
    $view = ($_REQUEST['view'] == 'day' ? 'day' : 'week');

    // DAY VIEW
    if ($view == 'day') {

        // GET COLUMNS (HOURS)
        $cols = $_uccms_staff->dayHours($date);

        $prev_date = date('Y-m-d', strtotime('-1 Day', strtotime($cols[0])));
        $next_date = date('Y-m-d', strtotime('+1 Day', strtotime($cols[0])));

    // WEEK VIEW
    } else {

        // GET COLUMNS (DAYS)
        $cols = $_uccms_staff->weekDays($date);

        $prev_date = date('Y-m-d', strtotime('-1 Week', strtotime($cols[0])));
        $next_date = date('Y-m-d', strtotime('+1 Week', strtotime($cols[0])));

    }

    // STAFF
    $staffs = $_uccms_staff->getStaffs();

    // ROUTES
    $routes = $_uccms_staff->getRoutes();

    // ROUTES BY TIMEFRAME
    $troutes = $_uccms_staff->getRoutesByTimeframe([
        'start' => date('Y-m-d 00:00:00', strtotime($cols[0])),
        'end'   => date('Y-m-d 23:59:59', strtotime($cols[count($cols)-1])),
    ]);

    //print_r($troutes);
    //exit;

    // ROUTE MAP JS DATA
    $rmjsd = [];

    // ROUTE - TIME - ACTIVE
    $rta = [];

    // ROUTES IN TIMEFRAME
    foreach ((array)$troutes as $troute) {

        //print_r($troute);

        if ($view == 'day') {
            $date = date('Y-m-d H:00:00', strtotime($troute['date']));
            $time = date('H', strtotime($troute['date']));
        } else {
            $date = date('Y-m-d 00:00:00', strtotime($troute['date']));
            $time = date('Y-m-d', strtotime($troute['date']));
        }

        // GET ROUTE STAFF
        $trstaffs = $_uccms_staff->routeStaffs($troute['id'], $date);
        if (count((array)$trstaffs) > 0) {
            $troute['staff'] = $trstaffs;
            $troute['staff_keys'] = implode('.', array_keys($trstaffs));
        }

        // GET ROUTE JOBS
        $rjobs = $_uccms_staff->routeJobs($troute['id'], $date);

        //print_r($rjobs);

        // GET ROUTE CUSTOMERS
        $rcustomers = $_uccms_staff->routeCustomers($troute['id'], [
            'jobs' => $rjobs['jobs']
        ]);

        /*
        // HAVE E-COMMERCE
        if (class_exists('uccms_Ecommerce')) {

            if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce();

            // CUSTOMER CONTACTS IN ROUTE
            $rcc_sql = "SELECT * FROM `" .$_uccms_ecomm->tables['customer_contacts']. "` WHERE (`route_id`=" .$troute['id']. ") AND (`active`=1) GROUP BY `customer_id`";
            $rcc_q = sqlquery($rcc_sql);
            while ($rcc = sqlfetch($rcc_q)) {
                $rcustomers[$rcc['customer_id']][$rcc['id']] = $rcc['id'];
            }

            // JOBS IN ROUTE
            foreach ((array)$rjobs['jobs'] as $rjob) {
                foreach ((array)$rjob['channels'] as $channel) {

                    if (($channel['channel'] = 'com.umbrella.ecommerce') && ($channel['what'] == 'order') && ($channel['item_id'])) {

                        $order = $_uccms_ecomm->orderInfo((int)$channel['item_id']);

                        $customer = $_uccms_ecomm->getCustomer($order['customer_id']);

                        $rccs = $_uccms_ecomm->customerContacts($customer['id'], [
                            'route_id' => $troute['id']
                        ]);
                        if (count((array)$rccs['contacts']) > 0) {
                            foreach ($rccs['contacts'] as $rcc) {
                                $rcustomers[$rcc['customer_id']][$rcc['id']] = $rcc['id'];
                            }
                        } else {
                            $ccid = 0;
                            if ($customer['contacts']['default']['delivery']) {
                                $ccid = $customer['contacts']['default']['delivery'];
                            } else if ($customer['contacts']['default']['shipping']) {
                                $ccid = $customer['contacts']['default']['shipping'];
                            } else if ($customer['contacts']['default']['billing']) {
                                $ccid = $customer['contacts']['default']['billing'];
                            }
                            if ($ccid) {
                                $rcc = $_uccms_ecomm->customerContact($customer['id'], $ccid);
                                if ($rcc['id']) {
                                    $rcustomers[$rcc['customer_id']][$rcc['id']] = $rcc['id'];
                                }
                            }
                        }

                    }

                }
            }

        }
        */

        // CUSTOMERS BY JOB
        $jcustomers = [];

        // HAVE E-COMMERCE
        if (class_exists('uccms_Ecommerce')) {

            $_uccms_ecomm = new uccms_Ecommerce();

            // CONVERT ROUTE CUSTOMERS TO JOB CUSTOMERS
            foreach ((array)$rcustomers as $customer_id => $contacts) {
                foreach ($contacts as $contact) {
                    foreach ((array)$contact['jobs'] as $job_id) {
                        $jcustomers[$job_id] = $_uccms_ecomm->customerContact($customer_id, $contact['contact_id']);
                    }
                }
            }

        }

        // ROUTE TIME ARRAY
        $rta[$troute['id']][$time] = [
            'staff' => $troute['staff'],
            'staff_keys' => $troute['staff_keys'],
            'jobs' => $rjobs['jobs'],
            'jobs_order' => array_keys((array)$jcustomers),
            'customers' => $rcustomers,
            'jcustomers' => $jcustomers,
        ];

        // JS DATA
        $rmjsd['routes'] = $rta;

        /*
        foreach ((array)$rjobs['jobs'] as $rjob) {
            $rmjsd['jobs'][$rjob['id']] = $rjob;
        }
        */

    }

    //print_r($rta);
    //exit;

    ?>

    <style type="text/css">

        #routes .colRoutes .heading h2 {
            float: left;
            margin: 0px;
            font-size: 1.4rem;
            line-height: 26px;
        }
        #routes .colRoutes .heading .options {
            float: right;
        }
        #routes .colRoutes .heading .option {
            margin-left: 10px;
        }
        #routes .colRoutes .heading .options .btn {
            border-color: #d3d9df;
            font-size: 13px;
            line-height: 13px;
            text-transform: uppercase;
        }
        #routes .colRoutes .heading .options .btn.disabled {
            background: transparent;
        }

        #routes .colRoutes .interface {
            margin-top: 15px;
        }

        #routes .colRoutes .interface th {
            font-weight: 600;
            color: #212529;
        }

        #routes .colRoutes .interface thead th {
            text-transform: uppercase;
            color: #212529c7;
        }
        #routes .colRoutes .interface thead th:not(:first-child) {
            text-align: center;
        }
        #routes .colRoutes .interface thead th.current {
            background-color: #e8e5c4;
        }

        #routes .colRoutes .interface tbody th {
            padding: 10px;
            background-color: #eeeeee75;
            line-height: 1em;
            color: #212529;
        }
        #routes .colRoutes .interface .table.week tbody th {
            /*width: 16%;*/
        }
        #routes .colRoutes .interface tbody tr.open th {
            color: #212529c7;
        }
        #routes .colRoutes .interface tbody th span {
            display: inline-block;
        }
        #routes .colRoutes .interface tbody th .staff {
            margin-top: 3px;
        }
        #routes .colRoutes .interface tbody th .staff a {
            display: inline-block;
            margin-right: 5px;
        }
        #routes .colRoutes .interface tbody th .staff img {
            width: 25px;
            height: 25px;
        }
        #routes .colRoutes .interface tbody td {
            position: relative;
            padding: 0;
            background-color: #fff;
        }
        #routes .colRoutes .interface .table.week tbody td {
            width: 12%;
        }
        #routes .colRoutes .interface tbody td.current {
            background-color: #FFFDE7;
        }
        #routes .colRoutes .interface tbody td.active {
            background-color: #57bdb042;
        }
        #routes .colRoutes .interface tbody td.active.jobs {
            background-color: #57BDB0;
        }

        #routes .colRoutes .interface tbody td .edit {
            display: none;
            position: absolute;
            top: 8px;
            right: 10px;
            font-size: 1.2em;
            z-index: 5;
        }
        #routes .colRoutes .interface tbody td:hover .edit {
            display: block;
        }
        #routes .colRoutes .interface tbody td.jobs .edit {
            color: rgba(0, 0, 0, .7);
        }

        #routes .colRoutes .interface tbody td .info {
            position: absolute;
            width: calc(100% - 10px);
            height: calc(100% - 10px);
            margin: 5px;
            padding: 4px 7px 0;
            background-color: rgba(255, 255, 255, .5);
            border-radius: 3px;
            font-size: 13px;
            font-weight: normal;
            text-transform: uppercase;
            transition: .3s;
        }
        #routes .colRoutes .interface tbody td.jobs .info {
            cursor: pointer;
        }
        #routes .colRoutes .interface tbody td .info:hover {
            background-color: rgba(255, 255, 255, .7);
        }
        #routes .colRoutes .interface tbody td .info .item {
            display: inline-block;
            margin: 0 7px 7px 0;
            font-size: 14px;
            opacity: .8;
        }
        #routes .colRoutes .interface tbody td .info .item i {
            margin-right: 3px;
        }
        #routes .colRoutes .interface tbody td .info .item.customers {
            opacity: .4;
        }
        #routes .colRoutes .interface tbody td .info .depart {
            opacity: .8;
        }
        #routes .colRoutes .interface tbody td .staff {
            position: absolute;
            right: 5px;
            bottom: 9px;
            opacity: .8;
        }
        #routes .colRoutes .interface tbody td .staff a {
            display: inline-block;
            margin-right: 5px;
        }
        #routes .colRoutes .interface tbody td .staff img {
            width: 20px;
            height: 20px;
        }

        /*
        #routes .colRoutes .interface tbody td .routes {
            position: absolute;
            width: calc(100% - 10px);
            height: calc(100% - 10px);
            margin: 5px;
            padding-bottom: 10px;
        }
        #routes .colRoutes .interface tbody td .routes .route {
            padding: 10px;
            font-size: 13px;
            line-height: 14px;
            font-weight: 500;
            color: #fff;
        }
        #routes .colRoutes .interface tbody td .routes .route .time {
            display: inline-block;
            margin-right: 5px;
        }
        #routes .colRoutes .interface tbody td .routes .route .role {
            display: inline-block;
            padding: 4px 5px 3px;
            background-color: #3F887F;
            border-radius: 3px;
            font-size: 13px;
            font-weight: normal;
            text-transform: uppercase;
        }
        */

    </style>

    <script type="text/javascript">

        routeMapData = <?php echo json_encode($rmjsd); ?>;

        $(function() {

            $('#routes .colRoutes .interface [data-toggle="tooltip"]').tooltip();

            // OPTION - TODAY
            $('#routes .colRoutes .heading .option.today').click(function(e) {
                e.preventDefault();
                $('#routes input[name="date"]').val($(this).data('date'));
                getRoutes();
            });

            // OPTION - NEXT / PREV
            $('#routes .colRoutes .heading .option.prev-next .btn').click(function(e) {
                e.preventDefault();
                $('#routes input[name="date"]').val($(this).data('date'));
                getRoutes();
            });

            // OPTION - VIEW
            $('#routes .colRoutes .heading .option.view .btn').click(function(e) {
                e.preventDefault();
                $('#routes input[name="view"]').val($(this).data('view'));
                getRoutes();
            });

            //getRoleList();

            // ROUTE EDIT CLICK
            $('#routes .colRoutes .interface tbody td .edit').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                var $parent = $(this).closest('td');
                editRoute({
                    id: $parent.data('routeid'),
                    date: $parent.data('date'),
                });
            });

            // ROUTE INFO CLICK
            $('#routes .colRoutes .interface tbody td.jobs .info').click(function(e) {
                e.preventDefault();
                var $parent = $(this).closest('td');
                populateRouteMap({
                    route_id: $parent.data('routeid'),
                    date: $parent.data('date'),
                });
            });

            // SAVE
            $('#modal-editRoute .btn.save').on('click', function(e) {
                e.preventDefault();
                saveRoute();
            });

        });

        // POPULATE MAP
        function populateRouteMap(params, options, callback) {

            if (params.route_id && params.date) {

                params.date = params.date.replace(' 00:00:00', '');

                route = routeMapData.routes[params.route_id][params.date];

                console.log(route);

                if (typeof route.jobs == 'object') {

                    nj = Object.keys(route.jobs).length;

                    var mapWays = [];

                    $.each(route.jobs_order, function(ji, job_id) {

                        job = route.jobs[job_id];

                        jcustomer = route.jcustomers[job.id];

                        //console.log(jcustomer);

                        if (ji == 1) {
                            jcustomer.address_full = '12007 Trianon Ln, Austin, TX 78727';
                        } else if (ji == 2) {
                            jcustomer.address_full = '7306 Grass Cove, Austin, TX 78759';
                        }

                        $('#routes .colRight .mapContainer').addMarker({
                            address: jcustomer.address_full,
                            title: ' ',
                            text:  job.title,
                            /*
                            markerContentClick: function() {
                                alert('Open edit job lightwindow.');
                            }
                            */
                        });

                        mapWays.push(jcustomer);

                    });

                    if (nj > 1) {

                        var start = mapWays.shift();
                        var end = mapWays.pop();

                        var steps = [];
                        $.each(mapWays, function (i, v) {
                            steps.push(v.address_full);
                        });

                        $('#routes .colRight .mapContainer').addWay({
                            start: start.address_full,
                            end:  end.address_full,
                            route : 'way',
                            langage : 'english',
                            step: steps
                        });

                    }

                }

            }

        }

        // EDIT ROUTE
        function editRoute(params, options, callback) {
            $('#modal-editRoute').modal('show', {
                focus: false
            });
            $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/jobs/routes/edit/', params, function(data) {
                $('#modal-editRoute .modal-body .data').html(data);
                if (typeof callback == 'function') {
                    callback(data);
                }
            }, 'html');
        }

        // SAVE ROUTE
        function saveRoute(params, options, callback) {
            $('#modal-editRoute form .alert').hide();
            if (typeof params == 'undefined') {
                params = $('#modal-editRoute form').serialize();
            }
            var sort = $('#modal-editRoute .jobs-list .items').sortable('serialize', {
                attribute: 'data-id',
                key: 'sort[]',
                expression: /(.+)/
            });
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/jobs/routes/process/', params+ '&' +sort, function(data) {
                if (data.success) {
                    $('#modal-editRoute form .alert strong').html('Route saved!');
                    $('#modal-editRoute form .alert').removeClass('alert-danger').addClass('alert-success').show();
                    if (typeof callback == 'function') {
                        callback(data);
                    }
                    getRoutes();
                } else {
                    $('#modal-editRoute form .alert strong').html((data.error ? data.error : 'Failed to edit route.'));
                    $('#modal-editRoute form .alert').removeClass('alert-success').addClass('alert-danger').show();
                }
            }, 'json');
        }

        // ON JOB SAVE
        function modalEditJob_saveCallback(data) {
            editRoute({
                id: $('#modal-editRoute input[name="id"]').val(),
                date: $('#modal-editRoute input[name="date"]').val(),
            });
            getRoutes();
        }

    </script>


    <div class="heading clearfix">

        <h2>
            <?php
            if ($view == 'day') {
                echo date('F d', strtotime($cols[0]));
            } else {
                echo date('F d', strtotime($cols[0])). ' - ' .date('F d', strtotime($cols[6]));
            }
            ?>
        </h2>

        <div class="options">

            <a href="#" class="option today btn btn-light" data-date="<?php echo date('Y-m-d'); ?>">Today</a>

            <div class="option prev-next btn-group" role="group">
                <button type="button" class="btn btn-light" title="Previous <?php echo $view; ?>" data-date="<?php echo $prev_date; ?>"><i class="fas fa-angle-left"></i></button>
                <button type="button" class="btn btn-light disabled"><i class="far fa-calendar-alt"></i></button>
                <button type="button" class="btn btn-light" title="Next <?php echo $view; ?>" data-date="<?php echo $next_date; ?>"><i class="fas fa-angle-right"></i></button>
            </div>

            <? /*
            <div class="option view btn-group" role="group">
                <button type="button" class="btn btn-light <?php echo ($view == 'day' ? 'active' : ''); ?>" data-view="day">Day</button>
                <button type="button" class="btn btn-light <?php echo ($view == 'week' ? 'active' : ''); ?>" data-view="week">Week</button>
            </div>
            */ ?>

        </div>

    </div>

    <div class="interface">
        <div class="table-responsive">
            <table class="table table-light table-bordered <?php echo $view; ?>">
                <thead>
                    <tr>

                        <th scope="col">Routes</th>

                        <?php

                        foreach ($cols as $col) {

                            $current = false;
                            if ($view == 'day') {
                                if (date('Y-m-d H:i:s', strtotime($col)) == date('Y-m-d H:00:00')) {
                                    $current = true;
                                }
                            } else {
                                if (date('Y-m-d', strtotime($col)) == date('Y-m-d')) {
                                    $current = true;
                                }
                            }

                            ?>
                            <th scope="col" class="<?php echo ($current ? 'current' : ''); ?>">
                                <?php
                                if ($view == 'day') {
                                    echo date('g:i a', strtotime($col));
                                } else {
                                    echo date('D d', strtotime($col));
                                }
                                ?>
                            </th>
                            <?php

                        }

                        ?>

                    </tr>
                </thead>
                <tbody>

                    <? /*
                    <tr class="open">
                        <th scope="row">Open</th>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    */ ?>

                    <?php

                    /*
                    // ROLES
                    foreach ($roles as $role) {

                        ?>

                        <tr class="heading">
                            <td colspan="8">
                                <h3><?php echo stripslashes($role['title']); ?></h3>
                            </td>
                        </tr>

                        <?php

                    }
                    */

                    // LOOP THROUGH ROUTES
                    foreach ((array)$routes as $route) {

                        // ROUTE STAFF
                        $rstaffs = $_uccms_staff->routeStaffs($route['id']);

                        $rstaff_keys = implode('.', array_keys($rstaffs));

                        ?>

                        <tr class="route">
                            <th scope="row">
                                <span class="name">
                                    <?php echo trim(stripslashes($route['title'])); ?>
                                </span>
                                <?php if (count((array)$rstaffs) > 0) { ?>
                                    <div class="staff">
                                        <?php
                                        foreach ($rstaffs as $rstaff) {
                                            $rstaff = $staffs['staff'][$rstaff['staff_id']];
                                            if ($rstaff['id']) {
                                                ?>
                                                <a href="#"><img src="<?php echo $rstaff['image']['avatar']; ?>" alt="<?php echo $rstaff['name']; ?>" title="<?php echo $rstaff['name']; ?>" /></a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                            </th>
                            <?php

                            // LOOP THROUGH DAYS / HOURS
                            foreach ($cols as $col) {

                                // CURRENT
                                $current = false;
                                if ($view == 'day') {
                                    if (date('Y-m-d H:i:s', strtotime($col)) == date('Y-m-d H:00:00')) {
                                        $current = true;
                                    }
                                } else {
                                    if (date('Y-m-d', strtotime($col)) == date('Y-m-d')) {
                                        $current = true;
                                    }
                                }

                                if ($view == 'day') {
                                    $rroute = $rta[$route['id']][date('H', strtotime($col))];
                                } else {
                                    $rroute = $rta[$route['id']][date('Y-m-d', strtotime($col))];
                                }

                                //echo $rroute['staff_keys'];

                                $route_active = is_array($rroute);

                                $num_jobs = count((array)$rroute['jobs']);

                                $num_customers = count((array)$rroute['customers']);

                                ?>
                                <td data-routeid="<?php echo $route['id']; ?>" data-date="<?php echo date('Y-m-d H:i:s', strtotime($col)); ?>" class="<?php echo ($current ? ' current' : ''); ?><?php echo ($route_active ? ' active' : ''); ?><?php echo ($num_jobs ? ' jobs' : ''); ?>">

                                    <a href="#" class="edit" data-toggle="tooltip" data-placement="top" title="Edit route"><i class="fas fa-edit"></i></a>

                                    <?php if ($route_active) { ?>

                                        <div class="info">

                                            <div class="item customers">
                                                <span data-toggle="tooltip" data-placement="top" title="<?php echo number_format($num_customers, 0); ?> Customers">
                                                    <span class="num"><i class="fas fa-users"></i><?php echo $num_customers; ?></span>
                                                </span>
                                            </div>

                                            <?php if ($num_jobs) { ?>

                                                <div class="item jobs">
                                                    <span data-toggle="tooltip" data-placement="top" title="<?php echo number_format($num_jobs, 0); ?> Jobs">
                                                        <span class="num"><i class="fas fa-briefcase"></i><?php echo $num_jobs; ?></span>
                                                    </span>
                                                </div>

                                                <div class="depart">
                                                    Departs: <?php echo date('g:i a', strtotime($route['depart'])); ?>
                                                </div>

                                            <?php } ?>

                                        </div>

                                        <?php if ($rroute['staff_keys'] != $rstaff_keys) { ?>

                                            <div class="staff">
                                                <?php

                                                foreach ($rroute['staff'] as $rstaff) {
                                                    $rstaff = $staffs['staff'][$rstaff['staff_id']];
                                                    if ($rstaff['id']) {
                                                        ?>
                                                        <a href="#"><img src="<?php echo $rstaff['image']['avatar']; ?>" alt="<?php echo $rstaff['name']; ?>" title="<?php echo $rstaff['name']; ?>" /></a>
                                                        <?php
                                                    }
                                                }

                                                 ?>
                                            </div>

                                        <?php } ?>

                                    <?php } ?>

                                </td>
                                <?php

                            }

                            ?>
                        </tr>

                        <?php

                    }

                    ?>

                </tbody>
            </table>
        </div>
    </div>

    <?php

}

?>
