<?php
    class uccms_Staff extends BigTreeModule {

        var $Table = "";

        var $Extension = "com.umbrella.staff";

        // DB TABLES
        var $tables = array(
            'jobs'                  => 'uccms_staff_jobs',
            'jobs_channels'         => 'uccms_staff_jobs_channels',
            'photos'                => 'uccms_staff_photos',
            'roles'                 => 'uccms_staff_roles',
            'routes'                => 'uccms_staff_routes',
            'routes_overrides'      => 'uccms_staff_routes_overrides',
            'shifts'                => 'uccms_staff_shifts',
            'staff'                 => 'uccms_staff_staff',
            'staff_jobs'            => 'uccms_staff_staff_jobs_rel',
            'staff_roles'           => 'uccms_staff_staff_roles_rel',
            'staff_routes'          => 'uccms_staff_staff_routes_rel',
            'staff_shifts'          => 'uccms_staff_staff_shifts_rel',
            'settings'              => 'uccms_staff_settings',
            'time_tracking'         => 'uccms_staff_time_tracking',
        );

        var $settings, $misc;

        // START AS 0, WILL ADJUST LATER
        static $PerPage = 0;


        function __construct() {

        }


        #############################
        # GENERAL - MISC
        #############################


        // FRONTEND PATH
        public function frontendPath() {
            if (!isset($this->settings['frontend_path'])) {
                $this->settings['frontend_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*staff')"))['path']);
            }
            if (!$this->settings['frontend_path']) $this->settings['frontend_path'] = 'staff';
            return $this->settings['frontend_path'];
        }


        // MODULE ID
        public function moduleID() {
            if (!isset($this->settings['module_id'])) {
                $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
            }
            return $this->settings['module_id'];
        }


        // ADMIN USER ID
        public function adminID() {
            return $_SESSION['bigtree_admin']['id'];
        }


        // ADMIN ACCESS LEVEL
        public function adminModulePermission() {
            global $admin;
            if (!isset($this->settings['admin_module_permission'])) {
                $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
            }
            return $this->settings['admin_module_permission'];
        }


        // STRING IS JSON
        public function isJson($string) {
            if (substr($string, 0, 2) == '{"') {
                return true;
            }
        }


        // CONVERTS A STRING INTO A REWRITE-ABLE URL
        public function makeRewrite($string) {

            $string = strtolower(trim($string));

            // REMOVE ALL QUOTES
            $string = str_replace("'", '', $string);
            $string = str_replace('"', '', $string);

            // STRIP ALL NON WORD CHARS
            $string = preg_replace('/\W/', ' ', $string);

            // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
            $string = preg_replace('/\ +/', '-', $string);

            // TRIM DASHES
            $string = preg_replace('/\-$/', '', $string);
            $string = preg_replace('/^\-/', '', $string);

            return $string;

        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
        public function imageBridgeIn($string) {
            $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $dpa = explode('/', $string);
                return array_pop($dpa);
                //$what = array_pop($dpa);
            }
        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
        public function imageBridgeOut($string, $what, $file=false) {
            $string = stripslashes($string);
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $base = ($file ? SITE_ROOT : STATIC_ROOT);
                return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
            }
        }


        // CONVERT BIGTREE FILE LOCATION TO FULL URL
        public function bigtreeFileURL($file) {
            return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
        }


        // SET NUMBER PER PAGE
        public function setPerPage($num) {
            static::$PerPage = (int)$num;
        }


        // NUMBER PER PAGE
        public function perPage() {
            global $cms;
            if (!static::$PerPage) {
                $pp = $this->getSetting('ads_per_page');
                if ($pp) {
                    $this->setPerPage($pp);
                }
            }
            if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
            if (!static::$PerPage) $this->setPerPage(15);
            return static::$PerPage;
        }


        // GET NUMBER OF PAGES FROM QUERY
        public function pageCount($num, $perpage=null) {
            if ($perpage === null) $perpage = $this->perPage();
            return ceil($num / $perpage);
        }


        // TRIM URL
        public function trimURL($string) {
            return trim(preg_replace('#^https?://#', '', $string));
        }


        // CONVERT ARRAY TO CSV
        public function arrayToCSV($array=array()) {
            if (is_array($array)) {
                return implode(',', $array);
            }
        }


        /*
        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            );
        }
        */

        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                7 => array(
                    'code'  => 'SU',
                    'title' => 'Sunday'
                ),
                1 => array(
                    'code'  => 'MO',
                    'title' => 'Monday'
                ),
                2 => array(
                    'code'  => 'TU',
                    'title' => 'Tuesday'
                ),
                3 => array(
                    'code'  => 'WE',
                    'title' => 'Wednesday'
                ),
                4 => array(
                    'code'  => 'TH',
                    'title' => 'Thursday'
                ),
                5 => array(
                    'code'  => 'FR',
                    'title' => 'Friday'
                ),
                6 => array(
                    'code'  => 'SA',
                    'title' => 'Saturday'
                )
            );
        }


        // GET RANGE BETWEEN TWO DATES
        public function date_range($from, $to, $step = '+1 day', $output_format = 'Y-m-d') {
            $dates = array();
            $current = strtotime($from);
            $to = strtotime($to);
            while( $current <= $to ) {
                $dates[] = date($output_format, $current);
                $current = strtotime($step, $current);
            }
            return $dates;
        }


        // CALCULATE THE DIFFERENT BETWEEN TO DATE/TIMES
        public function dtDiff($from, $to='', $options=array()) {

            if ((!$to) || ($to == '0000-00-00 00:00:00')) {
                $to = date('Y-m-d H:i:s');
            }

            $d1 = new DateTime($from);
            $d2 = new DateTime($to);

            $diff = $d2->diff($d1);

            if ($options['format']) {

                return $diff->format($options['format']);

            } else {

                $time = array(
                    'y' => $diff->y,
                    'm' => $diff->m,
                    'd' => $diff->d,
                    'h' => $diff->h,
                    'i' => $diff->i,
                    's' => $diff->s
                );

                return $time;

            }

        }


        // FORMAT SECONDS (TO HH:MM, AND POSSIBLE SS.MS)
        function formatSeconds($seconds, $options=array()) {
            $hours = 0;
            $milliseconds = str_replace("0.", '', $seconds - floor($seconds));
            if ($seconds > 3600 ) {
                $hours = floor($seconds / 3600);
            }
            $seconds = $seconds % 3600;
            return str_pad($hours, 2, '0', STR_PAD_LEFT)
                .gmdate( ':i', $seconds)
                //.gmdate( ':i:s', $seconds)
                //.($milliseconds ? ".$milliseconds" : '')
            ;
        }


        // STATUSES
        public function statuses($what='staff') {
            if ($what == 'jobs') {
                return $this->jobStatuses();
            } else {
                return $this->staffStatuses();
            }
        }


        // SPLIT A NAME
        public function nameSplit($name) {
            $name = trim($name);
            $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
            return array($first_name, $last_name);
        }


        // COMBINE A NAME
        public function combineName($array=array()) {
            return trim(stripslashes($array['firstname']. ' ' .$array['lastname']));
        }


        // CREATE A DB SET
        public function createSet($array=[]) {
            return uccms_createSet($array);
        }


        // ARRAY OF DAYS IN WEEK
        public function weekDays($date='') {
            $out = [];
            if (!$date) $date = date('Y-m-d');
            $week = date('W', strtotime($date));
            $year = date('Y', strtotime($date));
            $first = strtotime($year.'W'.$week);
            $out[0] = date('Y-m-d', $first);
            for ($i=1; $i<=6; $i++) {
                $out[$i] = date('Y-m-d', strtotime('+' .$i. ' Day', $first));
            }
            return $out;
        }

        // ARRAY OF HOURS IN DAY
        public function dayHours($date='') {
            $out = [];
            if (!$date) $date = date('Y-m-d');
            $first = strtotime(date('Y-m-d 00:00:00', strtotime($date)));
            $out[0] = date('Y-m-d H:i:s', $first);
            for ($i=1; $i<=23; $i++) {
                $out[$i] = date('Y-m-d H:i:s', strtotime('+' .$i. ' Hour', $first));
            }
            return $out;
        }

        // CLEAN A PHONE NUMBER (TO JUST NUMBERS)
        public function cleanPhone($number) {
            return trim(preg_replace('/\D/', '', $number));
        }

        // FORMAT PHONE NUMBER
        public function prettyPhone($number) {
            $number = preg_replace('/\D/', '', $number);
            if ($number) {
                return '(' .substr($number, 0, 3). ') ' .substr($number, 3, 3). '-' .substr($number, 6);
            }
        }


        #############################
        # GENERAL - SETTINGS
        #############################


        // GET SETTING
        public function getSetting($id='') {
            $id = sqlescape($id);
            if ($id) {
                if (isset($this->settings[$id])) {
                    return $this->settings[$id];
                } else {
                    $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tables['settings']. "` WHERE `id`='" .$id. "'"));
                    if ($this->isJson($f['value'])) {
                        $val = json_decode($f['value'], true);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$id] = $val;
                }
                return $val;
            }
        }


        // GET SETTINGS
        public function getSettings($array='') {
            $out = array();
            if (is_array($array)) {
                $oa = array();
                foreach ($array as $id) {
                    $id = sqlescape($id);
                    if ($id) {
                        $oa[] = "`id`='" .$id. "'";
                    }
                }
                if (count($oa) > 0) {
                    $or = "(" .implode(") OR (", $oa). ")";
                }
                if ($or) {
                    $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` WHERE " .$or;
                }
            } else {
                $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` ORDER BY `id` ASC";
            }
            if ($query) {
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    if ($this->isJson($string)) {
                        $val = json_decode($f['value']);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$f['id']] = $val;
                    $out[$f['id']] = $val;
                }
            }
            return $out;
        }


        // SET SETTING
        public function setSetting($id='', $value='') {

            // CLEAN UP
            $id = sqlescape($id);

            // HAVE ID
            if ($id) {

                // VALUE IS ARRAY
                if (is_array($value)) {
                    $value = json_encode($value);

                // VALUE IS STRING
                } else {
                    $value = sqlescape($value);
                }

                $query = "
                INSERT INTO `" .$this->tables['settings']. "`
                SET `id`='" .$id. "', `value`='" .$value. "'
                ON DUPLICATE KEY UPDATE `value`='" .$value. "'
                ";
                if (sqlquery($query)) {
                    return true;
                }

            }

        }


        #############################
        # JOBS
        #############################


        // RETURNS ARRAY OF JOB STATUSES
        public function jobStatuses($i=null) {
            $statuses = array(
                0 => [
                    'title' => 'Inactive',
                    'color' => '#E2E3E5',
                ],
                1 => [
                    'title' => 'Active',
                    'color' => '#28a745',
                ],
                2 => [
                    'title' => 'Ongoing',
                    'color' => '#CCE5FF',
                ],
                3 => [
                    'title' => 'Complete',
                    'color' => '#56bdd0',
                ],
                8 => [
                    'title' => 'Cancelled',
                    'color' => '#efa766',
                ],
                9 => [
                    'title' => 'Deleted',
                    'color' => '#F8D7DA',
                ],
            );
            if (is_numeric($i)) {
                return $statuses[$i];
            } else {
                return $statuses;
            }
        }

        // RETURNS ARRAY OF MATCHING JOBS
        public function getJobs($params=[], $options=[]) {
            $out = [];

            $page = (int)$params['page'];
            $limit = (int)$params['limit'];

            $start = ($params['start'] ? date('Y-m-d H:i:s', strtotime($params['start'])) : '');
            $end = ($params['end'] ? date('Y-m-d H:i:s', strtotime($params['end'])) : '');

            if (!$page) $page = 1;
            if (!$limit) $limit = $this->perPage();

            $ij = [];
            $wa = [];

            // STATUS
            if (strlen($params['status'])) {
                $wa[] = "j.status=" .(int)$params['status'];
            } else {
                $wa[] = "j.status!=9";
            }

            // START / END
            if (($start) && ($end)) {
                $wa[] = "
                (j.start BETWEEN '" .$start. "' AND '" .$end. "')
                OR
                (j.end BETWEEN '" .$start. "' AND '" .$end. "')
                OR
                (('" .$start. "'>=j.start) AND ('" .$end. "'<=j.end))
                ";
            } else if ($start) {
                $wa[] = "j.start>='" .$start. "'";
            } else if ($end) {
                $wa[] = "j.end<='" .$end. "'";
            }

            // KEYWORD
            if ($params['q']) {
                $wa[] = "LOWER(j.title) LIKE '%" .strtolower(trim(sqlescape($params['q']))). "%'";
            }

            // STAFF ID
            if ($params['staff_id']) {
                $ij['staff_jobs'] = "INNER JOIN " .$this->tables['staff_jobs']. " AS `sj` ON j.id=sj.job_id";
                $wa[] = "sj.staff_id=" .(int)$params['staff_id'];
            }

            // ROUTE ID
            if ($params['route_id']) {
                $wa[] = "j.route_id=" .(int)$params['route_id'];
            }

            // CHANNELS
            foreach ((array)$params['channels'] as $channel) {
                $ij['jobs_channels'] = "INNER JOIN " .$this->tables['jobs_channels']. " AS `jc` ON j.id=jc.job_id";
                $wa[] = "jc.channel='" .sqlescape($channel['channel']). "' AND jc.what='" .sqlescape($channel['what']). "' AND jc.item_id='" .sqlescape($channel['item_id']). "'";
            }

            if (count($ij) > 0) {
                $inner_join = "";
            } else {
                $inner_join = "";
            }

            $inner_join = (count($ij) > 0 ? implode(" ", $ij) : "");

            $where = "WHERE (" .implode(") AND (", $wa). ")";

            // BASE QUERY
            $query = "
            FROM `" .$this->tables['jobs']. "` AS `j`
            " .$inner_join. "
            " .$where. "
            ";

            // TOTAL
            $total = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` " .$query))['total'];

            // PAGED QUERY
            $query_paged = "
            SELECT j.*
            " .$query. "
            ORDER BY j.id DESC
            LIMIT " .(($page - 1) * $limit). "," .$limit. "
            ";
            $q = sqlquery($query_paged);
            while ($job = sqlfetch($q)) {

                $job['channels'] = $this->jobChannels($job['id']);
                $job['staff'] = $this->jobStaff($job['id']);

                $out['jobs'][$job['id']] = $job;

            }

            // PAGES
            $out['pages'] = $this->pageCount($total, $limit);
            $out['page'] = $page;

            return $out;
        }

        // RETURNS A SPECIFIC JOB
        public function getJob($id) {
            $id = (int)$id;
            return (array)$this->getJobs([ 'id' => $id ])['jobs'][$id];
        }

        // SAVES A JOB
        public function saveJob($params=[], $options=[]) {
            $out = [];

            $id = (int)$params['id'];

            $job_dbc = [];

            if (isset($params['status'])) $job_dbc['status'] = (int)$params['status'];
            if (isset($params['title'])) $job_dbc['title'] = $params['title'];
            if (isset($params['start'])) $job_dbc['start'] = date('Y-m-d H:i:s', strtotime($params['start']));
            if (isset($params['end'])) $job_dbc['end'] = date('Y-m-d H:i:s', strtotime($params['end']));
            if (isset($params['route_id'])) $job_dbc['route_id'] = (int)$params['route_id'];

            if ($params['dt_created']) $job_dbc['dt_created'] = date('Y-m-d H:i:s', strtotime($params['dt_created']));
            if ($params['dt_updated']) $job_dbc['dt_updated'] = date('Y-m-d H:i:s', strtotime($params['dt_updated']));

            // HAVE ID
            if ($id) {

                if (!$job_dbc['dt_updated']) $job_dbc['dt_updated'] = date('Y-m-d H:i:s');

                // UPDATE JOB
                $query = "UPDATE `" .$this->tables['jobs']. "` SET " .$this->createSet($job_dbc). " WHERE (`id`=" .$id. ")";
                if (sqlquery($query)) {
                    $out['success'] = true;
                    $out['id'] = $id;
                } else {
                    unset($id);
                    $out['error'] = 'Failed to update job.';
                }

            // NO ID
            } else {

                if (!$job_dbc['dt_created']) $job_dbc['dt_created'] = date('Y-m-d H:i:s');

                // CREATE JOB
                $query = "INSERT INTO `" .$this->tables['jobs']. "` SET " .$this->createSet($job_dbc). "";
                if (sqlquery($query)) {

                    $out['success'] = true;

                    // NEW ID
                    $id = sqlid();

                    $out['id'] = $id;

                // FAILED
                } else {
                    $out['error'] = 'Failed to create job.';
                }

            }

            // HAVE ID
            if ($id) {

                // HAVE CHANNELS
                if (is_array($params['channels'])) {

                    // GET CURRENT CHANNEL RELATIONS FOR JOB
                    $job_channels = $this->jobChannels($id);

                    $jch = [];
                    foreach ((array)$job_channels as $jc) {
                        $jch[$jc['hash']] = $jc['id'];
                    }

                    foreach ($params['channels'] as $channel) {

                        // GENERATE HASH
                        $hash = md5($channel['channel'] . $channel['what'] . $channel['item_id']);

                        // NOT YET ASSOCIATED
                        if (!$jch[$hash]) {

                            $rel_columns = [
                                'job_id'    => $id,
                                'channel'   => $channel['channel'],
                                'what'      => $channel['what'],
                                'item_id'   => $channel['item_id'],
                                'hash'      => $hash,
                            ];

                            // CREATE RECORD
                            $rel_sql = "INSERT INTO `" .$this->tables['jobs_channels']. "` SET " .$this->createSet($rel_columns);
                            sqlquery($rel_sql);

                        }

                    }

                }

                // GET CURRENT STAFF FOR JOB
                $job_staff = $this->jobStaff($id);

                // HAVE STAFF
                if (is_array($params['staff'])) {

                    foreach ($params['staff'] as $staff_id) {

                        $staff_id = (int)$staff_id;

                        // ALREADY ASSOCIATED
                        if ($job_staff[$staff_id]['id']) {

                            $role_id = (int)$params['staff_roles'][$staff_id];

                            $rel_columns = [];

                            if ($role_id) {
                                $rel_columns['role_id'] = $role_id;
                            }

                            if (count($rel_columns) > 0) {

                                // UPDATE RECORD
                                $rel_sql = "UPDATE `" .$this->tables['staff_jobs']. "` SET " .$this->createSet($rel_columns). " WHERE (`id`=" .$job_staff[$staff_id]['id']. ")";
                                sqlquery($rel_sql);

                            }

                            unset($job_staff[$staff_id]);

                        // ADD MISSING
                        } else {

                            $rel_columns = [
                                'staff_id'  => $staff_id,
                                'job_id'    => $id,
                                'role_id'   => (int)$params['staff_roles'][$staff_id]
                            ];

                            // CREATE RECORD
                            $rel_sql = "INSERT INTO `" .$this->tables['staff_jobs']. "` SET " .$this->createSet($rel_columns);
                            sqlquery($rel_sql);

                        }

                    }

                }

                // DELETE OLD
                if (count($job_staff) > 0) {
                    foreach ($job_staff as $staff) {
                        $del_sql = "DELETE FROM `" .$this->tables['staff_jobs']. "` WHERE (`staff_id`=" .$staff['staff_id']. ") AND (`job_id`=" .$id. ")";
                        sqlquery($del_sql);
                    }
                }

            }

            return $out;

        }

        // GET STAFF FOR JOB
        public function jobStaff($id) {
            $out = [];

            $id = (int)$id;

            if ($id) {

                $query = "
                SELECT *
                FROM `" .$this->tables['staff_jobs']. "`
                WHERE (`job_id`=" .$id. ")
                ";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['staff_id']] = $row;
                }

            }

            return $out;
        }

        // GET CHANNELS FOR JOB
        public function jobChannels($id) {
            $out = [];

            $id = (int)$id;

            if ($id) {

                $query = "
                SELECT *
                FROM `" .$this->tables['jobs_channels']. "`
                WHERE (`job_id`=" .$id. ")
                ";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[] = $row;
                }

            }

            return $out;
        }

        #############################
        # PHOTOS
        #############################


        public function getMimeExtension($mime) {
            if(empty($mime)) return false;
            switch($mime) {
               case 'image/bmp': return 'bmp';
               case 'image/cis-cod': return 'cod';
               case 'image/gif': return 'gif';
               case 'image/ief': return 'ief';
               case 'image/jpeg': return 'jpg';
               case 'image/pipeg': return 'jfif';
               case 'image/tiff': return 'tif';
               case 'image/x-cmu-raster': return 'ras';
               case 'image/x-cmx': return 'cmx';
               case 'image/x-icon': return 'ico';
               case 'image/x-portable-anymap': return 'pnm';
               case 'image/x-portable-bitmap': return 'pbm';
               case 'image/x-portable-graymap': return 'pgm';
               case 'image/x-portable-pixmap': return 'ppm';
               case 'image/x-rgb': return 'rgb';
               case 'image/x-xbitmap': return 'xbm';
               case 'image/x-xpixmap': return 'xpm';
               case 'image/x-xwindowdump': return 'xwd';
               case 'image/png': return 'png';
               case 'image/x-jps': return 'jps';
               case 'image/x-freehand': return 'fh';
               default: return false;
           }
        }

        // SAVE
        public function photos_savePhoto($staff_id, $options) {

            $out = array();

            $staff_id = (int)$staff_id;

            // HAVE STAFF ID
            if ($staff_id) {

                //$out['success'] = true;
                //$out['id'] = 555;

                // HAVE BASE64
                if ($options['image_base64']) {

                    // BINARY
                    $data_binary = base64_decode($options['image_base64']);

                    // GET SOME FILE INFO
                    if ($finfo = @getimagesizefromstring($data_binary)) {

                        // GET EXTENSION FROM MIME
                        $extension = $this->getMimeExtension($finfo['mime']);

                        // HAVE EXTENSION
                        if ($extension) {

                            // FOLDER TO SAVE IN
                            $folder = SITE_ROOT. 'extensions/' .$this->Extension. '/files/photos/' .date('Y-m'). '/';

                            // FOLDER DOESN'T EXIST
                            if (!is_dir($folder)) {
                                mkdir($folder); // CREATE
                            }

                            // FOLDER NOW EXISTS
                            if (is_dir($folder)) {

                                // GENERATE FILE NAME
                                $file_name = md5(time().uniqid()). '.' .$extension;

                                // FILE
                                $file = $folder . $file_name;

                                // SAVE FILE
                                if (file_put_contents($file, $data_binary)) {

                                    // TIME SPECIFIED
                                    if ($options['timestamp']) {
                                        $dt = strtotime($options['timestamp']);
                                    } else {
                                        $dt = time();
                                    }
                                    $dt_formatted = date('Y-m-d H:i:s', $dt);

                                    $columns = array(
                                        'staff_id'  => $staff_id,
                                        'job_id'    => (int)$options['job_id'],
                                        'dt'        => $dt_formatted,
                                        'file'      => $file_name,
                                    );

                                    if (is_array($options['gps'])) {
                                        if ($options['gps']['latitude']) {
                                            $columns['gps_latitude'] = (float)$options['gps']['latitude'];
                                        }
                                        if ($options['gps']['longitude']) {
                                            $columns['gps_longitude'] = (float)$options['gps']['longitude'];
                                        }
                                    }

                                    // CREATE IMAGE RECORD
                                    $query = "INSERT INTO `" .$this->tables['photos']. "` SET " .uccms_createSet($columns);
                                    if (sqlquery($query)) {

                                        $out['success'] = true;
                                        $out['id'] = sqlid();

                                        // INIT DAM
                                        $_dam = new DigitalAssetManager();

                                        // DIGITAL ASSET MANAGER VARS
                                        $dam_vars = array(
                                            'image_url'                 => $this->photoURL($out['id']),
                                            'website_extension'         => $this->Extension,
                                            'website_extension_item'    => 'photos',
                                            'website_extension_item_id' => $out['id'],
                                        );

                                        if ($columns['gps_latitude']) {
                                            $dam_vars['gps']['latitude'] = $columns['gps_latitude'];
                                        }
                                        if ($columns['gps_longitude']) {
                                            $dam_vars['gps']['longitude'] = $columns['gps_longitude'];
                                        }

                                        /*
                                        if (($columns['gps_latitude']) && ($columns['gps_longitude'])) {
                                            $dam_vars['geolocation'] = array(
                                                $columns['gps_latitude'],
                                                $columns['gps_longitude']
                                            );
                                        }
                                        */

                                        //if (is_array($albums)) $dam_vars['albums'] = $albums;
                                        //if (is_array($tags)) $dam_vars['tags'] = $tags;

                                        //file_put_contents('dam_vars', print_r($dam_vars, true));

                                        // ADD MEDIA
                                        $dam_result = $_dam->addMedia($dam_vars);

                                        //file_put_contents('dam_result', print_r($dam_result, true));

                                        //$out['debug']['dam_result'] = $dam_result;

                                        if ($dam_result['elastic_search_id']) {

                                            // RETURN SOME DAM INFO
                                            $out['dam'] = array(
                                                'id'    => $dam_result['elastic_search_id'],
                                                'url'   => 'https://s3.amazonaws.com/' .$dam_result['s3_location'],
                                            );

                                        } else {
                                            if ($dam_result['error']) {
                                                $out['dam']['err'] = $dam_result['error'];
                                            } else {
                                                $out['dam']['err'] = 'Failed to save image to Digital Asset Manager.';
                                            }
                                        }


                                    } else {
                                        $out['error'] = 'Failed to save photo.';
                                        @unlink($file);
                                    }

                                }

                            }

                        // NO MATCHING EXTENSION
                        } else {
                            $out['error'] = 'No matching file extension.';
                        }

                    }

                }

            }

            return $out;

        }

        // GET URL OF A PHOTO
        public function photoURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['photos']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['id']) {
                return $this->imageBridgeOut(date('Y-m', strtotime($data['dt'])). '/' .$data['file'], 'photos');
            }
        }


        #############################
        # ROLES
        #############################


        // RETURNS ARRAY OF ROLES
        public function getRoles($onlyActive=true) {
            $out = [];
            if ($onlyActive) $active_sql = " WHERE (`active`=1) ";
            $query = "SELECT * FROM `" .$this->tables['roles']. "` " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($role = sqlfetch($q)) {
                $out[$role['id']] = $role;
            }
            return $out;
        }

        // RETURNS A SPECIFIC ROLE
        public function getRole($id=0) {
            if ($id) {
                $query = "SELECT * FROM `" .$this->tables['roles']. "` WHERE (`id`=" .(int)$id. ")";
                return sqlfetch(sqlquery($query));
            } else {
                return [];
            }
        }


        #############################
        # ROUTES
        #############################

        // RETURNS ARRAY OF ROUTES
        public function getRoutes($onlyActive=true) {
            $out = [];
            if ($onlyActive) $active_sql = " WHERE (`active`=1) ";
            $query = "SELECT * FROM `" .$this->tables['routes']. "` " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($route = sqlfetch($q)) {
                $out[$route['id']] = $route;
            }
            return $out;
        }

        // GET ROUTES BY TIMEFRAME
        public function getRoutesByTimeframe($params=[], $options=[]) {
            $out = [];

            if (($params['start']) && ($params['end'])) {

                $wa = [];

                if (isset($params['active'])) {
                    $wa[] = "r.active=" .(int)$params['active'];
                }

                // TIMEFRAME
                if ($params['start']) {
                    $start = strtotime($params['start']);
                } else {
                    $start = time();
                }
                if ($params['end']) {
                    $end = strtotime($params['end']);
                } else {
                    $end = time();
                }
                $start = date('Y-m-d 00:00:00', $start);
                $end = date('Y-m-d 23:59:59', $end);

                $wa[] = "((r.start<='" .$end. "') AND (r.end>='" .$start. "')) OR ((r.start<='" .$end. "') AND (DATE(r.end)='0000-00-00'))";

                // IS SEARCHING
                if ($params['q']) {
                    $qparts = explode(" ", $params['q']);
                    $twa = array();
                    foreach ($qparts as $part) {
                        $part = sqlescape(strtolower($part));
                        $twa[] = "LOWER(r.title) LIKE '%$part%'";
                    }
                    $wa[] = "(" .implode(" OR ", $twa). ")";
                }

                if (count($wa) > 0) {
                    $where_sql = "WHERE (" .implode(") AND (", $wa). ")";
                } else {
                    $where_sql = "";
                }

                $routes_query = "
                SELECT r.*
                FROM `" .$this->tables['routes']. "` AS `r`
                " .$where_sql. "
                ORDER BY r.sort ASC, r.title ASC, r.id ASC
                ";
                $routes_q = sqlquery($routes_query);
                while ($route = sqlfetch($routes_q)) {

                    // IS RECURRING
                    if ($route['recurring']) {

                        // GENERATE RECURRING RULE
                        $rrule = $this->rrule_generate($route);

                        //echo $rrule;

                        // TRY When
                        try {

                            $r = new When\When();
                            $r->RFC5545_COMPLIANT = When\When::IGNORE;

                            // GET OCCURANCES
                            $r->startDate(new DateTime($route['start']))->rrule($rrule)->generateOccurrences();

                            //print_r($r->occurrences);

                            // HAVE OCCURRENCES
                            if (count($r->occurrences) > 0) {

                                // LOOP
                                foreach ($r->occurrences as $occ) {

                                    $route['date'] = $occ->format('Y-m-d H:i:s');

                                    // DATE IS WITHIN TIMEFRAME
                                    if ((strtotime($route['date']) >= strtotime($start)) && (strtotime($route['date']) <= strtotime($end))) {
                                        $out[] = $route;
                                    }

                                }

                            }

                        // When ERROR
                        } catch (Exception $e) {
                            //echo 'Caught exception: ',  $e->getMessage(), "\n";
                            $out['error'] = $e->getMessage();
                        }

                    // SINGLE EVENT
                    } else {
                        $route['date'] = $route['start'];
                        $out[] = $route;
                    }

                }

                //print_r($out);
                //exit;

                // HAVE RESULTS
                if (count($out) > 0) {

                    // SORT BY DATE/TIME ASCENDING
                    usort($out, function($a, $b) {
                        return strtotime($a['date']) - strtotime($b['date']);
                    });

                }

            } else {
                $out['error'] = 'Start and end not specified.';
            }

            return $out;

        }

        // RETURNS A SPECIFIC ROUTE
        public function getRoute($id=0, $date='') {
            if ($id) {
                $query = "SELECT * FROM `" .$this->tables['routes']. "` WHERE (`id`=" .(int)$id. ")";
                $route = sqlfetch(sqlquery($query));
                if ($route['id']) {
                    if ($date) {
                        $override = $this->getRouteOverride($route['id'], $date);
                        $route['override'] = $override;
                    }
                }
                return $route;
            } else {
                return [];
            }
        }

        // GET STAFF FOR A ROUTE
        public function routeStaffs($id, $date='', $params=[], $options=[]) {
            $out = [];

            $id = (int)$id;

            if ($id) {

                $query = "
                SELECT *
                FROM `" .$this->tables['staff_routes']. "`
                WHERE (`route_id`=" .$id. ")
                ";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['staff_id']] = $row;
                }

                // HAVE DATE
                if ($date) {

                    // GET ROUTE
                    $route = $this->getRoute($id, $date);

                    // OVERRIDE JOB ORDER
                    if (count((array)$route['override']['options']['staff']) > 0) {
                        unset($out);
                        foreach ($route['override']['options']['staff'] as $staff_id) {
                            $out[$staff_id] = [
                                'route_id' => $route['id'],
                                'staff_id' => $staff_id,
                            ];
                        }
                    }

                }

            }

            return $out;
        }


        // GET CUSTOMERS FOR A ROUTE
        public function routeCustomers($id, $params=[], $options=[]) {
            $out = [];

            $id = (int)$id;

            if ($id) {

                // HAVE E-COMMERCE
                if (class_exists('uccms_Ecommerce')) {

                    $_uccms_ecomm = new uccms_Ecommerce();

                    // CUSTOMER CONTACTS IN ROUTE
                    $rcc_sql = "SELECT * FROM `" .$_uccms_ecomm->tables['customer_contacts']. "` WHERE (`route_id`=" .$id. ") AND (`active`=1) GROUP BY `customer_id`";
                    $rcc_q = sqlquery($rcc_sql);
                    while ($rcc = sqlfetch($rcc_q)) {
                        $out[$rcc['customer_id']][$rcc['id']] = [
                            'contact_id' => $rcc['id']
                        ];
                    }

                    // GET JOBS
                    if (!$params['jobs']) {
                        $params['jobs'] = $this->getJobs([
                            'start' => $params['start'],
                            'end' => $params['end'],
                            'route_id' => $id,
                            'limit' => 1000
                        ])['jobs'];
                    }

                    //print_r($params['jobs']);

                    // JOBS IN ROUTE
                    foreach ((array)$params['jobs'] as $rjob) {

                        //print_r($out);

                        // APPLY TO CUSTOMERS FROM CONTACTS WITH ROUTE
                        foreach((array)$out as $customer_id => $contacts) {
                            foreach ($contacts as $contact) {
                                $out[$customer_id][$contact['contact_id']]['jobs'][$rjob['id']] = $rjob['id'];
                            }
                        }

                        // LOOP THROUGH JOB CHANNELS
                        foreach ((array)$rjob['channels'] as $channel) {

                            if (($channel['channel'] = 'com.umbrella.ecommerce') && ($channel['what'] == 'order') && ($channel['item_id'])) {

                                $order = $_uccms_ecomm->orderInfo((int)$channel['item_id']);

                                $customer = $_uccms_ecomm->getCustomer($order['customer_id']);

                                $rccs = $_uccms_ecomm->customerContacts($customer['id'], [
                                    'route_id' => $id
                                ]);
                                if (count((array)$rccs['contacts']) > 0) {
                                    foreach ($rccs['contacts'] as $rcc) {
                                        $cjobs = (array)$out[$rcc['customer_id']][$rcc['id']]['jobs'];
                                        $cjobs[$rjob['id']] = $rjob['id'];
                                        $out[$rcc['customer_id']][$rcc['id']] = [
                                            'contact_id' => $rcc['id'],
                                            'jobs' => $cjobs
                                        ];
                                    }
                                } else {
                                    $ccid = 0;
                                    if ($customer['contacts']['default']['delivery']) {
                                        $ccid = $customer['contacts']['default']['delivery'];
                                    } else if ($customer['contacts']['default']['shipping']) {
                                        $ccid = $customer['contacts']['default']['shipping'];
                                    } else if ($customer['contacts']['default']['billing']) {
                                        $ccid = $customer['contacts']['default']['billing'];
                                    }
                                    if ($ccid) {
                                        $rcc = $_uccms_ecomm->customerContact($customer['id'], $ccid);
                                        if ($rcc['id']) {
                                            $cjobs = (array)$out[$rcc['customer_id']][$rcc['id']]['jobs'];
                                            $cjobs[$rjob['id']] = $rjob['id'];
                                            $out[$rcc['customer_id']][$rcc['id']] = [
                                                'contact_id' => $rcc['id'],
                                                'jobs' => $cjobs
                                            ];
                                        }
                                    }
                                }

                            }

                        }

                    }

                }

            }

            return $out;
        }

        // GET JOBS ON ROUTE
        public function routeJobs($route_id, $date, $params=[], $options=[]) {

            $params['route_id'] = (int)$route_id;

            $params['start'] = date('Y-m-d 00:00:00', strtotime($date));
            $params['end'] = date('Y-m-d 23:59:59', strtotime($date));

            if (!$params['limit']) $params['limit'] = 1000;

            $rjobs = $this->getJobs($params);

            // HAVE JOBS
            if (count((array)$rjobs['jobs']) > 0) {

                // GET ROUTE
                $route = $this->getRoute($route_id, $date);

                // OVERRIDE JOB ORDER
                if (count((array)$route['override']['options']['jobSort']) > 0) {
                    $trjobs = [];
                    foreach ($route['override']['options']['jobSort'] as $job_id) {
                        $trjobs[$job_id] = $rjobs['jobs'][$job_id];
                        unset($rjobs['jobs'][$job_id]);
                    }
                    foreach ((array)$rjobs['jobs'] as $rjob) {
                        $trjobs[$rjob['id']] = $rjob;
                    }
                    $rjobs['jobs'] = $trjobs;
                    unset($trjobs);
                }

            }

            return $rjobs;

        }

        // SAVE A ROUTE'S OVERRIDE
        public function routeOverrideSave($route_id, $date, $params=[], $options=[]) {

            $route_id = (int)$route_id;
            $date = date('Y-m-d H:i:s', strtotime($date));

            // CHECK FOR OVERRIDE
            $override = $this->getRouteOverride($route_id, $date);

            $params['options'] = array_merge((array)$override['options'], (array)$params['options']);

            $columns = [
                'options' => json_encode($params['options']),
            ];

            // UPDATE
            if ($override['id']) {

                $out['id'] = $override['id'];

                $over_sql = "UPDATE `" .$this->tables['routes_overrides']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .$override['id']. ")";
                if (sqlquery($over_sql)) {
                    $out['success'] = true;
                } else {
                    $out['error'] = 'Failed to update route override.';
                }

            // CREATE
            } else {

                $columns['route_id'] = $route_id;
                $columns['date'] = $date;

                $over_sql = "INSERT INTO `" .$this->tables['routes_overrides']. "` SET " .$this->createSet($columns);
                if (sqlquery($over_sql)) {
                    $out['success'] = true;
                    $out['id'] = sqlid();
                } else {
                    $out['error'] = 'Failed to save route override.';
                }

            }

            return $out;

        }

        // GET A ROUTE'S OVERRIDE
        public function getRouteOverride($route_id, $date) {

            $route_id = (int)$route_id;
            $date = date('Y-m-d H:i:s', strtotime($date));

            // CHECK FOR OVERRIDE
            $over_sql = "SELECT * FROM `" .$this->tables['routes_overrides']. "` WHERE (`route_id`=" .$route_id. ") AND (`date`='" .$date. "') LIMIT 1";
            $over_q = sqlquery($over_sql);
            $over = sqlfetch($over_q);

            $over['options'] = json_decode(stripslashes($over['options']), true);

            return $over;

        }



        #############################
        # SHIFTS
        #############################


        // RETURNS ARRAY OF SHIFTS
        public function getShifts($onlyActive=true) {
            $out = [];
            if ($onlyActive) $active_sql = " WHERE (`active`=1) ";
            $query = "SELECT * FROM `" .$this->tables['shifts']. "` " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($shift = sqlfetch($q)) {
                $out[$shift['id']] = $shift;
            }
            return $out;
        }

        // RETURNS A SPECIFIC SHIFT
        public function getShift($id=0) {
            if ($id) {
                $query = "SELECT * FROM `" .$this->tables['shifts']. "` WHERE (`id`=" .(int)$id. ")";
                return sqlfetch(sqlquery($query));
            } else {
                return [];
            }
        }


        #############################
        # STAFF
        #############################


        // RETURNS ARRAY OF STAFF STATUSES
        public function staffStatuses($i=null) {
            $statuses = array(
                0 => [
                    'title' => 'Inactive',
                    'color' => '#E2E3E5',
                ],
                1 => [
                    'title' => 'Active',
                    'color' => '#28a745',
                ],
                5 => [
                    'title' => 'Vacation',
                    'color' => '#CCE5FF',
                ],
                7 => [
                    'title' => 'Suspended',
                    'color' => '#D1ECF1',
                ],
                8 => [
                    'title' => 'Terminated',
                    'color' => '#FFF3CD',
                ],
                9 => [
                    'title' => 'Deleted',
                    'color' => '#F8D7DA',
                ],
            );
            if (is_numeric($i)) {
                return $statuses[$i];
            } else {
                return $statuses;
            }
        }

        // RETURNS ARRAY OF MATCHING STAFF
        public function getStaffs($params=[], $options=[]) {
            $out = [];

            $page = (int)$params['page'];
            $limit = (int)$params['limit'];

            if (!$page) $page = 1;
            if (!$limit) $limit = $this->perPage();

            $wa = [];

            if ($params['id']) {
                $wa[] = "s.id=" .(int)$params['id'];
            } else {
                $wa[] = "s.id>0";
            }

            if (strlen($params['status'])) {
                $wa[] = "s.status=" .(int)$params['status'];
            } else {
                $wa[] = "s.status!=9";
            }

            if ($params['q']) {
                $clean = strtolower(trim(sqlescape($params['q'])));
                $wa[] = "(LOWER(s.firstname) LIKE '%" .$clean. "%') OR (LOWER(s.lastname) LIKE '%" .$clean. "%')";
            }

            $where = "WHERE (" .implode(") AND (", $wa). ")";

            $query = "
            FROM `" .$this->tables['staff']. "` AS `s`
            " .$where. "
            ";

            // TOTAL
            $total = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` " .$query))['total'];

            $query_paged = "
            SELECT *
            " .$query. "
            ORDER BY s.firstname ASC, s.lastname ASC
            LIMIT " .(($page - 1) * $limit). "," .$limit. "
            ";

            $q = sqlquery($query_paged);
            while ($staff = sqlfetch($q)) {

                $staff['name'] = trim(stripslashes($staff['firstname']. ' ' .$staff['lastname']));

                if ($staff['image']) {
                    $staff['image'] = [
                        'file'      => $staff['image'],
                        'large'     => $this->imageBridgeOut($staff['image'], 'staff'),
                        'avatar'    => BigTree::prefixFile($this->imageBridgeOut($staff['image'], 'staff'), 'a_'),
                    ];
                } else {
                    $staff['image'] = [
                        'large'     => '/extensions/' .$this->Extension. '/images/default-profile.png',
                        'avatar'    => '/extensions/' .$this->Extension. '/images/a_default-profile.png',
                    ];
                }

                $staff['roles'] = $this->staffRoles($staff['id']);
                $staff['routes'] = $this->staffRoutes($staff['id']);
                $staff['shifts'] = $this->staffShifts($staff['id']);

                $out['staff'][$staff['id']] = $staff;

            }

            // PAGES
            $out['pages'] = $this->pageCount($total, $limit);
            $out['page'] = $page;

            return $out;

        }

        // RETURNS A SPECIFIC STAFF
        public function getStaff($id) {
            $id = (int)$id;
            return (array)$this->getStaffs([ 'id' => $id ])['staff'][$id];
        }

        // GET ROLES FOR A STAFF
        public function staffRoles($id) {
            $out = [];

            $id = (int)$id;

            if ($id) {

                $query = "
                SELECT *
                FROM `" .$this->tables['staff_roles']. "`
                WHERE (`staff_id`=" .$id. ")
                ";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['role_id']] = $row;
                }

            }

            return $out;
        }

        // GET ROUTES FOR A STAFF
        public function staffRoutes($id) {
            $out = [];

            $id = (int)$id;

            if ($id) {

                $query = "
                SELECT *
                FROM `" .$this->tables['staff_routes']. "`
                WHERE (`staff_id`=" .$id. ")
                ";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['route_id']] = $row;
                }

            }

            return $out;
        }

        // GET SHIFTS FOR A STAFF
        public function staffShifts($id) {
            $out = [];

            $id = (int)$id;

            if ($id) {

                $query = "
                SELECT ss.*
                FROM `" .$this->tables['staff_shifts']. "` AS `ss`
                INNER JOIN `" .$this->tables['shifts']. "` AS `s` ON ss.shift_id = s.id
                WHERE (ss.staff_id=" .$id. ")
                ORDER BY s.sort ASC, s.title ASC, s.id ASC
                ";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['shift_id']] = $row;
                }

            }

            return $out;
        }


        #############################
        # TIME TRACKING
        #############################


        // GET TIME TRACKING RECORD(S)
        public function timeTracking_getRecords($staff_id, $options) {

            $out = array();

            $staff_id = (int)$staff_id;

            // HAVE STAFF ID
            if ($staff_id) {

                $wa = array();
                $wa[] = "`staff_id`=" .$staff_id;

                // HAVE TIME ID
                if ($options['time_id']) {
                    $wa[] = "`id`=" .(int)$options['time_id'];

                // HAVE JOB ID
                } else if ($options['job_id']) {
                    $wa[] = "`job_id`=" .(int)$options['job_id'];
                }

                // TIME RANGE
                $start = ($options['dt_start'] ? date('Y-m-d H:i:s', strtotime($options['dt_start'])) : '');
                $end = ($options['dt_end'] ? date('Y-m-d H:i:s', strtotime($options['dt_end'])) : '');

                if (($start) && ($end)) {
                    $wa[] = "
                    (`dt_start` BETWEEN '" .$start. "' AND '" .$end. "')
                    OR
                    (`dt_end` BETWEEN '" .$start. "' AND '" .$end. "')
                    OR
                    (('" .$start. "'>=`dt_start`) AND ('" .$end. "'<=`dt_end`))
                    ";
                } else if ($start) {
                    $wa[] = "`dt_start`>='" .$start. "'";
                } else if ($end) {
                    $wa[] = "`dt_end`<='" .$end. "'";
                }

                $where_sql = "(" .implode(") AND (", $wa). ")";

                if ($options['offset']) {
                    $offset = (int)$options['offset'];
                } else {
                    $offset = 0;
                }

                if ($options['limit']) {
                    $limit = (int)$options['limit'];
                } else {
                    $limit = $this->perPage();
                }

                // GET MATCHING
                $query = "
                SELECT *
                FROM `" .$this->tables['time_tracking']. "`
                WHERE " .$where_sql. "
                ORDER BY `dt_start` DESC
                LIMIT " .$offset. "," .$limit. "
                ";

                // QUERY SUCCESSFUL
                if ($q = sqlquery($query)) {

                    $out['success'] = true;

                    $tti = 0;

                    // LOOP
                    while ($time_tracking = sqlfetch($q)) {

                        if (($time_tracking['dt_end']) && ($time_tracking['dt_end'] != '0000-00-00 00:00:00')) {
                            $end = date('c', strtotime($time_tracking['dt_end']));
                        } else {
                            $end = '';
                        }

                        $out['records'][$tti] = array(
                            'id'        => $time_tracking['id'],
                            'job_id'    => $time_tracking['job_id'],
                            'start'     => date('c', strtotime($time_tracking['dt_start'])),
                            'end'       => $end,
                            'gps'       => array(
                                'start' => array(
                                    'latitude'  => ($time_tracking['gps_start_latitude'] != 0 ? $time_tracking['gps_start_latitude'] : ''),
                                    'longitude' => ($time_tracking['gps_start_longitude'] != 0 ? $time_tracking['gps_start_longitude'] : ''),
                                ),
                                'end'   => array(
                                    'latitude'  => ($time_tracking['gps_end_latitude'] != 0 ? $time_tracking['gps_end_latitude'] : ''),
                                    'longitude' => ($time_tracking['gps_end_longitude'] != 0 ? $time_tracking['gps_end_longitude'] : ''),
                                ),
                            ),
                        );

                        $tti++;

                    }

                } else {
                    $out['error'] = 'Failed to find any time records.';
                }

            // NO STAFF ID
            } else {
                $out['error'] = 'Staff ID not specified.';
            }

            return $out;

        }



        // RECORD
        public function timeTracking_saveRecord($staff_id, $options) {

            $out = array();

            $staff_id = (int)$staff_id;

            // HAVE STAFF ID
            if ($staff_id) {

                // TIME SPECIFIED
                if ($options['timestamp']) {
                    $dt = strtotime($options['timestamp']);
                } else {
                    $dt = time();
                }
                $dt_formatted = date('Y-m-d H:i:s', $dt);

                // HAVE TIME ID
                if ($options['time_id']) {

                    // IS START
                    if ($options['start']) {

                        $columns = array(
                            'dt_start' => $dt_formatted
                        );

                        if (is_array($options['gps'])) {
                            if ($options['gps']['latitude']) {
                                $columns['gps_start_latitude'] = (float)$options['gps']['latitude'];
                            }
                            if ($options['gps']['longitude']) {
                                $columns['gps_start_longitude'] = (float)$options['gps']['longitude'];
                            }
                        }

                    // DEFAULT TO END
                    } else {

                        $columns = array(
                            'dt_end' => $dt_formatted
                        );

                        if (is_array($options['gps'])) {
                            if ($options['gps']['latitude']) {
                                $columns['gps_end_latitude'] = (float)$options['gps']['latitude'];
                            }
                            if ($options['gps']['longitude']) {
                                $columns['gps_end_longitude'] = (float)$options['gps']['longitude'];
                            }
                        }

                    }

                    $query = "UPDATE `" .$this->tables['time_tracking']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .(int)$options['time_id']. ") AND (`staff_id`=" .$staff_id. ")";
                    if (sqlquery($query)) {
                        $out['success'] = true;
                        $out['id'] = (int)$options['time_id'];
                    } else {
                        $out['error'] = 'Failed to update time record.';
                    }

                // HAVE JOB ID
                } else if ($options['job_id']) {

                    $time_id = 0;

                    if (!$options['new']) {

                        // GET CURRENT FOR JOB
                        $current_query = "SELECT * FROM `" .$this->tables['time_tracking']. "` WHERE (`staff_id`=" .$staff_id. ") AND (`job_id`=" .(int)$options['job_id']. ") ORDER BY `dt_start` DESC LIMIT 1";
                        $current_q = sqlquery($current_query);
                        $current = sqlfetch($current_q);

                        // CURRENT IS ACTIVE
                        if ($current['dt_end'] == '0000-00-00 00:00:00') {
                            $time_id = $current['id'];
                        }

                    }

                    // HAVE TIME ID
                    if ($time_id) {

                        $columns = array(
                            'dt_end' => $dt_formatted
                        );

                        if (is_array($options['gps'])) {
                            if ($options['gps']['latitude']) {
                                $columns['gps_end_latitude'] = (float)$options['gps']['latitude'];
                            }
                            if ($options['gps']['longitude']) {
                                $columns['gps_end_longitude'] = (float)$options['gps']['longitude'];
                            }
                        }

                        $query = "UPDATE `" .$this->tables['time_tracking']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$time_id. ")";
                        if (sqlquery($query)) {
                            $out['success'] = true;
                            $out['id'] = $time_id;
                        } else {
                            $out['error'] = 'Failed to update time record for job.';
                        }

                    // NO TIME ID
                    } else {

                        $columns = array(
                            'staff_id'  => $staff_id,
                            'job_id'    => (int)$options['job_id'],
                            'dt_start'  => $dt_formatted
                        );

                        if (is_array($options['gps'])) {
                            if ($options['gps']['latitude']) {
                                $columns['gps_start_latitude'] = (float)$options['gps']['latitude'];
                            }
                            if ($options['gps']['longitude']) {
                                $columns['gps_start_longitude'] = (float)$options['gps']['longitude'];
                            }
                        }

                        $query = "INSERT INTO `" .$this->tables['time_tracking']. "` SET " .uccms_createSet($columns);
                        if (sqlquery($query)) {
                            $out['success'] = true;
                            $out['id'] = sqlid();
                        } else {
                            $out['error'] = 'Failed to create time record for job.';
                        }

                    }

                // NO TIME ID OR JOB ID
                } else {

                    $time_id = 0;

                    if (!$options['new']) {

                        // GET CURRENT FOR STAFF
                        $current_query = "SELECT * FROM `" .$this->tables['time_tracking']. "` WHERE (`staff_id`=" .$staff_id. ") ORDER BY `dt_start` DESC LIMIT 1";
                        $current_q = sqlquery($current_query);
                        $current = sqlfetch($current_q);

                        // CURRENT IS ACTIVE
                        if ($current['dt_end'] == '0000-00-00 00:00:00') {
                            $time_id = $current['id'];
                        }

                    }

                    //file_put_contents('options.txt', print_r($options, true));

                    // HAVE TIME ID
                    if ($time_id) {

                        $columns = array(
                            'dt_end' => $dt_formatted
                        );

                        if (is_array($options['gps'])) {
                            if ($options['gps']['latitude']) {
                                $columns['gps_end_latitude'] = (float)$options['gps']['latitude'];
                            }
                            if ($options['gps']['longitude']) {
                                $columns['gps_end_longitude'] = (float)$options['gps']['longitude'];
                            }
                        }

                        $query = "UPDATE `" .$this->tables['time_tracking']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$time_id. ")";

                        //file_put_contents('staff_sql.txt', $query);

                        if (sqlquery($query)) {
                            $out['success'] = true;
                            $out['id'] = $time_id;
                        } else {
                            $out['error'] = 'Failed to update staff time record.';
                        }

                    // NO TIME ID
                    } else {

                        $columns = array(
                            'staff_id'  => $staff_id,
                            'dt_start'  => $dt_formatted
                        );

                        if (is_array($options['gps'])) {
                            if ($options['gps']['latitude']) {
                                $columns['gps_start_latitude'] = (float)$options['gps']['latitude'];
                            }
                            if ($options['gps']['longitude']) {
                                $columns['gps_start_longitude'] = (float)$options['gps']['longitude'];
                            }
                        }

                        $query = "INSERT INTO `" .$this->tables['time_tracking']. "` SET " .uccms_createSet($columns);

                        //file_put_contents('staff_sql.txt', $query);

                        if (sqlquery($query)) {
                            $out['success'] = true;
                            $out['id'] = sqlid();
                        } else {
                            $out['error'] = 'Failed to create staff time record.';
                        }

                    }

                }

                // HAVE ID
                if ($out['id']) {

                    // GET INFO
                    $current_query = "SELECT * FROM `" .$this->tables['time_tracking']. "` WHERE (`id`=" .$out['id']. ")";
                    $current_q = sqlquery($current_query);
                    $current = sqlfetch($current_q);

                    $out['dt_start'] = ((($current['dt_start']) && ($current['dt_start'] != '0000-00-00 00:00:00')) ? date('c', strtotime($current['dt_start'])) : '');
                    $out['dt_end'] = ((($current['dt_end']) && ($current['dt_end'] != '0000-00-00 00:00:00')) ? date('c', strtotime($current['dt_end'])) : '');

                    if (!$out['dt_end']) {
                        $out['active'] = 1;
                    }

                }

            // NO STAFF ID
            } else {
                $out['error'] = 'Staff ID not specified.';
            }

            return $out;

        }


        #############################
        # MISC
        #############################

        // BUILD RRULE
        public function rrule_generate($event) {
            $out = '';

            // IS RECURRING
            if ($event['recurring']) {

                $parts = array();

                // FREQUENCY
                switch ($event['recurring_freq']) {
                    case 'day':
                        $parts['FREQ'] = 'DAILY';
                        break;
                    case 'week':
                        $parts['FREQ'] = 'WEEKLY';
                        break;
                    case 'month':
                        $parts['FREQ'] = 'MONTHLY';
                        break;
                    case 'year':
                        $parts['FREQ'] = 'YEARLY';
                        break;
                }

                // INTERVAL
                if ($event['recurring_interval'] > 0) {
                    $parts['INTERVAL'] = $event['recurring_interval'];
                }

                // COUNT
                if ($event['recurring_count'] > 0) {
                    $parts['COUNT'] = $event['recurring_count'];
                }

                // BY DAY
                if ($event['recurring_byday']) {
                    $parts['BYDAY'] = stripslashes($event['recurring_byday']);
                }

                // BY DAY OF MONTH
                if ($event['recurring_bymonthday']) {
                    $parts['BYMONTHDAY'] = stripslashes($event['recurring_bymonthday']);
                }

                // HAVE END DATE
                if (substr($event['end'], 0, 10) != '0000-00-00') {
                    $parts['UNTIL'] = date('c', strtotime($event['end']));
                }

                // HAVE PARTS
                if (count($parts) > 0) {
                    $tpa = array();
                    foreach ($parts as $name => $value) {
                        $tpa[] = $name. '=' .$value;
                    }
                    $out = implode(';', $tpa);
                    unset($parts);
                    unset($tpa);
                }

            }

            return $out;

        }


        #############################
        # CRON
        #############################

        /*
        // RUN THE MAIN CRON
        public function runCron() {

            return;

            $out = array();

            return implode("\n", $out). "\n";

        }
        */


    }

?>