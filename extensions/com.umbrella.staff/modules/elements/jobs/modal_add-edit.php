<script type="text/javascript">

    var modalLink_editJob = {};

    $(function() {

        $('#modal_editJob').appendTo('#page');

        // ADD / EDIT JOB LINK CLICK
        $('#page').on('click', '.modalLink-editJob', function(e) {
            e.preventDefault();
            modalLink_editJob = $(this);
            var ji = $(this).data('jobid');
            var jd = $(this).data('jobdata');
            if (ji) {
                $('#modal_editJob .modal-title').text('Edit Job');
            } else {
                $('#modal_editJob .modal-title').text('Create Job');
            }
            modalEditJob({
                id: $(this).data('jobid'),
                data: (jd ? JSON.parse(window.atob($(this).data('jobdata'))) : '{}'),
            });
        });

        // SAVE
        $('body').on('click', '#modal_editJob .btn.save', function(e) {
            $('#modal_editJob form .alert').hide();
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/jobs/process/', $('#modal_editJob form').serialize(), function(data) {
                if (data.success) {
                    if (typeof modalLink_editJob.removeData == 'function') {
                        modalLink_editJob.removeData('jobid').attr('data-jobid', data.id);
                    }
                    $('#modal_editJob .modal-title').text('Edit Job');
                    $('#modal_editJob form .alert strong').html('Job saved!');
                    $('#modal_editJob form .alert').removeClass('alert-danger').addClass('alert-success').show();
                } else {
                    $('#modal_editJob form .alert strong').html((data.error ? data.error : 'Failed to create job.'));
                    $('#modal_editJob form .alert').removeClass('alert-success').addClass('alert-danger').show();
                }
                if (typeof modalEditJob_saveCallback == 'function') {
                    modalEditJob_saveCallback(data);
                }
            }, 'json');
        });

    });

    if (typeof modalEditJob !== 'function') {

        function modalEditJob(params, options, callback) {
            if (typeof params != 'object') params = {};
            $('#modal_editJob').modal('show');
            $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/jobs/edit/', {
                id: params.id,
                channels: params.data.channels,
            }, function(data) {
                $('#modal_editJob .modal-body .data').html(data);
                if (typeof callback == 'function') {
                    callback(data);
                }
            }, 'html');
        }

    }

</script>

<div class="modal fade" id="modal_editJob" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo ($el_settings['job']['id'] ? 'Edit' : 'Create'); ?> Job</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="save btn btn-primary" <? /*disabled="disabled"*/ ?>>Save</button>
            </div>
        </div>
    </div>
</div>