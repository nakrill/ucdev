<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - Job Routes';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Jobs', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => 'Routes', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2]. '/' .$bigtree['path'][3] ];

/*
// ROLES
$roles = $_uccms_staff->getRoles();

// STAFF
$staffs = $_uccms_staff->getStaffs();
*/

?>

<style type="text/css">

    #routes .colRoutes .loading {
        padding: 15px;
        text-align: center;
    }
    #routes .colRoutes .loading i {
        margin-right: 3px;
    }

    #routes .colRight label:first-child {
        font-weight: 500;
    }
    #routes .colRight .map {
        /*margin-top: 22px;*/
    }
    #routes .colRight .mapContainer {
        display: block;
        width: 100%;
        height: 100vh;
        border: 0px none;
    }

</style>

<script type="text/javascript">

    $(function() {

        getRoutes();

    });

    function getRoutes(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/jobs/routes/', $('#routes').serialize(), function(data) {
            $('#routes .colRoutes > .loading').hide();
            $('#routes .colRoutes > .data').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<form id="routes">

    <input type="hidden" name="date" value="<?php echo ($_REQUEST['date'] ? date('Y-m-d', strtotime($_REQUEST['date'])) : date('Y-m-d')); ?>" />
    <input type="hidden" name="view" value="week" />

    <div class="row">

        <div class="colRoutes col-md-10">

            <div class="loading">
                <i class="fas fa-spinner fa-spin"></i> Loading..
            </div>

            <div class="data">

            </div>

        </div>

        <div class="colRight col-md-2">

            <? /*
            <div class="option shift">
                <input type="hidden" name="shift" value="" />
                <div class="form-group">
                    <label>Shift</label>
                    <select name="shift_id" class="custom_control form-control">
                        <option value="">All</option>
                        <?php foreach ($_uccms_staff->getShifts() as $shift) { ?>
                            <option value="<?php echo $shift['id']; ?>"><?php echo stripslashes($shift['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="option role">
                <input type="hidden" name="role" value="" />
                <div class="form-group">
                    <label>Role</label>
                    <select name="role_id" class="custom_control form-control">
                        <option value="">All</option>
                        <?php foreach ($_uccms_staff->getRoles() as $role) { ?>
                            <option value="<?php echo $role['id']; ?>"><?php echo stripslashes($role['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="option location">
                <input type="hidden" name="location" value="" />
                <div class="form-group">
                    <label>Location</label>
                    <select name="location_id" class="custom_control form-control">
                        <option value="">All</option>
                    </select>
                </div>
            </div>

        </div>
        */ ?>

        <?php

        // GOOGLE MAPS KEY
        $google_maps_key = $cms->getSetting('google_maps_key');
        if ($google_maps_key) {
            ?>

            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_maps_key; ?>"></script>
            <script src="/js/lib/jquery.googlemap.js"></script>

            <script type="text/javascript">

                $(document).ready(function() {

                    // MAP
                    $('#routes .colRight .mapContainer').googleMap({
                        zoom: 10,
                        overviewMapControl: true,
                        streetViewControl: true,
                        scrollwheel: true,
                        mapTypeControl: true
                    });

                });

            </script>

            <div class="map">
                <div class="mapContainer"></div>
            </div>

            <?php
        }

        ?>

    </div>

</form>

<div class="modal fade" id="modal-editRoute" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog mw-100 w-50" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Route</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="save btn btn-primary" disabled="disabled">Save</button>
            </div>
        </div>
    </div>
</div>