<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - Jobs';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Jobs', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// MODAL - EDIT JOB
include_once(SERVER_ROOT. 'extensions/' .$_uccms_staff->Extension. '/modules/elements/jobs/modal_add-edit.php');

?>

<style type="text/css">

    #jobs .loading, #jobs .no-results {
        padding: 10px;
        text-align: center;
        font-size: .9em;
    }

    #jobs .colJobs .table > .summary {
        padding-top: 0px;
    }

    #jobs .colJobs .jobs-list .items {
        margin-top: 5px;
    }
    #jobs .colJobs .jobs-list .items .date {
        opacity: .8;
    }
    #jobs .colJobs .jobs-list .items .item:hover .date {
        opacity: 1;
    }

    #jobs .colRight label:first-child {
        font-weight: 500;
    }
    #jobs .colRight .map {
        margin-top: 22px;
    }
    #jobs .colRight .mapContainer {
        display: block;
        width: 100%;
        height: 100vh;
        border: 0px none;
    }

</style>

<script type="text/javascript">

    $(function() {

        getJobList();

        // FILTER - KEYWORD
        $('#jobs .colJobs .summary .filter input[name="q"]').keyup(function(e) {
            delay(function() {
                getJobList($('form#jobs').serialize());
            }, 400);
        });

        // STATUS CLICK
        $('#jobs .colJobs .summary .filter .status a').click(function(e) {
            $('#jobs .colJobs .summary .filter .status a').removeClass('active');
            $(this).addClass('active');
            $('form#jobs input[name="status"]').val($(this).data('id'));
            getJobList($('form#jobs').serialize());
        });

        // PAGING CLICK
        $('#jobs .colJobs .summary .view_paging').on('click', 'a', function(e) {
            $('form#jobs input[name="page"]').val($(this).attr('href').replace('#', ''));
            getJobList($('form#jobs').serialize());
        });

        // RIGHT COLUMN INPUT CHANGE
        $('#jobs .colRight input, #jobs .colRight select').change(function(e) {
            getJobList($('form#jobs').serialize());
        });

    });

    // GET THE JOB LIST
    function getJobList(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/jobs/list/', params, function(data) {
            $('#jobs .colJobs .jobs-list').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

    // MODAL - EDIT JOB - SAVE CALLBACK
    function modalEditJob_saveCallback(data) {
        if (data.success) {
            getJobList($('form#jobs').serialize());
        }
    }

</script>

<form id="jobs">

    <input type="hidden" name="view" value="week" />

    <input type="hidden" name="page" value="" />
    <input type="hidden" name="status" value="" />

    <div class="row">

        <div class="colJobs col-md-10">

            <div class="table">

                <div class="summary clearfix">

                    <div class="filter">

                        <div class="control search">
                            <input type="text" name="q" value="" class="form-control" placeholder="Search" />
                        </div>

                        <div class="control status links">
                            <a href="#" data-id="" class="active">All</a>
                            <?php foreach ($_uccms_staff->jobStatuses() as $status_id => $status) { ?>
                                <a href="#" data-id="<?php echo $status_id; ?>"><?php echo $status['title']; ?></a>
                            <?php } ?>
                        </div>

                    </div>

                    <div class="paging ajax">
                        <nav class="view_paging"></nav>
                    </div>

                    <a class="btn btn-primary action" href="./edit/" <? /*data-toggle="modal" data-target="#modal-editJob"*/ ?> data-id="0"><i class="fas fa-plus"></i>Add job</a>

                </div>

                <div class="jobs-list table-responsive">
                    <div class="loading">Loading..</div>
                </div>

            </div>

        </div>

        <div class="colRight col-md-2">

            <div class="option staff">
                <div class="form-group">
                    <label>Staff</label>
                    <select name="staff_id" class="custom_control form-control">
                        <option value="">All</option>
                        <?php foreach ($_uccms_staff->getStaffs()['staff'] as $staff) { ?>
                            <option value="<?php echo $staff['id']; ?>"><?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="option route">
                <div class="form-group">
                    <label>Route</label>
                    <select name="route_id" class="custom_control form-control">
                        <option value="">All</option>
                        <?php foreach ($_uccms_staff->getRoutes() as $route) { ?>
                            <option value="<?php echo $route['id']; ?>"><?php echo $route['title']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <fieldset class="form-group">
                <label>Start time</label>
                <?php
                $field = array(
                    'key'       => 'start', // The value you should use for the "name" attribute of your form field
                    'value'     => '', // The existing value for this form field
                    'id'        => 'jobStart', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => []
                );
                include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                ?>
            </fieldset>

            <fieldset class="form-group">
                <label>End time</label>
                <?php
                $field = array(
                    'key'       => 'end', // The value you should use for the "name" attribute of your form field
                    'value'     => '', // The existing value for this form field
                    'id'        => 'jobEnd', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => []
                );
                include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                ?>
            </fieldset>

            <?php

            // GOOGLE MAPS KEY
            $google_maps_key = $cms->getSetting('google_maps_key');
            if ($google_maps_key) {
                ?>

                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_maps_key; ?>"></script>
                <script src="/js/lib/jquery.googlemap.js"></script>

                <script type="text/javascript">

                    $(document).ready(function() {

                        // MAP
                        $('#jobs .colRight .mapContainer').googleMap({
                            zoom: 10,
                            overviewMapControl: true,
                            streetViewControl: true,
                            scrollwheel: true,
                            mapTypeControl: true
                        });

                    });

                </script>

                <div class="map">
                    <div class="mapContainer"></div>
                </div>

                <?php
            }

            ?>

        </div>

    </div>

</form>