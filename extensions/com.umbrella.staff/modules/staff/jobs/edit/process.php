<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['id'];

    // SAVE JOB
    $result = $_uccms_staff->saveJob($_POST['job']);

    if ($result['success']) {
        $admin->growl('Staff', 'Job updated!');
    } else {
        $admin->growl('Staff', ($result['error'] ? $result['error'] : 'Failed to save job.'));
    }

}

BigTree::redirect(MODULE_ROOT.'jobs/edit/?id=' .$result['id']);

?>