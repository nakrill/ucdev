<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

$job = [];

// ID SPECIFIED
if ($id) {

    // GET JOB
    $job = $_uccms_staff->getJob($id);

    // JOB FOUND
    if ($job['id']) {

        // GET JOB STAFF
        $job['staff'] = $_uccms_staff->jobStaff($job['id']);

    }

}

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - ' .($job['id'] ? 'Edit' : 'Add'). ' Job';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Jobs', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => ($job['id'] ? 'Edit' : 'Add'), 'link' => '' ];

// GET ROLES
$roles = $_uccms_staff->getRoles();

?>

<style type="text/css">

    #form_job .staff .form-check {
        line-height: 26px;
    }
    #form_job .staff select {
        display: inline-block;
        width: auto;
        height: auto;
        margin-left: 10px;
        padding: 0 5px;
        vertical-align: middle;
        line-height: unset;
        font-size: 14px;
    }

</style>

<script type="text/javascript">

    $(function() {


    });

</script>

<div class="container legacy">

    <form id="form_job" enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="job[id]" value="<?php echo $job['id']; ?>" />

    <header>
        <h2><?php if ($job['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Job</h2>
    </header>

    <section>

        <div class="row">

            <div class="col-md-6">

                <fieldset class="form-group">
                    <label>Title</label>
                    <input type="text" name="job[title]" value="<?php echo stripslashes($job['title']); ?>" class="form-control" />
                </fieldset>

                <div class="row">

                    <div class="col-md-6">

                        <fieldset class="form-group">
                            <label>Start time</label>
                            <?php
                            $field = array(
                                'key'       => 'job[start]', // The value you should use for the "name" attribute of your form field
                                'value'     => ($job['start'] ? $job['start'] : ''), // The existing value for this form field
                                'id'        => 'jobStart', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => []
                            );
                            include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                            ?>
                        </fieldset>

                    </div>

                    <div class="col-md-6">

                        <fieldset class="form-group">
                            <label>End time</label>
                            <?php
                            $field = array(
                                'key'       => 'job[end]', // The value you should use for the "name" attribute of your form field
                                'value'     => ($job['end'] ? $job['end'] : ''), // The existing value for this form field
                                'id'        => 'jobEnd', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => []
                            );
                            include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                            ?>
                        </fieldset>

                    </div>

                </div>

            </div>

            <div class="col-md-6">

                <fieldset class="form-group">
                    <label>Status</label>
                    <select name="job[status]" class="custom_control form-control">
                        <?php
                        foreach ($_uccms_staff->jobStatuses() as $status_id => $status) {
                            if ($status_id != 9) {
                                ?>
                                <option value="<?php echo $status_id; ?>" <?php if ($status_id == $job['status']) { ?>selected="selected"<?php } ?>><?php echo $status['title']; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </fieldset>

            </div>

        </div>

        <div class="card staff">
            <div class="card-body">
                <h5 class="card-title">Staff</h5>
                <div>
                    <?php foreach ($_uccms_staff->getStaffs()['staff'] as $staff) { ?>
                        <div class="form-check">
                            <input class="form-check-input custom_control" type="checkbox" name="job[staff][]" value="<?php echo $staff['id']; ?>" id="jobStaff-<?php echo $staff['id']; ?>" <?php if ($job['staff'][$staff['id']]) { ?>checked="checked"<?php } ?> />
                            <label class="form-check-label" for="jobStaff-<?php echo $staff['id']; ?>">
                                <?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?>
                                <?php if (count((array)$staff['roles']) > 0) { ?>
                                    <select name="job[staff_roles][<?php echo $staff['id']; ?>]" class="custom_control form-control">
                                        <?php foreach ($staff['roles'] as $role) { ?>
                                            <option value="<?php echo $role['role_id']; ?>" <?php if ($role['role_id'] == $job['staff'][$staff['id']]['role_id']) { ?>selected="selected" <?php } ?>><?php echo stripslashes($roles[$role['role_id']]['title']); ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </section>

    <footer>
        <input class="btn btn-primary" type="submit" value="Save" />
    </footer>

    </form>

</div>

<?php

// HAVE JOB ID
if ($job['id']) {

    ?>


    <?php

}

?>

<? //include BigTree::path("admin/layouts/_html-field-loader.php") ?>