<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE STAFF
    $query = "UPDATE `" .$_uccms_staff->tables['staff']. "` SET " .uccms_createSet($columns). ", `dt_deleted`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Staff', 'Deleted');

    // QUERY FAILED
    } else {
        $admin->growl('Staff', 'Failed to delete');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Staff', 'No ad specified');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT.'staff/');
}

?>