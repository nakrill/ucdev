<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - Staff';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Staff', 'link' => $bigtree['path'][2]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #staff .loading, #staff .no-results {
        padding: 10px;
        text-align: center;
        font-size: .9em;
    }

</style>

<script type="text/javascript">

    $(function() {

        getStaffList();

        // FILTER - KEYWORD
        $('#staff .summary .filter input[name="q"]').keyup(function(e) {
            delay(function() {
                getStaffList($('#staff .summary .filter form').serialize());
            }, 400);
        });

        // STATUS CLICK
        $('#staff .summary .filter .status a').click(function(e) {
            $('#staff .summary .filter .status a').removeClass('active');
            $(this).addClass('active');
            $('#staff .summary .filter input[name="status"]').val($(this).data('id'));
            getStaffList($('#staff .summary .filter form').serialize());
        });

        // PAGING CLICK
        $('#staff .summary .view_paging').on('click', 'a', function(e) {
            $('#staff .summary .filter input[name="page"]').val($(this).attr('href').replace('#', ''));
            getStaffList($('#staff .summary .filter form').serialize());
        });

    });

    // GET THE STAFF LIST
    function getStaffList(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/staff/list/', params, function(data) {
            $('#staff .staff-list').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<div id="staff" class="table">

    <div class="summary clearfix">

        <div class="filter">
            <form>
            <input type="hidden" name="page" value="" />
            <input type="hidden" name="status" value="" />

            <div class="control search">
                <input type="text" name="q" value="" class="form-control" placeholder="Search" />
            </div>

            <div class="control status links">
                <a href="#" data-id="" class="active">All</a>
                <?php foreach ($_uccms_staff->staffStatuses() as $status_id => $status) { ?>
                    <a href="#" data-id="<?php echo $status_id; ?>"><?php echo $status['title']; ?></a>
                <?php } ?>
            </div>

            </form>
        </div>

        <div class="paging ajax">
            <nav class="view_paging"></nav>
        </div>

        <a class="btn btn-primary action" href="./edit/" <? /*data-toggle="modal" data-target="#modal-editStaff"*/ ?> data-id="0"><i class="fas fa-plus"></i>Add staff</a>

    </div>

    <div class="staff-list table-responsive">
        <div class="loading">Loading..</div>
    </div>

</div>