<?php

// HAVE ID
if ($_GET['id']) {

    // DELETE RECORD
    $delete_query = "DELETE FROM `" .$_uccms_staff->tables['time_tracking']. "` WHERE (`id`=" .(int)$_GET['id']. ")";
    if (sqlquery($delete_query)) {
        $admin->growl('Time Tracking', 'Record deleted');
    } else {
        $admin->growl('Time Tracking', 'Failed to delete time record');
    }

// NOT SHIPMENT ID
} else {
    $admin->growl('Time Tracking', 'ID not specified');
}

BigTree::redirect(MODULE_ROOT. 'staff/edit/?id=' .$_GET['staff_id']. '#time_tracking');

?>