<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    $success = false;

    // CLEAN UP
    $id         = (int)$_POST['staff']['id'];
    $password   = trim($_POST['staff']['password']);

    // DB COLUMNS
    $columns = array(
        'status'            => (int)$_POST['staff']['status'],
        'bigtree_user_id'   => (int)$_POST['staff']['bigtree_user_id'],
        'firstname'         => $_POST['staff']['firstname'],
        'lastname'          => $_POST['staff']['lastname'],
        'email'             => $_POST['staff']['email'],
        'phone'             => $_POST['staff']['phone'],
        'rate_hourly'       => number_format((float)$_POST['staff']['rate_hourly'], 2, '.', ''),
    );

    // HAVE ID
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_staff->tables['staff']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";
        if (sqlquery($query)) {

            $admin->growl('Staff', 'Updated!');

            // HAVE BIGTREE USER ID
            if ($columns['bigtree_user_id']) {

                $bigtree_user_columns = array(
                    'email' => $columns['email'],
                    'name'  => trim($columns['firstname']. ' ' .$columns['lastname'])
                );

                // UPDATE BIGTREE USER
                $query = "UPDATE `bigtree_users` SET " .uccms_createSet($bigtree_user_columns). " WHERE (`id`=" .$columns['bigtree_user_id']. ")";
                sqlquery($query);

                // PASSWORD SPECIFIED
                if ($password) {
                    $admin->updateUserPassword($columns['bigtree_user_id'], $password);
                }

            }

        } else {
            $admin->growl('Staff', 'Failed to update.');
            BigTree::redirect(MODULE_ROOT.'staff/edit/?id=' .$id);
        }

    // NO ID
    } else {

        $revsocial_account_id = 0;

        // BIGTREE USER ID SPECIFIED
        if ($columns['bigtree_user_id']) {

            // MAKE SURE NO OTHER STAFF HAVE THIS BIGTREE_USER_ID ALREADY
            $user_used_query = "SELECT * FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`bigtree_user_id`=" .$columns['bigtree_user_id']. ") AND (`status`!=9) AND (`dt_deleted`='0000-00-00 00:00:00')";
            $user_used_q = sqlquery($user_used_query);
            $user_used = sqlfetch($user_used_q);

            // USER USED
            if ($user_used['id']) {
                $admin->growl('Staff', 'Failed to add, user already assigned to another staff.');
                BigTree::redirect(MODULE_ROOT.'staff/edit/');
            }

            // GET BIGTREE USER
            $user_query = "SELECT * FROM `bigtree_users` WHERE (`id`=" .$columns['bigtree_user_id']. ")";
            $user_q = sqlquery($user_query);
            $user = sqlfetch($user_q);

            // USER FOUND
            if ($user['id']) {

                $revsocial_account_id = $user['revsocial_account_id'];

                if ((!$columns['firstname']) && (!$columns['lastname'])) {
                    $nameparts = explode(' ', stripslashes($user['name']), 2);
                    $columns['firstname'] = $nameparts[0];
                    $columns['lastname'] = $nameparts[1];
                }

                if (!$columns['email']) $columns['email'] = $user['email'];

            // USER NOT FOUND
            } else {
                $columns['bigtree_user_id'] = 0;
            }

        }

        /*
        echo 'rsaid: ' .$revsocial_account_id;
        echo '<br />';
        echo 'buid: ' .$columns['bigtree_user_id'];
        exit;
        */

        // NO REVSOCIAL ACCOUNT ID
        if (!$revsocial_account_id) {

            // REVSOCIAL API KEY
            $api_key = $cms->getSetting('rs_api-key');

            // HAVE API KEY
            if ($api_key) {

                // REST CLIENT
                require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

                // INIT REST API CLASS
                $_api = new RestClient(array(
                    'base_url' => 'https://api.revsocial.com/v1'
                ));

                // MAKE API CALL
                $result = $_api->post('account/create', array(
                    'api_key'   => $api_key,
                    'data'      => array(
                        'email'     => $columns['email'],
                        'password'  => $password,
                        'firstname' => $columns['firstname'],
                        'lastname'  => $columns['lastname']
                    )
                ));

                // API RESPONSE
                $resp = $result->decode_response();

                //echo print_r($resp);
                //exit;

                if (($resp->success) && ($resp->account_id)) {
                    $revsocial_account_id = $resp->account_id;
                }

            }

        }

        // NO BIGTREE USER ID
        if (!$columns['bigtree_user_id']) {

            // GET STAFF MODULE
            $module_query = "SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$_uccms_staff->Extension. "')";
            $module_q = sqlquery($module_query);
            $module = sqlfetch($module_q);

            // MODULE FOUND
            if ($module['id']) {

                $permissions = array(
                    'page' => array(),
                    'module' => array(
                        $module['id'] => 'e'
                    ),
                    'resources' => array(),
                    'module_gbp' => array()
                );

            }

            // CREATE BIGTREE USER
            $columns['bigtree_user_id'] = (int)$admin->createUser(array(
                'level'                 => 0,
                'email'                 => $columns['email'],
                'password'              => $password,
                'name'                  => trim($columns['firstname']. ' ' .$columns['lastname']),
                'permissions'           => $permissions
            ));

        }

        // BIGTREE USER ID
        if ($columns['bigtree_user_id']) {

            // HAVE REVSOCIAL ACCOUNT ID
            if ($revsocial_account_id) {

                // UPDATE BIGTREE USER
                $query = "UPDATE `bigtree_users` SET `revsocial_account_id`=" .$revsocial_account_id. " WHERE (`id`=" .$columns['bigtree_user_id']. ")";
                sqlquery($query);

            }

            // PASSWORD SPECIFIED
            if ($password) {
                $admin->updateUserPassword($columns['bigtree_user_id'], $password);
            }

            // CREATE STAFF
            $query = "INSERT INTO `" .$_uccms_staff->tables['staff']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW()";

            // STAFF RECORD CREATED
            if (sqlquery($query)) {

                $admin->growl('Staff', 'Created!');

                // NEW ID
                $id = sqlid();

            // FAILED
            } else {
                $admin->growl('Staff', 'Failed to create.');
                BigTree::redirect(MODULE_ROOT.'staff/edit/');
            }

        }

    }

    // HAVE ID
    if ($id) {

        // GET CURRENT ROLES FOR STAFF
        $staff_roles = $_uccms_staff->staffRoles($id);

        // HAVE ROLES
        if (is_array($_REQUEST['staff']['roles'])) {

            foreach ($_REQUEST['staff']['roles'] as $role_id) {

                $role_id = (int)$role_id;

                // ALREADY ASSOCIATED
                if ($staff_roles[$role_id]['id']) {
                    unset($staff_roles[$role_id]);

                // ADD MISSING
                } else {

                    $rel_columns = [
                        'staff_id'  => $id,
                        'role_id'   => $role_id,
                    ];

                    // CREATE RECORD
                    $rel_sql = "INSERT INTO `" .$_uccms_staff->tables['staff_roles']. "` SET " .$_uccms_staff->createSet($rel_columns);
                    sqlquery($rel_sql);

                }

            }

        }

        // DELETE OLD
        if (count($staff_roles) > 0) {
            foreach ($staff_roles as $role) {
                $del_sql = "DELETE FROM `" .$_uccms_staff->tables['staff_roles']. "` WHERE (`staff_id`=" .$id. ") AND (`role_id`=" .$role['id']. ")";
                sqlquery($del_sql);
            }
        }

        // GET CURRENT ROUTES FOR STAFF
        $staff_routes = $_uccms_staff->staffRoutes($id);

        // HAVE ROUTES
        if (is_array($_REQUEST['staff']['routes'])) {

            foreach ($_REQUEST['staff']['routes'] as $route_id) {

                $route_id = (int)$route_id;

                // ALREADY ASSOCIATED
                if ($staff_routes[$route_id]['id']) {
                    unset($staff_routes[$route_id]);

                // ADD MISSING
                } else {

                    $rel_columns = [
                        'staff_id'  => $id,
                        'route_id'   => $route_id,
                    ];

                    // CREATE RECORD
                    $rel_sql = "INSERT INTO `" .$_uccms_staff->tables['staff_routes']. "` SET " .$_uccms_staff->createSet($rel_columns);
                    sqlquery($rel_sql);

                }

            }

        }

        // DELETE OLD
        if (count($staff_routes) > 0) {
            foreach ($staff_routes as $route) {
                $del_sql = "DELETE FROM `" .$_uccms_staff->tables['staff_routes']. "` WHERE (`staff_id`=" .$id. ") AND (`route_id`=" .$route['id']. ")";
                sqlquery($del_sql);
            }
        }

        // GET CURRENT SHIFTS FOR STAFF
        $staff_shifts = $_uccms_staff->staffShifts($id);

        // HAVE SHIFTS
        if (is_array($_REQUEST['staff']['shifts'])) {

            foreach ($_REQUEST['staff']['shifts'] as $shift_id) {

                $shift_id = (int)$shift_id;

                // ALREADY ASSOCIATED
                if ($staff_shifts[$shift_id]['id']) {

                    $rel_columns = [
                        'role_id'   => (int)$_REQUEST['staff']['shift_roles'][$shift_id],
                    ];

                    // UPDATE RECORD
                    $rel_sql = "UPDATE `" .$_uccms_staff->tables['staff_shifts']. "` SET " .$_uccms_staff->createSet($rel_columns). " WHERE (`id`=" .$staff_shifts[$shift_id]['id']. ")";
                    sqlquery($rel_sql);

                    unset($staff_shifts[$shift_id]);

                // ADD MISSING
                } else {

                    $rel_columns = [
                        'staff_id'  => $id,
                        'shift_id'  => $shift_id,
                        'role_id'   => (int)$_REQUEST['staff']['shift_roles'][$shift_id],
                    ];

                    // CREATE RECORD
                    $rel_sql = "INSERT INTO `" .$_uccms_staff->tables['staff_shifts']. "` SET " .$_uccms_staff->createSet($rel_columns);
                    sqlquery($rel_sql);

                }

            }

        }

        // DELETE OLD
        if (count($staff_shifts) > 0) {
            foreach ($staff_shifts as $shift) {
                $del_sql = "DELETE FROM `" .$_uccms_staff->tables['staff_shifts']. "` WHERE (`id`=" .$shift['id']. ")";
                sqlquery($del_sql);
            }
        }

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['image']['name']) || ((!$_POST['image']) || (substr($_POST['image'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `image` FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`id`=" .$id. ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['image']) {

                // REMOVE IMAGE
                @unlink($_uccms_staff->imageBridgeOut($ex['image'], 'staff', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_staff->tables['staff']. "` SET `image`='' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['image']['name']) || ($_POST['image'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = [
                'type'      => 'upload',
                'title'     => 'Image',
                'key'       => 'image',
                'options'   => [
                    'directory'     => 'extensions/' .$_uccms_staff->Extension. '/files/staff',
                    'image'         => true,
                    'min_width'     => 100,
                    'min_height'    => 100,
                    'thumbs' => [
                        array(
                            //'prefix'    => 't_',
                            'width'     => '600', // MATTD: make controlled through settings
                            'height'    => '600' // MATTD: make controlled through settings
                        )
                    ],
                    'center_crops' => [
                        array(
                            'prefix'    => 'a_',
                            'width'     => '100', // MATTD: make controlled through settings
                            'height'    => '100' // MATTD: make controlled through settings
                        )
                    ]
                ]
            ];

            // UPLOADED FILE
            if ($_FILES['image']['name']) {
                $field['file_input'] = $_FILES['image'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['image']) {
                $field['input'] = $_POST['image'];
            }

            // DIGITAL ASSET MANAGER VARS
            $field['dam_vars'] = array(
                'website_extension'         => $_uccms_staff->Extension,
                'website_extension_item'    => 'staff',
                'website_extension_item_id' => $id,
                'title'                     => $_POST['staff']['title'],
                'description'               => $_POST['staff']['description'],
                'link'                      => $_POST['staff']['link']
            );

            $keywords = array();
            if ($_POST['staff']['keywords']) {
                $ka = explode(',', $_POST['staff']['keywords']);
                foreach ($ka as $kw) {
                    $kwc = trim($kw);
                    if ($kwc) {
                        $keywords[] = $kwc;
                    }
                }
            }
            if (count($keywords) > 0) {
                $field['dam_vars']['keywords'] = $keywords;
            }

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_staff->tables['staff']. "` SET `image`='" .sqlescape($_uccms_staff->imageBridgeIn($file_path)). "' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                echo print_r($bigtree['errors']);
                exit;

                $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

            }

        }

    }

}

BigTree::redirect(MODULE_ROOT.'staff/edit/?id=' .$id);

?>