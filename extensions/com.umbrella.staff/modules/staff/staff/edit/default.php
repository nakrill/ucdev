<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];
$bigtree_user_id = (int)$_REQUEST['user_id'];

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Staff', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => ($id ? 'Edit' : 'Add'), 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2]. '/' .$bigtree['path'][3] ];

// ID SPECIFIED
if ($id) {

    // GET STAFF INFO
    $staff = $_uccms_staff->getStaff($id);
    /*
    $staff_query = "SELECT * FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`id`=" .$id. ")";
    $staff_q = sqlquery($staff_query);
    $staff = sqlfetch($staff_q);
    */

    // HAVE BIGTREE USER ID
    if ($staff['bigtree_user_id']) {

        // GET BIGTREE USER
        $bigtree_user_query = "SELECT * FROM `bigtree_users` WHERE (`id`=" .$staff['bigtree_user_id']. ")";
        $bigtree_user_q = sqlquery($bigtree_user_query);
        $bigtree_user = sqlfetch($bigtree_user_q);

    }

// NO ID SPECIFIED
} else {

    $bigtree_users = array();
    $staff_users = array();

    // BIGTREE USER ID SPECIFIED
    if ($bigtree_user_id) {

        // GET BIGTREE USER
        $bigtree_user_query = "SELECT * FROM `bigtree_users` WHERE (`id`=" .$bigtree_user_id. ")";
        $bigtree_user_q = sqlquery($bigtree_user_query);
        $bigtree_user = sqlfetch($bigtree_user_q);

        if ($bigtree_user['id']) {

            // SPLIT NAME
            $name = $_uccms_staff->nameSplit(stripslashes($bigtree_user['name']));

            // STAFF INFO
            $staff = array(
                'status'            => 1,
                'bigtree_user_id'   => $bigtree_user['id'],
                'firstname'         => $name[0],
                'lastname'          => $name[1],
                'email'             => $bigtree_user['email'],
            );

        } else {

            $admin->growl('User', 'Failed to get user details.');
            BigTree::redirect(MODULE_ROOT.'staff/edit/');

        }

    } else {

        $staff['status'] = 1;

        // GET ALL BIGTREE USERS
        $bigtree_users_query = "SELECT * FROM `bigtree_users` ORDER BY `name` ASC";
        $bigtree_users_q = sqlquery($bigtree_users_query);
        while ($bigtree_user = sqlfetch($bigtree_users_q)) {
            $bigtree_users[$bigtree_user['id']] = $bigtree_user;
            /*
            if ($bigtree_user['revsocial_account_id']) {
                $rsuua[$bigtree_user['revsocial_account_id']] = $bigtree_user['id'];
            }
            */
        }

        // GET ALL STAFF
        $staff_users_query = "SELECT * FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`status`!=9)";
        $staff_users_q = sqlquery($staff_users_query);
        while ($staff_user = sqlfetch($staff_users_q)) {
            //$staff_users[$staff_user['id']] = $staff_user;
            /*
            if ($bigtree_user['revsocial_account_id']) {
                $rsuua[$bigtree_user['revsocial_account_id']] = $bigtree_user['id'];
            }
            */
            $staff_users[$staff_user['bigtree_user_id']] = $staff_user['id'];
        }

    }

}

// GET ROLES
$roles = $_uccms_staff->getRoles(false);

// GET ROUTES
$routes = $_uccms_staff->getRoutes(false);

?>

<style type="text/css">

    #staff_select-user .users {
        margin-top: 20px;
    }
    #staff_select-user .users .user {
        text-align: left;
        font-size: .9em;
        cursor: pointer;
    }
    #staff_select-user .users .user:hover {
        background-color: #43b4ae1c;
    }
    #staff_select-user .users .user.disabled {
        cursor: default;
        background-color: transparent;
    }
    #staff_select-user .users .user i {
        margin-right: 5px;
    }
    #staff_select-user .users .user .fas {
        color: #43b4ae;
    }
    #staff_select-user .users .user .email {
        opacity: .7;
    }
    #staff_select-user .users .user .badge {
        float: right;
    }

    #form_staff .shifts .form-check {
        line-height: 28px;
    }
    #form_staff .shifts select {
        display: inline-block;
        width: auto;
        height: auto;
        margin-left: 10px;
        padding: 0 5px;
        line-height: unset;
        font-size: 14px;
    }

</style>

<script type="text/javascript">

    $(function() {

        // HOVER
        $('#staff_select-user .user:not(.disabled)').hover(function() {
            $(this).find('i').removeClass('far').addClass('fas');
        }, function() {
            $(this).find('i').removeClass('fas').addClass('far');
        });

        // CREATE CLICK
        $('#staff_create-select .create').click(function(e) {
            e.preventDefault();
            $('#staff_create-select').hide();
            $('#staff_select-user').hide();
            $('#staff_main-info').show();
            $('#form_staff footer input[type="submit"]').show();
        });

        // ROLE CHECK
        $('#form_staff .roles .role input[type="checkbox"]').click(function(e) {
            var roles = {};
            $('#form_staff .roles .role input[type="checkbox"]').each(function(i, v) {
                if ($(this).prop('checked')) {
                    roles[$(this).val()] = $(this).next('label').text().trim();
                }
            });
            $('#form_staff .shifts .shift select').each(function(i, v) {
                var $this = $(this);
                var $val = $this.val();
                if (Object.keys(roles).length > 0) {
                    $this.empty();
                    $this.append($('<option></option>').attr('value', 0).text('None'));
                    $.each(roles, function(key, value) {
                        $this.append($('<option></option>').attr('value', key).text(value));
                    });
                    if ($this.closest('.shift').find('input[type="checkbox"]').prop('checked')) {
                        $this.val($val).show();
                    } else {
                        $this.hide();
                    }
                } else {
                    $this.hide();
                }
            });
        });

        // SHIFT CHECK
        $('#form_staff .shifts .shift input[type="checkbox"]').click(function(e) {
            if ($(this).prop('checked')) {
                $(this).closest('.shift').find('select').show();
            } else {
                $(this).closest('.shift').find('select').hide();
            }
        });

    });
</script>

<div class="container">

    <form id="form_staff" enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="staff[id]" value="<?php echo $staff['id']; ?>" />

    <header>
        <h2><?php if ($staff['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Staff</h2>
        <?php if ($bigtree_user['id']) { ?>
            <a href="<?php echo ADMIN_ROOT; ?>/users/edit/<?php echo $bigtree_user['id']; ?>/" class="button">Edit User</a>
        <?php } ?>
    </header>

    <section>

        <?php if ($bigtree_user['id']) { ?>

            <input type="hidden" name="staff[bigtree_user_id]" value="<?php echo $staff['bigtree_user_id']; ?>" />

        <?php } else { ?>

            <div id="staff_create-select" style="text-align: center;">
                Select an existing account below or <a href="#" class="create">continue with a new one</a>.
            </div>

            <div id="staff_select-user">

                <div class="users list-group">

                    <?php

                    // LOOP
                    foreach ($bigtree_users as $user) {

                        $avail = !(bool)$staff_users[$user['id']];

                        ?>

                        <button type="submit" name="staff[bigtree_user_id]" value="<?php echo $user['id']; ?>" class="user list-group-item clearfix <?php echo ($avail ? '' : 'disabled'); ?>">
                            <i class="far fa-circle"></i>
                            <span class="name"><?php echo stripslashes($user['name']); ?> <span class="email">(<?php echo $user['email']; ?>)</span></span>
                            <span class="badge badge-<?php echo ($avail ? 'success' : 'secondary'); ?> badge-pill"><?php echo ($avail ? 'Available' : 'Taken'); ?></span>
                        </button>

                        <?php

                    }

                    ?>

                </div>

            </div>

        <?php } ?>

        <div id="staff_main-info" <?php if ((!$id) && (!$bigtree_user_id)) { ?>style="display: none;"<?php } ?>>

            <div class="row">

                <div class="col-md-6">

                    <fieldset class="form-group">
                        <label>First Name</label>
                        <input type="text" name="staff[firstname]" value="<?php echo stripslashes($staff['firstname']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="staff[lastname]" value="<?php echo stripslashes($staff['lastname']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Email</label>
                        <input type="text" name="staff[email]" value="<?php echo stripslashes($staff['email']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Phone</label>
                        <input type="text" name="staff[phone]" value="<?php echo stripslashes($staff['phone']); ?>" class="form-control" />
                    </fieldset>

                </div>

                <div class="col-md-6">

                    <fieldset class="status form-group">
                        <label>Status</label>
                        <select name="staff[status]" class="custom_control form-control">
                            <?php
                            foreach ($_uccms_staff->staffStatuses() as $status_id => $status) {
                                if ($status_id != 9) {
                                    ?>
                                    <option value="<?php echo $status_id; ?>" <?php if ($status_id == $staff['status']) { ?>selected="selected"<?php } ?>><?php echo $status['title']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Password</label>
                        <input type="text" name="staff[password]" value="" class="form-control" />
                        <?php if (($staff['id']) || ($bigtree_user_id)) { ?>
                            <label><small>Only enter if changing.</small></label>
                        <?php } ?>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Rate</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" name="staff[rate_hourly]" value="<?php echo number_format((float)$staff['rate_hourly'], 2, '.', ''); ?>" class="form-control" />
                            <div class="input-group-append">
                                <span class="input-group-text">/hr</span>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Image</label>
                        <div>
                            <?php
                            $bigtree['form']['embedded'] = true;
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'image', // The value you should use for the "name" attribute of your form field
                                'value'     => ($staff['image']['file'] ? $_uccms_staff->imageBridgeOut($staff['image']['file'], 'staff') : ''), // The existing value for this form field
                                'id'        => 'staff_image', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </div>
                    </fieldset>

                </div>

            </div>

            <div class="row">

                <div class="col-md-4">
                    <div class="roles card form-group">
                        <div class="card-body">
                            <h5 class="card-title">Roles</h5>
                            <div>
                                <?php foreach ($roles as $role) { ?>
                                    <div class="role form-check">
                                        <input class="form-check-input custom_control" type="checkbox" name="staff[roles][]" value="<?php echo $role['id']; ?>" id="staffRole-<?php echo $role['id']; ?>" <?php if ($staff['roles'][$role['id']]) { ?>checked="checked"<?php } ?> />
                                        <label class="form-check-label" for="staffRole-<?php echo $role['id']; ?>">
                                            <?php echo stripslashes($role['title']); ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="shifts card form-group">
                        <div class="card-body">
                            <h5 class="card-title">Shifts</h5>
                            <div>
                                <?php foreach ($_uccms_staff->getShifts(false) as $shift) { ?>

                                    <div class="shift form-check">
                                        <input class="form-check-input custom_control" type="checkbox" name="staff[shifts][]" value="<?php echo $shift['id']; ?>" id="staffShift-<?php echo $shift['id']; ?>" <?php if ($staff['shifts'][$shift['id']]) { ?>checked="checked"<?php } ?> />
                                        <label class="form-check-label" for="staffShift-<?php echo $shift['id']; ?>">
                                            <?php echo stripslashes($shift['title']); ?>
                                            <?php if (count((array)$staff['roles']) > 0) { ?>
                                                <select name="staff[shift_roles][<?php echo $shift['id']; ?>]" class="custom_control form-control" style="<?php if (!$staff['shifts'][$shift['id']]) { ?>display: none;<?php } ?>">
                                                    <option value="0">None</option>
                                                    <?php foreach ($staff['roles'] as $role) { ?>
                                                        <option value="<?php echo $role['role_id']; ?>" <?php if ($role['role_id'] == $staff['shifts'][$shift['id']]['role_id']) { ?>selected="selected" <?php } ?>><?php echo stripslashes($roles[$role['role_id']]['title']); ?></option>
                                                    <?php } ?>
                                                </select>
                                            <?php } ?>
                                        </label>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="routes card form-group">
                        <div class="card-body">
                            <h5 class="card-title">Routes</h5>
                            <div>
                                <?php foreach ($routes as $route) { ?>
                                    <div class="route form-check">
                                        <input class="form-check-input custom_control" type="checkbox" name="staff[routes][]" value="<?php echo $route['id']; ?>" id="staffRoute-<?php echo $route['id']; ?>" <?php if ($staff['routes'][$route['id']]) { ?>checked="checked"<?php } ?> />
                                        <label class="form-check-label" for="staffRoute-<?php echo $route['id']; ?>">
                                            <?php echo stripslashes($route['title']); ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" <?php if ((!$id) && (!$bigtree_user_id)) { ?>style="display: none;"<?php } ?> />
    </footer>

    </form>

</div>

<?php

// HAVE STAFF ID
if ($staff['id']) {

    ?>

    <a name="time_tracking"></a>

    <style type="text/css">

        #time_tracking .tt_dt {
            width: 150px;
            text-align: left;
        }
        #time_tracking .tt_job {
            width: 235px;
            text-align: left;
        }
        #time_tracking .tt_clock_in {
            width: 150px;
            text-align: left;
        }
        #time_tracking .tt_clock_out {
            width: 150px;
            text-align: left;
        }
        #time_tracking .tt_clock_out.active .circle {
            display: inline-block;
            margin: -2px 3px 0 0;
            width: 8px;
            height: 8px;
            background-color: #66BB6A;
            border-radius: 8px;
        }
        #time_tracking .tt_clock_out.active .title {
            text-transform: uppercase;
        }
        #time_tracking .tt_total {
            width: 150px;
            text-align: left;
        }
        #time_tracking .tt_edit {
            width: 50px;
        }
        #time_tracking .tt_delete {
            width: 50px;
        }

        #time_tracking .time_tracking ul .item {
            height: auto;
        }
        #time_tracking .time_tracking ul .item .edit {
            padding: 10px 20px 0;
            border-top: 1px dashed #ddd;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // EDIT ICON CLICK
            $('#time_tracking .time_tracking ul .item .icon_edit').click(function(e) {
                e.preventDefault();
                $(this).closest('.item').find('.edit').toggle();
            });

        });

    </script>

    <div id="time_tracking" class="container">

        <div class="time_tracking table" style="margin: 0px; border: 0px none;">

            <summary>
                <h2>Time Tracking</h2>
                <a class="add_resource add" href="../time-tracking/?staff_id=<?php echo $staff['id']; ?>"><span style="background-position: -385px -140px;"></span>View All</a>
            </summary>

            <header style="clear: both;">
                <span class="tt_dt">Date</span>
                <span class="tt_job">Job</span>
                <span class="tt_clock_in">Clock In</span>
                <span class="tt_clock_out">Clock Out</span>
                <span class="tt_total">Total Time</span>
                <span class="tt_edit">Edit</span>
                <span class="tt_delete">Delete</span>
            </header>

            <?php

            // GET TIME TRACKING
            $time_tracking_query = "SELECT * FROM `" .$_uccms_staff->tables['time_tracking']. "` WHERE (`staff_id`=" .$staff['id']. ") ORDER BY `dt_start` DESC LIMIT 10";
            $time_tracking_q = sqlquery($time_tracking_query);

            $num_time_tracking = sqlrows($time_tracking_q);

            // HAVE TIME TRACKING
            if ($num_time_tracking > 0) {

                ?>

                <ul class="items">

                    <?php

                    // LOOP
                    while ($time_tracking = sqlfetch($time_tracking_q)) {

                        ?>

                        <li class="item">

                            <div class="contain">
                                <section class="tt_dt">
                                    <?php echo date('n/j/Y', strtotime($time_tracking['dt_start'])); ?>
                                </section>
                                <section class="tt_job">

                                </section>
                                <section class="tt_clock_in">
                                    <?php echo date('g:i:s A', strtotime($time_tracking['dt_start'])); ?>
                                </section>
                                <section class="tt_clock_out active">
                                    <?php
                                    if ($time_tracking['dt_end'] == '0000-00-00 00:00:00') {
                                        ?>
                                        <span class="circle"></span> <span class="title">Active</span>
                                        <?php
                                    } else {
                                        echo date('g:i:s A', strtotime($time_tracking['dt_end']));
                                    }
                                    ?>
                                </section>
                                <section class="tt_total">
                                    <?php


                                    $diff = $_uccms_staff->dtDiff($time_tracking['dt_start'], $time_tracking['dt_end'], array(
                                        'format' => '%H:%I:%S'
                                    ));

                                    echo $diff;

                                    ?>
                                </section>
                                <section class="tt_edit">
                                    <? /*<a href="#" title="Edit" class="icon_edit"></a>*/ ?>
                                </section>
                                <section class="tt_delete">
                                    <a onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');" title="Delete" class="icon_delete" href="./processes/delete_time/?staff_id=<?php echo $staff['id']; ?>&id=<?php echo $time_tracking['id']; ?>"></a>
                                </section>
                            </div>

                            <? /*
                            <div class="edit contain" style="display: none;">

                                <form enctype="multipart/form-data" action="./processes/edit_transaction/" method="post">
                                <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
                                <input type="hidden" name="id" value="<?php echo $time_tracking['id']; ?>" />

                                <div style="width: 50%;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <?php if ($time_tracking['ip']) { ?>
                                            <tr>
                                                <td>IP:</td>
                                                <td><?php echo long2ip($time_tracking['ip']); ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($time_tracking['transaction_id']) { ?>
                                            <tr>
                                                <td>Transaction ID:</td>
                                                <td><?php echo $time_tracking['transaction_id']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($time_tracking['name']) { ?>
                                            <tr>
                                                <td>Name:</td>
                                                <td><?php echo $time_tracking['name']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($time_tracking['lastfour']) { ?>
                                            <tr>
                                                <td>Last Four:</td>
                                                <td><?php echo $time_tracking['lastfour']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if (($time_tracking['expiration']) && ($time_tracking['expiration'] != '0000-00-00')) { ?>
                                            <tr>
                                                <td>Expiration:</td>
                                                <td><?php echo date('m/d/Y', strtotime($time_tracking['expiration'])); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>

                                </div>

                                <div class="left">

                                    <fieldset class="form-group">
                                        <label>Date / Time</label>
                                        <div>
                                            <?php

                                            // FIELD VALUES
                                            $field = array(
                                                'id'        => 'transaction_dt',
                                                'key'       => 'dt',
                                                'value'     => $time_tracking['dt'],
                                                'required'  => true,
                                                'options'   => array(
                                                    'default_today' => false
                                                )
                                            );

                                            // INCLUDE FIELD
                                            include(BigTree::path('admin/form-field-types/draw/datetime.php'));

                                            ?>
                                        </div>
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Amount</label>
                                        $<input type="text" name="amount" value="<?php echo $time_tracking['amount']; ?>" style="display: inline; width: 100px;" />
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Method</label>
                                        <select name="method">
                                            <?php foreach ($payment_methods as $payment_method) { ?>
                                                <option value="<?php echo $payment_method['id']; ?>" <?php if ($time_tracking['method'] == $payment_method['id']) { ?>selected="selected"<?php } ?>><?php echo $payment_method['title']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>

                                </div>

                                <div class="right">

                                    <fieldset class="form-group">
                                        <label>Status</label>
                                        <select name="status">
                                            <option value="captured" <?php if ($time_tracking['status'] == 'captured') { ?>selected="selected"<?php } ?>>Captured</option>
                                            <option value="charged" <?php if ($time_tracking['status'] == 'charged') { ?>selected="selected"<?php } ?>>Charged</option>
                                            <option value="failed" <?php if ($time_tracking['status'] == 'failed') { ?>selected="selected"<?php } ?>>Failed</option>
                                            <option value="voided" <?php if ($time_tracking['status'] == 'voided') { ?>selected="selected"<?php } ?>>Voided</option>
                                            <option value="refunded" <?php if ($time_tracking['status'] == 'refunded') { ?>selected="selected"<?php } ?>>Refunded</option>
                                        </select>
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Note</label>
                                        <textarea name="note" style="height: 40px;"><?php echo stripslashes($time_tracking['note'] ? $time_tracking['note'] : $time_tracking['message']); ?></textarea>
                                    </fieldset>

                                    <input class="blue" type="submit" value="Update" />

                                </div>

                                </form>

                            </div>
                            */ ?>

                        </li>
                        <?php
                    }

                    ?>

                </ul>

                <?php

            // NO SHIPMENTS
            } else {
                ?>
                <div style="padding: 10px; text-align: center;">
                    No tracked time.
                </div>
                <?php
            }

            ?>

        </div>

    </div>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>