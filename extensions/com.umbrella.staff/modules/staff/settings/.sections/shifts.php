<?php

?>

<style type="text/css">

    #shifts .loading, #shifts .no-results {
        padding: 10px;
        text-align: center;
        font-size: .9em;
    }

</style>

<script type="text/javascript">

    $(function() {

        getShiftList();

        // "Add Shift" BUTTON CLICK
        $('#shifts .summary .add').click(function(e) {
            e.preventDefault();
            editShift();
        });

        // MODAL - EDIT - SAVE
        $('#modal-editShift .btn.save').on('click', function(e) {
            e.preventDefault();
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/shifts/process/', $('#modal-editShift form').serialize(), function(data) {
                if (data.success) {
                    getShiftList();
                    $('#modal-editShift').modal('hide');
                } else {
                    alert(data.error ? data.error : 'Failed to add shift.');
                }
            }, 'json');
        });

    });

    // GET THE SHIFT LIST
    function getShiftList() {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/shifts/list/', {}, function(data) {
            $('#shifts .shifts-list').html(data);
        }, 'html');
    }

    // EDIT SHIFT
    function editShift(params, options, callback) {
        $('#modal-editShift').modal('show', {
            focus: false
        });
        if (typeof params != 'object') params = {};
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/shifts/edit/', {
            id: params.id
        }, function(data) {
            $('#modal-editShift .modal-body .data').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<div id="shifts" class="table">

    <div class="summary clearfix">
        <h2>Shifts</h2>
        <a class="btn btn-primary action add" href="#" <? /*data-toggle="modal" data-target="#modal-editShift"*/ ?> data-id="0"><i class="fas fa-plus"></i>Add shift</a>
    </div>

    <div class="shifts-list table-responsive">
        <div class="loading">Loading..</div>
    </div>

</div>

<div class="modal fade" id="modal-editShift" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" shift="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Shift</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="save btn btn-primary" disabled="disabled">Save</button>
            </div>
        </div>
    </div>
</div>