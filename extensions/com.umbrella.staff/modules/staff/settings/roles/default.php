<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - Role Settings';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => 'Roles', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2]. '/' .$bigtree['path'][3] ];

// GET SETTINGS
$settings = $_uccms_staff->getSettings();

?>

<style type="text/css">

    #roles .loading, #roles .no-results {
        padding: 10px;
        text-align: center;
        font-size: .9em;
    }

</style>

<script type="text/javascript">

    $(function() {

        getRoleList();

        // "Add Role" BUTTON CLICK
        $('#roles .summary .add').click(function(e) {
            e.preventDefault();
            editRole();
        });

        // SAVE
        $('#modal-editRole .btn.save').on('click', function(e) {
            e.preventDefault();
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/roles/process/', $('#modal-editRole form').serialize(), function(data) {
                if (data.success) {
                    getRoleList();
                    $('#modal-editRole').modal('hide');
                } else {
                    alert(data.error ? data.error : 'Failed to add role.');
                }
            }, 'json');
        });

    });

    // GET THE ROLES LIST
    function getRoleList() {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/roles/list/', {}, function(data) {
            $('#roles .roles-list').html(data);
        }, 'html');
    }

    // EDIT ROLE
    function editRole(params, options, callback) {
        $('#modal-editRole').modal('show', {
            focus: false
        });
        if (typeof params != 'object') params = {};
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/roles/edit/', {
            id: params.id
        }, function(data) {
            $('#modal-editRole .modal-body .data').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<div id="roles" class="table">

    <div class="summary clearfix">
        <h2>Roles</h2>
        <a class="btn btn-primary action add" href="#" <? /*data-toggle="modal" data-target="#modal-editRole"*/ ?> data-id="0"><i class="fas fa-plus"></i>Add Role</a>
    </div>

    <div class="roles-list table-responsive">
        <div class="loading">Loading..</div>
    </div>

</div>

<div class="modal fade" id="modal-editRole" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="save btn btn-primary" disabled="disabled">Save</button>
            </div>
        </div>
    </div>
</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>