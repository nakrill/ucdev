<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - Route Settings';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => 'Routes', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2]. '/' .$bigtree['path'][3] ];

?>

<style type="text/css">

    #routes .loading, #routes .no-results {
        padding: 10px;
        text-align: center;
        font-size: .9em;
    }

</style>

<script type="text/javascript">

    $(function() {

        getRouteList();

        // "Add Route" BUTTON CLICK
        $('#routes .summary .add').click(function(e) {
            e.preventDefault();
            editRoute();
        });

        // MODAL - EDIT - SAVE
        $('#modal-editRoute .btn.save').on('click', function(e) {
            e.preventDefault();
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/routes/process/', $('#modal-editRoute form').serialize(), function(data) {
                if (data.success) {
                    getRouteList();
                    $('#modal-editRoute').modal('hide');
                } else {
                    alert(data.error ? data.error : 'Failed to add route.');
                }
            }, 'json');
        });

    });

    // GET THE ROUTE LIST
    function getRouteList() {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/routes/list/', {}, function(data) {
            $('#routes .routes-list').html(data);
        }, 'html');
    }

    // EDIT ROUTE
    function editRoute(params, options, callback) {
        $('#modal-editRoute').modal('show', {
            focus: false
        });
        if (typeof params != 'object') params = {};
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/settings/routes/edit/', {
            id: params.id
        }, function(data) {
            $('#modal-editRoute .modal-body .data').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<div id="routes" class="table">

    <div class="summary clearfix">
        <h2>Routes</h2>
        <a class="btn btn-primary action add" href="#" <? /*data-toggle="modal" data-target="#modal-editRoute"*/ ?> data-id="0"><i class="fas fa-plus"></i>Add route</a>
    </div>

    <div class="routes-list table-responsive">
        <div class="loading">Loading..</div>
    </div>

</div>

<div class="modal fade" id="modal-editRoute" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" route="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Route</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="save btn btn-primary" disabled="disabled">Save</button>
            </div>
        </div>
    </div>
</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>