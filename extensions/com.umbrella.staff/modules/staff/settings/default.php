<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - Settings';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET SETTINGS
$settings = $_uccms_staff->getSettings();

?>

<style type="text/css">

</style>

<? /*
<script type="text/javascript">

    $(document).ready(function() {

        // POSTS - COMMENTS ENABLED TOGGLE
        $('#post_comments_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('.post_comments_enabled_content').show();
            } else {
                $('.post_comments_enabled_content').hide();
            }
        });

    });

</script>
*/ ?>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>General</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <? /*
                <fieldset>
                    <label>Posts Per Page</label>
                    <input type="text" name="setting[posts_per_page]" value="<?php echo stripslashes($settings['posts_per_page']); ?>" placeholder="<?php echo $cms->getSetting('bigtree-internal-per-page'); ?>" class="smaller_60" />
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="setting[search_disabled]" value="1" <?php if ($settings['search_disabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Search disabled <small>Don't allow searching.</small></label>
                </fieldset>
                */ ?>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="btn btn-primary" type="submit" value="Save" />
    </footer>

</div>

</form>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>