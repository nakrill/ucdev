<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Time Tracking', 'link' => $_uccms_staff->Extension. '/' .$bigtree['path'][2] ];

// GOOGLE MAPS KEY
$google_maps_key = $cms->getSetting('google_maps_key');

/*
if ($_SERVER['REMOTE_ADDR'] == '68.7.118.150') {

    $staff_id = 1;

    $options = array(
        //'time_id'   => 3,
        //'job_id'    => 1,
        //'new' => true,
        //'start' => true,
        //'timestamp' => '2016-05-04 15:12:41'
    );

    $result = $_uccms_staff->timeTracking_saveRecord($staff_id, $options);

    echo print_r($result);
    exit;

}
*/

/*
if ($_SERVER['REMOTE_ADDR'] == '68.7.118.150') {

    $staff_id = 1;

    $options = array(
        //'time_id'   => 3,
        //'job_id'    => 1,
        //'offset'    => 10,
        //'limit'     => 15
    );

    $result = $_uccms_staff->timeTracking_getRecords($staff_id, $options);

    echo print_r($result);
    exit;

}
*/

?>

<?php

// HAVE GOOGLE MAPS KEY
if ($google_maps_key) { ?>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_maps_key; ?>"></script>
    <script src="/js/lib/jquery.googlemap.js"></script>

    <script type="text/javascript">

        /*
        $(document).ready(function() {

            // MAP
            $('#time_tracking .main_map .map_container').googleMap({
                zoom: 10,
                overviewMapControl: true,
                streetViewControl: true,
                scrollwheel: true,
                mapTypeControl: true
            });

        });
        */

    </script>

    <?php

}

?>

<style type="text/css">

    #time_tracking .main_section {
        float: left;
        width: 620px;
        border: 0px;
    }

    #time_tracking .main_map {
        float: left;
        width: 340px;
        margin: -20px 0 0;
        background-image: url('/images/misc/map.jpg');
        background-position: center center;
        background-size: cover;
    }
    #time_tracking .main_map .no-google-maps-key {
        min-height: 616px;
        padding: 20px;
        text-align: center;
        font-weight: bold;
    }
    #time_tracking .main_map .map_container {
        width: 100%;
        min-height: 616px;
    }

    #time_tracking .main_section .search_filter {
        margin: 0 15px 15px 0;
    }
    #time_tracking .main_section .search_filter .filter {
        float: left;
    }
    #time_tracking .main_section .search_filter .filter .option {
        float: left;
        max-width: 200px;
        margin-right: 10px;
    }
    #time_tracking .main_section .search_filter .filter .option:last-child {
        margin-right: 0px;
    }
    #time_tracking .main_section .search_filter .search_paging {
        float: right;
        margin-top: 6px;
        padding: 0px;
    }

    #time_tracking .time_tracking {
        border: 0px;
        border-radius: 0px;
    }

    #time_tracking .time_tracking header {
        height: 30px;
    }
    #time_tracking .time_tracking header span {
        line-height: 34px;
    }

    #time_tracking .time_tracking .items .item {
        cursor: pointer;
    }
    #time_tracking .time_tracking .tt_staff {
        width: 200px;
        text-align: left;
    }
    #time_tracking .time_tracking .tt_staff a {
        color: #333;
    }
    #time_tracking .time_tracking .tt_staff .avatar {
        display: inline-block;
        width: 30px;
        height: 30px;
        margin: -2px 5px 0 0;
        border-radius: 30px;
        background-color: #ddd;
        background-position: center center;
        background-size: cover;
        vertical-align: middle;
    }
    #time_tracking .time_tracking .tt_dt {
        width: 100px;
        text-align: left;
    }
    #time_tracking .time_tracking .tt_job {
        width: 200px;
        text-align: left;
    }
    #time_tracking .time_tracking .tt_clock_in {
        width: 100px;
        text-align: left;
    }
    #time_tracking .time_tracking .tt_clock_out {
        width: 100px;
        text-align: left;
    }
    #time_tracking .time_tracking .tt_clock_out.active .circle {
        display: inline-block;
        margin: -2px 3px 0 0;
        width: 8px;
        height: 8px;
        background-color: #66BB6A;
        border-radius: 8px;
    }
    #time_tracking .time_tracking .tt_clock_out.active .title {
        text-transform: uppercase;
    }
    #time_tracking .time_tracking .tt_total {
        width: 100px;
        text-align: left;
    }
    #time_tracking .time_tracking .tt_edit {
        width: 50px;
    }
    #time_tracking .time_tracking .tt_delete {
        width: 50px;
    }

    #time_tracking .time_tracking .table > ul li section {
        height: 50px;
        line-height: 50px;
    }

    #time_tracking .time_tracking ul .item {
        height: auto;
    }
    #time_tracking .time_tracking ul .item.active, #time_tracking .time_tracking ul .item:hover {
        background-color: #FFF9C4;
    }

    #time_tracking .time_tracking ul .item .edit {
        padding: 10px 20px 0;
        border-top: 1px dashed #ddd;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        /*
        // EDIT ICON CLICK
        $('#time_tracking .time_tracking ul .item .icon_edit').click(function(e) {
            e.preventDefault();
            $(this).closest('.item').find('.edit').toggle();
        });
        */

        // SEARCH / FILTER - STAFF CHANGE
        $('#time_tracking .main_section .search_filter .filter select').change(function(e) {
            BigTree.localSearch();
        });

    });

</script>

<div id="time_tracking" class="clearfix">

    <div class="main_section">

        <div class="search_filter clearfix">

            <div class="filter">

                <div class="option clearfix">
                    <select name="staff_id">
                        <option value="0">All Staff</option>
                        <?php
                        $staff_query = "SELECT *, CONCAT(firstname, ' ', lastname) AS `sortname` FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`status`!=9) ORDER BY `sortname` ASC";
                        $staff_q = sqlquery($staff_query);
                        while ($staff = sqlfetch($staff_q)) {
                            ?>
                            <option value="<?php echo $staff['id']; ?>"><?php echo trim(stripslashes($staff['firstname']. ' ' .$staff['lastname'])); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="option clearfix">
                    <select name="status">
                        <option value="0">All Statuses</option>
                        <option value="active">Active</option>
                        <option value="complete">Complete</option>
                    </select>
                </div>

            </div>

            <div class="search_paging contain">
                <? /*
                <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
                <span class="form_search_icon"></span>
                */ ?>
                <nav id="view_paging" class="view_paging"></nav>
            </div>

        </div>

        <div class="time_tracking container">

            <div class="table" style="margin: 0px; border: 0px none;">

                <header style="clear: both;">
                    <span class="tt_staff">Staff</span>
                    <span class="tt_dt">Date</span>
                    <? /*<span class="tt_job">Job</span>*/ ?>
                    <span class="tt_clock_in">Clock In</span>
                    <span class="tt_clock_out">Clock Out</span>
                    <span class="tt_total">Total Time</span>
                    <? /*
                    <span class="tt_edit">Edit</span>
                    <span class="tt_delete">Delete</span>
                    */ ?>
                </header>

                <ul id="results" class="items">
                    <? include(EXTENSION_ROOT. 'ajax/admin/time-tracking/get-page.php'); ?>
                </ul>

            </div>

        </div>

    </div>

    <div class="main_map">

        <?php

        // HAVE GOOGLE MAPS KEY
        if ($google_maps_key) {

            ?>

            <div class="map_container"></div>

            <?php

        } else {
            ?>
            <div class="no-google-maps-key">
                Please set up a "google_maps_key" setting to display maps.
            </div>
            <?php
        }

        ?>

    </div>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function(vars) {
        if (!vars) vars = {};
        $('#results').load('<?=ADMIN_ROOT?>*/<?=$_uccms_staff->Extension?>/ajax/admin/time-tracking/get-page/', {
            'page': vars.page,
            'query': escape($("#query").val()),
            'staff_id': escape($('#time_tracking .main_section .search_filter .filter select[name="staff_id"]').val()),
            'status': escape($('#time_tracking .main_section .search_filter .filter select[name="status"]').val()),
        });
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        BigTree.localSearch({
            page: BigTree.cleanHref($(this).attr('href'))
        });
        return false;
    });
</script>