<?php

//echo 'Access level: ' .$_uccms_staff->adminModulePermission();

$staffa = array();

// GET STAFF
$staff_query = "SELECT *, CONCAT(firstname, ' ', lastname) AS `sortname` FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`status`!=9) ORDER BY `sortname` ASC";
$staff_q = sqlquery($staff_query);

// LOOP
while ($staff = sqlfetch($staff_q)) {
    $staffa[$staff['id']] = $staff;
}

?>

<style type="text/css">

    #staff_dashboard .col_left {
        width: 100%;
        padding-top: 0px;
        padding-left: 0px;
        vertical-align: top;
    }

    #staff_dashboard .col_right {
        margin-left: 30px;
        border-left: 1px solid #D1D6DC;
        /*background-color: #f5f5f5;*/
    }
    #staff_dashboard .col_right .size {
        width: 200px;
        min-height: 400px;
        padding: 0 15px;
    }


    #staff_dashboard .report.today header {
        height: 30px;
    }
    #staff_dashboard .report.today header span {
        line-height: 34px;
    }

    #staff_dashboard .report.today .r_staff {
        width: 220px;
        text-align: left;
    }
    #staff_dashboard .report.today .r_clock_in {
        width: 150px;
        text-align: left;
    }
    #staff_dashboard .report.today .r_clock_out {
        width: 170px;
        text-align: left;
    }
    #staff_dashboard .report.today .r_clock_out.active .circle {
        display: inline-block;
        margin: -2px 3px 0 0;
        width: 8px;
        height: 8px;
        background-color: #66BB6A;
        border-radius: 8px;
    }
    #staff_dashboard .report.today .r_clock_out.active .title {
        text-transform: uppercase;
    }
    #staff_dashboard .report.today .r_hours {
        width: 120px;
        text-align: left;
    }

    #staff_dashboard .report.today .item.today {
        cursor: pointer;
    }

    /*
    #staff_dashboard .toggle_bar {
        padding: 15px 8px;
        background-color: #f8f8f8;
    }
    #staff_dashboard .toggle_bar ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #staff_dashboard .toggle_bar ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 10px;
        font-weight: bold;
        text-transform: uppercase;
    }
    #staff_dashboard .toggle_bar ul li a {
        color: #aaa;
    }
    #staff_dashboard .toggle_bar ul li a:hover, #staff_dashboard .toggle_bar ul li a.active {
        color: #333;
    }
    #staff_dashboard .toggle_bar ul li a .num {
        font-size: .9em;
        font-weight: normal;
        color: #aaa;
    }

    #staff_dashboard .toggle_content_container {
    }
    #staff_dashboard .toggle_content_container .toggle {
        display: none;
    }
    #staff_dashboard .toggle_content_container .toggle:first-child {
        display: block;
    }
    #staff_dashboard .toggle_content_container .toggle .none {
        padding: 15px;
        text-align: center;
    }

    #staff_dashboard .toggle_content_container .item {
        padding: 15px 15px 15px 0;
        line-height: 1em;
    }
    #staff_dashboard .toggle_content_container .item:hover {
        background-color: #f5faff;
    }
    #staff_dashboard .toggle_content_container .item table {
        width: 100%;
        margin: 0px;
        padding: 0px;
        border: 0px none;
    }
    #staff_dashboard .toggle_content_container .item table td {
        padding: 0px;
        vertical-align: top;
    }
    #staff_dashboard .toggle_content_container .item table td table td {
        font-size: 1em;
        color: #999;
        text-transform: uppercase;
    }
    #staff_dashboard .toggle_content_container .item td.icon {
        padding: 0 15px;
        white-space: nowrap;
        font-size: 2em;
        opacity: .6;
    }
    #staff_dashboard .toggle_content_container .item .icon i.fa {
        margin-top: -3px;
    }
    #staff_dashboard .toggle_content_container .item td.content {
        width: 100%;
    }
    #staff_dashboard .toggle_content_container .item .content .title {
        padding-bottom: 6px;
        font-size: 1.2em;
        font-weight: bold;
    }
    #staff_dashboard .toggle_content_container .item .content .title a {
        color: #555;
    }
    #staff_dashboard .toggle_content_container .item .content .status {
        width: 20%;
    }
    #staff_dashboard .toggle_content_container .item .content .status .published {
        color: #6BD873;
    }
    #staff_dashboard .toggle_content_container .item .content .link {
        width: 50%;
        text-transform: none;
    }
    #staff_dashboard .toggle_content_container .item .content .actions {
        width: 30%;
        text-align: right;
    }
    #staff_dashboard .toggle_content_container .item .content .actions ul {
        opacity: 0;
    }
    #staff_dashboard .toggle_content_container .item:hover .content .actions ul {
        opacity: 1;
    }
    #staff_dashboard .toggle_content_container .item ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #staff_dashboard .toggle_content_container .item ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 0 0 8px;
        line-height: 1em;
    }
    #staff_dashboard .toggle_content_container .more {
        padding-top: 15px;
        text-align: center;
    }
    */

</style>

<script type="text/javascript">

    $(document).ready(function() {

        /*
        $('#staff_dashboard .toggle_bar a').click(function(e) {
            e.preventDefault();
            var set = $(this).closest('.toggle_bar').attr('data-set');
            var what = $(this).attr('data-what');
            $('#staff_dashboard .toggle_bar a').removeClass('active');
            $(this).addClass('active');
            $('#staff_dashboard .toggle_content_container.set-' +set+ ' .toggle').hide();
            $('#staff_dashboard .toggle_content_container.set-' +set+ ' .toggle.' +what).show();

        });
        */

        // STAFF CLICK
        $('#staff_dashboard .report.today .item.today').click(function(e) {
            e.preventDefault();
            window.location.href = './daily-report/?staff_id=' +$(this).attr('data-staff-id')+ '&from=<?php echo date('m/d/Y'); ?>&to=<?php echo date('m/d/Y'); ?>';
        });

    });

</script>

<div id="staff_dashboard">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 0px;">
        <tr>

            <td class="col_left">

                <div class="report today">

                    <div class="table" style="margin: 0px; border: 0px none;">

                        <header style="clear: both;">
                            <span class="r_staff">Staff</span>
                            <span class="r_clock_in">Clock In</span>
                            <span class="r_clock_out">Clock Out</span>
                            <span class="r_hours">Hours</span>
                        </header>

                        <ul id="results" class="items">

                            <?php

                            $date = date('Y-m-d');

                            // LOOP
                            foreach ($staffa as $staff) {

                                $total_time = 0;

                                $dt_clock_in = '';
                                $dt_clock_out = '';

                                // CURRENT STATUS
                                $current_query = "
                                SELECT *
                                FROM `" .$_uccms_staff->tables['time_tracking']. "`
                                WHERE (`staff_id`=" .$staff['id']. ")
                                ORDER BY `dt_start` DESC
                                LIMIT 1
                                ";
                                $current_q = sqlquery($current_query);
                                $current = sqlfetch($current_q);

                                // GET TIME TRACKING
                                $time_tracking_query = "
                                SELECT *
                                FROM `" .$_uccms_staff->tables['time_tracking']. "`
                                WHERE
                                    (`staff_id`=" .$staff['id']. ") AND
                                    ((`dt_start` BETWEEN '" .$date. " 00:00:00' AND '" .$date. " 23:59:59')
                                    OR
                                    (`dt_end` BETWEEN '" .$date. " 00:00:00' AND '" .$date. " 23:59:59')
                                    OR
                                    (('" .$date. " 00:00:00'>=`dt_start`) AND ('" .$date. " 23:59:59'<=`dt_end`)))
                                ORDER BY `dt_start` ASC
                                ";

                                //echo $time_tracking_query;

                                $time_tracking_q = sqlquery($time_tracking_query);
                                while ($time_tracking = sqlfetch($time_tracking_q)) {

                                    // CLOCK IN TIME (FIRST OF DAY)
                                    if (!$dt_clock_in) {
                                        $dt_clock_in = $time_tracking['dt_start'];
                                    }

                                    // CLOCK OUT TIME
                                    $dt_clock_out = '';
                                    if (($time_tracking['dt_end']) && ($time_tracking['dt_end'] != '0000-00-00 00:00:00')) {
                                        $dt_clock_out = $time_tracking['dt_end'];
                                    }

                                    //echo 'out:' .$dt_clock_out;

                                    // HAVE END DATE / TIME
                                    if ($time_tracking['dt_end'] != '0000-00-00 00:00:00') {

                                        // ADD TO TOTAL TIME
                                        $total_time += strtotime($time_tracking['dt_end']) - strtotime($time_tracking['dt_start']);

                                    // NO END DATE / TIME
                                    } else {

                                        // ADD TO TOTAL TIME
                                        $total_time += time() - strtotime($time_tracking['dt_start']);

                                    }

                                }

                                //echo 'OUT' .$dt_clock_out;

                                ?>

                                <li class="item report today" data-staff-id="<?php echo $staff['id']; ?>">
                                    <div class="contain clearfix">
                                        <section class="r_staff">
                                            <span class="avatar" <?php if ($sl['image']) { ?>style="background-image: url('<?php echo $_uccms_staff->imageBridgeOut($sl['image'], 'staff'); ?>');"<?php } ?>></span>
                                            <?php echo $_uccms_staff->combineName($staff); ?>
                                        </section>
                                        <section class="r_clock_in">
                                            <?php if (($dt_clock_in) && (date('Y-m-d', strtotime($dt_clock_in)) == $date)) { ?>
                                                <?php echo date('h:i A', strtotime($dt_clock_in)); ?>
                                            <?php } else { ?>
                                                <span class="none">-</span>
                                            <?php } ?>
                                        </section>
                                        <section class="r_clock_out <?php if (!$dt_clock_out) { ?>active<?php } ?>">
                                            <?php if ($dt_clock_in) { ?>
                                                <?php if ($dt_clock_out) { ?>
                                                    <?php echo date('h:i A', strtotime($dt_clock_out)); ?>
                                                <?php } else { ?>
                                                    <span class="circle"></span>
                                                    <span class="title">Active</span>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php if (($current['dt_end']) && ($current['dt_end'] != '0000-00-00 00:00:00')) { ?>
                                                    <span class="circle"></span>
                                                    <span class="title">Active</span>
                                                <?php } else if ($dt_clock_out) { ?>
                                                    <?php echo date('h:i A', strtotime($dt_clock_out)); ?>
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            <?php } ?>
                                        </section>
                                        <section class="r_hours">
                                            <?php

                                            if ($dt_clock_in) {

                                                if (!$total_time) {
                                                    $total_time = time() - strtotime($current['dt_start']);
                                                }

                                                /*
                                                $time = $_uccms_staff->dtDiff('@0', "@$total_time", array(
                                                    'format' => '%H:%I'
                                                ));
                                                */

                                                echo $_uccms_staff->formatSeconds($total_time). ' hr';

                                            } else {
                                                echo ' - ';
                                            }

                                            ?>
                                        </section>
                                    </div>
                                </li>

                                <?php

                            }

                            ?>

                        </ul>

                    </div>

                </div>

            </td>

            <td class="col_right" valign="top">
                <div class="size">

                    <h3 style="text-align: center;">Recent Activity</h3>

                    <div class="activity_container">

                        <?php

                        // ITEM ARRAY
                        $itema = array();

                        // LAST CREATED
                        $lcreated_query = "SELECT *, `dt_created` AS `sort_by`, 'created' AS `item_what` FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`dt_created`!='0000-00-00 00:00:00') ORDER BY `dt_created` DESC LIMIT 10";
                        $lcreated_q = sqlquery($lcreated_query);
                        while ($lcreated = sqlfetch($lcreated_q)) {
                            $itema[] = $lcreated;
                        }

                        /*
                        // LAST SCHEDULED
                        $lscheduled_query = "SELECT *, `dt_updated` AS `sort_by`, 'scheduled' AS `item_what` FROM `" .$_uccms_staff->tables['posts']. "` WHERE (`dt_publish`!='0000-00-00 00:00:00') ORDER BY `dt_updated` DESC LIMIT 10";
                        $lscheduled_q = sqlquery($lscheduled_query);
                        while ($lscheduled = sqlfetch($lscheduled_q)) {
                            $itema[] = $lscheduled;
                        }
                        */

                        // LAST UPDATED
                        $lupdated_query = "SELECT *, `dt_updated` AS `sort_by`, 'updated' AS `item_what` FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`dt_updated`!='0000-00-00 00:00:00') ORDER BY `dt_updated` DESC LIMIT 10";
                        $lupdated_q = sqlquery($lupdated_query);
                        while ($lupdated = sqlfetch($lupdated_q)) {
                            $itema[] = $lupdated;
                        }

                        /*
                        // LAST PUBLISHED
                        $lpublished_query = "SELECT *, `dt_published` AS `sort_by`, 'published' AS `item_what` FROM `" .$_uccms_staff->tables['posts']. "` WHERE (`dt_published`!='0000-00-00 00:00:00') ORDER BY `dt_published` DESC LIMIT 10";
                        $lpublished_q = sqlquery($lpublished_query);
                        while ($lpublished = sqlfetch($lpublished_q)) {
                            $itema[] = $lpublished;
                        }
                        */

                        // LAST DELETED
                        $ldeleted_query = "SELECT *, `dt_deleted` AS `sort_by`, 'deleted' AS `item_what` FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`dt_deleted`!='0000-00-00 00:00:00') ORDER BY `dt_deleted` DESC LIMIT 10";
                        $ldeleted_q = sqlquery($ldeleted_query);
                        while ($ldeleted = sqlfetch($ldeleted_q)) {
                            $itema[] = $ldeleted;
                        }

                        // GET TIME TRACKING
                        $time_tracking_query = "
                        SELECT *
                        FROM `" .$_uccms_staff->tables['time_tracking']. "`
                        ORDER BY `dt_start` DESC LIMIT 10
                        ";
                        $time_tracking_q = sqlquery($time_tracking_query);
                        while ($time_tracking = sqlfetch($time_tracking_q)) {

                            // CLOCK IN
                            $itema[] = array(
                                'sort_by'       => $time_tracking['dt_start'],
                                'item_what'     => 'clock_in',
                                'view_link'     => './daily-report/?staff_id=' .$time_tracking['staff_id']. '&from=' .date('m/d/Y', strtotime($time_tracking['dt_start'])). '&to=' .date('m/d/Y', strtotime($time_tracking['dt_start'])),
                                'staff_name'    => $_uccms_staff->combineName($staffa[$time_tracking['staff_id']]),
                            );

                            // HAVE END DATE / TIME
                            if ($time_tracking['dt_end'] != '0000-00-00 00:00:00') {

                                // CLOCK OUT
                                $itema[] = array(
                                    'sort_by'       => $time_tracking['dt_end'],
                                    'item_what'     => 'clock_out',
                                    'view_link'     => './daily-report/?staff_id=' .$time_tracking['staff_id']. '&from=' .date('m/d/Y', strtotime($time_tracking['dt_end'])). '&to=' .date('m/d/Y', strtotime($time_tracking['dt_end'])),
                                    'staff_name'    => $_uccms_staff->combineName($staffa[$time_tracking['staff_id']]),
                                );

                            }

                        }

                        // GET PHOTOS
                        $photos_query = "
                        SELECT *
                        FROM `" .$_uccms_staff->tables['photos']. "`
                        ORDER BY `dt` DESC
                        LIMIT 10
                        ";

                        $photos_q = sqlquery($photos_query);
                        while ($photo = sqlfetch($photos_q)) {

                            // PHOTO
                            $itema[] = array(
                                'sort_by'       => $photo['dt'],
                                'item_what'     => 'photo_taken',
                                'view_link'     => './daily-report/?staff_id=' .$photo['staff_id']. '&from=' .date('m/d/Y', strtotime($photo['dt'])). '&to=' .date('m/d/Y', strtotime($photo['dt'])),
                                'staff_name'    => $_uccms_staff->combineName($staffa[$photo['staff_id']]),
                            );

                        }

                        // HAVE ITEMS
                        if (count($itema) > 0) {

                            // SORT BY DATE/TIME ASCENDING
                            usort($itema, function($a, $b) {
                                return strtotime($b['sort_by']) - strtotime($a['sort_by']);
                            });

                            // STATUSES
                            $statusa = array(
                                'created'   => array(
                                    'title'     => 'Created',
                                    'icon'      => 'fa-plus-circle',
                                    'color'     => '#85eeb8',
                                    'actions'   => '<a href="./staff/edit/?id={id}">Edit</a>'
                                ),
                                'updated'     => array(
                                    'title'     => 'Updated',
                                    'icon'      => 'fa-floppy-o',
                                    'color'     => '#4dd0e1',
                                    'actions'   => '<a href="./staff/edit/?id={id}">Edit</a>'
                                ),
                                /*
                                'published' => array(
                                    'title'     => 'Published',
                                    'icon'      => 'fa-check-circle',
                                    'color'     => '#00c05d',
                                    'actions'   => '<a href="{link_view}">View</a> | <a href="./posts/staff/?id={id}">Edit</a>'
                                ),
                                'scheduled' => array(
                                    'title'     => 'Scheduled',
                                    'icon'      => 'fa-clock-o',
                                    'color'     => '#4db6ac',
                                    'actions'   => '<a href="{link_view}">View</a> | <a href="./posts/staff/?id={id}">Edit</a>'
                                ),
                                */
                                'deleted'   => array(
                                    'title'     => 'Deleted',
                                    'icon'      => 'fa-times-circle',
                                    'color'     => '#e53935'
                                ),
                                'clock_in' => array(
                                    'title'     => 'Clocked In',
                                    'icon'      => 'fa-play-circle',
                                    'color'     => '#8CCD77',
                                    'actions'   => '<a href="{link_view}">View</a>'
                                ),
                                'clock_out' => array(
                                    'title'     => 'Clocked Out',
                                    'icon'      => 'fa-stop-circle',
                                    'color'     => '#FA9289',
                                    'actions'   => '<a href="{link_view}">View</a>'
                                ),
                                'photo_taken' => array(
                                    'title'     => 'Photo Taken',
                                    'icon'      => 'fa-camera',
                                    'color'     => '#4db6ac',
                                    'actions'   => '<a href="{link_view}">View</a>'
                                ),
                            );

                            $i = 1;

                            // LOOP
                            foreach ($itema as $item) {
                                $actiona = array(
                                    $item['id'],
                                    $item['view_link'],
                                );
                                ?>
                                <div class="item contain">
                                    <?php if ($statusa[$item['item_what']]['icon']) { ?>
                                        <div class="icon" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'background-color: ' .$statusa[$item['item_what']]['color']; } ?>"><i class="fa fa-fw <?php echo $statusa[$item['item_what']]['icon']; ?>"></i></div>
                                    <?php } ?>
                                    <div class="main">
                                        <?php if ($statusa[$item['item_what']]['title']) { ?>
                                            <div class="status" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'color: ' .$statusa[$item['item_what']]['color']; } ?>">
                                                <?php echo $statusa[$item['item_what']]['title']; ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($item['staff_name']) { ?>
                                            <div class="title">
                                                <?php if ($item['status'] != 9) { ?>
                                                    <a href="./staff/edit/?id=<?php echo $item['id']; ?>">
                                                <?php } ?>
                                                <?php echo $item['staff_name']; ?>
                                                <?php if ($item['status'] != 9) { ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="time"><?php echo date('j M g:i A', strtotime($item['sort_by'])); ?></div>
                                        <?php if ($statusa[$item['item_what']]['actions']) { ?>
                                            <div class="actions">
                                                <i class="fa fa-caret-right"></i> <?php echo str_replace(array('{id}','{link_view}'), $actiona, $statusa[$item['item_what']]['actions']); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if ($i == 10) {
                                    break;
                                } else {
                                    ?>
                                    <div class="split"></div>
                                    <?php
                                    $i++;
                                }
                            }

                            unset($itema);

                        }

                        ?>

                    </div>

                </div>
            </td>

        </tr>
    </table>

</div>

<?php

function this_item_staff($staff) {
    global $_uccms_staff;
    if ($staff['id']) {
        $out = '
        <div class="item">
            <table>
                <tr>
                    <td class="icon"><i class="fa fa-comment-o"></i></td>
                    <td class="content">
                        <div class="title"><a href="./staff/edit/?id=' .$staff['id']. '">' .$_uccms_staff->combineName($staff). '</a></div>
                        <table>
                            <tr>
                                <td class="status">
                                    ';
                                    if ($staff['status'] == 1) {
                                        $out .= '<span class="published">Active</span>';
                                    } else {
                                        $out .= '<span class="draft">Inactive</span>';
                                    }
                                    $out .= '
                                </td>
                                <td class="link">
                                    ' .$staff['link']. '
                                </td>
                                <td class="actions">
                                    <ul>
                                        <li><a href="./staff/edit/?id=' .$staff['id']. '">Edit</a></li>
                                        <li><a href="./staff/delete/?id=' .$staff['id']. '&from=dashboard" onclick="return confirmPrompt(this.href, \'Are you sure you want to delete this this?\');">Delete</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        ';
    }
    return $out;
}

?>