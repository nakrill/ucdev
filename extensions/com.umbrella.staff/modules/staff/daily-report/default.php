<?php

// STAFF ID SPECIFIED
if ($_REQUEST['staff_id']) {

    $staff_id = (int)$_REQUEST['staff_id'];

    $staff_query = "SELECT * FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`id`=" .$staff_id. ")";
    $staff_q = sqlquery($staff_query);
    $staff = sqlfetch($staff_q);

}

// DATES SPECIFIED
if (($_REQUEST['from']) && ($_REQUEST['to'])) {
    $from = date('Y-m-d', strtotime($_REQUEST['from']));
    $to = date('Y-m-d', strtotime($_REQUEST['to']));
} else {
    $from = date('Y-m-d');
    $to = date('Y-m-d');
}

// GOOGLE MAPS KEY
$google_maps_key = $cms->getSetting('google_maps_key');

// HAVE GOOGLE MAPS KEY
if ($google_maps_key) { ?>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_maps_key; ?>"></script>
    <script src="/js/lib/jquery.googlemap.js"></script>

    <script type="text/javascript">

        /*
        $(document).ready(function() {

            // MAP
            $('#report .main_map .map_container').googleMap({
                zoom: 10,
                overviewMapControl: true,
                streetViewControl: true,
                scrollwheel: true,
                mapTypeControl: true
            });

        });
        */

    </script>

    <?php

}

?>

<link href="//cdn.rawgit.com/noelboss/featherlight/1.7.8/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.8/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

<style type="text/css">

    #report .staff_list {
        float: left;
        width: 200px;
        margin-top: -20px;
        padding-top: 52px;
        background-color: #fafafa;
        border: 1px solid #f5f5f5;
    }

    #report .main_section {
        float: left;
        width: 738px;
        padding-left: 20px;
        border: 0px;
    }

    #report .staff_list .staff {
        display: block;
        padding: 11px 10px 10px;
        border-bottom: 1px solid #ddd;
        color: #333;
    }
    #report .staff_list .staff:last-child {
        border-bottom: 0px none;
    }
    #report .staff_list .staff:hover, #report .staff_list .staff.active {
        margin-left: -1px;
        margin-right: -1px;
        background-color: #fff;
    }
    #report .staff_list .avatar {
        display: inline-block;
        width: 30px;
        height: 30px;
        margin: -2px 5px 0 0;
        border-radius: 30px;
        background-color: #ddd;
        background-position: center center;
        background-size: cover;
        vertical-align: middle;
    }

    #report .main_section .search_filter {
        margin: 0 0 10px 0;
    }
    #report .main_section .search_filter .filter {
        float: left;
    }
    #report .main_section .search_filter .filter .option {
        float: left;
        margin-right: 10px;
    }
    #report .main_section .search_filter .filter .option:last-child {
        margin-right: 0px;
    }

    #report .main_section .search_filter .filter .option.staff-name {
        line-height: 30px;
        font-size: 1.2em;
        font-weight: bold;
    }

    #report .main_section .search_filter .filter .ds {
        float: left;
        position: relative;
    }
    #report .main_section .search_filter .filter .ds input.date_picker {
        width: 100px !important;
    }
    #report .main_section .search_filter .filter .ds .icon_small {
        position: absolute;
        top: 8px;
        right: 4px;
    }
    #report .main_section .search_filter .filter .split {
        float: left;
        margin: 0 15px;
        line-height: 30px;
    }

    #report .main_section .search_filter .search_paging {
        float: right;
        margin-top: 6px;
        padding: 0px;
    }

    #report .main_section .totals {
        padding: 15px;
        background-color: #59A8E9;
        color: #fff;
    }
    #report .main_section .totals .item .label {
        font-size: .9em;
        text-transform: uppercase;
        opacity: .8;
    }
    #report .main_section .totals .item .value {
        margin-top: 4px;
        font-size: 1.1em;
    }
    #report .main_section .totals .item.date {
        float: left;
    }
    #report .main_section .totals .item.total {
        display: none;
        float: right;
        margin-right: 120px;
    }

    #report .report {
        border: 0px;
        border-radius: 0px;
    }

    #report .report header {
        height: 30px;
    }
    #report .report header span {
        line-height: 34px;
    }

    #report .report .r_dt {
        width: 220px;
        text-align: left;
    }
    #report .report .r_clock_in {
        width: 150px;
        text-align: left;
    }
    #report .report .r_clock_out {
        width: 170px;
        text-align: left;
    }
    #report .report .r_clock_out.active .circle {
        display: inline-block;
        margin: -2px 3px 0 0;
        width: 8px;
        height: 8px;
        background-color: #66BB6A;
        border-radius: 8px;
    }
    #report .report .r_clock_out.active .title {
        text-transform: uppercase;
    }
    #report .report .r_hours {
        width: 120px;
        text-align: left;
    }
    #report .report .r_expand {
        width: 50px;
        text-align: right;
    }
    #report .report .r_expand a {
        margin: 0px;
        padding: 10px;
        color: #333;
    }

    #report .report .toggle_content {
        display: none;
        padding: 0 15px 15px;
    }
    #report .report .toggle_content .map {
        clear: both;
        width: 100%;
        height: 180px;
        /*
        background-image: url(/images/misc/map.jpg);
        background-position: center center;
        background-size: cover;
        */
    }

    #report .report .toggle_content .map .gm-style .gm-style-iw h1 {
        margin: 0px;
        font-size: 1em;
        line-height: 1em;
        font-weight: bold;
    }
    #report .report .toggle_content .map .gm-style .gm-style-iw .date {
        margin-right: 5px;
    }

    #report .report .toggle_content .timeline {
        /*
        width: 500px;
        height: 415px;
        margin: 20px 0 20px 20px;
        background-image: url('https://i.pinimg.com/736x/f8/d4/4d/f8d44d9f1b172ca4395b05fa39895f07--timeline-ux.jpg');
        background-position: -35px -150px;
        background-size: 120%;
        */
        margin: 15px 0 0 25px;
        border-left: 2px solid #ddd;
    }
    #report .report .toggle_content .timeline .event {
        margin: 0 0 0 -21px;
        padding: 5px;
    }
    #report .report .toggle_content .timeline .event:hover, #report .report .toggle_content .timeline .event.active {
        background-color: #FFF9C4;
    }
    #report .report .toggle_content .timeline .event .icon {
        float: left;
        margin-right: 15px;
    }
    #report .report .toggle_content .timeline .event .icon i {
        width: 30px;
        height: 30px;
        border-radius: 30px;
        background-color: #eee;
        text-align: center;
        font-size: 1.5em;
        line-height: 30px;
        color: #fff;
    }
    #report .report .toggle_content .timeline .event .map_marker {
        float: left;
        margin: 1px 15px 0 0;
    }
    #report .report .toggle_content .timeline .event .map_marker img {
        display: block;
        width: auto;
        max-height: 30px;
    }
    #report .report .toggle_content .timeline .event .dt {
        float: left;
        margin-right: 15px;
        font-weight: bold;
        opacity: .8;
    }
    #report .report .toggle_content .timeline .event .title {
        float: left;
    }
    #report .report .toggle_content .timeline .event .photo {
        clear: both;
        margin: 15px 0 0 160px;
    }
    #report .report .toggle_content .timeline .event .photo .thumb {
        display: block;
        width: 80px;
        height: 80px;
        background-size: cover;
        background-position: center center;
        border-radius: 3px;
        cursor: pointer;
    }

    #report .report .table > ul li section {
        height: 50px;
        line-height: 50px;
    }

    #report .report ul .item {
        height: auto;
    }

    #report .report ul .item .edit {
        padding: 10px 20px 0;
        border-top: 1px dashed #ddd;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STAFF LIST SELECT
        $('#report .staff_list .staff').click(function(e) {
            e.preventDefault();
            $('#report .main_section .search_filter input[name="staff_id"]').val($(this).attr('data-id'));
            $('#report .main_section .search_filter form').submit();
        });

        // SEARCH / FILTER - DATE CHANGE
        $('#report .main_section .search_filter .filter .ds input.date_picker').change(function(e) {
            $('#report .main_section .search_filter form').submit();
        });

    });

</script>

<div id="report" class="clearfix">

    <div class="staff_list">

        <?php
        $sl_query = "SELECT *, CONCAT(firstname, ' ', lastname) AS `sortname` FROM `" .$_uccms_staff->tables['staff']. "` WHERE (`status`!=9) ORDER BY `sortname` ASC";
        $sl_q = sqlquery($sl_query);
        while ($sl = sqlfetch($sl_q)) {
            ?>
            <a href="#" class="staff <?php if ($sl['id'] == $_REQUEST['staff_id']) { echo 'active'; } ?>" data-id="<?php echo $sl['id']; ?>">
                <span class="avatar" <?php if ($sl['image']) { ?>style="background-image: url('<?php echo $_uccms_staff->imageBridgeOut($sl['image'], 'staff'); ?>');"<?php } ?>></span>
                <?php echo $_uccms_staff->combineName($sl); ?>
            </a>
            <?php
        }
        ?>

    </div>

    <div class="main_section">

        <div class="search_filter clearfix">

            <form method="get">
            <input type="hidden" name="staff_id" value="<?php echo $_REQUEST['staff_id']; ?>" />

            <div class="filter">

                <?php if ($staff['firstname']) { ?>
                    <div class="option staff-name clearfix">
                        <?php echo $_uccms_staff->combineName($staff); ?>
                    </div>
                <?php } ?>

                <div class="option from-to clearfix">

                    <div class="ds contain">
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'dt_from',
                            'key'       => 'from',
                            'value'     => $from,
                            'required'  => false,
                            'options'   => array(
                                'default_now'   => false
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>

                    <div class="split">-</div>

                    <div class="ds contain">
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'dt_to',
                            'key'       => 'to',
                            'value'     => $to,
                            'required'  => false,
                            'options'   => array(
                                'default_now'   => false
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>

                </div>

            </div>

            <div class="search_paging contain">
                <? /*
                <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
                <span class="form_search_icon"></span>
                */ ?>
                <nav id="view_paging" class="view_paging"></nav>
            </div>

            </form>

        </div>

        <div class="report container">

            <div class="totals clearfix">

                <div class="item date">
                    <div class="label">Date</div>
                    <div class="value">
                        <?php echo date('M d', strtotime($from)); if ($to != $from) { echo ' - ' .date('M d', strtotime($to)); } ?>
                    </div>
                </div>

                <div class="item total">
                    <div class="label">Total</div>
                    <div class="value"></div>
                </div>

            </div>

            <div class="table" style="margin: 0px; border: 0px none;">

                <header style="clear: both;">
                    <span class="r_dt">Date</span>
                    <span class="r_clock_in">Clock In</span>
                    <span class="r_clock_out">Clock Out</span>
                    <span class="r_hours">Hours</span>
                    <span class="r_expand"></span>
                </header>

                <ul id="results" class="items">
                    <? include(EXTENSION_ROOT. 'ajax/admin/daily-report/get-page.php'); ?>
                </ul>

            </div>

        </div>

    </div>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function(vars) {
        if (!vars) vars = {};
        $('#results').load('<?=ADMIN_ROOT?>*/<?=$_uccms_staff->Extension?>/ajax/admin/daily-report/get-page/', {
            'page': vars.page,
            'query': escape($("#query").val()),
            'staff_id': escape($('#report .main_section .search_filter input[name="staff_id"]').val()),
            'from': escape($('#report .main_section .search_filter input[name="from"]').val()),
            'to': escape($('#report .main_section .search_filter input[name="to"]').val()),
        });
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        BigTree.localSearch({
            page: BigTree.cleanHref($(this).attr('href'))
        });
        return false;
    });
</script>