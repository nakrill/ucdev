<?php

// INCLUDE COMPOSER
include_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

use When\When;

// PAGE TITLE
$bigtree['admin_title'] = 'Staff';

// BIGTREE
$bigtree['css'][]   = 'master.css';
$bigtree['js'][]    = 'master.js';

// MODULE CLASS
$_uccms_staff = new uccms_Staff;

// BREADCRUMBS
$bigtree['breadcrumb'] = [
    [ 'title' => 'Staff', 'link' => $bigtree['path'][1] ],
];

// MODULE MAIN NAV SELECTED ARRAY
if ($bigtree['path'][2]) {
    $mmnsa[$bigtree['path'][2]] = 'active';
} else {
    $mmnsa['dashboard'] = 'active';
}

?>

<nav class="main">
    <section>
        <ul>
            <li class="<?php echo $mmnsa['dashboard']; ?>">
                <a href="<?=MODULE_ROOT;?>"><span class="dashboard"></span>Dashboard</a>
            </li>
            <li class="<?php echo $mmnsa['staff']; ?>">
                <a href="<?=MODULE_ROOT;?>staff/"><span class="users"></span>Staff</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>staff/">List</a></li>
                    <li><a href="<?=MODULE_ROOT;?>staff/edit/">New</a></li>
                    <li class="split-top"><a href="<?=MODULE_ROOT;?>staff/schedule/">Schedule</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['jobs']; ?>">
                <a href="<?=MODULE_ROOT;?>jobs/"><span class="developer"></span>Jobs</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>jobs/">List</a></li>
                    <li><a href="<?=MODULE_ROOT;?>jobs/edit/">New</a></li>
                    <li class="split-top"><a href="<?=MODULE_ROOT;?>jobs/schedule/">Schedule</a></li>
                    <li class="split-top"><a href="<?=MODULE_ROOT;?>jobs/routes/">Routes</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['time-tracking']; ?>">
                <a href="<?=MODULE_ROOT;?>time-tracking/"><span class="developer"></span>Time Tracking</a>
            </li>
            <li class="<?php echo $mmnsa['photos']; ?>">
                <a href="<?=MODULE_ROOT;?>photos/"><span class="developer"></span>Photos</a>
            </li>
            <li class="<?php echo $mmnsa['settings']; ?>">
                <a href="<?=MODULE_ROOT;?>settings/"><span class="developer"></span>Settings</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                    <li><a href="<?=MODULE_ROOT;?>settings/roles/">Roles</a></li>
                    <li><a href="<?=MODULE_ROOT;?>settings/shifts/">Shifts</a></li>
                    <li><a href="<?=MODULE_ROOT;?>settings/routes/">Routes</a></li>
                </ul>
            </li>
            <? /*
            <li>
                <a href="<?=MODULE_ROOT;?>settings/" class="<?php echo $mmnsa['settings']; ?>"><span class="settings"></span>Settings</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                </ul>
            </li>
            */ ?>
        </ul>
    </section>
</nav>
