<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Staff - Schedule';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Schedule', 'link' => $_uccms_staff->Extension. '/' .$bigtree['path'][2] ];

/*
// ROLES
$roles = $_uccms_staff->getRoles();

// STAFF
$staffs = $_uccms_staff->getStaffs();
*/

?>

<style type="text/css">

    #schedule .colSchedule .loading {
        padding: 15px;
        text-align: center;
    }
    #schedule .colSchedule .loading i {
        margin-right: 3px;
    }

</style>

<script type="text/javascript">

    $(function() {

        getSchedule();

    });

    function getSchedule(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.staff/ajax/admin/schedule/main/', $('#schedule').serialize(), function(data) {
            $('#schedule .colSchedule > .loading').hide();
            $('#schedule .colSchedule > .data').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<form id="schedule">

    <input type="hidden" name="date" value="<?php echo ($_REQUEST['date'] ? date('Y-m-d', strtotime($_REQUEST['date'])) : date('Y-m-d')); ?>" />
    <input type="hidden" name="view" value="week" />

    <div class="row">

        <div class="colSchedule col-md-10">

            <div class="loading">
                <i class="fas fa-spinner fa-spin"></i> Loading..
            </div>

            <div class="data">

            </div>

        </div>

        <div class="colFilter col-md-2">

            <div class="option shift">
                <input type="hidden" name="shift" value="" />
                <div class="form-group">
                    <label>Shift</label>
                    <select name="shift_id" class="custom_control form-control">
                        <option value="">All</option>
                        <?php foreach ($_uccms_staff->getShifts() as $shift) { ?>
                            <option value="<?php echo $shift['id']; ?>"><?php echo stripslashes($shift['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="option role">
                <input type="hidden" name="role" value="" />
                <div class="form-group">
                    <label>Role</label>
                    <select name="role_id" class="custom_control form-control">
                        <option value="">All</option>
                        <?php foreach ($_uccms_staff->getRoles() as $role) { ?>
                            <option value="<?php echo $role['id']; ?>"><?php echo stripslashes($role['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="option location">
                <input type="hidden" name="location" value="" />
                <div class="form-group">
                    <label>Location</label>
                    <select name="location_id" class="custom_control form-control">
                        <option value="">All</option>
                    </select>
                </div>
            </div>

        </div>

    </div>

</form>