<?php

$extension_nav =
["link" => "com.umbrella.staff*staff", "title" => "Staff", "access" => 1, "children" => [
    ["link" => ".", "title" => "Dashboard", "access" => 1],
    ["link" => "staff", "title" => "Staff", "access" => 1],
    ["link" => "jobs", "title" => "Jobs", "access" => 1],
    ["link" => "time-tracking", "title" => "Time Tracking", "access" => 1],
    ["link" => "photos", "title" => "Photos", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>