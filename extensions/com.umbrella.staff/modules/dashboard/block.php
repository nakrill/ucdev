<?php

/*

// MODULE CLASS
//$_uccms_staff = new uccms_Staff;

// HAS ACCESS
if ($_uccms_staff->adminModulePermission()) {

    ?>

    <style type="text/css">

        #block_staff #latest_staff header span, #block_staff #latest_staff .item section {
            text-align: left;
        }

        #block_staff #latest_staff .ad_titlex {
            width: 340px;
        }
        #block_staff #latest_staff .ad_area {
            width: 400px;
        }
        #block_staff #latest_staff .ad_tags {
            width: 200px;
        }
        #block_staff #latest_staff .ad_date {
            width: 130px;
        }
        #block_staff #latest_staff .ad_status {
            width: 90px;
        }
        #block_staff #latest_staff .ad_edit {
            width: 55px;
        }
        #block_staff #latest_staff .ad_delete {
            width: 55px;
        }

        #block_staff #latest_staff .items .ad_area .area_status {
            font-size: 1em !important;
        }
        #block_staff #latest_staff .items .ad_area .area_status.status-active {
            color: #81c784;
        }
        #block_staff #latest_staff .items .ad_area .area_status.status-inactive {
            color: #bbb;
        }
        #block_staff #latest_staff .items .ad_area .area_status.status-scheduled {
            color: #81d4fa;
        }
        #block_staff #latest_staff .items .ad_area .area_status.status-expired {
            color: #ffcc80;
        }

    </style>

    <div id="block_staff" class="block">
        <h2><a href="../<?php echo $extension; ?>*staff/">Staff</a></h2>
        <div id="latest_staff" class="table">
            <summary>
                <h2>Latest Staff</h2>
                <a class="add_resource add" href="../<?php echo $extension; ?>*staff/staff/">
                    View All
                </a>
            </summary>
            <header style="clear: both;">
                <span class="ad_titlex">Name</span>
                <span class="ad_area">Info</span>
                <span class="ad_status">Status</span>
                <span class="ad_edit">Edit</span>
                <span class="ad_delete">Delete</span>
            </header>
            <ul class="items">
                <?php
                $_GET['base_url'] = '../' .$extension. '*staff/staff';
                $_GET['limit'] = 5;
                include($extension_dir. '/ajax/admin/staff/get-page.php');
                ?>
            </ul>
        </div>
    </div>

    <?php

}

//unset($_uccms_staff);

*/

?>