<?php

class global_uccms_FormBuilder extends uccms_FormBuilder {


    /*
    #############################
    # GENERAL: Sitemap
    #############################

    public function general_sitemap($page=array()) {
        global $bigtree;

        $out = array();

        // PLANS
        $query = "SELECT * FROM `" .$this->tables['plans']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => WWW_ROOT . $this->frontendPath() . $this->planURL($r['id'], $r)
            );
        }

        return $out;

    }
    */


    /*
    #############################
    # GENERAL: Search
    #############################

    public function general_search($q='', $params=array()) {

        $results = array();

        // GET MATCHING PLANS
        $results_query = "SELECT `id`, `slug`, `title` FROM `" .$this->tables['plans']. "` WHERE (`title` LIKE '%" .$q. "%') AND (`active`=1) ORDER BY `id` DESC LIMIT " .(int)$params['limit'];
        $results_q = sqlquery($results_query);
        while ($row = sqlfetch($results_q)) {
            if ($row['slug']) {
                $slug = stripslashes($row['slug']);
            } else {
                $slug = $this->makeRewrite($row['title']);
            }
            $results[] = array(
                'title' => stripslashes($row['title']),
                'url'   => WWW_ROOT . $this->frontendPath() . $this->planURL($row['id'], $row),
                'type'  => 'membership'
            );
        }

        return $results;

    }
    */


    /*
    #############################
    # GENERAL: Cron
    #############################

    // RUN THE MAIN CRON
    public function runCron() {

    }
    */

    /*
    #############################
    # PAYMENT - Transactions
    #############################

    public function payment_transactions($vars=array()) {

        $out = array();

        $wa = array();

        if ($vars['query']['orderby']['field'] == 'dt') {
            $orderby_sql = " ORDER BY `dt` " .($vars['query']['orderby']['direction'] == "desc" ? "DESC" : "ASC"). " ";
        }

        if ($vars['query']['limit']['num'] > 0) {
            $limit_sql = " LIMIT " .(int)$vars['query']['limit']['num']. " ";
        }

        $accounta = array();

        // GET TRANSACTION LOGS
        $trans_query = "SELECT * FROM `" .$this->tables['transaction_log']. "` " .$orderby_sql . $limit_sql;
        $trans_q = sqlquery($trans_query);
        while ($trans = sqlfetch($trans_q)) {

            // HAVE ACCOUNT ID
            if (($trans['account_id']) && (!$accounta[$trans['account_id']])) {

                // GET ACCOUNT INFO
                $account_query = "
                SELECT *
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$trans['account_id']. ")
                ";
                $account_q = sqlquery($account_query);
                $accounta[$trans['account_id']] = sqlfetch($account_q);

            }

            if ($accounta[$trans['account_id']]['id']) {
                $account = $accounta[$trans['account_id']];
            } else {
                $account = array();
            }

            $out[] = array(
                'number'        => $account['id'],
                'dt'            => $trans['dt'],
                'name'          => trim(stripslashes($account['firstname']. ' ' .$account['lastname'])),
                'method'        => $trans['method'],
                'description'   => 'Membership',
                'amount'        => $trans['amount'],
                'lastfour'      => $trans['lastfour'],
                'url'           => array(
                    'edit' => '/admin/' .$this->Extension. '*membership/members/edit/?id=' .$account['id']
                ),
            );

        }

        return $out;

    }
    */

}

?>