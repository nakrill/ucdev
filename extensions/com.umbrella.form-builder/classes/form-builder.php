<?

class uccms_FormBuilder extends BigTreeModule {

    var $Table = "";

    var $Extension = "com.umbrella.form-builder";

    // DB TABLES
    var $tables = array(
        'entries'           => 'uccms_formbuilder_entries',
        'fields'            => 'uccms_formbuilder_fields',
        'forms'             => 'uccms_formbuilder_forms',
        'settings'          => 'uccms_formbuilder_settings',
        'transaction_log'   => 'uccms_formbuilder_transaction_log',
    );

    var $settings;

    // START AS 0, WILL ADJUST LATER
    static $PerPage = 0;

    var $SearchPageCount = false;


    public function __construct() {

    }


    #############################
    # GENERAL - MISC
    #############################


    // FRONTEND PATH
    public function frontendPath() {
        if (!isset($this->settings['frontend_path'])) {
            $this->settings['frontend_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*form-builder')"))['path']);
        }
        if (!$this->settings['frontend_path']) $this->settings['frontend_path'] = 'directory';
        return $this->settings['frontend_path'];
    }


    // MODULE ID
    public function moduleID() {
        if (!isset($this->settings['module_id'])) {
            $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
        }
        return $this->settings['module_id'];
    }


    // ADMIN USER ID
    public function adminID() {
        return $_SESSION['bigtree_admin']['id'];
    }


    // ADMIN ACCESS LEVEL
    public function adminModulePermission() {
        global $admin;
        if (!isset($this->settings['admin_module_permission'])) {
            $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
        }
        return $this->settings['admin_module_permission'];
    }


    // STRING IS JSON
    public function isJson($string) {
        if (substr($string, 0, 2) == '{"') {
            return true;
        }
    }


    // CONVERTS A STRING INTO A REWRITE-ABLE URL
    public function makeRewrite($string) {

        $string = strtolower(trim($string));

        // REMOVE ALL QUOTES
        $string = str_replace("'", '', $string);
        $string = str_replace('"', '', $string);

        // STRIP ALL NON WORD CHARS
        $string = preg_replace('/\W/', ' ', $string);

        // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
        $string = preg_replace('/\ +/', '-', $string);

        // TRIM DASHES
        $string = preg_replace('/\-$/', '', $string);
        $string = preg_replace('/^\-/', '', $string);

        return $string;

    }


    // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
    public function imageBridgeIn($string) {
        $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
        // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
        if (substr($string, 0, 3) == 'http') {
            return $string;
        } else {
            $dpa = explode('/', $string);
            return array_pop($dpa);
            //$what = array_pop($dpa);
        }
    }


    // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
    public function imageBridgeOut($string, $what, $file=false) {
        $string = stripslashes($string);
        // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
        if (substr($string, 0, 3) == 'http') {
            return $string;
        } else {
            $base = ($file ? SITE_ROOT : STATIC_ROOT);
            return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
        }
    }


    // CONVERT BIGTREE FILE LOCATION TO FULL URL
    public function bigtreeFileURL($file) {
        return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
    }


    // SET NUMBER PER PAGE
    public function setPerPage($num) {
        static::$PerPage = (int)$num;
    }


    // NUMBER PER PAGE
    public function perPage() {
        global $cms;
        if (!static::$PerPage) {
            $pp = $this->getSetting('forms_per_page');
            if ($pp) {
                $this->setPerPage($pp);
            }
        }
        if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
        if (!static::$PerPage) $this->setPerPage(15);
        return static::$PerPage;
    }


    // GET NUMBER OF PAGES FROM QUERY
    public function pageCount($num) {
        return ceil($num / $this->perPage());
    }


    // TRIM URL
    public function trimURL($string) {
        return trim(preg_replace('#^https?://#', '', $string));
    }


    // CONVERT ARRAY TO CSV
    public function arrayToCSV($array=array()) {
        if (is_array($array)) {
            return implode(',', $array);
        }
    }


    // CREATE ARRAY OF TIMEZONES
    public function timezoneArray($show_time=true, $show_offset=true) {
        $tz_list = DateTimeZone::listAbbreviations();
        $tz_idents = DateTimeZone::listIdentifiers();
        $tz_data = $offset = $added = array();
        foreach ($tz_list as $abbr => $info) {
            foreach ($info as $zone) {
                if ((!empty($zone['timezone_id'])) && (!in_array($zone['timezone_id'], $added)) && (in_array($zone['timezone_id'], $tz_idents))) {
                    $z = new DateTimeZone($zone['timezone_id']);
                    $c = new DateTime(null, $z);
                    $zone['time'] = $c->format('h:i A');
                    $zone['offset'] = $z->getOffset($c);
                    $tz_data[] = $zone;
                    $offset[] = $z->getOffset($c);
                    $added[] = $zone['timezone_id'];
                }
            }
        }
        array_multisort($offset, SORT_ASC, $tz_data);
        $retval = array();
        foreach ($tz_data as $key => $row) {
            $out = '';
            if ($show_time) {
                $out .= $row['time']. ' - ';
            }
            $out .= $row['timezone_id'];
            if ($show_offset) {
                $out .= ' (' .$this->timezone_formatOffset($row['offset']). ')';
            }
            $retval[$row['timezone_id']] = $out;
        }
        return $retval;
    }


    // FORMAT TIMEZONE OFFSET
    public function timezone_formatOffset($offset) {
        $hours = $offset / 3600;
        $remainder = $offset % 3600;
        $sign = $hours > 0 ? '+' : '-';
        $hour = (int) abs($hours);
        $minutes = (int) abs($remainder / 60);
        if ($hour == 0 && $minutes == 0) {
            $sign = ' ';
        }
        return 'GMT' . $sign . str_pad($hour, 2, '0', STR_PAD_LEFT).':'. str_pad($minutes,2, '0');
    }


    // GET TIMEZONE
    public function timezone() {
        $timezone = $this->getSetting('timezone');
        if (!$timezone) $timezone = 'America/New_York';
        return $timezone;
    }


    #############################
    # GENERAL - SETTINGS
    #############################


    // GET SETTING
    public function getSetting($id='') {
        $id = sqlescape($id);
        if ($id) {
            if (isset($this->settings[$id])) {
                return $this->settings[$id];
            } else {
                $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tabless['settings']. "` WHERE `id`='" .$id. "'"));
                if ($this->isJson($f['value'])) {
                    $val = json_decode($f['value'], true);
                } else {
                    $val = stripslashes($f['value']);
                }
                $this->settings[$id] = $val;
            }
            return $val;
        }
    }


    // GET SETTINGS
    public function getSettings($array='') {
        $out = array();
        if (is_array($array)) {
            $oa = array();
            foreach ($array as $id) {
                $id = sqlescape($id);
                if ($id) {
                    $oa[] = "`id`='" .$id. "'";
                }
            }
            if (count($oa) > 0) {
                $or = "(" .implode(") OR (", $oa). ")";
            }
            if ($or) {
                $query = "SELECT `id`, `value` FROM `" .$this->tabless['settings']. "` WHERE " .$or;
            }
        } else {
            $query = "SELECT `id`, `value` FROM `" .$this->tabless['settings']. "` ORDER BY `id` ASC";
        }
        if ($query) {
            $q = sqlquery($query);
            while ($f = sqlfetch($q)) {
                if ($this->isJson($string)) {
                    $val = json_decode($f['value']);
                } else {
                    $val = stripslashes($f['value']);
                }
                $this->settings[$f['id']] = $val;
                $out[$f['id']] = $val;
            }
        }
        return $out;
    }


    // SET SETTING
    public function setSetting($id='', $value='') {

        // CLEAN UP
        $id = sqlescape($id);

        // HAVE ID
        if ($id) {

            // VALUE IS ARRAY
            if (is_array($value)) {
                $value = json_encode($value);

            // VALUE IS STRING
            } else {
                $value = sqlescape($value);
            }

            $query = "
            INSERT INTO `" .$this->tabless['settings']. "`
            SET `id`='" .$id. "', `value`='" .$value. "'
            ON DUPLICATE KEY UPDATE `value`='" .$value. "'
            ";
            if (sqlquery($query)) {
                return true;
            }

        }

    }


    #############################
    # FORMS
    #############################


    /*
        Function: getForm
            Gets form information and related fields.

        Parameters:
            id - The form ID

        Returns:
            A form array or false if the form doesn't exist.
    */

    public function getForm($id) {
        $id = sqlescape($id);
        $form = sqlfetch(sqlquery("SELECT * FROM `" .$this->tables['forms']. "` WHERE id = '$id'"));
        if (!$form) {
            return false;
        }

        $fields = array();
        $object_count = 0;
        $field_query = sqlquery("SELECT * FROM `" .$this->tables['fields']. "` WHERE form = '$id' AND `column` = '0' ORDER BY position DESC, id ASC");
        while ($field = sqlfetch($field_query)) {
            $object_count++;

            if ($field["type"] == "column") {
                // Get left column
                $column_fields = array();
                $column_query = sqlquery("SELECT * FROM `" .$this->tables['fieds']. "` WHERE `column` = '".$field["id"]."' AND `alignment` = 'left' ORDER BY position DESC, id ASC");
                while ($sub_field = sqlfetch($column_query)) {
                    $column_fields[] = $sub_field;
                    $object_count++;
                }
                $field["fields"] = $column_fields;
                $fields[] = $field;

                // Get right column
                $column_fields = array();
                $column_query = sqlquery("SELECT * FROM `" .$this->tables['fields']. "` WHERE `column` = '".$field["id"]."' AND `alignment` = 'right' ORDER BY position DESC, id ASC");
                while ($sub_field = sqlfetch($column_query)) {
                    $column_fields[] = $sub_field;
                    $object_count++;
                }
                $field["fields"] = $column_fields;
                $fields[] = $field;

                // Column start/end count as objects so we add 3 since there's two columns
                $object_count += 3;
            } else {
                $fields[] = $field;
            }
        }

        $form["fields"] = $fields;
        $form["object_count"] = $object_count - 1; // We start at 0
        return $form;
    }

    /*
        Function: getAllForms
            Returns all forms (without fields).

        Parameters:
            sort - Sort order (defaults to id ASC)

        Returns:
            An array of form arrays.
    */

    public function getAllForms($sort = "id ASC") {
        $mod = new BigTreeModule($this->tables['forms']);
        return $mod->getAll($sort);
    }

    /*
        Function: getEntries
            Returns a form's user entries, most recent first.

        Parameters:
            id - Form ID

        Returns:
            An array of entries arrays.
    */

    public function getEntries($id) {
        $mod = new BigTreeModule($this->tables['entries']);
        return $mod->getMatching("form",$id,"id DESC");
    }

    /*
        Function: getEntry
            Returns a user entry.

        Parameters:
            id - Entry ID

        Returns:
            An array.
    */

    public function getEntry($id) {

        $out = array();

        $mod = new BigTreeModule($this->tables['entries']);
        $out = $mod->get($id);

        if ($out['data']['payment']['card']['expiration']) {
            $out['data']['payment']['card']['month'] = date('m', strtotime($out['data']['payment']['card']['expiration']));
            $out['data']['payment']['card']['year'] = date('Y', strtotime($out['data']['payment']['card']['expiration']));
        }

        return $out;

    }

    /*
        Function: searchEntries
            Searches a form's entries and returns a page of 15 ordered by most recent first.
            Sets FormBuilder::$SearchPageCount to the number of pages of results.

        Parameters:
            id - Form ID
            query - Search terms
            page - Page to return (beginning with 1 as first page)

        Returns:
            An array of entries.
    */

    public function searchEntries($id=0, $query, $page=1, $limit=15) {
        $mod = new BigTreeModule($this->tables['entries']);
        $results = $mod->search($query,"id DESC");
        $form_results = array();
        foreach ($results as $result) {
            if ((!$id) || ($result["form"] == $id)) {
                $form_results[] = $result;
            }
        }
        if (!$limit) $limit = 15;
        if (count($form_results) > 0) {
            $pages = ceil(count($form_results) / $limit);
            $this->SearchPageCount = $pages ? $pages : 1;
            return array_slice($form_results, ($page - 1) * $limit, $limit);
        }
    }


    #############################
    # PAYMENT
    #############################

    /*
    // ALL AVAILABLE PAYMENT METHODS
    public function allPaymentMethods() {
        $out = array(
            'authorize'     => 'Authorize.Net',
            'paypal-rest'   => 'PayPal REST API',
            'paypal'        => 'PayPal Payments Pro',
            'payflow'       => 'PayPal Payflow Gateway',
            'linkpoint'     => 'First Data / LinkPoint',
            'stripe'        => 'Stripe',
            'instapay'      => 'InstaPay',
            'check_cash'    => 'Check / Cash'
        );
        return $out;
    }


    // GET PAYMENT METHODS FROM DB
    public function getPaymentMethods($active=false) {
        $out = array();
        if ($active) {
            $where = "WHERE (`active`=1)";
        }
        $query = "SELECT * FROM `" .$this->tables['payment_methods']. "` " .$where;
        $q = sqlquery($query);
        while ($f = sqlfetch($q)) {
            $out[$f['id']] = $f;
        }
        return $out;
    }
    */

    // LOG TRANSACTION
    public function logTransaction($vars) {
        if (!$vars['dt']) {
            $vars['dt'] = date('Y-m-d H:i:s');
        }
        $query = "INSERT INTO `" .$this->tables['transaction_log']. "` SET " .uccms_createSet($vars);
        if (sqlquery($query)) {
            return true;
        }
    }


    #############################
    # REVSOCIAL
    #############################


    // RECORD ORDER IN REVSOCIAL
    public function revsocial_recordOrder($order_id, $customer_id, $vars, $import=false) {
        global $cms;

        // REVSOCIAL API KEY
        $api_key = $cms->getSetting('rs_api-key');

        // HAVE API KEY
        if ($api_key) {

            // ORDER INFO
            $data = array(
                'order' => array(
                    'id' => $order_id,
                    'timestamp' => date('c'), // ISO 8601 formatted date
                    'customer' => array(
                        'id'            => $customer_id,
                        'firstname'     => $vars['billing']['firstname'],
                        'lastname'      => $vars['billing']['lastname'],
                        'middlename'    => $vars['billing']['middlename'],
                        'prefix'        => $vars['billing']['prefix'],
                        'suffix'        => $vars['billing']['suffix'],
                        'email'         => $vars['billing']['email'],
                        'phone'         => $vars['billing']['phone'],
                        'dob'           => $vars['billing']['dob'],
                        'gender'        => $vars['billing']['gender'],
                        'country'       => $vars['billing']['country'],
                        'state'         => $vars['billing']['state'],
                        'city'          => $vars['billing']['city'],
                        'zip'           => $vars['billing']['zip'],
                        'street'        => trim($vars['billing']['address1']. ' ' .$vars['billing']['address2']),
                        'company'       => $vars['billing']['company']
                    ),
                    'billing' => array(
                        'firstname'     => $vars['billing']['firstname'],
                        'lastname'      => $vars['billing']['lastname'],
                        'middlename'    => $vars['billing']['middlename'],
                        'prefix'        => $vars['billing']['prefix'],
                        'suffix'        => $vars['billing']['suffix'],
                        'email'         => $vars['billing']['email'],
                        'phone'         => $vars['billing']['phone'],
                        'dob'           => $vars['billing']['dob'],
                        'gender'        => $vars['billing']['gender'],
                        'country'       => $vars['billing']['country'],
                        'state'         => $vars['billing']['state'],
                        'city'          => $vars['billing']['city'],
                        'zip'           => $vars['billing']['zip'],
                        'street'        => trim($vars['billing']['address1']. ' ' .$vars['billing']['address2']),
                        'company'       => $vars['billing']['company']
                    ),
                    'shipping' => array(
                        'firstname'     => $vars['shipping']['firstname'],
                        'lastname'      => $vars['shipping']['lastname'],
                        'middlename'    => $vars['shipping']['middlename'],
                        'prefix'        => $vars['shipping']['prefix'],
                        'suffix'        => $vars['shipping']['suffix'],
                        'email'         => $vars['shipping']['email'],
                        'phone'         => $vars['shipping']['phone'],
                        'dob'           => $vars['shipping']['dob'],
                        'gender'        => $vars['shipping']['gender'],
                        'country'       => $vars['shipping']['country'],
                        'state'         => $vars['shipping']['state'],
                        'city'          => $vars['shipping']['city'],
                        'zip'           => $vars['shipping']['zip'],
                        'street'        => trim($vars['shipping']['address1']. ' ' .$vars['shipping']['address2']),
                        'company'       => $vars['shipping']['company']
                    ),
                    'subtotal'          => $vars['order']['subtotal'],
                    'coupon_code'       => $vars['order']['coupon_code'],
                    'discount_amount'   => $vars['order']['discount_amount'],
                    'shipping_amount'   => $vars['order']['shipping'],
                    'tax_amount'        => $vars['order']['tax'],
                    'order_total'       => $vars['order']['total'],
                    'paid'              => $vars['order']['paid'],
                    'currency_code'     => 'USD',
                    'shipping_method'   => $vars['order']['shipping_method'],
                    'transaction_id'    => $vars['order']['transaction_id'],
                    'total_products'    => $vars['items']['num_products'],
                    'total_items'       => $vars['items']['num_quantity'],
                    'products'          => $vars['items']['products']
                )
            );

            // IS IMPORTING
            if ($import) {
                $data['ip'] = $vars['order']['ip'];

            // IS NOT IMPORTING
            } else {

                // NOT ADMIN
                if (!$this->adminID()) {

                    $data['ip'] = $_SERVER['REMOTE_ADDR'];
                    $data['session'] = $_COOKIE['__rstid'];

                }

            }

            // REST CLIENT
            require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

            // INIT REST API CLASS
            $_api = new RestClient(array(
                'base_url' => 'https://api.revsocial.com/v1'
            ));

            // MAKE API CALL
            $result = $_api->post('ecommerce/addOrder', array(
                'api_key'   => $api_key,
                'data'      => $data
            ));

        }

    }


}

?>