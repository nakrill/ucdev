<?php

$extension_nav =
["link" => "com.umbrella.form-builder*form-builder", "title" => "Form Builder", "access" => 1, "children" => [
    ["link" => ".", "title" => "Forms", "access" => 1],
    ["link" => "add", "title" => "New Form", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>