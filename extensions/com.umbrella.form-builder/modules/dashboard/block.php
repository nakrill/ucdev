<?php

// MODULE CLASS
if (!$_uccms_formbuilder) $_uccms_formbuilder= new uccms_FormBuilder;

// HAS ACCESS
if ($_uccms_formbuilder->adminModulePermission()) {

    // GET LATEST SUBMISSIONS
    $sub_query = "SELECT * FROM `" .$_uccms_formbuilder->tables['entries']. "` ORDER BY `id` DESC LIMIT 5";
    $sub_q = sqlquery($sub_query);

    ?>

    <style type="text/css">

        #block_formbuilder #latest_submissions header span, #block_formbuilder #latest_submissions .item section {
            text-align: left;
        }

        #block_formbuilder #latest_submissions .submission_form {
            width: 200px;
        }
        #block_formbuilder #latest_submissions .submission_date {
            width: 140px;
        }
        #block_formbuilder #latest_submissions .submission_data {
            width: 490px;
        }
        #block_formbuilder #latest_submissions .submission_edit {
            width: 55px;
        }
        #block_formbuilder #latest_submissions .submission_delete {
            width: 55px;
        }

    </style>

    <div id="block_formbuilder" class="block">
        <h2><a href="../<?php echo $extension; ?>*form-builder/">Form Builder</a></h2>
        <div id="latest_submissions" class="table">
            <summary>
                <h2>Latest Submissions</h2>
                <a class="add_resource add" href="../<?php echo $extension; ?>*form-builder/">
                    View All
                </a>
            </summary>
            <header style="clear: both;">
                <span class="submission_date">Date</span>
                <span class="submission_form">Form</span>
                <span class="submission_data">Data</span>
                <span class="submission_edit">View</span>
                <span class="submission_delete">Delete</span>
            </header>
            <?php if (sqlrows($sub_q) > 0) { ?>
                <ul id="results" class="items">
                    <?php
                    //$_REQUEST['base_url'] = '../' .$extension. '*form-builder';

                    // ARRAY OF FORMS
                    $forma = array();

                    // LOOP THROUGH SUBMISSIONS
                    while ($sub = sqlfetch($sub_q)) {

                        // NO FORM INFO
                        if (!$forma[$sub['form']]) {

                            // GET FORM INFO
                            $form_query = "SELECT * FROM `" .$_uccms_formbuilder->tables['forms']. "` WHERE (`id`=" .$sub['form']. ")";
                            $form_q = sqlquery($form_query);
                            $forma[$sub['form']] = sqlfetch($form_q);

                        }

                        // CLEAN UP DATA
                        $data = json_decode(str_replace("\n", '<br />', stripslashes(str_replace('\"', '&quot;', $sub['data']))), true);
                        $data = recursive_implode($data, ' | ', false, false);

                        ?>
                        <li class="item">
                            <section class="submission_date">
                                <?php echo date('n/j/Y g:i A', strtotime($sub['created_at'])); ?>
                            </section>
                            <section class="submission_form">
                                <a href="../<?php echo $extension; ?>*form-builder/edit/<?php echo $sub['form']; ?>/"><?php echo stripslashes($forma[$sub['form']]['title']); ?></a>
                            </section>
                            <section class="submission_data">
                                <?php echo $data; ?>
                            </section>
                            <section class="submission_edit">
                                <a class="icon_view_details" title="View Submission" href="../<?php echo $extension; ?>*form-builder/view-entry/<?php echo $sub['id']; ?>/"></a>
                            </section>
                            <section class="submission_delete">
                                <a href="../<?php echo $extension; ?>*form-builder/delete-entry/?id=<?php echo $sub['id']; ?>&form_id=<?php echo $sub['form']; ?>" class="icon_delete" title="Delete Submission" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
                            </section>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            <?php } else { ?>
                <div style="padding: 10px 0; text-align: center;">
                    No submissions.
                </div>
            <?php } ?>
        </div>
    </div>

    <?php

}

unset($_uccms_formbuilder);

function recursive_implode(array $array, $glue = ',', $include_keys=false, $trim_all=true) {
    $glued_string = '';
    array_walk_recursive($array, function($value, $key) use ($glue, $include_keys, &$glued_string) {
        $include_keys and $glued_string .= $key.$glue;
        $glued_string .= $value.$glue;
    });
    strlen($glue) > 0 and $glued_string = substr($glued_string, 0, -strlen($glue));
    $trim_all and $glued_string = preg_replace("/(\s)/ixsm", '', $glued_string);
    return (string) $glued_string;
}

?>