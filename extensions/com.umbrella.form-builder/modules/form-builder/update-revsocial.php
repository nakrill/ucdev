<?php

// CLEAN UP
$id = (int)$_POST['id'];

// HAVE FORM ID
if ($id) {

    // GET CURRENT INFO
    $form_query = "SELECT * FROM `" .$_uccms_formbuilder->tables['forms']. "` WHERE (`id`=" .$id. ")";
    $form_q = sqlquery($form_query);
    $form = sqlfetch($form_q);

    // REVSOCIAL INFO
    if ($form['revsocial']) {
        $form['revsocial'] = json_decode(stripslashes($form['revsocial']), true);
    }

    // REVSOCIAL INFO
    $form['revsocial']['form_id'] = (int)$_POST['revsocial']['form_id'];

    // HAVE FIELDS
    if (count($_POST['fields']) > 0) {

        // LOOP THROUGH FIELDS
        foreach ($_POST['fields'] as $bt_field => $rs_field) {
            if (is_array($rs_field)) {
                $form['revsocial']['map'][$bt_field] = $rs_field;
            } else {
                $form['revsocial']['map'][$bt_field] = (int)$rs_field;
            }
        }

    // NO FIELDS
    } else {
        $form['revsocial']['map'] = '';
    }

    // DB COLUMNS
    $columns = array(
        'revsocial' => json_encode($form['revsocial'])
    );

    // DB QUERY
    $query = "UPDATE `" .$_uccms_formbuilder->tables['forms']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";
    if (sqlquery($query)) {
        $admin->growl('Form Builder', 'RevSocial info updated.');
    } else {
        $admin->growl('Form Builder', 'Failed to update RevSocial info.');
    }

    BigTree::redirect(MODULE_ROOT. 'edit/' .$id. '/?view_data=' .$_POST['view_data']. '#revsocial');

// NO FORM ID
} else {
    $admin->growl('Form Builder', 'Form ID not specified.');
    BigTree::redirect(MODULE_ROOT);
}

?>