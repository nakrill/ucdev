<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE ENTRY
    $query = "DELETE FROM `" .$_uccms_formbuilder->tables['entries']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Submission', 'Submission deleted.');

        // UPDATE FORM COUNT
        sqlquery("UPDATE `" .$_uccms_formbuilder->tables['forms']. "` SET entries=(entries-1) WHERE (id=" .(int)sqlescape($_GET['form_id']). ")");
        BigTreeAutoModule::recacheItem($_GET['form_id'], $_uccms_formbuilder->tables['forms']);

    // QUERY FAILED
    } else {
        $admin->growl('Delete Submission', 'Failed to delete submission.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Submission', 'No submission specified.');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT. '/entries/' .$_GET['form_id']. '/');
}

?>