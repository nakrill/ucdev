<?php

//echo 'un-exit';
//exit;

// ARRAY OF FORMS
$forma = array();

$num_success = 0;

// GET ENTRIES
$entries_query = "SELECT `id` FROM `" .$_uccms_formbuilder->tables['entries']. "`";
$entries_q = sqlquery($entries_query);
while ($entry = sqlfetch($entries_q)) {

    // GET FULL ENTRY
    $entry = $_uccms_formbuilder->getEntry($entry['id']);

    //echo print_r($entry);
    //exit;

    // HAS PAYMENT INFO
    if (is_array($entry['data']['payment'])) {

        //echo print_r($entry);

        $data       = $entry['data'];
        $payment    = $entry['data']['payment'];

        // FORM INFO
        if (!$forma[$entry['form']]) {
            $forma[$entry['form']] = $_uccms_formbuilder->getForm($entry['form']);
        }
        $form = $forma[$entry['form']];

        //echo print_r($form);
        //exit;

        $fielda = array();

        // FORM HAS FIELDS
        if (is_array($form['fields'])) {
            foreach ($form['fields'] as $field) {
                $field['data'] = json_decode($field['data'], true);
                if (is_array($field['data']['list'])) {
                    foreach ($field['data']['list'] as $item) {
                        $field['prices'][$item['value']] = $item['price'];
                    }
                }
                $fielda[$field['id']] = $field;
            }
        }

        //echo print_r($fielda);
        //exit;

        if ($payment['card']['expiration']) {
            $expiration = $payment['card']['expiration'];
        } else {
            $expiration = $payment['card']['year']. '-' .$payment['card']['month']. '-28';
        }

        $amount = 0;

        //echo print_r($entry['data']);
        //exit;

        // PAID FORM
        if ($form['paid']) {
            $amount += str_replace(array('$', ','), '', $form['base_price']);
        }

        // LOOP THROUGH SUBMITTED DATA
        foreach ($entry['data'] as $field_id => $value) {
            if (is_array($value)) {
                foreach ($value as $val) {
                    $amount += str_replace(array('$', ','), '', $fielda[$field_id]['prices'][$val]);
                }
            } else if ($value) {
                if ($fielda[$field_id]['price']) {
                    $amount += str_replace(array('$', ','), '', $fielda[$field_id]['price']);
                }
            }
        }

        /*
        // HAVE FIELDS
        if (is_array($form['fields'])) {
            foreach ($form['fields'] as $field) {
                $fdata = json_decode($field['data'], true);
                if ($fdata['price']) {
                    if ($entry['data'][$field['id']]) {
                        $amount += $fdata['price'];
                    }
                }
                if (is_array($fdata['list'])) {
                    foreach ($fdata['list'] as $list) {
                        if ($list['price']) {
                            if ($entry['data'][$field['id']]['test']) {
                                $amount += $list['price'];
                            }
                        }

                    }
                }
            }
        }
        */

        $log_vars = array(
            'account_id'            => 0,
            'form_id'               => $entry['form'],
            'dt'                    => $entry['created_at'],
            'status'                => 'charged',
            'amount'                => number_format((float)$amount, 2, '.', ''),
            'method'                => 'credit-card',
            'payment_profile_id'    => (int)$payment['card']['id'],
            'name'                  => trim($payment['name']['first']. ' ' .$payment['name']['last']),
            'lastfour'              => $payment['card']['number'],
            'expiration'            => $expiration,
            'transaction_id'        => '',
            'message'               => '',
            'ip'                    => 0
        );

        // LOG TRANSACTION
        if ($_uccms_formbuilder->logTransaction($log_vars)) {
            $num_success++;
        }

    }

}

echo 'All done! ' .$num_success. ' added.';

?>