<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Form Builder';

// MODULE CLASS
if (!$_uccms_formbuilder) $_uccms_formbuilder = new uccms_FormBuilder();

$bigtree["css"][] = "form-builder.css";
$bigtree["js"][] = "form-builder.js";

// Create the settings array if it doesn't exist
if (!$admin->settingExists("settings")) {
	$admin->createSetting(array("id" => "settings","system" => "on","name" => "Form Builder Settings"));
}

$settings = (array)$cms->getSetting("settings");

?>