<?
    $entry = $_uccms_formbuilder->getEntry($bigtree["commands"][0]);
    $form = $_uccms_formbuilder->getForm($entry["form"]);

    function _local_draw_form_builder_form_fields($fields) {
        global $entry;
        foreach ($fields as $field) {
            $value = _local_recursive_clean($entry["data"][$field["id"]]);
            $field["data"] = json_decode($field["data"],true);
            $label = $field["data"]["label"] ? $field["data"]["label"] : ucwords($field["type"]);

            if ($field["type"] == "column") {
                _local_draw_form_builder_form_fields($field["fields"]);
            } elseif ($field["type"] != "section") {
                echo "<fieldset>";
                echo "<label><strong>$label</strong></label>";
                echo "<p>";
                if ($field["type"] == "name") {
                    if (is_array($value)) {
                        echo $value["first"]." ".$value["last"]."";
                    } else {
                        echo $value;
                    }
                } elseif ($field["type"] == "address") {
                    if (is_array($value)) {
                        echo $value["street"]."<br />";
                        if ($value["street2"]) {
                            echo $value["street2"]."<br />";
                        }
                        echo $value["city"].", ".$value["state"]." ".$value["zip"]."<br />".$value["country"]."";
                    } else {
                        echo $value;
                    }
                } elseif ($field["type"] == "checkbox") {
                    if (is_array($value)) {
                        echo implode(", ",$value)."";
                    } else {
                        echo $value;
                    }
                } elseif ($field["type"] == "upload") {
                    echo '<a href="'.$value.'">'.$value.'</a>';
                } else {
                    echo $value;
                }
                echo "</p>";
                echo "</fieldset>";
            }
        }
    }

    function _local_recursive_clean($val) {
        if (is_array($val)) {
            foreach ($val as $k => $v) {
                if (is_array($v)) {
                    $val[$k] = _local_recursive_clean($v);
                } else {
                    $val[$k] = htmlspecialchars(htmlspecialchars_decode(strip_tags($v)));
                }
            }
            return $val;
        } else {
            return htmlspecialchars(htmlspecialchars_decode(strip_tags($val)));
        }
    }
?>
<div class="container">
    <summary>
        <h2>Entry Details <small>from &ldquo;<?=$form["title"]?>&rdquo;</small></h2>
    </summary>
    <section>
        <fieldset>
            <label><strong>Date Created</strong></label>
            <p><?=date("F j, Y @ g:ia",strtotime($entry["created_at"]))?></p>
        </fieldset>
        <? _local_draw_form_builder_form_fields($form["fields"]) ?>
    </section>
</div>

<?php if (is_array($entry['data']['payment'])) { ?>
    <div class="container">
        <summary>
            <h2>Payment Details</h2>
        </summary>
        <section>
            <fieldset>
                <label><strong>Name</strong></label>
                <p><?=$entry['data']['payment']['name']['first']. ' ' .$entry['data']['payment']['name']['last']?></p>
            </fieldset>
            <fieldset>
                <label><strong>Address</strong></label>
                <p>
                    <?php
                    echo $entry['data']['payment']['address']['street'];
                    if ($entry['data']['payment']['address']['street2']) {
                        echo '<br />' .$entry['data']['payment']['address']['street2'];
                    }
                    echo '<br />' .$entry['data']['payment']['address']['city']. ', ' .$entry['data']['payment']['address']['state']. ' ' .$entry['data']['payment']['address']['zip'];
                    ?>
                </p>
            </fieldset>
            <fieldset>
                <label><strong>Card: Last 4</strong></label>
                <p><?=$entry['data']['payment']['card']['number']?></p>
            </fieldset>
            <fieldset>
                <label><strong>Card: Expiration</strong></label>
                <p><?=$entry['data']['payment']['card']['month']. '/' .$entry['data']['payment']['card']['year']?></p>
            </fieldset>
        </section>
    </div>
<?php } ?>