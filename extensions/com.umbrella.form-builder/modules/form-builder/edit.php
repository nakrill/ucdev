<?php

// GET FORM
$form = $_uccms_formbuilder->getForm($bigtree["commands"][0]);

?>

<div class="container">
	<form method="post" action="<?=MODULE_ROOT?>update/<?=$form["id"]?>/" class="module">
    <input type="hidden" name="view_data" value="<?php echo $_REQUEST['view_data']; ?>" />
	<?php include('_form.php'); ?>
	<footer>
		<input type="submit" class="button blue" value="Update" />
	</footer>
	</form>
</div>

<?php

// HAVE FORM ID & FIELDS
if (($form['id']) && (count($form['fields']) > 0)) {

    // REVSOCIAL API KEY
    $api_key = $cms->getSetting('rs_api-key');

    // HAVE API KEY
    if ($api_key) {

        // REVSOCIAL INFO
        $revsocial = json_decode($form['revsocial'], true);

        // REST CLIENT
        require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

        // INIT REST API CLASS
        $_api = new RestClient(array(
            'base_url' => 'https://api.revsocial.com/v1'
        ));

        ?>

        <a name="revsocial"></a>

        <div class="container">

            <header>
                <h2>RevSocial Form</h2>
            </header>

            <form method="post" action="<?=MODULE_ROOT?>update-revsocial/" class="module">
            <input type="hidden" name="id" value="<?php echo $form['id']; ?>" />
            <input type="hidden" name="view_data" value="<?php echo $_REQUEST['view_data']; ?>" />

            <section>

                <?php

                // MAKE API CALL
                $forms_result = $_api->get('form/forms', array(
                    'api_key'   => $api_key,
                    'data'      => array(
                        'active'    => true
                    )
                ));

                // API RESPONSE
                $forms_resp = $forms_result->decode_response();

                // HAVE FORMS
                if (count($forms_resp->forms) > 0) {

                    ?>

                    <fieldset class="form-group">
                        <label>Form</label>
                        <select name="revsocial[form_id]" class="form-control">
                            <option value="0">None</option>
                            <?php foreach ($forms_resp->forms as $rsform) { ?>
                                <option value="<?php echo stripslashes($rsform->id); ?>" <?php if ($rsform->id == $revsocial['form_id']) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsform->title); ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <?php

                    // HAVE REVSOCIAL FORM ID
                    if ($revsocial['form_id']) {

                        // MAKE API CALL
                        $fields_result = $_api->get('form/fields', array(
                            'api_key'   => $api_key,
                            'data'      => array(
                                'form'  => array(
                                    'id'    => $revsocial['form_id']
                                )
                            )
                        ));

                        // API RESPONSE
                        $fields_resp = $fields_result->decode_response();

                        // HAVE FORMS
                        if (count($fields_resp->fields) > 0) {

                            ?>

                            <label style="margin-top: 30px;">
                                Match form builder fields to RevSocial fields.
                            </label>

                            <table>

                                <?php

                                // LOOP THROUGH BIGTREE FIELDS
                                foreach ($form['fields'] as $field) {

                                    // FIELD DATA
                                    $fdata = json_decode($field['data'], true);

                                    // FIELD TYPE: NAME
                                    if ($field['type'] == 'name') {

                                        ?>
                                        <tr>
                                            <td>
                                                First Name
                                            </td>
                                            <td>
                                                <select name="fields[<?php echo $field['id']; ?>][0]" class="form-control">
                                                    <option value="0">None</option>
                                                    <?php foreach ($fields_resp->fields as $rsfield) { ?>
                                                        <option value="<?php echo $rsfield->id; ?>" <?php if ($rsfield->id == $revsocial['map'][$field['id']][0]) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsfield->title); ?> (<?php echo stripslashes($rsfield->type); ?>)</option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Last Name
                                            </td>
                                            <td>
                                                <select name="fields[<?php echo $field['id']; ?>][1]" class="form-control">
                                                    <option value="0">None</option>
                                                    <?php foreach ($fields_resp->fields as $rsfield) { ?>
                                                        <option value="<?php echo $rsfield->id; ?>" <?php if ($rsfield->id == $revsocial['map'][$field['id']][1]) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsfield->title); ?> (<?php echo stripslashes($rsfield->type); ?>)</option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php

                                    // FIELD TYPE: ADDRESS
                                    } else if ($field['type'] == 'address') {

                                        ?>
                                        <tr>
                                            <td>
                                                Address
                                            </td>
                                            <td>
                                                <select name="fields[<?php echo $field['id']; ?>][0]" class="form-control">
                                                    <option value="0">None</option>
                                                    <?php foreach ($fields_resp->fields as $rsfield) { ?>
                                                        <option value="<?php echo $rsfield->id; ?>" <?php if ($rsfield->id == $revsocial['map'][$field['id']][0]) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsfield->title); ?> (<?php echo stripslashes($rsfield->type); ?>)</option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                City
                                            </td>
                                            <td>
                                                <select name="fields[<?php echo $field['id']; ?>][1]" class="form-control">
                                                    <option value="0">None</option>
                                                    <?php foreach ($fields_resp->fields as $rsfield) { ?>
                                                        <option value="<?php echo $rsfield->id; ?>" <?php if ($rsfield->id == $revsocial['map'][$field['id']][1]) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsfield->title); ?> (<?php echo stripslashes($rsfield->type); ?>)</option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                State
                                            </td>
                                            <td>
                                                <select name="fields[<?php echo $field['id']; ?>][2]" class="form-control">
                                                    <option value="0">None</option>
                                                    <?php foreach ($fields_resp->fields as $rsfield) { ?>
                                                        <option value="<?php echo $rsfield->id; ?>" <?php if ($rsfield->id == $revsocial['map'][$field['id']][2]) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsfield->title); ?> (<?php echo stripslashes($rsfield->type); ?>)</option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Zip
                                            </td>
                                            <td>
                                                <select name="fields[<?php echo $field['id']; ?>][3]" class="form-control">
                                                    <option value="0">None</option>
                                                    <?php foreach ($fields_resp->fields as $rsfield) { ?>
                                                        <option value="<?php echo $rsfield->id; ?>" <?php if ($rsfield->id == $revsocial['map'][$field['id']][3]) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsfield->title); ?> (<?php echo stripslashes($rsfield->type); ?>)</option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php

                                    // THE REST
                                    } else {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $fdata['label']; ?>
                                            </td>
                                            <td>
                                                <select name="fields[<?php echo $field['id']; ?>]" class="form-control">
                                                    <option value="0">None</option>
                                                    <?php foreach ($fields_resp->fields as $rsfield) { ?>
                                                        <option value="<?php echo $rsfield->id; ?>" <?php if ($rsfield->id == $revsocial['map'][$field['id']]) { echo 'selected="selected"'; } ?>><?php echo stripslashes($rsfield->title); ?> (<?php echo stripslashes($rsfield->type); ?>)</option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php
                                    }

                                }

                                ?>

                            </table>

                            <?php

                        // NO FIELDS
                        } else {
                            ?>
                            No fields set up for RevSocial form. Please <a href="http://www.revsocial.com/dashboard/forms/build/?id=<?php echo $revsocial['form_id']; ?>" target="_blank">create some</a> first.
                            <?php
                        }

                    }

                // NO FORMS
                } else {

                    // CLEAR REVSOCIAL SETTINGS FOR FORM
                    sqlquery("UPDATE `" .$_uccms_formbuilder->tables['forms']. "` SET `revsocial`='' WHERE (`id`=" .$form['id']. ")");

                    ?>
                    No <a href="http://www.revsocial.com/dashboard/forms/" target="_blank">RevSocial forms</a>.
                    <?php

                }

                ?>

            </section>

            <footer>
                <input type="submit" class="button blue" value="Update" />
            </footer>

            </form>

        </div>

        <?php

    }

}

?>