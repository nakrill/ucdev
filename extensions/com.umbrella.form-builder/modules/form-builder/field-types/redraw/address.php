<label>
	<?=htmlspecialchars($data["label"])?>
	<? if ($data["required"]) { ?>
	<span class="required">*</span>
	<? } ?>
</label>
<div class="form_builder_object form_builder_full">
	<input type="text" class="form_builder_text form-control" />
	<label>Street Address</label>
</div>
<div class="form_builder_object form_builder_full">
	<input type="text" class="form_builder_text form-control" />
	<label>Street Address Line 2</label>
</div>
<div class="form_builder_object form_builder_split">
	<input type="text" class="form_builder_text form-control" />
	<label>City</label>
</div>
<div class="form_builder_object form_builder_split form_builder_last">
	<input type="text" class="form_builder_text form-control" />
	<label>State / Province / Region</label>
</div>
<div class="form_builder_object form_builder_split">
	<input type="text" class="form_builder_text form-control" />
	<label>Postal / Zip Code</label>
</div>
<div class="form_builder_object form_builder_split form_builder_last">
	<input type="text" class="form_builder_text form-control" />
	<label>Country</label>
</div>