<label>
	<?=htmlspecialchars($data["label"])?>
	<? if ($data["required"]) { ?>
	<span class="required">*</span>
	<? } ?>
</label>
<input type="text" class="form_builder_text form-control" placeholder="<?=htmlspecialchars($data["placeholder"])?>" />