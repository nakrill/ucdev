<fieldset class="form-group">
	<label>Section Title</label>
	<input type="text" name="title" value="<?=htmlspecialchars($data["title"])?>" class="form-control" />
</fieldset>
<fieldset class="form-group">
	<label>Section Description</label>
	<textarea name="description" class="form-control"><?=htmlspecialchars($data["description"])?></textarea>
</fieldset>