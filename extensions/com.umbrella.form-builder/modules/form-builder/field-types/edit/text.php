<fieldset class="form-group">
	<label>Field Label</label>
	<input type="text" name="label" value="<?=htmlspecialchars($data["label"])?>" class="form-control" />
</fieldset>
<fieldset class="form-group">
	<label>Placeholder Field Value</label>
	<input type="text" name="placeholder" value="<?=htmlspecialchars($data["placeholder"])?>" class="form-control" />
</fieldset>
<fieldset class="form-group">
	<label>Maximum Length <small>(leave empty or 0 for no max)</small></label>
	<input type="text" name="maxlength" value="<?=intval($data["maxlength"])?>" class="form-control" />
</fieldset>