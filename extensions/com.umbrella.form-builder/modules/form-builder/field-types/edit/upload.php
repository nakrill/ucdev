<fieldset class="form-group">
	<label>Field Label</label>
	<input type="text" name="label" value="<?=htmlspecialchars($data["label"])?>" class="form-control" />
</fieldset>
<fieldset class="form-group">
	<label>Upload Directory <small>(relative to /site/, defaults to /site/files/form-builder/)</small></label>
	<input type="text" name="directory" value="<?=htmlspecialchars($data["directory"])?>" class="form-control" />
</fieldset>