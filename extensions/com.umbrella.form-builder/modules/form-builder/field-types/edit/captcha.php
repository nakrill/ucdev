<fieldset class="form-group">
	<label>Field Label</label>
	<input type="text" name="label" value="<?=htmlspecialchars($data["label"])?>" class="form-control" />
</fieldset>
<fieldset class="form-group">
	<label>Field Instructions</label>
	<textarea name="instructions" class="form-control"><?=htmlspecialchars($data["instructions"])?></textarea>
</fieldset>