<fieldset class="form-group">
    <label>Field Name</label>
    <input type="text" name="label" value="<?=htmlspecialchars($data["label"])?>" class="form-control" />
</fieldset>
<fieldset class="form-group">
    <label>Code</label>
    <textarea name="code" class="form-control"><?=htmlspecialchars($data["code"])?></textarea>
</fieldset>