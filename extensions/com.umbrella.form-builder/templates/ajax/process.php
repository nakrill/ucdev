<?php

$out = array();

$bigtree['resources']['form']       = (int)$_POST['form_id'];
$bigtree['resources']['form_ajax']  = true;
$emails                             = ($_POST['form_tea'] ? uccms_decrypt($_POST['form_tea']) : '');

include(SERVER_ROOT. 'extensions/com.umbrella.form-builder/templates/routed/form-builder/_header.php');
include(SERVER_ROOT. 'extensions/com.umbrella.form-builder/templates/routed/form-builder/process.php');
include(SERVER_ROOT. 'extensions/com.umbrella.form-builder/templates/routed/form-builder/_footer.php');

echo json_encode($out);

?>