<?
$classes = array();
if ($d["required"]) {
	$classes[] = "form_builder_required";
}
if ($d["default"]) {
	$classes[] = "default";
}
if ($error) {
	$classes[] = "form_builder_error";
}
?>
<fieldset class="fs-<?php echo $d['id']; ?> form-group">
	<label for="form_builder_field_<?=$count?>">
		<?=htmlspecialchars($d["label"])?>
		<? if ($d["required"]) { ?>
		    <span class="form_builder_required_star">*</span>
		<? } ?>
	</label>
	<textarea id="form_builder_field_<?=$count?>" name="<?=$field_name?>" class="form-control <?php if (count($classes)) { echo implode(" ",$classes); } ?>" placeholder="<?=htmlspecialchars($d["placeholder"])?>"><?=htmlspecialchars($default)?></textarea>
</fieldset>
