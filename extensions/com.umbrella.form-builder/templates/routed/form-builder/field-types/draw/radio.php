<fieldset class="fs-<?php echo $d['id']; ?> form-group">
    <?
    if ($d["label"]) { ?>
        <label>
	        <?=htmlspecialchars($d["label"])?>
	        <? if ($d["required"]) { ?>
	        <span class="form_builder_required_star">*</span>
	        <? } ?>
        </label>
        <?
	}
	foreach ($d["list"] as $item) {
	    ?>
	    <div class="form_builder_radio radio">
            <label class="form_builder_for_checkbox" for="form_builder_field_<?=$count?>">
                <input type="radio" id="form_builder_field_<?=$count?>" name="<?=$field_name?>" value="<?=htmlspecialchars($item["value"])?>" <? if ($default == $item["value"] || ($default === false && $item["selected"])) { ?>checked="checked" <? } ?> data-price="<?=number_format((float)str_replace(array('$',','), '', $item["price"]), 2, '.', '')?>" />
		        <?=htmlspecialchars($item["description"])?>
            </label>
	    </div>
	    <?
		// IF THIS IS A PAID FORM, WE WATCH THE ELEMENT FOR CHANGES TO CALCULATE THE TOTAL
		if ($form["paid"]) {
			$radio_watch[] = "form_builder_field_$count";
		}
		$count++;
	}
	?>
</fieldset>
