<?

$card_types = array("Visa" => "Visa","MasterCard" => "MasterCard");

// PAYMENT PROFILES
$payment_profiles = array();

// USER IS LOGGED IN
if ($_uccms['_account']->loggedIn()) {

    // PAYMENT PROFILES ENABLED
    if ($_uccms['_account']->paymentProfilesEnabled()) {

        // GET PAYMENT PROFILES
        $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$_uccms['_account']->userID(). ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
        $pp_q = sqlquery($pp_query);
        while ($pp = sqlfetch($pp_q)) {
            $payment_profiles[$pp['id']] = $pp;
        }

    }

    // LOGGED IN AND HAVE PAYMENT PROFILES
    if (count($payment_profiles) > 0) { ?>
        <div class="payment_profile">
            <fieldset class="form-group">
                <label>Your Cards</label>
                <select name="payment[profile_id]" class="form-control">
                    <?php foreach ($payment_profiles as $pp) { ?>
                        <option value="<?php echo $pp['id']; ?>" <?php if ($pp['id'] == $default['payment']['profile_id']) { ?>selected="selected"<?php } ?>><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                    <?php } ?>
                    <option value="">New Card</option>
                </select>
            </fieldset>
        </div>
        <?php

    }

}

?>

<div id="card_new" style="<?php if (($_uccms['_account']->loggedIn()) && (count($payment_profiles) > 0)) { ?>display: none;<?php } ?>">
    <fieldset class="fs-creditcard form-group">
	    <label for="form_builder_field_<?=$count?>">
		    Credit Card
		    <span class="form_builder_required_star">*</span>
	    </label>
	    <div class="form_builder_wrap">
		    <div class="form_builder_card_number">
			    <input type="text" name="<?=$field_name?>[number]" id="form_builder_field_<?=$count?>" class="form-control <?=implode(" ",$classes)?><? if ($error && !$default["number"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["number"])?>" autocomplete="off" maxlength="19" />
			    <p class="help-block form_builder_centered">Number</p>
		    </div>
	    </div>
	    <div class="form_builder_wrap">
		    <? $count++; ?>
		    <div class="form_builder_card_date">
			    <input type="text" name="<?=$field_name?>[month]" id="form_builder_field_<?=$count?>" class="form_builder_card_month form-control <?=implode(" ",$classes)?><? if ($error && !$default["month"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["month"])?>" maxlength="2" />
			    <? $count++; ?>
			    <input type="text" name="<?=$field_name?>[year]" id="form_builder_field_<?=$count?>" class="form_builder_card_year form-control <?=implode(" ",$classes)?><? if ($error && !$default["year"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["year"])?>" maxlength="4" />
			    <p class="help-block form_builder_centered">Expiration (MM-YYYY)</p>
		    </div>
		    <? $count++; ?>
		    <div class="form_builder_card_code">
			    <input type="text" name="<?=$field_name?>[code]" maxlength="4" id="form_builder_field_<?=$count?>" class="form-control <?=implode(" ",$classes)?><? if ($error && !$default["code"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["code"])?>" autocomplete="off" />
			    <p class="help-block form_builder_centered">CVV Code</p>
		    </div>
	    </div>
    </fieldset>
</div>

<div class="form_builder_section_header form_builder_total_section">
	<h2>Total</h2>
	<p>Your total charge will be: <span id="form_builder_total">$<?=number_format($form["base_price"],2)?></span></p>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        $('#formbuilder-<?php echo $form['id']; ?> .payment_profile select[name="payment[profile_id]"]').change(function() {
            if ($(this).val()) {
                $('#card_new').hide();
            } else {
                $('#card_new').show();
            }
        });

    });

</script>