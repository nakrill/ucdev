<?
$req = $d["required"] ? " form_builder_required" : "";

if (!$default["country"]) {
	$default["country"] = "United States";
}
?>
<fieldset class="fs-<?php echo $d['id']; ?> form_builder_address form-group">
	<label for="form_builder_field_<?=$count?>">
		<?=htmlspecialchars($d["label"])?>
		<? if ($d["required"]) { ?>
		<span class="form_builder_required_star">*</span>
		<? } ?>
	</label>
	<div class="form_builder_full">
		<input type="text" name="<?=$field_name?>[street]" id="form_builder_field_<?=$count?>" class="form-control form_builder_text<?=$req?><? if ($error && !$default["street"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["street"])?>" />
		<p class="help-block">Street Address</p>
	</div>
	<? $count++; ?>
	<div class="form_builder_full">
		<input type="text" name="<?=$field_name?>[street2]" id="form_builder_field_<?=$count?>" class="form-control form_builder_text" value="<?=htmlspecialchars($default["street2"])?>"/>
		<p class="help-block">Street Address Line 2</p>
	</div>
    <? $count++; ?>
    <div class="row">
	    <div class="col-md-6">
		    <input type="text" name="<?=$field_name?>[city]" id="form_builder_field_<?=$count?>" class="form-control form_builder_text<?=$req?><? if ($error && !$default["city"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["city"])?>" />
		    <p class="help-block">City</p>
	    </div>
	    <? $count++; ?>
	    <div class="col-md-6">
		    <input type="text" name="<?=$field_name?>[state]" id="form_builder_field_<?=$count?>" class="form-control form_builder_text<?=$req?><? if ($error && !$default["state"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["state"])?>" />
		    <p class="help-block">State / Province / Region</p>
	    </div>
    </div>
	<? $count++; ?>
    <div class="row">
	    <div class="col-md-6">
		    <input type="text" name="<?=$field_name?>[zip]" id="form_builder_field_<?=$count?>" class="form-control form_builder_text<?=$req?><? if ($error && !$default["zip"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["zip"])?>" />
		    <p class="help-block">Postal / Zip Code</p>
	    </div>
	    <? $count++; ?>
	    <div class="col-md-6">
		    <input type="text" name="<?=$field_name?>[country]" id="form_builder_field_<?=$count?>" class="form-control form_builder_text<?=$req?><? if ($error && !$default["country"]) { ?> form_builder_error<? } ?>" value="<?=htmlspecialchars($default["country"])?>" />
		    <p class="help-block">Country</p>
	    </div>
    </div>
</fieldset>
