<?php
// WE ONLY WANT TO DRAW A SINGLE CAPTCHA, EVEN IF THE FORM BUILDER ADDED TWO FOR WHATEVER REASON
if (!defined("BTXFORMBUILDER_CAPTCHA_USED")) {
	define("BTXFORMBUILDER_CAPTCHA_USED",true);
    ?>
    <script src="//www.google.com/recaptcha/api.js"></script>
    <fieldset class="fs-<?php echo $d['id']; ?> form-group">
	    <?
        if ($d["label"]) { ?>
	        <label>
		        <?=htmlspecialchars($d["label"])?>
		        <span class="form_builder_required_star">*</span>
	        </label>
	        <?
		}
		if ($d["instructions"]) {
	        ?>
	        <p><?=htmlspecialchars($d["instructions"])?></p>
	        <?
		}
		if ($error) {
	        ?>
	        <div class="form_builder_captcha_error">
		        <p>The code you entered was not correct. Please try again.</p>
	        </div>
	        <?
		}
	    ?>
	    <div class="g-recaptcha" data-sitekey="<?=$settings["recaptcha"]["site_key"]?>"></div>
    </fieldset>
    <?
}
?>