<fieldset class="fs-<?php echo $d['id']; ?> form-group">
    <? if ($d["label"]) { ?>
        <label>
            <?=htmlspecialchars($d["label"])?>
            <? if ($d["required"]) { ?>
            <span class="form_builder_required_star">*</span>
            <? } ?>
        </label>
    <?
    }
    foreach ($d["list"] as $item) {
        $value = $item["value"] ? htmlspecialchars($item["value"]) : htmlspecialchars($item["description"]);
        if ($item['price']) {
            $price = number_format((float)str_replace(array('$',','), '', $item["price"]), 2, '.', '');
        } else {
            $price = 0;
        }
        ?>
        <div class="form_builder_checkbox checkbox">
            <label class="form_builder_for_checkbox" for="form_builder_field_<?=$count?>">
                <input type="checkbox" id="form_builder_field_<?=$count?>" name="<?=$field_name?><? if (count($d["list"]) > 1) { ?>[]<? } ?>" value="<?=$value?>"<? if ((is_array($default) && in_array($value,$default)) || ($default === false && $item["selected"])) { ?> checked="checked"<? } ?> data-price="<?=$price?>" />
                <?=htmlspecialchars($item["description"])?><? if ($d["required"] && !$d["label"]) { ?><span class="form_builder_required_star">*</span><? } ?>
            </label>
        </div>
        <?
        // IF THIS IS A PAID FORM, WE WATCH THE ELEMENT FOR CHANGES TO CALCULATE THE TOTAL
        if ($form["paid"]) {
            $check_watch[] = 'form_builder_field_' .$count;
        }
        $count++;
    }
    ?>
</fieldset>
