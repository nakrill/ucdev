<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_formbuilder->Extension;?>/css/master/form.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_formbuilder->Extension;?>/css/custom/form.css" />
<script src="/extensions/<?=$_uccms_formbuilder->Extension;?>/js/master/form.js"></script>
<script src="/extensions/<?=$_uccms_formbuilder->Extension;?>/js/custom/form.js"></script>

<h1><?php echo $bigtree['resources']['page_header']; ?></h1>

<?php if ($bigtree['resources']['page_content']) { ?>
    <div class="content">
        <?php echo $bigtree['resources']['page_content']; ?>
    </div>
<?php } ?>

<?php if ($form['limit_entries'] && $form['entries'] >= $form['max_entries']) { ?>

    <h2>Maximum Entries Reached</h2>
    <p>This form has reached the maximum number of entries.</p>

<?php } else { ?>

    <?php

    // DISPLAY ANY SITE MESSAGES
    echo $_uccms['_site-message']->display();

    ?>

    <form id="formbuilder-<?php echo $form['id']; ?>" method="post" action="<?php echo $page_link; ?>process/" enctype="multipart/form-data" class="form_builder">
    <input type="hidden" name="form_id" value="<?php echo $form['id']; ?>" />
    <input type="hidden" name="form_tea" value="<?php echo ($bigtree['resources']['form_email'] ? uccms_encrypt($bigtree['resources']['form_email']) : ''); ?>" />

    <div class="form_builder_required_message">
        <p><span class="form_builder_required_star">*</span> = required field</p>
    </div>

    <?php

    // SETUP PRICE WATCHERS
    $text_watch = array();
    $check_watch = array();
    $radio_watch = array();
    $select_watch = array();

    $last_field = false;
    $count = 0;

    // LOOP THROUGH FIELDS
    foreach ($form['fields'] as $field) {

        $count++;
        $t = $field['type'];
        $d = json_decode($field['data'], true);

        // NOT A COLUMN
        if ($t != 'column') {
            if ($d['name']) {
                $field_name = $d['name'];
            } else {
                $field_name = 'data_' .$field['id'];
            }
            $error = false;
            if (isset($_SESSION['form_builder']['fields'])) {
                $default = $_SESSION['form_builder']['fields'][$field_name];
            } else {
                if (isset($d['default'])) {
                    $default = $d['default'];
                } else {
                    $default = false;
                }
            }
            if (is_array($_SESSION['form_builder']['errors']) && in_array($field_name, $_SESSION['form_builder']['errors'])) {
                $error = true;
            }
            include 'field-types/draw/' .$t. '.php';

        // A COLUMN
        } else {
            if ($last_field == 'column') {
                echo '<div class="form_builder_column form_builder_last">';
            } else {
                echo '<div class="form_builder_column">';
            }
            foreach ($field['fields'] as $subfield) {
                $count++;
                $d = json_decode($subfield['data'],true);
                if ($d['name']) {
                    $field_name = $d['name'];
                } else {
                    $field_name = 'data_' .$subfield['id'];
                }
                $error = false;
                if (isset($_SESSION['form_builder']['fields'])) {
                    $default = $_SESSION['form_builder']['fields'][$field_name];
                } else {
                    if (isset($d['default'])) {
                        $default = $d['default'];
                    } else {
                        $default = false;
                    }
                }
                if (is_array($_SESSION['form_builder']['errors']) && in_array($field_name,$_SESSION['form_builder']['errors'])) {
                    $error = true;
                }
                include 'field-types/draw/' .$subfield['type']. '.php';
            }
            echo '</div>';
        }
        $last_field = $t;
    }

    ?>

    <input type="submit" class="button btn btn-primary" value="Submit" />

    </form>

    <?php

    // MAKE THE PRICE WATCHERS
    if ($form['paid']) {

        if ($form['early_bird_date'] && strtotime($form['early_bird_date']) > time()) {
            $bp = $form['early_bird_base_price'] ? $form['early_bird_base_price'] : $form['base_price'];
        } else {
            $bp = $form['base_price'] ? $form['base_price'] : '0.00';
        }

        ?>

        <script type="text/javascript">

            var PreviousValues = {};
            var Total = <?php echo $bp; ?>;

            function formatMoney(number, places, symbol, thousand, decimal) {
                places = !isNaN(places = Math.abs(places)) ? places : 2;
                symbol = symbol !== undefined ? symbol : '$';
                thousand = thousand || ',';
                decimal = decimal || '.';
                var negative = number < 0 ? '-' : '';
                var i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + '';
                var j = (j = i.length) > 3 ? j % 3 : 0;
                return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
            }

            var element;

            $(document).ready(function() {

                <?php

                // TEXT FIELDS TO WATCH FOR PRICE CHANGES
                if (is_array($text_watch)) {
                    foreach ($text_watch as $id) {
                        ?>
                        element = $('#<?php echo $id; ?>');
                        if (element.val()) {
                            var price = parseFloat(element.val().replace(/[^0-9-.]/g, ''));
                            if (price < 0) {
                                price = 0;
                            }
                            Total += price;
                            PreviousValues[element.attr('name')] = price;
                            element.val(formatMoney(price));
                        }
                        element.change(function() {
                            var old_price = PreviousValues[$(this).attr('name')];
                            if (old_price) {
                                Total -= old_price;
                            }
                            var price = parseFloat($(this).val().replace(/[^0-9-.]/g, ''));
                            if (price < 0) {
                                price = 0;
                            }
                            this.value = formatMoney(price);
                            if (isNaN(price)) {
                                PreviousValues[$(this).attr('name')] = 0;
                            } else {
                                Total += price;
                                PreviousValues[$(this).attr('name')] = price;
                            }
                            $('#form_builder_total').html(formatMoney(Total));
                        });
                        <?
                    }
                }

                // CHECK BOXES TO WATCH FOR PRICE CHANGES
                if (is_array($check_watch)) {
                    foreach ($check_watch as $id) {
                        ?>
                        element = $('#<?php echo $id; ?>');
                        if (element.prop('checked')) {
                            Total += parseFloat(element.attr('data-price'));
                        }
                        element.change(function() {
                            var price = parseFloat($(this).attr('data-price'));
                            if ($(this).prop('checked')) {
                                Total += price;
                            } else {
                                Total -= price;
                            }
                            $('#form_builder_total').html(formatMoney(Total));
                        });
                        <?
                    }
                }

                // RADIO BUTTONS TO WATCH FOR PRICE CHANGES
                if (is_array($radio_watch)) {
                    foreach ($radio_watch as $id) {
                        ?>
                        element = $('#<?php echo $id; ?>');
                        if (element.prop('checked')) {
                            Total += parseFloat(element.attr('data-price'));
                            PreviousValues[element.attr('name')] = parseFloat(element.attr('data-price'));
                        }
                        element.change(function() {
                            var old_price = PreviousValues[$(this).attr('name')];
                            if (old_price) {
                                Total -= old_price;
                            }
                            var price = parseFloat($(this).attr('data-price'));
                            Total += price;
                            PreviousValues[$(this).attr('name')] = price;
                            $('#form_builder_total').html(formatMoney(Total));
                        });
                        <?
                    }
                }

                // SELECT BOXES TO WATCH FOR PRICE CHANGES
                if (is_array($select_watch)) {
                    foreach ($select_watch as $id) {
                        ?>
                        var element = $('#<?php echo $id; ?>');
                        var value = parseFloat($('option:selected', element).attr('data-price'));
                        if (value) {
                            Total += value;
                            PreviousValues[element.attr('name')] = value;
                        }
                        element.change(function() {
                            var old_price = PreviousValues[$(this).attr('name')];
                            if (old_price) {
                                Total -= old_price;
                            }
                            var price = parseFloat($('option:selected', this).attr('data-price'));
                            Total += price;
                            PreviousValues[$(this).attr('name')] = price;
                            $('#form_builder_total').html(formatMoney(Total));
                        });
                        <?
                    }
                }

                ?>

                $('#form_builder_total').html(formatMoney(Total));

            });

        </script>

        <?php
    }

    unset($_SESSION['form_builder']['payment_error']);
    unset($_SESSION['form_builder']['errors']);
    unset($_SESSION['form_builder']['fields']);

}

?>