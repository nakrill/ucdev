<?php

$storage = new BigTreeStorage;
$total = 0;
$email = '';
$errors = $entry = array();

$field_values = array();

// START UP THE RUNNING TOTAL IF WE'RE PAID
if ($form['paid']) {
    if ($form['early_bird_date'] && strtotime($form['early_bird_date']) > time()) {
       $total = $form['early_bird_base_price'] ? $form['early_bird_base_price'] : $form['base_price'];
    } else {
       $total = $form['base_price'] ? $form['base_price'] : 0;
    }
}

// WALK THROUGH EACH FORM FIELD, SEE IF IT'S REQUIRED.
// IF IT IS AND THERE WAS NO ENTRY, ADD AN ERROR, OTHERWISE ADD THE ENTRY DATA.
foreach ($form['fields'] as $field) {

    $t = $field['type'];
    $d = json_decode($field['data'],true);

    // IS CREDIT CARD
    if ($t == 'credit-card') {

        // PAYMENT PROFILES ENABLED & PAYMENT PROFILE ID SPECIFIED
        if (($_uccms['_account']->paymentProfilesEnabled()) && ($_POST['payment']['profile_id'])) {
            $d['required'] = false; // MAKE CREDIT CARD INFO NOT REQUIRED
        }

    }

    // IF IT'S NOT A COLUMN, JUST PROCESS IT
    if ($t != 'column') {

        if ($d['name']) {
            $field_name = $d['name'];
        } else {
            $field_name = 'data_'. $field['id'];
        }
        $value = false;
        include 'field-types/process/' .$t. '.php';
        if ($value !== false && $field['id']) {
            $entry[$field['id']] = $value;
        }
        if ($d['required'] && !$value) {
            $errors[] = $field_name;
        }

        $field_values[$field['id']] = $value;

    // IF IT IS A COLUMN, WE NEED TO PROCESS EVERYTHING INSIDE THE COLUMN FIRST
    } else {

        foreach ($field['fields'] as $subfield) {

            $d = json_decode($subfield['data'],true);
            if ($d['name']) {
                $field_name = $d['name'];
            } else {
                $field_name = 'data_' .$subfield['id'];
            }
            $value = false;
            include 'field-types/process/' .$subfield['type']. '.php';
            if ($value !== false) {
                $entry[$subfield['id']] = $value;
            }
            if ($d['required'] && !$value) {
                $errors[] = $field_name;
            }

            $field_values[$field['id']] = $value;

        }

    }
}

// IF WE HAD ERRORS, REDIRECT BACK WITH THE SAVED DATA AND ERRORS
if (count($errors)) {

    //unset($_POST['fb_cc_card']['number']);
    //unset($_POST['fb_cc_card']['code']);
    $_SESSION['form_builder']['fields'] = $_POST;
    $_SESSION['form_builder']['errors'] = $errors;

    $_uccms['_site-message']->set('error', 'One or more required fields are missing. Please fill out all required fields and submit again.');

    if ($bigtree['resources']['form_ajax']) {
        $out['message'] = $_uccms['_site-message']->display();
        return;
    } else {
        BigTree::redirect($page_link);
    }

}

// IF IT'S PAID, LET'S TRY TO CHARGE THEM
if ($form['paid']) {

    // AMOUNT
    $total = round($total, 2);

    // HAVE PRICE TO CHARGE
    if ($total) {

        $payment_success = false;

        // PAYMENT PROFILE SPECIFIED & LOGGED IN
        if (($_POST['payment']['profile_id']) && ($_uccms['_account']->userID())) {

            // GET PAYMENT PROFILE
            $pp = $_uccms['_account']->pp_getProfile((int)$_POST['payment']['profile_id'], $_uccms['_account']->userID());

            // PAYMENT PROFILE FOUND
            if ($pp['id']) {

                $payment_method     = 'profile';
                $payment_name       = stripslashes($pp['name']);
                $payment_lastfour   = $pp['lastfour'];
                $payment_expiration = $pp['expires'];

            // PAYMENT PROFILE NOT FOUND
            } else {
                $payment_missing = true;
                $_uccms['_site-message']->set('error', 'Payment method not found.');
            }

        // USE CREDIT CARD INFO
        } else {

            // REQUIRE PAYMENT FIELDS
            $reqfa = array(
                'fb_cc_billing_name' => 'Name On Card',
                'fb_cc_billing_address' => array(
                    'zip'   => 'Card Zip'
                ),
                'fb_cc_card' => array(
                    'number'    => 'Card Number',
                    'month'     => 'Expiration Month',
                    'year'      => 'Expiration Year',
                    'code'      => 'CVV Code'
                )
            );

            // CHECK REQUIRED
            foreach ($reqfa as $name => $title) {
                if (is_array($title)) {
                    foreach ($title as $name2 => $title2) {
                        if (!$_POST[$name][$name2]) {
                            $payment_missing = true;
                            $_uccms['_site-message']->set('error', 'Missing / Incorrect: Payment - ' .$title2);
                        }
                    }
                } else {
                    if (!$_POST[$name]) {
                        $payment_missing = true;
                        $_uccms['_site-message']->set('error', 'Missing / Incorrect: Payment - ' .$title);
                    }
                }
            }

            $payment_method = 'credit-card';

            $payment_name = ($_POST['fb_cc_billing_name']['first']. ' ' .$_POST['fb_cc_billing_name']['last']);

            // PAYMENT LAST FOUR
            $payment_lastfour = substr(preg_replace("/[^0-9]/", '', $_POST['fb_cc_card']['number']), -4);

            // PAYMENT EXPIRATION DATE FORMATTING
            $payment_expiration = date('Y-m-d', strtotime($_POST['fb_cc_card']['month']. '/01/' .$_POST['fb_cc_card']['year']));

        }

        // NO MISSING FIELDS
        if (!$payment_missing) {

            // PAYMENT - CHARGE
            $payment_data = array(
                'amount'    => $total,
                'customer' => array(
                    'id'            => $_uccms['_account']->userID(),
                    'name'          => $payment_name,
                    'email'         => $_POST['fb_cc_billing_email'],
                    'phone'         => '',
                    'address'       => array(
                        'street'    => $_POST['fb_cc_billing_address']['street'],
                        'street2'   => $_POST['fb_cc_billing_address']['street2'],
                        'city'      => $_POST['fb_cc_billing_address']['city'],
                        'state'     => $_POST['fb_cc_billing_address']['state'],
                        'zip'       => $_POST['fb_cc_billing_address']['zip'],
                        'country'   => $_POST['fb_cc_billing_address']['country']
                    ),
                    'description'   => ''
                ),
                'note'  => ($page_header ? $page_header : $bigtree['page']['nav_title'])
            );

            // HAVE PAYMENT PROFILE
            if ($pp['id']) {
                $payment_data['card'] = array(
                    'id'            => $pp['id'],
                    'name'          => $payment_name,
                    'number'        => $payment_lastfour,
                    'expiration'    => $payment_expiration
                );

            // USE CARD
            } else {
                $payment_data['card'] = array(
                    'name'          => $payment_name,
                    'number'        => $_POST['fb_cc_card']['number'],
                    'expiration'    => $payment_expiration,
                    'zip'           => $_POST['fb_cc_billing_address']['zip'],
                    'cvv'           => $_POST['fb_cc_card']['code']
                );
            }

            // PROCESS PAYMENT
            $payment_result = $_uccms['_account']->payment_charge($payment_data);

            // TRANSACTION ID
            $transaction_id = $payment_result['transaction_id'];

            // TRANSACTION MESSAGE (ERROR)
            $transaction_message = $payment_result['error'];

            // CHARGE SUCCESSFUL
            if ($transaction_id) {
                $payment_success = true;
            }

        }

        $_POST['fb_cc_card']['number'] = $payment_lastfour;

        // LOG TRANSACTION
        $_uccms_formbuilder->logTransaction(array(
            'account_id'            => $_uccms['_account']->userID(),
            'form_id'               => $form['id'],
            'dt'                    => date('Y-m-d H:i:s'),
            'status'                => ($payment_success ? 'charged' : 'failed'),
            'amount'                => number_format((float)$total, 2, '.', ''),
            'method'                => $payment_method,
            'payment_profile_id'    => $pp['id'],
            'name'                  => $payment_name,
            'lastfour'              => $payment_lastfour,
            'expiration'            => $payment_expiration,
            'transaction_id'        => $transaction_id,
            'message'               => $transaction_message,
            'ip'                    => $_SERVER['REMOTE_ADDR']
        ));

        // PAYMENT SUCCESSFUL
        if ($payment_success) {

            // ADD BILLING INFO TO THE EMAIL
            $address = $_POST['fb_cc_billing_address'];
            $card = $_POST['fb_cc_card'];
            $email .= "Billing Address:\n";
            $email .= implode(" ",$_POST['fb_cc_billing_name']). "\n";
            $email .= $address['street']."\n";
            if ($address['street2']) {
                $email .= $address['street2']."\n";
            }
            $email .= $address['city'].", ".$address['state']." ".$address['zip']."\n";
            $email .= $address['country'];
            $email .= "\n\n";

            $email .= "Credit Card Info:\n";
            $email .= $card['type']."\n";
            $email .= "**** **** **** ".$pg->Last4CC."\n";
            $email .= "Expires ".$card['month']."/".$card['year'];
            $email .= "\n\n";

            $email .= 'Total Charged: $' .number_format($total, 2);

        // PAYMENT FAILED
        } else {

            //unset($_POST['fb_cc_card']['number']);
            //unset($_POST['fb_cc_card']['code']);
            $_SESSION['form_builder']['fields'] = $_POST;
            $_SESSION['form_builder']['errors'] = $errors;

            if ($transaction_message) {
                $_SESSION['form_builder']['payment_error'] = $transaction_message;
                $_uccms['_site-message']->set('error', 'Payment failed - your credit card has not been charged.<br />The error returned was: ' .$transaction_message);
            } else {
                $_uccms['_site-message']->set('error', 'Payment failed - your credit card has not been charged.');
            }

            if ($bigtree['resources']['form_ajax']) {
                $out['message'] = $_uccms['_site-message']->display();
                return;
            } else {
                BigTree::redirect($page_link);
            }

        }

    }

    // SAVE BILLING INFO TO THE DB
    $entry['payment'] = array(
        'amount'    => $total,
        'name'      => $_POST['fb_cc_billing_name'],
        'address'   => $_POST['fb_cc_billing_address'],
        'card'      => array(
            'id'            => $pp['id'],
            'number'        => $payment_lastfour,
            'expiration'    => $payment_expiration
        )
    );
    if ($transaction_id) {
        $entry['payment']['transaction_id'] = $transaction_id;
    }

    $entry['ip'] = $_SERVER['REMOTE_ADDR'];

}

// GET RID OF SAVED DATA OR ERRORS
unset($_SESSION['form_builder']);

// ADD THE ENTRY TO THE ENTRIES TABLE
$entry_id = BigTreeAutoModule::createItem('uccms_formbuilder_entries', array('form' => $form['id'], 'data' => $entry, 'created_at' => 'NOW()'));

// UPDATE THE TOTALS FOR THE FORM AND RECACHE IT
sqlquery("UPDATE `uccms_formbuilder_forms` SET `entries`=(`entries` + 1), `last_entry`=NOW(), `total_collected`=(`total_collected`+" .round($total, 2). ") WHERE (`id`='" .$form['id']. "')");
BigTreeAutoModule::recacheItem($form['id'], 'uccms_formbuilder_forms');

// REVSOCIAL API KEY
$api_key = $cms->getSetting('rs_api-key');

// HAVE API KEY
if ($api_key) {

    // REVSOCIAL INFO
    $revsocial = json_decode($form['revsocial'], true);

    // HAVE FORM ID
    if ($revsocial['form_id']) {

        // HAVE MAPPING
        if (is_array($revsocial['map'])) {

            // REST CLIENT
            require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

            // INIT REST API CLASS
            $_api = new RestClient(array(
                'base_url' => 'https://api.revsocial.com/v1'
            ));

            $form_fields = array();

            // LOOP THROUGH REVSOCIAL FIELDS
            foreach ($revsocial['map'] as $bt_field => $rs_field) {
                if (($bt_field) && ($rs_field)) {
                    if (is_array($rs_field)) {

                        // NAME FIELD
                        if (isset($field_values[$bt_field]['first'])) {
                            $form_fields[$rs_field[0]] = $field_values[$bt_field]['first'];
                            $form_fields[$rs_field[1]] = $field_values[$bt_field]['last'];

                        // ADDRESS FIELD
                        } else if (isset($field_values[$bt_field]['street'])) {
                            $form_fields[$rs_field[0]] = trim($field_values[$bt_field]['street']. ' ' .$field_values[$bt_field]['street2']);
                            $form_fields[$rs_field[1]] = $field_values[$bt_field]['city'];
                            $form_fields[$rs_field[2]] = $field_values[$bt_field]['state'];
                            $form_fields[$rs_field[3]] = $field_values[$bt_field]['zip'];
                        }

                    } else if (isset($field_values[$bt_field])) {
                        $form_fields[$rs_field] = $field_values[$bt_field];
                    }
                }
            }

            // FORM INFO
            $form_data = array(
                'form'  => array(
                    'id'        => $revsocial['form_id'],
                    'fields'    => $form_fields
                ),
                'ip'        => $_SERVER['REMOTE_ADDR'],
                'session'   => $_COOKIE['__rstid']
            );

            // MAKE API CALL
            $result = $_api->post('form/process', array(
                'api_key'   => $api_key,
                'data'      => $form_data
            ));

            // RESULT: SUCCESS
            if ($result->info->http_code == 200) {

                // API RESPONSE
                $resp = $result->decode_response();

                // SAVE REVSOCIAL INFO WITH RECORD
                $rs_submission_id = $resp->submission_id;
                $rs_profile_id = $resp->profile->mpid;

            }

        }

    }

}

// PAID FORM
if ($form['paid']) {

    $rs_orderdata = array(
        'customer' => array(
            'firstname'     => $_POST['fb_cc_billing_name']['first'],
            'lastname'      => $_POST['fb_cc_billing_name']['last'],
            'middlename'    => '',
            'prefix'        => '',
            'suffix'        => '',
            'email'         => $_POST['fb_cc_billing_email'],
            'phone'         => '',
            'dob'           => '',
            'gender'        => '',
            'country'       => $_POST['fb_cc_billing_address']['country'],
            'state'         => $_POST['fb_cc_billing_address']['state'],
            'city'          => $_POST['fb_cc_billing_address']['city'],
            'zip'           => $_POST['fb_cc_billing_address']['zip'],
            'street'        => implode(', ', array($_POST['fb_cc_billing_address']['street'], $_POST['fb_cc_billing_address']['street2'])),
            'company'       => ''
        ),
        'billing' => array(
            'firstname'     => $_POST['fb_cc_billing_name']['first'],
            'lastname'      => $_POST['fb_cc_billing_name']['last'],
            'middlename'    => '',
            'prefix'        => '',
            'suffix'        => '',
            'email'         => $_POST['fb_cc_billing_email'],
            'phone'         => '',
            'country'       => $_POST['fb_cc_billing_address']['country'],
            'state'         => $_POST['fb_cc_billing_address']['state'],
            'city'          => $_POST['fb_cc_billing_address']['city'],
            'zip'           => $_POST['fb_cc_billing_address']['zip'],
            'street'        => implode(', ', array($_POST['fb_cc_billing_address']['street'], $_POST['fb_cc_billing_address']['street2'])),
            'company'       => ''
        ),
        'shipping' => array(),
        'order' => array(
            'subtotal'          => $total,
            'shipping'          => '0.00',
            'tax'               => '0.00',
            'total'             => $total,
            'paid'              => $total,
            'transaction_id'    => $transaction_id
        ),
        'items' => array(
            'total_products'    => 1,
            'total_items'       => 1,
            'products' => array(
                0 => array(
                    'sku'       => $form['id'],
                    'name'      => stripslashes($form['title']),
                    'quantity'  => 1,
                    'total'     => $total
                )
            )
        )

    );

    // RECORD ORDER WITH REVSOCIAL
    $_uccms_formbuilder->revsocial_recordOrder($entry_id, $_uccms['_account']->userID(), $rs_orderdata);

}

// GET THE NO-REPLY DOMAIN
$no_reply_domain = str_replace(array('http://www.', 'https://www.', 'http://', 'https://'), '', DOMAIN);

// EMAIL SUBJECT
$page_name = $page_header ? $page_header : $bigtree['page']['nav_title'];

##########################
# EMAIL
##########################

// EMAIL SETTINGS
$esettings = array(
    'custom_template'   => SERVER_ROOT. 'extensions/' .$_uccms_formbuilder->Extension. '/templates/email/form_submitted.html',
    'subject'           => 'Form Submitted - ' .stripslashes($form['title']),
    //'from_name'   => '',
    'from_email'  => 'no-reply@' .$no_reply_domain
);

// EMAIL VARIABLES
$evars = array(
    'heading'               => 'Form Submitted',
    'date'                  => date('n/j/Y'),
    'time'                  => date('g:i A T'),
    'form_name'             => stripslashes($form['title']),
    'page_name'             => $page_name,
    'entry_id'              => $entry_id,
    'data'                  => nl2br($email),
    'button_view'              => '<a href="' .ADMIN_ROOT . $_uccms_formbuilder->Extension. '*form-builder/view-entry/' .$entry_id. '/" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">View Submission</a>'

);

// WHO TO SEND TO
$esettings['to_email'] = $emails;

// SEND EMAIL
$result = $_uccms['_account']->sendEmail($esettings, $evars);

//echo print_r($result);
//exit;

##########################


if ($bigtree['resources']['form_ajax']) {
    $out['success'] = true;
} else {
    BigTree::redirect($page_link. 'thanks/');
}

?>