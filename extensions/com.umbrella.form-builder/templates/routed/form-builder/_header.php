<?php

// Make sure this page is never cached.
if (!defined('BIGTREE_DO_NOT_CACHE')) {
	define('BIGTREE_DO_NOT_CACHE',true);
}

// NEW CLASS
$_uccms_formbuilder = new uccms_FormBuilder();

// GET FORM
$form = $_uccms_formbuilder->getForm($bigtree['resources']['form']);

// CUSTOM EDIT LINK
$bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_formbuilder->Extension. '*form-builder/edit/' .$bigtree['resources']['form']. '/';

// GET SETTINGS
$settings = $cms->getSetting('settings');

// PAID FORM
if ($form['paid']) {

    // MAKE IT SECURE
	$cms->makeSecure();

    // PAYMENT FIELDS
	$form['fields'] = array_merge($form['fields'], array(

		// Section Header
		array(
			'type' => 'section',
			'data' => json_encode(array(
				'title' => 'Payment',
				'description' => 'Please enter your billing information as it appears on your credit card.',
			))
		),

		// Billing Name
		array(
			'type' => 'name',
			'data' => json_encode(array(
				'name' => 'fb_cc_billing_name',
				'required' => true,
				'label' => 'Billing Name'
			))
		),

		// Billing Email
		array(
			'type' => 'email',
			'data' => json_encode(array(
				'name' => 'fb_cc_billing_email',
				'required' => true,
				'label' => 'Billing Email'
			))
		),

		// Billing Address
		array(
			'type' => 'address',
			'data' => json_encode(array(
				'name' => 'fb_cc_billing_address',
				'required' => true,
				'label' => 'Billing Address'
			))
		),

		// Credit Card
		array(
			'type' => 'credit-card',
			'data' => json_encode(array(
				'name' => 'fb_cc_card',
				'required' => true
			))
		)

	));

	$page_link = str_replace('http://', 'https://', WWW_ROOT) . $bigtree['page']['path']. '/';

// NOT PAID
} else {
	$page_link = WWW_ROOT . $bigtree['page']['path'] . '/';
}

if (!$bigtree['resources']['form_ajax']) {

    ?>

    <div class="p_form">
        <div class="container">

        <?php

}