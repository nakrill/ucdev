<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_properties) $_uccms_properties = new uccms_Properties;

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
$agency_id      = (int)$_GET['agency_id'];
$limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_properties->perPage();

if (isset($_GET['base_url'])) {
    $base_url = $_GET['base_url'];
} else if (defined('MODULE_ROOT')) {
    $base_url = MODULE_ROOT. 'agents';
} else {
    $base_url = '.';
}

//$_uccms_properties->setPerPage(5);

// WHERE ARRAY
unset($wa);
$wa[] = "at.id>0";

// STATUSES
$statusa = array(
    'inactive'  => 0,
    'active'    => 1,
    'deleted'   => 9
);
if (isset($statusa[$_REQUEST['status']])) {
    $wa[] = "at.status=" .$statusa[$_REQUEST['status']];
} else {
    $wa[] = "at.status!=9";
}

// AGENCY SPECIFIED
if ($agency_id > 0) {
    $wa[] = "at.agency_id=" .$agency_id;
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(at.name) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED AGENTS
$agent_query = "
SELECT at.*
FROM `" .$_uccms_properties->tables['agents']. "` AS `at`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY at.name ASC, at.id ASC
LIMIT " .(($page - 1) * $limit). "," .$limit;
$agent_q = sqlquery($agent_query);

// NUMBER OF PAGED AGENTS
$num_agents = sqlrows($agent_q);

// TOTAL NUMBER OF AGENTS
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_properties->tables['agents']. "` AS `at` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_properties->pageCount($total_results['total']);

// HAVE AGENTS
if ($num_agents > 0) {

    // AGENCIES ARRAY
    $agenciesa = array();

    // ACCOUNTS ARRAY
    $accta = array();

    // LOOP
    while ($agent = sqlfetch($agent_q)) {

        // GET PROPERTIES
        $prop_query = "SELECT `id` FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`agent_id`=" .$agent['id']. ")";
        $prop_q = sqlquery($prop_query);
        $num_properties = sqlrows($prop_q);

        // HAVE AGENCY ID
        if ($agent['agency_id'] > 0) {

            // NOT IN ACCOUNT ARRAY
            if (!$agenciesa[$agent['agency_id']]) {

                // GET AGENCY INFO
                $agency_query = "
                SELECT ay.*
                FROM `" .$_uccms_properties->tables['agencies']. "` AS `ay`
                WHERE (ay.id=" .$agent['agency_id']. ")
                ";
                $agency_q = sqlquery($agency_query);
                $agency = sqlfetch($agency_q);
                $agenciesa[$agent['agency_id']] = $agency;

            }

        }

        // HAVE ACCOUNT ID
        if ($agent['account_id'] > 0) {

            // NOT IN ACCOUNT ARRAY
            if (!$accta[$agent['account_id']]) {

                // GET ACCOUNT INFO
                $acct_query = "
                SELECT a.id, a.username, a.email, ad.firstname, ad.lastname
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$agent['account_id']. ")
                LIMIT 1
                ";
                $acct_q = sqlquery($acct_query);
                $acct = sqlfetch($acct_q);
                $accta[$agent['account_id']] = $acct;

            }

        }

        ?>

        <li class="item">
            <section class="agent_name">
                <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $agent['id']; ?>"><?php echo stripslashes($agent['name']); ?></a>
            </section>
            <section class="agent_agency">
                <a href="<?php echo $base_url; ?>/../agencies/edit/?id=<?php echo $agent['agency_id']; ?>"><?php echo stripslashes($agenciesa[$agent['agency_id']]['title']); ?></a>
            </section>
            <section class="agent_properties">
                <a href="<?php echo $base_url; ?>/../agents/?agency_id=<?php echo $agent['id']; ?>"><?php echo $num_properties; ?></a>
            </section>
            <section class="agent_account">
                <?php
                if (($accta[$agent['account_id']]['firstname']) || ($accta[$agent['account_id']]['lastname'])) {
                    echo trim(stripslashes($accta[$agent['account_id']]['firstname']. ' ' .$accta[$agent['account_id']]['lastname'])). ' (' .stripslashes($accta[$agent['account_id']]['email']). ')';
                } else {
                    echo stripslashes($accta[$agent['account_id']]['email']);
                }
                ?>
            </section>
            <section class="agent_status status_<?php if ($agent['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                <?php if ($agent['status'] == 1) { echo 'Active' ; } else { echo 'Inactive'; } ?>
            </section>
            <section class="agent_edit">
                <a class="icon_edit" title="Edit Agent" href="<?php echo $base_url; ?>/edit/?id=<?php echo $agent['id']; ?>"></a>
            </section>
            <section class="agent_delete">
                <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $agent['id']; ?>" class="icon_delete" title="Delete Agent" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
            </section>
        </li>

        <?php

    }

    unset($accta);

// NO AGENTS
} else {
    ?>
    <li style="text-align: center;">
        No agents.
    </li>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>