<?php

// MODULE CLASS
if (!$_uccms_properties) $_uccms_properties = new uccms_Properties;

// HAS ACCESS
if ($_uccms_properties->adminModulePermission()) {

    // CLEAN UP
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
    $agency_id      = (int)$_GET['agency_id'];
    $property_id    = (int)$_GET['agent_id'];
    $limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_properties->perPage();

    if (isset($_GET['base_url'])) {
        $base_url = $_GET['base_url'];
    } else if (defined('MODULE_ROOT')) {
        $base_url = MODULE_ROOT. 'properties';
    } else {
        $base_url = '.';
    }

    //$_uccms_properties->setPerPage(5);

    // WHERE ARRAY
    unset($wa);
    $wa[] = "p.id>0";

    // STATUSES
    $statusa = array(
        'inactive'  => 0,
        'active'    => 1,
        'deleted'   => 9
    );
    if (isset($statusa[$_REQUEST['status']])) {
        $wa[] = "p.status=" .$statusa[$_REQUEST['status']];
    } else {
        $wa[] = "p.status!=9";
    }

    // TYPE SPECIFIED
    if ($_REQUEST['type']) {
        if ($_REQUEST['type'] == 'sale') {
            $wa[] = "p.listing_type=1";
        } else if ($_REQUEST['type'] == 'rental') {
            $wa[] = "p.listing_type=2";
        }
    }

    // AGENCY SPECIFIED
    if ($agency_id > 0) {
        $wa[] = "p.agency_id=" .$agency_id;
    }

    // AGENT SPECIFIED
    if ($property_id > 0) {
        $wa[] = "p.agent_id=" .$property_id;
    }

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(p.title) LIKE '%$part%') OR (LOWER(p.address1) LIKE '%$part%') OR (LOWER(p.city) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED PROPERTIES
    $property_query = "
    SELECT p.*
    FROM `" .$_uccms_properties->tables['properties']. "` AS `p`
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY p.title ASC, p.address1 ASC, p.id ASC
    LIMIT " .(($page - 1) * $limit). "," .$limit;
    $property_q = sqlquery($property_query);

    // NUMBER OF PAGED PROPERTIES
    $num_properties = sqlrows($property_q);

    // TOTAL NUMBER OF PROPERTIES
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_properties->tables['properties']. "` AS `p` WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_properties->pageCount($total_results['total']);

    // HAVE PROPERTIES
    if ($num_properties > 0) {

        // AGENCIES ARRAY
        $agenciesa = array();

        // AGENTS ARRAY
        $agentsa = array();

        // LOOP
        while ($property = sqlfetch($property_q)) {

            // HAVE AGENCY ID
            if ($property['agency_id'] > 0) {

                // NOT IN AGENCIES ARRAY
                if (!$agenciesa[$property['agency_id']]) {

                    // GET AGENCY INFO
                    $agency_query = "
                    SELECT ay.*
                    FROM `" .$_uccms_properties->tables['agencies']. "` AS `ay`
                    WHERE (ay.id=" .$property['agency_id']. ")
                    ";
                    $agency_q = sqlquery($agency_query);
                    $agency = sqlfetch($agency_q);
                    $agenciesa[$property['agency_id']] = $agency;

                }

            }

            // HAVE AGENT ID
            if ($property['agent_id'] > 0) {

                // NOT IN AGENTS ARRAY
                if (!$agentsa[$property['agent_id']]) {

                    // GET AGENT INFO
                    $agent_query = "
                    SELECT at.*
                    FROM `" .$_uccms_properties->tables['agents']. "` AS `at`
                    WHERE (at.id=" .$property['agent_id']. ")
                    ";
                    $agent_q = sqlquery($agent_query);
                    $agent = sqlfetch($agent_q);
                    $agentsa[$property['agent_id']] = $agent;

                }

            }

            ?>

            <li class="item">
                <section class="property_street">
                    <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $property['id']; ?>">
                        <?php
                        if ($property['title']) {
                            echo stripslashes($property['title']);
                        } else {
                            echo stripslashes($property['address1']);
                        }
                        ?>
                    </a>
                </section>
                <section class="property_city">
                    <?php echo stripslashes($property['city']); ?>
                </section>
                <section class="property_price">
                    $<?php echo number_format($property['price_from'], 2); if ($property['price_to'] > 0.00) { echo ' - $' .number_format($property['price_to'], 2); } ?>
                </section>
                <section class="property_agent">
                    <?php if ($agentsa[$property['agent_id']]['id']) { ?>
                        <a href="<?php echo $base_url; ?>/../agents/edit/?id=<?php echo $property['agent_id']; ?>"><?php echo stripslashes($agentsa[$property['agent_id']]['name']); ?></a>
                    <?php } else if ($agenciesa[$property['agency_id']]['id']) { ?>
                        <a href="<?php echo $base_url; ?>/../agencies/edit/?id=<?php echo $property['agency_id']; ?>"><?php echo stripslashes($agenciesa[$property['agency_id']]['title']); ?></a>
                    <?php } ?>
                </section>
                <section class="property_status status_<?php if ($property['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php if ($property['listing_type'] == 2) { echo 'Rental' ; } else { echo 'Sale'; } ?>
                </section>
                <section class="property_edit">
                    <a class="icon_edit" title="Edit Property" href="<?php echo $base_url; ?>/edit/?id=<?php echo $property['id']; ?>"></a>
                </section>
                <section class="property_delete">
                    <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $property['id']; ?>" class="icon_delete" title="Delete Property" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                </section>
            </li>

            <?php

        }

        unset($accta);

    // NO AGENTS
    } else {
        ?>
        <li style="text-align: center;">
            No properties.
        </li>
        <?php
    }

    ?>

    <script>
        BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>