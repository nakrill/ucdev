<?php

$admin->requireLevel(1);

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
$property_id    = (int)$_GET['property_id'];

// MODULE CLASS
if (!$_uccms_properties) $_uccms_properties = new uccms_Properties;

//$_uccms_properties->setPerPage(5);

// WHERE ARRAY
$wa[] = "pel.deleted_dt='0000-00-00 00:00:00'";

// PROPERTY SPECIFIED
if ($property_id > 0) {
    $wa[] = "pel.property_id=" .$property_id;
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(pel.data) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED LOGS
$log_query = "
SELECT pel.*, p.title, p.address1
FROM `" .$_uccms_properties->tables['property_email_log']. "` AS `pel`
INNER JOIN `" .$_uccms_properties->tables['properties']. "` AS `p` ON pel.property_id=p.id
WHERE (" .implode(") AND (", $wa). ")
ORDER BY pel.dt DESC
LIMIT " .(($page - 1) * $_uccms_properties->perPage()). "," .$_uccms_properties->perPage();
$log_q = sqlquery($log_query);

// NUMBER OF PAGED LOGS
$num_logs = sqlrows($log_q);

// TOTAL NUMBER OF LOGS
$total_results = sqlfetch(sqlquery("
SELECT COUNT('x') AS `total`
FROM `" .$_uccms_properties->tables['property_email_log']. "` AS `pel`
INNER JOIN `" .$_uccms_properties->tables['properties']. "` AS `b` ON pel.property_id=b.id
WHERE (" .implode(") AND (", $wa). ")
"));

// NUMBER OF PAGES
$pages = $_uccms_properties->pageCount($total_results['total']);

// HAVE LOGS
if ($num_logs > 0) {

    // LOOP
    while ($log = sqlfetch($log_q)) {

        unset($fielda);

        ?>

        <li class="item">
            <section class="log_date">
                <?php echo $log['dt']; ?>
            </section>
            <section class="log_property">
                <a href="../edit/?id=<?php echo $log['property_id']; ?>" target="_blank">ID: <?php echo $log['property_id']; ?></a>
                <?php
                if ($log['title']) {
                    echo ' ' .stripslashes($log['title']);
                } else if ($log['address1']) {
                    echo ' ' .stripslashes($log['address1']);
                }
                ?>
            </section>
            <section class="log_data">
                <?php
                $data = json_decode(str_replace("\n", '<br />', stripslashes($log['data'])), true);
                if (count($data) > 0) {
                    foreach ($data as $field_name => $field_value) {
                        $fielda[] = $field_name. ': ' .$field_value;
                    }
                    echo implode(', ', $fielda);
                }
                ?>
            </section>
            <section class="log_view" style="text-align: center;">
                <a class="icon_edit" title="View Log" href="./view/?id=<?php echo $log['id']; ?>"></a>
            </section>
            <section class="log_delete" style="text-align: center;">
                <a href="./delete/?id=<?php echo $log['id']; ?>" class="icon_delete" title="Delete Log" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
            </section>
        </li>

        <?php

        unset($data);

    }

// NO LOGS
} else {
    ?>
    <li style="text-align: center;">
        No logs.
    </li>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>