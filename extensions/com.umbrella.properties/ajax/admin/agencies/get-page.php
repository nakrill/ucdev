<?php

$admin->requireLevel(1);

// MODULE CLASS
if (!$_uccms_properties) $_uccms_properties = new uccms_Properties;

// CLEAN UP
$query          = isset($_GET["query"]) ? $_GET["query"] : "";
$page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
$limit          = isset($_GET['limit']) ? intval($_GET['limit']) : $_uccms_properties->perPage();

if (isset($_GET['base_url'])) {
    $base_url = $_GET['base_url'];
} else if (defined('MODULE_ROOT')) {
    $base_url = MODULE_ROOT. 'agencies';
} else {
    $base_url = '.';
}

//$_uccms_properties->setPerPage(5);

// WHERE ARRAY
unset($wa);
$wa[] = "ay.id>0";

// STATUSES
$statusa = array(
    'inactive'  => 0,
    'active'    => 1,
    'deleted'   => 9
);
if (isset($statusa[$_REQUEST['status']])) {
    $wa[] = "ay.status=" .$statusa[$_REQUEST['status']];
} else {
    $wa[] = "ay.status!=9";
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(ay.title) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED AGENCIES
$agency_query = "
SELECT ay.*
FROM `" .$_uccms_properties->tables['agencies']. "` AS `ay`
WHERE (" .implode(") AND (", $wa). ")
ORDER BY ay.title ASC, ay.id ASC
LIMIT " .(($page - 1) * $limit). "," .$limit;
$agency_q = sqlquery($agency_query);

// NUMBER OF PAGED AGENCIES
$num_agencies = sqlrows($agency_q);

// TOTAL NUMBER OF AGENCIES
$total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_properties->tables['agencies']. "` AS `ay` WHERE (" .implode(") AND (", $wa). ")"));

// NUMBER OF PAGES
$pages = $_uccms_properties->pageCount($total_results['total']);

// HAVE AGENCIES
if ($num_agencies > 0) {

    // ACCOUNTS ARRAY
    $accta = array();

    // LOOP
    while ($agency = sqlfetch($agency_q)) {

        // GET AGENTS
        $agent_query = "SELECT `id` FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`agency_id`=" .$agency['id']. ")";
        $agent_q = sqlquery($agent_query);
        $num_agents = sqlrows($agent_q);

        // GET PROPERTIES
        $prop_query = "SELECT `id` FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`agency_id`=" .$agency['id']. ")";
        $prop_q = sqlquery($prop_query);
        $num_properties = sqlrows($prop_q);

        // HAVE ACCOUNT ID
        if ($agency['account_id'] > 0) {

            // NOT IN ACCOUNT ARRAY
            if (!$accta[$agency['account_id']]) {

                // GET ACCOUNT INFO
                $acct_query = "
                SELECT a.id, a.username, a.email, ad.firstname, ad.lastname
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$agency['account_id']. ")
                LIMIT 1
                ";
                $acct_q = sqlquery($acct_query);
                $acct = sqlfetch($acct_q);
                $accta[$agency['account_id']] = $acct;

            }

        }

        ?>

        <li class="item">
            <section class="agency_title">
                <a href="<?php echo $base_url; ?>/edit/?id=<?php echo $agency['id']; ?>"><?php echo stripslashes($agency['title']); ?></a>
            </section>
            <section class="agency_agents">
                <a href="<?php echo $base_url; ?>/../agents/?agency_id=<?php echo $agency['id']; ?>"><?php echo $num_agents; ?></a>
            </section>
            <section class="agency_properties">
                <a href="<?php echo $base_url; ?>/../properties/?agency_id=<?php echo $agency['id']; ?>"><?php echo $num_properties; ?></a>
            </section>
            <section class="agency_account">
                <a href="/admin/accounts/edit/?id=<?php echo $agency['account_id']; ?>">
                    <?php
                    if (($accta[$agency['account_id']]['firstname']) || ($accta[$agency['account_id']]['lastname'])) {
                        echo trim(stripslashes($accta[$agency['account_id']]['firstname']. ' ' .$accta[$agency['account_id']]['lastname'])). ' (' .stripslashes($accta[$agency['account_id']]['email']). ')';
                    } else {
                        echo stripslashes($accta[$agency['account_id']]['email']);
                    }
                    ?>
                </a>
            </section>
            <section class="agency_status status_<?php if ($agency['status'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                <?php if ($agency['status'] == 1) { echo 'Active' ; } else { echo 'Inactive'; } ?>
            </section>
            <section class="agency_edit">
                <a class="icon_edit" title="Edit Agency" href="<?php echo $base_url; ?>/edit/?id=<?php echo $agency['id']; ?>"></a>
            </section>
            <section class="agency_delete">
                <a href="<?php echo $base_url; ?>/delete/?id=<?php echo $agency['id']; ?>" class="icon_delete" title="Delete Agency" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
            </section>
        </li>

        <?php

    }

    unset($accta);

// NO AGENCIES
} else {
    ?>
    <li style="text-align: center;">
        No agencies.
    </li>
    <?php
}

?>

<script>
    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
</script>