<?php

// BIGTREE
$bigtree['css'][]   = 'master.css';
$bigtree['js'][]    = 'master.js';

// MODULE CLASS
$_uccms_properties = new uccms_Properties;

// BREADCRUMBS
$bigtree['breadcrumb'] = [
    [ 'title' => 'Properties', 'link' => $bigtree['path'][1] ],
];

// MODULE MAIN NAV SELECTED ARRAY
if ($bigtree['path'][2]) {
    $mmnsa[$bigtree['path'][2]] = 'active';
} else {
    $mmnsa['dashboard'] = 'active';
}

?>

<nav class="main">
    <section>
        <ul>
            <li class="<?php echo $mmnsa['dashboard']; ?>">
                <a href="<?=MODULE_ROOT;?>"><span class="dashboard"></span>Dashboard</a>
                <?php /*
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>calendar/">Calendar</a></li>
                </ul>
                */ ?>
            </li>
            <li class="<?php echo $mmnsa['agencies']; ?>">
                <a href="<?=MODULE_ROOT;?>agencies/"><span class="pages"></span>Agencies</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>agencies/">Agencies</a></li>
                    <li><a href="<?=MODULE_ROOT;?>agents/">Agents</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>agencies/edit/">New Agency</a></li>
                    <li><a href="<?=MODULE_ROOT;?>agents/edit/">New Agent</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['locations']; ?>">
                <a href="<?=MODULE_ROOT;?>locations/"><span class="developer"></span>Locations</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>locations/">Cities & Neighborhoods</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['properties']; ?>">
                <a href="<?=MODULE_ROOT;?>properties/"><span class="developer"></span>Properties</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>properties/">All</a></li>
                    <li><a href="<?=MODULE_ROOT;?>properties/?status=active">Active</a></li>
                    <li><a href="<?=MODULE_ROOT;?>properties/?status=inactive">Inactive</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>properties/edit/?id=new">New Property</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>properties/email-log/">Email Logs</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['attributes']; ?>">
                <a href="<?=MODULE_ROOT;?>attributes/"><span class="developer"></span>Attributes</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>attributes/">All</a></li>
                    <li style="border-top: 1px solid #ccc;"><a href="<?=MODULE_ROOT;?>attributes/edit/?id=new">New Attribute</a></li>
                </ul>
            </li>
            <li class="<?php echo $mmnsa['settings']; ?>">
                <a href="<?=MODULE_ROOT;?>settings/"><span class="settings"></span>Settings</a>
                <ul>
                    <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                    <li><a href="<?=MODULE_ROOT;?>settings/payment/">Payment</a></li>
                    <li><a href="<?=MODULE_ROOT;?>settings/email/">Email</a></li>
                </ul>
            </li>
        </ul>
    </section>
</nav>
