<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE AGENCY
    $query = "UPDATE `" .$_uccms_properties->tables['agencies']. "` SET " .uccms_createSet($columns). ", `dt_deleted`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Agency', 'Agency deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Agency', 'Failed to delete agency.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Agency', 'No agency specified.');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT.'agencies/');
}

?>