<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET AGENCY INFO
    $agency_query = "SELECT * FROM `" .$_uccms_properties->tables['agencies']. "` WHERE (`id`=" .$id. ")";
    $agency_q = sqlquery($agency_query);
    $agency = sqlfetch($agency_q);

// NO ID SPECIFIED
} else {

    // USE URL VARS FOR BUSINESS INFO
    $agency = $_REQUEST;

}

// GET FRONTEND ACCOUNTS
$account_query = "
SELECT CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, a.*, ad.*
FROM `uccms_accounts` AS `a`
INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
ORDER BY `fullname` ASC, `email` ASC, a.id ASC
";
$account_q = sqlquery($account_query);
while ($account = sqlfetch($account_q)) {
    $accta[$account['id']] = $account;
}

// ARRAYS
if (!is_array($accta)) $accta = array();

?>

<div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="agency[id]" value="<?php echo $agency['id']; ?>" />

    <div class="container legacy">

        <header>
            <h2><?php if ($agency['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Agency</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left">

                    <fieldset>
                        <label>Agency Name</label>
                        <input type="text" value="<?=$agency['title']?>" name="agency[title]" />
                    </fieldset>

                    <fieldset>
                        <label>Image</label>
                        <div>
                            <?php
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'image', // The value you should use for the "name" attribute of your form field
                                'value'     => ($agency['image'] ? $_uccms_properties->imageBridgeOut($agency['image'], 'agencies') : ''), // The existing value for this form field
                                'id'        => 'agency_image', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label>Contact Name</label>
                        <input type="text" value="<?=$agency['contact_name']?>" name="agency[contact_name]" />
                    </fieldset>

                    <fieldset>
                        <label>Address</label>
                        <input type="text" value="<?=$agency['address1']?>" name="agency[address1]" />
                    </fieldset>

                    <fieldset>
                        <label>Address Cont.</label>
                        <input type="text" value="<?=$agency['address2']?>" name="agency[address2]" />
                    </fieldset>

                    <fieldset>
                        <label>City</label>
                        <input type="text" value="<?=$agency['city']?>" name="agency[city]" />
                    </fieldset>

                    <fieldset>
                        <label>State</label>
                        <select name="agency[state]">
                            <option value="">Select</option>
                            <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                <option value="<?=$state_code?>" <?php if ($state_code == $agency['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Zip</label>
                        <input type="text" value="<?=$agency['zip']?>" name="agency[zip]" style="display: inline; width: 50px;" />
                    </fieldset>

                </div>

                <div class="right">

                    <div style="margin-bottom: 15px; padding: 20px; background-color: #f5f5f5;">

                        <fieldset>
                            <label>Status</label>
                            <select name="agency[status]">
                                <option value="0" <?php if ($agency['status'] == 0) { ?>selected="selected"<?php } ?>>Inactive</option>
                                <option value="1" <?php if ($agency['status'] == 1) { ?>selected="selected"<?php } ?>>Active</option>
                            </select>
                        </fieldset>

                        <fieldset>
                            <label>Account</label>
                            <select name="agency[account_id]">
                                <option value="0">None</option>
                                <?php foreach ($accta as $account_id => $account) { ?>
                                    <option value="<?php echo $account_id; ?>" <?php if ($account_id == $agency['account_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($account['fullname']). ' (' .stripslashes($account['email']). ')'; ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>

                    </div>

                    <fieldset>
                        <label>Phone</label>
                        <input type="text" value="<?=$agency['phone']?>" name="agency[phone]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$agency['phone_ext']?>" name="agency[phone_ext]" style="display: inline-block; width: 30px;" />
                    </fieldset>

                    <fieldset>
                        <label>Mobile</label>
                        <input type="text" value="<?=$agency['phone_mobile']?>" name="agency[phone_mobile]" style="display: inline-block; width: 100px;" />
                    </fieldset>

                    <fieldset>
                        <label>Fax</label>
                        <input type="text" value="<?=$agency['fax']?>" name="agency[fax]" style="display: inline-block; width: 100px;" />
                    </fieldset>

                    <fieldset>
                        <label>Email</label>
                        <input type="text" value="<?=$agency['email']?>" name="agency[email]" />
                    </fieldset>

                    <fieldset>
                        <label>Lead Email</label>
                        <input type="text" value="<?=$agency['email_lead']?>" name="agency[email_lead]" />
                    </fieldset>

                    <fieldset>
                        <label>Website URL</label>
                        <input type="text" value="<?=$agency['url']?>" name="agency[url]" placeholder="www.domain.com" />
                    </fieldset>

                </div>

            </div>

            <div class="contain" style="margin-top: 10px;">

                <div class="left">

                    <div class="contain">

                        <div style="float: left; margin-right: 25px;">

                            <fieldset>
                                <label>Listing Price Override</label>
                                $<input type="text" name="agency[listing_price]" value="<?php if ($agency['listing_price']) { echo $_uccms_properties->toFloat($agency['listing_price']); } ?>" class="smaller_60" placeholder="<?php echo $_uccms_properties->toFloat($_uccms_properties->getSetting('listing_price')); ?>" style="display: inline-block;" />
                            </fieldset>

                        </div>

                        <div style="float: left;">
                            <fieldset>
                                <label>Per Override</label>
                                <select name="agency[listing_price_per]">
                                    <option value="">Select</option>
                                    <option value="w" <?php if ($agency['listing_price_per'] == 'w') { ?>selected="selected"<?php } ?>>Week</option>
                                    <option value="m" <?php if ($agency['listing_price_per'] == 'm') { ?>selected="selected"<?php } ?>>Month</option>
                                    <option value="q" <?php if ($agency['listing_price_per'] == 'q') { ?>selected="selected"<?php } ?>>Quarter</option>
                                    <option value="y" <?php if ($agency['listing_price_per'] == 'y') { ?>selected="selected"<?php } ?>>Year</option>
                                </select>
                            </fieldset>
                        </div>

                    </div>

                </div>

                <div class="right">

                </div>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
            <a class="button back" href="../">&laquo; Back</a>
        </footer>

    </div>

    </form>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>