<?php

//echo print_r($_POST);
//exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['agency']['id'];

    // USE AGENCY TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['agency']['slug']) {
        $_POST['agency']['slug'] = $_POST['agency']['name'];
    }

    // LISTING PRICE OVERRIDE
    if ($_POST['agency']['listing_price']) {
        $listing_price = $_uccms_properties->toFloat($_POST['agency']['listing_price']);
    } else {
        $listing_price = '';
    }

    // DB COLUMNS
    $columns = array(
        'slug'                      => $_uccms_properties->makeRewrite($_POST['agency']['slug']),
        'status'                    => (int)$_POST['agency']['status'],
        'account_id'                => (int)$_POST['agency']['account_id'],
        'title'                     => $_POST['agency']['title'],
        'contact_name'              => $_POST['agency']['contact_name'],
        'phone'                     => preg_replace('/\D/', '', $_POST['agency']['phone']),
        'phone_ext'                 => $_POST['agency']['phone_ext'],
        'phone_mobile'              => preg_replace('/\D/', '', $_POST['agency']['phone_mobile']),
        'fax'                       => preg_replace('/\D/', '', $_POST['agency']['fax']),
        'address1'                  => $_POST['agency']['address1'],
        'address2'                  => $_POST['agency']['address2'],
        'city'                      => $_POST['agency']['city'],
        'state'                     => $_POST['agency']['state'],
        'zip'                       => preg_replace('/\D/', '', $_POST['agency']['zip']),
        'url'                       => $_uccms_properties->trimURL($_POST['agency']['url']),
        'email'                     => $_POST['agency']['email'],
        'email_lead'                => $_POST['agency']['email_lead'],
        'listing_price'             => $listing_price,
        'listing_price_per'         => $_POST['agency']['listing_price_per']
    );

    // HAVE AGENCY ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_properties->tables['agencies']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";

    // NO ITEM ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_properties->tables['agencies']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Agency', 'Agency added!');
        } else {
            $admin->growl('Agency', 'Agency updated!');
        }

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['image']['name']) || ((!$_POST['image']) || (substr($_POST['image'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `image` FROM `" .$_uccms_properties->tables['agencies']. "` WHERE (`id`=" .$id. ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['image']) {

                // REMOVE IMAGE
                @unlink($_uccms_properties->imageBridgeOut($ex['image'], 'agencies', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_properties->tables['agencies']. "` SET `image`='' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['image']['name']) || ($_POST['image'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'Agency Image',
                'key'           => 'image',
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_properties->Extension. '/files/agencies',
                    'image' => true,
                    'thumbs' => array(
                        array(
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES['image']['name']) {
                $field['file_input'] = $_FILES['image'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['image']) {
                $field['input'] = $_POST['image'];
            }

            // DIGITAL ASSET MANAGER VARS
            $field['dam_vars'] = array(
                'website_extension'         => $_uccms_properties->Extension,
                'website_extension_item'    => 'agency',
                'website_extension_item_id' => $id
            );

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_properties->tables['agencies']. "` SET `image`='" .sqlescape($_uccms_properties->imageBridgeIn($file_path)). "' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                echo print_r($bigtree['errors']);
                exit;

                $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Agency', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT. 'agencies/edit/?id=' .$id);

?>