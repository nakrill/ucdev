<?php

// GET SETTINGS
//$settings = $_uccms_properties->getSettings();

// $gateway = new BigTreePaymentGateway;

// GET PAYMENT METHODS
$pma = $_uccms_properties->getPaymentMethods();

?>


<style type="text/css">

    .payment_methods .method {
        margin-bottom: 10px;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    .payment_methods .method:last-child {
        margin-bottom: 0px;
    }

    .payment_methods .method .top .icon {
        float: left;
        display: inline-block;
        width: 32px;
        height: 32px;
        background: url("<?=ADMIN_ROOT?>images/icon-sprite.svg") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
    }
    .payment_methods .method .top .icon_email {
        float: left;
        margin: 4px 4px 6px 4px;
    }
    .payment_methods .method .top .icon.instapay {
        background: url("<?=ADMIN_ROOT?>images/instapay.jpg") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
    }

    .payment_methods .method.authorize .top .icon {
        background-position: -124px -214px;
    }
    .payment_methods .method.paypal-rest .top .icon, .payment_methods .method.paypal .top .icon {
        background-position: -94px -214px;
    }
    .payment_methods .method.payflow .top .icon {
        background-position: -190px -214px;
    }
    .payment_methods .method.linkpoint .top .icon {
        background-position: -155px -214px;
    }

    .payment_methods .method .top p {
        float: left;
        margin: 0px;
        padding: 8px 0 0 10px;
    }

    .payment_methods .method .top fieldset.right {
        width: auto;
        margin: 0px;
        padding: 8px 0 0 0;
    }

    .payment_methods .method .options {
        margin-top: 10px;
        padding-top: 10px;
        border-top: 1px solid #ccc;
    }

    .payment_methods .method .options .left {
        margin-right: 0px
    }

    .payment_methods .method .options textarea {
        height: 47px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // PAYMENT METHOD ENABLE TOGGLE
        $('.payment_methods .method .top .pm_enable').change(function(e) {
            var what = $(this).attr('id').replace('pm_', '');
            if ($(this).attr('checked')) {
                $('.payment_methods .method.' +what+ ' .options').show();
            } else {
                $('.payment_methods .method.' +what+ ' .options').hide();
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>Payment Settings</h2>
    </header>

    <section class="payment_methods">

        <?php foreach ($_uccms_properties->allPaymentMethods() as $pm_id => $pm_title) { ?>

            <div class="method <?=$pm_id?>">
                <div class="contain top">
                    <span class="icon<?php if ($pm_id == 'check_cash') { echo '_email'; } ?> <?php echo $pm_id; ?>"></span>
                    <p><?=$pm_title?></p>
                    <fieldset class="right">
                        <input id="pm_<?=$pm_id?>" type="checkbox" name="pm[<?=$pm_id?>]" value="1" <?php if ($pma[$pm_id]['active']) { ?>checked="checked"<?php } ?> class="pm_enable" />
                        <label class="for_checkbox">Enabled</label>
                    </fieldset>
                </div>
                <div class="options" style="<?php if (!$pma[$pm_id]['active']) { ?>display: none;<?php } ?>">
                    <div class="contain">
                        <div class="left last">
                            <fieldset>
                                Manage account settings in the <a href="<?=ADMIN_ROOT?>developer/payment-gateway/<?=$pm_id?>/" target="_blank">Payment Gateway</a> section.
                            </fieldset>
                            <fieldset>
                                <label>Display Name</label>
                                <input type="text" name="title[<?=$pm_id?>]" value="<?php echo stripslashes($pma[$pm_id]['title']); ?>" />
                            </fieldset>
                        </div>
                        <div class="right last">
                            <fieldset>
                                <label>Description / Instructions</label>
                                <textarea name="description[<?=$pm_id?>]"><?php echo stripslashes($pma[$pm_id]['description']); ?></textarea>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

</form>