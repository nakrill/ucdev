<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SET ALL TO INACTIVE QUICKLY
    $query = "UPDATE `" .$_uccms_properties->tables['payment_methods']. "` SET `active`=0";
    sqlquery($query);

    // HAVE ONES TO UPDATE
    if (count($_POST['pm']) > 0) {

        // LOOP
        foreach ($_POST['pm'] as $pm => $active) {

            // DB COLUMNS
            $columns = array(
                'id'                => $pm,
                'active'            => (int)$active,
                'title'             => $_POST['title'][$pm],
                'description'       => $_POST['description'][$pm]
            );

            $fields = $_uccms_properties->createSet($columns);

            // ADD / UPDATE
            $query = "
            INSERT INTO `" .$_uccms_properties->tables['payment_methods']. "`
            SET " .$fields. "
            ON DUPLICATE KEY UPDATE " .$fields. "
            ";
            sqlquery($query);

        }

    }

    $admin->growl('Payment Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/payment/');

?>