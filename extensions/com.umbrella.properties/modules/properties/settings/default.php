<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET SETTINGS
$settings = $_uccms_properties->getSettings();

?>

<form enctype="multipart/form-data" action="./process/" method="post">

<? /*
<div class="container legacy">

    <header>
        <h2>General</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>
*/ ?>

<div class="container legacy">

    <header>
        <h2>Home</h2>
    </header>

    <section>

        <fieldset>
            <label>Content</label>
            <div>
                <?php
                $field = array(
                    'key'       => 'setting[home_content]', // The value you should use for the "name" attribute of your form field
                    'value'     => stripslashes($settings['home_content']), // The existing value for this form field
                    'id'        => 'home_content', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'simple' => false
                    )
                );
                include(BigTree::path('admin/form-field-types/draw/html.php'));
                ?>
            </div>
        </fieldset>

        <? /*
        <div class="contain" style="padding-bottom: 20px;">

            <div class="left last">

                <fieldset>
                    <label>Number of Featured Properties</label>
                    <input type="text" name="setting[home_featured_num]" value="<?php echo (int)$settings['home_featured_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset>
                    <label>Featured Properties Title</label>
                    <input type="text" name="setting[home_featured_title]" value="<?php echo stripslashes($settings['home_featured_title']); ?>" placeholder="Featured Properties" />
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <label>Number of Recent Properties</label>
                    <input type="text" name="setting[home_recent_num]" value="<?php echo (int)$settings['home_recent_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset>
                    <label>Recent Properties Title</label>
                    <input type="text" name="setting[home_recent_title]" value="<?php echo stripslashes($settings['home_recent_title']); ?>" placeholder="Recent Properties" />
                </fieldset>

            </div>

        </div>
        */ ?>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="home_seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-home_seo" style="display: none;">

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="setting[home_meta_title]" value="<?php echo stripslashes($settings['home_meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="setting[home_meta_description]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="setting[home_meta_keywords]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Browsing</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Number of Listings Per Page</label>
                    <input type="text" name="setting[listings_per_page]" value="<?php echo (int)$settings['listings_per_page']; ?>" class="smaller_60" />
                </fieldset>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<script type="text/javascript">

    $(document).ready(function() {

        // CONTACT FIELDS TOGGLE
        $('#listing_contact_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_listing_contact').show();
            } else {
                $('#toggle_listing_contact').hide();
            }
        });

        // CONTACT FIELDS - SORTABLE
        $('form .listing_contact table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // CONTACT FIELDS - ADD
        $('form .listing_contact .add').click(function(e) {
            e.preventDefault();
            $('#add_listing_contact_field').toggle();
        });

        // CONTACT FIELDS - REMOVE
        $('form .listing_contact .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

        // PROPERTY - REVIEW CRITERIA TOGGLE
        $('#listing_ratings_reviews_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_listing_reviews').show();
            } else {
                $('#toggle_listing_reviews').hide();
            }
        });

        // PROPERTY - REVIEW CRITERIA - SORTABLE
        $('form .property_review-criteria table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // PROPERTY - REVIEW CRITERIA - ADD
        $('form .property_review-criteria .add').click(function(e) {
            e.preventDefault();
            $('#add_property_review-criteria').toggle();
        });

        // PROPERTY - REVIEW CRITERIA - REMOVE
        $('form .property_review-criteria .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

        // CHANGE CATEGORY ENABLED TOGGLE
        $('#listing_change_category_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_listing_change_category').show();
            } else {
                $('#toggle_listing_change_category').hide();
            }
        });

    });

</script>

<a name="property_listing"></a>
<div class="container legacy">

    <header>
        <h2>Property Listing</h2>
    </header>

    <section>

        <div class="contain">

            <div class="contain" style="margin-bottom: 10px;">

                <div class="left">

                    <div class="contain" style="margin-bottom: 15px;">

                        <div style="float: left; margin-right: 25px;">

                            <fieldset>
                                <label>Listing Price</label>
                                $<input type="text" name="setting[listing_price]" value="<?php echo $_uccms_properties->toFloat($settings['listing_price']); ?>" class="smaller_60" style="display: inline-block;" />
                            </fieldset>

                        </div>

                        <div style="float: left;">
                            <fieldset>
                                <label>Per</label>
                                <select name="setting[listing_price_per]">
                                    <option value="w" <?php if ($settings['listing_price_per'] == 'w') { ?>selected="selected"<?php } ?>>Week</option>
                                    <option value="m" <?php if (($settings['listing_price_per'] == 'm') || (!$settings['listing_price_per'])) { ?>selected="selected"<?php } ?>>Month</option>
                                    <option value="q" <?php if ($settings['listing_price_per'] == 'q') { ?>selected="selected"<?php } ?>>Quarter</option>
                                    <option value="y" <?php if ($settings['listing_price_per'] == 'y') { ?>selected="selected"<?php } ?>>Year</option>
                                </select>
                            </fieldset>
                        </div>

                    </div>

                    <fieldset>
                        <input type="checkbox" name="setting[cron_auto_charge]" value="1" <?php if ($settings['cron_auto_charge']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Automatically renew listings and charge card on file.</label>
                    </fieldset>

                </div>

                <div class="right">

                </div>

            </div>

            <fieldset>
                <input id="listing_contact_enabled" type="checkbox" name="setting[listing_contact_enabled]" value="1" <?php if ($settings['listing_contact_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Contact Form Enabled</label>
            </fieldset>

            <div id="toggle_listing_contact" class="contain" style="<?php if (!$settings['listing_contact_enabled']) { ?>display: none;<?php } ?>">

                <div class="left">

                    <fieldset class="listing_contact">
                        <div class="contain">
                            <div class="left inner_quarter last">
                                <label>Fields</label>
                            </div>
                            <div class="left inner_quarter last" style="margin: 0px; text-align: right;">
                                <a href="#" class="add button_small">Add</a>
                            </div>
                        </div>
                        <div id="add_listing_contact_field" style="display: none; padding: 5px 0 10px 0;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px;">
                                <tr>
                                    <td style="padding-right: 0px;">
                                        <input type="text" name="listing_contact_field_title[0]" value="" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td style="padding-right: 0px;">
                                        <input type="checkbox" name="listing_contact_field_required[0]" value="1" /> Req
                                    </td>
                                    <td>
                                        <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                            <?php foreach ($_uccms_properties->listingContactFields() as $field_id => $field) { ?>
                                <tr id="pg-<?=$field_id?>" class="item">
                                    <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                                    <td width="60%" style="padding-right: 0px;">
                                        <input type="text" name="listing_contact_field_title[<?=$field_id?>]" value="<?=$field['title']?>" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td width="40%" style="padding-right: 0px; white-space: nowrap;">
                                         <input type="checkbox" name="listing_contact_field_required[<?=$field_id?>]" value="1" <?php if ($field['required'] == 1) { ?>checked="checked"<?php } ?> /> Req
                                    </td>
                                    <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </fieldset>

                </div>

                <div class="right">

                    <fieldset>
                        <label>Success Message</label>
                        <div>
                            <?php
                            $field = array(
                                'key'       => 'setting[listing_contact_success]', // The value you should use for the "name" attribute of your form field
                                'value'     => stripslashes($settings['listing_contact_success']), // The existing value for this form field
                                'id'        => 'listing_contact_success', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'simple' => false
                                )
                            );
                            include(BigTree::path('admin/form-field-types/draw/html.php'));
                            ?>
                        </div>
                    </fieldset>

                </div>

            </div>

            <div class="contain">

                <fieldset>
                    <input id="listing_ratings_reviews_enabled" type="checkbox" name="setting[ratings_reviews_enabled]" value="1" <?php if ($settings['ratings_reviews_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Ratings & Reviews Enabled</label>
                </fieldset>

                <div id="toggle_listing_reviews" class="contain" style="<?php if (!$settings['ratings_reviews_enabled']) { ?>display: none;<?php } ?>">

                    <div class="left">

                        <fieldset class="property_review-criteria">
                            <div class="contain">
                                <div class="left inner_quarter last">
                                    <label>Review Criteria</label>
                                </div>
                                <div class="left inner_quarter last" style="margin: 0px; text-align: right;">
                                    <a href="#" class="add button_small">Add</a>
                                </div>
                            </div>
                            <div id="add_property_review-criteria" style="display: none; padding: 5px 0 10px 0;">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px;">
                                    <tr>
                                        <td style="padding-right: 0px;">
                                            <input type="text" name="property_review-criteria_title[0]" value="" placeholder="Title" style="width: 250px;" />
                                        </td>
                                        <td style="padding-right: 0px;">
                                            <input type="checkbox" name="property_review-criteria_required[0]" value="1" /> Req
                                        </td>
                                        <td>
                                            <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                                <?php foreach ($_uccms_properties->review_criteria('property') as $criteria_id => $criteria) { ?>
                                    <tr id="prc-<?=$criteria_id?>" class="item">
                                        <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                                        <td width="60%" style="padding-right: 0px;">
                                            <input type="text" name="property_review-criteria_title[<?=$criteria_id?>]" value="<?=$criteria['title']?>" placeholder="Title" style="width: 250px;" />
                                        </td>
                                        <td width="40%" style="padding-right: 0px; white-space: nowrap;">
                                             <input type="checkbox" name="property_review-criteria_required[<?=$criteria_id?>]" value="1" <?php if ($criteria['required'] == 1) { ?>checked="checked"<?php } ?> /> Req
                                        </td>
                                        <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </fieldset>

                    </div>

                    <div class="right">

                    </div>

                </div>

            </div>

            <? /*
            <fieldset>
                <label>Max Images per Listing</label>
                <input type="text" name="setting[listing_max_images]" value="<?php echo $settings['listing_max_images']; ?>" class="smaller_60" />
                <small>0 = none allowed, blank = no limit</small>
            </fieldset>

            <fieldset class="last">
                <label>Max Videos per Listing</label>
                <input type="text" name="setting[listing_max_videos]" value="<?php echo $settings['listing_max_videos']; ?>" class="smaller_60" />
                <small>0 = none allowed, blank = no limit</small>
            </fieldset>
            */ ?>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<? /*
<script type="text/javascript">

    $(document).ready(function() {

        // NEIGHBORHOODS - SORTABLE
        $('#neighborhoods table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // NEIGHBORHOODS - ADD
        $('#neighborhoods .add').click(function(e) {
            e.preventDefault();
            $('#add_neighborhood').toggle();
        });

        // NEIGHBORHOODS - REMOVE
        $('#neighborhoods .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

    });

</script>

<?php

// NEIGHBORHOODS ARRAY
$hooda = $_uccms_properties->neighborhoods();

?>

<div id="neighborhoods" class="container table">

    <summary>
        <h2>Neighborhoods</h2>
        <a class="add_resource add" href="#"><span></span>Add Neighborhood</a>
    </summary>

    <section>

        <fieldset>

            <div id="add_neighborhood" style="display: none; margin-bottom: 15px; padding-bottom: 10px; border-bottom: 1px solid #ccc;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: auto; margin-bottom: 0px; border: 0px none;">
                    <tr>
                        <td>
                            <input type="text" name="neighborhood_title[0]" value="" placeholder="Name" style="width: 300px;" />
                        </td>
                        <td>
                            <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                        </td>
                    </tr>
                </table>
            </div>

            <?php if (count($hooda) > 0) { ?>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                    <?php foreach ($hooda as $hood_id => $hood) { ?>
                        <tr id="loc-<?=$hood_id?>" class="item">
                            <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                            <td style="padding-right: 0px;" width="90%"><input type="text" name="neighborhood_title[<?=$hood_id?>]" value="<?=$hood['title']?>" placeholder="Name" style="width: 300px;" /></td>
                            <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php } ?>
                </table>
            <?php } else { ?>
                <div style="text-align: center;">
                    No neighborhoods set up yet.
                </div>
            <?php } ?>

        </fieldset>

    </section>

    <?php if (count($hooda) > 0) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>
*/ ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>

</form>