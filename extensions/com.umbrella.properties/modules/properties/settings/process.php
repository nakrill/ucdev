<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // FORMAT
    if ($_POST['setting']['listing_price_mo']) $_POST['setting']['listing_price_mo'] = number_format($_POST['setting']['listing_price_mo'], 2);
    if ($_POST['setting']['listing_price_yr']) $_POST['setting']['listing_price_yr'] = number_format($_POST['setting']['listing_price_yr'], 2);

    // SETTINGS ARRAY
    $seta = array(
        'home_content',
        'home_featured_num',
        'home_featured_title',
        'home_recent_num',
        'home_recent_title',
        'home_meta_title',
        'home_meta_description',
        'home_meta_keywords',
        'listings_per_page',
        'listing_price',
        'listing_price_per',
        'cron_auto_charge',
        'listing_max_images',
        'listing_max_videos',
        'listing_contact_enabled',
        'listing_contact_success',
        'ratings_reviews_enabled'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_properties->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('General Settings', 'Settings saved!');

    ########################
    # LISTING CONTACT FIELDS
    ########################

    // NEW FIELD
    $new_field['title']     = $_POST['listing_contact_field_title'][0];
    $new_field['required']  = $_POST['listing_contact_field_required'][0];
    unset($_POST['listing_contact_field_title'][0]);
    unset($_POST['listing_contact_field_required'][0]);

    // CONTACT FIELD ARRAY
    $cfa = array();

    // TOP FIELD ID
    $top_field_id = 0;

    // CONTACT FIELDS SPECIFIED
    if (count($_POST['listing_contact_field_title']) > 0) {

        // LOOP
        foreach ($_POST['listing_contact_field_title'] as $field_id => $field_title) {
            $cfa[$field_id] = array(
                'title'     => $field_title,
                'required'  => (int)$_POST['listing_contact_field_required'][$field_id]
            );
            if ($field_id > $top_field_id) $top_field_id = $field_id;
        }

    // NO FIELDS SPECIFIED
    } else {

        // USE DEFAULTS
        foreach ($_uccms_properties->listingContactFields() as $field_id => $field) {
            $cfa[$field_id] = $field;
            if ($field_id > $top_field_id) $top_field_id = $field_id;
        }

    }

    // HAVE NEW FIELD
    if ($new_field['title']) {
        $cfa[$top_field_id+1] = array(
            'title'     => $new_field['title'],
            'required'  => $new_field['required']
        );
    }

    // SAVE LISTING CONTACT FIELDS
    $_uccms_properties->setSetting('listing_contact_fields', $cfa);

    ########################
    # PROPERTY - REVIEW CRITERIA
    ########################

    // NEW FIELD
    $new_field['title']     = $_POST['property_review-criteria_title'][0];
    $new_field['required']  = $_POST['property_review-criteria_required'][0];
    unset($_POST['property_review-criteria_title'][0]);
    unset($_POST['property_review-criteria_required'][0]);

    // REVIEW CRITERIA FIELD ARRAY
    $prca = array();

    // TOP FIELD ID
    $top_field_id = 0;

    // REVIEW CRITERIA FIELDS SPECIFIED
    if (count($_POST['property_review-criteria_title']) > 0) {

        // LOOP
        foreach ($_POST['property_review-criteria_title'] as $field_id => $field_title) {
            $prca[$field_id] = array(
                'title'     => $field_title,
                'required'  => (int)$_POST['property_review-criteria_required'][$field_id]
            );
            if ($field_id > $top_field_id) $top_field_id = $field_id;
        }

    }

    // HAVE NEW FIELD
    if ($new_field['title']) {
        $prca[$top_field_id+1] = array(
            'title'     => $new_field['title'],
            'required'  => $new_field['required']
        );
    }

    // SAVE REVIEW CRITERIA FIELDS
    $_uccms_properties->setSetting('review_criteria-property', $prca);

    /*
    ########################
    # NEIGHBORHOODS
    ########################

    // NEW NEIGHBORHOOD
    $new_neighborhood['title'] = $_POST['neighborhood_title'][0];
    unset($_POST['neighborhood_title'][0]);

    // NEIGHBORHOODS ARRAY
    $la = array();

    // TOP NEIGHBORHOOD ID
    $top_neighborhood_id = 0;

    // NEIGHBORHOODS SPECIFIED
    if (count($_POST['neighborhood_title']) > 0) {

        // LOOP
        foreach ($_POST['neighborhood_title'] as $neighborhood_id => $neighborhood_title) {
            $la[$neighborhood_id] = array(
                'title' => $neighborhood_title
            );
            if ($neighborhood_id > $top_neighborhood_id) $top_neighborhood_id = $neighborhood_id;
        }

    // NO NEIGHBORHOODS SPECIFIED
    } else {

        // USE DEFAULTS
        foreach ($_uccms_properties->neighborhoods() as $neighborhood_id => $neighborhood) {
            $la[$neighborhood_id] = $neighborhood;
            if ($neighborhood_id > $top_neighborhood_id) $top_neighborhood_id = $neighborhood_id;
        }

    }

    // HAVE NEW NEIGHBORHOOD
    if ($new_neighborhood['title']) {
        $la[$top_neighborhood_id+1] = array(
            'title' => $new_neighborhood['title']
        );
    }

    // SAVE NEIGHBORHOODS
    $_uccms_properties->setSetting('neighborhoods', $la);

    ########################
    */

}

BigTree::redirect(MODULE_ROOT.'settings/');

?>