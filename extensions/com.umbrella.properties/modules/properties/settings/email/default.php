<?php

// GET SETTINGS
$settings = $_uccms_properties->getSettings();

?>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy">

    <header>
        <h2>Email Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>From Name</label>
                <input type="text" name="setting[email_from_name]" value="<?php echo $settings['email_from_name']; ?>" />
            </fieldset>

            <fieldset>
                <label>From Email</label>
                <input type="text" name="setting[email_from_email]" value="<?php echo $settings['email_from_email']; ?>" />
            </fieldset>

        </div>

        <div class="right last">

            <fieldset>
                <label>Recieve admin notification emails at:</label>
                <input type="text" name="setting[email_admin_notify]" value="<?php echo $settings['email_admin_notify']; ?>" />
                <label><small>(Seperate email addresses by comma.)</small></label>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

</form>