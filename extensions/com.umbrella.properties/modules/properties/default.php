<style type="text/css">

    #properties_dashboard .col_left {
        width: 100%;
        padding-top: 0px;
        padding-left: 0px;
        vertical-align: top;
    }

    #properties_dashboard .col_right {
        margin-left: 30px;
        border-left: 1px solid #ccc;
        background-color: #f5f5f5;
    }
    #properties_dashboard .col_right .size {
        width: 200px;
        min-height: 400px;
        padding: 0 15px;
    }

    #properties_dashboard .toggle_bar {
        padding: 15px 8px;
        background-color: #f8f8f8;
    }
    #properties_dashboard .toggle_bar ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #properties_dashboard .toggle_bar ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 10px;
        font-weight: bold;
        text-transform: uppercase;
    }
    #properties_dashboard .toggle_bar ul li a {
        color: #aaa;
    }
    #properties_dashboard .toggle_bar ul li a:hover, #properties_dashboard .toggle_bar ul li a.active {
        color: #333;
    }
    #properties_dashboard .toggle_bar ul li a .num {
        font-size: .9em;
        font-weight: normal;
        color: #aaa;
    }

    #properties_dashboard .toggle_content_container {
    }
    #properties_dashboard .toggle_content_container .toggle {
        display: none;
    }
    #properties_dashboard .toggle_content_container .toggle:first-child {
        display: block;
    }
    #properties_dashboard .toggle_content_container .toggle .none {
        padding: 15px;
        text-align: center;
    }

    #properties_dashboard .toggle_content_container .item {
        padding: 15px 15px 15px 0;
        line-height: 1em;
    }
    #properties_dashboard .toggle_content_container .item:hover {
        background-color: #f5faff;
    }
    #properties_dashboard .toggle_content_container .item table {
        width: 100%;
        margin: 0px;
        padding: 0px;
        border: 0px none;
    }
    #properties_dashboard .toggle_content_container .item table td {
        padding: 0px;
        vertical-align: top;
    }
    #properties_dashboard .toggle_content_container .item table td table td {
        font-size: 1em;
        color: #999;
        text-transform: uppercase;
    }
    #properties_dashboard .toggle_content_container .item td.icon {
        padding: 0 15px;
        white-space: nowrap;
        font-size: 2em;
        opacity: .6;
    }
    #properties_dashboard .toggle_content_container .item .icon i.fa {
        /*margin-top: -3px;*/
    }
    #properties_dashboard .toggle_content_container .item td.content {
        width: 100%;
    }
    #properties_dashboard .toggle_content_container .item .content .title {
        padding-bottom: 6px;
        font-size: 1.2em;
        font-weight: bold;
    }
    #properties_dashboard .toggle_content_container .item .content .title a {
        color: #555;
    }
    #properties_dashboard .toggle_content_container .item .content .status {
        width: 20%;
    }
    #properties_dashboard .toggle_content_container .item .content .status .active {
        color: #6BD873;
    }
    #properties_dashboard .toggle_content_container .item .content .status .submitted {
        color: #4dd0e1;
    }
    #properties_dashboard .toggle_content_container .item .content .type {
        width: 20%;
    }
    #properties_dashboard .toggle_content_container .item .content .date {
        width: 20%;
    }
    #properties_dashboard .toggle_content_container .item .content .actions {
        width: 40%;
        text-align: right;
    }
    #properties_dashboard .toggle_content_container .item .content .actions ul {
        opacity: 0;
    }
    #properties_dashboard .toggle_content_container .item:hover .content .actions ul {
        opacity: 1;
    }
    #properties_dashboard .toggle_content_container .item ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #properties_dashboard .toggle_content_container .item ul li {
        display: inline-block;
        margin: 0px;
        padding: 0 0 0 8px;
        line-height: 1em;
    }
    #properties_dashboard .toggle_content_container .more {
        padding-top: 15px;
        text-align: center;
    }

    #properties_dashboard .activity_container {
        border-left: 2px solid #bbb;
    }

    #properties_dashboard .activity_container .day {
        position: relative;
        margin-top: 20px;
        padding-bottom: 20px;
    }

    #properties_dashboard .activity_container .day:first-child {
        margin-top: 0px;
    }

    #properties_dashboard .activity_container .day .icon {
        position: absolute;
        top: 5px;
        left: -7px;
        width: 0.9em;
        height: 0.9em;
        background-color: #bbb;
        border-radius: 0.9em;
    }

    #properties_dashboard .activity_container .day .date {
        margin-left: 20px;
        padding: 5px 10px;
        background-color: #e0e0e0;
        border-radius: 10px;
        text-align: center;
        font-weight: bold;
        color: #666;
    }

    #properties_dashboard .activity_container .item {
        position: relative;
    }

    #properties_dashboard .activity_container .item .icon {
        position: absolute;
        top: -3px;
        left: -10px;
        width: 1.6em;
        height: 1.6em;
        background-color: #aaa;
        text-align: center;
        font-size: 1em;
        line-height: 1.6em;
        color: #fff;
        border-radius: 1.6em;
    }

    #properties_dashboard .activity_container .item .main {
        padding-left: 20px;
    }

    #properties_dashboard .activity_container .item .main .title, #properties_dashboard .activity_container .item .main .title a {
        padding-bottom: 3px;
        font-size: 1.05em;
        font-weight: bold;
        color: #555;
    }

    #properties_dashboard .activity_container .item .main .time {
        padding-bottom: 3px;
        font-size: .9em;
        color: #777;
    }

    #properties_dashboard .activity_container .item .main .details {
        padding-top: 1px;
        padding-bottom: 3px;
        font-size: .9em;
        color: #999;
    }

    #properties_dashboard .activity_container .item .main .status {
        padding-bottom: 2px;
    }
    #properties_dashboard .activity_container .item .main .action {
        color: #777;
    }

    #properties_dashboard .activity_container .split {
        height: 1px;
        margin: 15px 0;
        border-bottom: 1px dashed #ddd;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        $('#properties_dashboard .toggle_bar a').click(function(e) {
            e.preventDefault();
            var set = $(this).closest('.toggle_bar').attr('data-set');
            var what = $(this).attr('data-what');
            $('#properties_dashboard .toggle_bar a').removeClass('active');
            $(this).addClass('active');
            $('#properties_dashboard .toggle_content_container.set-' +set+ ' .toggle').hide();
            $('#properties_dashboard .toggle_content_container.set-' +set+ ' .toggle.' +what).show();

        });

    });

</script>


<div id="properties_dashboard">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 0px;">
        <tr>

            <td class="col_left">

                <?php

                // GET ALL PROPERTIES
                $all_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`status`!=9)";
                $all_q = sqlquery($all_query);
                $num_all = sqlrows($all_q);
                $all_query = $all_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $all_q = sqlquery($all_query);

                // GET ACTIVE PROPERTIES
                $active_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`status`=1)";
                $active_q = sqlquery($active_query);
                $num_active = sqlrows($active_q);
                $active_query = $active_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $active_q = sqlquery($active_query);

                // GET SALES
                $sale_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`listing_type`=1) AND (`status`!=9)";
                $sale_q = sqlquery($sale_query);
                $num_sale = sqlrows($sale_q);
                $sale_query = $sale_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $sale_q = sqlquery($sale_query);

                // GET RENTALS
                $rental_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`listing_type`=2) AND (`status`!=9)";
                $rental_q = sqlquery($rental_query);
                $num_rental = sqlrows($rental_q);
                $rental_query = $rental_query. " ORDER BY `dt_created` DESC, `id` DESC LIMIT 12";
                $rental_q = sqlquery($rental_query);

                ?>

                <div class="toggle_bar" data-set="properties">
                    <ul>
                        <li><a href="#" class="active" data-what="all">All Properties <span class="num">(<?php echo number_format($num_all, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="active">Active <span class="num">(<?php echo number_format($num_active, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="sale">Sale <span class="num">(<?php echo number_format($num_sale, 0); ?>)</span></a></li>
                        <li><a href="#" data-what="rental">Rental <span class="num">(<?php echo number_format($num_rental, 0); ?>)</span></a></li>
                    </ul>
                </div>

                <div class="toggle_content_container set-properties">

                    <div class="toggle all">
                        <?php if ($num_all > 0) { ?>
                            <div class="items">
                                <?php
                                while ($property = sqlfetch($all_q)) {
                                    echo this_item_property($property);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./properties/" class="button">All Properties</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No properties.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle active">
                        <?php if ($num_active > 0) { ?>
                            <div class="items">
                                <?php
                                while ($property = sqlfetch($active_q)) {
                                    echo this_item_property($property);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./properties/?status=active" class="button">All Active Properties</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No active properties.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle sale">
                        <?php if ($num_sale > 0) { ?>
                            <div class="items">
                                <?php
                                while ($property = sqlfetch($sale_q)) {
                                    echo this_item_property($property);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./properties/?type=sale" class="button">All Sales</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No sale listings.
                            </div>
                        <?php } ?>
                    </div>

                    <div class="toggle rental">
                        <?php if ($num_rental > 0) { ?>
                            <div class="items">
                                <?php
                                while ($property = sqlfetch($rental_q)) {
                                    echo this_item_property($property);
                                }
                                ?>
                            </div>
                            <div class="more">
                                <a href="./properties/?type=rental" class="button">All Rentals</a>
                            </div>
                        <?php } else { ?>
                            <div class="none">
                                No rental listings.
                            </div>
                        <?php } ?>
                    </div>

                </div>

            </td>

            <td class="col_right" valign="top">
                <div class="size">

                    <h3 style="text-align: center;">Recent Activity</h3>

                    <div class="activity_container">

                        <?php

                        // ITEM ARRAY
                        $itema = array();

                        // LAST CREATED
                        $lcreated_query = "SELECT *, `dt_created` AS `sort_by`, 'created' AS `item_what` FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`dt_created`!='0000-00-00 00:00:00') ORDER BY `dt_created` DESC LIMIT 10";
                        $lcreated_q = sqlquery($lcreated_query);
                        while ($lcreated = sqlfetch($lcreated_q)) {
                            $itema[] = $lcreated;
                        }

                        // LAST UPDATED
                        $lupdated_query = "SELECT *, `dt_updated` AS `sort_by`, 'updated' AS `item_what` FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`dt_updated`!='0000-00-00 00:00:00') AND (`dt_updated`!=`dt_created`) ORDER BY `dt_updated` DESC LIMIT 10";
                        $lupdated_q = sqlquery($lupdated_query);
                        while ($lupdated = sqlfetch($lupdated_q)) {
                            $itema[] = $lupdated;
                        }

                        // LAST DELETED
                        $ldeleted_query = "SELECT *, `dt_deleted` AS `sort_by`, 'deleted' AS `item_what` FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`dt_deleted`!='0000-00-00 00:00:00') ORDER BY `dt_deleted` DESC LIMIT 10";
                        $ldeleted_q = sqlquery($ldeleted_query);
                        while ($ldeleted = sqlfetch($ldeleted_q)) {
                            $itema[] = $ldeleted;
                        }

                        // HAVE ITEMS
                        if (count($itema) > 0) {

                            // SORT BY DATE/TIME ASCENDING
                            usort($itema, function($a, $b) {
                                return strtotime($b['sort_by']) - strtotime($a['sort_by']);
                            });

                            // STATUSES
                            $statusa = array(
                                'created'   => array(
                                    'title'     => 'Created',
                                    'icon'      => 'fa-plus-circle',
                                    'color'     => '#85eeb8',
                                    'actions'   => '<a href="./properties/edit/?id={id}">Edit</a>'
                                ),
                                'updated' => array(
                                    'title'     => 'Updated',
                                    'icon'      => 'fa-floppy-o',
                                    'color'     => '#00c05d',
                                    'actions'   => '<a href="./properties/edit/?id={id}">Edit</a>'
                                ),
                                'deleted'   => array(
                                    'title'     => 'Deleted',
                                    'icon'      => 'fa-times-circle',
                                    'color'     => '#e53935'
                                )
                            );

                            $i = 1;

                            // LOOP
                            foreach ($itema as $item) {
                                $actiona = array(
                                    $item['id']
                                );
                                $title = 'ID: ' .$item['id'];
                                if ($item['title']) {
                                    $title .= ' ' .stripslashes($item['title']);
                                } else if ($item['address1']) {
                                    $title .= ' ' .stripslashes($item['address1']);
                                }
                                ?>
                                <div class="item contain">
                                    <?php if ($statusa[$item['item_what']]['icon']) { ?>
                                        <div class="icon" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'background-color: ' .$statusa[$item['item_what']]['color']; } ?>"><i class="fa fa-fw <?php echo $statusa[$item['item_what']]['icon']; ?>"></i></div>
                                    <?php } ?>
                                    <div class="main">
                                        <?php if ($statusa[$item['item_what']]['title']) { ?>
                                            <div class="status" style="<?php if ($statusa[$item['item_what']]['color']) { echo 'color: ' .$statusa[$item['item_what']]['color']; } ?>">
                                                <?php echo $statusa[$item['item_what']]['title']; if ($item['pending']) { echo ' <span style="color: #ff8a80;">(pending)</span>'; } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="title">
                                            <?php if ($item['status'] != 9) { ?>
                                                <a href="./properties/edit/?id=<?php echo $item['id']; ?>">
                                            <?php } ?>
                                            <?php echo $title; ?>
                                            <?php if ($item['status'] != 9) { ?>
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <div class="time"><?php echo date('j M g:i A', strtotime($item['sort_by'])); ?></div>
                                        <?php if ($statusa[$item['item_what']]['actions']) { ?>
                                            <div class="actions">
                                                <i class="fa fa-caret-right"></i> <?php echo str_replace(array('{id}'), $actiona, $statusa[$item['item_what']]['actions']); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if ($i == 10) {
                                    break;
                                } else {
                                    ?>
                                    <div class="split"></div>
                                    <?php
                                    $i++;
                                }
                            }

                            unset($itema);

                        }

                        ?>

                    </div>

                </div>
            </td>

        </tr>
    </table>

</div>

<?php

function this_item_property($prop) {
    global $_uccms_properties;
    if ($prop['id']) {
        $out = '
        <div class="item">
            <table>
                <tr>
                    <td class="icon"><i class="fa fa-building-o"></i></td>
                    <td class="content">
                        <div class="title">
                            <a href="./properties/edit/?id=' .$prop['id']. '">ID: ' .$prop['id']. '</a>
                            ';
                            if ($prop['title']) {
                                $out .= ' ' .stripslashes($prop['title']);
                            } else if ($prop['address1']) {
                                $out .= ' ' .stripslashes($prop['address1']);
                            }
                            $out .= '
                        </div>
                        <table>
                            <tr>
                                <td class="status">
                                    ';
                                    if ($prop['status'] == 1) {
                                        $out .= '<span class="active">Active</span>';
                                    } else {
                                        $out .= '<span class="inactive">Inactive</span>';
                                    }
                                    $out .= '
                                </td>
                                <td class="type">
                                    ';
                                    if ($prop['listing_type'] == 2) {
                                        $out .= 'Sale';
                                    } else {
                                        $out .= 'Rental';
                                    }
                                    $out .= '
                                </td>
                                <td class="date">
                                    ' .date('j M', strtotime($prop['dt_updated'])). '
                                </td>
                                <td class="actions">
                                    <ul>
                                        <li><a href="./properties/edit/?id=' .$prop['id']. '">Edit</a></li>
                                        <li><a href="' .$_uccms_properties->propertyURL($prop['id'], $prop['city_id'], $prop['neighborhood_id'], $prop). '">View</a></li>
                                        <li><a href="./properties/delete/?id=' .$prop['id']. '&from=dashboard" onclick="return confirmPrompt(this.href, \'Are you sure you want to delete this this?\');">Delete</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        ';
    }
    return $out;
}

?>