<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_properties->tables['neighborhoods']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Neighborhood', 'Neighborhood deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Neighborhood', 'Failed to delete neighborhood.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Neighborhood', 'No neighborhood specified.');
}

BigTree::redirect(MODULE_ROOT.'locations/city/?id=' .$_GET['city_id']. '#neighborhoods');

?>