<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET INFO
    $city_query = "SELECT * FROM `" .$_uccms_properties->tables['cities']. "` WHERE (`id`=" .$id. ")";
    $city_q = sqlquery($city_query);
    $city = sqlfetch($city_q);

}

?>

<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

    #items header span, #items .item section {
        text-align: left;
    }

    #neighborhoods .neighborhood_title {
        width: 680px;
    }
    #neighborhoods .neighborhood_properties {
        width: 150px;
    }
    #neighborhoods .neighborhood_status {
        width: 100px;
    }
    #neighborhoods .neighborhood_edit {
        width: 55px;
    }
    #neighborhoods .neighborhood_delete {
        width: 55px;
    }

</style>

<div class="container legacy">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="city[id]" value="<?php echo $city['id']; ?>" />

    <header>
        <h2><?php if ($city['id']) { ?>Edit<?php } else { ?>Add<?php } ?> city</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Name</label>
                    <input type="text" name="city[title]" value="<?php echo stripslashes($city['title']); ?>" />
                </fieldset>

            </div>

            <div class="right">

            </div>

        </div>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="city[slug]" value="<?php echo stripslashes($city['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="city[meta_title]" value="<?php echo stripslashes($city['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="city[meta_description]" style="height: 64px;"><?php echo stripslashes($city['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="city[meta_keywords]" style="height: 64px;"><?php echo stripslashes($city['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>

<?php if ($city['id']) { ?>

    <div id="neighborhoods">

        <form id="form_neighborhoods">
        <input type="hidden" name="query" value="<?=$_GET['query']?>" />

        <? /*
        <div class="status contain">
            <div style="float: right;">
                <select name="status">
                    <option value="">All</option>
                    <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                    <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
                    <option value="submitted" <?php if ($_REQUEST['status'] == 'submitted') { ?>selected="selected"<?php } ?>>Submitted</option>
                </select>
            </div>
        </div>
        */ ?>

        <div class="search_paging contain">
            <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
            <span class="form_search_icon"></span>
            <nav id="view_paging" class="view_paging"></nav>
        </div>

        <div id="items" class="table">
            <summary>
                <h2>Neighborhoods</h2>
                <a class="add_resource add" href="../neighborhood/?city_id=<?php echo $city['id']; ?>"><span></span>Add Neighborhood</a>
            </summary>
            <header style="clear: both;">
                <span class="neighborhood_title">Name</span>
                <span class="neighborhood_properties">Properties</span>
                <span class="neighborhood_edit">Edit</span>
                <span class="neighborhood_delete">Delete</span>
            </header>
            <ul id="results" class="items">
                <? include(EXTENSION_ROOT. 'ajax/admin/locations/get-page-neighborhoods.php'); ?>
            </ul>
        </div>

        </form>

    </div>

    <script>
        BigTree.localSearchTimer = false;
        BigTree.localSearch = function() {
            $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_properties->Extension?>/ajax/admin/locations/get-page-neighborhoods/?page=1&city_id=<?php echo $city['id']; ?>&status=<?php echo $_REQUEST['status']; ?>&query=" +escape($("#query").val()));
        };
        $("#query").keyup(function() {
            if (BigTree.localSearchTimer) {
                clearTimeout(BigTree.localSearchTimer);
            }
            BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
        });
        $(".search_paging").on("click","#view_paging a",function() {
            if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
                return false;
            }
            $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_properties->Extension?>/ajax/admin/locations/get-page-neighborhoods/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&city_id=<?php echo $city['id']; ?>&status=<?php echo $_REQUEST['status']; ?>");
            return false;
        });
    </script>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>