<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET INFO
    $neighborhood_query = "SELECT * FROM `" .$_uccms_properties->tables['neighborhoods']. "` WHERE (`id`=" .$id. ")";
    $neighborhood_q = sqlquery($neighborhood_query);
    $neighborhood = sqlfetch($neighborhood_q);

}

if ($neighborhood['city_id']) {
    $city_id = $neighborhood['city_id'];
} else {
    $city_id = $_GET['city_id'];
}

?>

<style type="text/css">

    .file_wrapper .data {
        width: 211px;
    }

</style>

<div class="container legacy">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="neighborhood[id]" value="<?php echo $neighborhood['id']; ?>" />

    <header>
        <h2><?php if ($neighborhood['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Neighborhood</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Name</label>
                    <input type="text" name="neighborhood[title]" value="<?php echo stripslashes($neighborhood['title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>City</label>
                    <select name="neighborhood[city_id]">
                        <option value="0">Select</option>
                        <?php
                        $city_query = "SELECT * FROM `" .$_uccms_properties->tables['cities']. "` ORDER BY `title` ASC, `id` ASC";
                        $city_q = sqlquery($city_query);
                        while ($city = sqlfetch($city_q)) {
                            ?>
                            <option value="<?php echo $city['id']; ?>" <?php if ($city['id'] == $city_id) { ?>selected="selected"<?php } ?>><?php echo stripslashes($city['title']); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </fieldset>

            </div>

            <div class="right">

            </div>

        </div>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="neighborhood[slug]" value="<?php echo stripslashes($neighborhood['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="neighborhood[meta_title]" value="<?php echo stripslashes($neighborhood['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="neighborhood[meta_description]" style="height: 64px;"><?php echo stripslashes($neighborhood['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="neighborhood[meta_keywords]" style="height: 64px;"><?php echo stripslashes($neighborhood['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../city/?id=<?php echo $city_id; ?>#neighborhoods">&laquo; Back</a>
    </footer>

    </form>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>