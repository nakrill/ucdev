<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['neighborhood']['id'];

    // USE TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['neighborhood']['slug']) {
        $_POST['neighborhood']['slug'] = $_POST['neighborhood']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_properties->makeRewrite($_POST['neighborhood']['slug']),
        'city_id'           => (int)$_POST['neighborhood']['city_id'],
        'title'             => $_POST['neighborhood']['title'],
        'meta_title'        => $_POST['neighborhood']['meta_title'],
        'meta_description'  => $_POST['neighborhood']['meta_description'],
        'meta_keywords'     => $_POST['neighborhood']['meta_keywords']
    );

    // HAVE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_properties->tables['neighborhoods']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_properties->tables['neighborhoods']. "` SET " .uccms_createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Neighborhood', 'Neighborhood added!');
        } else {
            $admin->growl('Neighborhood', 'Neighborhood updated!');
        }

    // QUERY FAILED
    } else {
        $admin->growl('Neighborhood', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'/locations/neighborhood/?id=' .$id);

?>