<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_properties->tables['cities']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete City', 'City deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete City', 'Failed to delete city.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete City', 'No city specified.');
}

BigTree::redirect(MODULE_ROOT.'locations/');

?>