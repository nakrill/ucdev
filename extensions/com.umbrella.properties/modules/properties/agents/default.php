<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Agents', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #items header span, #items .item section {
        text-align: left;
    }

    #items .agent_name {
        width: 200px;
    }
    #items .agent_agency {
        width: 200px;
    }
    #items .agent_properties {
        width: 100px;
    }
    #items .agent_account {
        width: 230px;
    }
    #items .agent_status {
        width: 100px;
    }
    #items .agent_edit {
        width: 55px;
    }
    #items .agent_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#agents .status select').change(function() {
            $('#form_agents').submit();
        });

    });

</script>

<div id="agents">

    <form id="form_agents">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <? /*
    <div class="status contain">
        <div style="float: right;">
            <select name="status">
                <option value="">All</option>
                <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
                <option value="submitted" <?php if ($_REQUEST['status'] == 'submitted') { ?>selected="selected"<?php } ?>>Submitted</option>
            </select>
        </div>
    </div>
    */ ?>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>agents</h2>
            <a class="add_resource add" href="./edit/?agency_id=<?php echo $_REQUEST['agency_id']; ?>"><span></span>Add agent</a>
        </summary>
        <header style="clear: both;">
            <span class="agent_name">Name</span>
            <span class="agent_agency">Agency</span>
            <span class="agent_properties">Properties</span>
            <span class="agent_account">Account</span>
            <span class="agent_status">Status</span>
            <span class="agent_edit">Edit</span>
            <span class="agent_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/agents/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_properties->Extension?>/ajax/admin/agents/get-page/?page=1&status=<?php echo $_REQUEST['status']; ?>&query=" +escape($("#query").val())+ "&agency_id=<?=$_REQUEST['agency_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_properties->Extension?>/ajax/admin/agents/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=<?php echo $_REQUEST['status']; ?>&query=" +escape($("#query").val())+ "&agency_id=<?=$_REQUEST['agency_id']?>");
        return false;
    });
</script>