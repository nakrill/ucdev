<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE AGENT
    $query = "UPDATE `" .$_uccms_properties->tables['agents']. "` SET " .uccms_createSet($columns). ", `dt_deleted`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Agent', 'Agent deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Agent', 'Failed to delete agent.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Agent', 'No agent specified.');
}

if ($_REQUEST['from'] == 'dashboard') {
    BigTree::redirect(MODULE_ROOT);
} else {
    BigTree::redirect(MODULE_ROOT.'agents/');
}

?>