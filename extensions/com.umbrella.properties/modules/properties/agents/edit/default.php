<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET AGENT INFO
    $agent_query = "SELECT * FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`id`=" .$id. ")";
    $agent_q = sqlquery($agent_query);
    $agent = sqlfetch($agent_q);

// NO ID SPECIFIED
} else {

    // USE URL VARS FOR BUSINESS INFO
    $agent = $_REQUEST;

}

// ARRAY OF AGENCIES
$agenciesa = array();

// GET AGENCIES
$agencies_query = "
SELECT ay.*
FROM `" .$_uccms_properties->tables['agencies']. "` AS `ay`
WHERE (ay.status!=9)
ORDER BY ay.title ASC, ay.id ASC
";
$agencies_q = sqlquery($agencies_query);
while ($agency = sqlfetch($agencies_q)) {
    $agenciesa[$agency['id']] = $agency;
}

// GET FRONTEND ACCOUNTS
$account_query = "
SELECT CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, a.*, ad.*
FROM `uccms_accounts` AS `a`
INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
ORDER BY `fullname` ASC, `email` ASC, a.id ASC
";
$account_q = sqlquery($account_query);
while ($account = sqlfetch($account_q)) {
    $accta[$account['id']] = $account;
}

// ARRAYS
if (!is_array($accta)) $accta = array();

?>

<div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="agent[id]" value="<?php echo $agent['id']; ?>" />

    <div class="container legacy">

        <header>
            <h2><?php if ($agent['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Agent</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left">

                    <fieldset>
                        <label>Agent Name</label>
                        <input type="text" value="<?=$agent['name']?>" name="agent[name]" />
                    </fieldset>

                    <fieldset>
                        <label>Image</label>
                        <div>
                            <?php
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'image', // The value you should use for the "name" attribute of your form field
                                'value'     => ($agent['image'] ? $_uccms_properties->imageBridgeOut($agent['image'], 'agents') : ''), // The existing value for this form field
                                'id'        => 'agent_image', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label>Address</label>
                        <input type="text" value="<?=$agent['address1']?>" name="agent[address1]" />
                    </fieldset>

                    <fieldset>
                        <label>Address Cont.</label>
                        <input type="text" value="<?=$agent['address2']?>" name="agent[address2]" />
                    </fieldset>

                    <fieldset>
                        <label>City</label>
                        <input type="text" value="<?=$agent['city']?>" name="agent[city]" />
                    </fieldset>

                    <fieldset>
                        <label>State</label>
                        <select name="agent[state]">
                            <option value="">Select</option>
                            <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                <option value="<?=$state_code?>" <?php if ($state_code == $agent['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Zip</label>
                        <input type="text" value="<?=$agent['zip']?>" name="agent[zip]" style="display: inline; width: 50px;" />
                    </fieldset>

                </div>

                <div class="right">

                    <div style="margin-bottom: 15px; padding: 20px; background-color: #f5f5f5;">

                        <fieldset>
                            <label>Status</label>
                            <select name="agent[status]">
                                <option value="0" <?php if ($agent['status'] == 0) { ?>selected="selected"<?php } ?>>Inactive</option>
                                <option value="1" <?php if ($agent['status'] == 1) { ?>selected="selected"<?php } ?>>Active</option>
                            </select>
                        </fieldset>

                        <fieldset>
                            <label>Agency</label>
                            <select name="agent[agency_id]">
                                <option value="0">None</option>
                                <?php foreach ($agenciesa as $agency_id => $agency) { ?>
                                    <option value="<?php echo $agency_id; ?>" <?php if ($agency_id == $agent['agency_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($agency['title']); ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>

                        <fieldset>
                            <label>Account</label>
                            <select name="agent[account_id]">
                                <option value="0">None</option>
                                <?php foreach ($accta as $account_id => $account) { ?>
                                    <option value="<?php echo $account_id; ?>" <?php if ($account_id == $agent['account_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($account['fullname']). ' (' .stripslashes($account['email']). ')'; ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>

                    </div>

                    <fieldset>
                        <label>Phone</label>
                        <input type="text" value="<?=$agent['phone']?>" name="agent[phone]" style="display: inline-block; width: 100px;" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$agent['phone_ext']?>" name="agent[phone_ext]" style="display: inline-block; width: 30px;" />
                    </fieldset>

                    <fieldset>
                        <label>Mobile</label>
                        <input type="text" value="<?=$agent['phone_mobile']?>" name="agent[phone_mobile]" style="display: inline-block; width: 100px;" />
                    </fieldset>

                    <fieldset>
                        <label>Fax</label>
                        <input type="text" value="<?=$agent['fax']?>" name="agent[fax]" style="display: inline-block; width: 100px;" />
                    </fieldset>

                    <fieldset>
                        <label>Email</label>
                        <input type="text" value="<?=$agent['email']?>" name="agent[email]" />
                    </fieldset>

                    <fieldset>
                        <label>Lead Email</label>
                        <input type="text" value="<?=$agent['email_lead']?>" name="agent[email_lead]" />
                    </fieldset>

                    <fieldset>
                        <label>Website URL</label>
                        <input type="text" value="<?=$agent['url']?>" name="agent[url]" placeholder="www.domain.com" />
                    </fieldset>

                </div>

            </div>

            <div class="contain" style="margin-top: 10px;">

                <div class="left">

                    <div class="contain">

                        <div style="float: left; margin-right: 25px;">

                            <fieldset>
                                <label>Listing Price Override</label>
                                $<input type="text" name="agent[listing_price]" value="<?php if ($agent['listing_price']) { echo $_uccms_properties->toFloat($agent['listing_price']); } ?>" class="smaller_60" placeholder="<?php echo $_uccms_properties->toFloat($_uccms_properties->getSetting('listing_price')); ?>" style="display: inline-block;" />
                            </fieldset>

                        </div>

                        <div style="float: left;">
                            <fieldset>
                                <label>Per Override</label>
                                <select name="agent[listing_price_per]">
                                    <option value="">Select</option>
                                    <option value="w" <?php if ($agent['listing_price_per'] == 'w') { ?>selected="selected"<?php } ?>>Week</option>
                                    <option value="m" <?php if ($agent['listing_price_per'] == 'm') { ?>selected="selected"<?php } ?>>Month</option>
                                    <option value="q" <?php if ($agent['listing_price_per'] == 'q') { ?>selected="selected"<?php } ?>>Quarter</option>
                                    <option value="y" <?php if ($agent['listing_price_per'] == 'y') { ?>selected="selected"<?php } ?>>Year</option>
                                </select>
                            </fieldset>
                        </div>

                    </div>

                </div>

                <div class="right">

                </div>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
            <a class="button back" href="../">&laquo; Back</a>
        </footer>

    </div>

    </form>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>