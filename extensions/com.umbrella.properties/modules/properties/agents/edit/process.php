<?php

//echo print_r($_POST);
//exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['agent']['id'];

    // USE AGENT TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['agent']['slug']) {
        $_POST['agent']['slug'] = $_POST['agent']['name'];
    }

    // LISTING PRICE OVERRIDE
    if ($_POST['agent']['listing_price']) {
        $listing_price = $_uccms_properties->toFloat($_POST['agent']['listing_price']);
    } else {
        $listing_price = '';
    }

    // DB COLUMNS
    $columns = array(
        'agency_id'                 => (int)$_POST['agent']['agency_id'],
        'account_id'                => (int)$_POST['agent']['account_id'],
        'slug'                      => $_uccms_properties->makeRewrite($_POST['agent']['slug']),
        'status'                    => (int)$_POST['agent']['status'],
        'name'                      => $_POST['agent']['name'],
        'phone'                     => preg_replace('/\D/', '', $_POST['agent']['phone']),
        'phone_ext'                 => $_POST['agent']['phone_ext'],
        'phone_mobile'              => preg_replace('/\D/', '', $_POST['agent']['phone_mobile']),
        'fax'                       => preg_replace('/\D/', '', $_POST['agent']['fax']),
        'address1'                  => $_POST['agent']['address1'],
        'address2'                  => $_POST['agent']['address2'],
        'city'                      => $_POST['agent']['city'],
        'state'                     => $_POST['agent']['state'],
        'zip'                       => preg_replace('/\D/', '', $_POST['agent']['zip']),
        'url'                       => $_uccms_properties->trimURL($_POST['agent']['url']),
        'email'                     => $_POST['agent']['email'],
        'email_lead'                => $_POST['agent']['email_lead'],
        'listing_price'             => $listing_price,
        'listing_price_per'         => $_POST['agent']['listing_price_per']
    );

    // HAVE AGENT ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_properties->tables['agents']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";

    // NO ITEM ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_properties->tables['agents']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Agent', 'Agent added!');
        } else {
            $admin->growl('Agent', 'Agent updated!');
        }

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['image']['name']) || ((!$_POST['image']) || (substr($_POST['image'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `image` FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`id`=" .$id. ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['image']) {

                // REMOVE IMAGE
                @unlink($_uccms_properties->imageBridgeOut($ex['image'], 'agents', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_properties->tables['agents']. "` SET `image`='' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['image']['name']) || ($_POST['image'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'agent Image',
                'key'           => 'image',
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_properties->Extension. '/files/agents',
                    'image' => true,
                    'thumbs' => array(
                        array(
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES['image']['name']) {
                $field['file_input'] = $_FILES['image'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['image']) {
                $field['input'] = $_POST['image'];
            }

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // DIGITAL ASSET MANAGER VARS
            $field['dam_vars'] = array(
                'website_extension'         => $_uccms_properties->Extension,
                'website_extension_item'    => 'agent',
                'website_extension_item_id' => $id
            );

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_properties->tables['agents']. "` SET `image`='" .sqlescape($_uccms_properties->imageBridgeIn($file_path)). "' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                echo print_r($bigtree['errors']);
                exit;

                $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Agent', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT. 'agents/edit/?id=' .$id);

?>