<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Properties', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #items header span, #items .item section {
        text-align: left;
    }

    #items .property_street {
        width: 260px;
    }
    #items .property_city {
        width: 155px;
    }
    #items .property_price {
        width: 150px;
    }
    #items .property_agent {
        width: 165px;
    }
    #items .property_status {
        width: 100px;
    }
    #items .property_edit {
        width: 55px;
    }
    #items .property_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#properties .status select').change(function() {
            $('#form_properties').submit();
        });

    });

</script>

<div id="properties">

    <form id="form_properties">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <? /*
    <div class="status contain">
        <div style="float: right;">
            <select name="status">
                <option value="">All</option>
                <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
                <option value="submitted" <?php if ($_REQUEST['status'] == 'submitted') { ?>selected="selected"<?php } ?>>Submitted</option>
            </select>
        </div>
    </div>
    */ ?>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Properties</h2>
            <a class="add_resource add" href="./edit/"><span></span>Add Property</a>
        </summary>
        <header style="clear: both;">
            <span class="property_street">Name / Street</span>
            <span class="property_city">City</span>
            <span class="property_price">Price</span>
            <span class="property_agent">Agent</span>
            <span class="property_status">Status</span>
            <span class="property_edit">Edit</span>
            <span class="property_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/properties/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_propertydir->Extension?>/ajax/admin/properties/get-page/?page=1&status=<?php echo $_REQUEST['status']; ?>&type=<?php echo $_REQUEST['type']; ?>&query=" +escape($("#query").val())+ "&agency_id=<?=$_REQUEST['agency_id']?>&agent_id=<?=$_REQUEST['agent_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_propertydir->Extension?>/ajax/admin/properties/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=<?php echo $_REQUEST['status']; ?>&type=<?php echo $_REQUEST['type']; ?>&query=" +escape($("#query").val())+ "&agency_id=<?=$_REQUEST['agency_id']?>&agent_id=<?=$_REQUEST['agent_id']?>");
        return false;
    });
</script>