<?php

/*
echo print_r($_REQUEST);
exit;
*/

// CLEAN UP
$id = (int)$_REQUEST['property_id'];

// FORM SUBMITTED
if (is_array($_REQUEST)) {

    // HAVE PROPERTY ID
    if ($id) {

        // HAVE GROUP
        if ($_REQUEST['group_id']) {

            // REMOVE FROM DB
            $query = "DELETE FROM `" .$_uccms_properties->tables['property_attribute_groups']. "` WHERE (`property_id`=" .$id. ") AND (`group_id`=" .(int)$_REQUEST['group_id']. ")";

            // RUN QUERY
            if (sqlquery($query)) {
                $admin->growl('Attributes', 'Attribute group removed!');
            } else {
                $admin->growl('Attributes', 'Failed to remove attribute group.');
            }

        }

    // property ID NOT SPECIFIED
    } else {
        $admin->growl('Property', 'Property ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id. '#attributes');

?>