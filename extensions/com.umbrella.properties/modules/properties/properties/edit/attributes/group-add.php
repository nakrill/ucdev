<?php

/*
echo print_r($_POST);
exit;
*/

// CLEAN UP
$id = (int)$_POST['property']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE PROPERTY ID
    if ($id) {

        // HAVE GROUP
        if ($_POST['group']['id']) {

            $columns = array(
                'property_id'   => $id,
                'group_id'      => (int)$_POST['group']['id']
            );

            // INSERT INTO DB
            $query = "INSERT INTO `" .$_uccms_properties->tables['property_attribute_groups']. "` SET " .uccms_createSet($columns);

            // RUN QUERY
            if (sqlquery($query)) {
                $admin->growl('Attributes', 'Attribute group added!');
            } else {
                $admin->growl('Attributes', 'Failed to add attribute group.');
            }

        }

    // property ID NOT SPECIFIED
    } else {
        $admin->growl('Property', 'Property ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id. '#attributes');

?>