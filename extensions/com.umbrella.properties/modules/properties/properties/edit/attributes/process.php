<?php

//echo print_r($_POST);
//exit;

// CLEAN UP
$id = (int)$_POST['property']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE PROPERTY ID
    if ($id) {

        $data = $_POST;

        // GET PROPERTY INFO
        $property_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`id`=" .$id. ")";
        $property_q = sqlquery($property_query);
        $property = sqlfetch($property_q);

        /*
        // GET ATTRIBUTE GROUPS FOR PROPERTY
        $groups_query = "
        SELECT *
        FROM `" .$_uccms_properties->tables['property_attribute_groups']. "` AS `pag`
        INNER JOIN `" .$_uccms_properties->tables['attribute_groups']. "` AS `ag` ON pag.group_id=ag.id
        WHERE (pag.property_id=" .$id. ")
        ORDER BY ag.sort ASC, ag.title ASC";
        */

        // GET ATTRIBUTE GROUPS FOR PROPERTY TYPE
        $groups_query = "
        SELECT ag.*
        FROM `" .$_uccms_properties->tables['attribute_groups']. "` AS `ag`
        INNER JOIN `" .$_uccms_properties->tables['attribute_groups_property_types']. "` AS `agpt` ON ag.id=agpt.group_id
        WHERE (ag.active=1) AND (agpt.property_type_code='" .$property['property_type_code']. "')
        ORDER BY `sort` ASC, `title` ASC
        ";
        $groups_q = sqlquery($groups_query);
        while ($group = sqlfetch($groups_q)) {

            // GET ATTRIBUTES FOR GROUP
            $attr_query = "SELECT * FROM `" .$_uccms_properties->tables['attributes']. "` WHERE (`group_id`=" .$group['id']. ")";
            $attr_q = sqlquery($attr_query);
            while ($attr = sqlfetch($attr_q)) {

                // IS FILE
                if ($attr['type'] == 'file') {

                    $remove_file = '';

                    // UPLOAD CLASS
                    require_once(SERVER_ROOT. 'uccms/includes/classes/upload.php');

                    // INIT UPLOAD CLASS
                    $_upload = new upload();

                    // FILE NAME
                    $file_name = $_upload->getName('attribute-' .$attr['id']);

                    // REMOVING FILE
                    if ($data['attribute-' .$attr['id']. '-remove']) {
                        $remove_file = str_replace(array('..', '/', '\/'), '', $data['attribute-' .$attr['id']. '-existing']);
                    }

                    // DEFAULT - FILE NOT MISSING
                    $file_missing = false;

                    // NEW FILE UPLOADED
                    if ($file_name) {
                        if (($data['attribute-' .$attr['id']. '-existing']) && (($file_name != $data['attribute-' .$attr['id']. '-existing']))) {
                            $remove_file = str_replace(array('..', '/', '\/'), '', $data['attribute-' .$attr['id']. '-existing']);
                        }

                    // NO FILE UPLOADED
                    } else {
                        $file_missing = true;
                        if ($attr['required']) {
                            $missa[$attr['id']] = 'Missing field: ' .stripslashes($attr['title']);
                        }
                    }

                    // REMOVING FILE
                    if ($remove_file) {
                        @unlink(SITE_ROOT. 'extensions/' .$_uccms_properties->Extension. '/files/properties/attributes/' .$id. '-' .$attr['id']. '_' .$remove_file);
                    }

                    // FILE IS NOT MISSING
                    if (!$file_missing) {

                        // HAVE REQUIRED EXTENSIONS
                        if ($attr['file_types']) {

                            // ALLOWED FILE TYPES ARRAY
                            $fta = explode(',', stripslashes($attr['file_types']));

                            // FILE EXTENSION
                            $ext = $_upload->getExt('attribute-' .$attr['id']);

                            // FILE EXTENSION NOT ALLOWED
                            if (!in_array($ext, $fta)) {
                                $file_missing = true;
                                $missa[$attr['id']] = 'File type not allowed for: ' .stripslashes($attr['title']);

                            }

                        }

                        // FILE STILL NOT MISSING
                        if (!$file_missing) {

                            // WHERE TO PUT FILE
                            $file_loc = SITE_ROOT. 'extensions/' .$_uccms_properties->Extension. '/files/properties/attributes/' .$id. '-' .$attr['id']. '_' .$file_name;

                            // UPLOAD
                            $result = $_upload->doUpload('attribute-' .$attr['id'], $file_loc);

                            // UPLOAD WENT OK
                            if ($result == 'ok') {
                                $data['attribute'][$attr['id']][] = $file_name;

                            // UPLOAD FAILED
                            } else {
                                $missa[$attr['id']] = 'Failed to upload file: ' .stripslashes($attr['title']). ' (' .$result. ')';
                            }

                        }

                    }

                // EVERY OTHER ATTRIBUTE
                } else {

                    // IS LIMITED OR REQUIRED
                    if (($attr['limit']) || ($attr['required'])) {

                        // IS LIMITED
                        if ($attr['limit']) {

                            // OVER LIMIT
                            if (count($data['attribute'][$attr['id']]) > $attr['limit']) {
                                $overa[$attr['id']] = 'Over limit: ' .stripslashes($attr['title']);
                            }

                        }

                        // IS REQUIRED
                        if ($attr['required']) {

                            // FIELD VALUE
                            if (is_array($data['attribute'][$attr['id']][0])) { // IS ARRAY
                                $val = $data['attribute'][$attr['id']];
                            } else {
                                $val = trim($data['attribute'][$attr['id']][0]); // IS STRING
                            }

                            // HAS VALUE
                            if ($val) {

                                /*
                                // IS INCORRECT
                                if (!$this->checkRequired($val, $attr['required'])) {
                                    $missa[$attr['id']] = 'Incorrect field: ' .stripslashes($attr['title']);
                                }
                                */

                            // MISSING VALUE
                            } else {
                                $missa[$attr['id']] = 'Missing field: ' .stripslashes($attr['title']);
                            }

                        }

                    }

                }

            }

        }

        // ENCODE ATTRIBUTES
        if (is_array($data['attribute'])) {
            $attributes = json_encode($data['attribute']);
        } else {
            $attributes = '';
        }

        $columns = array(
            'attributes'    => $attributes
        );

        // INSERT INTO DB
        $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";

        // RUN QUERY
        if (sqlquery($query)) {
            $admin->growl('Attributes', 'Attributes updated!');
        } else {
            $admin->growl('Attributes', 'Failed to update attributes.');
        }

    // property ID NOT SPECIFIED
    } else {
        $admin->growl('Property', 'Property ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id. '#attributes');

?>