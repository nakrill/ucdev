<div class="options items checkboxes">

    <?php

    // HAVE OPTIONS
    if (count($options) > 0) {

        // LOOP THROUGH OPTIONS
        foreach ($options as $option) {

            // DEFAULT
            if (count($fva) == 0) {
                if ($option['default']) {
                    $fva[] = stripslashes($option['title']);
                }
            }

            ?>
            <div class="option item contain"><input type="checkbox" name="attribute[<?php echo $attribute['id']; ?>][]" value="<?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?>" <?php if (in_array(stripslashes($option['title']), $fva)) { ?>checked="checked"<?php } ?> /><label class="for-checkbox"><?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?><?php if ($option['markup']) { ?> (+$<?php echo number_format(stripslashes($option['markup']), 2); ?>)<?php } ?></label></div>
            <?php
        }

    }

    ?>

</div>