<div class="options">

    <select name="attribute[<?php echo $attribute['id']; ?>][]">
        <option value="">Select</option>
        <?php

        // LOOP
        foreach ($options as $option) {

            // DEFAULT
            if (count($fva) == 0) {
                if ($option['default']) {
                    $fva[] = stripslashes($option['title']);
                }
            }

            ?>
            <option value="<?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?>" <?php if (in_array(stripslashes($option['title']), $fva)) { ?>selected="selected"<?php } ?>><?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?><?php if ($option['markup']) { ?> (+$<?php echo number_format(stripslashes($option['markup']), 2); ?>)<?php } ?></option>
            <?php
        }
        ?>
    </select>

</div>