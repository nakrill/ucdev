<div class="options select_box clearfix">

    <?php

    // HAVE OPTIONS
    if (count($options) > 0) {

        // LOOP THROUGH OPTIONS
        foreach ($options as $option) {

            // DEFAULT
            if (count($fva) == 0) {
                if ($option['default']) {
                    $fva[] = stripslashes($option['title']);
                }
            }

            ?>
            <div class="option <?php if (in_array(stripslashes($option['title']), $fva)) { echo 'selected'; } ?>" data-value="<?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?>">
                <div class="border2">
                    <?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?>
                    <?php if ($option['markup']) { ?>
                        <div class="markup">+$<?php echo number_format(stripslashes($option['markup']), 2); ?></div>
                    <?php } ?>
                </div>
            </div>
            <?php
        }

        ?>

        <input type="hidden" name="attribute[<?php echo $attribute['id']; ?>][]" value="<?php echo stripslashes($fva[0]); ?>" />

        <?php

    }

    ?>

</div>