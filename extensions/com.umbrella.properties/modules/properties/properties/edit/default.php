<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET PROPERTY INFO
    $property_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`id`=" .$id. ")";
    $property_q = sqlquery($property_query);
    $property = sqlfetch($property_q);

    $coolinga   = explode(',', $property['cooling_type_codes']);
    $heatinga   = explode(',', $property['heating_type_codes']);
    $parkinga   = explode(',', $property['parking_type_codes']);
    $petsa      = explode(',', $property['pet_type_codes']);

// NO ID SPECIFIED
} else {

    // USE URL VARS FOR PROPERTY INFO
    $property = $_REQUEST;

    // HAVE PROPERTY TITLE
    if ($property['title']) {
        $property['slug'] = $_uccms_properties->makeRewrite($property['title']); // CREATE SLUG
    }

}

if (!is_array($coolinga)) $coolinga = array();
if (!is_array($heatinga)) $heatinga = array();
if (!is_array($parkinga)) $parkinga = array();
if (!is_array($petsa)) $petsa = array();

// ARRAY OF AGENCIES
$agenciesa = array();

// GET AGENCIES
$agencies_query = "
SELECT ay.*
FROM `" .$_uccms_properties->tables['agencies']. "` AS `ay`
WHERE (ay.status!=9)
ORDER BY ay.title ASC, ay.id ASC
";
$agencies_q = sqlquery($agencies_query);
while ($agency = sqlfetch($agencies_q)) {
    $agenciesa[$agency['id']] = $agency;
}

// ARRAY OF AGENTS
$agentsa = array();

// GET AGENTS
$agents_query = "
SELECT at.*
FROM `" .$_uccms_properties->tables['agents']. "` AS `at`
WHERE (at.status!=9)
ORDER BY at.name ASC, at.id ASC
";
$agents_q = sqlquery($agents_query);
while ($agent = sqlfetch($agents_q)) {
    $agentsa[$agent['agency_id']][$agent['id']] = $agent;
}

// ARRAY OF CITIES
$citiesa = array();

// GET CITIES
$cities_query = "
SELECT c.*
FROM `" .$_uccms_properties->tables['cities']. "` AS `c`
ORDER BY c.title ASC, c.id ASC
";
$cities_q = sqlquery($cities_query);
while ($agency = sqlfetch($cities_q)) {
    $citiesa[$agency['id']] = $agency;
}

// ARRAY OF NEIGHBORHOODS
$neighborhoodsa = array();

// GET NEIGHBORHOODS
$neighborhoods_query = "
SELECT n.*
FROM `" .$_uccms_properties->tables['neighborhoods']. "` AS `n`
ORDER BY n.title ASC, n.id ASC
";
$neighborhoods_q = sqlquery($neighborhoods_query);
while ($neighborhood = sqlfetch($neighborhoods_q)) {
    $neighborhoodsa[$neighborhood['city_id']][$neighborhood['id']] = $neighborhood;
}

// GET PAYMENT METHODS
$payment_methods = $_uccms_properties->getPaymentMethods(true);

$payment_profiles = array();

$aid = 0;

// HAS AGENT
if ($property['agent_id']) {

    // GET AGENT ACCOUNT ID
    $query = "SELECT `account_id` FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
    $q = sqlquery($query);
    $agent = sqlfetch($q);

    if ($agent['account_id']) {
        $aid = $agent['account_id'];
    }

}

// STILL NO ACCOUNT ID, BUT IS AGENCY
if ((!$aid) && ($property['agency_id'])) {

    // GET AGENCY ACCOUNT ID
    $query = "SELECT `account_id` FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
    $q = sqlquery($query);
    $agent = sqlfetch($q);

    if ($agent['account_id']) {
        $aid = $agent['account_id'];
    }

}

// HAVE ACCOUNT ID
if ($aid) {

    // PAYMENT PROFILES ENABLED
    if ($_uccms['_account']->paymentProfilesEnabled()) {

        // GET PAYMENT PROFILES
        $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$aid. ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
        $pp_q = sqlquery($pp_query);
        while ($pp = sqlfetch($pp_q)) {
            $payment_profiles[$pp['id']] = $pp;
        }

    }

}

// GET PRICING INFO
$pricing = $_uccms_properties->listingPricing($property['id']);

?>

<style type="text/css">

    #property_container .top_stats {
        margin-bottom: 15px;
    }

    #property_container .top_stats .stat {
        float: left;
        width: 25%;
        text-align: center;
    }
    #property_container .top_stats .stat .padding {
        display: inline-block;
        min-width: 150px;
        margin: 10px;
        padding: 20px 20px 15px;
        background-color: #eee;
        text-align: center;
        border-radius: 5px;
    }
    #property_container .top_stats .stat .num {
        display: block;
        font-size: 2em;
    }
    #property_container .top_stats .stat .title {
        display: block;
        margin-top: 5px;
        font-size: 1.2em;
        font-weight: bold;
    }
    #property_container .top_stats .stat .period {
        display: block;
        margin-top: 2px;
        opacity: .8;
    }

    #property_container .lt-sale, #property_container .lt-rental {
        display: none;
    }

    #property_container .items.checkboxes .item {
        margin-bottom: 3px;
        line-height: 17px;
    }
    #property_container .items.checkboxes .item:last-child {
        margin-bottom: 0px;
    }

    #property_container .time_picker {
        margin-left: 6px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // LISTING TYPE
        $('#listing_type').change(function(e) {
            if ($(this).val() == 1) {
                $('#property_container .lt-rental').hide();
                $('#property_container .lt-sale').show();
            } else if ($(this).val() == 2) {
                $('#property_container .lt-sale').hide();
                $('#property_container .lt-rental').show();
            }
        });

        $('#listing_type').change();

        // AGENCY CHANGE
        $('#property_container select[name="property[agency_id]"]').change(function() {
            $('#agents .agency').hide();
            $('#agents .agency-' +$(this).val()).show();
        });

        // CITY CHANGE
        $('#property_container select[name="property[city_id]"]').change(function() {
            $('#neighborhoods .city').hide();
            $('#neighborhoods .city-' +$(this).val()).show();
        });

    });

</script>

<div id="property_container" class="container legacy" style="margin-bottom: 0px; border: 0px none;">

    <?php

    // PROPERTY EXISTS
    if ($property['id']) {

        // GET TOTAL STATS
        $stats_total = $_uccms_properties->property_getStats($property['id']);

        // GET TIMEFRAME STATS
        $stats_timeframe = $_uccms_properties->property_getStats($property['id'], array(
            'date_from' => date('Y-m-d H:i:s', strtotime('-7 Days')),
            'date_to'   => date('Y-m-d H:i:s')
        ));

        ?>

        <div id="top_stats" class="top_stats contain">
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_timeframe['view'], 0); ?></span>
                    <span class="title">View<?php if ($stats_timeframe['view'] != 1) { ?>s<?php } ?></span>
                    <span class="period">7 Days</span>
                </div>
            </div>
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_total['view'], 0); ?></span>
                    <span class="title">View<?php if ($stats_total['view'] != 1) { ?>s<?php } ?></span>
                    <span class="period">Total</span>
                </div>
            </div>
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_timeframe['email'], 0); ?></span>
                    <span class="title">Email<?php if ($stats_timeframe['email'] != 1) { ?>s<?php } ?></span>
                    <span class="period">7 Days</span>
                </div>
            </div>
            <div class="stat">
                <div class="padding">
                    <span class="num"><?php echo number_format((int)$stats_total['email'], 0); ?></span>
                    <span class="title">Email<?php if ($stats_total['email'] != 1) { ?>s<?php } ?></span>
                    <span class="period">Total</span>
                </div>
            </div>
        </div>

        <div class="contain" style="margin-bottom: 10px;">
            <h3 class="uccms_toggle" data-what="property_misc"><span>MISC</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-property_misc" style="display: none;">

                <table border="0" cellpadding="0" cellspacing="0" style="width: auto;">
                    <tr>
                        <td><strong>ID:</strong>&nbsp;</td>
                        <td><strong><?php echo $property['id']; ?></strong></td>
                    </tr>
                    <tr>
                        <td>Status:&nbsp;</td>
                        <td><?php echo $_uccms_properties->statuses[$property['status']]; ?></td>
                    </tr>
                    <?php if ($property['dt_submitted'] != '0000-00-00 00:00:00') { ?>
                        <tr>
                            <td>Submitted:&nbsp;</td>
                            <td><?php echo date('n/j/Y g:i A', strtotime($property['dt_submitted'])); ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($property['dt_created'] != '0000-00-00 00:00:00') { ?>
                        <tr>
                            <td>Created:&nbsp;</td>
                            <td><?php echo date('n/j/Y g:i A', strtotime($property['dt_created'])); ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($property['dt_updated'] != '0000-00-00 00:00:00') { ?>
                        <tr>
                            <td>Updated:&nbsp;</td>
                            <td><?php echo date('n/j/Y g:i A', strtotime($property['dt_updated'])); ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($property['payment_expires'] != '0000-00-00') { ?>
                        <tr>
                            <td>Payment Expires:&nbsp;</td>
                            <td><?php echo date('n/j/Y', strtotime($property['payment_expires'])); ?> (<a href="#billing">manage</a>)</td>
                        </tr>
                    <?php } ?>
                </table>

            </div>
        </div>

        <?php

    }

    ?>

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="property[id]" value="<?php echo $property['id']; ?>" />

    <div class="container legacy">
        <section>

            <fieldset>
                <label>Listing Type</label>
                <select id="listing_type" name="property[listing_type]">
                    <?php foreach ($_uccms_properties->listingTypes as $type_id => $type_title) { ?>
                        <option value="<?php echo $type_id; ?>" <?php if ($type_id == $property['listing_type']) { ?>selected="selected"<?php } ?>><?php echo $type_title; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

        </section>
    </div>

    <div class="container legacy">

        <header>
            <h2><?php if ($property['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Property</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left">

                    <fieldset>
                        <label>Property Name</label>
                        <input type="text" value="<?=$property['title']?>" name="property[title]" />
                    </fieldset>

                    <fieldset>
                        <label>Property ID</label>
                        <input type="text" value="<?=$property['property_id']?>" name="property[property_id]" />
                    </fieldset>

                    <div style="margin-bottom: 15px; padding: 20px; background-color: #f5f5f5;">

                        <fieldset>
                            <label>Status <span class="required">*</span></label>
                            <select name="property[status]">
                                <?php foreach ($_uccms_properties->statuses as $status_id => $status_title) { ?>
                                    <?php if ($status_id != 9) { ?>
                                        <option value="<?php echo $status_id; ?>" <?php if ($property['status'] == $status_id) { ?>selected="selected"<?php } ?>><?php echo $status_title; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </fieldset>

                        <fieldset>
                            <label>Agency</label>
                            <select name="property[agency_id]">
                                <option value="0">None</option>
                                <?php foreach ($agenciesa as $agency) { ?>
                                    <option value="<?php echo $agency['id']; ?>" <?php if ($agency['id'] == $property['agency_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($agency['title']); ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>

                        <fieldset id="agents">
                            <label>Agent</label>
                            <?php foreach ($agentsa as $agency_id => $agents) { ?>
                                <div class="agency agency-<?php echo $agency_id; ?>" <?php if ($agency_id != $property['agency_id']) { ?>style="display: none;"<?php } ?>>
                                    <select name="property[agent][<?php echo $agency_id; ?>][agent_id]">
                                        <option value="0">None</option>
                                        <?php foreach ($agents as $agent) { ?>
                                            <option value="<?php echo $agent['id']; ?>" <?php if ($agent['id'] == $property['agent_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($agent['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                        </fieldset>

                    </div>

                    <fieldset>
                        <label>Listing Slug</label>
                        /<input type="text" name="property[slug]" value="<?=$property['slug']?>" placeholder="listing-name" style="display: inline-block; width: 400px;" />/
                        <label style="margin-bottom: 0px;"><small>The URL this listing is available at on this site.</small></label>
                    </fieldset>

                </div>

                <div class="right">

                    <fieldset>
                        <label>Property Type</label>
                        <select name="property[property_type_code]">
                            <?php foreach ($_uccms_properties->propertyTypes() as $type_code => $type_title) { ?>
                                <option value="<?php echo $type_code; ?>" <?php if ($type_code == $property['property_type_code']) { ?>selected="selected"<?php } ?>><?php echo $type_title; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Rental Space</label>
                        <select name="property[rental_space_code]">
                            <?php foreach ($_uccms_properties->propertyRentalSpaces() as $rs_code => $rs_title) { ?>
                                <option value="<?php echo $rs_code; ?>" <?php if ($rs_code == $property['rental_space_code']) { ?>selected="selected"<?php } ?>><?php echo $rs_title; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Price <span class="lt-rental">From</span></label>
                        $<input type="text" name="property[price_from]" value="<?=number_format($property['price_from'], 2)?>" style="display: inline; width: 80px;" />
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Price To</label>
                        $<input type="text" name="property[price_to]" value="<?=number_format($property['price_to'], 2)?>" style="display: inline; width: 80px;" />
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Price Per</label>
                        <select name="property[price_term_code]">
                            <option value="">N/A</option>
                            <?php foreach ($_uccms_properties->propertyPriceTerms() as $pt_code => $pt_title) { ?>
                                <option value="<?php echo $pt_code; ?>" <?php if ($pt_code == $property['price_term_code']) { ?>selected="selected"<?php } ?>><?php echo $pt_title; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Check In Office</label>
                        <input type="text" name="property[checkin_office]" value="<?=$property['checkin_office']?>" />
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Check In</label>
                        <select name="property[checkin_day]">
                            <option value="0">None</option>
                            <?php foreach ($_uccms_properties->daysOfWeek_Schema() as $day_id => $day_title) { ?>
                                <option value="<?php echo $day_id; ?>" <?php if ($day_id == $property['checkin_day']) { ?>selected="selected"<?php } ?>><?php echo $day_title; ?></option>
                            <?php } ?>
                        </select>
                        <div class="contain" style="margin-left: 6px;">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'checkin_time',
                                'key'       => 'property[checkin_time]',
                                'value'     => ($property['checkin_time'] ? $property['checkin_time'] : ''),
                                'required'  => false,
                                'options'   => array(
                                    'default_now'   => false
                                )
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/time.php'));

                            ?>
                        </div>
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Check Out</label>
                        <select name="property[checkout_day]">
                            <option value="0">None</option>
                            <?php foreach ($_uccms_properties->daysOfWeek_Schema() as $day_id => $day_title) { ?>
                                <option value="<?php echo $day_id; ?>" <?php if ($day_id == $property['checkout_day']) { ?>selected="selected"<?php } ?>><?php echo $day_title; ?></option>
                            <?php } ?>
                        </select>
                        <div class="contain" style="margin-left: 6px;">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'checkout_time',
                                'key'       => 'property[checkout_time]',
                                'value'     => ($property['checkout_time'] ? $property['checkout_time'] : ''),
                                'required'  => false,
                                'options'   => array(
                                    'default_now'   => false
                                )
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/time.php'));

                            ?>
                        </div>
                    </fieldset>

                </div>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
            <?php if ($property['pending']) { ?><input class="blue" type="submit" name="approve" value="Save & Approve" /><?php } ?>
            <a class="button back" href="../">&laquo; Back</a>
        </footer>

    </div>

    <div class="container legacy">

        <header>
            <h2>Location</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left">

                    <fieldset>
                        <label>City</label>
                        <select name="property[city_id]">
                            <option value="0">None</option>
                            <?php foreach ($citiesa as $city) { ?>
                                <option value="<?php echo $city['id']; ?>" <?php if ($city['id'] == $property['city_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($city['title']); ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset id="neighborhoods">
                        <label>Neighborhood</label>
                        <?php foreach ($neighborhoodsa as $city_id => $neighborhoods) { ?>
                            <div class="city city-<?php echo $city_id; ?>" <?php if ($city_id != $property['city_id']) { ?>style="display: none;"<?php } ?>>
                                <select name="property[neighborhood][<?php echo $city_id; ?>][neighborhood_id]">
                                    <option value="0">None</option>
                                    <?php foreach ($neighborhoods as $neighborhood) { ?>
                                        <option value="<?php echo $neighborhood['id']; ?>" <?php if ($neighborhood['id'] == $property['neighborhood_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($neighborhood['title']); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                    </fieldset>

                    <fieldset>
                        <label>Address</label>
                        <input type="text" name="property[address1]" value="<?=$property['address1']?>" />
                    </fieldset>

                    <fieldset>
                        <label>Address Cont.</label>
                        <input type="text" name="property[address2]" value="<?=$property['address2']?>" />
                    </fieldset>

                    <fieldset>
                        <label>Unit</label>
                        <input type="text" name="property[unit]" value="<?=$property['unit']?>" />
                    </fieldset>

                    <fieldset>
                        <label>City</label>
                        <input type="text" name="property[city]" value="<?=$property['city']?>" />
                    </fieldset>

                    <fieldset>
                        <label>State</label>
                        <select name="property[state]">
                            <option value="">Select</option>
                            <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                <option value="<?=$state_code?>" <?php if ($state_code == $property['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Zip</label>
                        <input type="text" value="<?=$property['zip']?>" name="property[zip]" style="display: inline; width: 50px;" /><span class="required">*</span> - <input type="text" value="<?=$property['zip4']?>" name="property[zip4]" style="display: inline; width: 40px;" />
                    </fieldset>

                </div>

                <div class="right">

                    Map?

                </div>

            </div>

            <div class="contain">

                <fieldset>
                    <label>Property Location</label>
                    <select name="property[location_code]">
                        <option value="">N/A</option>
                        <?php foreach ($_uccms_properties->propertyLocations() as $loc_code => $loc) { ?>
                            <option value="<?php echo $loc_code; ?>" <?php if ($loc_code == $property['location_code']) { ?>selected="selected"<?php } ?>><?php echo $loc['title']; ?> (<?php echo $loc['description']; ?>)</option>
                        <?php } ?>
                    </select>
                </fieldset>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>

    </div>

    <div class="container legacy">

        <header>
            <h2>Description</h2>
        </header>

        <section>

            <div class="contain">

                <fieldset>
                    <?php
                    $field = array(
                        'key'       => 'property[description]', // The value you should use for the "name" attribute of your form field
                        'value'     => stripslashes($property['description']), // The existing value for this form field
                        'id'        => 'property_description', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'simple' => true
                        )
                    );
                    include(BigTree::path('admin/form-field-types/draw/html.php'));
                    ?>
                </fieldset>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>

    </div>

    <div class="container legacy">

        <header>
            <h2>Details</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left">

                    <fieldset>
                        <label>SqFt</label>
                        <input type="text" name="property[sqft]" value="<?=number_format($property['sqft'], 0)?>" style="width: 80px;" />
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Accommodates</label>
                        <input type="text" name="property[accommodates_num]" value="<?=$property['accommodates_num']?>" style="display: inline-block; width: 40px;" /> people
                    </fieldset>

                    <fieldset>
                        <label># Bedrooms</label>
                        <select name="property[bedrooms_num]">
                            <?php for ($i=0; $i<=50; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $property['bedrooms_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label># Master Bedrooms</label>
                        <select name="property[bedrooms_num_master]">
                            <?php for ($i=0; $i<=50; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $property['bedrooms_num_master']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label># Bathrooms</label>
                        <select name="property[bathrooms_num]">
                            <?php
                            $i = 0;
                            while ($i <= 50) {
                                ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $property['bathrooms_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                                <?php
                                $i += .5;
                            }
                            ?>
                        </select>
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Bathroom Access</label>
                        <select name="property[rental_bathrooms_code]">
                            <?php foreach ($_uccms_properties->propertyRentalBathrooms() as $rb_code => $rb_title) { ?>
                                <option value="<?php echo $rb_code; ?>" <?php if ($rb_code == $property['rental_bathrooms_code']) { ?>selected="selected"<?php } ?>><?php echo $rb_title; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label># Parking Spots</label>
                        <select name="property[parking_num]">
                            <?php for ($i=0; $i<=50; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $property['parking_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Parking Type</label>
                        <div class="items checkboxes">
                            <?php foreach ($_uccms_properties->propertyParkingTypes() as $pt_code => $pt_title) { ?>
                                <div class="item contain">
                                    <input type="checkbox" name="property[parking][]" value="<?=$pt_code?>" <?php if (in_array($pt_code, $parkinga)) { ?>checked="checked"<?php } ?> /> <?php echo $pt_title; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label>Gated Access</label>
                        <select name="property[gated_code]">
                            <option value="">No</option>
                            <?php foreach ($_uccms_properties->propertyGatedTypes() as $gt_code => $gt_title) { ?>
                                <option value="<?php echo $gt_code; ?>" <?php if ($gt_code == $property['gated_code']) { ?>selected="selected"<?php } ?>><?php echo $gt_title; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                </div>

                <div class="right">

                    <fieldset>
                        <label>Lot Acres</label>
                        <input type="text" name="property[lot_acres]" value="<?=number_format($property['lot_acres'], 2)?>" style="width: 80px;" />
                    </fieldset>

                    <fieldset>
                        <label>Year Built</label>
                        <input type="text" name="property[year_built]" value="<?=$property['year_built']?>" style="width: 40px;" />
                    </fieldset>

                    <fieldset>
                        <label>Cooling</label>
                        <div class="items checkboxes">
                            <?php foreach ($_uccms_properties->propertyCoolingTypes() as $ct_code => $ct_title) { ?>
                                <div class="item contain">
                                    <input type="checkbox" name="property[cooling][]" value="<?=$ct_code?>" <?php if (in_array($ct_code, $coolinga)) { ?>checked="checked"<?php } ?> /> <?php echo $ct_title; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label>Heating</label>
                        <div class="items checkboxes">
                            <?php foreach ($_uccms_properties->propertyHeatingTypes() as $ht_code => $ht_title) { ?>
                                <div class="item contain">
                                    <input type="checkbox" name="property[heating][]" value="<?=$ht_code?>" <?php if (in_array($ht_code, $heatinga)) { ?>checked="checked"<?php } ?> /> <?php echo $ht_title; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label># Fireplaces</label>
                        <select name="property[fireplace_num]">
                            <?php for ($i=0; $i<=50; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $property['fireplace_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Clothes Washer / Dryer</label>
                        <select name="property[washer_dryer_code]">
                            <option value="">None</option>
                            <?php foreach ($_uccms_properties->propertyWasherDryerTypes() as $wd_code => $wd_title) { ?>
                                <option value="<?php echo $wd_code; ?>" <?php if ($wd_code == $property['washer_dryer_code']) { ?>selected="selected"<?php } ?>><?php echo $wd_title; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label># Elevators</label>
                        <select name="property[elevator_num]">
                            <?php for ($i=0; $i<=10; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $property['elevator_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset class="lt-rental">
                        <input type="checkbox" name="property[has_wifi]" value="1" <?php if ($property['has_wifi']) { ?>checked="checked"<?php } ?> />
                        <label class="for-checkbox">Has Wifi</label>
                    </fieldset>

                    <fieldset>
                        <label>Pool</label>
                        <select name="property[pool_code]">
                            <option value="">None</option>
                            <?php foreach ($_uccms_properties->propertyPoolTypes() as $pt_code => $pt_title) { ?>
                                <option value="<?php echo $pt_code; ?>" <?php if ($pt_code == $property['pool_code']) { ?>selected="selected"<?php } ?>><?php echo $pt_title; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Pool Size</label>
                        <input type="text" name="property[pool_size]" value="<?=$property['pool_size']?>" style="width: 80px;" />
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Pets Allowed</label>
                        <div class="items checkboxes">
                            <?php foreach ($_uccms_properties->propertyPetTypes() as $pt_code => $pt_title) { ?>
                                <div class="item contain">
                                    <input type="checkbox" name="property[pets][]" value="<?=$pt_code?>" <?php if (in_array($pt_code, $petsa)) { ?>checked="checked"<?php } ?> /> <?php echo $pt_title; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </fieldset>

                    <fieldset class="lt-rental">
                        <label>Pet Limit</label>
                        <select name="property[pets_num]">
                            <option value="255">None</option>
                            <?php if (!$petsa[0]) { ?>
                                <option value="0" <?php if ($property['pets_num'] == 0) { ?>selected="selected"<?php } ?>>0</option>
                            <?php } ?>
                            <?php for ($i=1; $i<=10; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $property['pets_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                </div>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>

    </div>

    </form>

    <?php if ($property['id']) { ?>

        <style type="text/css">

            #rooms.table .add_container {
                display: none;
                border-bottom: 2px solid #ccc;
            }
            #rooms.table .add_container .button {
                padding: 10px 25px 25px;
            }

            #rooms.table ul.items li.item {
                height: auto;
                padding: 10px;
                border-bottom: 1px solid #ccc;
                line-height: 1.2em;
            }
            #rooms.table ul.items li.item:last-child {
                border-bottom: 0px none;
            }
            #rooms.table ul.items li.item table {
                margin: 0px;
                border: 0px none;
            }
            #rooms.table .item .heading td {
                padding: 0px;
            }
            #rooms.table .item .heading .icon_sort {
                margin-top: 0px;
            }
            #rooms.table .item .heading .toggle {
                padding-top: 5px;
                text-align: right;
                font-size: 1.2em;
            }
            #rooms.table .item .heading .collapse_info .what {
                color: #bbb;
            }
            #rooms.table .item .expand_content {
                display: none;
                margin-top: 10px;
                padding: 10px 0 10px;
                border-top: 1px solid #e5e5e5;
            }
            #rooms.table .item .delete {
                float: right;
                text-align: right;
            }
            #rooms.table .item .dimensions input {
                width: 140px;
            }
            #rooms.table .item .beds {
                padding-top: 20px;
            }
            #rooms.table .item .description {
                padding: 10px 15px 0;
            }
            #rooms.table .item .shared {
                padding: 0 15px;
            }

            #rooms.table .item.ui-sortable-helper {
                height: 44px;
                background-color: #fcffdf;
            }
            .room-sortable-placeholder {
                height: 44px;
                background-color: #fafafa;
            }

        </style>

        <script type="text/javascript">
            $(document).ready(function() {

                // ROOMS - ADD CLICK
                $('#rooms .add_resource').click(function(e) {
                    e.preventDefault();
                    $('#rooms .add_container').toggle();
                });

                // ROOMS - SORTABLE
                $('#rooms ul.ui-sortable').sortable({
                    handle: '.icon_sort',
                    axis: 'y',
                    containment: 'parent',
                    items: 'li',
                    placeholder: 'room-sortable-placeholder',
                    update: function() {
                        var rows = $(this).find('li');
                        var sort = [];
                        rows.each(function() {
                            sort.push($(this).attr('id').replace('room-', ''));
                        });
                        $('#rooms input[name="sort"]').val(sort.join());
                    }
                });

                // ROOMS - EXPAND
                $('#rooms.table .item .heading .toggle a').click(function(e) {
                    e.preventDefault();
                    var tog = $(this).closest('.item').find('.expand_content');
                    if (tog.is(':visible')) {
                        tog.hide();
                        $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-down');
                    } else {
                        tog.show();
                        $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
                    }
                });

                // ROOMS - REMOVE CLICK
                $('#rooms .icon_delete').click(function(e) {
                    e.preventDefault();
                    if (confirm('Are you sure you want to delete this?')) {
                        var id = $(this).attr('data-id');
                        var del = $('#rooms input[name="delete"]').val();
                        if (del) del += ',';
                        del += id;
                        $('#rooms input[name="delete"]').val(del);
                        $('#rooms li#room-' +id).fadeOut(500);
                    }
                });

            });
        </script>

        <a name="rooms"></a>

        <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

            <div id="rooms" class="table">

                <summary>
                    <h2>Rooms</h2>
                    <a class="add_resource add" href="#"><span></span>Add Room</a>
                </summary>

                <div class="add_container">

                    <form enctype="multipart/form-data" action="./rooms/process/" method="post">
                    <input type="hidden" name="property[id]" value="<?php echo $property['id']; ?>" />

                    <ul class="items">

                        <li id="room-new" class="item contain">

                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>

                                    <td width="33%" valign="top">
                                        <label>Level</label>
                                        <select name="room[new][level]">
                                            <?php foreach ($_uccms_properties->propertyRoomLevels() as $rl_id => $rl_title) { ?>
                                                <option value="<?php echo $rl_id; ?>" <?php if ($rl_id == 1) { ?>selected="selected"<?php } ?>><?php echo $rl_title; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>

                                    <td width="33%" valign="top">
                                        <fieldset>
                                            <label>Type</label>
                                            <select name="room[new][type_code]">
                                                <?php foreach ($_uccms_properties->propertyRoomTypes() as $rt_code => $rt_title) { ?>
                                                    <option value="<?php echo $rt_code; ?>"><?php echo $rt_title; ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </td>

                                    <td class="dimensions" width="33%" valign="top">
                                        <label>Dimensions</label>
                                        <fieldset>
                                            <input type="text" name="room[new][dimensions]" value="" placeholder="10 x 12 sqft" />
                                        </fieldset>
                                    </td>

                                </tr>
                            </table>

                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>

                                    <td width="50%" valign="top" style="<?php if ($prop['listing_type'] != 2) { ?>display: none;<?php } ?>">

                                        <div class="sleeps lt-rental">
                                            <fieldset>
                                                <label>Sleeps</label>
                                                <select name="room[new][sleeps_num]">
                                                    <?php for ($i=0; $i<=20; $i++) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>

                                        <div class="beds lt-rental">
                                            <fieldset>
                                                <label>Beds</label>
                                                <div class="items checkboxes">
                                                    <?php foreach ($_uccms_properties->propertyBedTypes() as $bt_code => $bt_title) { ?>
                                                        <div class="item">
                                                            <input type="checkbox" name="room[new][beds][]" value="<?=$bt_code?>" />
                                                            <label class="for_checkbox"><?php echo $bt_title; ?></label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </fieldset>
                                        </div>

                                    </td>

                                    <td width="50%" valign="top">
                                        <fieldset>
                                            <div class="items checkboxes">
                                                <div class="item">
                                                    <input type="checkbox" name="room[new][has_ac]" value="1" />
                                                    <label class="for_checkbox">Has AC</label>
                                                </div>
                                                <div class="item">
                                                    <input type="checkbox" name="room[new][has_ceiling_fan]" value="1" />
                                                    <label class="for_checkbox">Has Ceiling Fan</label>
                                                </div>
                                                <div class="item">
                                                    <input type="checkbox" name="room[new][has_fireplace]" value="1" />
                                                    <label class="for_checkbox">Has Fireplace</label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </td>

                                </tr>
                            </table>

                            <div class="description">
                                <fieldset>
                                    <label>Description</label>
                                    <textarea name="room[new][description]"></textarea>
                                </fieldset>
                            </div>

                            <div class="shared lt-rental" style="padding-top: 15px;">
                                <fieldset>
                                    <input type="checkbox" name="room[new][shared]" value="1" />
                                    <label class="for_checkbox">Is Shared</label>
                                </fieldset>
                            </div>

                        </li>

                    </ul>

                    <div class="button">
                        <input type="submit" value="Add" class="blue" />
                    </div>

                    </form>

                </div>

                <?php

                // GET ROOMS
                $rooms_query = "SELECT * FROM `" .$_uccms_properties->tables['property_rooms']. "` WHERE (`property_id`=" .$property['id']. ") ORDER BY `sort` ASC, `id` ASC";
                $rooms_q = sqlquery($rooms_query);

                // NUMBER OF ROOMS
                $num_rooms = sqlrows($rooms_q);

                // HAVE ROOMS
                if ($num_rooms > 0) {

                    ?>

                    <form enctype="multipart/form-data" action="./rooms/process/" method="post">
                    <input type="hidden" name="property[id]" value="<?php echo $property['id']; ?>" />

                    <input type="hidden" name="sort" value="" />
                    <input type="hidden" name="delete" value="" />

                    <ul class="items ui-sortable">

                        <?php

                        // LOOP
                        while ($room = sqlfetch($rooms_q)) {

                            // LEVEL
                            if (!$room['level']) $room['level'] = 1;

                            // BEDS
                            unset($bedsa);
                            $bedsa = explode(',', $room['bed_codes']);
                            if (!is_array($bedsa)) $bedsa = array();

                            ?>

                            <li id="room-<?php echo $room['id']; ?>" class="item contain">

                                <div class="heading">

                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>

                                            <td width="10%" valign="top">
                                                <div class="sort">
                                                    <span class="icon_sort ui-sortable-handle"></span>
                                                </div>
                                            </td>

                                            <td width="80%">
                                                <div class="collapse_info">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="25%" valign="middle">
                                                                <span class="what">Level:</span> <?php echo $_uccms_properties->propertyRoomLevels()[$room['level']]; ?>
                                                            </td>
                                                            <td width="30%" valign="middle">
                                                                <span class="what">Type:</span> <?php echo $_uccms_properties->propertyRoomTypes()[$room['type_code']]; ?>
                                                            </td>
                                                            <td width="45%" valign="middle">
                                                                <span class="what">Dimensions:</span> <?php echo stripslashes($room['dimensions']); ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>

                                            <td width="10%" valign="top">
                                                <div class="toggle">
                                                    <a href="#"><i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </td>

                                        </tr>
                                    </table>

                                </div>

                                <div class="expand_content">

                                    <div style="float: left;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>

                                                <td width="33%" valign="top">
                                                    <fieldset>
                                                        <label>Level</label>
                                                        <select name="room[<?php echo $room['id']; ?>][level]">
                                                            <?php foreach ($_uccms_properties->propertyRoomLevels() as $rl_id => $rl_title) { ?>
                                                                <option value="<?php echo $rl_id; ?>" <?php if ($rl_id == $room['level']) { ?>selected="selected"<?php } ?>><?php echo $rl_title; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </td>

                                                <td width="33%" valign="top">
                                                    <fieldset>
                                                        <label>Type</label>
                                                        <select name="room[<?php echo $room['id']; ?>][type_code]">
                                                            <?php foreach ($_uccms_properties->propertyRoomTypes() as $rt_code => $rt_title) { ?>
                                                                <option value="<?php echo $rt_code; ?>" <?php if ($rt_code == $room['type_code']) { ?>selected="selected"<?php } ?>><?php echo $rt_title; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </td>

                                                <td class="dimensions" width="33%" valign="top">
                                                    <fieldset>
                                                        <label>Dimensions</label>
                                                        <input type="text" name="room[<?php echo $room['id']; ?>][dimensions]" value="<?php echo stripslashes($room['dimensions']); ?>" placeholder="10 x 12 sqft" />
                                                    </fieldset>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>

                                    <div class="view_action delete">
                                        <a class="icon_delete" href="#" data-id="<?php echo $room['id']; ?>"></a>
                                    </div>

                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>

                                            <td width="50%" valign="top" style="<?php if ($prop['listing_type'] != 2) { ?>display: none;<?php } ?>">

                                                <div class="sleeps lt-rental">
                                                    <fieldset>
                                                        <label>Sleeps</label>
                                                        <select name="room[<?php echo $room['id']; ?>][sleeps_num]">
                                                            <?php for ($i=0; $i<=20; $i++) { ?>
                                                                <option value="<?php echo $i; ?>" <?php if ($i == $room['sleeps_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>

                                                <div class="beds lt-rental">
                                                    <fieldset>
                                                        <label>Beds</label>
                                                        <div class="items checkboxes">
                                                            <?php foreach ($_uccms_properties->propertyBedTypes() as $bt_code => $bt_title) { ?>
                                                                <div class="item">
                                                                    <input type="checkbox" name="room[<?php echo $room['id']; ?>][beds][]" value="<?=$bt_code?>" <?php if (in_array($bt_code, $bedsa)) { ?>checked="checked"<?php } ?> />
                                                                    <label class="for_checkbox"><?php echo $bt_title; ?></label>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </fieldset>
                                                </div>

                                            </td>

                                            <td width="50%" valign="top">
                                                <fieldset>
                                                    <div class="items checkboxes">
                                                        <div class="item">
                                                            <input type="checkbox" name="room[<?php echo $room['id']; ?>][has_ac]" value="1" <?php if ($room['has_ac']) { ?>checked="checked"<?php } ?> />
                                                            <label class="for_checkbox">Has AC</label>
                                                        </div>
                                                        <div class="item">
                                                            <input type="checkbox" name="room[<?php echo $room['id']; ?>][has_ceiling_fan]" value="1" <?php if ($room['has_ceiling_fan']) { ?>checked="checked"<?php } ?> />
                                                            <label class="for_checkbox">Has Ceiling Fan</label>
                                                        </div>
                                                        <div class="item">
                                                            <input type="checkbox" name="room[<?php echo $room['id']; ?>][has_fireplace]" value="1" <?php if ($room['has_fireplace']) { ?>checked="checked"<?php } ?> />
                                                            <label class="for_checkbox">Has Fireplace</label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </td>

                                        </tr>
                                    </table>

                                    <div class="description">
                                        <fieldset>
                                            <label>Description</label>
                                            <textarea name="room[<?php echo $room['id']; ?>][description]"><?php echo stripslashes($room['description']); ?></textarea>
                                        </fieldset>
                                    </div>

                                    <div class="shared lt-rental" style="padding-top: 15px;">
                                        <fieldset>
                                            <input type="checkbox" name="room[<?php echo $room['id']; ?>][shared]" value="1" <?php if ($room['shared']) { ?>checked="checked"<?php } ?> />
                                            <label class="for_checkbox">Is Shared</label>
                                        </fieldset>
                                    </div>

                                </div>

                            </li>

                            <?php

                        }

                        ?>

                    </ul>

                    <footer>
                        <input class="blue" type="submit" value="Save" />
                    </footer>

                    </form>

                    <?php

                } else {

                    ?>

                    <div style="padding: 10px; text-align: center;">
                        No rooms added yet.
                    </div>

                    <?php

                }

                ?>

            </div>

        </div>

        <style type="text/css">

            #attributes.table .add_container {
                display: none;
                border-bottom: 2px solid #ccc;
            }
            #attributes.table .add_container .button {
                padding: 10px 25px 25px;
            }

            #attributes.table ul.items li.item {
                height: auto;
                padding: 10px;
                border-bottom: 1px solid #ccc;
                line-height: 1.2em;
            }
            #attributes.table ul.items li.item:last-child {
                border-bottom: 0px none;
            }
            #attributes.table ul.items li.item table {
                margin: 0px;
                border: 0px none;
            }
            #attributes.table .item .heading td {
                padding: 0px;
            }
            #attributes.table .item .heading .icon_sort {
                margin-top: 0px;
            }
            #attributes.table .item .heading .toggle {
                text-align: right;
                font-size: 1.2em;
            }
            #attributes.table .item .heading .collapse_info .what {
                color: #bbb;
            }
            #attributes.table .item .expand_content {
                display: none;
                margin-top: 10px;
                padding: 10px 0 10px;
                border-top: 1px solid #e5e5e5;
            }
            #attributes.table .item .delete {
                float: right;
                text-align: right;
            }
            #attributes.table .item .expand_content .attribute {
                width: 100%;
                margin-bottom: 15px;
            }
            #attributes.table .item .expand_content .attribute:last-child {
                margin-bottom: 0px;
            }
            #attributes.table .item .expand_content .attribute label {
                margin-bottom: 2px;
                font-weight: bold;
            }
            #attributes.table .item .expand_content .attribute > .options {
                padding: 10px;
                background-color: #f7f7f7;
            }
            #attributes.table .item .expand_content .attribute > .options .options:after {
                content:" ";
                display:block;
                clear:both;
            }
            #attributes.table .item .expand_content .attribute .options .option {
                clear: both;
            }
            #attributes.table .item .expand_content .attribute.checkbox .options .option {
                padding-bottom: 4px;
            }
            #attributes.table .item .expand_content .attribute.checkbox .options .option:last-child {
                padding-bottom: 0px;
            }
            #attributes.table .item .expand_content .attribute.file .files .remove {
                color: #C0362F;
            }

        </style>

        <script type="text/javascript">
            $(document).ready(function() {

                // ATTRIBUTES - ADD CLICK
                $('#attributes .add_resource').click(function(e) {
                    e.preventDefault();
                    $('#attributes .add_container').toggle();
                });

                // ATTRIBUTES - EXPAND
                $('#attributes.table .item .heading .toggle a').click(function(e) {
                    e.preventDefault();
                    var tog = $(this).closest('.item').find('.expand_content');
                    if (tog.is(':visible')) {
                        tog.hide();
                        $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-down');
                    } else {
                        tog.show();
                        $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
                    }
                });

                // ATTRIBUTES - REMOVE CLICK
                $('#attributes .icon_delete').click(function(e) {
                    if (confirm('Are you sure you want to delete this?')) {
                        return true;
                    } else {
                        return false;
                    }
                });

                // ATTRIBUTES - REMOVE FILE CLICK
                $('#attributes .attribute.file .files .remove').click(function(e) {
                    e.preventDefault();
                    var id = $(this).attr('data-id');
                    $('#attributes .attribute.file .files input[name="attribute-' +id+ '-remove"]').val('1');
                    $('#attributes .attribute.file .files .file-' +id).remove();
                });

            });
        </script>

        <?php

        // SELECTED ATTRIBUTES
        $selattra = json_decode(stripslashes($property['attributes']), true);

        // PROPERTY GROUPS ARRAY
        $pgroupsa = array();

        /*
        // GET ATTRIBUTE GROUPS FOR PROPERTY
        $groups_query = "
        SELECT *
        FROM `" .$_uccms_properties->tables['property_attribute_groups']. "` AS `pag`
        INNER JOIN `" .$_uccms_properties->tables['attribute_groups']. "` AS `ag` ON pag.group_id=ag.id
        WHERE (pag.property_id=" .$property['id']. ")
        ORDER BY ag.sort ASC, ag.title ASC";
        $groups_q = sqlquery($groups_query);
        while ($group = sqlfetch($groups_q)) {
            $pgroupsa[$group['id']] = $group;
        }
        */

        // GET ATTRIBUTE GROUPS FOR PROPERTY TYPE
        $groups_query = "
        SELECT ag.*
        FROM `" .$_uccms_properties->tables['attribute_groups']. "` AS `ag`
        INNER JOIN `" .$_uccms_properties->tables['attribute_groups_property_types']. "` AS `agpt` ON ag.id=agpt.group_id
        WHERE (ag.active=1) AND (agpt.property_type_code='" .$property['property_type_code']. "')
        ORDER BY `sort` ASC, `title` ASC
        ";
        $groups_q = sqlquery($groups_query);
        while ($group = sqlfetch($groups_q)) {
            $pgroupsa[$group['id']] = $group;
        }

        // NUMBER OF GROUPS
        $num_groups = count($pgroupsa);

        ?>

        <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

            <div id="attributes" class="table">

                <summary>
                    <h2>Attributes</h2>
                    <? /*<a class="add_resource add" href="#"><span></span>Add Attribute</a>*/ ?>
                </summary>

                <? /*
                <div class="add_container">

                    <form enctype="multipart/form-data" action="./attributes/group-add/" method="post">
                    <input type="hidden" name="property[id]" value="<?php echo $property['id']; ?>" />

                    <ul class="items">

                        <li id="agroup-new" class="item contain">

                            <table cellpadding="0" cellspacing="0" style="width: auto;">
                                <tr>
                                    <td>Group:</td>
                                    <td>
                                        <select name="group[id]" style="display: inline-block; float: none;">
                                            <?php
                                            $group_query = "SELECT `id`, `title` FROM `" .$_uccms_properties->tables['attribute_groups']. "` WHERE (`active`=1) ORDER BY `sort` ASC, `title` ASC";
                                            $group_q = sqlquery($group_query);
                                            while ($group = sqlfetch($group_q)) {
                                                if (!$pgroupsa[$group['id']]) {
                                                    ?>
                                                    <option value="<?php echo $group['id']; ?>"><?php echo stripslashes($group['title']); ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="submit" value="Add" class="blue" />
                                    </td>
                                </tr>
                            </table>

                        </li>

                    </ul>

                    </form>

                </div>
                */ ?>

                <?php

                // HAVE GROUPS
                if ($num_groups > 0) {

                    ?>

                    <form enctype="multipart/form-data" action="./attributes/process/" method="post">
                    <input type="hidden" name="property[id]" value="<?php echo $property['id']; ?>" />

                    <input type="hidden" name="sort" value="" />
                    <input type="hidden" name="delete" value="" />

                    <ul class="items ui-sortable">

                        <?php

                        // LOOP
                        foreach ($pgroupsa as $group) {

                            ?>

                            <li id="agroup-<?php echo $group['id']; ?>" class="item contain">

                                <div class="heading">

                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>

                                            <td width="90%">
                                                <div class="collapse_info">
                                                    <?php echo stripslashes($group['title']); ?>
                                                </div>
                                            </td>

                                            <td width="10%" valign="top">
                                                <div class="toggle">
                                                    <a href="#"><i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </td>

                                        </tr>
                                    </table>

                                </div>

                                <div class="expand_content">

                                    <div style="float: left; width: 50%;">

                                        <?php

                                        // GET ATTRIBUTES FOR GROUP
                                        $attr_query = "SELECT * FROM `" .$_uccms_properties->tables['attributes']. "` WHERE (`group_id`=" .$group['id']. ")";
                                        $attr_q = sqlquery($attr_query);
                                        while ($attribute = sqlfetch($attr_q)) {

                                            $key = $attribute['id'];

                                            ?>

                                            <div class="attribute contain">

                                                <?php

                                                // ATTRIBUTE USE COUNT ARRAY
                                                $auca = array();

                                                unset($field_values);

                                                // HAVE ALREADY SELECTED OPTIONS
                                                if (is_array($selattra)) {

                                                    // CHECKBOX, WIDTH/HEIGHT
                                                    if (($attribute['type'] == 'checkbox') || ($attribute['type'] == 'width_height')) {
                                                        $field_values = $selattra[$attribute['id']]; // ARRAY OF SELECTED OPTIONS

                                                    // ALL OTHER FIELDS
                                                    } else {
                                                        if (!$auca[$attribute['id']]) $auca[$attribute['id']] = 0;
                                                        $field_values = $selattra[$attribute['id']][$auca[$attribute['id']]]; // ONLY FIRST VALUE OF ARRAY EACH TIME
                                                    }

                                                // USE DEFAULTS
                                                } else {

                                                    // ATTRIBUTE HAS OPTIONS
                                                    if ($attribute['options']) {
                                                        $options = json_decode($attribute['options'], true);
                                                        foreach ($options as $option) {
                                                            if ($option['default']) {
                                                                $field_values = stripslashes($option['title']); // DEFAULT
                                                            }
                                                        }
                                                    }

                                                }

                                                // ATTRIBUTE ELEMENT
                                                include(dirname(__FILE__). '/attributes/types/base.php');

                                                $auca[$attribute['id']]++;

                                                ?>

                                            </div>

                                            <?php

                                        }

                                        ?>

                                    </div>

                                    <? /*
                                    <div class="view_action delete">
                                        <a class="icon_delete" href="./attributes/group-remove/?property_id=<?php echo $group['id']; ?>&group_id=<?php echo $group['id']; ?>"></a>
                                    </div>
                                    */ ?>

                                </div>

                            </li>

                            <?php

                        }

                        ?>

                    </ul>

                    <footer>
                        <input class="blue" type="submit" value="Save" />
                    </footer>

                    </form>

                    <?php

                } else {

                    ?>

                    <div style="padding: 10px; text-align: center;">
                        No attributes for property type.
                    </div>

                    <?php

                }

                ?>

            </div>

        </div>

        <style type="text/css">

            #images .add_container, #videos .add_container {
                padding: 10px;
                background-color: #fafafa;
                border-bottom: 1px solid #ccc;
            }

            #images .add_container td, #videos .add_container td {
                vertical-align: top;
            }

            #images.table section a, #categories.table section a, #videos.table section a, #videos.table section a {
                margin-top: 0px;
                margin-bottom: 0px;
            }

            #images.table > ul li, #videos.table > ul li {
                height: auto;
                padding: 10px;
                line-height: 1.2em;
            }

            #images.table > ul li section, #videos.table > ul li section {
                height: auto;
                overflow: visible;
                white-space: normal;
            }

            #images.table > ul li section:first-child, #videos.table > ul li section:first-child {
                padding-left: 0px;
            }

            #images .items .item .image, #videos .items .item .video {
                width: 460px;
                padding: 0 25px;
            }
            #images .items .item .title, #videos .items .item .title {
                width: 370px;
            }

            #images.table .currently .remove_resource, #videos.table .currently .remove_resource {
                margin-top: -3px;
                margin-bottom: -10px;
            }

            #images .items .item .title fieldset label, #videos .items .item .title fieldset label {
                margin-bottom: 2px;
                text-align: left;
            }

            #categories.table > ul li {
                height: auto;
                padding: 10px;
                line-height: 1.2em;
            }

            #categories.table > ul li li {
                padding-bottom: 0px;
                padding-left: 20px;
                border: 0px none;
                background-color: transparent;
            }

            #categories.table > ul li section {
                height: auto;
                overflow: visible;
                white-space: normal;
            }

            #categories.table > ul li section:first-child {
                padding-left: 0px;
            }

            #categories ul .checkbox {
                margin-top: -1px;
            }

        </style>

        <script type="text/javascript">
            $(document).ready(function() {

                // IMAGES - SORTABLE
                $('#images ul.ui-sortable').sortable({
                    handle: '.icon_sort',
                    axis: 'y',
                    containment: 'parent',
                    items: 'li',
                    //placeholder: 'ui-sortable-placeholder',
                    update: function() {
                        var rows = $(this).find('li');
                        var sort = [];
                        rows.each(function() {
                            sort.push($(this).attr('id').replace('row_', ''));
                        });
                        $('#images input[name="sort"]').val(sort.join());
                    }
                });

                // IMAGES - ADD CLICK
                $('#images .add_resource').click(function(e) {
                    e.preventDefault();
                    $('#images .add_container').toggle();
                });

                // IMAGES - REMOVE CLICK
                $('#images .icon_delete').click(function(e) {
                    e.preventDefault();
                    if (confirm('Are you sure you want to delete this?')) {
                        var id = $(this).attr('data-id');
                        var del = $('#images input[name="delete"]').val();
                        if (del) del += ',';
                        del += id;
                        $('#images input[name="delete"]').val(del);
                        $('#images li#row_' +id).fadeOut(500);
                    }
                });

                // VIDEOS - SORTABLE
                $('#videos ul.ui-sortable').sortable({
                    handle: '.icon_sort',
                    axis: 'y',
                    containment: 'parent',
                    items: 'li',
                    //placeholder: 'ui-sortable-placeholder',
                    update: function() {
                        var rows = $(this).find('li');
                        var sort = [];
                        rows.each(function() {
                            sort.push($(this).attr('id').replace('row_', ''));
                        });
                        $('#videos input[name="sort"]').val(sort.join());
                    }
                });

                // VIDEOS - ADD CLICK
                $('#videos .add_resource').click(function(e) {
                    e.preventDefault();
                    $('#videos .add_container').toggle();
                });

                // VIDEOS - REMOVE CLICK
                $('#videos .icon_delete').click(function(e) {
                    e.preventDefault();
                    if (confirm('Are you sure you want to delete this?')) {
                        var id = $(this).attr('data-id');
                        var del = $('#videos input[name="delete"]').val();
                        if (del) del += ',';
                        del += id;
                        $('#videos input[name="delete"]').val(del);
                        $('#videos li#row_' +id).fadeOut(500);
                    }
                });

            });

        </script>

        <a name="images"></a>

        <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

            <form enctype="multipart/form-data" action="./images/process/" method="post">
            <input type="hidden" name="property[id]" value="<?php echo $property['id']; ?>" />

            <div id="images" class="table">

                <input type="hidden" name="sort" value="" />
                <input type="hidden" name="delete" value="" />

                <summary>
                    <h2>Images</h2>
                    <a class="add_resource add" href="#"><span></span>Add Image</a>
                </summary>

                <div class="add_container" style="display: none;">
                    <table style="margin-bottom: 0px; border: 0px;">
                        <tr>
                            <td width="460">
                                <?php
                                $field = array(
                                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                    'key'       => 'new', // The value you should use for the "name" attribute of your form field
                                    'value'     => '', // The existing value for this form field
                                    'id'        => 'image-new', // A unique ID you can assign to your form field for use in JavaScript
                                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                    'options'   => array(
                                        'image' => true
                                    ),
                                    'required'  => false // A boolean value of whether this form field is required or not
                                );
                                include(BigTree::path('admin/form-field-types/draw/upload.php'));
                                ?>
                            </td>
                            <td>
                                <fieldset>
                                    <input type="text" name="new-caption" value="" placeholder="Caption / Alt Text" style="width: 300px;" />
                                </fieldset>
                            </td>
                            <td>
                                <input class="blue" type="submit" value="Add" />
                            </td>
                        </tr>
                    </table>
                </div>

                <?php

                // GET IMAGES
                $image_query = "SELECT * FROM `" .$_uccms_properties->tables['property_images']. "` WHERE (`property_id`=" .$property['id']. ") ORDER BY `sort` ASC, `id` ASC";
                $image_q = sqlquery($image_query);

                // NUMBER OF IMAGES
                $num_images = sqlrows($image_q);

                // HAVE IMAGES
                if ($num_images > 0) {

                    ?>

                    <ul class="items ui-sortable">

                        <?php

                        // LOOP
                        while ($image = sqlfetch($image_q)) {

                            ?>

                            <li id="row_<?php echo $image['id']; ?>" class="item contain">
                                <section class="sort">
                                    <span class="icon_sort ui-sortable-handle"></span>
                                </section>
                                <section class="image">
                                    <?php

                                    $field = array(
                                        'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                        'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                        'key'       => 'image-' .$image['id'], // The value you should use for the "name" attribute of your form field
                                        'value'     => ($image['image'] ? $_uccms_properties->imageBridgeOut($image['image'], 'properties') : ''), // The existing value for this form field
                                        'id'        => 'image-' .$image['id'], // A unique ID you can assign to your form field for use in JavaScript
                                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                        'options'   => array(
                                            'image' => true
                                        ),
                                        'required'  => false // A boolean value of whether this form field is required or not
                                    );
                                    include(BigTree::path('admin/form-field-types/draw/upload.php'));

                                    ?>
                                </section>
                                <section class="title">
                                    <fieldset>
                                        <input type="text" name="image-<?php echo $image['id']; ?>-caption" value="<?php echo stripslashes($image['caption']); ?>" placeholder="Caption / Alt Text" style="width: 300px;" />
                                    </fieldset>
                                </section>
                                <section class="view_action">
                                    <a class="icon_delete" href="#" data-id="<?php echo $image['id']; ?>"></a>
                                </section>
                            </li>

                            <?php

                        }

                        ?>

                    </ul>

                    <footer>
                        <input class="blue" type="submit" value="Save" />
                    </footer>

                    <?php

                } else {

                    ?>

                    <div style="padding: 10px; text-align: center;">
                        No images added yet.
                    </div>

                    <?php

                }

                ?>

            </div>

            </form>

        </div>

        <a name="videos"></a>

        <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

            <form enctype="multipart/form-data" action="./videos/process/" method="post">
            <input type="hidden" name="property[id]" value="<?php echo $property['id']; ?>" />

            <div id="videos" class="table">

                <input type="hidden" name="sort" value="" />
                <input type="hidden" name="delete" value="" />

                <summary>
                    <h2>Videos</h2>
                    <a class="add_resource add" href="#"><span></span>Add Video</a>
                </summary>

                <div class="add_container" style="display: none;">
                    <table style="margin-bottom: 0px; border: 0px;">
                        <tr>
                            <td width="460">
                                <input id="video-new" type="text" name="video[new][url]" value="" placeholder="YouTube / Vimeo URL" style="width: 400px;" />
                            </td>
                            <td>
                                <fieldset>
                                    <input type="text" name="video[new][caption]" value="" placeholder="Title" style="width: 300px;" />
                                </fieldset>
                            </td>
                            <td>
                                <input class="blue" type="submit" value="Add" />
                            </td>
                        </tr>
                    </table>
                </div>

                <?php

                // GET VIDEOS
                $video_query = "SELECT * FROM `" .$_uccms_properties->tables['property_videos']. "` WHERE (`property_id`=" .$property['id']. ") ORDER BY `sort` ASC, `id` ASC";
                $video_q = sqlquery($video_query);

                // NUMBER OF VIDEOS
                $num_videos = sqlrows($video_q);

                // HAVE VIDEOS
                if ($num_videos > 0) {

                    ?>

                    <ul class="items ui-sortable">

                        <?php

                        // LOOP
                        while ($video = sqlfetch($video_q)) {

                            // GET VIDEO INFO
                            $vi = videoInfo(stripslashes($video['video']));

                            ?>

                            <li id="row_<?php echo $video['id']; ?>" class="item contain">
                                <section class="sort">
                                    <span class="icon_sort ui-sortable-handle"></span>
                                </section>
                                <section class="video">
                                    <input type="text" name="video[<?php echo $video['id']; ?>][url]" value="<?php echo stripslashes($video['video']); ?>" placeholder="YouTube / Vimeo URL" style="width: 400px;" />
                                </section>
                                <section class="title">
                                    <fieldset>
                                        <input type="text" name="video[<?php echo $video['id']; ?>][caption]" value="<?php echo stripslashes($video['caption']); ?>" placeholder="Title" style="width: 300px;" />
                                    </fieldset>
                                </section>
                                <section class="view_action">
                                    <a class="icon_delete" href="#" data-id="<?php echo $video['id']; ?>"></a>
                                </section>
                                <?php if ($vi['thumb']) { ?>
                                    <div style="clear: both; padding: 20px 0 0 42px;">
                                        <?php if ($vi['thumb']) { ?>
                                            <a href="<?php echo $video['video']; ?>" target="_blank"><img src="<?php echo $vi['thumb']; ?>" alt="" style="max-width: 150px;" /></a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </li>

                            <?php

                        }

                        ?>

                    </ul>

                    <footer>
                        <input class="blue" type="submit" value="Save" />
                    </footer>

                    <?php

                } else {

                    ?>

                    <div style="padding: 10px; text-align: center;">
                        No videos added yet.
                    </div>

                    <?php

                }

                ?>

            </div>

            </form>

        </div>

        <a name="billing"></a>

        <form enctype="multipart/form-data" action="./payment/update/" method="post">
        <input type="hidden" name="property_id" value="<?php echo $property['id']; ?>" />

        <div id="payment" class="container legacy">
            <header>
                <h2>Billing</h2>
            </header>
            <section>

                <fieldset>
                    <label>Expires</label>
                    <div class="contain">
                        <?php

                        if ($property['payment_expires']) {
                            if ($property['payment_expires'] == '0000-00-00') {
                                $expires = '';
                            } else {
                                $expires = $property['payment_expires'];
                            }
                        } else {
                            $expires = '';
                        }

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'payment_expires',
                            'key'       => 'property[payment_expires]',
                            'value'     => $expires,
                            'required'  => false,
                            'options'   => array(
                                'default_now'   => false
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>
                </fieldset>

            </section>
            <footer>
                <input class="blue" type="submit" value="Update" />
            </footer>
        </div>

        </form>

        <a name="payment"></a>

        <style type="text/css">

            #payment .method {
                clear: both;
                margin-bottom: 10px;
            }
            #payment .method:last-child {
                margin-bottom: 0px;
            }
            #payment .method .radio_button {
                margin-top: -3px;
            }
            #payment .method .info {
                clear: both;
                display: none;
                padding: 10px;
            }
            #payment .method:last-child .info {
                padding-bottom: 0px;
            }

        </style>

        <script type="text/javascript">

            $(document).ready(function() {

                // PAYMENT METHOD SELECT
                $('#payment .method input[name="payment[method]"]').click(function() {
                    $('#payment .method .info').hide();
                    $(this).closest('.method').find('.info').show();
                });

                // CARD SELECT
                $('#payment .method .payment_profile select[name="payment[profile_id]"').change(function() {
                    if ($(this).val() != null) {
                        $('#card_new').hide();
                    } else {
                        $('#card_new').show();
                    }
                });

            });

        </script>

        <form enctype="multipart/form-data" action="./payment/process/" method="post">
        <input type="hidden" name="property_id" value="<?php echo $property['id']; ?>" />

        <div id="payment" class="container expand">
            <header>
                <h2>Record Payment</h2>
            </header>
            <section>

                <fieldset>
                    <label>Amount</label>
                    $<input type="text" name="amount" value="<?php echo $pricing['listing_price']; ?>" class="smaller_60" />
                </fieldset>

                <?php

                // MAKE SURE CHECK / CASH IS OPTION
                if (!$payment_methods['check_cash']) {
                    $payment_methods['check_cash'] = array(
                        'id'        => 'check_cash',
                        'active'    => 1,
                        'title'     => 'Check / Cash'
                    );
                }

                // PAYMENT METHODS
                foreach ($payment_methods as $payment) {

                    ?>

                    <div class="method">
                        <div class="top">
                            <input type="radio" name="payment[method]" value="<?php echo $payment['id']; ?>" <?php if (($payment['id'] == $vals['payment']['method']) || ($num_payment_methods == 1)) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($payment['title']); ?>
                        </div>
                        <div class="info">

                            <?php if ($payment['id'] == 'check_cash') { ?>

                                <fieldset>
                                    <label>Check # / Note</label>
                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][note]" value="" />
                                </fieldset>

                            <?php } else { ?>

                                <?php if (count($payment_profiles) > 0) { ?>
                                    <div class="payment_profile">
                                        <fieldset>
                                            <select name="payment[profile_id]">
                                                <?php foreach ($payment_profiles as $pp) { ?>
                                                    <option value="<?php echo $pp['id']; ?>"><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                                                <?php } ?>
                                                <option value="">New Card</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                <?php } ?>

                                <div id="card_new" style="<?php if (count($payment_profiles) > 0) { ?>display: none; padding-top: 15px;<?php } ?>">

                                    <fieldset>
                                        <label>Name On Card *</label>
                                        <input type="text" name="payment[<?php echo $payment['id']; ?>][name]" value="" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Card Number *</label>
                                        <input type="text" name="payment[<?php echo $payment['id']; ?>][number]" value="" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Card Expiration *</label>
                                        <input type="text" name="payment[<?php echo $payment['id']; ?>][expiration]" value="" placeholder="mm/yy" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Card Zip *</label>
                                        <input type="text" name="payment[<?php echo $payment['id']; ?>][zip]" value="" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Card CVV *</label>
                                        <input type="text" name="payment[<?php echo $payment['id']; ?>][cvv]" value="" />
                                    </fieldset>

                                </div>

                            <?php } ?>

                        </div>
                    </div>

                    <?php

                }

                ?>

            </section>
            <footer>
                <input class="blue" type="submit" value="Process" /> <small style="display: inline-block; margin-top: 12px;">Only click once.</small>
            </footer>
        </div>

        </form>

        <style type="text/css">

            #transactions .transaction_method {
                width: 150px;
                text-align: left;
            }
            #transactions .transaction_dt {
                width: 160px;
                text-align: left;
            }
            #transactions .transaction_status {
                width: 100px;
                text-align: left;
            }
            #transactions .transaction_amount {
                width: 90px;
                text-align: left;
            }
            #transactions .transaction_id {
                width: 200px;
                text-align: left;
            }
            #transactions .transaction_message {
                width: 240px;
                text-align: left;
            }

        </style>

        <a name="payments"></a>

        <?php

        // GET PAYMENT METHODS
        $payment_methods = $_uccms_properties->getPaymentMethods(true);

        // GET TRANSACTION LOG
        $translog_query = "SELECT * FROM `" .$_uccms_properties->tables['transaction_log']. "` WHERE (`property_id`='" .$property['id']. "') ORDER BY `dt` DESC";
        $translog_q = sqlquery($translog_query);

        ?>

        <div id="transactions" class="table">
            <summary>
                <h2>Payment History</h2>
            </summary>
            <header style="clear: both;">
                <span class="transaction_method">Method</span>
                <span class="transaction_dt">Time</span>
                <span class="transaction_status">Status</span>
                <span class="transaction_amount">Amount</span>
                <span class="transaction_id">ID</span>
                <span class="transaction_message">Message</span>
            </header>

            <?php if (sqlrows($translog_q) > 0) { ?>

                <ul class="items">

                    <?php

                    // LOOP
                    while ($transaction = sqlfetch($translog_q)) {

                        ?>

                        <li class="item">
                            <section class="transaction_method">
                                <?php if ($transaction['method'] == 'profile') { echo 'Profile'; } else { echo stripslashes($payment_methods[$transaction['method']]['title']); } if ($transaction['lastfour']) { echo ' (' .$transaction['lastfour']. ')'; } ?>
                            </section>
                            <section class="transaction_dt">
                                <?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?>
                            </section>
                            <section class="transaction_status">
                                <?php echo ucwords($transaction['status']); ?>
                            </section>
                            <section class="transaction_amount">
                                $<?php echo number_format($transaction['amount'], 2); ?>
                            </section>
                            <section class="transaction_id">
                                <?php echo stripslashes($transaction['transaction_id']); ?>
                            </section>
                            <section class="transaction_message">
                                <?php echo stripslashes($transaction['message']); ?>
                            </section>
                        </li>

                        <?php

                        unset($oitems);

                    }

                    ?>

                </ul>

                <?php

            // NO ITEMS
            } else {
                ?>
                <div style="padding: 10px 0; text-align: center;">
                    No transactions.
                </div>
                <?php
            }

            ?>

        </div>

        <script src="/js/lib/ratings-reviews/jquery.barrating.min.js"></script>

        <style type="text/css">

            .uccms_ratings-reviews .pages.top {
                display: block;
                margin-bottom: 10px;
                text-align: right;
            }

            .uccms_ratings-reviews .results .items .item .criteria .select, .uccms_ratings-reviews .results .items .item .stars .select {
                display: none;
            }
            .uccms_ratings-reviews .results .items .item .criteria label, .uccms_ratings-reviews .results .items .item .stars label {
                display: inline-block;
            }

        </style>

        <a name="reviews"></a>

        <div id="reviews">

            <div class="container legacy">

                <header>
                    <h2>Reviews</h2>
                </header>

                <section>

                    <div class="contain">

                        <?php

                        // RATINGS / REVIEWS
                        include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
                        if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

                        // VARS FOR REVIEWS
                        $reviews_vars = array(
                            'source'    => $_uccms_properties->Extension,
                            'what'      => 'property',
                            'item_id'   => $property['id']
                        );

                        // IS OWNER
                        $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'reply', true);
                        $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'status', true);
                        $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'delete', true);

                        ?>

                        <div class="section reviews uccms_ratings-reviews">
                            <div class="results">
                                <?php include(SERVER_ROOT. 'templates/ajax/ratings-reviews/paged-results-admin.php'); ?>
                            </div>
                        </div>

                    </div>

                </section>

            </div>

        </div>

    <?php } ?>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>