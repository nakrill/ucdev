<?php

//echo print_r($_POST);
//exit;

// CLEAN UP
$id = (int)$_POST['property']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE PROPERTY ID
    if ($id) {

        // HAVE ROOMS
        if (is_array($_POST['room'])) {

            // LOOP
            foreach ($_POST['room'] as $room_id => $room) {

                if (!is_array($room['beds'])) $room['beds'] = array();

                // DB COLUMNS
                $columns = array(
                    'property_id'       => $id,
                    'sort'              => 999,
                    'level'             => $room['level'],
                    'type_code'         => $room['type_code'],
                    'dimensions'        => $room['dimensions'],
                    'sleeps_num'        => (int)$room['sleeps_num'],
                    'bed_codes'         => implode(',', $room['beds']),
                    'has_ac'            => (int)$room['has_ac'],
                    'has_ceiling_fan'   => (int)$room['has_ceiling_fan'],
                    'has_fireplace'     => (int)$room['has_fireplace'],
                    'description'       => $room['description'],
                    'shared'            => (int)$room['shared'],
                );

                // NEW ROOM
                if (($room_id == 'new') && ($columns['type_code'])) {

                    // INSERT INTO DB
                    $query = "INSERT INTO `" .$_uccms_properties->tables['property_rooms']. "` SET " .uccms_createSet($columns);

                // EXISTING ROOM
                } else {

                    // UPDATE DB
                    $query = "UPDATE `" .$_uccms_properties->tables['property_rooms']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$room_id. ")";

                }

                // RUN QUERY
                if (sqlquery($query)) {

                    if ($room_id == 'new') {
                        $admin->growl('Room', 'Room added!');
                    } else {
                        $admin->growl('Room', 'Room(s) updated!');
                    }

                }

            }

        }

        // SORT ARRAY
        $sorta = explode(',', $_POST['sort']);

        // ARE DELETING
        if ($_POST['delete']) {

            // GET ID'S TO DELETE
            $ida = explode(',', $_POST['delete']);

            // HAVE ID'S
            if (count($ida) > 0) {

                // LOOP
                foreach ($ida as $room_id) {

                    // CLEAN UP
                    $room_id = (int)$room_id;

                    // HAVE VALID ID
                    if ($room_id) {

                        // REMOVE FROM DATABASE
                        $query = "DELETE FROM `" .$_uccms_properties->tables['property_rooms']. "` WHERE (`id`=" .$room_id. ")";
                        sqlquery($query);

                        // REMOVE FROM SORT ARRAY
                        if (($key = array_search($room_id, $sorta)) !== false) {
                            unset($sorta[$key]);
                        }


                    }

                }

            }

        }

        // HAVE ROOMS TO SORT
        if (count($sorta) > 0) {

            $i = 0;

            // LOOP
            foreach ($sorta as $room_id) {

                // CLEAN UP
                $room_id = (int)$room_id;

                // HAVE VALID ID
                if ($room_id) {

                    // UPDATE DATABASE
                    $query = "UPDATE `" .$_uccms_properties->tables['property_rooms']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$room_id. ")";
                    sqlquery($query);

                    $i++;

                }

            }

        }

    // property ID NOT SPECIFIED
    } else {
        $admin->growl('Property', 'Property ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id. '#rooms');

?>