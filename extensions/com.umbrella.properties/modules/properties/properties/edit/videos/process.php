<?php

/*
echo print_r($_FILES);
echo print_r($_POST);
exit;
*/

// CLEAN UP
$id = (int)$_POST['property']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE PROPERTY ID
    if ($id) {

        // HAVE VIDEOS
        if (is_array($_POST['video'])) {

            // LOOP
            foreach ($_POST['video'] as $video_id => $video) {

                /*
                // NOT NEW
                if ($video_id != 'new') {

                    // CLEAN UP
                    $video_id = (int)$video_id;

                    // HAVE VIDEO ID
                    if ($video_id) {

                        // DELETING EXISTING
                        if (!$_POST[$video_field]) {

                            // GET CURRENT VIDEO
                            $ex_query = "SELECT `video` FROM `" .$_uccms_properties->tables['property_videos']. "` WHERE (`id`=" .$video_id. ")";
                            $ex = sqlfetch(sqlquery($ex_query));

                            // THERE'S AN EXISTING VIDEO
                            if ($ex['video']) {

                                // UPDATE DATABASE
                                $query = "UPDATE `" .$_uccms_properties->tables['property_videos']. "` SET `video`='' WHERE (`id`=" .$video_id. ")";
                                sqlquery($query);

                            }

                        }

                    }

                }
                */

                $query = '';

                // DB COLUMNS
                $columns = array(
                    'property_id'   => $id,
                    'sort'          => 999,
                    'video'         => $video['url'],
                    'caption'       => $video['caption']
                );

                // NEW VIDEO
                if ($video_id == 'new') {

                    // HAVE VIDEO
                    if ($columns['video']) {

                        // INSERT INTO DB
                        $query = "INSERT INTO `" .$_uccms_properties->tables['property_videos']. "` SET " .uccms_createSet($columns);

                    }

                // EXISTING VIDEO
                } else {

                    // UPDATE DB
                    $query = "UPDATE `" .$_uccms_properties->tables['property_videos']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$video_id. ")";

                }

                // HAVE QUERY
                if ($query) {

                    // RUN QUERY
                    if (sqlquery($query)) {

                        // NEW
                        if ($video_id == 'new') {
                            $admin->growl('Video', 'Video added!');
                        }

                    }

                }

            }

        }

        // SORT ARRAY
        $sorta = explode(',', $_POST['sort']);

        // ARE DELETING
        if ($_POST['delete']) {

            // GET ID'S TO DELETE
            $ida = explode(',', $_POST['delete']);

            // HAVE ID'S
            if (count($ida) > 0) {

                // LOOP
                foreach ($ida as $video_id) {

                    // CLEAN UP
                    $video_id = (int)$video_id;

                    // HAVE VALID ID
                    if ($video_id) {

                        // REMOVE FROM DATABASE
                        $query = "DELETE FROM `" .$_uccms_properties->tables['property_videos']. "` WHERE (`id`=" .$video_id. ")";
                        sqlquery($query);

                        // REMOVE FROM SORT ARRAY
                        if (($key = array_search($video_id, $sorta)) !== false) {
                            unset($sorta[$key]);
                        }


                    }

                }

            }

        }

        // HAVE VIDEOS TO SORT
        if (count($sorta) > 0) {

            $i = 0;

            // LOOP
            foreach ($sorta as $video_id) {

                // CLEAN UP
                $video_id = (int)$video_id;

                // HAVE VALID ID
                if ($video_id) {

                    // UPDATE DATABASE
                    $query = "UPDATE `" .$_uccms_properties->tables['property_videos']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$video_id. ")";
                    sqlquery($query);

                    $i++;

                }

            }

        }

    // property ID NOT SPECIFIED
    } else {
        $admin->growl('Property', 'Property ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id. '#videos');

?>