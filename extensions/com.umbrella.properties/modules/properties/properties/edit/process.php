<?php

//echo print_r($_POST);
//exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['property']['id'];

    // SLUG NOT SPECIFIED
    if (!$_POST['property']['slug']) {
        if ($_POST['property']['title']) {
            $_POST['property']['slug'] = $_POST['property']['title'];
        } else {
            $_POST['property']['slug'] = $_POST['property']['address1'];
        }
    }

    if (!is_array($_POST['property']['cooling'])) $_POST['property']['cooling'] = array();
    if (!is_array($_POST['property']['heating'])) $_POST['property']['heating'] = array();
    if (!is_array($_POST['property']['parking'])) $_POST['property']['parking'] = array();
    if (!is_array($_POST['property']['pets'])) $_POST['property']['pets'] = array();

    if (count($_POST['property']['pets']) > 0) {
        if (!$_POST['property']['pets_num']) $_POST['property']['pets_num'] = 255;
    } else {
        $_POST['property']['pets_num'] = 0;
    }

    // DB COLUMNS
    $columns = array(
        'slug'                      => $_uccms_properties->makeRewrite($_POST['property']['slug']),
        'property_id'               => $_POST['property']['property_id'],
        'agency_id'                 => (int)$_POST['property']['agency_id'],
        'agent_id'                  => (int)$_POST['property']['agent'][$_POST['property']['agency_id']]['agent_id'],
        'city_id'                   => (int)$_POST['property']['city_id'],
        'neighborhood_id'           => (int)$_POST['property']['neighborhood'][$_POST['property']['city_id']]['neighborhood_id'],
        'status'                    => (int)$_POST['property']['status'],
        'listing_type'              => (int)$_POST['property']['listing_type'],
        'property_type_code'        => $_POST['property']['property_type_code'],
        'rental_space_code'         => $_POST['property']['rental_space_code'],
        'address1'                  => $_POST['property']['address1'],
        'address2'                  => $_POST['property']['address2'],
        'unit'                      => $_POST['property']['unit'],
        'city'                      => $_POST['property']['city'],
        'state'                     => $_POST['property']['state'],
        'zip'                       => $_POST['property']['zip'],
        'zip4'                      => $_POST['property']['zip4'],
        'location_code'             => $_POST['property']['location_code'],
        'title'                     => $_POST['property']['title'],
        'description'               => $_POST['property']['description'],
        'price_from'                => $_uccms_properties->toFloat($_POST['property']['price_from']),
        'price_to'                  => $_uccms_properties->toFloat($_POST['property']['price_to']),
        'price_term_code'           => $_POST['property']['price_term_code'],
        'sqft'                      => $_uccms_properties->toFloat($_POST['property']['sqft'], false),
        'lot_acres'                 => preg_replace('/\D\./', '', $_POST['property']['lot_acres']),
        'year_built'                => $_uccms_properties->toFloat($_POST['property']['year_built'], false),
        'accommodates_num'          => $_uccms_properties->toFloat($_POST['property']['accommodates_num'], false),
        'bedrooms_num'              => $_uccms_properties->toFloat($_POST['property']['bedrooms_num'], false),
        'bedrooms_num_master'       => $_uccms_properties->toFloat($_POST['property']['bedrooms_num_master'], false),
        'bathrooms_num'             => preg_replace('/\D\./', '', $_POST['property']['bathrooms_num']),
        'rental_bathrooms_code'     => $_POST['property']['rental_bathrooms_code'],
        'cooling_type_codes'        => implode(',', $_POST['property']['cooling']),
        'heating_type_codes'        => implode(',', $_POST['property']['heating']),
        'fireplace_num'             => $_uccms_properties->toFloat($_POST['property']['fireplace_num'], false),
        'washer_dryer_code'         => $_POST['property']['washer_dryer_code'],
        'elevator_num'              => $_uccms_properties->toFloat($_POST['property']['elevator_num'], false),
        'has_wifi'                  => (int)$_POST['property']['has_wifi'],
        'parking_num'               => $_uccms_properties->toFloat($_POST['property']['parking_num'], false),
        'parking_type_codes'        => implode(',', $_POST['property']['parking']),
        'gated_code'                => $_POST['property']['gated_code'],
        'pool_code'                 => $_POST['property']['pool_code'],
        'pool_size'                 => $_POST['property']['pool_size'],
        'pets_num'                  => (int)$_POST['property']['pets_num'],
        'pet_type_codes'            => implode(',', $_POST['property']['pets']),
        'checkin_office'            => $_POST['property']['checkin_office'],
        'checkin_day'               => $_uccms_properties->toFloat($_POST['property']['checkin_day'], false),
        'checkin_time'              => ($_POST['property']['checkin_time'] ? date('H:i:s', strtotime($_POST['property']['checkin_time'])) : '00:00:00'),
        'checkout_day'              => $_uccms_properties->toFloat($_POST['property']['checkout_day'], false),
        'checkout_time'             => ($_POST['property']['checkout_time'] ? date('H:i:s', strtotime($_POST['property']['checkout_time'])) : '00:00:00'),
    );


    //echo print_r($columns);
    //exit;

    // HAVE PROPERTY ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";

    // NO ITEM ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW(), `dt_updated`=NOW()";

    }

    //echo $query;
    //exit;

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Property', 'Property added!');
        } else {
            $admin->growl('Property', 'Property updated!');
        }

    // QUERY FAILED
    } else {
        $admin->growl('Property', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id);

?>