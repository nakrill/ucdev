<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['property_id'];

    // HAVE PROPERTY ID
    if ($id) {

        $columns = array();

        // EXPIRATION DATE SPECIFIED
        if ($_POST['property']['payment_expires']) {

            // FORMAT
            $columns['payment_expires'] = date('Y-m-d', strtotime($_POST['property']['payment_expires']));

            /*
            // IS EXPIRED
            if (strtotime($columns['payment_expires']) < time()) {
                $columns['status'] = 0;
            }
            */

        } else {
            $columns['payment_expires'] = '0000-00-00';
        }

        // UPDATE PROPERTY
        $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";
        if (sqlquery($query)) {
            $admin->growl('Billing', 'Updated');
        } else {
            $admin->growl('Billing', 'Failed to update.');
        }

    // NO PROPERTY ID
    } else {
        $admin->growl('Payment', 'Property ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id);

?>