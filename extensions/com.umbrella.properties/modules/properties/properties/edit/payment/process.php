<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['property_id'];

    // HAVE PROPERTY ID
    if ($id) {

        // FORMAT AMOUNT
        $amount = $_uccms_properties->toFloat($_POST['amount']);

        // HAVE REQUIRED INFO
        if (($amount > 0.00) && ($_POST['payment']['method'])) {

            // GET PROPERTY INFO
            $property_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`id`=" .$id. ")";
            $property_q = sqlquery($property_query);
            $property = sqlfetch($property_q);

            // PROPERTY FOUND
            if ($property['id']) {

                unset($customer);
                $customer = array();

                // HAS AGENT
                if ($property['agent_id']) {
                    $query = "SELECT * FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
                    $q = sqlquery($query);
                    $agent = sqlfetch($q);
                    if ($agent['account_id']) {
                        $customer = $agent;
                    }
                }

                // STILL NO CUSTOMER, BUT IS AGENCY
                if ((!$customer['account_id']) && ($property['agency_id'])) {
                    $query = "SELECT `account_id` FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
                    $q = sqlquery($query);
                    $agent = sqlfetch($q);
                    if ($agent['account_id']) {
                        $customer = $agency;
                    }

                }

                /*
                // ORDER DATA FOR REVSOCIAL API
                $rs_orderdata = array(
                    'billing'   => array(
                        'firstname'     => stripslashes($property['billing_firstname']),
                        'lastname'      => stripslashes($property['billing_lastname']),
                        'company'       => stripslashes($property['billing_company']),
                        'address1'      => stripslashes($property['billing_address1']),
                        'address2'      => stripslashes($property['billing_address2']),
                        'city'          => stripslashes($property['billing_city']),
                        'state'         => stripslashes($property['billing_state']),
                        'zip'           => stripslashes($property['billing_zip']),
                        'country'       => stripslashes($property['billing_country']),
                        'phone'         => stripslashes($property['billing_phone']),
                        'email'         => stripslashes($property['billing_email'])
                    ),
                    'shipping'  => array(
                        'firstname'     => stripslashes($property['shipping_firstname']),
                        'lastname'      => stripslashes($property['shipping_lastname']),
                        'company'       => stripslashes($property['shipping_company']),
                        'address1'      => stripslashes($property['shipping_address1']),
                        'address2'      => stripslashes($property['shipping_address2']),
                        'city'          => stripslashes($property['shipping_city']),
                        'state'         => stripslashes($property['shipping_state']),
                        'zip'           => stripslashes($property['shipping_zip']),
                        'country'       => stripslashes($property['shipping_country']),
                        'phone'         => stripslashes($property['shipping_phone']),
                        'email'         => stripslashes($property['shipping_email'])
                    ),
                    'ip'                => long2ip($property['ip'])
                );

                // QUANTITY COUNT
                $rs_items['num_products'] = count($citems);
                $rs_items['num_quantity'] = 0;

                // ADD TO PRODUCTS ARRAY
                $rs_items['products'][] = array(
                    'sku'       => stripslashes($item['sku']),
                    'name'      => stripslashes($item['title']),
                    'options'   => $options,
                    'quantity'  => $oitem['quantity'],
                    'total'     => $item['other']['total']
                );
                */

                $success = false;

                // PAYING BY CHECK/CASH
                if ($_POST['payment']['method'] == 'check_cash') {

                    // LOG TRANSACTION
                    $_uccms_properties->logTransaction(array(
                        'property_id'   => $property['id'],
                        'status'        => 'charged',
                        'amount'        => $amount,
                        'method'        => $_POST['payment']['method'],
                        'note'          => $_POST['payment'][$_POST['payment']['method']]['note'],
                        'ip'            => ip2long($_SERVER['REMOTE_ADDR'])
                    ));

                    $success = true;
                    $admin->growl('Payment', 'Payment processed.');

                // PAYING OTHER WAY
                } else {

                    $payment_missing = false;

                    // PAYMENT PROFILE SPECIFIED
                    if ($_POST['payment']['profile_id']) {

                        // GET PAYMENT PROFILE
                        $pp = $_uccms['_account']->pp_getProfile((int)$_POST['payment']['profile_id'], $customer['account_id']);

                        // PAYMENT PROFILE FOUND
                        if ($pp['id']) {

                            $payment_method     = 'profile';
                            $payment_name       = stripslashes($pp['name']);
                            $payment_lastfour   = $pp['lastfour'];
                            $payment_expiration = $pp['expires'];

                        // PAYMENT PROFILE NOT FOUND
                        } else {
                            $payment_missing = true;
                            $admin->growl('Payment', 'Payment method not found.');
                        }

                    // USE CREDIT CARD INFO
                    } else {

                        // PAYMENT METHOD
                        $payment_method = $_POST['payment']['method'];

                        // REQUIRE PAYMENT FIELDS
                        $reqfa = array(
                            'name'          => 'Name On Card',
                            'number'        => 'Card Number',
                            'expiration'    => 'Card Expiration',
                            'zip'           => 'Card Zip',
                            'cvv'           => 'Card CVV'
                        );

                        // CHECK REQUIRED
                        foreach ($reqfa as $name => $title) {
                            if (!$_uccms_properties->checkRequired($_POST['payment'][$payment_method][$name])) {
                                $payment_missing = true;
                                $admin->growl('Payment', 'Missing / Incorrect: Payment - ' .$title);
                            }
                        }

                        $payment_name = $_POST['payment'][$payment_method]['name'];

                        // PAYMENT LAST FOUR
                        $payment_lastfour = substr(preg_replace("/[^0-9]/", '', $_POST['payment'][$payment_method]['number']), -4);

                        // PAYMENT EXPIRATION DATE FORMATTING
                        if ($_POST['payment'][$payment_method]['expiration']) {
                            $payment_expiration = date('Y-m-d', strtotime(str_replace('/', '/01/', $_POST['payment'][$payment_method]['expiration'])));
                        } else {
                            $payment_expiration = '0000-00-00';
                        }

                    }

                    // NO MISSING FIELDS
                    if (!$payment_missing) {

                        // PAYMENT - CHARGE
                        $payment_data = array(
                            'amount'    => $amount,
                            'customer' => array(
                                'id'            => $customer['account_id'],
                                'name'          => $payment_name,
                                'email'         => $customer['email'],
                                'phone'         => $customer['phone'],
                                'address'       => array(
                                    'street'    => $customer['address1'],
                                    'street2'   => $customer['address2'],
                                    'city'      => $customer['city'],
                                    'state'     => $customer['state'],
                                    'zip'       => $customer['zip'],
                                    'country'   => $customer['city']
                                ),
                                'description'   => ''
                            ),
                            'note'  => 'Payment for property ID: ' .$property['id']
                        );

                        // HAVE PAYMENT PROFILE
                        if ($pp['id']) {
                            $payment_data['card'] = array(
                                'id'            => $pp['id'],
                                'name'          => $payment_name,
                                'number'        => $payment_lastfour,
                                'expiration'    => $payment_expiration
                            );

                        // USE CARD
                        } else {
                            $payment_data['card'] = array(
                                'name'          => $payment_name,
                                'number'        => $_POST['payment'][$payment_method]['number'],
                                'expiration'    => $payment_expiration,
                                'zip'           => $_POST['payment'][$payment_method]['zip'],
                                'cvv'           => $_POST['payment'][$payment_method]['cvv']
                            );
                        }

                        // PROCESS PAYMENT
                        $payment_result = $_uccms['_account']->payment_charge($payment_data);

                        // TRANSACTION ID
                        $transaction_id = $payment_result['transaction_id'];

                        // TRANSACTION MESSAGE (ERROR)
                        $transaction_message = $payment_result['error'];

                    // PAYMENT INFO MISSING
                    } else {
                        BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$property['id']);
                        exit;
                    }

                    // LOG TRANSACTION
                    $_uccms_properties->logTransaction(array(
                        'account_id'            => $customer['account_id'],
                        'property_id'           => $property['id'],
                        'status'                => ($transaction_id ? 'charged' : 'failed'),
                        'amount'                => $amount,
                        'method'                => $_POST['payment']['method'],
                        'payment_profile_id'    => $payment_result['profile_id'],
                        'name'                  => $_POST['payment'][$_POST['payment']['method']]['name'],
                        'lastfour'              => $payment_lastfour,
                        'expiration'            => $payment_expiration,
                        'transaction_id'        => $transaction_id,
                        'message'               => $transaction_message,
                        'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                    ));

                    // CHARGE SUCCESSFUL
                    if ($transaction_id) {

                        // ADD TO INFO
                        $property['transaction_id'] = $transaction_id;

                        /*
                        $columns = array(
                            'status'    => 1
                        );

                        if ($amount > 0.00) {
                            $columns['paying'] = 1;
                        }

                        // EXPIRATION LESS THAN TODAY
                        if (strtotime($property['payment_expires']) < time()) {
                            $columns['payment_expires'] = date('Y-m-d', strtotime('+' .$days. ' Days'));
                        } else {
                            if ($pricing['listing_price'] > 0.00) { // PAID AN AMOUNT
                                $columns['payment_expires'] = date('Y-m-d', strtotime('+' .$days. ' Days', strtotime($property['payment_expires'])));
                            }
                        }

                        // UPDATE PROPERTY
                        $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$property['id']. ")";
                        if (sqlquery($query)) {
                            $_uccms['_site-message']->set('success', 'Listing is now active!');
                        } else {
                            $_uccms['_site-message']->set('error', 'Failed to make listing active.');
                        }
                        */

                        $success = true;
                        $admin->growl('Payment', 'Payment processed.');

                    // CHARGE FAILED
                    } else {

                        /*
                        // UPDATE PROPERTY
                        $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$property['id']. ")";
                        if (sqlquery($query)) {
                            $_uccms['_site-message']->set('success', 'Listing is now active!');
                        } else {
                            $_uccms['_site-message']->set('error', 'Failed to make listing active.');
                        }
                        */

                        $admin->growl('Payment', 'Payment failed. (' .$gateway->Message. ')');

                    }

                }

                // WAS SUCCESS
                if ($success) {

                    /*
                    // EMAIL SETTINGS
                    $esettings = array(
                        'template'      => 'admin_payment-processed.html',
                        'subject'       => 'Payment Processed',
                        'order_id'      => $property['id'],
                        'from_name'     => stripslashes($_uccms_properties->getSetting('email_from_name')),
                        'from_email'    => stripslashes($_uccms_properties->getSetting('email_from_email')),
                        'to_name'       => trim(stripslashes($property['billing_firstname']. ' ' .$property['billing_lastname'])),
                        'to_email'      => stripslashes($property['billing_email']),
                        'sent_by'       => 0
                    );

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'order_id'              => $property['id'],
                        'order_hash'            => $property['hash'],
                        'firstname'             => stripslashes($property['billing_firstname']),
                        'amount'                => number_format($_uccms_properties->toFloat($amount), 2),
                        'order_link'            => WWW_ROOT . $_uccms_properties->storePath(). '/order/review/?id=' .$property['id']. '&h=' .stripslashes($property['hash'])
                    );

                    // SEND EMAIL
                    $eresult = $_uccms_properties->sendEmail($esettings, $evars);

                    if ($payment_method == 'check_cash') {
                        $pm = 'Check / Cash';
                    } else {
                        $pm = 'credit card ending in ' .$payment_lastfour;
                    }

                    // LOG MESSAGE
                    $_uccms_properties->logQuoteMessage(array(
                        'order_id'      => $property['id'],
                        'by'            => 0,
                        'message'       => 'Paid $' .number_format($amount, 2). ' with ' .$pm. '.',
                        'status'        => 'charged',
                        'email_log_id'  => $eresult['log_id']
                    ));
                    */

                    /*
                    // ORDER DATA FOR REVSOCIAL API
                    $rs_orderdata['order']          = $property;
                    $rs_orderdata['order']['total'] = number_format($_uccms_properties->toFloat($property['order_total']), 2);
                    $rs_orderdata['order']['paid']  = number_format($_uccms_properties->toFloat($amount), 2);
                    $rs_orderdata['items']          = $rs_items;

                    // RECORD ORDER WITH REVSOCIAL
                    $_uccms_properties->revsocial_recordOrder($property['id'], $property['customer_id'], $rs_orderdata, true);
                    */

                }

            // PROPERTY NOT FOUND
            } else {
                $admin->growl('Payment', 'Property not found.');
            }

        // MISSING
        } else {
            $admin->growl('Payment', 'Amount and method required.');
        }

    // NO PROPERTY ID
    } else {
        $admin->growl('Payment', 'Property ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'properties/edit/?id=' .$id);

?>