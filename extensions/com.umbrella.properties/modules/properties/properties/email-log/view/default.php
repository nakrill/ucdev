<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET LOG INFO
    $log_query = "SELECT * FROM `" .$_uccms_properties->tables['property_email_log']. "` WHERE (`id`=" .$id. ")";
    $log_q = sqlquery($log_query);
    $log = sqlfetch($log_q);

    // LOG FOUND
    if ($log['id']) {

        // GET PROPERTY
        $prop = $_uccms_properties->getProperty($log['property_id'], false, 'none');

    }

}

// LOG NOT FOUND
if (!$log['id']) {
    $admin->growl('Email Log', 'Could not find log.');
    BigTree::redirect(MODULE_ROOT. 'properties/email-log/');
}

?>

<div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

    <div class="container legacy">

        <header>
            <h2>Email Log</h2>
        </header>

        <section>

            <div class="contain">

                <table style="width: auto;">
                    <tr>
                        <td>Property: </td>
                        <td>
                            <a href="../../edit/?id=<?php echo $prop['id']; ?>">ID: <?php echo stripslashes($prop['id']); ?></a>
                            <?php
                            if ($prop['title']) {
                                echo ' ' .stripslashes($prop['title']);
                            } else if ($prop['address1']) {
                                echo ' ' .stripslashes($prop['address1']);
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Date: </td>
                        <td><?php echo date('n/j/Y g:i A', strtotime($log['dt'])); ?></td>
                    </tr>
                    <tr>
                        <td>Logs: </td>
                        <td><a href="../?property_id=<?php echo $prop['id']; ?>">all logs for property</a></td>
                    </tr>
                </table>

            </div>

            <div class="contain">

                <?php

                // GET SUBMITTED DATA
                $data = json_decode(str_replace("\n", '<br />', stripslashes($log['data'])), true);

                // HAVE DATA
                if (count($data) > 0) {

                    ?>

                    <table>

                        <?php

                        foreach ($data as $field_name => $field_value) {
                            ?>
                            <tr>
                                <td style="white-space: nowrap; vertical-align: top;"><?php echo $field_name; ?>: </td>
                                <td style="width: 100%;"><?php echo $field_value; ?></td>
                            </tr>
                            <?php
                        }

                        ?>

                    </table>

                    <?php

                // NO DATA
                } else {
                    echo 'No data submitted.';
                }

                ?>

            </div>

        </section>

        <footer>
            <a class="button back" href="../">&laquo; Back</a>
        </footer>

    </div>

</div>