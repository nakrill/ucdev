<style type="text/css">

    #items header span, #items .item section {
        text-align: left;
    }

    #items .log_date {
        width: 150px;
    }
    #items .log_property {
        width: 200px;
    }
    #items .log_data {
        width: 480px;
    }
    #items .log_view {
        width: 55px;
        text-align: center;
    }
    #items .log_delete {
        width: 55px;
        text-align: center;
    }

</style>

<? /*
<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#properties .status select').change(function() {
            $('#form_properties').submit();
        });

    });

</script>
*/ ?>

<div id="logs">

    <form id="form_logs">
    <input type="hidden" name="query" value="<?=$_GET['query']?>" />

    <? /*
    <div class="status contain">
        <div style="float: right;">
            <select name="status">
                <option value="">All</option>
                <option value="active" <?php if ($_REQUEST['status'] == 'active') { ?>selected="selected"<?php } ?>>Active</option>
                <option value="inactive" <?php if ($_REQUEST['status'] == 'inactive') { ?>selected="selected"<?php } ?>>Inactive</option>
                <option value="submitted" <?php if ($_REQUEST['status'] == 'submitted') { ?>selected="selected"<?php } ?>>Submitted</option>
            </select>
        </div>
    </div>
    */ ?>

    <div class="search_paging contain">
        <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </div>

    <div id="items" class="table">
        <summary>
            <h2>Email Logs</h2>
        </summary>
        <header style="clear: both;">
            <span class="log_date">Date</span>
            <span class="log_property">Property</span>
            <span class="log_data">Data</span>
            <span class="log_view">View</span>
            <span class="log_delete">Delete</span>
        </header>
        <ul id="results" class="items">
            <? include(EXTENSION_ROOT. 'ajax/admin/properties/email-log/get-page.php'); ?>
        </ul>
    </div>

    </form>

</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_properties->Extension?>/ajax/admin/properties/email-log/get-page/?page=1&status=" +escape($('#properties .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/<?=$_uccms_properties->Extension?>/ajax/admin/properties/email-log/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&status=" +escape($('#properties .status select').val())+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>");
        return false;
    });
</script>