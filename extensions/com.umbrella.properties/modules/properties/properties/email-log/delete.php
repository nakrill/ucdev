<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // UPDATE LOG
    $query = "UPDATE `" .$_uccms_properties->tables['property_email_log']. "` SET `deleted_dt`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Email Log', 'Email log deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Email Log', 'Failed to delete email log.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Email Log', 'No log specified.');
}

BigTree::redirect(MODULE_ROOT.'properties/email-log/');

?>