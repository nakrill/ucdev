<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Attributes', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #groups .group_title {
        width: 610px;
    }
    #groups .group_attributes {
        width: 120px;
    }
    #groups .group_status {
        width: 100px;
    }
    #groups .group_edit {
        width: 55px;
    }
    #groups .group_delete {
        width: 55px;
    }

</style>

<div id="groups" class="table">
    <summary>
        <h2>Groups</h2>
        <a class="add_resource add" href="./group/"><span></span>Add Group</a>
    </summary>
    <header style="clear: both;">
        <span class="group_title">Title</span>
        <span class="group_attributes">Attributes</span>
        <span class="group_status">Status</span>
        <span class="group_edit">Edit</span>
        <span class="group_delete">Delete</span>
    </header>
    <ul class="items">

        <?php

        // GET GROUPS
        $group_query = "SELECT * FROM `" .$_uccms_properties->tables['attribute_groups']. "` ORDER BY `sort`, `title` ASC, `id` ASC";
        $group_q = sqlquery($group_query);

        // LOOP
        while ($group = sqlfetch($group_q)) {

            // GET NUMBER OF ATTRIBUTES
            $attr_query = "SELECT `id` FROM `" .$_uccms_properties->tables['attributes']. "` WHERE (`group_id`=" .$group['id']. ")";
            $attr_q = sqlquery($attr_query);
            $num_attr = sqlrows($attr_q);

            ?>

            <li class="item">
                <section class="group_title">
                    <a href="./group/?id=<?php echo $group['id']; ?>"><?php echo stripslashes($group['title']); ?></a>
                </section>
                <section class="group_attributes">
                    <?php echo number_format($num_attr, 0); ?>
                </section>
                <section class="group_status status_<?php if ($group['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php if ($group['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                </section>
                <section class="group_edit">
                    <a class="icon_edit" title="Edit Group" href="./group/?id=<?php echo $group['id']; ?>"></a>
                </section>
                <section class="group_delete">
                    <a href="./group/delete/?id=<?php echo $group['id']; ?>" class="icon_delete" title="Delete Group" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?\nAttributes in this group will not be deleted.');"></a>
                </section>
            </li>

            <?php

        }

        ?>

    </ul>
</div>