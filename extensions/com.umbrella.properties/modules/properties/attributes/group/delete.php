<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE PROPERTY / GROUP RELATIONS
    $query = "DELETE FROM `" .$_uccms_properties->tables['property_attribute_groups']. "` WHERE (`group_id`=" .$id. ")";
    sqlquery($query);

    // DELETE ATTRIBUTES
    $query = "DELETE FROM `" .$_uccms_properties->tables['attributes']. "` WHERE (`group_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['attribute_groups']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Attribute Group', 'Attribute group deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Attribute Group', 'Failed to delete attribute group.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Attribute Group', 'No attribute group specified.');
}

BigTree::redirect(MODULE_ROOT.'items/attributes/');

?>