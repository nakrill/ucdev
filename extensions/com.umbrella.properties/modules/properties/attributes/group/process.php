<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['group']['id'];

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['group']['active'],
        'title'         => $_POST['group']['title']
    );

    // HAVE ATTRIBUTE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_properties->tables['attribute_groups']. "` SET " .$_uccms_properties->createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO ATTRIBUTE ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_properties->tables['attribute_groups']. "` SET " .$_uccms_properties->createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Attribute Group', 'Saved!');

        // PROPERTY TYPE ADD ARRAY
        $ptaa = array();

        // HAVE SUBMITTED PROPERTY TYPES
        if (is_array($_POST['property_type'])) {
            foreach ($_POST['property_type'] as $pt) {
                $ptaa[$pt] = $pt;
            }
        }

        // GET PROPERTY TYPES
        $pta = $_uccms_properties->attributeGroupPropertyTypes($id);

        // HAVE PROPERTY TYPES FOR GROUP
        if (count($pta) > 0) {

            // LOOP
            foreach ($pta as $key => $pt) {

                // NO LONGER SELECTED
                if (!$ptaa[$pt]) {

                    // DELETE RELATION
                    $query = "DELETE FROM `" .$_uccms_properties->tables['attribute_groups_property_types']. "` WHERE (`group_id`=" .$id. ") AND (`property_type_code`='" .$pt. "')";
                    sqlquery($query);

                }

                unset($ptaa[$pt]);

            }
        }

        // STILL HAVE ONES TO ADD
        if (count($ptaa) > 0) {

            // LOOP
            foreach ($ptaa as $pt) {

                // ADD RELATION
                $query = "INSERT INTO `" .$_uccms_properties->tables['attribute_groups_property_types']. "` SET `group_id`=" .$id. ", `property_type_code`='" .$pt. "'";
                sqlquery($query);

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Attribute Group', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'attributes/group/?id=' .$id);

?>