<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $group_query = "SELECT * FROM `" .$_uccms_properties->tables['attribute_groups']. "` WHERE (`id`=" .$id. ")";
    $group_q = sqlquery($group_query);
    $group = sqlfetch($group_q);

}

// GET PROPERTY TYPES
$pta = $_uccms_properties->attributeGroupPropertyTypes($group['id']);

?>


<style type="text/css">

    #attributes .contain .contain {
        padding-bottom: 10px;
    }

    #attributes .contain .contain:last-child {
        padding-bottom: 0px;
    }

    #attributes .contain .contain .checkbox {
        margin-top: -2px;
    }

</style>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="group[id]" value="<?php echo $group['id']; ?>" />

<div class="container legacy">

    <header>
        <h2><?php if ($group['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Group</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>Title</label>
                <input type="text" name="group[title]" value="<?php echo stripslashes($group['title']); ?>" />
            </fieldset>

            <fieldset>
                <input type="checkbox" name="group[active]" value="1" <?php if ($group['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

        </div>

        <div class="right last">

            <fieldset>
                <label>Property Type(s)</label>
                <?php foreach ($_uccms_properties->propertyTypes() as $type_code => $type_title) { ?>
                    <div style="margin-bottom: 3px;">
                        <input type="checkbox" name="property_type[<?php echo $type_code; ?>]" value="<?php echo $type_code; ?>" <?php if ($pta[$type_code]) { ?>checked="checked"<?php } ?>>
                        <label class="for_checkbox"><?php echo $type_title; ?></label>
                    </div>
                <?php } ?>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

</div>

</form>

<?php if ($group['id']) { ?>

    <style type="text/css">

        #attributes .attribute_title {
            width: 610px;
        }
        #attributes .attribute_type {
            width: 120px;
        }
        #attributes .attribute_status {
            width: 100px;
        }
        #attributes .attribute_edit {
            width: 55px;
        }
        #attributes .attribute_delete {
            width: 55px;
        }

    </style>

    <div id="attributes" class="table">
        <summary>
            <h2>Attributes</h2>
            <a class="add_resource add" href="../edit/?group_id=<?php echo $group['id']; ?>"><span></span>Add Attribute</a>
        </summary>
        <header style="clear: both;">
            <span class="attribute_title">Title</span>
            <span class="attribute_type">Type</span>
            <span class="attribute_status">Status</span>
            <span class="attribute_edit">Edit</span>
            <span class="attribute_delete">Delete</span>
        </header>
        <ul class="items">

            <?php

            // GET ATTRIBUTES
            $attribute_query = "SELECT * FROM `" .$_uccms_properties->tables['attributes']. "` WHERE (`group_id`=" .$group['id']. ") ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $attribute_q = sqlquery($attribute_query);

            // LOOP
            while ($attribute = sqlfetch($attribute_q)) {

                if ($attribute['type'] == 'dt') {
                    $type = 'Date & Time';
                } else {
                    $type = ucwords(str_replace('_', ' ', stripslashes($attribute['type'])));
                }

                ?>

                <li class="item">
                    <section class="attribute_title">
                        <a href="./edit/?id=<?php echo $attribute['id']; ?>"><?php echo stripslashes($attribute['title']); ?></a>
                    </section>
                    <section class="attribute_type">
                        <?php echo $type; ?>
                    </section>
                    <section class="attribute_status status_<?php if ($attribute['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                        <?php if ($attribute['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                    </section>
                    <section class="attribute_edit">
                        <a class="icon_edit" title="Edit Attribute" href="../edit/?id=<?php echo $attribute['id']; ?>"></a>
                    </section>
                    <section class="attribute_delete">
                        <a href="../delete/?id=<?php echo $attribute['id']; ?>&group_id=<?php echo $group['id']; ?>" class="icon_delete" title="Delete Attribute" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                    </section>
                </li>

                <?php

            }

            ?>

        </ul>
    </div>

<?php } ?>