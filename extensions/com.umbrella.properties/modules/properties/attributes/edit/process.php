<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['attribute']['id'];

    // REQUIRED
    if ($_POST['attribute']['required_checkbox']) {
        $required = $_POST['attribute']['required'];
    } else {
        $required = '';
    }

    // FILE TYPES
    if ($_POST['attribute']['file_types']) {
        $ffta = array();
        $tfta = explode(',', $_POST['attribute']['file_types']);
        foreach ($tfta as $ft) {
            $ffta[] = trim(str_replace('.', '', $ft));
        }
        if (count($ffta) > 0) {
            $file_types = implode(',', $ffta);
        }
    }

    // DB COLUMNS
    $columns = array(
        'group_id'      => (int)$_POST['attribute']['group_id'],
        'active'        => (int)$_POST['attribute']['active'],
        'title'         => $_POST['attribute']['title'],
        'description'   => $_POST['attribute']['description'],
        'type'          => $_POST['attribute']['type'],
        'limit'         => (int)$_POST['attribute']['limit'],
        'required'      => $required,
        'file_types'    => $file_types
    );

    // HAVE ATTRIBUTE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_properties->tables['attributes']. "` SET " .$_uccms_properties->createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO ATTRIBUTE ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_properties->tables['attributes']. "` SET " .$_uccms_properties->createSet($columns). "";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Attribute', 'Saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Attribute', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'attributes/edit/?id=' .$id);

?>