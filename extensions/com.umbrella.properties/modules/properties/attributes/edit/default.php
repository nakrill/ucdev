<?php

// CLEAN UP
$id         = (int)$_REQUEST['id'];
$group_id   = (int)$_REQUEST['group_id'];

// HAVE ID
if ($id) {

    $aoa = array();

    // GET INFO FROM DB
    $attribute_query = "SELECT * FROM `" .$_uccms_properties->tables['attributes']. "` WHERE (`id`=" .$id. ")";
    $attribute_q = sqlquery($attribute_query);
    $attribute = sqlfetch($attribute_q);

    if ($attribute['group_id']) $group_id = $attribute['group_id'];

    // ATTRIBUTE OPTION ARRAY
    if ($attribute['options']) {
        $aoa = json_decode($attribute['options'], true);
    }

}

?>


<style type="text/css">

    #options header {
        clear: both;
    }

    #options.table > ul li {
        height: auto;
        padding: 5px 0;
        line-height: inherit;
    }

    #options.table > ul li section {
        height: auto;
        overflow: visible;
    }

    #options .option_sort {
        width: 55px;
    }
    #options .option_title {
        width: 680px;
        text-align: left;
    }
    #options .option_default {
        width: 150px;
        text-align: left;
    }
    #options .option_delete {
        width: 55px;
    }

    #options .option_default .radio_button {
        margin-top: 3px;
        margin-left: 12px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // TYPE SELECT
        $('#select_type').change(function(e) {
            if ($(this).val() == 'checkbox') {
                $('#attribute .limit').show();
            } else {
                $('#attribute .limit').hide();
            }
            if ($(this).val() == 'file') {
                $('#attribute .file_types').show();
            } else {
                $('#attribute .file_types').hide();
            }
        });

        // REQUIRED TOGGLE
        $('#required').change(function(e) {
            if ($(this).prop('checked')) {
                if ($('#select_type').val() != 'file') {
                    $('#toggle_required').show();
                }
            } else {
                $('#toggle_required').hide();
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="attribute[id]" value="<?php echo $attribute['id']; ?>" />

<div id="attribute" class="container legacy">

    <header>
        <h2><?php if ($attribute['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Attribute</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>Title</label>
                <input type="text" name="attribute[title]" value="<?php echo stripslashes($attribute['title']); ?>" />
            </fieldset>

            <fieldset>
                <label>Description</label>
                <textarea name="attribute[description]" style="height: 60px;"><?php echo stripslashes($attribute['description']); ?></textarea>
            </fieldset>

        </div>

        <div class="right last">

            <div class="contain">

                <div class="left inner_quarter type">
                    <fieldset>
                        <label>Group</label>
                        <select name="attribute[group_id]">
                            <?php
                            $group_query = "SELECT * FROM `" .$_uccms_properties->tables['attribute_groups']. "` ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                            $group_q = sqlquery($group_query);
                            while ($group = sqlfetch($group_q)) {
                                ?>
                                <option value="<?php echo $group['id']; ?>" <?php if ($group['id'] == $group_id) { ?>selected="selected"<?php } ?>><?php echo stripslashes($group['title']); ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </fieldset>
                </div>

                <div class="left inner_quarter type">
                    <fieldset>
                        <label>Type</label>
                        <select id="select_type" name="attribute[type]">
                            <option value="dropdown" <?php if ($attribute['type'] == 'dropdown') { ?>selected="selected"<?php } ?>>Dropdown</option>
                            <option value="checkbox" <?php if ($attribute['type'] == 'checkbox') { ?>selected="selected"<?php } ?>>Checkbox(s)</option>
                            <option value="select_box" <?php if ($attribute['type'] == 'select_box') { ?>selected="selected"<?php } ?>>Select Box(s)</option>
                            <option value="text" <?php if ($attribute['type'] == 'text') { ?>selected="selected"<?php } ?>>Text</option>
                            <option value="textarea" <?php if ($attribute['type'] == 'textarea') { ?>selected="selected"<?php } ?>>Textarea</option>
                            <option value="date" <?php if ($attribute['type'] == 'date') { ?>selected="selected"<?php } ?>>Date</option>
                            <option value="time" <?php if ($attribute['type'] == 'time') { ?>selected="selected"<?php } ?>>Time</option>
                            <option value="file" <?php if ($attribute['type'] == 'file') { ?>selected="selected"<?php } ?>>File</option>
                        </select>
                    </fieldset>
                </div>

                <? /*
                <div class="right inner_quarter limit" style="<?php if ($attribute['type'] != 'checkbox') { ?>display: none;<?php } ?>">
                    <fieldset>
                        <label>Limit</label>
                        <input type="text" name="attribute[limit]" value="<?php echo (int)$attribute['limit']; ?>" class="smaller_60" />
                        <label><small>The number of options a person may select. 0 for no limit.</small></label>
                    </fieldset>
                </div>
                */ ?>

                <div class="right inner_quarter file_types" style="<?php if ($attribute['type'] != 'file') { ?>display: none;<?php } ?>">
                    <fieldset>
                        <label>Allowed File Types</label>
                        <input type="text" name="attribute[file_types]" value="<?php echo $attribute['file_types']; ?>" style="width: 180px;" />
                        <label><small>Separate by comma. Ex: jpg, png, pdf</small></label>
                    </fieldset>
                </div>

            </div>

            <fieldset>
                <input type="checkbox" name="attribute[active]" value="1" <?php if ($attribute['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

            <? /*
            <fieldset>
                <input id="required" type="checkbox" name="attribute[required_checkbox]" value="1" <?php if ($attribute['required']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Required</label>
            </fieldset>

            <fieldset id="toggle_required" style="<?php if ((!$attribute['required']) || ($attribute['type'] == 'file')) { ?>display: none;<?php } ?>">
                <label>Required Type</label>
                <select id="select_type" name="attribute[required]">
                    <option value="general" <?php if ($attribute['required'] == 'general') { ?>selected="selected"<?php } ?>>General</option>
                    <option value="number" <?php if ($attribute['required'] == 'number') { ?>selected="selected"<?php } ?>>Number</option>
                    <option value="email" <?php if ($attribute['required'] == 'email') { ?>selected="selected"<?php } ?>>Email</option>
                    <option value="url" <?php if ($attribute['required'] == 'url') { ?>selected="selected"<?php } ?>>URL</option>
                </select>
            </fieldset>
            */ ?>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <?php if ($group_id) { ?><a class="button back" href="../group/?id=<?php echo $group_id; ?>">&laquo; Back</a><?php } ?>
    </footer>

</div>

</form>

<?php if (($attribute['id']) && ($attribute['type'] != 'file')) { ?>

    <script type="text/javascript">

        $(document).ready(function() {

            var newOptionKey = 1;

            // OPTIONS - SORTABLE
            $('#options ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                }
            });

            // OPTIONS - ADD
            $('#options .add_resource').click(function(e) {
                e.preventDefault();
                var key = 'new-' +newOptionKey;
                newOptionKey++;
                var el = $('#option-new').clone();
                el.html(el.html().replace(/###/g, key)).removeAttr('id').appendTo('#options ul.ui-sortable').show();
                //var radio = el.find('input[type=radio]');
                //radio.customControl = BigTreeRadioButton(radio);
            });

            // OPTIONS - DEFAULT RADIO CLICK
            $('#options .items .item .option_default input').click(function(e) {
                console.log('click');
                var key = $(this).attr('data-key');
                $('#options .items .item .option_default input').each(function() {
                    if ($(this).attr('data-key') != key) {
                        $(this).removeAttr('checked');
                        $(this).next('.radio_button').find('a').removeClass('checked');
                    }
                });
            });

            // OPTIONS - DELETE
            $('#options .items').on('click', '.item .icon_delete', function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    $(this).closest('.item').fadeOut(500, function() {
                        $(this).remove();
                    });
                }
            });

        });

    </script>

    <a name="options"></a>

    <div class="container legacy" style="border: 0px none;">

        <form enctype="multipart/form-data" action="./process-options/" method="post">
        <input type="hidden" name="attribute[id]" value="<?php echo $attribute['id']; ?>" />

        <div id="options" class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Options</h2>
                <a class="add_resource add" href="#"><span></span>Add Option</a>
            </summary>

            <header>
                <span class="option_sort"></span>
                <span class="option_title">Name</span>
                <span class="option_default">Default</span>
                <span class="option_delete">Delete</span>
            </header>

            <ul class="items ui-sortable">

                <?php

                foreach ($aoa as $key => $option) {

                    if ($option['markup']) {
                        number_format($option['markup'], 2);
                    } else {
                        $option['markup'] = '';
                    }

                    ?>

                    <li class="item contain">
                        <section class="option_sort sort">
                            <span class="icon_sort ui-sortable-handle"></span>
                        </section>
                        <section class="option_title">
                            <input type="text" name="option[<?php echo $key; ?>][title]" value="<?php echo stripslashes($option['title']); ?>" placeholder="Name" style="width: 300px;" />
                        </section>
                        <section class="option_default">
                            <input type="radio" name="option[<?php echo $key; ?>][default]" value="1" data-key="<?php echo $key; ?>" <?php if ($option['default']) { ?>checked="checked"<?php } ?> />
                        </section>
                        <section class="option_delete view_action">
                            <a class="icon_delete" href="#"></a>
                        </section>
                    </li>

                    <?php

                }

                ?>

            </ul>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

        </div>

        </form>

        <li id="option-new" class="item contain" style="display: none;">
            <section class="option_sort sort">
                <span class="icon_sort ui-sortable-handle"></span>
            </section>
            <section class="option_title">
                <input type="text" name="option[###][title]" value="" placeholder="Name" style="width: 300px;" />
            </section>
            <section class="option_default">
                <input type="radio" name="option[###][default]" value="1" data-key="###" />
            </section>
            <section class="option_delete view_action">
                <a class="icon_delete" href="#"></a>
            </section>
        </li>

    </div>

<?php } ?>