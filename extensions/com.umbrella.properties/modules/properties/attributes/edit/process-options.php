<?php

// CLEAN UP
$id = (int)$_POST['attribute']['id'];

// HAVE ATTRIBUTE ID
if ($id) {

    // OPTIONS
    if (count($_POST['option']) > 0) {
        $i = 1;
        foreach ($_POST['option'] as $option) {
            $oa[$i] = $option;
            $i++;
        }
        $options = json_encode($oa);
    } else {
        $options = '';
    }

    // DB QUERY
    $query = "UPDATE `" .$_uccms_properties->tables['attributes']. "` SET `options`='" .$options. "' WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Attribute Options', 'Saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Attribute Options', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'attributes/edit/?id=' .$id. '#options');

?>