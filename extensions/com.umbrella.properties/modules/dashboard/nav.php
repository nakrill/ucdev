<?php

$extension_nav =
["link" => "com.umbrella.properties*properties", "title" => "Properties", "access" => 1, "children" => [
    ["link" => ".", "title" => "Dashboard", "access" => 1],
    ["link" => "agencies", "title" => "Agencies", "access" => 1],
    ["link" => "locations", "title" => "Locations", "access" => 1],
    ["link" => "properties", "title" => "Properties", "access" => 1],
    ["link" => "attributes", "title" => "Attributes", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>