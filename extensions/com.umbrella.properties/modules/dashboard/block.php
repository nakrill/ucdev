<?php

// MODULE CLASS
$_uccms_properties = new uccms_Properties;

// HAS ACCESS
if ($_uccms_properties->adminModulePermission()) {

    ?>

    <style type="text/css">

        #block_properties #latest_properties header span, #block_properties #latest_properties .item section {
            text-align: left;
        }

        #block_properties #latest_properties .property_street {
            width: 260px;
        }
        #block_properties #latest_properties .property_city {
            width: 155px;
        }
        #block_properties #latest_properties .property_price {
            width: 150px;
        }
        #block_properties #latest_properties .property_agent {
            width: 165px;
        }
        #block_properties #latest_properties .property_status {
            width: 100px;
        }
        #block_properties #latest_properties .property_edit {
            width: 55px;
        }
        #block_properties #latest_properties .property_delete {
            width: 55px;
        }

    </style>

    <div id="block_properties" class="block">
        <h2><a href="../<?php echo $extension; ?>*properties/">Properties</a></h2>
        <div id="latest_properties" class="table">
            <summary>
                <h2>Latest properties</h2>
                <a class="add_resource add" href="../<?php echo $extension; ?>*properties/properties/">
                    View All
                </a>
            </summary>
            <header style="clear: both;">
                <span class="property_street">Name / Street</span>
                <span class="property_city">City</span>
                <span class="property_price">Price</span>
                <span class="property_agent">Agent</span>
                <span class="property_status">Status</span>
                <span class="property_edit">Edit</span>
                <span class="property_delete">Delete</span>
            </header>
            <ul id="results" class="items">
                <?php
                $_GET['base_url'] = '../' .$extension. '*properties/properties';
                $_GET['limit'] = 5;
                include($extension_dir. '/ajax/admin/properties/get-page.php');
                ?>
            </ul>
        </div>
    </div>

    <?php

}

unset($_uccms_properties);

?>