<?
    class uccms_Properties extends BigTreeModule {

        var $Table = "";

        var $Extension = "com.umbrella.properties";

        // DB TABLES
        var $tables = array(
            'agencies'                          => 'uccms_properties_agencies',
            'agents'                            => 'uccms_properties_agents',
            'attribute_groups'                  => 'uccms_properties_attribute_groups',
            'attributes'                        => 'uccms_properties_attributes',
            'cities'                            => 'uccms_properties_cities',
            'neighborhoods'                     => 'uccms_properties_neighborhoods',
            'properties'                        => 'uccms_properties_properties',
            'property_attribute_groups'         => 'uccms_properties_property_attribute_group_rel',
            'attribute_groups_property_types'   => 'uccms_properties_attribute_groups_property_types_rel',
            'property_categories'               => 'uccms_properties_property_category_rel',
            'property_email_log'                => 'uccms_properties_email_log',
            'property_images'                   => 'uccms_properties_property_images',
            'property_levels'                   => 'uccms_properties_property_levels',
            'property_rooms'                    => 'uccms_properties_property_rooms',
            'property_stats_log'                => 'uccms_properties_property_stats_log',
            'property_videos'                   => 'uccms_properties_property_videos',
            'categories'                        => 'uccms_properties_categories',
            'payment_methods'                   => 'uccms_properties_settings_payment_methods',
            'settings'                          => 'uccms_properties_settings',
            'transaction_log'                   => 'uccms_properties_transaction_log'
        );

        // PROPERTY STATUSES
        var $statuses = array(
            0 => 'Inactive',
            1 => 'Active',
            4 => 'Draft',
            5 => 'Pending',
            9 => 'Deleted'
        );

        // LISTING TYPES
        var $listingTypes = array(
            1 => 'Sale',
            2 => 'Rental'
        );

        var $settings, $misc;

        // START AS 0, WILL ADJUST LATER
        static $PerPage = 0;


        function __construct() {

        }


        #############################
        # GENERAL - MISC
        #############################


        // FRONTEND PATH
        public function frontendPath() {
            if (!isset($this->settings['frontend_path'])) {
                $this->settings['frontend_path'] = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*properties')"))['path']);
            }
            if (!$this->settings['frontend_path']) $this->settings['frontend_path'] = 'directory';
            return $this->settings['frontend_path'];
        }


        // MODULE ID
        public function moduleID() {
            if (!isset($this->settings['module_id'])) {
                $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
            }
            return $this->settings['module_id'];
        }


        // ADMIN USER ID
        public function adminID() {
            return $_SESSION['bigtree_admin']['id'];
        }


        // ADMIN ACCESS LEVEL
        public function adminModulePermission() {
            global $admin;
            if (!isset($this->settings['admin_module_permission'])) {
                $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
            }
            return $this->settings['admin_module_permission'];
        }


        // STRING IS JSON
        public function isJson($string) {
            if (substr($string, 0, 2) == '{"') {
                return true;
            }
        }


        // CREATE SET
        public function createSet($columns) {
            return uccms_createSet($columns);
        }


        // CONVERTS A STRING INTO A REWRITE-ABLE URL
        public function makeRewrite($string) {

            $string = strtolower(trim($string));

            // REMOVE ALL QUOTES
            $string = str_replace("'", '', $string);
            $string = str_replace('"', '', $string);

            // STRIP ALL NON WORD CHARS
            $string = preg_replace('/\W/', ' ', $string);

            // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
            $string = preg_replace('/\ +/', '-', $string);

            // TRIM DASHES
            $string = preg_replace('/\-$/', '', $string);
            $string = preg_replace('/^\-/', '', $string);

            return $string;

        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
        public function imageBridgeIn($string) {
            $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $dpa = explode('/', $string);
                return array_pop($dpa);
                //$what = array_pop($dpa);
            }
        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
        public function imageBridgeOut($string, $what, $file=false) {
            $string = stripslashes($string);
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $base = ($file ? SITE_ROOT : STATIC_ROOT);
                return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
            }
        }


        // CONVERT BIGTREE FILE LOCATION TO FULL URL
        public function bigtreeFileURL($file) {
            return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
        }


        // SET NUMBER PER PAGE
        public function setPerPage($num) {
            static::$PerPage = (int)$num;
        }


        // NUMBER PER PAGE
        public function perPage() {
            global $cms;
            if (!static::$PerPage) $this->setPerPage($this->getSetting('listings_per_page'));
            if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
            if (!static::$PerPage) $this->setPerPage(15);
            return static::$PerPage;
        }


        // GET NUMBER OF PAGES FROM QUERY
        public function pageCount($num) {
            return ceil($num / $this->perPage());
        }


        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            );
        }


        // DAYS OF WEEK FOR SCHEMA.ORG
        public function daysOfWeek_Schema() {
            return array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            );
        }


        // PAYMENT METHODS
        public function paymentMethods() {
            return array(
                1 => 'All',
                2 => 'Visa',
                3 => 'Mastercard',
                4 => 'American Express',
                5 => 'Discover',
                6 => 'Diners',
                7 => 'Cash',
                8 => 'Check',
                9 => 'Debit'
            );
        }


        // CLEAN A PHONE NUMBER (TO JUST NUMBERS)
        public function cleanPhone($number) {
            return trim(preg_replace('/\D/', '', $number));
        }


        // FORMAT PHONE NUMBER
        public function prettyPhone($number) {
            $number = preg_replace('/\D/', '', $number);
            return '(' .substr($number, 0, 3). ') ' .substr($number, 3, 3). '-' .substr($number, 6);
        }


        // TRIM URL
        public function trimURL($string) {
            return trim(preg_replace('#^https?://#', '', $string));
        }


        // CONVERT ARRAY TO CSV
        public function arrayToCSV($array=array()) {
            if (is_array($array)) {
                return implode(',', $array);
            }
        }


        // RETURN THE FLOAT VAL
        public function toFloat($str, $decimal=true) {
            $str = floatval(preg_replace("/[^-0-9\.]/", '' , $str));
            if ($decimal) {
                $str = number_format($str, 2, '.', '');
            }
            return $str;
        }


        // BREADCRUMB OUTPUT
        public function generateBreadcrumbs($array, $options=array(), $echo_string=true) {

            // OPTIONS
            if (!$options['base']) $options['base'] = './';
            if (!$options['home_title']) $options['home_title'] = 'Properties'; // MATTD: Make into a configurable setting
            if (!$options['sep']) $options['sep'] = ' / '; // MATTD: Make into a configurable setting

            // ELEMENTS
            $els = array();

            // SHOW HOME LINK
            if (!$options['no_home']) {
                $els[] = '<a href="' .$options['base']. '">' .$options['home_title']. '</a>';
            }

            // NUMBER OF PARENT CATEGORIES
            $num_cat = count($array);

            // HAVE PARENT CATEGORIES
            if ($num_cat > 0) {

                // REVERSE ARRAY FOR EASIER OUTPUT
                $tcat = array_reverse($array);

                $i = 1;

                // LOOP
                foreach ($tcat as $cat) {
                    if ($options['rewrite']) {
                        $url = $this->categoryURL($cat['id'], $cat);
                    } else {
                        $url = $options['base']. '?id=' .$cat['id'];
                    }
                    $line = '';
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '<a href="' .$url. '">';
                    }
                    $line .= stripslashes($cat['title']);
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '</a>';
                    }
                    $els[] = $line;
                    $i++;
                }

            }

            if ($echo_string) {
                echo implode($options['sep'], $els);
            } else {
                return $els;
            }

        }


        // FORMAT PROPERTY ADDRESS
        public function formatAddress($prop, $sep='<br />') {
            if ($prop['address1']) $pa[] = stripslashes($prop['address1']);
            if ($prop['address2']) $pa[] = stripslashes($prop['address2']);
            if ($prop['unit']) $pa[] = stripslashes($prop['unit']);
            if (($prop['city']) && ($prop['state'])) {
                $pa2[] = stripslashes($prop['city']). ', ';
            } else if ($prop['city']) {
                $pa2[] = stripslashes($prop['city']);
            }
            if ($prop['state']) $pa2[] = stripslashes($prop['state']);
            if ($prop['zip']) $pa2[] = stripslashes($prop['zip']);
            if (count($pa2) > 0) {
                $pa[] = implode(' ', $pa2);
            }
            if (count($pa) > 0) {
                return implode($sep, $pa);
            }
        }



        #############################
        # GENERAL - SETTINGS
        #############################


        // GET SETTING
        public function getSetting($id='') {
            $id = sqlescape($id);
            if ($id) {
                if (isset($this->settings[$id])) {
                    return $this->settings[$id];
                } else {
                    $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tables['settings']. "` WHERE `id`='" .$id. "'"));
                    if ($this->isJson($f['value'])) {
                        $val = json_decode($f['value'], true);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$id] = $val;
                }
                return $val;
            }
        }


        // GET SETTINGS
        public function getSettings($array='') {
            $out = array();
            if (is_array($array)) {
                $oa = array();
                foreach ($array as $id) {
                    $id = sqlescape($id);
                    if ($id) {
                        $oa[] = "`id`='" .$id. "'";
                    }
                }
                if (count($oa) > 0) {
                    $or = "(" .implode(") OR (", $oa). ")";
                }
                if ($or) {
                    $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` WHERE " .$or;
                }
            } else {
                $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` ORDER BY `id` ASC";
            }
            if ($query) {
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    if ($this->isJson($string)) {
                        $val = json_decode($f['value']);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$f['id']] = $val;
                    $out[$f['id']] = $val;
                }
            }
            return $out;
        }


        // SET SETTING
        public function setSetting($id='', $value='') {

            // CLEAN UP
            $id = sqlescape($id);

            // HAVE ID
            if ($id) {

                // VALUE IS ARRAY
                if (is_array($value)) {
                    $value = json_encode($value);

                // VALUE IS STRING
                } else {
                    $value = sqlescape($value);
                }

                $query = "
                INSERT INTO `" .$this->tables['settings']. "`
                SET `id`='" .$id. "', `value`='" .$value. "'
                ON DUPLICATE KEY UPDATE `value`='" .$value. "'
                ";

                if (sqlquery($query)) {
                    return true;
                }

            }

        }


        #############################
        # ATTRIBUTE GROUP
        #############################


        // GET PROPERTY TYPES FOR GROUP
        public function attributeGroupPropertyTypes($group_id=0) {
            $out = array();
            if ($group_id) {
                $query = "SELECT `property_type_code` FROM `" .$this->tables['attribute_groups_property_types']. "` WHERE (`group_id`=" .(int)$group_id. ")";
                $q = sqlquery($query);
                while ($row = sqlfetch($q)) {
                    $out[$row['property_type_code']] = $row['property_type_code'];
                }
            }
            return $out;
        }


        #############################
        # CITY
        #############################


        // GET CITIES
        public function cities() {
            $out = array();
            $query = "SELECT * FROM `" .$this->tables['cities']. "` ORDER BY `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $out[$row['id']] = $row;
            }
            return $out;
        }


        // GENERATE CITY URL
        public function cityURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            return '/' .$this->frontendPath(). '/' .$data['slug']. '/';
        }


        #############################
        # NEIGHBORHOOD
        #############################


        // GET NEIGHBORHOODS
        public function neighborhoods() {
            $out = array();
            $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` ORDER BY `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $out[$row['id']] = $row;
            }
            return $out;
        }


        // GENERATE NEIGHBORHOOD URL
        public function neighborhoodURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            $city_query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .$data['city_id']. ")";
            $city_data = sqlfetch(sqlquery($city_query));
            return '/' .$this->frontendPath(). '/' .$city_data['slug']. '/' .$data['slug']. '/';
        }


        #############################
        # AGENCY
        #############################


        // GENERATE AGENCY URL
        public function agencyURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['agencies']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/agency/' .$slug. '/';
        }


        #############################
        # AGENT
        #############################


        // GENERATE AGENT URL
        public function agentURL($id, $data=array()) {
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['agents']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            return '/' .$this->frontendPath(). '/agent/' .$data['slug']. '/';
        }


        #############################
        # PROPERTY
        #############################


        // GENERATE PROPERTY URL
        public function propertyURL($id, $city_id=0, $neighborhood_id=0, $data=array()) {

            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['properties']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }

            if (!$city_id) $city_id = $data['city_id'];
            if (!$neighborhood_id) $neighborhood_id = $data['neighborhood_id'];

            if ($city_id) {
                $city_slug = stripslashes($data['location']['city']['slug']);
                if (!$city_slug) {
                    $city_query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .(int)$city_id. ")";
                    $city = sqlfetch(sqlquery($city_query));
                    if ($city['slug']) {
                        $city_slug = stripslashes($city['slug']);
                    } else {
                        $city_slug = $this->makeRewrite(stripslashes($city['title']));
                    }
                }
                if ($city_slug) {
                    if ($neighborhood_id) {
                        $neighborhood_slug = stripslashes($data['location']['neighborhood']['slug']);
                        if (!$neighborhood_slug) {
                            $neighborhood_query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` WHERE (`id`=" .(int)$neighborhood_id. ")";
                            $neighborhood = sqlfetch(sqlquery($neighborhood_query));
                            if ($neighborhood['slug']) {
                                $neighborhood_slug = stripslashes($neighborhood['slug']);
                            } else {
                                $neighborhood_slug = $this->makeRewrite(stripslashes($neighborhood['title']));
                            }
                        }
                    }
                }
            }

            $pa = array();
            if ($city_slug) {
                $pa[] = $city_slug;
                if ($neighborhood_slug) {
                    $pa[] = $neighborhood_slug;
                }
            } else {
                $pa[] = 'property';
            }
            $pa[] = $slug. '-' .$id;

            return '/' .$this->frontendPath(). '/' .implode('/', $pa). '/';

        }


        // BUILD WHERE ARRAY FOR getProperty SEARCH METHOD (BELOW)
        private function gpBuildWA($var, $db_field, $type) {

            // HAVE VALUE FOR VAR
            if ($var) {

                // IS ARRAY
                if (is_array($var)) {

                    // IS STRING
                    if ($type == 'str') {

                        // APPLY TO ALL VALS
                        $vals = array_map(function($val) {
                            return sqlescape($val);
                        }, $var);

                        // QUERY STRING
                        $out = "(" .$db_field. "='" .implode("') OR (" .$db_field. "='", $vals). "')";

                    // IS STRING - MULTIPLE
                    } else if ($type == 'str_multi') {

                        // LOOP THROUGH VALUES
                        foreach ($var as $val) {
                            $pa[] = "(FIND_IN_SET('" .sqlescape($val). "', " .$db_field. "))";
                        }

                        // QUERY STRING
                        $out = "(" .implode(") OR (", $pa). ")";

                    // IS NUMBER
                    } else if ($type == 'num') {

                        // APPLY TO ALL VALS
                        $vals = array_map(function($val) {
                            return (float)$val;
                        }, $var);

                        // QUERY STRING
                        $out = "(" .$db_field. "=" .implode(") OR (" .$db_field. "=", $vals). ")";

                    // IS MIN/MAX
                    } else if ($type == 'min_max') {

                        // HAVE MIN AND MAX
                        if (($var[0]) && ($var[1])) {
                            $out = "(" .$db_field. ">=" .(float)$var[0]. ") AND (" .$db_field. "<=" .(int)$var[1]. ")";

                        // HAVE MIN
                        } else if ($var[0]) {
                            $out = "(" .$db_field. ">=" .(float)$var[0]. ")";

                        // HAVE MAX
                        } else if ($var[1]) {
                            $out = "(" .$db_field. "<=" .(float)$var[1]. ")";
                        }

                    }

                // IS SINGLE
                } else {

                    // IS STRING
                    if ($type == 'str') {
                        $out = $db_field. "='" .sqlescape($var). "'";

                    // IS STRING - MULTIPLE
                    } else if ($type == 'str_multi') {
                        $out = "(FIND_IN_SET('" .sqlescape($var). "', " .$db_field. "))";
                        // WHERE (CONCAT(",", `cooling_type_codes`, ",") REGEXP ",(F|W),")

                    // IS NUMBER
                    } else if ($type == 'num') {
                        $out = $db_field. "=" .(float)$var;

                    } else if ($type == 'has') {
                        $out = $db_field. "!=''";
                    }

                }

                return $out;

            }

        }

        // GET PROPERTIES
        public function getProperties($vars) {

            $out = array();

            /*
            // STRING
            if ($vars['vars']['slug']) {
                if (is_array($vars['vars']['slug'])) {
                    $vals = array_map(function($val) {
                        return sqlescape($val);
                    }, $vars['vars']['slug']);
                    $wa[] = "(p.slug='" .implode("') OR (p.slug='", $vals). "')";
                } else {
                    $wa[] = "p.slug='" .sqlescape($vars['vars']['slug']). "'";
                }
            }

            // NUMBER
            if ($vars['vars']['id']) {
                if (is_array($vars['vars']['id'])) {
                    $vals = array_map(function($val) {
                        return (float)$val;
                    }, $vars['vars']['id']);
                    $wa[] = "(p.id=" .implode(") OR (p.id=", $vals). ")";
                } else {
                    $wa[] = "p.id=" .(float)$vars['vars']['slug'];
                }
            }
            */

            // BY ID
            if ($val = $this->gpBuildWA($vars['vars']['id'], 'p.id', 'num')) {
                $wa[] = $val;
            }

            // BY SLUG
            if ($val = $this->gpBuildWA($vars['vars']['slug'], 'p.slug', 'str')) {
                $wa[] = $val;
            }

            // BY PROPERTY ID
            if ($val = $this->gpBuildWA($vars['vars']['property_id'], 'p.property_id', 'str')) {
                $wa[] = $val;
            }

            // BY AGENCY
            if ($val = $this->gpBuildWA($vars['vars']['agency_id'], 'p.agency_id', 'num')) {
                $wa[] = $val;
            }

            // BY AGENT
            if ($val = $this->gpBuildWA($vars['vars']['agent_id'], 'p.agent_id', 'num')) {
                $wa[] = $val;
            }

            // BY CITY
            if ($val = $this->gpBuildWA($vars['vars']['city_id'], 'p.city_id', 'num')) {
                $wa[] = $val;
            }

            // BY NEIGHBORHOOD
            if ($val = $this->gpBuildWA($vars['vars']['neighborhood_id'], 'p.neighborhood_id', 'num')) {
                $wa[] = $val;
            }

            // BY LISTING TYPE
            if ($val = $this->gpBuildWA($vars['vars']['listing_type'], 'p.listing_type', 'num')) {
                $wa[] = $val;
            }

            // BY PROPERTY TYPE CODE
            if ($val = $this->gpBuildWA($vars['vars']['property_type_code'], 'p.property_type_code', 'str')) {
                $wa[] = $val;
            }

            // BY RENTAL SPACE CODE
            if ($val = $this->gpBuildWA($vars['vars']['rental_space_code'], 'p.rental_space_code', 'str')) {
                $wa[] = $val;
            }

            // BY CITY
            if ($val = $this->gpBuildWA($vars['vars']['city'], 'p.city', 'str')) {
                $wa[] = $val;
            }

            // BY STATE
            if ($val = $this->gpBuildWA($vars['vars']['state'], 'p.state', 'str')) {
                $wa[] = $val;
            }

            // BY ZIP
            if ($val = $this->gpBuildWA($vars['vars']['zip'], 'p.zip', 'str')) {
                $wa[] = $val;
            }

            // BY LOCATION CODE
            if ($val = $this->gpBuildWA($vars['vars']['location_code'], 'p.location_code', 'str')) {
                $wa[] = $val;
            }

            // PRICE - MIN & MAX
            if (($vars['vars']['price_min']) && ($vars['vars']['price_max'])) {
                $price_min = preg_replace("/[^0-9\.]/", '', $vars['vars']['price_min']);
                $price_max = preg_replace("/[^0-9\.]/", '', $vars['vars']['price_max']);
                $wa[] = "((p.price_from>=" .$price_min. ") AND (p.price_to<=" .$price_max. ")) OR ((p.price_from>=" .$price_min. ") AND (p.price_from<=" .$price_max. "))";

            // PRICE - MIN
            } else if ($vars['vars']['price_min']) {
                $price_min = (float)preg_replace("/[^0-9\.]/", '', $vars['vars']['price_min']);
                $wa[] = "p.price_from>=" .$price_min;

            // PRICE - MAX
            } else if ($vars['vars']['price_max']) {
                $price_max = preg_replace("/[^0-9\.]/", '', $vars['vars']['price_max']);
                $wa[] = "((p.price_to<=" .$price_max. ") AND (p.price_to!=0.00)) OR (p.price_from<=" .$price_max. ")";

            }

            // BY PRICE TERM CODE
            if ($val = $this->gpBuildWA($vars['vars']['price_term_code'], 'p.price_term_code', 'str')) {
                $wa[] = $val;
            }

            // BY SQFT
            if ($val = $this->gpBuildWA(array($vars['vars']['sqft_min'], $vars['vars']['sqft_max']), 'p.sqft', 'min_max')) {
                $wa[] = $val;
            }

            // BY LOT ACRES
            if ($val = $this->gpBuildWA(array($vars['vars']['lot_acres_min'], $vars['vars']['lot_acres_max']), 'p.lot_acres', 'min_max')) {
                $wa[] = $val;
            }

            // BY YEAR BUILT
            if ($val = $this->gpBuildWA(array($vars['vars']['year_built_min'], $vars['vars']['year_built_max']), 'p.year_built', 'min_max')) {
                $wa[] = $val;
            }

            // BY ACCOMMODATES NUM
            if ($val = $this->gpBuildWA(array($vars['vars']['accommodates_num_min'], $vars['vars']['accommodates_num_max']), 'p.accommodates_num', 'min_max')) {
                $wa[] = $val;
            }

            // BY BEDROOM NUM
            if ($val = $this->gpBuildWA(array($vars['vars']['bedrooms_num_min'], $vars['vars']['bedrooms_num_max']), 'p.bedrooms_num', 'min_max')) {
                $wa[] = $val;
            }

            // BY BATHROOM NUM
            if ($val = $this->gpBuildWA(array($vars['vars']['bathrooms_num_min'], $vars['vars']['bathrooms_num_max']), 'p.bathrooms_num', 'min_max')) {
                $wa[] = $val;
            }

            // BY RENTAL BATHROOMS CODE
            if ($val = $this->gpBuildWA($vars['vars']['rental_bathrooms_code'], 'p.rental_bathrooms_code', 'str')) {
                $wa[] = $val;
            }

            // BY HAS COOLING
            if ($val = $this->gpBuildWA($vars['vars']['has_cooling'], 'p.cooling_type_codes', 'has')) {
                $wa[] = $val;
            }

            // BY COOLING TYPE CODE
            if ($val = $this->gpBuildWA($vars['vars']['cooling_type_code'], 'p.cooling_type_codes', 'str_multi')) {
                $wa[] = $val;
            }

            // BY HAS HEATING
            if ($val = $this->gpBuildWA($vars['vars']['has_heating'], 'p.heating_type_codes', 'has')) {
                $wa[] = $val;
            }

            // BY HEATING TYPE CODE
            if ($val = $this->gpBuildWA($vars['vars']['heating_type_code'], 'p.heating_type_codes', 'str_multi')) {
                $wa[] = $val;
            }

            // BY FIREPLACE NUM
            if ($val = $this->gpBuildWA(array($vars['vars']['fireplaces_num_min'], $vars['vars']['fireplaces_num_max']), 'p.fireplace_num', 'min_max')) {
                $wa[] = $val;
            }

            // BY HAS WASHER / DRYER
            if ($val = $this->gpBuildWA($vars['vars']['has_washer_dryer'], 'p.washer_dryer_code', 'has')) {
                $wa[] = $val;
            }

            // BY WASHER / DRYER CODE
            if ($val = $this->gpBuildWA($vars['vars']['washer_dryer_code'], 'p.washer_dryer_code', 'str')) {
                $wa[] = $val;
            }

            // BY ELEVATOR NUM
            if ($val = $this->gpBuildWA(array($vars['vars']['elevators_num_min'], $vars['vars']['elevators_num_max']), 'p.elevator_num', 'min_max')) {
                $wa[] = $val;
            }

            // BY HAS WIFI
            if ($val = $this->gpBuildWA($vars['vars']['has_wifi'], 'p.has_wifi', 'num')) {
                $wa[] = $val;
            }

            // BY PARKING NUM
            if ($val = $this->gpBuildWA(array($vars['vars']['parking_num_min'], $vars['vars']['parking_num_max']), 'p.parking_num', 'min_max')) {
                $wa[] = $val;
            }

            // BY PARKING TYPE CODE
            if ($val = $this->gpBuildWA($vars['vars']['parking_type_code'], 'p.parking_type_codes', 'str_multi')) {
                $wa[] = $val;
            }

            // BY IS GATED
            if ($val = $this->gpBuildWA($vars['vars']['is_gated'], 'p.gated_code', 'has')) {
                $wa[] = $val;
            }

            // BY GATED CODE
            if ($val = $this->gpBuildWA($vars['vars']['gated_code'], 'p.gated_code', 'str')) {
                $wa[] = $val;
            }

            // BY HAS POOL
            if ($val = $this->gpBuildWA($vars['vars']['has_pool'], 'p.pool_code', 'has')) {
                $wa[] = $val;
            }

            // BY POOL CODE
            if ($val = $this->gpBuildWA($vars['vars']['pool_code'], 'p.pool_code', 'str')) {
                $wa[] = $val;
            }

            // BY PETS NUM
            if ($val = $this->gpBuildWA(array($vars['vars']['pets_num_min'], $vars['vars']['pets_num_max']), 'p.pets_num', 'min_max')) {
                $wa[] = $val;
            }

            // BY PET CODE
            if ($val = $this->gpBuildWA($vars['vars']['pet_type_code'], 'p.pet_type_codes', 'str_multi')) {
                $wa[] = $val;
            }

            // BY RATING
            if ($val = $this->gpBuildWA(array($vars['vars']['rating_min'], $vars['vars']['rating_max']), 'p.rating', 'min_max')) {
                $wa[] = $val;
            }

            // BY REVIEWS
            if ($val = $this->gpBuildWA(array($vars['vars']['reviews_min'], $vars['vars']['reviews_max']), 'p.reviews', 'min_max')) {
                $wa[] = $val;
            }

            // QUERY
            if ($vars['vars']['query']) {
                $q = sqlescape($vars['vars']['query']);
                $wa[] = "(`property_id` LIKE '%" .$q. "%') OR (`address1` LIKE '%" .$q. "%') OR (`address2` LIKE '%" .$q. "%') OR (`title` LIKE '%" .$q. "%') OR (`description` LIKE '%" .$q. "%')";

            }

            // ACTIVE
            $wa[] = "p.status=1";

            // WHERE
            $where_sql = "(" .implode(") AND (", $wa). ")";

            // ORDER
            switch ($vars['order_field']) {
                case 'price':
                    $order_field = "p.price_from";
                    break;
                case 'city':
                    $order_field = "p.city";
                    break;
                case 'state':
                    $order_field = "p.state";
                    break;
                case 'zip':
                    $order_field = "p.zip";
                    break;
                case 'sqft':
                    $order_field = "p.sqft";
                    break;
                case 'lot_acres':
                    $order_field = "p.lot_acres";
                    break;
                case 'year_built':
                    $order_field = "p.year_built";
                    break;
                case 'accommodates_num':
                    $order_field = "p.accommodates_num";
                    break;
                case 'bedrooms_num':
                    $order_field = "p.bedrooms_num";
                    break;
                case 'bathrooms_num':
                    $order_field = "p.bathrooms_num";
                    break;
                case 'fireplaces_num':
                    $order_field = "p.fireplaces_num";
                    break;
                case 'parking_num':
                    $order_field = "p.parking_num";
                    break;
                case 'pets_num':
                    $order_field = "p.pets_num";
                    break;
                case 'rating':
                    $order_field = "p.rating";
                    break;
                case 'reviews':
                    $order_field = "p.reviews";
                    break;
                case 'new':
                    $order_field = "p.dt_created";
                    break;
                case 'updated':
                    $order_field = "p.dt_updated";
                    break;
            }

            if (!$order_field) {
                $order_field = "p.price_from";
            }

            // ORDER DIRECTION
            if (strtoupper($vars['order_direction']) == 'DESC') {
                $order_direction = "DESC";
            } else {
                $order_direction = "ASC";
            }

            // ORDER SQL
            $order_sql = $order_field. " " .$order_direction;

            // LIMIT
            if ($vars['limit']) {
                $limit_limit = (int)$vars['limit'];
            } else {
                $limit_limit = $this->perPage();
            }

            // OFFSET
            if ($vars['offset']) {
                $limit_offset = (int)$vars['offset'];
            } else {
                $limit_offset = 0;
            }

            // LIMIT SQL
            $limit_sql = $limit_offset. "," .$limit_limit;

            // GET PAGED MATCHING PROPERTIES
            $query = "
            SELECT p.id
            FROM `" .$this->tables['properties']. "` AS `p`
            WHERE " .$where_sql. "
            ORDER BY " .$order_sql. "
            LIMIT " .$limit_sql . "
            ";

            //echo $query;
            //exit;

            $q = sqlquery($query);

            // NUMBER OF RESULTS
            $out['num_results'] = sqlrows($q);

            // GET ALL MATCHING PROPERTIES
            $tquery = "
            SELECT p.id
            FROM `" .$this->tables['properties']. "` AS `p`
            WHERE " .$where_sql. "
            ";
            $tq = sqlquery($tquery);

            // TOTAL RESULTS
            $out['num_total'] = sqlrows($tq);

            // LOOP
            while ($prop = sqlfetch($q)) {
                $out['properties'][$prop['id']] = $prop;
            }

            return $out;

        }


        // GET PROPERTY
        public function getProperty($id, $active=true, $what='all') {

            $out = array();

            $id = (int)$id;

            // HAVE ID
            if ($id) {

                if ($active) {
                    $active_sql = " AND (`status`=1)";
                }

                // GET PROPERTY
                $query = "SELECT * FROM `" .$this->tables['properties']. "` WHERE (`id`=" .$id. ")" .$active_sql;
                $q = sqlquery($query);
                $prop = sqlfetch($q);

                // PROPERTY FOUND
                if ($prop['id']) {

                    // ATTRIBUTES (KEEP JSON FORM)
                    $prop['attributes_json'] = $prop['attributes'];
                    unset($prop['attributes']);

                    // PRICE FORMATTING
                    $prop['price_from'] = str_replace('.00', '', $prop['price_from']);
                    $prop['price_to'] = str_replace('.00', '', $prop['price_to']);
                    $prop['price_from_formatted'] = number_format($prop['price_from'], (int)strstr($prop['price_from'], '.'));
                    $prop['price_to_formatted'] = number_format($prop['price_to'], (int)strstr($prop['price_to'], '.'));

                    $out = $prop;

                    // GET AGENT / AGENCY
                    if (($what == 'all') || ((is_array($what)) && (in_array('agent', $what)))) {

                        // HAVE AGENCY
                        if ($prop['agency_id']) {
                            $agency_query = "SELECT * FROM `" .$this->tables['agencies']. "` WHERE (`id`=" .$prop['agency_id']. ") AND (`status`=1)";
                            $agency_q = sqlquery($agency_query);
                            $agency = sqlfetch($agency_q);
                            if ($agency['id']) {
                                $out['agency'] = $agency;
                            }
                        }

                        // HAVE AGENT
                        if ($prop['agent_id']) {
                            $agent_query = "SELECT * FROM `" .$this->tables['agents']. "` WHERE (`id`=" .$prop['agent_id']. ") AND (`status`=1)";
                            $agent_q = sqlquery($agent_query);
                            $agent = sqlfetch($agent_q);
                            if ($agent['id']) {
                                $out['agent'] = $agent;
                            }
                        }

                    }

                    // GET LOCATION
                    if (($what == 'all') || ((is_array($what)) && (in_array('location', $what)))) {

                        // HAVE CITY
                        if ($prop['city_id']) {
                            $city_query = "SELECT * FROM `" .$this->tables['cities']. "` WHERE (`id`=" .$prop['city_id']. ")";
                            $city_q = sqlquery($city_query);
                            $city = sqlfetch($city_q);
                            if ($city['id']) {
                                $out['location']['city'] = $city;
                            }
                        }

                        // HAVE NEIGHBORHOOD
                        if ($prop['neighborhood_id']) {
                            $neighborhood_query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` WHERE (`id`=" .$prop['neighborhood_id']. ")";
                            $neighborhood_q = sqlquery($neighborhood_query);
                            $neighborhood = sqlfetch($neighborhood_q);
                            if ($neighborhood['id']) {
                                $out['location']['neighborhood'] = $neighborhood;
                            }
                        }

                    }

                    // GET IMAGES
                    if (($what == 'all') || ((is_array($what)) && (in_array('images', $what)))) {
                        $images_query = "SELECT * FROM `" .$this->tables['property_images']. "` WHERE (`property_id`=" .$prop['id']. ") ORDER BY `sort` ASC, `id` ASC";
                        $images_q = sqlquery($images_query);
                        while ($image = sqlfetch($images_q)) {
                            $out['images'][$image['id']] = $image;
                            $out['images'][$image['id']]['url'] = $this->imageBridgeOut($image['image'], 'properties');
                            $out['images'][$image['id']]['url_thumb'] = BigTree::prefixFile($this->imageBridgeOut($image['image'], 'properties'), 't_');
                        }
                    }

                    // GET VIDEOS
                    if (($what == 'all') || ((is_array($what)) && (in_array('videos', $what)))) {
                        $videos_query = "SELECT * FROM `" .$this->tables['property_videos']. "` WHERE (`property_id`=" .$prop['id']. ") ORDER BY `sort` ASC, `id` ASC";
                        $videos_q = sqlquery($videos_query);
                        while ($video = sqlfetch($videos_q)) {
                            $out['videos'][$video['id']] = $video;
                            $out['videos'][$video['id']]['info'] = videoInfo(stripslashes($video['video']));
                            //$out['videos'][$video['id']]['url_thumb'] = $videoa[$ii]['info']['thumb'];
                        }
                    }

                    // GET ROOMS
                    if (($what == 'all') || ((is_array($what)) && (in_array('rooms', $what)))) {
                        $rooms_query = "SELECT * FROM `" .$this->tables['property_rooms']. "` WHERE (`property_id`=" .$prop['id']. ") ORDER BY `sort` ASC, `level` DESC, `id` ASC";
                        $rooms_q = sqlquery($rooms_query);
                        while ($room = sqlfetch($rooms_q)) {
                            $out['rooms'][$room['id']] = $room;
                        }
                    }

                    // GET ATTRIBUTES
                    if (($what == 'all') || ((is_array($what)) && (in_array('attributes', $what)))) {

                        // HAVE PROPERTY ATTRIBUTE OPTIONS
                        if ($prop['attributes_json']) {

                            // PROPERTY ATTRIBUTES
                            $propattra = json_decode(stripslashes($prop['attributes_json']), true);

                            $attra = array();

                            // GET ATTRIBUTE GROUPS FOR PROPERTY
                            $groups_query = "
                            SELECT ag.*
                            FROM `" .$this->tables['attribute_groups']. "` AS `ag`
                            INNER JOIN `" .$this->tables['attribute_groups_property_types']. "` AS `agpt` ON ag.id=agpt.group_id
                            WHERE (ag.active=1) AND (agpt.property_type_code='" .$prop['property_type_code']. "')
                            ORDER BY `sort` ASC, `title` ASC
                            ";
                            $groups_q = sqlquery($groups_query);
                            while ($group = sqlfetch($groups_q)) {

                                // GET ATTRIBUTES FOR GROUP
                                $attr_query = "SELECT * FROM `" .$this->tables['attributes']. "` WHERE (`group_id`=" .$group['id']. ") AND (`active`=1)";
                                $attr_q = sqlquery($attr_query);
                                while ($attribute = sqlfetch($attr_q)) {

                                    // PROPERTY HAS ATTRIBUTES SAVED FOR THIS
                                    if ($propattra[$attribute['id']]) {

                                        $out['attributes'][$group['id']]['title'] = stripslashes($group['title']);
                                        $out['attributes'][$group['id']]['attributes'][$attribute['id']]['title'] = stripslashes($attribute['title']);
                                        $out['attributes'][$group['id']]['attributes'][$attribute['id']]['type'] = $attribute['type'];
                                        $out['attributes'][$group['id']]['attributes'][$attribute['id']]['options'] = $propattra[$attribute['id']];

                                    }

                                }

                            }

                        }

                    }

                }

            }

            return $out;

        }


        // GET LISTING PRICE
        public function listingPricing($prop_id) {

            // GET PROPERTY INFO
            $prop = $this->getProperty((int)$prop_id, false, array('agent'));

            // GET BASE PRICING
            $pricing = $this->getSettings(array('listing_price','listing_price_per'));

            // HAVE AGENCY ID
            if ($prop['agency']['id']) {
                if ($prop['agency']['listing_price'] !== '') {
                    $pricing['listing_price'] = $this->toFloat($prop['agency']['listing_price']);
                }
                if ($prop['agency']['listing_price_per']) {
                    $pricing['listing_price_per'] = $prop['agency']['listing_price_per'];
                }
            }

            // HAVE AGENT ID
            if ($prop['agent']['id']) {
                if ($prop['agent']['listing_price'] !== '') {
                    $pricing['listing_price'] = $this->toFloat($prop['agent']['listing_price']);
                }
                if ($prop['agent']['listing_price_per']) {
                    $pricing['listing_price_per'] = $prop['agent']['listing_price_per'];
                }
            }

            if (!$pricing['listing_price']) $pricing['listing_price'] = 0.00;

            return $pricing;

        }


        // TYPES
        public function propertyTypes() {
            return array(
                'VR'    => 'Vacation Rental',
                'SFH'   => 'Single Family Home',
                'MFH'   => 'Multi-Family',
                'CT'    => 'Condo / Townhouse',
                'APT'   => 'Apartment',
                'HOT'   => 'Hotel',
                'MH'    => 'Mobile Home',
                'GH'    => 'Guest House',
                'CAB'   => 'Cabin',
                'FR'    => 'Farm / Ranch',
                'LND'   => 'Land',
                'STO'   => 'Studio',
                'VIL'   => 'Villa',
                'LOF'   => 'Loft',
                'BUN'   => 'Bungalow',
                'CRV'   => 'Camper / RV',
                'DOR'   => 'Dorm',
                'HUT'   => 'Hut',
                'LH'    => 'Lighthouse',
                'ISL'   => 'Island',
                'CAS'   => 'Castle',
                'BOT'   => 'Boat',
                'PLN'   => 'Plane',
                'TRN'   => 'Train',
                'TNT'   => 'Tent',
                'CAV'   => 'Cave',
                'IGL'   => 'Igloo',
                'TIP'   => 'Tipi',
                'TH'    => 'Treehouse',
                'OTH'   => 'Other'
            );
        }


        // RENTAL SPACE
        public function propertyRentalSpaces() {
            return array(
                'EP' => 'Entire Place',
                'EF' => 'Entire Floor',
                'PF' => 'Partial Floor',
                'PR' => 'Private Room',
                'SR' => 'Shared Room'
            );
        }


        // PRICING TERMS
        public function propertyPriceTerms() {
            return array(
                'N' => 'Night',
                'W' => 'Week',
                'M' => 'Month',
                'Y' => 'Year'
            );
        }


        // LOCATIONS
        public function propertyLocations() {
            return array(
                'OF'    => array(
                    'title'         => 'Oceanfront',
                    'description'   => 'No other property/lot between the home and the beach. Dunes may obstruct ocean views; may or may not have direct beach access.'
                ),
                '2OF'   => array(
                    'title'         => '2nd-Tier Oceanfront',
                    'description'   => 'Although considered oceanfront, 2nd-tier oceanfront lots are shaped like the ivory keys on a piano with the narrow end facing the ocean. Views to the side may be obscured by another home.'
                ),
                'SOF'   => array(
                    'title'         => 'Semi-Oceanfront',
                    'description'   => 'One lot from the beach; may or may not have direct beach access, ocean views or a street between the home and the beach.'
                ),
                'OS'    => array(
                    'title'         => 'Oceanside',
                    'description'   => 'Within a few blocks from ocean; distance to the beach varies.'
                ),
                'SF'    => array(
                    'title'         => 'Soundfront',
                    'description'   => 'Homes that directly front the sound; may or may not have access to the sound.'
                ),
                'SS'    => array(
                    'title'         => 'Soundside',
                    'description'   => 'Within a few blocks of sound; may or may not have direct access to the sound.'
                ),
                'LF'    => array(
                    'title'         => 'Lakefront',
                    'description'   => 'No other property/lot between the home and the lake.'
                ),
                'LS'    => array(
                    'title'         => 'Lakeside',
                    'description'   => 'Within a few blocks of lake; may or may not have direct access to the lake.'
                ),
                'CF'    => array(
                    'title'         => 'Canalfront',
                    'description'   => 'Along a canal.'
                )
            );
        }


        // ROOM LEVELS / FLOORS
        public function propertyRoomLevels() {
            return array(
                5   => 'Floor 5',
                4   => 'Floor 4',
                3   => 'Floor 3',
                2   => 'Floor 2',
                1   => 'Ground Floor',
                -1  => 'Basement'
            );
        }


        // ROOM TYPES
        public function propertyRoomTypes() {
            return array(
                'BR'    => 'Bedroom',
                'BA'    => 'Bathroom',
                'K'     => 'Kitchen',
                'DR'    => 'Dining Room',
                'BO'    => 'Bonus Room',
                'O'     => 'Other'
            );
        }


        // BEDS
        public function propertyBedTypes() {
            return array(
                'K'     => 'King Bed',
                'Q'     => 'Queen Bed',
                'D'     => 'Double Bed',
                'T'     => 'Twin Bed',
                'BS'    => 'Bunk Set with two twin beds',
                'DB'    => 'Bunk set with a double bed on the bottom and a twin bed on top',
                'DBS'   => 'Bunk set with a double bed on the bottom and a double bed on top',
                'TR'    => 'Trundle Bed; twin bed that stores under another bed',
                'DB'    => 'Daybed',
                'QSS'   => 'Sleep Sofa with a queen-size mattress',
                'DSS'   => 'Sleep Sofa with a double-size mattress',
                'TSS'   => 'Sleep Sofa with a twin-size mattress',
                'FD'    => 'Double-size Fouton',
                'FT'    => 'Twin-size Fouton',
                'AB'    => 'Air Bed',
                'C'     => 'Couch'
            );
        }


        // RENTAL BATHROOM TYPES
        public function propertyRentalBathrooms() {
            return array(
                'P' => 'Private',
                'S' => 'Shared'
            );
        }


        // COOLING TYPES
        public function propertyCoolingTypes() {
            return array(
                'C' => 'Central AC',
                'W' => 'Wall AC',
                'P' => 'Portable AC',
                'F' => 'Fan'
            );
        }


        // HEATING TYPES
        public function propertyHeatingTypes() {
            return array(
                'F'     => 'Furnace',
                'HP'    => 'Heat Pump',
                'E'     => 'Electric',
                'B'     => 'Boiler',
                'S'     => 'Solar',
                'FG'    => 'Fireplace - Gas',
                'FW'    => 'Fireplace - Wood'
            );
        }


        // PARKING TYPES
        public function propertyParkingTypes() {
            return array(
                'D'     => 'Driveway',
                'GA'    => 'Garage - Attached',
                'GD'    => 'Garage - Detached',
                'C'     => 'Covered',
                'OS'    => 'On Street',
                'A'     => 'Alley',
                'L'     => 'Parking Lot',
                'PG'    => 'Parking Garage'
            );
        }


        // GATED
        public function propertyGatedTypes() {
            return array(
                'G'     => 'Gated',
                'GCR'   => 'Gated - Code Required',
                'GG'    => 'Gated - Guarded'
            );
        }


        // PETS
        public function propertyPetTypes() {
            return array(
                'C'     => 'Cat',
                'D'     => 'Dog',
                'O'     => 'Other'
            );
        }


        // WASHER/DRYER TYPES
        public function propertyWasherDryerTypes() {
            return array(
                'I'     => 'Included',
                'IS'    => 'Included - Shared',
                'H'     => 'Hookups',
                'OS'    => 'On Site'
            );
        }

        // POOL TYPES
        public function propertyPoolTypes() {
            return array(
                'PU'    => 'Public',
                'GPU'   => 'Public - Gated',
                'PR'    => 'Private',
                'I'     => 'Indoor'
            );
        }


        // RETURNS ARRAY OF LISTING CONTACT FIELDS
        public function listingContactFields() {
            $out = $this->getSetting('listing_contact_fields');
            if (($out) && (!is_array($out))) {
                $out = json_decode($out, true);
            }
            if ((!$out) || ((is_array($out) && (count($out) == 0)))) {
                $out = array();
            }
            return $out;
        }


        // LOG A PROPERTY STAT
        public function property_logStat($property_id, $action, $value='') {
            global $_uccms;
            $columns = array(
                'property_id'   => (int)$property_id,
                'action'        => $action,
                'value'         => $value,
                'account_id'    => $_uccms['_account']->userID(),
                'ip'            => ip2long($_SERVER['REMOTE_ADDR'])
            );
            $query = "INSERT INTO `" .$this->tables['property_stats_log']. "` SET " .uccms_createSet($columns). ", `dt`=NOW()";
            if (sqlquery($query)) {
                return sqlid();
            }
        }


        // GET A PROPERTY STAT
        public function property_getStats($property_id, $vars=array()) {

            $out = array();

            if (($property_id) && ($property_id != 'new')) {

                $wa = array();

                $wa[] = "(`property_id`=" .$property_id. ")";

                if ($vars['action']) {
                    $wa[] = "(`action`='" .$vars['action']. "')";
                }

                if ($vars['value']) {
                    $wa[] = "(`value`='" .$vars['value']. "')";
                }

                if ($vars['date_from']) {
                    $wa[] = "(`dt`>='" .date('Y-m-d H:i:s', strtotime($vars['date_from'])). "')";
                }

                if ($vars['date_to']) {
                    $wa[] = "(`dt`<='" .date('Y-m-d H:i:s', strtotime($vars['date_to'])). "')";
                }

                // WHERE
                $where_sql = "(" .implode(") AND (", $wa). ")";

                // GET STATS
                $stat_query = "SELECT * FROM `" .$this->tables['property_stats_log']. "` WHERE " .$where_sql;
                $stat_q = sqlquery($stat_query);
                while ($stat = sqlfetch($stat_q)) {
                    $out[$stat['action']]++;
                }

            }

            return $out;

        }


        // LOG PROPERTY CONTACT EMAIL
        public function property_logEmail($property_id, $fields) {
            $data = '';
            if (is_array($fields)) {
                $da = array();
                foreach ($fields as $fid => $fval) {
                    $da[$fid] = str_replace(array("\n", "\r", '"', "'"), array('<br/>', '', '&quot;', '&#039;'), $fval);
                }
                $data = json_encode($da);
            }
            $query = "INSERT INTO `" .$this->tables['property_email_log']. "` SET `dt`=NOW(), `property_id`=" .(int)$property_id. ", `data`='" .$data. "'";
            if (sqlquery($query)) {
                return sqlid();
            }
        }


        #############################
        # PAYMENT
        #############################


        // ALL AVAILABLE PAYMENT METHODS
        public function allPaymentMethods() {
            $out = array(
                'authorize'     => 'Authorize.Net',
                'paypal-rest'   => 'PayPal REST API',
                'paypal'        => 'PayPal Payments Pro',
                'payflow'       => 'PayPal Payflow Gateway',
                'linkpoint'     => 'First Data / LinkPoint',
                'stripe'        => 'Stripe',
                'instapay'      => 'InstaPay',
                'check_cash'    => 'Check / Cash'
            );
            return $out;
        }


        // GET PAYMENT METHODS FROM DB
        public function getPaymentMethods($active=false) {
            $out = array();
            if ($active) {
                $where = "WHERE (`active`=1)";
            }
            $query = "SELECT * FROM `" .$this->tables['payment_methods']. "` " .$where;
            $q = sqlquery($query);
            while ($f = sqlfetch($q)) {
                $out[$f['id']] = $f;
            }
            return $out;
        }


        // LOG TRANSACTION
        public function logTransaction($vars) {
            $query = "INSERT INTO `" .$this->tables['transaction_log']. "` SET " .uccms_createSet($vars). ", `dt`=NOW()";
            sqlquery($query);
        }


        #############################
        # EMAIL
        #############################


        // SEND AN EMAIL
        public function sendEmail($esettings, $evars) {
            global $_uccms;
            return $_uccms['_account']->sendEmail($esettings, $evars);
        }


        #############################
        # REVIEWS
        #############################


        // CRITERIA
        public function review_criteria($what='') {
            if ($what) {
                $criteria = $this->getSetting('review_criteria-' .$what);
            } else {
                $criteria = $this->getSetting('review_criteria');
            }
            if (!is_array($criteria)) $criteria = array();
            return $criteria;
        }


        // RECORD A REVIEW
        public function review_record($vars) {
            global $cms, $_uccms;

            // PROPERTY
            if ($vars['review']['what'] == 'property') {

                $id = (int)$vars['review']['item_id'];

                // HAVE ID
                if ($id) {

                    // GET PROPERTY
                    $prop = $this->getProperty($id, false, array('agent'));

                    // PROPERTY FOUND
                    if ($prop['id']) {

                        // DB COLUMNS
                        $columns = array(
                            'rating'        => $vars['calc']['rating'],
                            'reviews'       => $vars['calc']['reviews'],
                            'review_data'   => json_encode($vars['calc']['criteria'])
                        );

                        // RECORD INFO
                        $query = "UPDATE `" .$this->tables['properties']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";
                        sqlquery($query);

                    }

                    // EMAIL SETTINGS
                    $esettings = array(
                        'property_id'   => $prop['id']
                    );

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'item_id'               => $prop['id'],
                        'item_url'              => WWW_ROOT . $this->propertyURL($prop['id'], $prop['city_id'], $prop['neighborhood_id'], $prop),
                        'from_name'             => $vars['review']['name'],
                        'content'               => $vars['review']['content'],
                        'view_url'              => WWW_ROOT . $this->propertyURL($prop['id'], $prop['city_id'], $prop['neighborhood_id'], $prop). '#reviews'
                    );

                    if ($prop['title']) {
                        $evars['item_title'] = stripslashes($prop['title']);
                    } else {
                        $evars['item_title'] = $this->formatAddress($prop, ', ');
                    }

                    // IS REPLY
                    if ($vars['review']['reply_to_id']) {

                        // GET ORIGINAL REVIEW
                        $orig_query = "SELECT * FROM `uccms_ratings_reviews` WHERE (`id`=" .(int)$vars['review']['reply_to_id']. ")";
                        $orig_q = sqlquery($orig_query);
                        $orig = sqlfetch($orig_q);

                        // HAVE ACCOUNT ID
                        if ($orig['account_id']) {

                            // GET ACCOUNT
                            $acct = $_uccms['_account']->getAccount('', true, $orig['account_id']);

                            // HAVE EMAIL ADDRESS
                            if ($acct['email']) {

                                $esettings['template']  = 'ratings-reviews/reply-received.html';
                                $esettings['subject']   = ($cms->getSetting('site_name') ? $cms->getSetting('site_name'). ' - ' : ''). 'Reply to Review Received';
                                $esettings['to_email']  = stripslashes($acct['email']);

                                $evars['heading']   = 'Reply to Review Received';
                                $evars['to_name']   = ($acct['firstname'] ? ' ' .stripslashes($acct['firstname']) : '');

                            }

                        }

                    // REVIEW
                    } else {

                        $esettings['template'] = 'ratings-reviews/review-submitted.html';
                        $esettings['subject']   = ($cms->getSetting('site_name') ? $cms->getSetting('site_name'). ' - ' : ''). 'Review Submitted';

                        // WHO TO SEND TO
                        if ($prop['agent']['email_lead']) {
                            $esettings['to_email'] = stripslashes($prop['agent']['email_lead']);
                        } elseif ($prop['agent']['email']) {
                            $esettings['to_email'] = stripslashes($prop['agent']['email']);
                        } elseif ($prop['agency']['email_lead']) {
                            $esettings['to_email'] = stripslashes($prop['agency']['email_lead']);
                        } else {
                            $esettings['to_email'] = stripslashes($prop['agency']['email']);
                        }

                        $evars['heading']       = 'Review Submitted';
                        $evars['manage_url']    = WWW_ROOT . '/account/properties/property/?id=' .$prop['id']. '&tab=reviews';

                    }

                    // HAVE EMAIL
                    if ($esettings['to_email']) {

                        // SEND EMAIL
                        $result = $_uccms['_account']->sendEmail($esettings, $evars);

                    }

                }

            }

        }


        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # CRON
        #############################

        // RUN THE MAIN CRON
        public function runCron() {
            global $_uccms;

            //return;

            $out = array();

            // WITHIN 4:00 AM AND 6:00 AM
            //if ((time() >= strtotime(date('n/j/Y 04:00:00'))) && (time() <= strtotime(date('n/j/Y 06:00:00')))) {

                // GET EMAIL RELATED SETTINGS
                $esitesetta = $this->getSettings(array('email_from_name','email_from_email','email_admin_notify'));

                #########################
                # EXPIRED
                #########################

                // AUTOMATICALLY CHARGE
                $auto_charge = $this->getSetting('cron_auto_charge');

                // GET
                $property_query = "SELECT * FROM `" .$this->tables['properties']. "` WHERE (`payment_expires`='" .date('Y-m-d'). "') AND (`status`=1)";
                $property_q = sqlquery($property_query);
                while ($property = sqlfetch($property_q)) {

                    unset($account);

                    $payment_success = false;
                    $transaction_message = '';

                    unset($customer);

                    // HAS AGENT
                    if ($property['agent_id']) {
                        $query = "SELECT * FROM `" .$this->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
                        $q = sqlquery($query);
                        $agent = sqlfetch($q);
                        if ($agent['account_id']) {
                            $customer = $agent;
                        }
                    }

                    // STILL NO CUSTOMER, BUT IS AGENCY
                    if ((!$customer['account_id']) && ($property['agency_id'])) {
                        $query = "SELECT `account_id` FROM `" .$this->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
                        $q = sqlquery($query);
                        $agent = sqlfetch($q);
                        if ($agent['account_id']) {
                            $customer = $agency;
                        }

                    }

                    // HAVE ACCOUNT ID
                    if ($customer['account_id']) {

                        // GET ACCOUNT
                        $query = "SELECT * FROM `uccms_accounts` WHERE (`id`=" .$customer['account_id']. ")";
                        $q = sqlquery($query);
                        $account = sqlfetch($q);

                    }

                    // TITLE
                    if ($property['title']) {
                        $property_title = stripslashes($property['title']);
                    } else if ($property['address1']) {
                        $property_title = stripslashes($property['address1']);
                    } else {
                        $property_title = 'Untitled';
                    }

                    // GET PRICING INFO
                    $pricing = $this->listingPricing($property['id']);

                    // AUTOMATICALLY CHARGE
                    if ($auto_charge) {

                        // HAVE PRICE
                        if ($pricing['listing_price'] > 0.00) {

                            // HAVE PAYMENT PROFILE ID
                            if ($property['payment_profile_id']) {

                                // GET PAYMENT PROFILE
                                $pp = $_uccms['_account']->pp_getProfile($property['payment_profile_id']);

                                // PAYMENT PROFILE FOUND
                                if ($pp['id']) {

                                    $payment_method     = 'profile';
                                    $payment_name       = stripslashes($pp['name']);
                                    $payment_lastfour   = $pp['lastfour'];
                                    $payment_expiration = $pp['expires'];

                                    // PAYMENT - CHARGE
                                    $payment_data = array(
                                        'amount'    => $pricing['listing_price'],
                                        'customer' => array(
                                            'id'            => $pp['account_id'],
                                            'name'          => $payment_name,
                                            'email'         => stripslashes($customer['email']),
                                            'phone'         => '',
                                            'address'       => array(
                                                'street'    => '',
                                                'street2'   => '',
                                                'city'      => '',
                                                'state'     => '',
                                                'zip'       => '',
                                                'country'   => ''
                                            ),
                                            'description'   => ''
                                        ),
                                        'note' => 'Renewal for property listing: ' .$property_title. ' (ID: ' .$property['id']. ')',
                                        'card' => array(
                                            'id'            => $pp['id'],
                                            'name'          => $payment_name,
                                            'number'        => $payment_lastfour,
                                            'expiration'    => $payment_expiration
                                        )
                                    );

                                    // PROCESS PAYMENT
                                    $payment_result = $_uccms['_account']->payment_charge($payment_data);

                                    // TRANSACTION ID
                                    $transaction_id = $payment_result['transaction_id'];

                                    // TRANSACTION MESSAGE (ERROR)
                                    $transaction_message = $payment_result['error'];

                                    // LOG TRANSACTION
                                    $this->logTransaction(array(
                                        'account_id'            => $property['account_id'],
                                        'plan_id'               => $plan['id'],
                                        'status'                => ($transaction_id ? 'charged' : 'failed'),
                                        'amount'                => $price,
                                        'term'                  => $term,
                                        'method'                => $payment_method,
                                        'payment_profile_id'    => $payment_result['profile_id'],
                                        'name'                  => $payment_name,
                                        'lastfour'              => $payment_lastfour,
                                        'expiration'            => $payment_expiration,
                                        'transaction_id'        => $transaction_id,
                                        'message'               => $transaction_message,
                                        //'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                                    ));

                                    // CHARGE SUCCESSFUL
                                    if ($transaction_id) {
                                        $payment_success = true;
                                    }

                                // PAYMENT PROFILE NOT FOUND
                                } else {
                                    $transaction_message = 'Payment method not found.';
                                }

                            // NO PAYMENT PROFILE ID
                            } else {
                                $transaction_message = 'No payment method assigned to listing.';
                            }

                        // NO PRICE
                        } else {
                            $payment_success = true;
                        }

                    }

                    // EMAIL SETTINGS
                    $esettings = array(
                        'from_name'         => stripslashes($esitesetta['email_from_name']),
                        'from_email'        => stripslashes($esitesetta['email_from_email'])
                    );

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'              => date('n/j/Y'),
                        'time'              => date('g:i A T'),
                        'property_id'       => $property['id'],
                        'property_title'    => $property_title,
                        'name'              => trim(stripslashes($customer['name']). ' ' .stripslashes($customer['title'])),
                        'url_manage'        => WWW_ROOT. 'account/properties/property/?id=' .$property['id'],
                        'url_view'          => WWW_ROOT . $this->propertyURL($property['id'], $property['city_id'], $property['neighborhood_id'], $property)
                    );

                    // WHO TO SEND EMAIL TO
                    $emailtoa = array();

                    // ACCOUNT EMAIL INFO
                    if ($customer['email']) {
                        $emailtoa[$customer['email']] = trim(stripslashes($customer['name']). ' ' .stripslashes($customer['title']));
                    } else {
                        $emailtoa[$account['email']] = trim($account['firstname']. ' ' .$account['lastname']);
                    }

                    switch ($pricing['listing_price_per']) {
                        case 'w':
                            $days = 7;
                            break;
                        case 'm':
                            $days = 30;
                            break;
                        case 'q':
                            $days = 90;
                            break;
                        case 'y':
                            $days = 365;
                            break;
                        default:
                            $days = 30;
                            break;
                    }

                    // PAYMENT SUCCESSFUL
                    if ($payment_success) {

                        // DB COLUMNS
                        $columns = array(
                            'status'            => 1,
                            'payment_expires'   => date('Y-m-d', strtotime('+' .$days. ' Days'))
                        );

                        if ($pricing['listing_price'] > 0.00) {
                            $columns['paying'] = 1;
                        }

                        // UPDATE PROPERTY
                        $query = "UPDATE `" .$this->tables['properties']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$property['id']. ")";
                        sqlquery($query);

                        // EMAIL SETTINGS
                        $esettings['custom_template'] = SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/email/property/renewed.html';
                        $esettings['subject']         = 'Listing Renewed';

                        // EMAIL VARIABLES
                        $evars['heading']           = 'Listing Renewed';
                        $evars['listing_expires']   = date('n/j/Y', strtotime('+' .$days. ' Days'));
                        $evars['total']             = '$' .number_format($pricing['listing_price'], 2);

                    // PAYMENT FAILED
                    } else {

                        // DB COLUMNS
                        $columns = array(
                            'status' => 0
                        );

                        // UPDATE PROPERTY
                        $query = "UPDATE `" .$this->tables['properties']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$property['id']. ")";
                        sqlquery($query);

                        // EMAIL VARIABLES
                        $evars['expired'] = date('n/j/Y', strtotime($property['payment_expires']));

                        // FAILED
                        if ($auto_charge) {

                            // EMAIL SETTINGS
                            $esettings['custom_template'] = SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/email/property/expired-failed.html';
                            $esettings['subject']         = 'Listing Renewal Failed';

                            // EMAIL VARIABLES
                            $evars['heading'] = 'Listing Renewal Failed';

                            if ($transaction_message) {
                                if ($payment_lastfour) {
                                    $evars['transaction_info'] = 'Failed to charge card ending in ' .$payment_lastfour. '. (' .$transaction_message. ')<br /><br />';
                                } else {
                                    $evars['transaction_info'] = $transaction_message. '<br /><br />';
                                }

                            } else {
                                $evars['transaction_info'] = '';
                            }

                        // NOT AUTO-CHARGING
                        } else {

                            // EMAIL SETTINGS
                            $esettings['custom_template'] = SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/email/property/expired.html';
                            $esettings['subject']         = 'Listing Expired';

                        }

                    }

                    ##########################
                    # EMAIL
                    ##########################

                    // COPY OF EMAIL TO ADMIN(S)
                    if ($esitesetta['email_admin_notify']) {
                        $emails = explode(',', stripslashes($esitesetta['email_admin_notify']));
                        foreach ($emails as $email) {
                            $emailtoa[$email] = $email;
                        }
                    }

                    // HAVE EMAILS TO SEND TO
                    if (count($emailtoa) > 0) {

                        // LOOP
                        foreach ($emailtoa as $to_email => $to_name) {

                            // WHO TO SEND TO
                            $esettings['to_name']   = $to_name;
                            $esettings['to_email']  = $to_email;

                            // SEND EMAIL
                            $result = $this->sendEmail($esettings, $evars);

                        }

                    }

                }

                return implode("\n", $out). "\n";

            //}

        }


        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # GLOBAL SEARCH
        #############################

        public function globalSearch($q='', $params=array()) {

            $results = array();

            // GET MATCHING PROPERTIES
            $results_query = "SELECT `id`, `slug`, `title`, `address1`, `city` FROM `" .$this->tables['properties']. "` WHERE (`title` LIKE '%" .$q. "%') ORDER BY `dt_updated` DESC, `dt_created` DESC LIMIT " .(int)$params['limit'];
            $results_q = sqlquery($results_query);
            while ($row = sqlfetch($results_q)) {
                if ($row['title']) {
                    $title = stripslashes($row['title']);
                } else {
                    $title = $this->formatAddress($row, ', ');
                }
                $results[] = array(
                    'title' => $title,
                    'url'   => $this->propertyURL($row['id'], 0, 0, $row),
                    'type'  => 'property'
                );
            }

            return $results;

        }


        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # SITEMAP
        #############################

        public function getSitemap($page=array()) {
            global $bigtree;

            $out = array();

            // CITIES
            $query = "SELECT * FROM `" .$this->tables['cities']. "` ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->cityURL($r['id'], $r)
                );
            }

            // NEIGHBORHOODS
            $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->neighborhoodURL($r['id'], $r)
                );
            }

            // PROPERTIES
            $query = "SELECT * FROM `" .$this->tables['properties']. "` WHERE (`status`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->propertyURL($r['id'], $r)
                );
            }

            // AGENTS
            $query = "SELECT * FROM `" .$this->tables['agents']. "` WHERE (`status`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->agentURL($r['id'], $r)
                );
            }

            // AGENCY
            $query = "SELECT * FROM `" .$this->tables['agencies']. "` WHERE (`status`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->agencyURL($r['id'], $r)
                );
            }

            return $out;

        }


    }
?>