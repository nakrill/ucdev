<?php

class global_uccms_Properties extends uccms_Properties {


    #############################
    # GENERAL: Sitemap
    #############################

    public function general_sitemap($page=array()) {
        global $bigtree;

        $out = array();

        // CITIES
        $query = "SELECT * FROM `" .$this->tables['cities']. "` ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->cityURL($r['id'], $r)
            );
        }

        // NEIGHBORHOODS
        $query = "SELECT * FROM `" .$this->tables['neighborhoods']. "` ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->neighborhoodURL($r['id'], $r)
            );
        }

        // PROPERTIES
        $query = "SELECT * FROM `" .$this->tables['properties']. "` WHERE (`status`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->propertyURL($r['id'], $r)
            );
        }

        // AGENTS
        $query = "SELECT * FROM `" .$this->tables['agents']. "` WHERE (`status`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->agentURL($r['id'], $r)
            );
        }

        // AGENCY
        $query = "SELECT * FROM `" .$this->tables['agencies']. "` WHERE (`status`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->agencyURL($r['id'], $r)
            );
        }

        return $out;

    }


    #############################
    # GENERAL: Search
    #############################

    public function general_search($q='', $params=array()) {

        $results = array();

        // GET MATCHING PROPERTIES
        $results_query = "SELECT `id`, `slug`, `title`, `address1`, `city` FROM `" .$this->tables['properties']. "` WHERE (`title` LIKE '%" .$q. "%') ORDER BY `dt_updated` DESC, `dt_created` DESC LIMIT " .(int)$params['limit'];
        $results_q = sqlquery($results_query);
        while ($row = sqlfetch($results_q)) {
            if ($row['title']) {
                $title = stripslashes($row['title']);
            } else {
                $title = $this->formatAddress($row, ', ');
            }
            $results[] = array(
                'title' => $title,
                'url'   => $this->propertyURL($row['id'], 0, 0, $row),
                'type'  => 'property'
            );
        }

        return $results;

    }


    #############################
    # GENERAL: Cron
    #############################

    // RUN THE MAIN CRON
    public function runCron() {
        global $_uccms;

        //return;

        $out = array();

        // WITHIN 4:00 AM AND 6:00 AM
        //if ((time() >= strtotime(date('n/j/Y 04:00:00'))) && (time() <= strtotime(date('n/j/Y 06:00:00')))) {

            // GET EMAIL RELATED SETTINGS
            $esitesetta = $this->getSettings(array('email_from_name','email_from_email','email_admin_notify'));

            #########################
            # EXPIRED
            #########################

            // AUTOMATICALLY CHARGE
            $auto_charge = $this->getSetting('cron_auto_charge');

            // GET
            $property_query = "SELECT * FROM `" .$this->tables['properties']. "` WHERE (`payment_expires`='" .date('Y-m-d'). "') AND (`status`=1)";
            $property_q = sqlquery($property_query);
            while ($property = sqlfetch($property_q)) {

                unset($account);

                $payment_success = false;
                $transaction_message = '';

                unset($customer);

                // HAS AGENT
                if ($property['agent_id']) {
                    $query = "SELECT * FROM `" .$this->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
                    $q = sqlquery($query);
                    $agent = sqlfetch($q);
                    if ($agent['account_id']) {
                        $customer = $agent;
                    }
                }

                // STILL NO CUSTOMER, BUT IS AGENCY
                if ((!$customer['account_id']) && ($property['agency_id'])) {
                    $query = "SELECT `account_id` FROM `" .$this->tables['agents']. "` WHERE (`id`=" .$property['agent_id']. ")";
                    $q = sqlquery($query);
                    $agent = sqlfetch($q);
                    if ($agent['account_id']) {
                        $customer = $agency;
                    }

                }

                // HAVE ACCOUNT ID
                if ($customer['account_id']) {

                    // GET ACCOUNT
                    $query = "SELECT * FROM `uccms_accounts` WHERE (`id`=" .$customer['account_id']. ")";
                    $q = sqlquery($query);
                    $account = sqlfetch($q);

                }

                // TITLE
                if ($property['title']) {
                    $property_title = stripslashes($property['title']);
                } else if ($property['address1']) {
                    $property_title = stripslashes($property['address1']);
                } else {
                    $property_title = 'Untitled';
                }

                // GET PRICING INFO
                $pricing = $this->listingPricing($property['id']);

                // AUTOMATICALLY CHARGE
                if ($auto_charge) {

                    // HAVE PRICE
                    if ($pricing['listing_price'] > 0.00) {

                        // HAVE PAYMENT PROFILE ID
                        if ($property['payment_profile_id']) {

                            // GET PAYMENT PROFILE
                            $pp = $_uccms['_account']->pp_getProfile($property['payment_profile_id']);

                            // PAYMENT PROFILE FOUND
                            if ($pp['id']) {

                                $payment_method     = 'profile';
                                $payment_name       = stripslashes($pp['name']);
                                $payment_lastfour   = $pp['lastfour'];
                                $payment_expiration = $pp['expires'];

                                // PAYMENT - CHARGE
                                $payment_data = array(
                                    'amount'    => $pricing['listing_price'],
                                    'customer' => array(
                                        'id'            => $pp['account_id'],
                                        'name'          => $payment_name,
                                        'email'         => stripslashes($customer['email']),
                                        'phone'         => '',
                                        'address'       => array(
                                            'street'    => '',
                                            'street2'   => '',
                                            'city'      => '',
                                            'state'     => '',
                                            'zip'       => '',
                                            'country'   => ''
                                        ),
                                        'description'   => ''
                                    ),
                                    'note' => 'Renewal for property listing: ' .$property_title. ' (ID: ' .$property['id']. ')',
                                    'card' => array(
                                        'id'            => $pp['id'],
                                        'name'          => $payment_name,
                                        'number'        => $payment_lastfour,
                                        'expiration'    => $payment_expiration
                                    )
                                );

                                // PROCESS PAYMENT
                                $payment_result = $_uccms['_account']->payment_charge($payment_data);

                                // TRANSACTION ID
                                $transaction_id = $payment_result['transaction_id'];

                                // TRANSACTION MESSAGE (ERROR)
                                $transaction_message = $payment_result['error'];

                                // LOG TRANSACTION
                                $this->logTransaction(array(
                                    'account_id'            => $property['account_id'],
                                    'plan_id'               => $plan['id'],
                                    'status'                => ($transaction_id ? 'charged' : 'failed'),
                                    'amount'                => $price,
                                    'term'                  => $term,
                                    'method'                => $payment_method,
                                    'payment_profile_id'    => $payment_result['profile_id'],
                                    'name'                  => $payment_name,
                                    'lastfour'              => $payment_lastfour,
                                    'expiration'            => $payment_expiration,
                                    'transaction_id'        => $transaction_id,
                                    'message'               => $transaction_message,
                                    //'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                                ));

                                // CHARGE SUCCESSFUL
                                if ($transaction_id) {
                                    $payment_success = true;
                                }

                            // PAYMENT PROFILE NOT FOUND
                            } else {
                                $transaction_message = 'Payment method not found.';
                            }

                        // NO PAYMENT PROFILE ID
                        } else {
                            $transaction_message = 'No payment method assigned to listing.';
                        }

                    // NO PRICE
                    } else {
                        $payment_success = true;
                    }

                }

                // EMAIL SETTINGS
                $esettings = array(
                    'from_name'         => stripslashes($esitesetta['email_from_name']),
                    'from_email'        => stripslashes($esitesetta['email_from_email'])
                );

                // EMAIL VARIABLES
                $evars = array(
                    'date'              => date('n/j/Y'),
                    'time'              => date('g:i A T'),
                    'property_id'       => $property['id'],
                    'property_title'    => $property_title,
                    'name'              => trim(stripslashes($customer['name']). ' ' .stripslashes($customer['title'])),
                    'url_manage'        => WWW_ROOT. 'account/properties/property/?id=' .$property['id'],
                    'url_view'          => WWW_ROOT . $this->propertyURL($property['id'], $property['city_id'], $property['neighborhood_id'], $property)
                );

                // WHO TO SEND EMAIL TO
                $emailtoa = array();

                // ACCOUNT EMAIL INFO
                if ($customer['email']) {
                    $emailtoa[$customer['email']] = trim(stripslashes($customer['name']). ' ' .stripslashes($customer['title']));
                } else {
                    $emailtoa[$account['email']] = trim($account['firstname']. ' ' .$account['lastname']);
                }

                switch ($pricing['listing_price_per']) {
                    case 'w':
                        $days = 7;
                        break;
                    case 'm':
                        $days = 30;
                        break;
                    case 'q':
                        $days = 90;
                        break;
                    case 'y':
                        $days = 365;
                        break;
                    default:
                        $days = 30;
                        break;
                }

                // PAYMENT SUCCESSFUL
                if ($payment_success) {

                    // DB COLUMNS
                    $columns = array(
                        'status'            => 1,
                        'payment_expires'   => date('Y-m-d', strtotime('+' .$days. ' Days'))
                    );

                    if ($pricing['listing_price'] > 0.00) {
                        $columns['paying'] = 1;
                    }

                    // UPDATE PROPERTY
                    $query = "UPDATE `" .$this->tables['properties']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$property['id']. ")";
                    sqlquery($query);

                    // EMAIL SETTINGS
                    $esettings['custom_template'] = SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/email/property/renewed.html';
                    $esettings['subject']         = 'Listing Renewed';

                    // EMAIL VARIABLES
                    $evars['heading']           = 'Listing Renewed';
                    $evars['listing_expires']   = date('n/j/Y', strtotime('+' .$days. ' Days'));
                    $evars['total']             = '$' .number_format($pricing['listing_price'], 2);

                // PAYMENT FAILED
                } else {

                    // DB COLUMNS
                    $columns = array(
                        'status' => 0
                    );

                    // UPDATE PROPERTY
                    $query = "UPDATE `" .$this->tables['properties']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$property['id']. ")";
                    sqlquery($query);

                    // EMAIL VARIABLES
                    $evars['expired'] = date('n/j/Y', strtotime($property['payment_expires']));

                    // FAILED
                    if ($auto_charge) {

                        // EMAIL SETTINGS
                        $esettings['custom_template'] = SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/email/property/expired-failed.html';
                        $esettings['subject']         = 'Listing Renewal Failed';

                        // EMAIL VARIABLES
                        $evars['heading'] = 'Listing Renewal Failed';

                        if ($transaction_message) {
                            if ($payment_lastfour) {
                                $evars['transaction_info'] = 'Failed to charge card ending in ' .$payment_lastfour. '. (' .$transaction_message. ')<br /><br />';
                            } else {
                                $evars['transaction_info'] = $transaction_message. '<br /><br />';
                            }

                        } else {
                            $evars['transaction_info'] = '';
                        }

                    // NOT AUTO-CHARGING
                    } else {

                        // EMAIL SETTINGS
                        $esettings['custom_template'] = SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/email/property/expired.html';
                        $esettings['subject']         = 'Listing Expired';

                    }

                }

                ##########################
                # EMAIL
                ##########################

                // COPY OF EMAIL TO ADMIN(S)
                if ($esitesetta['email_admin_notify']) {
                    $emails = explode(',', stripslashes($esitesetta['email_admin_notify']));
                    foreach ($emails as $email) {
                        $emailtoa[$email] = $email;
                    }
                }

                // HAVE EMAILS TO SEND TO
                if (count($emailtoa) > 0) {

                    // LOOP
                    foreach ($emailtoa as $to_email => $to_name) {

                        // WHO TO SEND TO
                        $esettings['to_name']   = $to_name;
                        $esettings['to_email']  = $to_email;

                        // SEND EMAIL
                        $result = $this->sendEmail($esettings, $evars);

                    }

                }

            }

            return implode("\n", $out). "\n";

        //}

    }


    #############################
    # PAYMENT - Transactions
    #############################

    public function payment_transactions($vars=array()) {

        $out = array();

        $wa = array();

        // DATE
        if (count($vars['date']) == 2) {
            $wa[] = "(`dt`>='" .date('Y-m-d', strtotime($vars['date']['from'])). " 00:00:00') AND (`dt`<='" .date('Y-m-d', strtotime($vars['date']['to'])). " 23:59:59')";
        } else if ($vars['date']['from']) {
            $wa[] = "`dt`>='" .date('Y-m-d', strtotime($vars['date']['from'])). " 00:00:00'";
        } else if ($vars['date']['to']) {
            $wa[] = "`dt`<='" .date('Y-m-d', strtotime($vars['date']['to'])). " 23:59:59'";
        }

        // KEYWORD
        if ($vars['q']) {
            $q = trim(sqlescape($vars['q']));
            $wa[] = "(`name` LIKE '%" .$q. "%') OR (`lastfour` LIKE '%" .$q. "%')";
        }

        // ORDERBY - Date
        if ($vars['query']['orderby']['field'] == 'dt') {
            $orderby_sql = " ORDER BY `dt` " .($vars['query']['orderby']['direction'] == "desc" ? "DESC" : "ASC"). " ";
        }

        // LIMIT
        if ($vars['query']['limit']['num'] > 0) {
            $limit_sql = " LIMIT " .(int)$vars['query']['limit']['num']. " ";
        }

        if (count($wa) > 0) {
            $where_sql = " WHERE (" .implode(" ) AND ( ", $wa). ")";
        } else {
            $where_sql = "";
        }

        $accounta = array();

        // GET TRANSACTION LOGS
        $trans_query = "SELECT * FROM `" .$this->tables['transaction_log']. "` " .$where_sql . $orderby_sql . $limit_sql;
        $trans_q = sqlquery($trans_query);
        while ($trans = sqlfetch($trans_q)) {

            // HAVE ACCOUNT ID
            if (($trans['account_id']) && (!$accounta[$trans['account_id']])) {

                // GET ACCOUNT INFO
                $account_query = "
                SELECT *
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$trans['account_id']. ")
                ";
                $account_q = sqlquery($account_query);
                $accounta[$trans['account_id']] = sqlfetch($account_q);

            }

            if ($accounta[$trans['account_id']]['id']) {
                $account = $accounta[$trans['account_id']];
            } else {
                $account = array();
            }

            $out[] = array(
                'number'        => $account['id'],
                'dt'            => $trans['dt'],
                'name'          => trim(stripslashes($account['firstname']. ' ' .$account['lastname'])),
                'method'        => $trans['method'],
                'description'   => 'Property',
                'amount'        => $trans['amount'],
                'lastfour'      => $trans['lastfour'],
                'success'       => ($trans['status'] == 'charged' ? true : false),
                'url'           => array(
                    'edit' => '/admin/' .$this->Extension. '*properties/properties/edit/?id=' .$trans['property_id']
                ),
            );

        }

        return $out;

    }


}

?>