$(document).ready(function() {

    // TOGGLE
    $('#propertiesContainer .toggle-options').change(function(e) {
        var type = $(this).attr('type');
        var tel = $(this).attr('data-toggle');
        if (type == 'checkbox') {
            if ($(this).prop('checked')) {
                $(tel).show();
            } else {
                $(tel).hide();
            }
        } else if (type == 'text') {
            if (($(this).val()) && ($(this).val() != 0)) {
                $(tel).show();
            } else {
                $(tel).hide();
            }
        }
    });

    // LISTING TYPE CHANGE
    $('#propertiesContainer .search-box select[name="listing_type"]').change(function() {
        if ($(this).val() == 1) {
            $('#propertiesContainer .search-box .lt-rental').hide();
            $('#propertiesContainer .search-box .lt-sale').show();
        } else if ($(this).val() == 2) {
            $('#propertiesContainer .search-box .lt-sale').hide();
            $('#propertiesContainer .search-box .lt-rental').show();
        }
    });
    $('#propertiesContainer .search-box select[name="listing_type"]').change();

    // CITY CHECK
    $('#propertiesContainer .search-box input[name="city_id[]"]').click(function() {
        var any_checked = false;
        $('#propertiesContainer .search-box input[name="city_id[]"]').each(function() {
            if ($(this).is(':checked')) {
                $('#propertiesContainer .search-box #neighborhoods .city-' +$(this).val()).show();
                any_checked = true;
            } else {
                $('#propertiesContainer .search-box #neighborhoods .city-' +$(this).val()).hide();
            }
        });
        /*
        if (any_checked) {
            $('#propertiesContainer .search-box #neighborhoods').show();
        } else {
            $('#propertiesContainer .search-box #neighborhoods').hide();
        }
        */
    });

});