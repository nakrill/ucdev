$(document).ready(function() {

    // SLIDER
    $('#propertiesContainer .search-box .option.slider').each(function() {

        var el_slider = $(this).find('.slider_element')[0];
        var el_min = $(this).find('.value .min');
        var el_max = $(this).find('.value .max');
        var el_input_min = $(this).find('input.min');
        var el_input_max = $(this).find('input.max');

        var step = parseFloat($(el_slider).attr('data-step'));
        if (isNaN(step)) step = 0;

        noUiSlider.create(el_slider, {
            start: [ parseFloat($(el_slider).attr('data-start')), parseFloat($(el_slider).attr('data-end')) ],
            behaviour: 'drag-tap',
            connect: true,
            range: {
                'min':  parseFloat($(el_slider).attr('data-min')),
                'max':  parseFloat($(el_slider).attr('data-max'))
            },
            step: step
        });

        var dec = $(el_slider).attr('data-decimal');

        el_slider.noUiSlider.on('update', function(values, handle) {
            if (dec) {
                el_min.text(values[0]);
                el_max.text(values[1]);
                el_input_min.val(values[0]);
                el_input_max.val(values[1]);
            } else {
                el_min.text(parseInt(values[0]));
                el_max.text(parseInt(values[1]));
                el_input_min.val(parseInt(values[0]));
                el_input_max.val(parseInt(values[1]));
            }
        });

    });

});