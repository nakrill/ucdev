$(document).ready(function() {

    // RATING STARS CLICK
    $('#propertiesContainer .property-full .rating_stars .stars, #propertiesContainer .property-full .rating_stars a').click(function(e) {
        e.preventDefault();
        $('#propertiesContainer .property-full .tabs1 .nav a[href="#reviews"]').click();
        $('html, body').animate({
            scrollTop: $('#propertiesContainer .property-full .reviews').offset().top
        }, 1500);
    });

    // TABS
    $('#propertiesContainer .property-full .tabs1 .nav a').click(function(e) {
        e.preventDefault();
        var parent = $(this).closest('.tabs1');
        parent.find('.content > .item').hide();
        parent.find('.nav a').removeClass('active');
        parent.find('.content ' +$(this).attr('href')).show();
        $(this).addClass('active');
    });

    // SWIPEBOX
    $('#propertiesContainer .swipebox').swipebox({
        removeBarsOnMobile: false
    });

    // CONTACT FORM SUBMIT
    $('#propertiesContainer .contact form').submit(function(e) {
        e.preventDefault();
        $('#propertiesContainer .contact .site-message').hide().html('');
        $.post('/*/com.umbrella.properties/ajax/property/contact_submit/', $(this).serialize(), function(data) {
            if (data['success']) {
                $('#propertiesContainer .contact .site-message.type-success').html(data['success']).show();
            } else {
                if (data['err']) {
                    $('#propertiesContainer .contact .site-message.type-error').html(data['err']).show();
                    if (data['missing_fields']) {
                        var missing_html = '';
                        $.each(data['missing_fields'], function(index, value) {
                            missing_html += '<li>' +value+ '</li>';
                        });
                        $('#propertiesContainer .contact .site-message.type-error').append('<ul>' +missing_html+ '</ul>');
                    }
                } else {
                    alert('Failed to submit contact form.');
                }
            }
        }, 'json');
    });

});