<?php

// GET PROPERTY
$prop = $_uccms_properties->getProperty($prop_id);

// PROPERTY FOUND
if ($prop['id']) {

    // LOG VIEW STAT
    $_uccms_properties->property_logStat($prop['id'], 'view');

    // CUSTOM EDIT LINK
    $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_properties->Extension. '*properties/properties/edit/?id=' .$prop['id'];

    // NUMBER OF IMAGES
    $num_images = count($prop['images']);

    // FIRST IMAGE
    if (is_array($prop['images'])) {
        $first_image = $prop['images'][array_keys($prop['images'])[0]];
    }

    // NUMBER OF VIDEOS
    $num_videos = count((array)$prop['videos']);

    /*
    // MAIN IMAGE
    if ($num_images > 0) {
        $image = $_uccms_properties->imageBridgeOut($prop['images'][array_keys($prop['images'])[0]]['image'], 'properties');
    } else {
        $image = '/extensions/' .$_uccms_properties->Extension. '/images/property_no-image.jpg';
    }
    */

    // FORMATTED ADDRESS
    $address = $_uccms_properties->formatAddress($prop, ' ');

    // META
    if ($prop['meta_title']) {
        $bigtree['page']['title'] = stripslashes($prop['meta_title']);
    } else if ($prop['title']) {
        $bigtree['page']['title'] = str_replace('"', '', stripslashes($prop['title']));
    } else if ($address) {
        $bigtree['page']['title'] = 'Address';
    } else {
        $bigtree['page']['title'] = 'Property #' .$prop['id'];
    }
    if ($prop['meta_description']) {
        $bigtree['page']['meta_description'] = stripslashes($prop['meta_description']);
    } else if ($prop['description']) {
        $bigtree['page']['meta_description'] = substrWord(str_replace('"', '', stripslashes($prop['description'])), 160, '..');
    }
    if ($prop['meta_keywords']) {
        $bigtree['page']['meta_keywords'] = stripslashes($prop['meta_keywords']);
    }

    // CANONICAL URL
    $bigtree['page']['canonical'] = $_uccms_properties->propertyURL($prop['id'], $prop['city_id'], $prop['neighborhood_id'], $prop);

}

?>