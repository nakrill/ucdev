<?php

// META
if ($_properties['settings']['home_meta_title']) $bigtree['page']['title'] = stripslashes($_properties['settings']['home_meta_title']);
if ($_properties['settings']['home_meta_description']) $bigtree['page']['meta_description'] = stripslashes($_properties['settings']['home_meta_description']);
if ($_properties['settings']['home_meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($_properties['settings']['home_meta_keywords']);

/*
$prop_vars = array(
    'vars'  => array(
        'id' => 1,
        'slug' => 'test',
        'property_id' => 'KD12345',
        'agency_id' => 1,
        'agent_id' => 1,
        'neighborhood_id' => 1,
        'listing_type' => 1,
        'property_type_code' => 'APT',
        'rental_space_code' => 'EF',
        'city' => 'Oceanside',
        'state' => 'CA',
        'zip' => '92054',
        'location_code' => 'SOF',
        'price_min' => '$1,000.25',
        'price_max' => '1600',
        'price_term_code' => 'M',
        'sqft_min' => 550,
        'sqft_max' => 1200,
        'lot_acres_min' => .5,
        'lot_acres_max' => 2,
        'year_built_min' => 1980,
        'year_built_max' => 2016,
        'accommodates_num_min' => 2,
        'accommodates_num_max' => 10,
        'bedrooms_num_min' => 1,
        'bedrooms_num_max' => 4,
        'bathrooms_num_min' => 1.5,
        'bathrooms_num_max' => 3,
        'rental_bathrooms_code' => 'P',
        'has_cooling' => true,
        'cooling_type_code' => 'F',
        'has_heating' => true,
        'heating_type_code' => 'FG',
        'fireplaces_num_min' => 1,
        'fireplacess_num_max' => 2,
        'has_washer_dryer' => true,
        'washer_dryer_code' => 'I',
        'elevators_num_min' => 1,
        'elevators_num_max' => 2,
        'has_wifi' => 1,
        'parking_num_min' => 1,
        'parking_num_max' => 5,
        'parking_type_code' => 'OS',
        'is_gated' => true,
        'gated_code' => 'GCR',
        'has_pool' => true,
        'pool_code' => 'PU',
        'pets_num_min' => 1,
        'pets_num_max' => 2,
        'pet_type_code' => 'C',
        'rating_min' => 2,
        'rating_max' => 5,
        'reviews_min' => 1,
        'reviews_max' => 15,
        'query' => 'main'
    ),
    'order_field'       => 'price',
    'order_direction'   => 'ASC',
    'limit'             => 10,
    'offset'            => 0
);
*/

// PAGE LIMIT
$page_limit = $_uccms_properties->perPage();

// OFFSET
if ($_GET['page']) {
    $offset = ((int)$_GET['page'] - 1) * $page_limit;
} else {
    $offset = 0;
}

// SORT
if ($_REQUEST['sort']) {
    list($order_field, $order_direction) = explode('.', $_REQUEST['sort']);
}

// VARIABLES TO FIND BY
$prop_vars = array(
    'vars'              => $_GET,
    'order_field'       => $order_field,
    'order_direction'   => $order_direction,
    'limit'             => $page_limit,
    'offset'            => $offset
);

//echo print_r($prop_vars);
//echo '<br /><br />';

// GET MATCHING PROPERTIES
$properties = $_uccms_properties->getProperties($prop_vars);

//echo '<br /><br />';
//echo print_r($properties);
//exit;

// HAVE RESULTS
if ($properties['num_results'] > 0) {

    // PAGING CLASS
    require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

    // INIT PAGING CLASS
    $_paging = new paging();

    // OPTIONAL - URL VARIABLES TO IGNORE
    $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

    // PREPARE PAGING
    $_paging->prepare($page_limit, 10, $_GET['page'], $_GET);

    // GENERATE PAGING
    $pages = $_paging->output($properties['num_results'], $properties['num_total']);

}

// DISPLAY FORMAT
$display_style = 'grid';

?>