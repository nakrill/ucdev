<?php

// IS CITY
if ($path_num == 2) {

    // GET CITY INFO
    $city_query = "SELECT * FROM `" .$_uccms_properties->tables['cities']. "` WHERE (`slug`='" .stripslashes($bigtree['path'][1]). "')";
    $city_q = sqlquery($city_query);
    $city = sqlfetch($city_q);

    // META
    if ($city['meta_title']) $bigtree['page']['title'] = stripslashes($city['meta_title']);
    if ($city['meta_description']) $bigtree['page']['meta_description'] = stripslashes($city['meta_description']);
    if ($city['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($city['meta_keywords']);

// IS NEIGHBORHOOD
} else if ($path_num == 3) {

    // GET NEIGHBORHOOD INFO
    $neighborhood_query = "SELECT * FROM `" .$_uccms_properties->tables['neighborhoods']. "` WHERE (`slug`='" .stripslashes($bigtree['path'][2]). "')";
    $neighborhood_q = sqlquery($neighborhood_query);
    $neighborhood = sqlfetch($neighborhood_q);

    // HAVE CITY ID
    if ($neighborhood['city_id']) {

        // META
        if ($neighborhood['meta_title']) $bigtree['page']['title'] = stripslashes($neighborhood['meta_title']);
        if ($neighborhood['meta_description']) $bigtree['page']['meta_description'] = stripslashes($neighborhood['meta_description']);
        if ($neighborhood['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($neighborhood['meta_keywords']);

        // GET CITY
        $city_query = "SELECT * FROM `" .$_uccms_properties->tables['cities']. "` WHERE (`id`=" .$neighborhood['city_id']. ")";
        $city_q = sqlquery($city_query);
        $city = sqlfetch($city_q);

    }

}

// PAGE LIMIT
$page_limit = $_uccms_properties->perPage();

// OFFSET
if ($_GET['page']) {
    $offset = ((int)$_GET['page'] - 1) * $page_limit;
} else {
    $offset = 0;
}

// SORT
if ($_REQUEST['sort']) {
    list($order_field, $order_direction) = explode('.', $_REQUEST['sort']);
}

// VARIABLES TO FIND BY
$prop_vars = array(
    'vars'              => array_merge($_GET, array(
        'city_id'           => $city['id'],
        'neighborhood_id'   => $neighborhood['id']
    )),
    'order_field'       => $order_field,
    'order_direction'   => $order_direction,
    'limit'             => $page_limit,
    'offset'            => $offset
);

//echo print_r($prop_vars);
//echo '<br /><br />';

// GET MATCHING PROPERTIES
$properties = $_uccms_properties->getProperties($prop_vars);

//echo '<br /><br />';
//echo print_r($properties);
//exit;

// HAVE RESULTS
if ($properties['num_results'] > 0) {

    // PAGING CLASS
    require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

    // INIT PAGING CLASS
    $_paging = new paging();

    // OPTIONAL - URL VARIABLES TO IGNORE
    $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

    // PREPARE PAGING
    $_paging->prepare($page_limit, 10, $_GET['page'], $_GET);

    // GENERATE PAGING
    $pages = $_paging->output($properties['num_results'], $properties['num_total']);

}

// DISPLAY FORMAT
$display_style = 'grid';

?>