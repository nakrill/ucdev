<?php

$properties = array();

$profile_query = '';

// IS AGENT
if ($bigtree['path'][1] == 'agent') {
    $profile_query = "SELECT * FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`slug`='" .sqlescape($bigtree['path'][2]). "') AND (`status`=1)";

// IS AGENCY
} else if ($bigtree['path'][1] == 'agency') {
    $profile_query = "SELECT * FROM `" .$_uccms_properties->tables['agents']. "` WHERE (`slug`='" .sqlescape($bigtree['path'][2]). "') AND (`status`=1)";

}

// HAVE QUERY
if ($profile_query) {

    // GET PROFILE
    $profile_q = sqlquery($profile_query);
    $profile = sqlfetch($profile_q);

    // HAVE PROFILE
    if ($profile['id']) {

        // NAME
        if (!$profile['name']) $profile['name'] = $profile['title'];

        // AGENT
        if ($bigtree['path'][1] == 'agent') {

            $profile['what']    = 'agent';
            $profile['whats']   = 'agents';

            $bigtree['page']['title'] = stripslashes($profile['name']);

            $_GET['agent_id']   = $profile['id'];

        // AGENCY
        } else if ($bigtree['path'][1] == 'agencies') {

            $profile['what']    = 'agency';
            $profile['whats']   = 'agencies';

            $bigtree['page']['title'] = stripslashes($profile['title']);

            $_GET['agency_id']   = $profile['id'];

        }

        // VARIABLES TO FIND BY
        $prop_vars = array(
            'vars'              => $_GET,
            //'order_field'       => $order_field,
            //'order_direction'   => $order_direction,
            'limit'             => 3,
            //'offset'            => $offset
        );

        // GET MATCHING PROPERTIES
        $properties = $_uccms_properties->getProperties($prop_vars);

    }

}

?>