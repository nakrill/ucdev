<link rel="stylesheet" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/swipebox.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.1/js/jquery.swipebox.min.js"></script>
<script type="text/javascript" src="<?=WWW_ROOT?>extensions/<?=$_uccms_properties->Extension;?>/js/lib/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/masonry/3.3.0/masonry.pkgd.min.js"></script>
<?php /*if (count($prop['videos']) > 0) { ?>
    <script type="text/javascript" src="/js/jquery.fitvids.js"></script>
    <script>
        $(document).ready(function(){
            $('#propertiesContainer .videos').fitVids();
        });
    </script>
<?php }*/ ?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/property.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/custom/property.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/master/property.js"></script>
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/custom/property.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

// PROPERTY FOUND
if ($prop['id']) {

    // SCHEMA
    include(dirname(__FILE__). '/../../elements/property-schema.php');

    ?>

    <div class="property-full clearfix">

        <div class="content">

            <div class="largeColumn">

                <div class="listing_type">For <?php if ($prop['listing_type'] == 2) { ?>Rent<?php } else { ?>Sale<?php } ?></div>

                <?php if ($prop['title']) { ?>
                    <h1 class="title"><?php echo stripslashes($prop['title']); ?><?php if ($prop['property_id']) { ?><span class="property_id"><?php echo stripslashes($prop['property_id']); ?></span><?php } ?></h1>
                <?php } ?>

                <div class="clearfix">

                    <?php if ($address) { ?>
                        <h2 class="address"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $address; ?></h2>
                    <?php } ?>

                    <?php if ($_uccms_properties->getSetting('ratings_reviews_enabled')) { ?>
                        <div class="rating_stars">
                            <?php
                            if ($prop['rating'] > 0) {
                                ?>
                                <div class="stars" title="<?php echo $prop['rating']; ?> from <?php echo number_format($prop['reviews'], 0); ?> reviews">
                                    <?php
                                    $star_vars = array(
                                        'el_id'     => 'property-stars-' .$prop['id'],
                                        'value'     => $prop['rating'],
                                        'readonly'  => true
                                    );
                                    include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                                    ?>
                                </div>
                                <?php
                            } else {
                                ?>
                                <a href="#reviews">No reviews</a>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } ?>

                    <?php if (($prop['location']['city']['title']) || ($prop['location']['neighborhood']['title'])) { ?>
                        <div class="city_neighborhood">
                            <?php if ($prop['location']['neighborhood']['title']) { ?>
                                <span class="neighborhood"><?php echo stripslashes($prop['location']['neighborhood']['title']); ?></span><?php if (($prop['location']['city']['title']) || ($prop['location']['neighborhood']['title'])) { echo ', '; } ?>
                            <?php } ?>
                            <?php if ($prop['location']['city']['title']) { ?>
                                <span class="city"><?php echo stripslashes($prop['location']['city']['title']); ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="tabs1">
                    <ul class="nav">
                        <li><a href="#images" class="active"><i class="fa fa-camera" aria-hidden="true"></i>Images</a></li>
                        <?php if ($address) { ?>
                            <li><a href="#map"><i class="fa fa-map-marker" aria-hidden="true"></i>Map</a></li>
                        <?php } ?>
                    </ul>
                    <div class="content">

                        <div id="images" class="item">

                            <?php

                            // HAVE IMAGES
                            if (count($prop['images']) == 0) {
                                $prop['images'][0]['url'] = '/extensions/' .$_uccms_properties->Extension. '/images/property_no-image.jpg';
                            }

                            ?>

                            <div class="images_container">

                                <?php if ($first_image['url']) { ?>
                                    <div class="main">
                                        <a href="<?php echo $first_image['url']; ?>" target="blank" class="swipebox <?php if ($i == 0) { ?>active<?php } ?>" rel="gallery-1"><img src="<?php echo $first_image['url']; ?>" alt="<?php if ($first_image['caption']) { echo stripslashes($first_image['caption']); } else { echo 'Image 1'; } ?>" itemprop="image" /></a>
                                    </div>
                                    <?php
                                    unset($imagea[0]);
                                } ?>

                                <?php

                                $ii = 0;
                                ?>
                                <div class="thumbs contain">
                                    <?php

                                    // LOOP THROUGH IMAGES
                                    if (count($prop['images'] > 0)) {
                                        $i = 1;
                                        foreach ($prop['images'] as $image) {
                                            if ($image['image']) {
                                                if ($image['caption']) {
                                                    $caption = stripslashes($image['caption']);
                                                } else {
                                                    $caption = 'Image ' .$i;
                                                }
                                                ?>
                                                <div class="thumb image <?php if ($ii == 0) { ?>active<?php } ?>">
                                                    <a href="<?php echo $image['url']; ?>" title="<?php echo $caption; ?>" target="_blank" class="swipebox" rel="gallery-1"><img src="<?php echo $image['url_thumb']; ?>" alt="<?php echo $caption; ?>" itemprop="image" /></a>
                                                </div>
                                                <?php
                                                $ii++;
                                                $i++;
                                            }
                                        }
                                    }

                                    ?>
                                </div>

                            </div>

                        </div>

                        <?php if ($address) { ?>
                            <div id="map" class="item map">
                                <iframe width="100%" height="412" frameborder="0" style="margin: 0; border: 0;" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $address; ?>&key=AIzaSyDb9IKaXNmMAc8bMuRtOmmEx35cOW1HZEo" allowfullscreen></iframe>
                            </div>
                        <?php } ?>

                    </div>

                </div>

                <div class="feature_grid">
                    <?php if ($prop['bedrooms_num'] > 0) { ?>
                        <div class="item bedrooms">
                            <i class="fa fa-bed" aria-hidden="true"></i><?php echo number_format($prop['bedrooms_num'], 0); ?>
                            <div class="label">Bedrooms</div>
                        </div>
                    <?php } ?>
                    <?php if ($prop['bathrooms_num'] > 0) { ?>
                        <div class="item bathrooms">
                            <i class="fa fa-tint" aria-hidden="true"></i><?php echo number_format($prop['bathrooms_num'], 0); ?>
                            <div class="label">Bathrooms</div>
                        </div>
                    <?php } ?>
                    <?php if ($prop['sqft'] > 0) { ?>
                        <div class="item sqft">
                            <i class="fa fa-home" aria-hidden="true"></i><?php echo number_format($prop['sqft'], 0); ?>
                            <div class="label">Sq ft</div>
                        </div>
                    <?php } ?>
                    <?php if ($prop['accommodates_num'] > 0) { ?>
                        <div class="item accommodates">
                            <i class="fa fa-users" aria-hidden="true"></i><?php echo number_format($prop['accommodates_num'], 0); ?>
                            <div class="label">Accommodates</div>
                        </div>
                    <?php } ?>
                </div>

                <?php if ($prop['description']) { ?>
                    <div class="description"><?php echo stripslashes($prop['description']); ?></div>
                <?php } ?>

                <div class="hr"></div>

                <div class="general_info">

                    <?php if ($prop['location_code']) { ?>
                        <div class="location pad-top">
                            <i class="fa fa-map-o" aria-hidden="true"></i><?php echo $_uccms_properties->propertyLocations()[$prop['location_code']]['title']; ?>
                            <div class="small">
                                <?php echo $_uccms_properties->propertyLocations()[$prop['location_code']]['description']; ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($prop['property_type_code']) { ?>
                        <div class="property_type pad-top"><i class="fa fa-fw fa-home" aria-hidden="true"></i><?php echo $_uccms_properties->propertyTypes()[$prop['property_type_code']]; ?></div>
                    <?php } ?>

                    <?php if ($prop['rental_space_code']) { ?>
                        <div class="rental_space pad-top"><i class="fa fa-fw fa-th-large" aria-hidden="true"></i><?php echo $_uccms_properties->propertyRentalSpaces()[$prop['rental_space_code']]; ?></div>
                    <?php } ?>

                    <?php if ($prop['lot_acres'] > 0) { ?>
                        <div class="acres pad-top"><i class="fa fa-fw fa-square-o" aria-hidden="true"></i>Acres: <?php echo number_format($prop['lot_acres'], 0); ?></div>
                    <?php } ?>

                    <?php if ($prop['year_built']) { ?>
                        <div class="year_built pad-top"><i class="fa fa-fw fa-clock-o" aria-hidden="true"></i>Year Built: <?php echo $prop['year_built']; ?></div>
                    <?php } ?>

                </div>

                <div class="features">

                    <h3>Features</h3>

                    <div class="items clearfix">

                        <?php if ($prop['bedrooms_num_master'] > 0) { ?>
                            <div class="master_bedrooms item"><i class="fa fa-check" aria-hidden="true"></i>Master Bedrooms: <?php echo number_format($prop['bedrooms_num_master'], 0); ?></div>
                        <?php } ?>

                        <?php if ($prop['rental_bathrooms_code']) { ?>
                            <div class="rental_bathrooms item"><i class="fa fa-check" aria-hidden="true"></i>Bathroom(s): <?php echo $_uccms_properties->propertyRentalBathrooms()[$prop['rental_bathrooms_code']]; ?></div>
                        <?php } ?>

                        <?php if ($prop['cooling_type_codes']) { ?>
                            <div class="cooling item">
                                <i class="fa fa-check" aria-hidden="true"></i>Cooling:
                                <?php
                                unset($pa2);
                                $pa = explode(',', $prop['cooling_type_codes']);
                                foreach ($pa as $part) {
                                    $pa2[] = $_uccms_properties->propertyCoolingTypes()[$part];
                                }
                                echo implode(', ', $pa2);
                                ?>
                            </div>
                        <?php } ?>

                        <?php if ($prop['heating_type_codes']) { ?>
                            <div class="heating item">
                                <i class="fa fa-check" aria-hidden="true"></i>Heating:
                                <?php
                                unset($pa2);
                                $pa = explode(',', $prop['heating_type_codes']);
                                foreach ($pa as $part) {
                                    $pa2[] = $_uccms_properties->propertyHeatingTypes()[$part];
                                }
                                echo implode(', ', $pa2);
                                ?>
                            </div>
                        <?php } ?>

                        <?php if ($prop['fireplace_num'] > 0) { ?>
                            <div class="fireplaces item"><i class="fa fa-check" aria-hidden="true"></i>Fireplaces: <?php echo number_format($prop['fireplace_num'], 0); ?></div>
                        <?php } ?>

                        <?php if ($prop['washer_dryer_code']) { ?>
                            <div class="washer_dryer item"><i class="fa fa-check" aria-hidden="true"></i>Washer / Dryer: <?php echo $_uccms_properties->propertyWasherDryerTypes()[$prop['washer_dryer_code']]; ?></div>
                        <?php } ?>

                        <?php if ($prop['has_wifi']) { ?>
                            <div class="has_wifi item"><i class="fa fa-check" aria-hidden="true"></i>Has WiFi</div>
                        <?php } ?>

                        <?php if ($prop['parking_num'] > 0) { ?>
                            <div class="parking_num item"><i class="fa fa-check" aria-hidden="true"></i>Parking Spots: <?php echo number_format($prop['parking_num'], 0); ?></div>
                        <?php } ?>

                        <?php if ($prop['heating_type_codes']) { ?>
                            <div class="heating item">
                                <i class="fa fa-check" aria-hidden="true"></i>Parking Type(s):
                                <?php
                                unset($pa2);
                                $pa = explode(',', $prop['parking_type_codes']);
                                foreach ($pa as $part) {
                                    $pa2[] = $_uccms_properties->propertyParkingTypes()[$part];
                                }
                                echo implode(', ', $pa2);
                                ?>
                            </div>
                        <?php } ?>

                        <?php if ($prop['gated_code']) { ?>
                            <div class="gated item"><i class="fa fa-check" aria-hidden="true"></i><?php echo $_uccms_properties->propertyGatedTypes()[$prop['gated_code']]; ?></div>
                        <?php } ?>

                        <?php if ($prop['pool_code']) { ?>
                            <div class="pool_type item"><i class="fa fa-check" aria-hidden="true"></i>Pool: <?php echo $_uccms_properties->propertyPoolTypes()[$prop['pool_code']]; ?></div>
                        <?php } ?>

                        <?php if ($prop['pets_num']) { ?>
                            <div class="pets_num item">
                                <i class="fa fa-check" aria-hidden="true"></i>Pets Allowed: <?php echo number_format($prop['pets_num'], 0); ?>
                                <?php
                                unset($pa2);
                                $pa = explode(',', $prop['pet_type_codes']);
                                foreach ($pa as $part) {
                                    $pa2[] = $_uccms_properties->propertyPetTypes()[$part];
                                }
                                if (count($pa2) > 0) {
                                    echo ' (' .implode(', ', $pa2). ')';
                                }
                                ?>
                            </div>
                        <?php } ?>

                    </div>

                </div>

                <?php if (count($prop['attributes']) > 0) { ?>
                    <div class="attributes">
                        <?php
                        foreach ($prop['attributes'] as $group) {
                            if (count($group['attributes']) > 0) {
                                foreach ($group['attributes'] as $attr_id => $attr) {
                                    if (count($attr['options']) > 0) {
                                        ?>
                                        <div class="attribute info_container1">
                                            <?php if ($attr['title']) { ?><h3><?php echo $attr['title']; ?></h3><?php } ?>
                                            <div class="items clearfix">
                                                <?php foreach ($attr['options'] as $option) { ?>
                                                    <div class="item">
                                                        <i class="fa fa-caret-right" aria-hidden="true"></i><?php
                                                        if ($attr['type'] == 'file') {
                                                            ?><a href="<?php echo $_uccms_properties->imageBridgeOut($prop['id']. '-' .$attr_id. '_' .$option, 'properties/attributes'); ?>" target="_blank">View / Download</a><?php
                                                        } else {
                                                            echo $option;
                                                        }
                                                        ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                <?php } ?>

                <?php if (($prop['pool_size']) || ($prop['pool_dt_open'] != '0000-00-00 00:00:00') || ($prop['pool_dt_close'] != '0000-00-00 00:00:00')) { ?>

                    <div class="pool_info info_container1">

                        <h3>Pool Info</h3>

                        <div class="items clearfix">

                            <?php if ($prop['pool_size']) { ?>
                                <div class="pool_size item"><i class="fa fa-caret-right" aria-hidden="true"></i>Pool Size: <?php echo stripslashes($prop['pool_size']); ?></div>
                            <?php } ?>

                            <?php if ($prop['pool_dt_open'] != '0000-00-00 00:00:00') { ?>
                                <div class="pool_open item"><i class="fa fa-caret-right" aria-hidden="true"></i>Pool Opens: <?php echo date('n/j/Y', strtotime($prop['pool_dt_open'])); ?></div>
                            <?php } ?>

                            <?php if ($prop['pool_dt_close'] != '0000-00-00 00:00:00') { ?>
                                <div class="pool_close item"><i class="fa fa-caret-right" aria-hidden="true"></i>Pool Closes: <?php echo date('n/j/Y', strtotime($prop['pool_dt_close'])); ?></div>
                            <?php } ?>

                        </div>

                    </div>

                <?php } ?>

                <?php if (($prop['checkin_office']) || (($prop['checkin_day']) || ($prop['checkin_time'] != '00:00:00')) || (($prop['checkout_day']) || ($prop['checkout_time'] != '00:00:00'))) { ?>

                    <div class="checkinout_info info_container1">

                        <h3>Check In / Out</h3>

                        <div class="items clearfix">

                            <?php if ($prop['checkin_office']) { ?>
                                <div class="checkin_office item"><i class="fa fa-caret-right" aria-hidden="true"></i>Check In At: <?php echo stripslashes($prop['checkin_office']); ?></div>
                            <?php } ?>

                            <?php if (($prop['checkin_day']) || ($prop['checkin_time'] != '00:00:00')) { ?>
                                <div class="checkin_dt item"><i class="fa fa-caret-right" aria-hidden="true"></i>Check In: <?php
                                    unset($pa);
                                    if ($prop['checkin_day']) $pa[] = $_uccms_properties->daysOfWeek()[$prop['checkin_day']];
                                    if ($prop['checkin_time'] != '00:00:00') $pa[] = date('g:i A', strtotime($prop['checkin_time']));
                                    echo implode(' ', $pa);
                                ?></div>
                            <?php } ?>

                            <?php if (($prop['checkout_day']) || ($prop['checkout_time'] != '00:00:00')) { ?>
                                <div class="checkout_dt item"><i class="fa fa-caret-right" aria-hidden="true"></i>Check Out: <?php
                                    unset($pa);
                                    if ($prop['checkout_day']) $pa[] = $_uccms_properties->daysOfWeek()[$prop['checkout_day']];
                                    if ($prop['checkpout_time'] != '00:00:00') $pa[] = date('g:i A', strtotime($prop['checkout_time']));
                                    echo implode(' ', $pa);
                                ?></div>
                            <?php } ?>

                        </div>

                    </div>

                <?php } ?>

                <?php if (count($prop['videos']) > 0) { ?>
                    <div class="videos">
                        <?php
                        foreach ($prop['videos'] as $video) {
                            if ($video['info']['source'])  {
                                ?>
                                <div class="video">
                                    <?php if ($video['caption']) { ?><h3><?php echo stripslashes($video['caption']); ?></h3><?php } ?>
                                    <iframe width="625" height="352" src="<?php echo $video['info']['source']; ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                <?php } ?>

                <?php if (count($prop['rooms']) > 0) { ?>
                    <div class="rooms">
                        <h3>Rooms</h3>
                        <div class="items">
                            <?php foreach ($prop['rooms'] as $room) { ?>
                                <div class="room clearfix">
                                    <div class="head items clearfix">
                                        <div class="type item"><?php echo $_uccms_properties->propertyRoomTypes()[$room['type_code']]; ?></div>
                                        <div class="level item">Level: <?php echo $_uccms_properties->propertyRoomLevels()[$room['level']]; ?></div>
                                        <div class="dimensions item"><?php echo stripslashes($room['dimensions']); ?></div>
                                    </div>
                                    <?php if (($room['sleeps_num'] > 0) || ($room['bed_codes'])) { ?>
                                        <div class="bed items clearfix">
                                            <div class="sleeps item">Sleeps: <?php echo number_format($room['sleeps_num'], 0); ?></div>
                                            <div class="beds item">
                                                Beds:&nbsp;
                                                <?php
                                                if ($room['bed_codes']) {
                                                    unset($pa2);
                                                    $pa = explode(',', $room['bed_codes']);
                                                    foreach ($pa as $part) {
                                                        $pa2[] = $_uccms_properties->propertyBedTypes()[$part];
                                                    }
                                                    echo implode(', ', $pa2);
                                                } else {
                                                    echo 0;
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="description"><?php echo stripslashes($room['description']); ?></div>
                                    <div class="options items clearfix">
                                        <?php if ($room['shared']) { ?>
                                            <div class="shared item"><i class="fa fa-check" aria-hidden="true"></i>Shared</div>
                                        <?php } ?>
                                        <?php if ($room['has_ac']) { ?>
                                            <div class="has_ac item"><i class="fa fa-check" aria-hidden="true"></i>Has AC</div>
                                        <?php } ?>
                                        <?php if ($room['has_ceiling_fan']) { ?>
                                            <div class="has_ceiling_fan item"><i class="fa fa-check" aria-hidden="true"></i>Has Ceiling Fan</div>
                                        <?php } ?>
                                        <?php if ($room['has_fireplace']) { ?>
                                            <div class="has_fireplace item"><i class="fa fa-check" aria-hidden="true"></i>Has Fireplace</div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <?php

                // RATINGS / REVIEWS
                if ($_uccms_properties->getSetting('ratings_reviews_enabled')) {

                    // VARS FOR REVIEW
                    $reviews_vars = array(
                        'source'    => $_uccms_properties->Extension,
                        'what'      => 'property',
                        'item_id'   => $prop['id']
                    );

                    // IS OWNER
                    $_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'reply', (($prop['agent']['account_id'] == $_uccms['_account']->userID()) || ($prop['agency']['account_id'] == $_uccms['_account']->userID())));

                    ?>
                    <div class="reviews">
                        <h3>Reviews</h3>
                        <div class="content">
                            <?php include(SERVER_ROOT. 'templates/elements/ratings-reviews/reviews.php'); ?>
                        </div>
                    </div>

                    <?php

                }

                ?>

            </div>

        </div>

        <div class="smallColumn">

            <div class="price">
                <span class="from"><span class="sign">$</span><?php echo $prop['price_from_formatted']; ?></span><?php if ($prop['price_to'] != 0.00) { ?> - <span class="to"><span class="sign">$</span><?php echo $prop['price_to_formatted']; ?></span><?php } ?><?php if ($prop['price_term_code']) { ?><span class="term">/<?php echo $_uccms_properties->propertyPriceTerms()[$prop['price_term_code']]; ?></span><?php } ?>
            </div>

            <?php if (($prop['agent']['id']) || ($prop['agency']['id'])) { ?>
                <div class="agent_agency">
                    <?php if ($prop['agent']['id']) { ?>
                        <div class="by agent">
                            <?php if ($prop['agent']['image']) { ?>
                                <a href="<?php echo $_uccms_properties->agentURL($prop['agent']['id'], $prop['agent']); ?>"><img src="<?php echo $_uccms_properties->imageBridgeOut($prop['agent']['image'], 'agents'); ?>" alt="<?php echo stripslashes($prop['agent']['name']); ?>" class="main" /></a>
                            <?php } ?>
                            <div class="name"><a href="<?php echo $_uccms_properties->agentURL($prop['agent']['id'], $prop['agent']); ?>"><?php echo stripslashes($prop['agent']['name']); ?></a></div>
                        </div>
                    <?php } else if ($prop['agency']['id']) { ?>
                        <div class="by agency">
                            <?php if ($prop['agency']['image']) { ?>
                                <a href="<?php echo $_uccms_properties->agencyURL($prop['agency']['id'], $prop['agency']); ?>"><img src="<?php echo $_uccms_properties->imageBridgeOut($prop['agency']['image'], 'agencies'); ?>" alt="<?php echo stripslashes($prop['agency']['title']); ?>" class="main" /></a>
                            <?php } ?>
                            <div class="name"><a href="<?php echo $_uccms_properties->agencyURL($prop['agency']['id'], $prop['agency']); ?>"><?php echo stripslashes($prop['agency']['title']); ?></a></div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php

            // CONTACT FORM ENABLED
            if (($_uccms_properties->getSetting('listing_contact_enabled')) && (($prop['agent']['email']) || ($prop['agent']['email_lead']) || ($prop['agency']['email']) || ($prop['agency']['email_lead'])) && (count($_uccms_properties->listingContactFields()) > 0)) {
                ?>
                <div class="contact">
                    <h3>Contact</h3>
                    <div class="site-message type-success"></div>
                    <div class="site-message type-error"></div>
                    <div>
                        <form action="" method="post">
                        <input type="hidden" name="property_id" value="<?php echo $prop['id']; ?>" />
                        <?php foreach ($_uccms_properties->listingContactFields() as $field_id => $field) { ?>
                            <div class="field">
                                <input type="text" name="field[<?php echo $field_id; ?>]" value="" placeholder="<?php echo $field['title']; ?>" />
                            </div>
                        <?php } ?>
                        <?php
                        $captcha_el = '#propertiesContainer .contact form .captcha .recaptcha';
                        include(SERVER_ROOT. 'templates/elements/captcha.php');
                        ?>
                        <div class="buttons">
                            <input type="submit" value="Submit" class="button" />
                        </div>
                        </form>
                    </div>
                </div>
                <?php
            }

            ?>

        </div>

    </div>

    <script type="text/javascript">

        <?php if (count($prop['images']) > 0) { ?>
            $(document).ready(function() {
                imagesLoaded('#propertiesContainer .property-full .images_container .thumbs', function() {
                    $('#propertiesContainer .property-full .images_container .thumbs').masonry({
                        itemSelector: '.thumb',
                        columnWidth: 1
                    });
                });
            });
        <?php } ?>

    </script>

    <?php

// NO PROPERTIES
} else {
    ?>
    <div class="no_properties">
        Property not found.
    </div>
    <?php
}

?>