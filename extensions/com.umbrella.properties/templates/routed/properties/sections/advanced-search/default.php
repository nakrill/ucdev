<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/lib/nouislider.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/lib/nouislider.min.js"></script>
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/advanced-search.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/custom/advanced-search.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/master/advanced-search.js"></script>
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/custom/advanced-search.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<?php

$var_arrays = array(
    'city_id',
    'neighborhood_id',
    'property_type_code',
    'rental_space_code',
    'location_code',
    'rental_bathrooms_code',
    'cooling_type_code',
    'heating_type_code',
    'washer_dryer_code',
    'parking_type_code',
    'gated_type_code',
    'pool_type_code',
    'pet_type_code'
);
foreach ($var_arrays as $var_id) {
    if (!is_array($_REQUEST[$var_id])) $_REQUEST[$var_id] = array();
}

?>

<h1>Advanced Search</h1>

<form action="/<?php echo $_uccms_properties->frontendPath(); ?>/" method="get">

<div class="search-box advanced clearfix">

    <div class="column">

        <div class="option">
            <div class="label">Property ID</div>
            <input type="text" name="property_id" value="<?php echo $_REQUEST['property_id']; ?>" />
        </div>

        <div class="option">
            <div class="label">Listing Type</div>
            <select name="listing_type">
                <option value="">All</option>
                <?php foreach ($_uccms_properties->listingTypes as $type_id => $type_title) { ?>
                    <option value="<?php echo $type_id; ?>" <?php if ($type_id == $_REQUEST['listing_type']) { ?>selected="selected"<?php } ?>><?php echo $type_title; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="option lt-rental">
            <div class="label">Rental Space</div>
            <div class="items checkboxes">
                <?php foreach ($_uccms_properties->propertyRentalSpaces() as $rs_code => $rs_title) { ?>
                    <div class="item"><input type="checkbox" name="rental_space_code[]" value="<?php echo $rs_code; ?>" <?php if (in_array($rs_code, $_REQUEST['rental_space_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $rs_title; ?></div>
                <?php } ?>
            </div>
        </div>

        <div class="option">
            <div class="label">City</div>
            <div class="items checkboxes">
                <?php
                $city_query = "SELECT * FROM `" .$_uccms_properties->tables['cities']. "` ORDER BY `title` ASC, `id` ASC";
                $city_q = sqlquery($city_query);
                while ($city = sqlfetch($city_q)) {
                    ?>
                    <div class="item"><input type="checkbox" name="city_id[]" value="<?php echo $city['id']; ?>" <?php if (in_array($city['id'], $_REQUEST['city_id'])) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($city['title']); ?></div>
                    <?php
                }
                ?>
            </div>
        </div>

        <div id="neighborhoods" class="option">
            <div class="label">Neighborhood</div>
            <div class="items checkboxes">
                <?php
                $neighborhood_query = "SELECT * FROM `" .$_uccms_properties->tables['neighborhoods']. "` ORDER BY `title` ASC, `id` ASC";
                $neighborhood_q = sqlquery($neighborhood_query);
                while ($neighborhood = sqlfetch($neighborhood_q)) {
                    ?>
                    <div class="item city-<?php echo $neighborhood['city_id']; ?>" style="<?php if (!in_array($neighborhood['city_id'], $_REQUEST['city_id'])) { ?>display: none;<?php } ?>"><input type="checkbox" name="neighborhood_id[]" value="<?php echo $neighborhood['id']; ?>" <?php if (in_array($neighborhood['id'], $_REQUEST['neighborhood_id'])) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($neighborhood['title']); ?></div>
                    <?php
                }
                ?>
            </div>
        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">City</div>
                <input type="text" name="city" value="<?php echo $_REQUEST['city']; ?>" />
            </div>

            <div class="option">
                <div class="label">State</div>
                <input type="text" name="state" value="<?php echo $_REQUEST['state']; ?>" />
            </div>

            <div class="option">
                <div class="label">Zip</div>
                <input type="text" name="zip" value="<?php echo $_REQUEST['zip']; ?>" />
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Price - Min</div>
                $<input type="text" name="price_min" value="<?php if ($_REQUEST['price_min']) { echo number_format($_uccms_properties->toFloat($_REQUEST['price_min'], true), 2); } ?>" />
            </div>

            <div class="option">
                <div class="label">Price - Max</div>
                $<input type="text" name="price_max" value="<?php if ($_REQUEST['price_max']) { echo number_format($_uccms_properties->toFloat($_REQUEST['price_max'], true), 2); } ?>" />
            </div>

            <div class="option lt-rental">
                <div class="label">Price Per</div>
                <select name="price_term_code">
                    <option value="">Select</option>
                    <?php foreach ($_uccms_properties->propertyPriceTerms() as $pt_code => $pt_title) { ?>
                        <option value="<?php echo $pt_code; ?>" <?php if ($pt_code == $_REQUEST['price_term_code']) { ?>selected="selected"<?php } ?>><?php echo $pt_title; ?></option>
                    <?php } ?>
                </select>
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Rating - Min</div>
                <select name="rating_min">
                    <?php for ($i=1; $i<=5; $i++) { ?>
                        <option value="<?php echo $i; ?>" <?php if ($i == $_REQUEST['rating_min']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="option">
                <div class="label">Rating - Max</div>
                <select name="rating_max">
                    <?php for ($i=1; $i<=5; $i++) { ?>
                        <option value="<?php echo $i; ?>" <?php if ($i == $_REQUEST['rating_max']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label"># Reviews - Min</div>
                <input type="text" name="reviews_min" value="<?php if ($_REQUEST['reviews_min']) { echo number_format($_REQUEST['reviews_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label"># Reviews - Max</div>
                <input type="text" name="reviews_max" value="<?php if ($_REQUEST['reviews_max']) { echo number_format($_REQUEST['reviews_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">SqFt - Min</div>
                <input type="text" name="sqft_min" value="<?php if ($_REQUEST['sqft_min']) { echo number_format($_REQUEST['sqft_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label">SqFt - Max</div>
                <input type="text" name="sqft_max" value="<?php if ($_REQUEST['sqft_max']) { echo number_format($_REQUEST['sqft_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Accommodates - Min</div>
                <input type="text" name="accommodates_num_min" value="<?php if ($_REQUEST['accommodates_num_min']) { echo number_format($_REQUEST['accommodates_num_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label">Accommodates - Max</div>
                <input type="text" name="accommodates_num_max" value="<?php if ($_REQUEST['accommodates_num_max']) { echo number_format($_REQUEST['accommodates_num_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Bedrooms - Min</div>
                <input type="text" name="bedrooms_num_min" value="<?php if ($_REQUEST['bedrooms_num_min']) { echo number_format($_REQUEST['bedrooms_num_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label">Bedrooms - Max</div>
                <input type="text" name="bedrooms_num_max" value="<?php if ($_REQUEST['bedrooms_num_max']) { echo number_format($_REQUEST['bedrooms_num_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Bathrooms - Min</div>
                <input type="text" name="bathrooms_num_min" value="<?php if ($_REQUEST['bathrooms_num_min']) { echo number_format($_REQUEST['bathrooms_num_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label">Bathrooms - Max</div>
                <input type="text" name="bathrooms_num_max" value="<?php if ($_REQUEST['bathrooms_num_max']) { echo number_format($_REQUEST['bathrooms_num_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option lt-rental">
            <div class="label">Bathroom Access</div>
            <div class="items checkboxes">
                <?php foreach ($_uccms_properties->propertyRentalBathrooms() as $rb_code => $rb_title) { ?>
                    <div class="item"><input type="checkbox" name="rental_bathrooms_code[]" value="<?php echo $rb_code; ?>" <?php if (in_array($rb_code, $_REQUEST['rental_bathrooms_code'])) { ?>checked="checked"<?php } ?>><?php echo $rb_title; ?></div>
                <?php } ?>
            </div>
        </div>

        <div class="option_row clearfix">

            <div class="option">
                <input type="checkbox" name="has_cooling" value="1" <?php if ($_REQUEST['has_cooling']) { ?>checked="checked"<?php } ?> class="toggle-options" data-toggle=".cooling_type" /> Has Cooling
            </div>

            <div class="option cooling_type <?php if (!$_REQUEST['has_cooling']) { echo 'hidden'; } ?>">
                <div class="label">Cooling Type</div>
                <div class="items checkboxes">
                    <?php foreach ($_uccms_properties->propertyCoolingTypes() as $ct_code => $ct_title) { ?>
                        <div class="item">
                            <input type="checkbox" name="cooling_type_code[]" value="<?php echo $ct_code; ?>" <?php if (in_array($ct_code, $_REQUEST['cooling_type_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $ct_title; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <input type="checkbox" name="has_heating" value="1" <?php if ($_REQUEST['has_heating']) { ?>checked="checked"<?php } ?> class="toggle-options" data-toggle=".heating_type" /> Has Heating
            </div>

            <div class="option heating_type <?php if (!$_REQUEST['has_heating']) { echo 'hidden'; } ?>">
                <div class="label">Heating Type</div>
                <div class="items checkboxes">
                    <?php foreach ($_uccms_properties->propertyHeatingTypes() as $ht_code => $ht_title) { ?>
                        <div class="item">
                            <input type="checkbox" name="heating_type_code[]" value="<?php echo $ht_code; ?>" <?php if (in_array($ht_code, $_REQUEST['heating_type_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $ht_title; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="option heating_type <?php if (!$_REQUEST['has_heating']) { echo 'hidden'; } ?>">
                <div class="label">Fireplaces - Min</div>
                <input type="text" name="fireplaces_num_min" value="<?php if ($_REQUEST['fireplaces_num_min']) { echo number_format($_REQUEST['fireplaces_num_min'], 0); } ?>" />
            </div>

            <div class="option heating_type <?php if (!$_REQUEST['has_heating']) { echo 'hidden'; } ?>">
                <div class="label">Fireplaces - Max</div>
                <input type="text" name="fireplaces_num_max" value="<?php if ($_REQUEST['fireplaces_num_max']) { echo number_format($_REQUEST['fireplaces_num_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <input type="checkbox" name="has_washer_dryer" value="1" <?php if ($_REQUEST['has_washer_dryer']) { ?>checked="checked"<?php } ?> class="toggle-options" data-toggle=".washer_dryer_type" /> Has Washer / Dryer
            </div>

            <div class="option washer_dryer_type <?php if (!$_REQUEST['has_washer_dryer']) { echo 'hidden'; } ?>">
                <div class="label">Washer / Dryer Type</div>
                <div class="items checkboxes">
                    <?php foreach ($_uccms_properties->propertyWasherDryerTypes() as $wd_code => $wd_title) { ?>
                        <div class="item">
                            <input type="checkbox" name="washer_dryer_code[]" value="<?php echo $wd_code; ?>" <?php if (in_array($wd_code, $_REQUEST['washer_dryer_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $wd_title; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Elevators - Min</div>
                <input type="text" name="elevators_num_min" value="<?php if ($_REQUEST['elevators_num_min']) { echo number_format($_REQUEST['elevators_num_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label">Elevators - Max</div>
                <input type="text" name="elevators_num_max" value="<?php if ($_REQUEST['elevators_num_max']) { echo number_format($_REQUEST['elevators_num_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option">
            <input type="checkbox" name="has_wifi" value="1" <?php if ($_REQUEST['has_wifi']) { ?>checked="checked"<?php } ?> /> Has WiFi
        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Parking - Min</div>
                <input type="text" name="parking_num_min" value="<?php if ($_REQUEST['parking_num_min']) { echo number_format($_REQUEST['parking_num_min'], 0); } ?>" class="toggle-options" data-toggle=".parking_type" />
            </div>

            <div class="option">
                <div class="label">Parking - Max</div>
                <input type="text" name="parking_num_max" value="<?php if ($_REQUEST['parking_num_max']) { echo number_format($_REQUEST['parking_num_max'], 0); } ?>" />
            </div>

            <div class="option parking_type <?php if (!$_REQUEST['parking_num_min']) { echo 'hidden'; } ?>">
                <div class="label">Parking Type</div>
                <div class="items checkboxes">
                    <?php foreach ($_uccms_properties->propertyParkingTypes() as $pt_code => $pt_title) { ?>
                        <div class="item">
                            <input type="checkbox" name="parking_type_code[]" value="<?php echo $pt_code; ?>" <?php if (in_array($pt_code, $_REQUEST['parking_type_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $pt_title; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <input type="checkbox" name="is_gated" value="1" <?php if ($_REQUEST['is_gated']) { ?>checked="checked"<?php } ?> class="toggle-options" data-toggle=".gated_type" /> Is Gated
            </div>

            <div class="option gated_type <?php if (!$_REQUEST['is_gated']) { echo 'hidden'; } ?>">
                <div class="label">Gated Type</div>
                <div class="items checkboxes">
                    <?php foreach ($_uccms_properties->propertyGatedTypes() as $gt_code => $gt_title) { ?>
                        <div class="item">
                            <input type="checkbox" name="gated_type_code[]" value="<?php echo $gt_code; ?>" <?php if (in_array($gt_code, $_REQUEST['gated_type_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $gt_title; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <input type="checkbox" name="has_pool" value="1" <?php if ($_REQUEST['has_pool']) { ?>checked="checked"<?php } ?> class="toggle-options" data-toggle=".pool_type" /> Has Pool
            </div>

            <div class="option pool_type <?php if (!$_REQUEST['has_pool']) { echo 'hidden'; } ?>">
                <div class="label">Pool Type</div>
                <div class="items checkboxes">
                    <?php foreach ($_uccms_properties->propertyPoolTypes() as $pt_code => $pt_title) { ?>
                        <div class="item">
                            <input type="checkbox" name="pool_type_code[]" value="<?php echo $pt_code; ?>" <?php if (in_array($pt_code, $_REQUEST['pool_type_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $pt_title; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>

        <div class="option_row lt-rental clearfix">

            <div class="option">
                <div class="label">Pets - Min</div>
                <input type="text" name="pets_num_min" value="<?php if ($_REQUEST['pets_num_min']) { echo number_format($_REQUEST['pets_num_min'], 0); } ?>" class="toggle-options" data-toggle=".pet_type" />
            </div>

            <div class="option">
                <div class="label">Pets - Max</div>
                <input type="text" name="pets_num_max" value="<?php if ($_REQUEST['pets_num_max']) { echo number_format($_REQUEST['pets_num_max'], 0); } ?>" />
            </div>

            <div class="option pet_type <?php if (!$_REQUEST['pets_num_min']) { echo 'hidden'; } ?>">
                <div class="label">Pet Type</div>
                <div class="items checkboxes">
                    <?php foreach ($_uccms_properties->propertyPetTypes() as $pt_code => $pt_title) { ?>
                        <div class="item">
                            <input type="checkbox" name="pet_type_code[]" value="<?php echo $pt_code; ?>" <?php if (in_array($pt_code, $_REQUEST['pet_type_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $pt_title; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>

    </div>

    <div class="column">

        <div class="option">
            <div class="label">Keyword</div>
            <input type="text" name="query" value="<?php echo $_REQUEST['query']; ?>" />
        </div>

        <div class="option">
            <div class="label">Property ID</div>
            <input type="text" name="id" value="<?php echo $_REQUEST['id']; ?>" />
        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Agency</div>
                <select name="agency_id">
                    <option value="">Select</option>
                    <?php
                    $agencies_query = "
                    SELECT ay.*
                    FROM `" .$_uccms_properties->tables['agencies']. "` AS `ay`
                    WHERE (ay.status=1)
                    ORDER BY ay.title ASC, ay.id ASC
                    ";
                    $agencies_q = sqlquery($agencies_query);
                    while ($agency = sqlfetch($agencies_q)) {
                        ?>
                        <option value="<?php echo $agency['id']; ?>" <?php if ($agency['id'] == $_REQUEST['agency_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($agency['title']); ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="option">
                <div class="label">Agent</div>
                <select name="agent_id">
                    <option value="">Select</option>
                    <?php
                    $agent_query = "
                    SELECT at.*
                    FROM `" .$_uccms_properties->tables['agents']. "` AS `at`
                    WHERE (at.status=1)
                    ORDER BY at.name ASC, at.id ASC
                    ";
                    $agent_q = sqlquery($agent_query);
                    while ($agent = sqlfetch($agent_q)) {
                        ?>
                        <option value="<?php echo $agent['id']; ?>" <?php if ($agent['id'] == $_REQUEST['agent_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($agent['name']); ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

        </div>

        <div class="option">
            <div class="label">Property Type</div>
            <div class="items checkboxes">
                <?php foreach ($_uccms_properties->propertyTypes() as $type_code => $type_title) { ?>
                    <div class="item"><input type="checkbox" name="property_type_code[]" value="<?php echo $type_code; ?>" <?php if (in_array($type_code, $_REQUEST['property_type_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $type_title; ?></div>
                <?php } ?>
            </div>
        </div>

        <div class="option">
            <div class="label">Location</div>
            <div class="items checkboxes">
                <?php foreach ($_uccms_properties->propertyLocations() as $loc_code => $loc) { ?>
                    <div class="item"><input type="checkbox" name="location_code[]" value="<?php echo $loc_code; ?>" <?php if (in_array($loc_code, $_REQUEST['location_code'])) { ?>checked="checked"<?php } ?> /> <?php echo $loc['title']; ?> <span class="note"><?php echo $loc['description']; ?></span></div>
                <?php } ?>
            </div>
        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Acres - Min</div>
                <input type="text" name="lot_acres_min" value="<?php if ($_REQUEST['lot_acres_min']) { echo number_format($_REQUEST['lot_acres_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label">Acres - Max</div>
                <input type="text" name="lot_acres_max" value="<?php if ($_REQUEST['lot_acres_max']) { echo number_format($_REQUEST['lot_acres_max'], 0); } ?>" />
            </div>

        </div>

        <div class="option_row clearfix">

            <div class="option">
                <div class="label">Year Built - Min</div>
                <input type="text" name="year_built_min" value="<?php if ($_REQUEST['year_built_min']) { echo number_format($_REQUEST['year_built_min'], 0); } ?>" />
            </div>

            <div class="option">
                <div class="label">Year Built - Max</div>
                <input type="text" name="year_built_max" value="<?php if ($_REQUEST['year_built_max']) { echo number_format($_REQUEST['year_built_max'], 0); } ?>" />
            </div>

        </div>

    </div>

    <div class="submit">
        <input type="submit" value="Search" />
    </div>

</div>

</form>