<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/property-box.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/custom/property-box.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/agent-agency.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/custom/agent-agency.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/master/agent-agency.js"></script>
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/custom/agent-agency.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="agent_agency-profile" itemscope itemtype="http://schema.org/ProfilePage">

    <?php

    // HAVE PROFILE
    if ($profile['id']) {

        ?>

        <div itemprop="about" itemscope itemtype="http://schema.org/Person">

            <div class="clearfix <?php echo $profile['what']; ?>">

                <div class="colSmall">

                    <div class="info">

                        <?php if ($profile['image']) { ?>
                            <img src="<?php echo $_uccms_properties->imageBridgeOut($profile['image'], $profile['whats']); ?>" alt="<?php echo stripslashes($profile['name']); ?>" class="main" itemprop="image" />
                        <?php } ?>

                    </div>

                </div>

                <div class="colLarge">

                    <h1 class="name" itemprop="name"><?php echo stripslashes($profile['name']); ?></h1>

                    <div class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <?php if ($profile['address1']) { ?>
                            <span itemprop="streetAddress"><?php echo stripslashes($profile['address1']); ?><?php if ($profile['address2']) { echo ', ' .stripslashes($profile['address2']); } ?></span>
                            <br />
                        <?php } ?>
                        <?php if ($profile['city']) { ?>
                            <span itemprop="addressLocality"><?php echo stripslashes($profile['city']); ?>, </span>
                        <?php } ?>
                        <?php if ($profile['state']) { ?>
                            <span itemprop="addressRegion"><?php echo stripslashes($profile['state']); ?></span>
                        <?php } ?>
                        <?php if ($profile['zip']) { ?>
                            <span itemprop="postalCode"><?php echo stripslashes($profile['zip']); ?></span>
                        <?php } ?>
                    </div>

                    <?php if ($profile['phone']) { ?>
                        <div class="phone" itemprop="telephone"><?php echo $_uccms_properties->prettyPhone($profile['phone']); ?><?php if ($profile['phone_ext']) { ?> Ext: <?php echo $profile['phone_ext']; ?><?php } ?></div>
                    <?php } ?>

                    <?php if ($profile['phone_mobile']) { ?>
                        <div class="phone mobile" itemprop="telephone"><?php echo $_uccms_properties->prettyPhone($profile['phone_mobile']); ?></div>
                    <?php } ?>

                    <?php if ($profile['url']) { ?>
                        <div class="website" itemprop="url"><a href="http://<?php echo stripslashes($profile['url']); ?>" target="_blank"><?php echo stripslashes($profile['url']); ?></a></div>
                    <?php } ?>

                </div>

            </div>

            <?php

            // HAVE PROPERTIES
            if (count($properties['properties']) > 0) {

                ?>

                <div class="listings">

                    <div class="head clearfix">
                        <h2>Listings by <?php echo stripslashes($profile['name']); ?></h2>
                        <a href="/<?php echo $_uccms_properties->frontendPath(); ?>/?<?php echo $profile['what']; ?>_id=<?php echo $profile['id']; ?>">View All</a>
                    </div>

                    <div class="properties style-grid clearfix">

                        <?php

                        // LOOP
                        foreach ($properties['properties'] as $prop_id => $prop) {

                            // GET PROPERTY
                            $prop = $_uccms_properties->getProperty($prop_id, true, array('agent','images'));

                            // DISPLAY
                            include(SERVER_ROOT. 'extensions/' .$_uccms_properties->Extension. '/templates/routed/properties/elements/property-grid.php');

                        }

                        ?>

                    </div>

                </div>

                <?php

            // NO PROPERTIES
            } else {
                ?>
                <div class="no_properties">
                    No properties found.
                </div>
                <?php
            }

            ?>

        </div>

        <?php

    } else {
        ?>
        <div class="no_properties">
            Profile not found.
        </div>
        <?php
    }

    ?>

</div>