<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/lib/nouislider.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/lib/nouislider.min.js"></script>
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/property-box.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/custom/property-box.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/home.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/custom/home.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/master/home.js"></script>
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/custom/home.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="clearfix">

    <?php

    // TOP SEARCH / FILTER BOX
    include(SERVER_ROOT. 'extensions/' .$_uccms_properties->Extension. '/templates/routed/properties/elements/search-box-horizontal.php');

    // HAVE CITY ID
    if ($city['id']) {

        // HAVE CITY TITLE
        if ($city['title']) {
            ?>
            <h2 class="location">
                <?php if ($neighborhood['id']) { ?>
                    <a href="../"><?php echo stripslashes($city['title']); ?></a> <i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo stripslashes($neighborhood['title']); ?>
                <?php } else { ?>
                    <?php echo stripslashes($city['title']); ?>
                <?php } ?>
            </h2>
            <?php
        }

        // HAVE PROPERTIES
        if (count($properties['properties']) > 0) {

            ?>

            <?php if ($pages) { ?>
                <form action="./" method="get">
                    <?php
                    $search_ignore = array('bigtree_htaccess_url','sort');
                    foreach ($_GET as $key => $val) {
                        if (($val) && (!in_array($key, $search_ignore))) {
                            ?>
                            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>" />
                            <?php
                        }
                    }
                    ?>
                    <div class="result_info top clearfix">
                        <div class="displaying">
                            Displaying <?php echo number_format($_paging->start, 0); ?> through <?php echo number_format($_paging->end, 0); ?> of <?php echo number_format($properties['num_total'], 0); ?> results.
                        </div>
                        <select name="sort">
                            <option value="price.asc" <?php if ($_REQUEST['sort'] == 'price.asc') { ?>selected="selected"<?php } ?>>Price - Lowest to Highest</option>
                            <option value="price.desc" <?php if ($_REQUEST['sort'] == 'price.desc') { ?>selected="selected"<?php } ?>>Price - Highest to Lowest</option>
                        </select>
                        <div class="paging">
                            <?php echo $pages; ?>
                        </div>
                    </div>
                </form>
            <?php } ?>

            <div class="properties style-<?php echo $display_style; ?> clearfix">

                <?php

                // LOOP
                foreach ($properties['properties'] as $prop_id => $prop) {

                    // GET PROPERTY
                    $prop = $_uccms_properties->getProperty($prop_id, true, array('agent','images'));

                    // DISPLAY
                    include(SERVER_ROOT. 'extensions/' .$_uccms_properties->Extension. '/templates/routed/properties/elements/property-' .$display_style. '.php');

                }

                ?>

            </div>

            <?php if ($pages) { ?>
                <div class="result_info bottom clearfix">
                    <div class="paging">
                        <?php echo $pages; ?>
                    </div>
                </div>
            <?php } ?>

            <?php

        // NO PROPERTIES
        } else {
            ?>
            <div class="no_properties">
                No properties found.
            </div>
            <?php
        }

    } else {
        ?>
        <div class="no_properties">
            City not found.
        </div>
        <?php
    }

    ?>

</div>