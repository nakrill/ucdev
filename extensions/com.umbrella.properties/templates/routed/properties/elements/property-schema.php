<div itemscope itemtype="http://schema.org/Product" style="display: none;">

    <?php if ($prop['title']) { ?>
        <span itemprop="name"><?php echo stripslashes($prop['title']); ?></span>
    <?php } ?>

    <?php if ($prop['description']) { ?>
        <span itemprop="description"><?php echo stripslashes($prop['description']); ?></span>
    <?php } ?>

    <?php if ($first_image['url']) { ?>
        <img itemprop="image" src="<?php echo $first_image['url']; ?>" />
    <?php } ?>

    <?php
    if ($_uccms_properties->getSetting('ratings_reviews_enabled')) {
        if ($prop['rating'] > 0) {
            ?>
            <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                <span itemprop="ratingValue"><?php echo $prop['rating']; ?></span>
                <span itemprop="reviewCount"><?php echo $prop['reviews']; ?></span>
            </div>
            <?php
        }
    }
    ?>

    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

        <div itemprop="availableAtOrFrom" itemscope itemtype="http://schema.org/Place">

            <?php if ($address) { ?>

                <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <?php if ($prop['address1']) { ?>
                        <span itemprop="streetAddress"><?php echo stripslashes($prop['address1']); ?><?php if ($prop['address2']) { echo ', ' .stripslashes($prop['address2']); } ?></span>
                    <?php } ?>
                    <?php if ($prop['city']) { ?>
                        <span itemprop="addressLocality"><?php echo stripslashes($prop['city']); ?></span>
                    <?php } ?>
                    <?php if ($prop['state']) { ?>
                        <span itemprop="addressRegion"><?php echo stripslashes($prop['state']); ?></span>
                    <?php } ?>
                    <?php if ($prop['zip']) { ?>
                        <span itemprop="postalCode"><?php echo stripslashes($prop['zip']); ?></span>
                    <?php } ?>
                </div>

                <a itemprop="hasMap" href="https://www.google.com/maps/embed/v1/place?q=<?php echo $address; ?>">map</a>

            <?php } ?>

        </div>

        <?php
        if ($prop['agency']['title']) {
            if ($prop['agent']['name']) {
                ?>
                <div itemprop="seller" itemscope itemtype="http://schema.org/RealEstateAgent">
                    <span itemprop="name"><?php echo stripslashes($prop['agent']['name']); ?></span>
                </div>
                <?php
            } else {
                ?>
                <div itemprop="seller" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name"><?php echo stripslashes($prop['agency']['title']); ?></span>
                </div>
                <?php
            }
        } else if ($prop['agent']['name']) {
            ?>
            <div itemprop="seller" itemscope itemtype="http://schema.org/Person">
                <span itemprop="name"><?php echo stripslashes($prop['agent']['name']); ?></span>
            </div>
            <?php
        }
        ?>

        <span itemprop="price"><?php echo $_uccms_properties->toFloat($prop['price_from']); ?></span>
        <meta itemprop="priceCurrency" content="USD" />

    </div>

</div>

<?php if (($prop['location']['city']['title']) || ($prop['location']['neighborhood']['title'])) { ?>
    <ol itemscope itemtype="http://schema.org/BreadcrumbList" style="display: none;">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="/<?php echo $_uccms_properties->frontendPath(); ?>/"><span itemprop="name">United States</span></a>
        </li>
        <? /*
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="/<?php echo $_uccms_properties->frontendPath(); ?>/?state=North+Carolina"><span itemprop="name">North Carolina</span></a>
        </li>
        */ ?>
        <?php if ($prop['location']['city']['title']) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php echo $_uccms_properties->cityURL($prop['location']['city']['id'], $prop['location']['city']); ?>"><span itemprop="name"><?php echo stripslashes($prop['location']['city']['title']); ?></span></a>
            </li>
        <?php } ?>
        <?php if ($prop['location']['neighborhood']['title']) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php echo $_uccms_properties->neighborhoodURL($prop['location']['neighborhood']['id'], $prop['location']['neighborhood']); ?>"><span itemprop="name"><?php echo stripslashes($prop['location']['neighborhood']['title']); ?></span></a>
            </li>
        <?php } ?>
    </ol>
<?php } ?>