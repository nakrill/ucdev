<?php

// NUMBER OF PAYMENT METHODS
$num_payment_methods = count($payment_methods);

// HAVE PAYMENT METHODS
if ($num_payment_methods > 0) {

    // PAYMENT METHODS
    foreach ($payment_methods as $payment) {

        ?>

        <div class="method">
            <div class="top">
                <input type="radio" name="payment[method]" value="<?php echo $payment['id']; ?>" <?php if (($payment['id'] == $vals['payment']['method']) || ($num_payment_methods == 1)) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($payment['title']); ?>
            </div>
            <div class="info" style="<?php if ($num_payment_methods > 1) { if ($payment['id'] != $vals['payment']['method']) { ?>display: none;<?php } } ?>">

                <?php if ($payment['description']) { ?>
                    <div class="description"><?php echo nl2br(stripslashes($payment['description'])); ?></div>
                <?php } ?>

                <?php

                // CARD PAYMENT
                if ($payment['id'] != 'check_cash') {

                    // LOGGED IN AND HAVE PAYMENT PROFILE(S)
                    if (($_uccms['_account']->userID()) && (count($payment_profiles) > 0)) { ?>
                        <div class="payment_profile">
                            <fieldset>
                                <label>Your Cards</label>
                                <select name="payment[profile_id]">
                                    <?php foreach ($payment_profiles as $pp) { ?>
                                        <option value="<?php echo $pp['id']; ?>" <?php if ($pp['id'] == $form_session['payment']['profile_id']) { ?>selected="selected"<?php } ?>><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                                    <?php } ?>
                                    <option value="">New Card</option>
                                </select>
                            </fieldset>
                        </div>
                    <?php } ?>

                    <div id="card_new" style="<?php if (($_uccms['_account']->userID()) && (count($payment_profiles) > 0)) { ?>display: none;<?php } ?>">

                        <fieldset>
                            <label>Name On Card *</label>
                            <input type="text" name="payment[<?php echo $payment['id']; ?>][name]" value="<?php echo $vals['payment'][$payment['id']]['name']; ?>" />
                        </fieldset>

                        <fieldset>
                            <label>Card Number *</label>
                            <input type="text" name="payment[<?php echo $payment['id']; ?>][number]" value="<?php echo $vals['payment'][$payment['id']]['number']; ?>" />
                        </fieldset>

                        <fieldset>
                            <label>Card Expiration *</label>
                            <input type="text" name="payment[<?php echo $payment['id']; ?>][expiration]" value="<?php echo $vals['payment'][$payment['id']]['expiration']; ?>" placeholder="mm/yy" />
                        </fieldset>

                        <fieldset>
                            <label>Card Zip *</label>
                            <input type="text" name="payment[<?php echo $payment['id']; ?>][zip]" value="<?php echo $vals['payment'][$payment['id']]['zip']; ?>" />
                        </fieldset>

                        <fieldset>
                            <label>Card CVV *</label>
                            <input type="text" name="payment[<?php echo $payment['id']; ?>][cvv]" value="<?php echo $vals['payment'][$payment['id']]['cvv']; ?>" />
                        </fieldset>

                        <div class="credit_card_logos">
                            <img src="/extensions/<?php echo $_uccms_properties->Extension; ?>/images/credit-card-icons.png" class="credit-cards" />
                        </div>

                    </div>

                    <?php

                }

                ?>

            </div>
        </div>

        <?php

    }

    ?>

    <script type="text/javascript">
        $(document).ready(function() {

            // CARD SELECT
            $('.method select[name="payment[profile_id]"').change(function() {
                if ($(this).val() != '') {
                    $('#card_new').hide();
                } else {
                    $('#card_new').show();
                }
            });

        });
    </script>

    <?php

// NO PAYMENT METHODS
} else {
    ?>
    No payment methods configured.
    <?php
}

?>