<div class="search-box horizontal clearfix">

    <form action="<?php if (count($bigtree['path']) > 1) { echo parse_url($_SERVER['REQUEST_URI'])['path']; } else { echo '/'. $_uccms_properties->frontendPath(). '/'; } ?>" method="get">
        <?php
        $search_ignore = array('bigtree_htaccess_url','city_id','neighborhood_id','price_max','accommodates_num_min','bedrooms_num_min','bathrooms_num_min');
        foreach ($_GET as $key => $val) {
            if (($val) && (!in_array($key, $search_ignore))) {
                ?>
                <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>" />
                <?php
            }
        }
        ?>

        <div class="option">
            $<input type="text" name="price_max" value="<?php if ($_REQUEST['price_max']) { echo number_format($_uccms_properties->toFloat($_REQUEST['price_max'], true), 2); } ?>" placeholder="Max Price" />
        </div>

        <div class="option">
            <select name="accommodates_num_min">
                <option value="">Accommodates</option>
                <?php for ($i=1; $i<=100; $i++) { ?>
                    <option value="<?php echo $i; ?>" <?php if ($i == $_REQUEST['accommodates_num_min']) { ?>selected="selected"<?php } ?>><?php echo $i; ?>+</option>
                <?php } ?>
            </select>
        </div>

        <div class="option">
            <select name="bedrooms_num_min">
                <option value="">Bedrooms</option>
                <?php for ($i=1; $i<=10; $i++) { ?>
                    <option value="<?php echo $i; ?>" <?php if ($i == $_REQUEST['bedrooms_num_min']) { ?>selected="selected"<?php } ?>><?php echo $i; ?>+</option>
                <?php } ?>
            </select>
        </div>

        <div class="option">
            <select name="bathrooms_num_min">
                <option value="">Bathrooms</option>
                <?php
                $i = .5;
                while ($i <= 10) {
                    ?>
                    <option value="<?php echo $i; ?>" <?php if ($i == $_REQUEST['bathrooms_num_min']) { ?>selected="selected"<?php } ?>><?php echo $i; ?>+</option>
                    <?php
                    $i += .5;
                }
                ?>
            </select>
        </div>

        <div class="submit">
            <input type="submit" value="Search" />
            <a href="/<?php echo $_uccms_properties->frontendPath(); ?>/advanced-search/<?php echo uccms_removeURLVar($_SERVER['QUERY_STRING'], 'bigtree_htaccess_url'); ?>" class="advanced">Advanced Search</a>
        </div>

    </form>

</div>