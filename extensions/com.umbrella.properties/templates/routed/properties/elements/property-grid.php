<?php

// PROPERTY URL
$prop_url = $_uccms_properties->propertyURL($prop['id'], $prop['city_id'], $prop['neighborhood_id'], $prop);

// NUMBER OF IMAGES
$num_images = count($prop['images']);

// MAIN IMAGE
if ($num_images > 0) {
    $image = $_uccms_properties->imageBridgeOut($prop['images'][array_keys($prop['images'])[0]]['image'], 'properties');
} else {
    $image = '/extensions/' .$_uccms_properties->Extension. '/images/property_no-image.jpg';
}

// FORMATTED ADDRESS
$address = $_uccms_properties->formatAddress($prop, ' ');

?>

<div class="property">
    <div class="container">
        <div class="image">
            <a href="<?php echo $prop_url; ?>" class="click" style="background-image: url('<?php echo $image; ?>');"></a>
            <div class="type">For <?php if ($prop['listing_type'] == 2) { ?>Rent<?php } else { ?>Sale<?php } ?></div>
            <div class="price"><span class="from"><span class="sign">$</span><?php echo $prop['price_from_formatted']; ?></span><?php if ($prop['price_to'] != 0.00) { ?> - <span class="to"><span class="sign">$</span><?php echo $prop['price_to_formatted']; ?></span><?php } ?><?php if ($prop['price_term_code']) { ?><span class="term">/<?php echo $_uccms_properties->propertyPriceTerms()[$prop['price_term_code']]; ?></span><?php } ?></div>
            <?php if ($prop['sqft'] > 0) { ?>
                <div class="sqft"><span class="num"><?php echo number_format($prop['sqft'], 0); ?></span><span class="label">/sq ft</span></div>
            <?php } ?>
            <?php if ($num_images > 0) { ?>
                <a href="<?php echo $prop_url; ?>" class="images"><i class="fa fa-camera" aria-hidden="true"></i><?php echo $num_images; ?></a>
            <?php } ?>
        </div>
        <div class="info">
            <?php if ($prop['title']) { ?>
                <div class="title"><a href="<?php echo $prop_url; ?>"><?php echo stripslashes($prop['title']); ?></a></div>
            <?php } ?>
            <?php if ($address) { ?>
                <div class="address">
                    <?php echo $address; ?>
                </div>
            <?php } ?>
            <div class="clearfix">
                <div class="quick clearfix">
                    <?php if ($prop['bedrooms_num'] > 0) { ?>
                        <div class="item bedrooms" title="Bedrooms"><i class="fa fa-bed" aria-hidden="true"></i><?php echo number_format($prop['bedrooms_num'], 0); ?></div>
                    <?php } ?>
                    <?php if ($prop['bathrooms_num'] > 0) { ?>
                        <div class="item bathrooms" title="Bathrooms"><i class="fa fa-tint" aria-hidden="true"></i><?php echo number_format($prop['bathrooms_num'], 0); ?></div>
                    <?php } ?>
                </div>
                <div class="more_button">
                    <a href="<?php echo $prop_url; ?>" class="button">Details</a>
                </div>
            </div>
        </div>
        <div class="bottom clearfix">
            <?php if (($prop['agent']['name']) || ($prop['agency']['title'])) { ?>
                <div class="by">
                    <i class="fa fa-user" aria-hidden="true"></i><?php if ($prop['agent']['name']) { echo stripslashes($prop['agent']['name']); } else { echo stripslashes($prop['agency']['title']); } ?>
                </div>
            <?php } ?>
            <?php if ($_uccms_properties->getSetting('ratings_reviews_enabled')) { ?>
                <div class="rating_stars">
                    <?php
                    if ($prop['rating'] > 0) {
                        ?>
                        <div class="stars" title="<?php echo $prop['rating']; ?> from <?php echo number_format($prop['reviews'], 0); ?> reviews">
                            <?php
                            $star_vars = array(
                                'el_id'     => 'property-stars-' .$prop['id'],
                                'value'     => $prop['rating'],
                                'readonly'  => true
                            );
                            include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                            ?>
                        </div>
                        <?php
                    } else {
                        ?>
                        No reviews
                        <?php
                    }
                    ?>
                </div>
            <?php } else { ?>
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo date('M j', strtotime($prop['dt_updated'])); ?></div>
            <?php } ?>
        </div>
    </div>
</div>