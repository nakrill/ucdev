<?php

// MODULE CLASS
$_uccms_properties = new uccms_Properties;

// GET SETTING(S)
$_properties['settings'] = $_uccms_properties->getSettings();

// NUMBER OF LEVELS IN URL PATH
$path_num = count($bigtree['path']);

// RATINGS / REVIEWS
include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/master/master.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/custom/master.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/master/master.js"></script>
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/custom/master.js"></script>

<div class="site-width block_center">
    <div class="page">

        <div id="propertiesContainer" class="clearfix">

            <?php

            // SECTIONS DIRECTORY
            $sections_dir = dirname(__FILE__). '/sections';

            // DATA DIRECTORY
            $data_dir = dirname(__FILE__). '/data';

            // HOME
            if ($path_num == 1) {
                include($data_dir. '/home.php');
                include($sections_dir. '/home/default.php');

            // ONE LEVEL DEEP
            } else if ($path_num == 2) {

                // SEARCH SECTION
                if ($bigtree['path'][1] == 'advanced-search') {
                    //include($data_dir. '/advanced-search.php');
                    include($sections_dir. '/advanced-search/default.php');

                // CITY
                } else {
                    include($data_dir. '/city-neighborhood.php');
                    include($sections_dir. '/city-neighborhood/default.php');
                }

            // TWO LEVELS DEEP
            } else if ($path_num == 3) {

                // GET LAST PATH PARTS
                $ppa = explode('-', $bigtree['path'][2]);

                unset($prop_id);
                $prop_id = $ppa[count($ppa) - 1];

                // PROPERTY
                if (($bigtree['path'][1] == 'property') || (is_numeric($prop_id))) {

                    include($data_dir. '/property.php');
                    include($sections_dir. '/property/default.php');

                // AGENT / AGENCY
                } else if (($bigtree['path'][1] == 'agent') || ($bigtree['path'][1] == 'agency')) {
                    include($data_dir. '/agent-agency.php');
                    include($sections_dir. '/agent-agency/default.php');

                // NEIGHBORHOOD
                } else {
                    include($data_dir. '/city-neighborhood.php');
                    include($sections_dir. '/city-neighborhood/default.php');
                }

            // THREE LEVELS DEEP
            } else if ($path_num == 4) {

                // GET LAST PATH PARTS
                $ppa = explode('-', $bigtree['path'][3]);

                $prop_id = $ppa[count($ppa) - 1];

                include($data_dir. '/property.php');
                include($sections_dir. '/property/default.php');

            }

            ?>

        </div>

    </div>
</div>