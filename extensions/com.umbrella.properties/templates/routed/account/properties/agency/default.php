<?php

// IS AGENCY
if ($_properties_agency['id']) {

    ?>

    <style type="text/css">

        #myAccount .section.agency input[type="text"] {
            width: 300px;
        }
        #myAccount .section.agency .image_field .or, #myAccount .section.agency .image_field .form_image_browser, #myAccount .section.agency .image_field .remove_resource, #myAccount .section.agency .image_field .currently label {
            display: none;
        }
        #myAccount .section.agency input.phone {
            width: 120px;
        }

    </style>

    <div class="section agency">

        <h3>Update Agency</h3>

        <form action="./process/" method="post">

        <div class="grid">

            <div class="col-1-2">

                <fieldset>
                    <label>Agency Name</label>
                    <input type="text" value="<?=$_properties_agency['title']?>" name="agency[title]" />
                </fieldset>

                <fieldset>
                    <label>Contact Name</label>
                    <input type="text" value="<?=$_properties_agency['contact_name']?>" name="agency[contact_name]" />
                </fieldset>

                <fieldset>
                    <label>Address</label>
                    <input type="text" value="<?=$_properties_agency['address1']?>" name="agency[address1]" />
                </fieldset>

                <fieldset>
                    <label>Address Cont.</label>
                    <input type="text" value="<?=$_properties_agency['address2']?>" name="agency[address2]" />
                </fieldset>

                <fieldset>
                    <label>City</label>
                    <input type="text" value="<?=$_properties_agency['city']?>" name="agency[city]" />
                </fieldset>

                <fieldset>
                    <label>State</label>
                    <select name="agency[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?=$state_code?>" <?php if ($state_code == $_properties_agency['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Zip</label>
                    <input type="text" value="<?=$_properties_agency['zip']?>" name="agency[zip]" />
                </fieldset>

            </div>

            <div class="col-1-2">

                <fieldset>
                    <label>Phone</label>
                    <input type="text" value="<?=$_properties_agency['phone']?>" name="agency[phone]" class="phone" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$_properties_agency['phone_ext']?>" name="agency[phone_ext]" style="display: inline-block; width: 50px;" />
                </fieldset>

                <fieldset>
                    <label>Mobile</label>
                    <input type="text" value="<?=$_properties_agency['phone_mobile']?>" name="agency[phone_mobile]" class="phone" />
                </fieldset>

                <fieldset>
                    <label>Fax</label>
                    <input type="text" value="<?=$_properties_agency['fax']?>" name="agency[fax]" class="phone" />
                </fieldset>

                <fieldset>
                    <label>Email</label>
                    <input type="text" value="<?=$_properties_agency['email']?>" name="agency[email]" />
                </fieldset>

                <fieldset>
                    <label>Lead Email</label>
                    <input type="text" value="<?=$_properties_agency['email_lead']?>" name="agency[email_lead]" />
                </fieldset>

                <fieldset>
                    <label>Website URL</label>
                    <input type="text" value="<?=$_properties_agency['url']?>" name="agency[url]" placeholder="www.domain.com" />
                </fieldset>

            </div>

        </div>

        <fieldset>
            <label>Image</label>
            <div>
                <?php
                $field = array(
                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                    'key'       => 'image', // The value you should use for the "name" attribute of your form field
                    'value'     => ($_properties_agency['image'] ? $_uccms_properties->imageBridgeOut($_properties_agency['image'], 'agencies') : ''), // The existing value for this form field
                    'id'        => 'agency_image', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'image' => true
                    ),
                    'required'  => false // A boolean value of whether this form field is required or not
                );
                include(BigTree::path('admin/form-field-types/draw/upload.php'));
                ?>
            </div>
        </fieldset>

        <input type="submit" value="Save" />

        </form>

    </div>

    <?php include(BigTree::path('admin/layouts/_html-field-loader.php')); ?>

<?php } else { ?>

    You are not an agency.

<?php } ?>