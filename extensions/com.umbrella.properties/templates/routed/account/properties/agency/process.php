<?php

// MUST BE LOGGED IN
$_uccms['_account']->forceAuth();

// FORM SUBMITTED
if (is_array($_POST)) {

    // DB COLUMNS
    $columns = array(
        'title'                     => $_POST['agency']['title'],
        'contact_name'              => $_POST['agency']['contact_name'],
        'phone'                     => preg_replace('/\D/', '', $_POST['agency']['phone']),
        'phone_ext'                 => $_POST['agency']['phone_ext'],
        'phone_mobile'              => preg_replace('/\D/', '', $_POST['agency']['phone_mobile']),
        'fax'                       => preg_replace('/\D/', '', $_POST['agency']['fax']),
        'address1'                  => $_POST['agency']['address1'],
        'address2'                  => $_POST['agency']['address2'],
        'city'                      => $_POST['agency']['city'],
        'state'                     => $_POST['agency']['state'],
        'zip'                       => preg_replace('/\D/', '', $_POST['agency']['zip']),
        'url'                       => $_uccms_properties->trimURL($_POST['agency']['url']),
        'email'                     => $_POST['agency']['email'],
        'email_lead'                => $_POST['agency']['email_lead']
    );

    // DB QUERY
    $query = "UPDATE `" .$_uccms_properties->tables['agencies']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`account_id`=" .$_uccms['_account']->userID(). ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $_uccms['_site-message']->set('success', 'Agency info updated.');

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['image']['name']) || ((!$_POST['image']) || (substr($_POST['image'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `image` FROM `" .$_uccms_properties->tables['agencies']. "` WHERE (`account_id`=" .$_uccms['_account']->userID(). ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['image']) {

                // REMOVE IMAGE
                @unlink($_uccms_properties->imageBridgeOut($ex['image'], 'agencies', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_properties->tables['agencies']. "` SET `image`='' WHERE (`account_id`=" .$_uccms['_account']->userID(). ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['image']['name']) || ($_POST['image'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'Agency Image',
                'key'           => 'image',
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_properties->Extension. '/files/agencies',
                    'image' => true,
                    'thumbs' => array(
                        array(
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES['image']['name']) {
                $field['file_input'] = $_FILES['image'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['image']) {
                $field['input'] = $_POST['image'];
            }

            // INIT BIGTREE ADMIN
            $admin = new BigTreeAdmin;

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_properties->tables['agencies']. "` SET `image`='" .sqlescape($_uccms_properties->imageBridgeIn($file_path)). "' WHERE (`id`=" .$_uccms['_account']->userID(). ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                $_uccms['_site-message']->set('error', 'Failed to upload image. (' .$bigtree['errors'][0]['error']. ')');

            }

        }

    // QUERY FAILED
    } else {
        $_uccms['_site-message']->set('error', 'Failed to save agency info.');
    }


}

BigTree::redirect('../');

?>