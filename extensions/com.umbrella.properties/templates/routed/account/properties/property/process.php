<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    //echo print_r($_POST);
    //exit;

    // IS AGENT OR AGENCY
    if (($_properties_agent['id']) || ($_properties_agency['id'])) {

        // TABS
        include(dirname(__FILE__). '/tabs.php');

        $property = array();

        // CLEAN UP
        $id = (int)$_REQUEST['id'];

        // ID SPECIFIED
        if ($id) {

            // IS AGENCY
            if ($_properties_agency['id']) {
                $where_sql = "`agency_id`=" .$_properties_agency['id'];

            // IS AGENT
            } else if ($_properties_agent['id']) {
                $where_sql = "`agent_id`=" .$_properties_agent['id'];
            }

            // GET PROPERTY INFO
            $property_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`id`=" .$id. ") AND (" .$where_sql. ")";
            $property_q = sqlquery($property_query);
            $property = sqlfetch($property_q);

            // PROPERTY NOT FOUND
            if (!$property['id']) {
                $_uccms['_site-message']->set('error', 'Property not found or is not yours.');
                BigTree::redirect('../../');
            }

        // CREATE PROPERTY
        } else {

            if ($_POST['property']['title']) {
                $slug = $_uccms_properties->makeRewrite($_POST['property']['title']);
            } else {
                $slug = 'new-property';
            }

            // DB COLUMNS
            $columns = array(
                'slug'                      => $slug,
                'agency_id'                 => (int)$_properties_agent['id'],
                'agent_id'                  => (int)$_properties_agent['id'],
                'status'                    => 4, // pending
                'title'                     => 'New Property'
            );

            // DB QUERY
            $query = "INSERT INTO `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW(), `dt_updated`=NOW()";

            // PROPERTY CREATED
            if (sqlquery($query)) {

                $_uccms['_site-message']->set('success', 'Property listing created.');

                // NEW PROPERTY ID
                $property['id'] = sqlid();

            // PROPERTY CREATION FAILED
            } else {
                $_uccms['_site-message']->set('error', 'Failed to create property listing.');
                BigTree::redirect('../../');
            }

        }

        // HAVE PROPERTY ID
        if ($property['id']) {

            // INCLUDE PROCESSING FILE
            include(dirname(__FILE__). '/processes/' .$tab. '.php');

        }

    // NOT AGENT OR AGENCY
    } else {
        $_uccms['_site-message']->set('error', 'Access denied.');
        BigTree::redirect('../../');
    }

// FORM NOT SUBMITTED
} else {
    $_uccms['_site-message']->set('error', 'Form not submitted.');
    BigTree::redirect('../../');
}

// REDIRECT
if ($taba[$_REQUEST['goto_tab']]) {
    BigTree::redirect('../?id=' .$property['id']. '&tab=' .$_REQUEST['goto_tab']);
} else {
    BigTree::redirect('../?id=' .$property['id']);
}

?>