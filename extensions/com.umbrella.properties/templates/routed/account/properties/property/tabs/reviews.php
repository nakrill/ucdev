<?php

// VARS FOR REVIEWS
$reviews_vars = array(
    'source'    => $_uccms_properties->Extension,
    'what'      => 'property',
    'item_id'   => $property['id']
);

// IS OWNER
$_uccms_ratings_reviews->setReviewAccessSession($reviews_vars, 'reply', true);

?>

<style type="text/css">

    .uccms_ratings-reviews .pages.top {
        display: block;
        margin-bottom: 10px;
        text-align: right;
    }

    #myAccount .section.property .tabs1 .uccms_ratings-reviews .content {
        padding: 0px;
    }

</style>

<div class="section reviews uccms_ratings-reviews">
    <div class="results">
        <?php include(SERVER_ROOT. 'templates/ajax/ratings-reviews/paged-results-admin.php'); ?>
    </div>
</div>