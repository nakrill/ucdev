<?php

// ARRAY OF CITIES
$citiesa = array();

// GET CITIES
$cities_query = "
SELECT c.*
FROM `" .$_uccms_properties->tables['cities']. "` AS `c`
ORDER BY c.title ASC, c.id ASC
";
$cities_q = sqlquery($cities_query);
while ($agency = sqlfetch($cities_q)) {
    $citiesa[$agency['id']] = $agency;
}

// ARRAY OF NEIGHBORHOODS
$neighborhoodsa = array();

// GET NEIGHBORHOODS
$neighborhoods_query = "
SELECT n.*
FROM `" .$_uccms_properties->tables['neighborhoods']. "` AS `n`
ORDER BY n.title ASC, n.id ASC
";
$neighborhoods_q = sqlquery($neighborhoods_query);
while ($neighborhood = sqlfetch($neighborhoods_q)) {
    $neighborhoodsa[$neighborhood['city_id']][$neighborhood['id']] = $neighborhood;
}

?>

<script type="text/javascript">

    $(document).ready(function() {

        // CITY CHANGE
        $('#tab-location select[name="property[city_id]"]').change(function() {
            $('#tab-location .neighborhoods .city').hide();
            $('#tab-location .neighborhoods .city-' +$(this).val()).show();
        });

    });

</script>

<div class="fields">

    <div class="group">

        <?php if (count($citiesa) > 0) { ?>
            <fieldset>
                <label>City</label>
                <select name="property[city_id]">
                    <option value="0">None</option>
                    <?php foreach ($citiesa as $city) { ?>
                        <option value="<?php echo $city['id']; ?>" <?php if ($city['id'] == $property['city_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($city['title']); ?></option>
                    <?php } ?>
                </select>
            </fieldset>
        <?php } ?>

        <?php if (count($neighborhoodsa) > 0) { ?>
        <fieldset class="neighborhoods">
            <label>Neighborhood</label>
            <?php foreach ($neighborhoodsa as $city_id => $neighborhoods) { ?>
                <div class="city city-<?php echo $city_id; ?>" <?php if ($city_id != $property['city_id']) { ?>style="display: none;"<?php } ?>>
                    <select name="property[neighborhood][<?php echo $city_id; ?>][neighborhood_id]">
                        <option value="0">None</option>
                        <?php foreach ($neighborhoods as $neighborhood) { ?>
                            <option value="<?php echo $neighborhood['id']; ?>" <?php if ($neighborhood['id'] == $property['neighborhood_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($neighborhood['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>
            <?php } ?>
        </fieldset>
        <?php } ?>

    </div>

    <div class="group">

        <fieldset>
            <label>Address</label>
            <input type="text" name="property[address1]" value="<?=$property['address1']?>" />
        </fieldset>

        <fieldset>
            <label>Address Cont.</label>
            <input type="text" name="property[address2]" value="<?=$property['address2']?>" />
        </fieldset>

        <fieldset>
            <label>Unit</label>
            <input type="text" name="property[unit]" value="<?=$property['unit']?>" />
        </fieldset>

        <fieldset>
            <label>City</label>
            <input type="text" name="property[city]" value="<?=$property['city']?>" />
        </fieldset>

        <fieldset>
            <label>State</label>
            <select name="property[state]">
                <option value="">Select</option>
                <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                    <option value="<?=$state_code?>" <?php if ($state_code == $property['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                <?php } ?>
            </select>
        </fieldset>

        <fieldset>
            <label>Zip</label>
            <input type="text" value="<?=$property['zip']?>" name="property[zip]" style="display: inline; width: 50px;" /><span class="required">*</span> - <input type="text" value="<?=$property['zip4']?>" name="property[zip4]" style="display: inline; width: 40px;" />
        </fieldset>

    </div>


    <?php if (count($_uccms_properties->propertyLocations()) > 0) { ?>
        <div class="group">

            <fieldset>
                <label>Property Location</label>
                <select name="property[location_code]" style="width: 175px;">
                    <option value="">N/A</option>
                    <?php foreach ($_uccms_properties->propertyLocations() as $loc_code => $loc) { ?>
                        <option value="<?php echo $loc_code; ?>" <?php if ($loc_code == $property['location_code']) { ?>selected="selected"<?php } ?>><?php echo $loc['title']; ?> (<?php echo $loc['description']; ?>)</option>
                    <?php } ?>
                </select>
            </fieldset>

        </div>
    <?php } ?>

</div>