<?php

// ARRAYS
$coolinga   = explode(',', $property['cooling_type_codes']);
$heatinga   = explode(',', $property['heating_type_codes']);
$parkinga   = explode(',', $property['parking_type_codes']);
$petsa      = explode(',', $property['pet_type_codes']);

if (!is_array($coolinga)) $coolinga = array();
if (!is_array($heatinga)) $heatinga = array();
if (!is_array($parkinga)) $parkinga = array();
if (!is_array($petsa)) $petsa = array();

$agenta = array();

// IS AGENCY
if ($_properties_agency['id']) {

    // GET AGENTS
    $agents_query = "
    SELECT at.*
    FROM `" .$_uccms_properties->tables['agents']. "` AS `at`
    WHERE (`agency_id`=" .$_properties_agency['id']. ") AND (at.status!=9)
    ORDER BY at.name ASC, at.id ASC
    ";
    $agents_q = sqlquery($agents_query);
    while ($agent = sqlfetch($agents_q)) {
        $agenta[$agent['id']] = $agent;
    }

}

?>

<?php if (($_properties_agency['id']) && (count($agenta) > 0)) { ?>
    <div class="group">

        <fieldset>
            <label>Agent</label>
            <select name="property[agent_id]">
                <option value="0">None</option>
                <?php foreach ($agenta as $agent) { ?>
                    <option value="<?php echo $agent['id']; ?>" <?php if ($agent['id'] == $property['agent_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($agent['name']); ?></option>
                <?php } ?>
            </select>
        </fieldset>

    </div>
<?php } ?>

<div class="group">

    <fieldset>
        <label>SqFt</label>
        <input type="text" name="property[sqft]" value="<?=number_format($property['sqft'], 0)?>" style="width: 80px;" />
    </fieldset>

    <fieldset class="lt-rental">
        <label>Accommodates</label>
        <input type="text" name="property[accommodates_num]" value="<?=$property['accommodates_num']?>" style="display: inline-block; width: 40px;" /> people
    </fieldset>

    <fieldset>
        <label># Bedrooms</label>
        <select name="property[bedrooms_num]">
            <?php for ($i=0; $i<=50; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($i == $property['bedrooms_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <fieldset>
        <label># Master Bedrooms</label>
        <select name="property[bedrooms_num_master]">
            <?php for ($i=0; $i<=50; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($i == $property['bedrooms_num_master']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <fieldset>
        <label># Bathrooms</label>
        <select name="property[bathrooms_num]">
            <?php
            $i = 0;
            while ($i <= 50) {
                ?>
                <option value="<?php echo $i; ?>" <?php if ($i == $property['bathrooms_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                <?php
                $i += .5;
            }
            ?>
        </select>
    </fieldset>

    <fieldset class="lt-rental">
        <label>Bathroom Access</label>
        <select name="property[rental_bathrooms_code]">
            <?php foreach ($_uccms_properties->propertyRentalBathrooms() as $rb_code => $rb_title) { ?>
                <option value="<?php echo $rb_code; ?>" <?php if ($rb_code == $property['rental_bathrooms_code']) { ?>selected="selected"<?php } ?>><?php echo $rb_title; ?></option>
            <?php } ?>
        </select>
    </fieldset>

</div>

<div class="group">

    <fieldset>
        <label># Parking Spots</label>
        <select name="property[parking_num]">
            <?php for ($i=0; $i<=50; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($i == $property['parking_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <fieldset>
        <label>Parking Type</label>
        <div class="items checkboxes">
            <?php foreach ($_uccms_properties->propertyParkingTypes() as $pt_code => $pt_title) { ?>
                <div class="item contain">
                    <input type="checkbox" name="property[parking][]" value="<?=$pt_code?>" <?php if (in_array($pt_code, $parkinga)) { ?>checked="checked"<?php } ?> /> <?php echo $pt_title; ?>
                </div>
            <?php } ?>
        </div>
    </fieldset>

    <fieldset>
        <label>Gated Access</label>
        <select name="property[gated_code]">
            <option value="">No</option>
            <?php foreach ($_uccms_properties->propertyGatedTypes() as $gt_code => $gt_title) { ?>
                <option value="<?php echo $gt_code; ?>" <?php if ($gt_code == $property['gated_code']) { ?>selected="selected"<?php } ?>><?php echo $gt_title; ?></option>
            <?php } ?>
        </select>
    </fieldset>

</div>

<div class="group">

    <fieldset>
        <label>Lot Acres</label>
        <input type="text" name="property[lot_acres]" value="<?=number_format($property['lot_acres'], 2)?>" style="width: 80px;" />
    </fieldset>

    <fieldset>
        <label>Year Built</label>
        <input type="text" name="property[year_built]" value="<?=$property['year_built']?>" style="width: 40px;" />
    </fieldset>

    <fieldset>
        <label>Cooling</label>
        <div class="items checkboxes">
            <?php foreach ($_uccms_properties->propertyCoolingTypes() as $ct_code => $ct_title) { ?>
                <div class="item contain">
                    <input type="checkbox" name="property[cooling][]" value="<?=$ct_code?>" <?php if (in_array($ct_code, $coolinga)) { ?>checked="checked"<?php } ?> /> <?php echo $ct_title; ?>
                </div>
            <?php } ?>
        </div>
    </fieldset>

    <fieldset>
        <label>Heating</label>
        <div class="items checkboxes">
            <?php foreach ($_uccms_properties->propertyHeatingTypes() as $ht_code => $ht_title) { ?>
                <div class="item contain">
                    <input type="checkbox" name="property[heating][]" value="<?=$ht_code?>" <?php if (in_array($ht_code, $heatinga)) { ?>checked="checked"<?php } ?> /> <?php echo $ht_title; ?>
                </div>
            <?php } ?>
        </div>
    </fieldset>

    <fieldset>
        <label># Fireplaces</label>
        <select name="property[fireplace_num]">
            <?php for ($i=0; $i<=50; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($i == $property['fireplace_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <fieldset>
        <label>Clothes Washer / Dryer</label>
        <select name="property[washer_dryer_code]">
            <option value="">None</option>
            <?php foreach ($_uccms_properties->propertyWasherDryerTypes() as $wd_code => $wd_title) { ?>
                <option value="<?php echo $wd_code; ?>" <?php if ($wd_code == $property['washer_dryer_code']) { ?>selected="selected"<?php } ?>><?php echo $wd_title; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <fieldset>
        <label># Elevators</label>
        <select name="property[elevator_num]">
            <?php for ($i=0; $i<=10; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($i == $property['elevator_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <fieldset class="lt-rental">
        <input type="checkbox" name="property[has_wifi]" value="1" <?php if ($property['has_wifi']) { ?>checked="checked"<?php } ?> />
        <label class="for-checkbox">Has Wifi</label>
    </fieldset>

    <fieldset>
        <label>Pool</label>
        <select name="property[pool_code]">
            <option value="">None</option>
            <?php foreach ($_uccms_properties->propertyPoolTypes() as $pt_code => $pt_title) { ?>
                <option value="<?php echo $pt_code; ?>" <?php if ($pt_code == $property['pool_code']) { ?>selected="selected"<?php } ?>><?php echo $pt_title; ?></option>
            <?php } ?>
        </select>
    </fieldset>

    <fieldset>
        <label>Pool Size</label>
        <input type="text" name="property[pool_size]" value="<?=$property['pool_size']?>" style="width: 80px;" />
    </fieldset>

    <fieldset class="lt-rental">
        <label>Pets Allowed</label>
        <div class="items checkboxes">
            <?php foreach ($_uccms_properties->propertyPetTypes() as $pt_code => $pt_title) { ?>
                <div class="item contain">
                    <input type="checkbox" name="property[pets][]" value="<?=$pt_code?>" <?php if (in_array($pt_code, $petsa)) { ?>checked="checked"<?php } ?> /> <?php echo $pt_title; ?>
                </div>
            <?php } ?>
        </div>
    </fieldset>

    <fieldset class="lt-rental">
        <label>Pet Limit</label>
        <select name="property[pets_num]">
            <option value="255">None</option>
            <?php if (!$petsa[0]) { ?>
                <option value="0" <?php if ($property['pets_num'] == 0) { ?>selected="selected"<?php } ?>>0</option>
            <?php } ?>
            <?php for ($i=1; $i<=10; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($i == $property['pets_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </fieldset>

</div>

<div class="group">

    <fieldset class="lt-rental">
        <label>Check In Office</label>
        <input type="text" name="property[checkin_office]" value="<?=$property['checkin_office']?>" />
    </fieldset>

    <fieldset class="lt-rental">
        <label>Check In</label>
        <div class="clearfix">
            <select name="property[checkin_day]" class="left">
                <option value="0">None</option>
                <?php foreach ($_uccms_properties->daysOfWeek_Schema() as $day_id => $day_title) { ?>
                    <option value="<?php echo $day_id; ?>" <?php if ($day_id == $property['checkin_day']) { ?>selected="selected"<?php } ?>><?php echo $day_title; ?></option>
                <?php } ?>
            </select>
            <div class="left clearfix" style="margin-left: 15px;">
                <?php

                // FIELD VALUES
                $field = array(
                    'id'        => 'checkin_time',
                    'key'       => 'property[checkin_time]',
                    'value'     => ($property['checkin_time'] ? $property['checkin_time'] : ''),
                    'required'  => false,
                    'options'   => array(
                        'default_now'   => false
                    )
                );

                // INCLUDE FIELD
                include(BigTree::path('admin/form-field-types/draw/time.php'));

                ?>
            </div>
        </div>
    </fieldset>

    <fieldset class="lt-rental">
        <label>Check Out</label>
        <select name="property[checkout_day]" class="left">
            <option value="0">None</option>
            <?php foreach ($_uccms_properties->daysOfWeek_Schema() as $day_id => $day_title) { ?>
                <option value="<?php echo $day_id; ?>" <?php if ($day_id == $property['checkout_day']) { ?>selected="selected"<?php } ?>><?php echo $day_title; ?></option>
            <?php } ?>
        </select>
        <div class="left clearfix" style="margin-left: 15px;">
            <?php

            // FIELD VALUES
            $field = array(
                'id'        => 'checkout_time',
                'key'       => 'property[checkout_time]',
                'value'     => ($property['checkout_time'] ? $property['checkout_time'] : ''),
                'required'  => false,
                'options'   => array(
                    'default_now'   => false
                )
            );

            // INCLUDE FIELD
            include(BigTree::path('admin/form-field-types/draw/time.php'));

            ?>
        </div>
    </fieldset>

</div>