<script type="text/javascript">
    $(document).ready(function() {

        // ROOMS - ADD CLICK
        $('#tab-rooms .add a').click(function(e) {
            e.preventDefault();
            $('#tab-rooms .add_item').toggle();
        });

        // ROOMS - SORTABLE
        $('#tab-rooms .items').sortable({
            handle: '.sort',
            axis: 'y',
            containment: 'parent',
            items: '.item',
            placeholder: 'room-sortable-placeholder',
            update: function() {
                var rows = $(this).find('.item');
                var sort = [];
                rows.each(function() {
                    sort.push($(this).attr('id').replace('room-', ''));
                });
                $('#tab-rooms input[name="sort"]').val(sort.join());
            }
        });

        // ROOMS - EXPAND
        $('#tab-rooms .items .item .heading .toggle a').click(function(e) {
            e.preventDefault();
            var tog = $(this).closest('.item').find('.expand_content');
            if (tog.is(':visible')) {
                tog.hide();
                $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-down');
            } else {
                tog.show();
                $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
        });

        // ROOMS - REMOVE CLICK
        $('#tab-rooms .delete').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to delete this?')) {
                var id = $(this).attr('data-id');
                var del = $('#tab-rooms input[name="delete"]').val();
                if (del) del += ',';
                del += id;
                $('#tab-rooms input[name="delete"]').val(del);
                $('#tab-rooms #room-' +id).fadeOut(500);
            }
        });

    });
</script>

<input type="hidden" name="sort" value="" />
<input type="hidden" name="delete" value="" />

<div class="add">
    <a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Add Room</a>
</div>

<div class="add_item">
    <div class="add_container clearfix">

        <div class="items">

            <div class="item contain">
                <div class="expand_content">

                    <div class="options-1 clearfix">
                        <div class="opt level">
                            <label>Level</label>
                            <select name="room[new][level]">
                                <?php foreach ($_uccms_properties->propertyRoomLevels() as $rl_id => $rl_title) { ?>
                                    <option value="<?php echo $rl_id; ?>" <?php if ($rl_id == 1) { ?>selected="selected"<?php } ?>><?php echo $rl_title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="opt type">
                            <label>Type</label>
                            <select name="room[new][type_code]">
                                <option value="">Select</option>
                                <?php foreach ($_uccms_properties->propertyRoomTypes() as $rt_code => $rt_title) { ?>
                                    <option value="<?php echo $rt_code; ?>"><?php echo $rt_title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="opt dimensions">
                            <label>Dimensions</label>
                            <input type="text" name="room[new][dimensions]" value="" placeholder="10 x 12 sqft" />
                        </div>
                    </div>

                    <div class="options-2 clearfix">

                        <div class="col" style="<?php if ($property['listing_type'] != 2) { ?>display: none;<?php } ?>">
                            <div class="opt sleeps lt-rental">
                                <label>Sleeps</label>
                                <select name="room[new][sleeps_num]">
                                    <?php for ($i=0; $i<=20; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="opt beds lt-rental">
                                <label>Beds</label>
                                <div class="checkboxes">
                                    <?php foreach ($_uccms_properties->propertyBedTypes() as $bt_code => $bt_title) { ?>
                                        <div class="opt">
                                            <input type="checkbox" name="room[new][beds][]" value="<?=$bt_code?>" />
                                            <label class="for_checkbox"><?php echo $bt_title; ?></label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="checkboxes">
                                <div class="opt">
                                    <input type="checkbox" name="room[new][has_ac]" value="1" />
                                    <label class="for_checkbox">Has AC</label>
                                </div>
                                <div class="opt">
                                    <input type="checkbox" name="room[new][has_ceiling_fan]" value="1" />
                                    <label class="for_checkbox">Has Ceiling Fan</label>
                                </div>
                                <div class="opt">
                                    <input type="checkbox" name="room[new][has_fireplace]" value="1" />
                                    <label class="for_checkbox">Has Fireplace</label>
                                </div>
                            </div>

                            <div class="description">
                                <label>Description</label>
                                <textarea name="room[new][description]"></textarea>
                            </div>

                            <div class="shared lt-rental" style="padding-top: 15px;">
                                <input type="checkbox" name="room[new][shared]" value="1" />
                                <label class="for_checkbox">Is Shared</label>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>

        <div class="buttons">
            <input type="submit" value="Add" class="button" />
        </div>

    </div>
</div>

<?php

// GET ROOMS
$rooms_query = "SELECT * FROM `" .$_uccms_properties->tables['property_rooms']. "` WHERE (`property_id`=" .$property['id']. ") ORDER BY `sort` ASC, `id` ASC";
$rooms_q = sqlquery($rooms_query);

// NUMBER OF ROOMS
$num_rooms = sqlrows($rooms_q);

// HAVE ROOMS
if ($num_rooms > 0) {

    ?>

    <div class="items ui-sortable">

        <?php

        // LOOP
        while ($room = sqlfetch($rooms_q)) {

            // LEVEL
            if (!$room['level']) $room['level'] = 1;

            // BEDS
            unset($bedsa);
            $bedsa = explode(',', $room['bed_codes']);
            if (!is_array($bedsa)) $bedsa = array();

            ?>

            <div id="room-<?php echo $room['id']; ?>" class="item contain">

                <div class="heading">

                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>

                            <td valign="top">
                                <div class="sort">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </div>
                            </td>

                            <td width="90%">
                                <div class="collapse_info">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="25%" valign="middle">
                                                <span class="what">Level:</span> <?php echo $_uccms_properties->propertyRoomLevels()[$room['level']]; ?>
                                            </td>
                                            <td width="30%" valign="middle">
                                                <span class="what">Type:</span> <?php echo $_uccms_properties->propertyRoomTypes()[$room['type_code']]; ?>
                                            </td>
                                            <td width="45%" valign="middle">
                                                <span class="what">Dimensions:</span> <?php echo stripslashes($room['dimensions']); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>

                            <td width="10%" valign="top">
                                <div class="toggle">
                                    <a href="#"><i class="fa fa-chevron-down"></i></a>
                                </div>
                            </td>

                        </tr>
                    </table>

                </div>

                <div class="expand_content">

                    <div class="buttons">
                        <a href="#" data-id="<?php echo $room['id']; ?>" class="delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                    </div>

                    <div class="options-1 clearfix">
                        <div class="opt level">
                            <label>Level</label>
                            <select name="room[<?php echo $room['id']; ?>][level]">
                                <?php foreach ($_uccms_properties->propertyRoomLevels() as $rl_id => $rl_title) { ?>
                                    <option value="<?php echo $rl_id; ?>" <?php if ($rl_id == $room['level']) { ?>selected="selected"<?php } ?>><?php echo $rl_title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="opt type">
                            <label>Type</label>
                            <select name="room[<?php echo $room['id']; ?>][type_code]">
                                <?php foreach ($_uccms_properties->propertyRoomTypes() as $rt_code => $rt_title) { ?>
                                    <option value="<?php echo $rt_code; ?>" <?php if ($rt_code == $room['type_code']) { ?>selected="selected"<?php } ?>><?php echo $rt_title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="opt dimensions">
                            <label>Dimensions</label>
                            <input type="text" name="room[<?php echo $room['id']; ?>][dimensions]" value="<?php echo stripslashes($room['dimensions']); ?>" placeholder="10 x 12 sqft" />
                        </div>
                    </div>

                    <div class="options-2 clearfix">

                        <div class="col" style="<?php if ($property['listing_type'] != 2) { ?>display: none;<?php } ?>">
                            <div class="opt sleeps lt-rental">
                                <label>Sleeps</label>
                                <select name="room[<?php echo $room['id']; ?>][sleeps_num]">
                                    <?php for ($i=0; $i<=20; $i++) { ?>
                                        <option value="<?php echo $i; ?>" <?php if ($i == $room['sleeps_num']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="opt beds lt-rental">
                                <label>Beds</label>
                                <div class="checkboxes">
                                    <?php foreach ($_uccms_properties->propertyBedTypes() as $bt_code => $bt_title) { ?>
                                        <div class="opt bed-size">
                                            <input type="checkbox" name="room[<?php echo $room['id']; ?>][beds][]" value="<?=$bt_code?>" <?php if (in_array($bt_code, $bedsa)) { ?>checked="checked"<?php } ?> />
                                            <label class="for_checkbox"><?php echo $bt_title; ?></label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="checkboxes">
                                <div class="opt has-ac">
                                    <input type="checkbox" name="room[<?php echo $room['id']; ?>][has_ac]" value="1" <?php if ($room['has_ac']) { ?>checked="checked"<?php } ?> />
                                    <label class="for_checkbox">Has AC</label>
                                </div>
                                <div class="opt has-cf">
                                    <input type="checkbox" name="room[<?php echo $room['id']; ?>][has_ceiling_fan]" value="1" <?php if ($room['has_ceiling_fan']) { ?>checked="checked"<?php } ?> />
                                    <label class="for_checkbox">Has Ceiling Fan</label>
                                </div>
                                <div class="opt has-fp">
                                    <input type="checkbox" name="room[<?php echo $room['id']; ?>][has_fireplace]" value="1" <?php if ($room['has_fireplace']) { ?>checked="checked"<?php } ?> />
                                    <label class="for_checkbox">Has Fireplace</label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="description">
                        <label>Description</label>
                        <textarea name="room[<?php echo $room['id']; ?>][description]"><?php echo stripslashes($room['description']); ?></textarea>
                    </div>

                    <div class="shared lt-rental">
                        <input type="checkbox" name="room[<?php echo $room['id']; ?>][shared]" value="1" <?php if ($room['shared']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Is Shared</label>
                    </div>

                </div>

            </div>

            <?php

        }

        ?>

    </div>

    <?php

} else {

    ?>

    <div style="padding: 10px; text-align: center;">
        No rooms added yet.
    </div>

    <?php

}

?>