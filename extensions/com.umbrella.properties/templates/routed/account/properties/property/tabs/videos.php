<script type="text/javascript">
    $(document).ready(function() {

        // VIDEOS - SORTABLE
        $('#tab-videos .items').sortable({
            handle: '.sort',
            axis: 'y',
            containment: 'parent',
            items: '.item',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
                var rows = $(this).find('div.item');
                var sort = [];
                rows.each(function() {
                    sort.push($(this).attr('id').replace('row_', ''));
                });
                $('#tab-videos input[name="sort"]').val(sort.join());
            }
        });

        // VIDEOS - ADD CLICK
        $('#tab-videos .add a').click(function(e) {
            e.preventDefault();
            $('#tab-videos .add_item').toggle();
        });

        // VIDEOS - REMOVE CLICK
        $('#tab-videos .items .item .delete').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to delete this?')) {
                var id = $(this).attr('data-id');
                var del = $('#tab-videos input[name="delete"]').val();
                if (del) del += ',';
                del += id;
                $('#tab-videos input[name="delete"]').val(del);
                $('#tab-videos #row_' +id).fadeOut(500);
            }
        });

    });

</script>

<input type="hidden" name="sort" value="" />
<input type="hidden" name="delete" value="" />

<div class="add">
    <a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Add Video</a>
</div>

<div class="add_item">
    <div class="add_container clearfix">
        <div class="upload">
            <input type="text" name="video[new][url]" value="" placeholder="YouTube / Vimeo URL" style="width: 300px;" />
        </div>
        <div class="title">
            <fieldset>
                <input type="text" name="video[new][caption]" value="" placeholder="Caption / Alt Text" style="width: 300px;" />
            </fieldset>
        </div>
        <div class="buttons">
            <input type="submit" value="Add" class="button" />
        </div>
    </div>
</div>

<?php

// GET VIDEOS
$video_query = "SELECT * FROM `" .$_uccms_properties->tables['property_videos']. "` WHERE (`property_id`=" .$property['id']. ") ORDER BY `sort` ASC, `id` ASC";
$video_q = sqlquery($video_query);

// NUMBER OF VIDEOS
$num_videos = sqlrows($video_q);

// HAVE VIDEOS
if ($num_videos > 0) {

    ?>

    <div class="items ui-sortable">

        <?php

        // LOOP
        while ($video = sqlfetch($video_q)) {

            // GET VIDEO INFO
            $vi = videoInfo(stripslashes($video['video']));

            ?>

            <div id="row_<?php echo $video['id']; ?>" class="item clearfix">
                <div class="sort">
                    <i class="fa fa-sort" aria-hidden="true"></i>
                </div>
                <div class="video">
                    <?php if ($vi['thumb']) { ?>
                        <a href="<?php echo $video['video']; ?>" target="_blank"><img src="<?php echo $vi['thumb']; ?>" alt="VIDEO" /></a>
                    <?php } ?>
                </div>
                <div class="title">
                    <input type="text" name="video[<?php echo $video['id']; ?>][caption]" value="<?php echo stripslashes($video['caption']); ?>" placeholder="Title" />
                    <div class="upload">
                        <input type="text" name="video[<?php echo $video['id']; ?>][url]" value="<?php echo stripslashes($video['video']); ?>" placeholder="YouTube / Vimeo URL" />
                    </div>
                </div>
                <div class="buttons">
                    <a href="#" data-id="<?php echo $video['id']; ?>" class="delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                </div>
            </div>

            <?php

        }

        ?>

    </div>

    <?php

} else {

    ?>

    <div style="padding: 10px; text-align: center;">
        No videos added yet.
    </div>

    <?php

}

?>