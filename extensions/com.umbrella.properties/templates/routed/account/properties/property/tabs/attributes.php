<script type="text/javascript">
    $(document).ready(function() {

        // ATTRIBUTES - ADD CLICK
        $('#tab-attributes .add_item .add').click(function(e) {
            e.preventDefault();
            $('#tab-attributes .add_item .add_container').toggle();
        });

        // ATTRIBUTES - EXPAND
        $('#tab-attributes .item .heading .toggle a').click(function(e) {
            e.preventDefault();
            var tog = $(this).closest('.item').find('.expand_content');
            if (tog.is(':visible')) {
                tog.hide();
                $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-down');
            } else {
                tog.show();
                $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
        });

        // ATTRIBUTES - REMOVE CLICK
        $('#tab-attributes .action.delete a').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to delete this?')) {
                var id = $(this).attr('data-id');
                var del = $('#tab-attributes input[name="delete"]').val();
                if (del) {
                    $('#tab-attributes input[name="delete"]').val(del+ ',' +id);
                } else {
                    $('#tab-attributes input[name="delete"]').val(id);
                }
                $('#agroup-' +id).fadeOut();
            }
        });

        // ATTRIBUTES - REMOVE FILE CLICK
        $('#tab-attributes .attribute.file .files .remove').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('#tab-attributes .attribute.file .files input[name="attribute-' +id+ '-remove"]').val('1');
            $('#tab-attributes .attribute.file .files .file-' +id).remove();
        });

    });
</script>

<?php

// SELECTED ATTRIBUTES
$selattra = json_decode(stripslashes($property['attributes']), true);

// PROPERTY GROUPS ARRAY
$pgroupsa = array();

/*
// GET ATTRIBUTE GROUPS FOR PROPERTY
$groups_query = "
SELECT *
FROM `" .$_uccms_properties->tables['property_attribute_groups']. "` AS `pag`
INNER JOIN `" .$_uccms_properties->tables['attribute_groups']. "` AS `ag` ON pag.group_id=ag.id
WHERE (pag.property_id=" .$property['id']. ") AND (ag.active=1)
ORDER BY ag.sort ASC, ag.title ASC";
$groups_q = sqlquery($groups_query);
while ($group = sqlfetch($groups_q)) {
    $pgroupsa[$group['id']] = $group;
}
*/

// GET ATTRIBUTE GROUPS FOR PROPERTY TYPE
$groups_query = "
SELECT ag.*
FROM `" .$_uccms_properties->tables['attribute_groups']. "` AS `ag`
INNER JOIN `" .$_uccms_properties->tables['attribute_groups_property_types']. "` AS `agpt` ON ag.id=agpt.group_id
WHERE (ag.active=1) AND (agpt.property_type_code='" .$property['property_type_code']. "')
ORDER BY `sort` ASC, `title` ASC
";
$groups_q = sqlquery($groups_query);
while ($group = sqlfetch($groups_q)) {
    $pgroupsa[$group['id']] = $group;
}

// NUMBER OF GROUPS
$num_groups = count($pgroupsa);

/*
// AVAILABLE GROUPS
$aga = array();

$group_query = "SELECT `id`, `title` FROM `" .$_uccms_properties->tables['attribute_groups']. "` WHERE (`active`=1) ORDER BY `sort` ASC, `title` ASC";
$group_q = sqlquery($group_query);
while ($group = sqlfetch($group_q)) {
    if (!$pgroupsa[$group['id']]) {
        $aga[$group['id']] = $group;
    }
}

?>

<div class="add_item">

    <a class="add" href="#"><i class="fa fa-plus" aria-hidden="true"></i> Add Attribute</a>

    <div class="add_container">

        <?php if (count($aga) > 0) { ?>
            <ul class="items">

                <li id="agroup-new" class="item contain">

                    <table cellpadding="0" cellspacing="0" style="width: auto;">
                        <tr>
                            <td>Group:</td>
                            <td>
                                <select name="new_group[id]" style="display: inline-block; float: none;">
                                    <?php foreach ($aga as $group) { ?>
                                        <option value="<?php echo $group['id']; ?>"><?php echo stripslashes($group['title']); ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>
                                <input type="submit" value="Add" class="blue" />
                            </td>
                        </tr>
                    </table>

                </li>

            </ul>
        <?php } else { ?>
            <div style="padding: 10px; text-align: center;">
                No attribute groups available.
            </div>
        <?php } ?>

    </div>

</div>

<?php
*/

// HAVE GROUPS
if ($num_groups > 0) {

    ?>

    <? /*<input type="hidden" name="delete" value="" />*/ ?>

    <ul class="items ui-sortable">

        <?php

        // LOOP
        foreach ($pgroupsa as $group) {

            ?>

            <li id="agroup-<?php echo $group['id']; ?>" class="item contain">

                <div class="heading">
                    <div class="collapse_info toggle clearfix">
                        <a href="#"><?php echo stripslashes($group['title']); ?><i class="fa fa-chevron-down"></i></a>
                    </div>
                </div>

                <div class="expand_content">

                    <div style="float: left; width: 50%;">

                        <?php

                        // GET ATTRIBUTES FOR GROUP
                        $attr_query = "SELECT * FROM `" .$_uccms_properties->tables['attributes']. "` WHERE (`group_id`=" .$group['id']. ")";
                        $attr_q = sqlquery($attr_query);
                        while ($attribute = sqlfetch($attr_q)) {

                            $key = $attribute['id'];

                            ?>

                            <div class="attribute contain">

                                <?php

                                // ATTRIBUTE USE COUNT ARRAY
                                $auca = array();

                                unset($field_values);

                                // HAVE ALREADY SELECTED OPTIONS
                                if (is_array($selattra)) {

                                    // CHECKBOX, WIDTH/HEIGHT
                                    if (($attribute['type'] == 'checkbox') || ($attribute['type'] == 'width_height')) {
                                        $field_values = $selattra[$attribute['id']]; // ARRAY OF SELECTED OPTIONS

                                    // ALL OTHER FIELDS
                                    } else {
                                        if (!$auca[$attribute['id']]) $auca[$attribute['id']] = 0;
                                        $field_values = $selattra[$attribute['id']][$auca[$attribute['id']]]; // ONLY FIRST VALUE OF ARRAY EACH TIME
                                    }

                                // USE DEFAULTS
                                } else {

                                    // ATTRIBUTE HAS OPTIONS
                                    if ($attribute['options']) {
                                        $options = json_decode($attribute['options'], true);
                                        foreach ($options as $option) {
                                            if ($option['default']) {
                                                $field_values = stripslashes($option['title']); // DEFAULT
                                            }
                                        }
                                    }

                                }

                                // ATTRIBUTE ELEMENT
                                include(SERVER_ROOT. 'extensions/' .$_uccms_properties->Extension. '/modules/properties/properties/edit/attributes/types/base.php');

                                $auca[$attribute['id']]++;

                                ?>

                            </div>

                            <?php

                        }

                        ?>

                    </div>

                    <? /*
                    <div class="action delete">
                        <a href="#" data-id="<?php echo $group['id']; ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                    </div>
                    */ ?>

                </div>

            </li>

            <?php

        }

        ?>

    </ul>

    <?php

} else {

    ?>

    <div style="padding: 10px; text-align: center;">
        No attributes available for property type.
    </div>

    <?php

}

?>