<?php

// GET PAYMENT METHODS
$payment_methods = $_uccms_properties->getPaymentMethods(true);

// PAYMENT PROFILES
$payment_profiles = array();

// IS LOGGED IN
if ($_uccms['_account']->userID()) {

    // PAYMENT PROFILES ENABLED
    if ($_uccms['_account']->paymentProfilesEnabled()) {

        // GET PAYMENT PROFILES
        $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$_uccms['_account']->userID(). ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
        $pp_q = sqlquery($pp_query);
        while ($pp = sqlfetch($pp_q)) {
            $payment_profiles[$pp['id']] = $pp;
        }

    }

}

?>

<script type="text/javascript">

    $(document).ready(function() {

        // CHANGE PAYMENT METHOD
        $('#tab-status .paying-info .change').click(function(e) {
            e.preventDefault();
            $('#tab-status .payment-form').toggle();
            $('#tab-status .buttons input[name="do-change-payment"]').toggle();
        });

        // VIEW PAYMENTS CLICK
        $('#tab-status .paying-info .view-payments a').click(function(e) {
            //e.preventDefault();
            $('#tab-status .transactions .transactions_content').show();
        });

        // PAYMENT LOG HEADING CLICK
        $('#tab-status .transactions h4 a').click(function(e) {
            e.preventDefault();
            $('#tab-status .transactions .transactions_content').toggle();
        });

    });

</script>

<div class="status">Status: <span class="value"><?php echo $_uccms_properties->statuses[$property['status']]; ?></span></div>

<?php

// GET PRICING INFO
$pricing = $_uccms_properties->listingPricing($property['id']);

// IS PAYING
if ($property['paying']) {

    ?>

    <div class="paying-info">

        <?php

        // HAS EXPIRATION DATE
        if ($property['payment_expires'] != '0000-00-00') {

            // NOT EXPIRED
            if (strtotime($property['payment_expires']) > time()) {
                ?>
                <div class="expires">
                    Paid through: <?php echo date('n/j/Y', strtotime($property['payment_expires'])); ?>
                </div>
                <?php

            // EXPIRED
            } else {
                ?>
                <div class="expired">
                    Expired: <?php echo date('n/j/Y', strtotime($property['payment_expires'])); ?>
                </div>
                <?php
            }

        }

        // HAVE PAYMENT PROFILE ID
        if ($property['payment_profile_id']) {

            // GET PAYMENT PROFILE INFO
            $pay_prof = $_uccms['_account']->pp_getProfile($property['payment_profile_id']);

            // PAYMENT PROFILE FOUND
            if ($pay_prof['id']) {
                ?>
                <div class="method">
                    Payment method: <?php if ($pay_prof['lastfour']) { echo $pay_prof['lastfour']; } if ($pay_prof['type']) { echo ' (' .ucwords($pay_prof['type']). ')'; } ?> <a href="#" class="change">change</a>
                </div>
                <?php

            // NO PAYMENT PROFILE
            } else {
                ?>
                <div class="method">
                    Payment method: Not found. <a href="#" class="change">Add / update</a>
                </div>
                <?php
            }

        }

        // GET PAYMENT LOGS
        $query = "SELECT `id` FROM `" .$_uccms_properties->tables['transaction_log']. "` WHERE (`property_id`=" .$property['id']. ") LIMIT 1";
        $q = sqlquery($query);

        // HAVE RESULTS
        if (sqlrows($q) > 0) {
            ?>
            <div class="view-payments">
                <a href="#transactions">View Payments</a>
            </div>
            <?php
        }

        ?>

    </div>

    <?php

}

$form_session['payment']['profile_id'] = $property['payment_profile_id'];

?>
<div class="payment-form" style="<?php if ((strtotime($property['payment_expires']) < time()) && ($pricing['listing_price'] > 0.00)) { } else { ?>display: none;<?php } ?>">
    <?php include(dirname(__FILE__). '/../../../../properties/elements/payment_options.php'); ?>
</div>

<div class="buttons">

    <?php if ($property['paying']) { ?>

        <input type="submit" name="do-change-payment" value="Change" class="button" style="display: none;" />

    <?php } else if ($property['status'] == 1) { ?>

        <input type="submit" name="do-change-payment" value="Save" class="button" />

    <?php } ?>

    <?php

    // INACTIVE
    if ($property['status'] == 0) {

        ?>

        <input type="submit" name="do-activate" value="Activate" class="button" />

        <?php

    // ACTIVE
    } else if ($property['status'] == 1) {

        ?>

        <input type="submit" name="do-deactivate" value="De-Activate" class="button" />

        <?php

    // DRAFT
    } else if ($property['status'] == 4) {

        ?>

        <input type="submit" name="do-activate" value="Activate" class="button" />

        <?php

    // PENDING
    } else if ($property['status'] == 5) {

        ?>

        <input type="submit" name="do-deactivate" value="De-Activate" class="button" />

        <?php

    }

    ?>

</div>

<?php

// GET PAYMENT LOGS
$translog_query = "SELECT * FROM `" .$_uccms_properties->tables['transaction_log']. "` WHERE (`property_id`=" .$property['id']. ") ORDER BY `dt` ASC";
$translog_q = sqlquery($translog_query);

// HAVE RESULTS
if (sqlrows($translog_q) > 0) {

    ?>

    <a name="transactions"></a>

    <div class="transactions">

        <h4><a href="#">Payment Log</a></h4>

        <div class="transactions_content">

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <th class="dt">
                        Date
                    </th>
                    <th class="amount">
                        Amount
                    </th>
                    <th class="method">
                        Method
                    </th>
                    <th class="status">
                        Status
                    </th>
                    <th class="note">
                        Note
                    </th>
                </tr>

                <?php while ($transaction = sqlfetch($translog_q)) { ?>
                    <tr class="item">
                        <td class="dt">
                            <?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?>
                        </td>
                        <td class="amount">
                            $<?php echo number_format($transaction['amount'], 2); ?>
                        </td>
                        <td class="method">
                            <?php if ($transaction['method'] == 'profile') { echo 'Profile'; } else { echo stripslashes($payment_methods[$transaction['method']]['title']); } if ($transaction['lastfour']) { echo ' (' .$transaction['lastfour']. ')'; } ?>
                        </td>
                        <td class="status">
                            <?php echo ucwords($transaction['status']); ?>
                        </td>
                        <td class="note">
                            <?php echo stripslashes($transaction['note'] ? $transaction['note'] : $transaction['message']); ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>

        </div>

    </div>

    <?php

}