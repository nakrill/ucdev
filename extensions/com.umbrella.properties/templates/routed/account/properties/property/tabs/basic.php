<div class="fields">

    <div class="group">

        <fieldset>
            <label>Listing Type</label>
            <select id="listing_type" name="property[listing_type]">
                <?php foreach ($_uccms_properties->listingTypes as $type_id => $type_title) { ?>
                    <option value="<?php echo $type_id; ?>" <?php if ($type_id == $property['listing_type']) { ?>selected="selected"<?php } ?>><?php echo $type_title; ?></option>
                <?php } ?>
            </select>
        </fieldset>

        <fieldset>
            <label>Property Name</label>
            <input type="text" value="<?=$property['title']?>" name="property[title]" />
        </fieldset>

        <fieldset>
            <label>Property ID</label>
            <input type="text" value="<?=$property['property_id']?>" name="property[property_id]" />
        </fieldset>

    </div>

    <div class="group">

        <fieldset>
            <label>Property Type</label>
            <select name="property[property_type_code]">
                <?php foreach ($_uccms_properties->propertyTypes() as $type_code => $type_title) { ?>
                    <option value="<?php echo $type_code; ?>" <?php if ($type_code == $property['property_type_code']) { ?>selected="selected"<?php } ?>><?php echo $type_title; ?></option>
                <?php } ?>
            </select>
        </fieldset>

        <fieldset class="lt-rental">
            <label>Rental Space</label>
            <select name="property[rental_space_code]">
                <?php foreach ($_uccms_properties->propertyRentalSpaces() as $rs_code => $rs_title) { ?>
                    <option value="<?php echo $rs_code; ?>" <?php if ($rs_code == $property['rental_space_code']) { ?>selected="selected"<?php } ?>><?php echo $rs_title; ?></option>
                <?php } ?>
            </select>
        </fieldset>

        <fieldset>
            <label>Price <span class="lt-rental">From</span></label>
            $<input type="text" name="property[price_from]" value="<?=number_format($property['price_from'], 2)?>" style="display: inline; width: 80px;" />
        </fieldset>

        <fieldset class="lt-rental">
            <label>Price To</label>
            $<input type="text" name="property[price_to]" value="<?=number_format($property['price_to'], 2)?>" style="display: inline; width: 80px;" />
        </fieldset>

        <fieldset class="lt-rental">
            <label>Price Per</label>
            <select name="property[price_term_code]">
                <option value="">N/A</option>
                <?php foreach ($_uccms_properties->propertyPriceTerms() as $pt_code => $pt_title) { ?>
                    <option value="<?php echo $pt_code; ?>" <?php if ($pt_code == $property['price_term_code']) { ?>selected="selected"<?php } ?>><?php echo $pt_title; ?></option>
                <?php } ?>
            </select>
        </fieldset>

    </div>

    <div class="group">

        <fieldset>
            <label>Description</label>
            <textarea id="property_description" name="property[description]" style="height: 100px;"><?=$property['description']?></textarea>
        </fieldset>

        <script>
            $(document).ready(function() {
                tinyMCE.init({
                    theme: "modern",
                    mode: "exact",
                    elements: "property_description",
                    menubar: false,
                    plugins: "link,code,visualblocks,lists",
                    toolbar: "bold italic underline alignleft aligncenter alignright bullist numlist link unlink",
                    paste_remove_spans: true,
                    paste_remove_styles: true,
                    paste_strip_class_attributes: true,
                    paste_auto_cleanup_on_paste: true,
                    gecko_spellcheck: true,
                    relative_urls: false,
                    remove_script_host: true,
                    extended_valid_elements : "*[*]"
                });
            });
        </script>

    </div>

</div>