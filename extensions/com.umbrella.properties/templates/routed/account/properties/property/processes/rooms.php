<?php

// HAVE ROOMS
if (is_array($_POST['room'])) {

    // LOOP
    foreach ($_POST['room'] as $room_id => $room) {

        $query = '';

        if (!is_array($room['beds'])) $room['beds'] = array();

        // DB COLUMNS
        $columns = array(
            'level'             => $room['level'],
            'type_code'         => $room['type_code'],
            'dimensions'        => $room['dimensions'],
            'sleeps_num'        => (int)$room['sleeps_num'],
            'bed_codes'         => implode(',', $room['beds']),
            'has_ac'            => (int)$room['has_ac'],
            'has_ceiling_fan'   => (int)$room['has_ceiling_fan'],
            'has_fireplace'     => (int)$room['has_fireplace'],
            'description'       => $room['description'],
            'shared'            => (int)$room['shared'],
        );

        // NEW ROOM
        if ($room_id == 'new') {

            // HAVE TYPE CODE
            if ($columns['type_code']) {

                $columns['property_id'] = $property['id'];
                $columns['sort']        = 999;

                // INSERT INTO DB
                $query = "INSERT INTO `" .$_uccms_properties->tables['property_rooms']. "` SET " .uccms_createSet($columns);

            }

        // EXISTING ROOM
        } else {

            // UPDATE DB
            $query = "UPDATE `" .$_uccms_properties->tables['property_rooms']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$room_id. ") AND (`property_id`=" .$property['id']. ")";

        }

        // RUN QUERY
        if (sqlquery($query)) {

            if ($room_id == 'new') {
                $_uccms['_site-message']->set('success', 'Room added!');
            }

        }

    }

    // NO MESSAGES YET
    if ($_uccms['_site-message']->count() == 0) {
        $_uccms['_site-message']->set('success', 'Rooms updated.');
    }

}

// SORT ARRAY
$sorta = explode(',', $_POST['sort']);

// ARE DELETING
if ($_POST['delete']) {

    // GET ID'S TO DELETE
    $ida = explode(',', $_POST['delete']);

    // HAVE ID'S
    if (count($ida) > 0) {

        // LOOP
        foreach ($ida as $room_id) {

            // CLEAN UP
            $room_id = (int)$room_id;

            // HAVE VALID ID
            if ($room_id) {

                // REMOVE FROM DATABASE
                $query = "DELETE FROM `" .$_uccms_properties->tables['property_rooms']. "` WHERE (`id`=" .$room_id. ") AND (`property_id`=" .$property['id']. ")";
                sqlquery($query);

                // REMOVE FROM SORT ARRAY
                if (($key = array_search($room_id, $sorta)) !== false) {
                    unset($sorta[$key]);
                }


            }

        }

    }

}

// HAVE ROOMS TO SORT
if (count($sorta) > 0) {

    $i = 0;

    // LOOP
    foreach ($sorta as $room_id) {

        // CLEAN UP
        $room_id = (int)$room_id;

        // HAVE VALID ID
        if ($room_id) {

            // UPDATE DATABASE
            $query = "UPDATE `" .$_uccms_properties->tables['property_rooms']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$room_id. ") AND (`property_id`=" .$property['id']. ")";
            sqlquery($query);

            $i++;

        }

    }

}

?>