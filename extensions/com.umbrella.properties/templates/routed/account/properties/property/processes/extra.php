<?php

// DB COLUMNS
$columns = array(
    'agent_id'                  => (int)$_POST['property']['agent_id'],
    'sqft'                      => $_uccms_properties->toFloat($_POST['property']['sqft'], false),
    'lot_acres'                 => preg_replace('/\D\./', '', $_POST['property']['lot_acres']),
    'year_built'                => $_uccms_properties->toFloat($_POST['property']['year_built'], false),
    'accommodates_num'          => $_uccms_properties->toFloat($_POST['property']['accommodates_num'], false),
    'bedrooms_num'              => $_uccms_properties->toFloat($_POST['property']['bedrooms_num'], false),
    'bedrooms_num_master'       => $_uccms_properties->toFloat($_POST['property']['bedrooms_num_master'], false),
    'bathrooms_num'             => preg_replace('/\D\./', '', $_POST['property']['bathrooms_num']),
    'rental_bathrooms_code'     => $_POST['property']['rental_bathrooms_code'],
    'cooling_type_codes'        => implode(',', $_POST['property']['cooling']),
    'heating_type_codes'        => implode(',', $_POST['property']['heating']),
    'fireplace_num'             => $_uccms_properties->toFloat($_POST['property']['fireplace_num'], false),
    'washer_dryer_code'         => $_POST['property']['washer_dryer_code'],
    'elevator_num'              => $_uccms_properties->toFloat($_POST['property']['elevator_num'], false),
    'has_wifi'                  => (int)$_POST['property']['has_wifi'],
    'parking_num'               => $_uccms_properties->toFloat($_POST['property']['parking_num'], false),
    'parking_type_codes'        => implode(',', $_POST['property']['parking']),
    'gated_code'                => $_POST['property']['gated_code'],
    'pool_code'                 => $_POST['property']['pool_code'],
    'pool_size'                 => $_POST['property']['pool_size'],
    'pets_num'                  => (int)$_POST['property']['pets_num'],
    'pet_type_codes'            => implode(',', $_POST['property']['pets']),
    'checkin_office'            => $_POST['property']['checkin_office'],
    'checkin_day'               => $_uccms_properties->toFloat($_POST['property']['checkin_day'], false),
    'checkin_time'              => ($_POST['property']['checkin_time'] ? date('H:i:s', strtotime($_POST['property']['checkin_time'])) : '00:00:00'),
    'checkout_day'              => $_uccms_properties->toFloat($_POST['property']['checkout_day'], false),
    'checkout_time'             => ($_POST['property']['checkout_time'] ? date('H:i:s', strtotime($_POST['property']['checkout_time'])) : '00:00:00')
);

// DB QUERY
$query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$property['id']. ")";
if (sqlquery($query)) {
    $_uccms['_site-message']->set('success', 'Extra information saved.');
} else {
    $_uccms['_site-message']->set('error', 'Failed to save extra information.');
}

?>