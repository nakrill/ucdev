<?php

// DB COLUMNS
$columns = array(
    'property_id'               => $_POST['property']['property_id'],
    'listing_type'              => (int)$_POST['property']['listing_type'],
    'property_type_code'        => $_POST['property']['property_type_code'],
    'rental_space_code'         => $_POST['property']['rental_space_code'],
    'title'                     => $_POST['property']['title'],
    'description'               => $_POST['property']['description'],
    'price_from'                => $_uccms_properties->toFloat($_POST['property']['price_from']),
    'price_to'                  => $_uccms_properties->toFloat($_POST['property']['price_to']),
    'price_term_code'           => $_POST['property']['price_term_code']
);

if ($_POST['property']['title']) {
    $columns['slug'] = $_uccms_properties->makeRewrite($_POST['property']['title']);
}

// DB QUERY
$query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$property['id']. ")";
if (sqlquery($query)) {
    $_uccms['_site-message']->set('success', 'Basic information saved.');
} else {
    $_uccms['_site-message']->set('error', 'Failed to save basic information.');
}

?>