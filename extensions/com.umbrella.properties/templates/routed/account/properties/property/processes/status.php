<?php

$admin_notify = false;

//echo print_r($_POST);
//exit;

// CHANGING PAYMENT
if ($_POST['do-change-payment']) {

    // PAYMENT PROFILE SPECIFIED
    if ($_POST['payment']['profile_id']) {

        // GET PAYMENT PROFILE
        $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`id`=" .(int)$_POST['payment']['profile_id']. ") AND (`account_id`=" .$_uccms['_account']->userID(). ")";
        $pp_q = sqlquery($pp_query);
        $pp = sqlfetch($pp_q);

        // PAYMENT PROFILE FOUND
        if ($pp['id']) {

            // UPDATE PROPERTY
            $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET `payment_profile_id`=" .$pp['id']. " WHERE (`id`=" .$property['id']. ")";
            if (sqlquery($query)) {
                $_uccms['_site-message']->set('success', 'Payment method updated.');
            } else {
                $_uccms['_site-message']->set('error', 'Failed to update payment method.');
            }

        // PAYMENT PROFILE NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Payment method not found.');
        }

    // NEW
    } else {

        // GET ACCOUNT INFO
        $account = $_uccms['_account']->getAccount(null, true, $_uccms['_account']->userID());

        // PROFILE DATA
        $data = array(
            'customer' => array(
                'description'   => '',
                'email'         => $account['email']
            ),
            'card' => array(
                'name'          => $_POST['payment'][$_POST['payment']['method']]['name'],
                'number'        => $_POST['payment'][$_POST['payment']['method']]['number'],
                'expiration'    => $_POST['payment'][$_POST['payment']['method']]['expiration'],
                'zip'           => $_POST['payment'][$_POST['payment']['method']]['zip'],
                'cvv'           => $_POST['payment'][$_POST['payment']['method']]['cvv']
            ),
            'default' => (int)$_POST['payment'][$_POST['payment']['method']]['default']
        );

        // CREATE PROFILE
        $result = $_uccms['_account']->pp_createProfile($data);

        // PROFILE CREATED
        if ($result['id']) {

            $_uccms['_site-message']->set('success', 'Payment method added.');

            // UPDATE PROPERTY
            $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET `payment_profile_id`=" .$result['id']. " WHERE (`id`=" .$property['id']. ")";
            if (sqlquery($query)) {
                $_uccms['_site-message']->set('success', 'Listing updated.');
            } else {
                $_uccms['_site-message']->set('error', 'Failed to update listing.');
            }

        // FAILED
        } else {
            if ($result['error']) {
                $_uccms['_site-message']->set('error', $result['error']);
            } else {
                $_uccms['_site-message']->set('error', 'Failed adding payment method. Please try again.');
            }
        }

    }

}

// ACTIVATING
if ($_POST['do-activate']) {

    $payment_success = false;

    // GET PRICING INFO
    $pricing = $_uccms_properties->listingPricing($property['id']);

    // HAS EXPIRATION DATE
    if ($property['payment_expires'] != '0000-00-00') {

        // NOT EXPIRED
        if (strtotime($property['payment_expires']) > time()) {
            $pricing['listing_price'] = 0.00;
        }

    }

    // HAVE PRICE TO CHARGE
    if ($pricing['listing_price']) {

        // PAYMENT PROFILE SPECIFIED
        if ($_POST['payment']['profile_id']) {

            // GET PAYMENT PROFILE
            $pp = $_uccms['_account']->pp_getProfile((int)$_POST['payment']['profile_id'], $_uccms['_account']->userID());

            // PAYMENT PROFILE FOUND
            if ($pp['id']) {

                $payment_method     = 'profile';
                $payment_name       = stripslashes($pp['name']);
                $payment_lastfour   = $pp['lastfour'];
                $payment_expiration = $pp['expires'];

                // UPDATE PROPERTY
                $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET `payment_profile_id`=" .$pp['id']. " WHERE (`id`=" .$property['id']. ")";
                sqlquery($query);

            // PAYMENT PROFILE NOT FOUND
            } else {
                $payment_missing = true;
                $_uccms['_site-message']->set('error', 'Payment method not found.');
            }

        // USE CREDIT CARD INFO
        } else {

            // REQUIRE PAYMENT FIELDS
            $reqfa = array(
                'name'          => 'Name On Card',
                'number'        => 'Card Number',
                'expiration'    => 'Card Expiration',
                'zip'           => 'Card Zip',
                'cvv'           => 'Card CVV'
            );

            // CHECK REQUIRED
            foreach ($reqfa as $name => $title) {
                if (!$form_session['payment'][$name]) {
                    $payment_missing = true;
                    $_uccms['_site-message']->set('error', 'Missing / Incorrect: Payment - ' .$title);
                }
            }

            $payment_method = 'credit-card';

            $payment_name = $form_session['payment']['name'];

            // PAYMENT LAST FOUR
            $payment_lastfour = substr(preg_replace("/[^0-9]/", '', $form_session['payment']['number']), -4);

            // PAYMENT EXPIRATION DATE FORMATTING
            if ($form_session['payment']['expiration']) {
                $payment_expiration = date('Y-m-d', strtotime(str_replace('/', '/01/', $form_session['payment']['expiration'])));
            } else {
                $payment_expiration = '0000-00-00';
            }

        }

        // NO MISSING FIELDS
        if (!$payment_missing) {

            // PAYMENT - CHARGE
            $payment_data = array(
                'amount'    => $pricing['listing_price'],
                'customer' => array(
                    'id'            => $_uccms['_account']->userID(),
                    'name'          => $payment_name,
                    'email'         => stripslashes($account['email']),
                    'phone'         => $form_session['business']['phone'],
                    'address'       => array(
                        'street'    => '',
                        'street2'   => '',
                        'city'      => '',
                        'state'     => '',
                        'zip'       => '',
                        'country'   => ''
                    ),
                    'description'   => ''
                ),
                'note'  => stripslashes($plan['title'])
            );

            // HAVE PAYMENT PROFILE
            if ($pp['id']) {
                $payment_data['card'] = array(
                    'id'            => $pp['id'],
                    'name'          => $payment_name,
                    'number'        => $payment_lastfour,
                    'expiration'    => $payment_expiration
                );

            // USE CARD
            } else {
                $payment_data['card'] = array(
                    'name'          => $payment_name,
                    'number'        => $form_session['payment']['number'],
                    'expiration'    => $form_session['payment']['expiration'],
                    'zip'           => $form_session['payment']['zip'],
                    'cvv'           => $form_session['payment']['cvv']
                );
            }

            // PROCESS PAYMENT
            $payment_result = $_uccms['_account']->payment_charge($payment_data);

            // TRANSACTION ID
            $transaction_id = $payment_result['transaction_id'];

            // TRANSACTION MESSAGE (ERROR)
            $transaction_message = $payment_result['error'];

        }

    // FREE
    } else {
        $transaction_id = 'free';
    }

    // EXPIRATION LESS THAN TODAY OR HAVE PAYMENT AMOUNT
    if ((strtotime($property['payment_expires']) < time()) || ($pricing['listing_price'] > 0.00)) {

        // LOG TRANSACTION
        $_uccms_properties->logTransaction(array(
            'account_id'            => $_uccms['_account']->userID(),
            'property_id'           => $property['id'],
            'status'                => ($transaction_id ? 'charged' : 'failed'),
            'amount'                => $pricing['listing_price'],
            'term'                  => $pricing['listing_price_per'],
            'method'                => $payment_method,
            'payment_profile_id'    => $payment_result['profile_id'],
            'name'                  => $payment_name,
            'lastfour'              => $payment_lastfour,
            'expiration'            => $payment_expiration,
            'transaction_id'        => $transaction_id,
            'message'               => $transaction_message,
            'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
        ));

    }

    // CHARGE SUCCESSFUL
    if ($transaction_id) {
        $payment_success = true;
    }

    // PAYMENT WAS SUCCESSFUL (OR NO PAYMENT DUE)
    if ($payment_success) {

        switch ($pricing['listing_price_per']) {
            case 'w':
                $days = 7;
                break;
            case 'm':
                $days = 30;
                break;
            case 'q':
                $days = 90;
                break;
            case 'y':
                $days = 365;
                break;
            default:
                $days = 30;
                break;
        }

        // WAS INACTIVE
        if ($property['status'] == 0) {
            $admin_notify = 'activated';

        // WAS DRAFT
        } else if ($property['status'] == 4) {
            $admin_notify = 'activated';
        }

        // DB COLUMNS
        $columns = array(
            'status'    => 1
        );

        if ($pricing['listing_price'] > 0.00) {
            $columns['paying'] = 1;
        }

        // EXPIRATION LESS THAN TODAY
        if (strtotime($property['payment_expires']) < time()) {
            $columns['payment_expires'] = date('Y-m-d', strtotime('+' .$days. ' Days'));
        } else {
            if ($pricing['listing_price'] > 0.00) { // PAID AN AMOUNT
                $columns['payment_expires'] = date('Y-m-d', strtotime('+' .$days. ' Days', strtotime($property['payment_expires'])));
            }
        }

        // UPDATE PROPERTY
        $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$property['id']. ")";
        if (sqlquery($query)) {
            $_uccms['_site-message']->set('success', 'Listing is now active!');
        } else {
            $_uccms['_site-message']->set('error', 'Failed to make listing active.');
        }

    }

// DE-ACTIVATING
} else if ($_POST['do-deactivate']) {

    // WAS ACTIVE
    if ($property['status'] == 1) {

    // WAS PENDING
    } else if ($property['status'] == 5) {

    }

    // UPDATE PROPERTY
    $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET `status`=0, `dt_updated`=NOW() WHERE (`id`=" .$property['id']. ")";
    if (sqlquery($query)) {
        $_uccms['_site-message']->set('success', 'Listing is now inactive!');
    } else {
        $_uccms['_site-message']->set('error', 'Failed to make listing inactive.');
    }

}

// NOTIFY ADMIN
if ($admin_notify) {

    // GET EMAIL RELATED SETTINGS
    $esitesetta = $_uccms_properties->getSettings(array('email_from_name','email_from_email','email_admin_notify'));

    // HAVE EMAIL ADDRESS(S)
    if ($esitesetta['email_admin_notify']) {

        ##########################
        # EMAIL
        ##########################

        // EMAIL SETTINGS
        $esettings = array(
            'id'                => $property['id']
        );

        if ($esitesetta['email_from_name']) $esettings['from_name'] = $esitesetta['email_from_name'];
        if ($esitesetta['email_from_email']) $esettings['from_email'] = $esitesetta['email_from_email'];

        // PROPERTY NAME
        $property_name = stripslashes($property['title']);
        if (!$property_name) $property_name = $_uccms_properties->formatAddress($property);

        // PROPERTY LINKS
        $property_links = '<br /><br /><a href="' .$_uccms_properties->propertyURL($property['id'], $property['city_id'], $property['neighborhood_id'], $property). '" target="_blank" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">View</a> <a href="' .ADMIN_ROOT. $_uccms_properties->Extension. '*properties/properties/edit/?id=' .$property['id']. '" target="_blank" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">Edit</a>';

        // EMAIL VARIABLES
        $evars = array(
            'date'                  => date('n/j/Y'),
            'time'                  => date('g:i A T'),
            'id'                    => $property['id'],
            'property_name'         => $property_name,
            'property_links'        => $property_links
        );

        // ACTIVATED
        if ($admin_notify == 'activated') {

            // EMAIL SETTINGS
            $esettings['custom_template'] = SERVER_ROOT. 'extensions/' .$_uccms_properties->Extension. '/templates/email/account/property-activated.html';
            $esettings['subject']         = 'Property Activated';

            // EMAIL VARIABLES
            $evars['heading'] = 'Business Listing Added';

        }

        // COPY OF ORDER TO ADMIN(S)
        $emails = explode(',', stripslashes($esitesetta['email_admin_notify']));
        foreach ($emails as $email) {
            $emailtoa[$email] = $email;
        }

        // HAVE EMAILS TO SEND TO
        if (count($emailtoa) > 0) {

            // LOOP
            foreach ($emailtoa as $to_email => $to_name) {

                // WHO TO SEND TO
                $esettings['to_email'] = $to_email;

                // SEND EMAIL
                $result = $_uccms['_account']->sendEmail($esettings, $evars);

            }

        }

    }

}

?>