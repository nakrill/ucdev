<?php

// HAVE IMAGES
if (count($_FILES) > 0) {

    // LOOP THROUGH IMAGES
    foreach ($_FILES as $image_field => $values) {

        $image_id = 0;

        // NOT NEW
        if ($image_field != 'new') {

            // CLEAN UP
            $image_id = (int)str_replace('image-', '', $image_field);

            // HAVE IMAGE ID
            if ($image_id) {

                // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
                if ($_FILES[$image_field]['name']) {

                    // GET CURRENT IMAGE
                    $ex_query = "SELECT `image` FROM `" .$_uccms_properties->tables['property_images']. "` WHERE (`id`=" .$image_id. ") AND (`property_id`=" .$property['id']. ")";
                    $ex = sqlfetch(sqlquery($ex_query));

                    // THERE'S AN EXISTING IMAGE
                    if ($ex['image']) {

                        // REMOVE IMAGES
                        @unlink($_uccms_properties->imageBridgeOut($ex['image'], 'properties', true));
                        @unlink(BigTree::prefixFile($_uccms_properties->imageBridgeOut($ex['image'], 'properties', true), 't_'));

                        // UPDATE DATABASE
                        $query = "UPDATE `" .$_uccms_properties->tables['property_images']. "` SET `image`='' WHERE (`id`=" .$image_id. ")";
                        sqlquery($query);

                    }

                }

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES[$image_field]['name']) || ($_POST[$image_field])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'Image',
                'key'           => $image_field,
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_properties->Extension. '/files/properties',
                    'image' => true,
                    'thumbs' => array(
                        array(
                            'width'     => '1024', // MATTD: make controlled through settings
                            'height'    => '1024' // MATTD: make controlled through settings
                        ),
                        array(
                            'prefix'    => 't_',
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES[$image_field]['name']) {
                $field['file_input'] = $_FILES[$image_field];
            }

            //echo print_r($field);
            //exit;

            // INIT BIGTREE ADMIN
            if (!$admin) $admin = new BigTreeAdmin;

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // DB COLUMNS
                $columns = array(
                    'image' => $_uccms_properties->imageBridgeIn($file_path)
                );

                // NEW IMAGE
                if ($image_field == 'new') {

                    $columns['property_id'] = $property['id'];
                    $columns['sort']        = 999;

                    // INSERT INTO DB
                    $query = "INSERT INTO `" .$_uccms_properties->tables['property_images']. "` SET " .uccms_createSet($columns). "";

                // EXISTING IMAGE
                } else if ($image_id) {

                    // UPDATE DB
                    $query = "UPDATE `" .$_uccms_properties->tables['property_images']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$image_id. ") AND (`property_id`=" .$property['id']. ")";

                }

                // RUN QUERY
                if (sqlquery($query)) {

                    if (!$image_id) $image_id = sqlid();

                    $_uccms['_site-message']->set('success', 'Image added!');

                }

            // UPLOAD FAILED
            } else {
                $_uccms['_site-message']->set('error', 'Failed to upload image. (' .$bigtree['errors'][0]['error']. ')');
            }

        }

        // HAVE ID
        if ($image_id) {

            // DB COLUMNS
            $columns = array(
                'caption' => $_POST[$image_field. '-caption']
            );

            // UPDATE DB
            $query = "UPDATE `" .$_uccms_properties->tables['property_images']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$image_id. ") AND (`property_id`=" .$property['id']. ")";
            sqlquery($query);

        }

    }

}

// SORT ARRAY
$sorta = explode(',', $_POST['sort']);

// ARE DELETING
if ($_POST['delete']) {

    // GET ID'S TO DELETE
    $ida = explode(',', $_POST['delete']);

    // HAVE ID'S
    if (count($ida) > 0) {

        // LOOP
        foreach ($ida as $image_id) {

            // CLEAN UP
            $image_id = (int)$image_id;

            // HAVE VALID ID
            if ($image_id) {

                // GET CURRENT IMAGE
                $ex_query = "SELECT `image` FROM `" .$_uccms_properties->tables['property_images']. "` WHERE (`id`=" .$image_id. ")";
                $ex = sqlfetch(sqlquery($ex_query));

                // THERE'S AN EXISTING IMAGE
                if ($ex['image']) {

                    // REMOVE IMAGES
                    @unlink($_uccms_properties->imageBridgeOut($ex['image'], 'properties', true));
                    @unlink(BigTree::prefixFile($_uccms_properties->imageBridgeOut($ex['image'], 'properties', true), 't_'));

                }

                // REMOVE FROM DATABASE
                $query = "DELETE FROM `" .$_uccms_properties->tables['property_images']. "` WHERE (`id`=" .$image_id. ") AND (`property_id`=" .$property['id']. ")";
                sqlquery($query);

                // REMOVE FROM SORT ARRAY
                if (($key = array_search($image_id, $sorta)) !== false) {
                    unset($sorta[$key]);
                }


            }

        }

    }

}

// HAVE IMAGES TO SORT
if (count($sorta) > 0) {

    $i = 0;

    // LOOP
    foreach ($sorta as $image_id) {

        // CLEAN UP
        $image_id = (int)$image_id;

        // HAVE VALID ID
        if ($image_id) {

            // UPDATE DATABASE
            $query = "UPDATE `" .$_uccms_properties->tables['property_images']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$image_id. ") AND (`property_id`=" .$property['id']. ")";
            sqlquery($query);

            $i++;

        }

    }

}

?>