<?php

// HAVE VIDEOS
if (is_array($_POST['video'])) {

    // LOOP
    foreach ($_POST['video'] as $video_id => $video) {

        $query = '';

        // DB COLUMNS
        $columns = array(
            'property_id'   => $property['id'],
            'sort'          => 999,
            'video'         => $video['url'],
            'caption'       => $video['caption']
        );

        // NEW VIDEO
        if ($video_id == 'new') {

            // HAVE VIDEO
            if ($columns['video']) {

                // INSERT INTO DB
                $query = "INSERT INTO `" .$_uccms_properties->tables['property_videos']. "` SET " .uccms_createSet($columns);

            }

        // EXISTING VIDEO
        } else {

            // UPDATE DB
            $query = "UPDATE `" .$_uccms_properties->tables['property_videos']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$video_id. ") AND (`property_id`=" .$property['id']. ")";

        }

        // HAVE QUERY
        if ($query) {

            // RUN QUERY
            if (sqlquery($query)) {

                // NEW
                if ($video_id == 'new') {
                    $_uccms['_site-message']->set('success', 'Video added!');
                }

            } else {
                $_uccms['_site-message']->set('error', 'Failed to add video.');
            }

        }

    }

    // NO MESSAGES YET
    if ($_uccms['_site-message']->count() == 0) {
        $_uccms['_site-message']->set('success', 'Videos updated.');
    }

}

// SORT ARRAY
$sorta = explode(',', $_POST['sort']);

// ARE DELETING
if ($_POST['delete']) {

    // GET ID'S TO DELETE
    $ida = explode(',', $_POST['delete']);

    // HAVE ID'S
    if (count($ida) > 0) {

        // LOOP
        foreach ($ida as $video_id) {

            // CLEAN UP
            $video_id = (int)$video_id;

            // HAVE VALID ID
            if ($video_id) {

                // REMOVE FROM DATABASE
                $query = "DELETE FROM `" .$_uccms_properties->tables['property_videos']. "` WHERE (`id`=" .$video_id. ") AND (`property_id`=" .$property['id']. ")";
                sqlquery($query);

                // REMOVE FROM SORT ARRAY
                if (($key = array_search($video_id, $sorta)) !== false) {
                    unset($sorta[$key]);
                }

            }

        }

    }

}

// HAVE VIDEOS TO SORT
if (count($sorta) > 0) {

    $i = 0;

    // LOOP
    foreach ($sorta as $video_id) {

        // CLEAN UP
        $video_id = (int)$video_id;

        // HAVE VALID ID
        if ($video_id) {

            // UPDATE DATABASE
            $query = "UPDATE `" .$_uccms_properties->tables['property_videos']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$video_id. ") AND (`property_id`=" .$property['id']. ")";
            sqlquery($query);

            $i++;

        }

    }

}

?>