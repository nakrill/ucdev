<?php

$city_id = (int)$_POST['property']['city_id'];

// HAVE CITY
if ($city_id) {

    // GET CITY INFO
    $city_query = "SELECT * FROM `" .$_uccms_properties->tables['cities']. "` WHERE (`id`=" .$city_id. ")";
    $city_q = sqlquery($city_query);
    $city = sqlfetch($city_q);

    if ($city['title']) {
        $_POST['property']['city'] = stripslashes($city['title']);
    }

}

// DB COLUMNS
$columns = array(
    'city_id'                   => $city_id,
    'neighborhood_id'           => (int)$_POST['property']['neighborhood'][$_POST['property']['city_id']]['neighborhood_id'],
    'address1'                  => $_POST['property']['address1'],
    'address2'                  => $_POST['property']['address2'],
    'unit'                      => $_POST['property']['unit'],
    'city'                      => $_POST['property']['city'],
    'state'                     => $_POST['property']['state'],
    'zip'                       => $_POST['property']['zip'],
    'zip4'                      => $_POST['property']['zip4'],
    'location_code'             => $_POST['property']['location_code']
);

// DB QUERY
$query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET " .uccms_createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$property['id']. ")";
if (sqlquery($query)) {
    $_uccms['_site-message']->set('success', 'Location information saved.');
} else {
    $_uccms['_site-message']->set('error', 'Failed to save location information.');
}

?>