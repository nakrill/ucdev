<?php

// IS AGENT OR AGENCY
if (($_properties_agent['id']) || ($_properties_agency['id'])) {

    // TABS
    include(dirname(__FILE__). '/tabs.php');

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // ID SPECIFIED
    if ($id) {

        // IS AGENCY
        if ($_properties_agency['id']) {
            $where_sql = "`agency_id`=" .$_properties_agency['id'];

        // IS AGENT
        } else if ($_properties_agent['id']) {
            $where_sql = "`agent_id`=" .$_properties_agent['id'];
        }

        // GET PROPERTY INFO
        $property_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`id`=" .$id. ") AND (" .$where_sql. ")";
        $property_q = sqlquery($property_query);
        $property = sqlfetch($property_q);

    }

    // NEW PROPERTY
    if (!$property['id']) {

        // ONLY FIRST TAB
        $taba = array_slice($taba, 0, 1);

        // DEFAULT TO SALE
        $property['listing_type'] = 1;

    }

    ?>

    <script src="/admin/js/tinymce4/tinymce.js"></script>

    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/account/master/property.css" />
    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/account/custom/property.css" />
    <script src="/extensions/<?=$_uccms_properties->Extension;?>/js/account/master/property.js"></script>
    <script src="/extensions/<?=$_uccms_properties->Extension;?>/js/account/custom/property.js"></script>

    <style type="text/css">

        <?php if ($property['listing_type'] == 1) { ?>
            #myAccount .section.property .lt-rental {
                display: none;
            }
            #myAccount .section.property .lt-sale {
                display: block;
            }
        <?php } else if ($property['listing_type'] == 2) { ?>
            #myAccount .section.property .lt-sale {
                display: none;
            }
            #myAccount .section.property .lt-rental {
                display: block;
            }
        <?php } ?>

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // LISTING TYPE
            $('#myAccount .section.property form.main #listing_type').change(function() {
                if ($(this).val() == 1) {
                    $('#myAccount .section.property .lt-rental').hide();
                    $('#myAccount .section.property .lt-sale').hide();
                } else if ($(this).val() == 2) {
                    $('#myAccount .section.property .lt-sale').hide();
                    $('#myAccount .section.property .lt-rental').show();
                }
            });

            // DATE / TIME PICKER
            $('#myAccount .section.property form.main .date_picker').datepicker({ dateFormat: 'mm/dd/yy', duration: 200, showAnim: "slideDown" });
            $('#myAccount .section.property form.main .time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
            $('#myAccount .section.property form.main .date_time_picker').datetimepicker({ dateFormat: 'mm/dd/yy', timeFormat: "hh:mm tt", duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10 });

        });

    </script>

    <div class="section property">

        <h3><?php if ($property['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Listing</h3>

        <form action="./process/" method="post" enctype="multipart/form-data" class="main">
        <input type="hidden" name="tab" value="<?php echo $tab; ?>" />
        <input type="hidden" name="id" value="<?php echo $property['id']; ?>" />
        <input type="hidden" name="goto_tab" value="<?php echo $tab; ?>" />

        <div class="tabs1 main">
            <ul class="nav">
                <?php foreach ($taba as $tab_id => $tab_title) { ?>
                    <li><a href="?id=<?php echo $property['id']; ?>&tab=<?php echo $tab_id; ?>" <?php if ($tab_id == $tab) { ?>class="active"<?php } ?> data-tab="<?php echo $tab_id; ?>"><?php echo $tab_title; ?></a></li>
                <?php } ?>
            </ul>
            <div class="content">
                <div id="tab-<?php echo $tab; ?>" class="item">
                    <?php include(dirname(__FILE__). '/tabs/' .$tab. '.php'); ?>
                </div>
            </div>
        </div>

        <?php if (($tab != 'status') && ($tab != 'reviews')) { ?>
            <div class="main_buttons">
                <input type="submit" value="Save" class="button save" />
                <?php if ($tab_next) { ?>
                    <input type="button" value="Save & Continue" class="button save-continue" data-next="<?php echo $tab_next; ?>" />
                <?php } ?>
            </div>
        <?php } ?>

        </form>

    </div>

    <?php include(BigTree::path('admin/layouts/_html-field-loader.php')); ?>

<?php } else { ?>

    Access denied.

<?php } ?>