<?php

$tab = '';

// TABS
$taba = array(
    'basic'         => 'Basic',
    'location'      => 'Location',
    'rooms'         => 'Rooms',
    //'extra'         => 'Extra',
    //'attributes'    => 'Attributes',
    'amenities'     => 'Amenities',
    'images'        => 'Images',
    'videos'        => 'Videos',
    'pricing'       => 'Pricing',
    'status'        => 'Status',
    'reviews'       => 'Reviews'
);

// RATINGS / REVIEWS ENABLED
if ($_uccms_properties->getSetting('ratings_reviews_enabled')) {
    $taba['reviews'] = 'Reviews';
}

if ($taba[$_REQUEST['tab']]) {
    $tab = $_REQUEST['tab'];
} else {
    $tab = 'basic';
}

// NEXT
$tab_keys = array_keys($taba);
$tab_position = array_search($tab, $tab_keys);
if (isset($tab_keys[$tab_position + 1])) {
    $tab_next = $tab_keys[$tab_position + 1];
}

?>