<?php

/*

// GET PROPERTY INFO
$property_query = "SELECT * FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`id`=" .$id. ") AND (" .$where_sql. ")";
$property_q = sqlquery($property_query);
$property = sqlfetch($property_q);

*/

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    unset($wa);

    // IS AGENCY
    if ($_properties_agency['id']) {
        $wa[] = "`agency_id`=" .$_properties_agency['id'];
    }

    // IS AGENT
    if ($_properties_agent['id']) {
        $wa[] = "`agent_id`=" .$_properties_agent['id'];
    }

    // WHERE
    $where_sql = "(" .implode(") OR (", $wa). ")";

    // MARK AS DELETED
    $query = "UPDATE `" .$_uccms_properties->tables['properties']. "` SET `status`=9, `dt_deleted`=NOW() WHERE (`id`=" .$id. ") AND (" .$where_sql. ")";
    if (sqlquery($query)) {
        $_uccms['_site-message']->set('success', 'Property listing deleted.');
    } else {
        $_uccms['_site-message']->set('error', 'Failed to delete property listing.');
    }

}

// REDIRECT
if ($_REQUEST['return']) {
    BigTree::redirect(urldecode($_REQUEST['return']));
} else {
    BigTree::redirect('../../listings/');
}

?>