<?php

// MODULE CLASS
if (!$_uccms_properties) $_uccms_properties = new uccms_Properties;

// GET SETTING(S)
$_properties['settings'] = $_uccms_properties->getSettings();

#########################################################

// RATINGS / REVIEWS
include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

$wa = array();

// IS AGENCY
if ($_properties_agency['id']) {
    $wa[] = "`agency_id`=" .$_properties_agency['id'];
}

// IS AGENT
if ($_properties_agent['id']) {
    $wa[] = "`agent_id`=" .$_properties_agent['id'];
}

// WHERE
$where_sql = "(" .implode(") OR (", $wa). ")";

// GET MATCHING PROPERTIES
$prop_query = "SELECT `id` FROM `" .$_uccms_properties->tables['properties']. "` WHERE (`status`!=9) AND (" .$where_sql. ") ORDER BY `title` ASC, `dt_updated` ASC";
$prop_q = sqlquery($prop_query);

// NUMBER OF PROPERTIES
$num_prop = sqlrows($prop_q);

// NUMBER OF REVIEWS
$num_reviews = 0;

// REVIEWS ENABLED
if ($_uccms_properties->getSetting('ratings_reviews_enabled')) {

    // LOOP
    while ($prop = sqlfetch($prop_q)) {

        // GET REVIEWS FOR PROPERTY
        $reviewsnum_query = "SELECT `id` FROM `uccms_ratings_reviews` WHERE (`source`='" .$_uccms_properties->Extension. "') AND (`what`='property') AND (`item_id`=" .$prop['id']. ") AND (`status`=1)";
        $reviewsnum_q = sqlquery($reviewsnum_query);

        $num_reviews += sqlrows($reviewsnum_q);

    }

}

#########################################################

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/account/master/master.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/account/custom/master.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/account/master/master.js"></script>
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/account/custom/master.js"></script>

<h3>Properties</h3>

<div class="dashboard clearfix">

    <div class="colSmall">

        <div class="infobox">

            <div class="name">
                <?php
                if ($_properties_agent['name']) {
                    echo stripslashes($_properties_agent['name']);
                } else if ($_properties_agency['title']) {
                    echo stripslashes($_properties_agency['title']);
                }
                ?>
            </div>

            <?php if ($_properties_agent['image']) { ?>
                <img src="<?php echo $_uccms_properties->imageBridgeOut($_properties_agent['image'], 'agents'); ?>" alt="<?php echo stripslashes($_properties_agent['name']); ?>" class="main" />
            <?php } else if ($_properties_agency['image']) { ?>
                <img src="<?php echo $_uccms_properties->imageBridgeOut($_properties_agency['image'], 'agencies'); ?>" alt="<?php echo stripslashes($_properties_agency['title']); ?>" class="main" />
            <?php } ?>

        </div>

        <div class="nav">
            <ul>
                <?php if ($_properties_agency['id']) { ?>
                    <li class="<?php if ($bigtree['path'][2] == 'agency') { echo 'active'; } ?>"><a href="/<?php echo $bigtree['path'][0]; ?>/<?php echo $bigtree['path'][1]; ?>/agency/">Agency</a></li>
                <?php } ?>
                <?php if ($_properties_agent['id']) { ?>
                    <li class="<?php if ($bigtree['path'][2] == 'agent') { echo 'active'; } ?>"><a href="/<?php echo $bigtree['path'][0]; ?>/<?php echo $bigtree['path'][1]; ?>/agent/">Agent</a></li>
                <?php } ?>
            </ul>
            <ul>
                <li class="<?php if ($bigtree['path'][2] == 'listings') { echo 'active'; } ?>"><a href="/<?php echo $bigtree['path'][0]; ?>/<?php echo $bigtree['path'][1]; ?>/listings/">Listings<span class="num"><?php echo number_format($num_prop, 0); ?></span></a></li>
                <?php if ($_uccms_properties->getSetting('ratings_reviews_enabled')) { ?>
                    <li class="<?php if ($bigtree['path'][2] == 'reviews') { echo 'active'; } ?>"><a href="/<?php echo $bigtree['path'][0]; ?>/<?php echo $bigtree['path'][1]; ?>/reviews/">Reviews<span class="num"><?php echo number_format($num_reviews, 0); ?></span></a></li>
                <?php } ?>
            </ul>
        </div>

    </div>

    <div class="colLarge-container">

        <div class="colLarge">

            <?php

            // INCLUDE FILE
            $include_file = dirname(__FILE__). '/dashboard/default.php';

            // HAVE COMMANDS
            if (count($bigtree['commands']) > 1) {

                $base = array_shift($bigtree['commands']);

                // GET BIGTREE ROUTING
                list($include, $commands) = BigTree::route(SERVER_ROOT. 'extensions/' .$_uccms_properties->Extension. '/templates/routed/account/' .$base. '/', $bigtree['commands']);

                // FILE TO INCLUDE
                if ($include) {
                    $include_file = $include;
                }

            }

            // INCLUDE ROUTED FILE
            include($include_file);

            ?>

        </div>

    </div>

</div>