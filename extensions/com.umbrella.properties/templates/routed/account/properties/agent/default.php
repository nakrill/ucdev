<?php

// IS AGENT
if ($_properties_agent['id']) {

    ?>

    <style type="text/css">

        #myAccount .section.agent input[type="text"] {
            width: 300px;
        }
        #myAccount .section.agent .image_field .or, #myAccount .section.agent .image_field .form_image_browser, #myAccount .section.agent .image_field .remove_resource, #myAccount .section.agent .image_field .currently label {
            display: none;
        }
        #myAccount .section.agent input.phone {
            width: 120px;
        }

    </style>

    <div class="section agent">

        <h3>Update Agent</h3>

        <form action="./process/" method="post">

        <div class="grid">

            <div class="col-1-2">

                <fieldset>
                    <label>Agent Name</label>
                    <input type="text" value="<?=$_properties_agent['name']?>" name="agent[name]" />
                </fieldset>

                <fieldset>
                    <label>Address</label>
                    <input type="text" value="<?=$_properties_agent['address1']?>" name="agent[address1]" />
                </fieldset>

                <fieldset>
                    <label>Address Cont.</label>
                    <input type="text" value="<?=$_properties_agent['address2']?>" name="agent[address2]" />
                </fieldset>

                <fieldset>
                    <label>City</label>
                    <input type="text" value="<?=$_properties_agent['city']?>" name="agent[city]" />
                </fieldset>

                <fieldset>
                    <label>State</label>
                    <select name="agent[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?=$state_code?>" <?php if ($state_code == $_properties_agent['state']) { ?>selected="selected"<?php } ?>><?=$state_name?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Zip</label>
                    <input type="text" value="<?=$_properties_agent['zip']?>" name="agent[zip]" />
                </fieldset>

            </div>

            <div class="col-1-2">

                <fieldset>
                    <label>Phone</label>
                    <input type="text" value="<?=$_properties_agent['phone']?>" name="agent[phone]" class="phone" />&nbsp;&nbsp;&nbsp;Ext: <input type="text" value="<?=$_properties_agent['phone_ext']?>" name="agent[phone_ext]" style="display: inline-block; width: 50px;" />
                </fieldset>

                <fieldset>
                    <label>Mobile</label>
                    <input type="text" value="<?=$_properties_agent['phone_mobile']?>" name="agent[phone_mobile]" class="phone" />
                </fieldset>

                <fieldset>
                    <label>Fax</label>
                    <input type="text" value="<?=$_properties_agent['fax']?>" name="agent[fax]" class="phone" />
                </fieldset>

                <fieldset>
                    <label>Email</label>
                    <input type="text" value="<?=$_properties_agent['email']?>" name="agent[email]" />
                </fieldset>

                <fieldset>
                    <label>Lead Email</label>
                    <input type="text" value="<?=$_properties_agent['email_lead']?>" name="agent[email_lead]" />
                </fieldset>

                <fieldset>
                    <label>Website URL</label>
                    <input type="text" value="<?=$_properties_agent['url']?>" name="agent[url]" placeholder="www.domain.com" />
                </fieldset>

            </div>

        </div>

        <fieldset>
            <label>Image</label>
            <div>
                <?php
                $field = array(
                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                    'key'       => 'image', // The value you should use for the "name" attribute of your form field
                    'value'     => ($_properties_agent['image'] ? $_uccms_properties->imageBridgeOut($_properties_agent['image'], 'agents') : ''), // The existing value for this form field
                    'id'        => 'agent_image', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'image' => true
                    ),
                    'required'  => false // A boolean value of whether this form field is required or not
                );
                include(BigTree::path('admin/form-field-types/draw/upload.php'));
                ?>
            </div>
        </fieldset>

        <input type="submit" value="Save" />

        </form>

    </div>

    <?php include(BigTree::path('admin/layouts/_html-field-loader.php')); ?>

<?php } else { ?>

    You are not an agent.

<?php } ?>