<?php

// STATS DATES
$stats_date_from    = date('Y-m-d H:i:s', strtotime('-7 Days'));
$stats_date_to      = date('Y-m-d H:i:s');

$wa = array();

// IS AGENCY
if ($_properties_agency['id']) {
    $wa[] = "p.agency_id=" .$_properties_agency['id'];
}

// IS AGENT
if ($_properties_agent['id']) {
    $wa[] = "p.agent_id=" .$_properties_agent['id'];
}

// WHERE
$where_sql = "(" .implode(") OR (", $wa). ")";

// STATS TOTALS
$stat_total_nvtf = 0;
$stat_total_nvt = 0;
$stat_total_netf = 0;
$stat_total_net = 0;

// STATS - GET ALL PROPERTIES
$prop_query = "SELECT p.id FROM `" .$_uccms_properties->tables['properties']. "` AS `p` WHERE (p.status!=9) AND (" .$where_sql. ")";
$prop_q = sqlquery($prop_query);
while ($prop = sqlfetch($prop_q)) {

    // GET TOTAL STATS
    $stats_total = $_uccms_properties->property_getStats($prop['id']);

    $stat_total_nvt += $stats_total['view'];
    $stat_total_net += $stats_total['email'];

    // GET TIMEFRAME STATS
    $stats_timeframe = $_uccms_properties->property_getStats($prop['id'], array(
        'date_from' => date('Y-m-d H:i:s', strtotime('-7 Days')),
        'date_to'   => date('Y-m-d H:i:s')
    ));

    $stat_total_nvtf += $stats_timeframe['view'];
    $stat_total_netf += $stats_timeframe['email'];

}

?>

<link rel="stylesheet" type="text/css" href="/css/lib/ratings-reviews/review-admin.css" />
<script src="/js/lib/ratings-reviews/review-admin.js"></script>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/account/master/dashboard.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_properties->Extension;?>/css/account/custom/dashboard.css" />
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/account/master/dashboard.js"></script>
<script src="/extensions/<?=$_uccms_properties->Extension;?>/js/account/custom/dashboard.js"></script>

<div class="section top_stats clearfix">
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_nvtf, 0); ?></span>
            <span class="title">View<?php if ($stat_total_nvtf != 1) { ?>s<?php } ?></span>
            <span class="period">7 Days</span>
        </div>
    </div>
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_nvt, 0); ?></span>
            <span class="title">View<?php if ($stat_total_nvt != 1) { ?>s<?php } ?></span>
            <span class="period">Total</span>
        </div>
    </div>
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_netf, 0); ?></span>
            <span class="title">Email<?php if ($stat_total_netf != 1) { ?>s<?php } ?></span>
            <span class="period">7 Days</span>
        </div>
    </div>
    <div class="stat">
        <div class="padding">
            <span class="num"><?php echo number_format((int)$stat_total_net, 0); ?></span>
            <span class="title">Email<?php if ($stat_total_net != 1) { ?>s<?php } ?></span>
            <span class="period">Total</span>
        </div>
    </div>
</div>

<?php

// GET LATEST REVIEWS
$reviews_query = "
SELECT rr.*
FROM `uccms_ratings_reviews` AS `rr`
INNER JOIN `" .$_uccms_properties->tables['properties']. "` AS `p` ON rr.item_id=p.id
WHERE (" .$where_sql. ") AND (rr.reply_to_id=0) AND (rr.status=1) AND (rr.source='" .$_uccms_properties->Extension. "') AND (rr.what='property') AND (p.status!=9)
GROUP BY rr.id
ORDER BY `dt_created` DESC
LIMIT 5
";
$reviews_q = sqlquery($reviews_query);

// HAVE REVIEWS
if (sqlrows($reviews_q) > 0) {

    // REVIEW SETTINGS
    $review_admin = array(
        'source'        => $_uccms_properties->Extension,
        'what'          => 'property',
        'perms'         => array(
            'reply'     => true
        ),
        'hide_replies'  => false
    );

    ?>

    <div class="section reviews uccms_ratings-reviews">

        <div class="heading-main clearfix">
            <h4>Latest Reviews</h4>
            <a href="./reviews/" class="add"><i class="fa fa-eye" aria-hidden="true"></i> All Reviews</a>
        </div>

        <div class="results">
            <div class="reviews items clearfix">
                <?php

                // LOOP
                while ($review = sqlfetch($reviews_q)) {

                    // IS OWNER
                    $_uccms_ratings_reviews->setReviewAccessSession($review, 'reply', true);

                    // DISPLAY
                    include(SERVER_ROOT. 'templates/elements/ratings-reviews/review-admin.php');

                }

                ?>
            </div>
        </div>

    </div>

    <?php

}

// GET MATCHING PROPERTIES
$prop_query = "SELECT p.id FROM `" .$_uccms_properties->tables['properties']. "` AS `p` WHERE (p.status!=9) AND (" .$where_sql. ") ORDER BY p.dt_updated DESC LIMIT 5";
$prop_q = sqlquery($prop_query);

// NUMBER OF PROPERTIES
$num_prop = sqlrows($prop_q);

?>

<a name="listings"></a>

<div class="section listings">

    <div class="heading-main clearfix">
        <h4>Latest Listings</h4>
        <a href="./listings/" class="add"><i class="fa fa-eye" aria-hidden="true"></i> All Listings</a>
    </div>

    <?php

    // HAVE PROPERTIES
    if ($num_prop > 0) {

        ?>

        <div class="properties clearfix">

            <?php

            // LOOP
            while ($prop = sqlfetch($prop_q)) {

                // GET PROPERTY
                $prop = $_uccms_properties->getProperty($prop['id'], false, array('agent','images'));

                // PROPERTY URL
                $prop_url = $_uccms_properties->propertyURL($prop['id'], $prop['city_id'], $prop['neighborhood_id'], $prop);

                // EDIT URL
                $edit_url = './property/?id=' .$prop['id'];

                // NUMBER OF IMAGES
                $num_images = count($prop['images']);

                // MAIN IMAGE
                if ($num_images > 0) {
                    $image = $_uccms_properties->imageBridgeOut($prop['images'][array_keys($prop['images'])[0]]['image'], 'properties');
                } else {
                    $image = '/extensions/' .$_uccms_properties->Extension. '/images/property_no-image.jpg';
                }

                // FORMATTED ADDRESS
                $address = $_uccms_properties->formatAddress($prop, ' ');

                // GET TOTAL STATS
                $stats_total = $_uccms_properties->property_getStats($prop['id']);

                // GET TIMEFRAME STATS
                $stats_timeframe = $_uccms_properties->property_getStats($prop['id'], array(
                    'date_from' => date('Y-m-d H:i:s', strtotime('-7 Days')),
                    'date_to'   => date('Y-m-d H:i:s')
                ));

                ?>
                <div class="property">
                    <div class="clearfix">
                        <div class="image">
                            <a href="<?php echo $edit_url; ?>"><img src="<?php echo $image; ?>" /></a>
                            <div class="num">
                                <a href="<?php echo $edit_url; ?>&tab=images"><?php echo $num_images; ?> image<?php if ($num_images != 1) { echo 's'; } ?></a>
                            </div>
                        </div>
                        <div class="main">
                            <? /*<div class="type">For <?php if ($prop['listing_type'] == 2) { ?>Rent<?php } else { ?>Sale<?php } ?></div>*/ ?>
                            <?php if ($prop['title']) { ?>
                                <div class="title"><a href="<?php echo $edit_url; ?>"><?php echo stripslashes($prop['title']); ?></a></div>
                            <?php } ?>
                            <?php if ($address) { ?>
                                <div class="address">
                                    <?php echo $address; ?>
                                </div>
                            <?php } ?>
                            <div class="price"><span class="from"><span class="sign">$</span><?php echo $prop['price_from_formatted']; ?></span><?php if ($prop['price_to'] != 0.00) { ?> - <span class="to"><span class="sign">$</span><?php echo $prop['price_to_formatted']; ?></span><?php } ?><?php if ($prop['price_term_code']) { ?><span class="term">/<?php echo $_uccms_properties->propertyPriceTerms()[$prop['price_term_code']]; ?></span><?php } ?></div>
                            <div class="quick clearfix">
                                <?php if ($prop['bedrooms_num'] > 0) { ?>
                                    <div class="item bedrooms" title="Bedrooms"><i class="fa fa-bed" aria-hidden="true"></i><?php echo number_format($prop['bedrooms_num'], 0); ?></div>
                                <?php } ?>
                                <?php if ($prop['bathrooms_num'] > 0) { ?>
                                    <div class="item bathrooms" title="Bathrooms"><i class="fa fa-tint" aria-hidden="true"></i><?php echo number_format($prop['bathrooms_num'], 0); ?></div>
                                <?php } ?>
                            </div>
                            <div class="stats clearfix">
                                <div class="group">
                                    <h4>7 Days</h4>
                                    <div class="clearfix">
                                        <div class="stat" title="Views in the past 7 days">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_timeframe['view'], 0); ?></span>
                                            </div>
                                        </div>
                                        <div class="stat" title="Emails in the past 7 days">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_timeframe['email'], 0); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="group">
                                    <h4>Total</h4>
                                    <div class="clearfix">
                                        <div class="stat" title="Total views">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_total['view'], 0); ?></span>
                                            </div>
                                        </div>
                                        <div class="stat" title="Total emails">
                                            <div class="padding">
                                                <span class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                                <span class="num"><?php echo number_format((int)$stats_total['email'], 0); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="buttons">
                            <a href="<?php echo $prop_url; ?>" target="_blank" class="button view"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                            <a href="<?php echo $edit_url; ?>" class="button edit"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                            <a href="./property/delete/?id=<?php echo $prop['id']; ?>&return=<?php echo urlencode('/' .$bigtree['path'][0]. '/' .$bigtree['path'][1]. '/#listings'); ?>" class="button delete" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                        </div>
                    </div>
                </div>
                <?php

            }

            ?>

        </div>
        <?php

    // NO PROPERTIES
    } else {
        ?>
        <div class="no_listings">
            It doesn't look like you have any properties yet.
        </div>
        <?php
    }

    ?>

</div>

<div class="section agency_agent grid">

    <?php if ($_properties_agency['id']) { ?>
        <div class="item agency col-1-2">
            <h4>Agency</h4>
            <div class="clearfix">
                <?php if ($_properties_agency['image']) { ?>
                    <img src="<?php echo $_uccms_properties->imageBridgeOut($_properties_agency['image'], 'agencies'); ?>" alt="<?php echo stripslashes($_properties_agency['title']); ?>" class="main" />
                <?php } ?>
                <div class="name">
                    <?php if ($_properties_agency['title']) { ?><?php echo stripslashes($_properties_agency['title']); ?> <?php } ?> <span class="edit">(<a href="./agency/">edit</a>)</span>
                </div>
                <div class="address">
                    <?php if ($_properties_agency['address1']) { ?>
                        <span class="address1"><?php echo stripslashes($_properties_agency['address1']); ?></span>
                        <br />
                    <?php } ?>
                    <?php if ($_properties_agency['address2']) { ?>
                        <span class="address2"><?php echo stripslashes($_properties_agency['address2']); ?></span>
                        <br />
                    <?php } ?>
                    <?php if ($_properties_agency['city']) { ?><span class="city"><?php echo stripslashes($_properties_agency['city']); ?></span>, <?php } ?><?php if ($_properties_agency['state']) { ?><span class="state"><?php echo stripslashes($_properties_agency['state']); ?></span> <?php } ?><?php if ($_properties_agency['zip']) { ?><span class="zip"><?php echo stripslashes($_properties_agency['zip']); ?></span><?php } ?>
                </div>
                <?php if (($_properties_agency['email']) || ($_properties_agency['email_lead'])) { ?>
                    <div class="email">
                        <?php if ($_properties_agency['email_lead']) { ?>
                            <a href="mailto:<?php echo stripslashes($_properties_agency['email_lead']); ?>"><?php echo stripslashes($_properties_agency['email_lead']); ?></a>
                        <?php } else { ?>
                            <a href="mailto:<?php echo stripslashes($_properties_agency['email']); ?>"><?php echo stripslashes($_properties_agency['email']); ?></a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php if ($_properties_agent['id']) { ?>
        <div class="item agent col-1-2">
            <h4>Agent</h4>
            <div class="clearfix">
                <?php if ($_properties_agent['image']) { ?>
                    <img src="<?php echo $_uccms_properties->imageBridgeOut($_properties_agent['image'], 'agents'); ?>" alt="<?php echo stripslashes($_properties_agent['name']); ?>" class="main" />
                <?php } ?>
                <div class="name">
                    <?php if ($_properties_agent['name']) { ?><?php echo stripslashes($_properties_agent['name']); ?> <?php } ?> <span class="edit">(<a href="./agent/">edit</a>)</span>
                </div>
                <div class="address">
                    <?php if ($_properties_agent['address1']) { ?>
                        <span class="address1"><?php echo stripslashes($_properties_agent['address1']); ?></span>
                        <br />
                    <?php } ?>
                    <?php if ($_properties_agent['address2']) { ?>
                        <span class="address2"><?php echo stripslashes($_properties_agent['address2']); ?></span>
                        <br />
                    <?php } ?>
                    <?php if ($_properties_agent['city']) { ?><span class="city"><?php echo stripslashes($_properties_agent['city']); ?></span>, <?php } ?><?php if ($_properties_agent['state']) { ?><span class="state"><?php echo stripslashes($_properties_agent['state']); ?></span> <?php } ?><?php if ($_properties_agent['zip']) { ?><span class="zip"><?php echo stripslashes($_properties_agent['zip']); ?></span><?php } ?>
                </div>
                <?php if (($_properties_agent['email']) || ($_properties_agent['email_lead'])) { ?>
                    <div class="email">
                        <?php if ($_properties_agent['email_lead']) { ?>
                            <a href="mailto:<?php echo stripslashes($_properties_agent['email_lead']); ?>"><?php echo stripslashes($_properties_agent['email_lead']); ?></a>
                        <?php } else { ?>
                            <a href="mailto:<?php echo stripslashes($_properties_agent['email']); ?>"><?php echo stripslashes($_properties_agent['email']); ?></a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

</div>