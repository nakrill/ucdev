<?php

$out = array();

// PROPERTY ID
$prop_id = (int)$_REQUEST['property_id'];

// HAVE ID
if ($prop_id) {

    // MODULE CLASS
    $_uccms_properties = new uccms_Properties;

    // GET PROPERTY
    $prop = $_uccms_properties->getProperty($prop_id, true, array('agent'));

    // PROPERTY FOUND
    if ($prop['id']) {

        // CONTACT FORM ENABLED
        if (($_uccms_properties->getSetting('listing_contact_enabled')) && (($prop['agent']['email']) || ($prop['agent']['email_lead']) || ($prop['agency']['email']) || ($prop['agency']['email_lead'])) && (count($_uccms_properties->listingContactFields()) > 0)) {

            $missing = array();
            $efields = array();
            $jfields = array();

            // LOOP THROUGH FIELDS
            foreach ($_uccms_properties->listingContactFields() as $field_id => $field) {
                $efields[] = $field['title']. ': ' .$_REQUEST['field'][$field_id];
                $jfields[$field['title']] = $_REQUEST['field'][$field_id];
                if ($field['required']) {
                    if (!$_REQUEST['field'][$field_id]) {
                        $missing[] = $field['title'];
                    }
                }
            }

            // GET reCAPTCHA SETTINGS
            $recaptcha_sitekey      = $_uccms_properties->getSetting('recaptcha_sitekey');
            $recaptcha_secretkey    = $_uccms_properties->getSetting('recaptcha_secretkey');

            // reCAPTCHA ENABLED
            if (($recaptcha_sitekey) && ($recaptcha_secretkey)) {
                $response = json_decode(BigTree::cURL('https://www.google.com/recaptcha/api/siteverify?secret=' .$recaptcha_secretkey. '&response=' .urlencode($_REQUEST['g-recaptcha-response']). '&remoteip=' .$_SERVER['REMOTE_ADDR']));
                if (!$response->success) {
                    $missing[] = 'Human verification';
                }
            }

            // HAVE MISSING
            if (count($missing) > 0) {
                $out['err'] = 'Required fields are missing.';
                $out['missing_fields'] = $missing;

            // GOOD TO GO
            } else {

                if ($_uccms_properties->getSetting('listing_contact_success')) {
                    $out['success'] = stripslashes($_uccms_properties->getSetting('listing_contact_success'));
                } else {
                    $out['success'] = 'Property contacted!';
                }

                ##########################
                # EMAIL
                ##########################

                // GET EMAIL RELATED SETTINGS
                //$esitesetta = $_uccms_properties->getSettings(array('email_from_name','email_from_email','email_submission_copy'));

                // EMAIL SETTINGS
                $esettings = array(
                    'custom_template'   => SERVER_ROOT. 'extensions/' .$_uccms_properties->Extension. '/templates/email/property/contact.html',
                    'subject'           => 'Property Listing Contact',
                    'property_id'       => $prop['id']
                );

                //if ($esitesetta['email_from_name']) $esettings['from_name'] = $esitesetta['email_from_name'];
                //if ($esitesetta['email_from_email']) $esettings['from_email'] = $esitesetta['email_from_email'];

                // EMAIL VARIABLES
                $evars = array(
                    'heading'               => 'Property Listing Contact',
                    'date'                  => date('n/j/Y'),
                    'time'                  => date('g:i A T'),
                    'property_id'           => $prop['id'],
                    'page_url'              => '',
                    'fields'                => implode('<br />', $efields)
                );

                if ($prop['title']) {
                    $evars['property_name'] = stripslashes($prop['title']);
                } else if ($prop['address1']) {
                    $evars['property_name'] = stripslashes($prop['address1']);
                }

                // WHO TO SEND TO
                if ($prop['agent']['email_lead']) {
                    $esettings['to_email'] = stripslashes($prop['agent']['email_lead']);
                } elseif ($prop['agent']['email']) {
                    $esettings['to_email'] = stripslashes($prop['agent']['email']);
                } elseif ($prop['agency']['email_lead']) {
                    $esettings['to_email'] = stripslashes($prop['agency']['email_lead']);
                } else {
                    $esettings['to_email'] = stripslashes($prop['agency']['email']);
                }

                // SEND EMAIL
                $result = $_uccms['_account']->sendEmail($esettings, $evars);

                //$out['email_result'] = $result;

                ##########################

                // RECORD EMAIL
                $email_log_id = $_uccms_properties->property_logEmail($prop['id'], $jfields);

                // LOG EMAIL STAT
                $_uccms_properties->property_logStat($prop['id'], 'email', $email_log_id);

            }

        // CONTACT FORM NOT ENABLED
        } else {
            $out['err'] = 'Contact form not available.';
        }

    // BUSINESS NOT FOUND
    } else {
        $out['err'] = 'Property not found.';
    }

} else {
    $out['err'] = 'Property ID not specified.';
}

echo json_encode($out);

?>