<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    ?>

    <div id="block_ecommerce" class="block">
        <h2><a href="../<?php echo $extension; ?>*ecommerce/">E-Commerce</a></h2>
        <div id="orders" class="table">
            <summary>
                <h2>Latest Orders</h2>
                <a class="add_resource add" href="../<?php echo $extension; ?>*ecommerce/orders/">
                    View All
                </a>
            </summary>
            <header style="clear: both;">
                <?php foreach ($_uccms_ecomm->adminOQListColumns('order') as $col_class => $col_title) { ?>
                    <span class="<?=$col_class?>"><?=$col_title?></span>
                <?php } ?>
            </header>
            <ul id="results" class="items">
                <?php
                $_REQUEST['base_url'] = '../' .$extension. '*ecommerce/orders';
                $_REQUEST['limit'] = 5;
                include($extension_dir. '/ajax/admin/orders/get-page.php');
                ?>
            </ul>
        </div>
    </div>

    <?php

}

unset($_uccms_ecomm);

?>