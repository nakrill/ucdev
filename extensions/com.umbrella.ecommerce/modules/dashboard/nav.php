<?php

$extension_nav =
["link" => "com.umbrella.ecommerce*ecommerce", "title" => "E-Commerce", "access" => 1, "children" => [
    ["link" => ".", "title" => "Dashboard", "access" => 1],
    ["link" => "categories", "title" => "Categories", "access" => 1],
    ["link" => "items", "title" => "Items", "access" => 1],
    ["link" => "inventory", "title" => "Inventory", "access" => 1],
    ["link" => "quotes", "title" => "Quotes", "access" => 1],
    ["link" => "orders", "title" => "Orders", "access" => 1],
    ["link" => "customers", "title" => "Customers", "access" => 1],
    ["link" => "settings", "title" => "Settings", "access" => 1],
]];

array_push($nav, $extension_nav);

?>