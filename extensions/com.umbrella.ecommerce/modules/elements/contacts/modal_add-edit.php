<script type="text/javascript">

    var modalLink_editContact = {};

    $(function() {

        $('#modal_editContact').appendTo('#page');

        // ADD / EDIT LINK CLICK
        $('#page').on('click', '.modalLink-editContact', function(e) {
            e.preventDefault();
            modalLink_editContact = $(this);
            var ci = $(this).data('contactid');
            var cd = $(this).data('contactdata');
            if (ci) {
                $('#modal_editContact .modal-title').text('Edit Contact');
            } else {
                $('#modal_editContact .modal-title').text('Create Contact');
            }
            modalEditContact({
                id: ci,
                data: (cd ? JSON.parse(window.atob(cd)) : '{}'),
            });
        });

        // SAVE
        $('body').on('click', '#modal_editContact .btn.save', function(e) {
            $('#modal_editContact form .alert').hide();
            $('#modal_editContact form input').removeAttr('disabled');
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/customers/contacts/process/', $('#modal_editContact form').serialize(), function(data) {
                if (data.success) {
                    if (typeof modalLink_editContact.removeData == 'function') {
                        modalLink_editContact.removeData('contactid').attr('data-contactid', data.id);
                    }
                    $('#modal_editContact .modal-title').text('Edit Contact');
                    $('#modal_editContact form .alert strong').html('Contact saved!');
                    $('#modal_editContact form .alert').removeClass('alert-danger').addClass('alert-success').show();
                } else {
                    $('#modal_editContact form .alert strong').html((data.error ? data.error : 'Failed to create contact.'));
                    $('#modal_editContact form .alert').removeClass('alert-success').addClass('alert-danger').show();
                }
                if (typeof modaleditContact_saveCallback == 'function') {
                    modaleditContact_saveCallback(data);
                }
            }, 'json');
        });

    });

    if (typeof modalEditContact !== 'function') {

        function modalEditContact(params, options, callback) {
            if (typeof params != 'object') params = {};
            $('#modal_editContact').modal('show');
            $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/customers/contacts/edit/', {
                id: params.id,
                customer_id: params.data.customer_id,
                data: params.data
            }, function(data) {
                $('#modal_editContact .modal-body .data').html(data);
                if (typeof callback == 'function') {
                    callback(data);
                }
            }, 'html');
        }

    }

</script>

<div class="modal fade" id="modal_editContact" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo ($el_settings['contact']['id'] ? 'Edit' : 'Create'); ?> Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="save btn btn-primary" <? /*disabled="disabled"*/ ?>>Save</button>
            </div>
        </div>
    </div>
</div>