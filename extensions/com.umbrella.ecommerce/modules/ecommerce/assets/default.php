<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Assets', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #assets .heading h3 {
        margin: 0px;
    }
    #assets .heading a {
        margin: 0px;
    }

    #assets .summary {
        margin: 0;
        padding: 0;
    }
    #assets .summary .filter .search {
        float: left;
    }
    #assets .summary .filter .search input[type="text"] {
        max-width: 300px;
    }
    #assets .summary .filter .paging {
        float: right;
    }
    #assets .summary .filter .assign {
        float: right;
    }

    #assets .data-loading {
        display: none;
    }

    #assets .data .alert {
        margin-top: 30px;
    }

    #modal-assignAsset .modal-header {
        padding: .5rem 1rem;
    }
    .modal-header .close {
        padding-top: 1.2rem;
    }
    #modal-assignAsset .modal-title img {
        display: inline-block;
        object-fit: cover;
        object-position: center;
        height: 40px;
        width: 40px;
        margin-right: 10px;
    }
    #modal-assignAsset .modal-title span {
        vertical-align: middle;
    }
    #modal-assignAsset .modal-title .item-title {
        margin-right: 5px;
    }
    #modal-assignAsset .modal-title .title {
        opacity: .7;
    }

    .modal-dialog {
        max-width: 50%;
    }

    @media (max-width: 900px) {
        .modal-dialog {
            max-width: 70%;
        }
    }

</style>




<script type="text/javascript">

    $(function() {

        // FILTER SUBMIT
        $('#form_filter').submit(function(e) {
            e.preventDefault();
            var params = $(this).serialize();
            params.page = 0;
            this_assetList(params);
            return false;
        });

        // FILTER - KEYWORD
        $('#assets .summary .filter input[name="q"]').keyup(function(e) {
            delay(function() {
                this_assetList($('#form_filter').serialize());
            }, 400);
        });

        <? /*
        // STATUS CLICK
        $('#assets .summary .filter .status a').click(function(e) {
            $('#assets .summary .filter .status a').removeClass('active');
            $(this).addClass('active');
            $('#assets input[name="status"]').val($(this).data('id'));
            this_assetList($('#form_filter').serialize());
        });
        */ ?>

        // PAGING CLICK
        $('#assets .summary .view_paging').on('click', 'a', function(e) {
            $('#form_filter input[name="page"]').val($(this).attr('href').replace('#', ''));
            this_assetList($('#form_filter').serialize());
        });

        // ADD / ASSIGN
        $('#assets .summary .assign').click(function(e) {
            assignAsset();
        });

        // ON MODAL CLOSE
        $('#modal-assignAsset').on('hidden.bs.modal', function(e) {
            this_assetList($('#form_filter').serialize());
            assetsModalChanged = false;
            $('#modal-assignAsset .modal-data').hide().html('');
        });

    });

    // GET ITEM LIST
    function this_assetList(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/assets/list/', params, function(data) {
            if (typeof callback == 'function') callback(data);
            $('#assets > .data').html(data);
        }, 'html');
    }

    // ASSIGN ASSET
    function assignAsset(params, options, callback) {
        $('#modal-assignAsset .modal-data').hide().html('');
        $('#modal-assignAsset .modal-loading').show();
        $('#modal-assignAsset').modal('show', {
            focus: false
        });
        if (typeof params != 'object') params = {};
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/assets/edit/', {
            id: params.id
        }, function(data) {
            $('#modal-assignAsset .modal-loading').hide();
            $('#modal-assignAsset .modal-data').html(data).show();
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<div id="assets">

    <form id="form_filter" action="" method="get">
    <input type="hidden" name="status" value="<?php echo $_REQUEST['status']; ?>" />
    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />

    <div class="table">

        <div class="summary clearfix">

            <div class="filter">

                <div class="control search">
                    <input type="text" name="q" value="" class="form-control" placeholder="Search" />
                </div>

                <? /*
                <div class="control status links">
                    <a href="#" data-id="" class="active">All</a>
                    <?php foreach ($_uccms_staff->jobStatuses() as $status_id => $status) { ?>
                        <a href="#" data-id="<?php echo $status_id; ?>"><?php echo $status['title']; ?></a>
                    <?php } ?>
                </div>
                */ ?>

            </div>

            <div class="paging ajax">
                <nav class="view_paging"></nav>
            </div>

            <a href="#" class="assign btn btn-primary float-right"><i class="fas fa-user-plus"></i> Assign</a>

        </div>

    </div>

    </form>

    <div class="data-loading">
        Loading..
    </div>
    <div class="data">
        <?php include(EXTENSION_ROOT. 'ajax/admin/assets/list.php'); ?>
    </div>

</div>

<div class="modal fade" id="modal-assignAsset" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Assign Asset
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-loading">
                    Loading..
                </div>
                <div class="modal-data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="save btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>