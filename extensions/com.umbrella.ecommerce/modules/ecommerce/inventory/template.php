<?php

$type = $_REQUEST['id'];

$out = [];

// HEADING
$out[] = [
    'Location ID',
    'SKU',
    'Available',
    'Unavailable',
    'Backordered',
    'Overwrite / Adjust'
];

// EXAMPLE DATA
if ($type == 'example') {

    $out[] = [
        0,
        'ts-s-r',
        42,
        0,
        0,
        'overwrite'
    ];

    $out[] = [
        0,
        'ts-s-g',
        0,
        0,
        18,
        'overwrite'
    ];

    $out[] = [
        1,
        'ts-s-b',
        15,
        5,
        0,
        'overwrite'
    ];

    $out[] = [
        1,
        'ts-m-r',
        -4,
        0,
        1,
        'adjust'
    ];

    $out[] = [
        0,
        'ts-m-g',
        30,
        0,
        0,
        'adjust'
    ];

// DATA FROM DB
} else {

    $out[] = 'Populate from DB.';

}

header('Content-type: text/csv');
header('Content-Disposition: attachment; filename=Inventory-Import-Template.csv');
header('Pragma: no-cache');
header('Expires: 0');

echo array_to_csv($out);

exit;

?>