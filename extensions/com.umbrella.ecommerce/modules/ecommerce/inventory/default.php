<?php

// INVENTORY NOT ENABLED
if (!$_uccms_ecomm->getSetting('inventory_track')) {
    $admin->growl('Error', 'Inventory not enabled.');
    BigTree::redirect(MODULE_ROOT);
}

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Inventory', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET CATEGORIES
$categories = $_uccms_ecomm->subCategories(0);

?>

<style type="text/css">

    /*
    #form_filter select, #form_filter input, #form_filter button {
        height: 30px;
        font-size: .9em;
    }
    #form_filter select {
        padding-top: 0px;
        padding-bottom: 0px;
        line-height: 0;
        color: #666;
    }
    #form_filter button {
        line-height: 0;
    }
    */

    #inventory .heading a {
        margin: -10px 0 10px;
    }

    #inventory .data-loading {
        display: none;
    }

    #inventory .data .alert {
        margin-top: 30px;
    }

    #inventory .data .paging {
        margin: 15px 0;
    }
    #inventory .data .paging h6 {
        float: left;
        margin-top: 16px;
    }
    #inventory .data .paging nav {
        float: right;
    }

    .modal-dialog {
        max-width: 50%;
    }


    #modal_import-inventory .modal-footer .templates {
        display: flex;
        width: 100%;
        justify-content: flex-start;
    }
    #modal_import-inventory .modal-footer .templates a {
        color: #43b4ae;
    }


    @media (max-width: 900px) {
        .modal-dialog {
            max-width: 70%;
        }
    }

</style>

<script type="text/javascript">

    $(function() {

        // FILTER SUBMIT
        $('#form_filter').submit(function(e) {
            e.preventDefault();
            var params = $(this).serialize();
            params.page = 0;
            this_itemList(params);
            return false;
        });

        // ON IMPORT MODAL OPEN
        $('#modal_import-inventory').on('shown.bs.modal', function(e) {
            $('#modal_import-inventory .modal-data').hide().html('');
            $('#modal_import-inventory .modal-loading').show();
            $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/import/', {
                id: $(e.relatedTarget).closest('li').data('id')
            }, function(data) {
                $('#modal_import-inventory .modal-loading').hide();
                $('#modal_import-inventory .modal-data').html(data).show();
            }, 'html');
        });

        // ON IMPORT MODAL CLOSE
        $('#modal_import-inventory').on('hidden.bs.modal', function(e) {
            this_itemList($('#form_filter').serialize());
            $('#inventory-import .alert').hide().html('').removeClass('alert-success alert-danger');
            $('#form_import-inventory').hide();
            $('#inventory-import .processing').hide();
            $('#inventory-import .log').html('').hide();
            $('#modal_import-inventory .btn.save').attr('disabled', 'disabled');
        });

    });

    // GET ITEM LIST
    function this_itemList(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/item-list/', params, function(data) {
            if (typeof callback == 'function') callback(data);
            $('#inventory > .data').html(data);
        }, 'html');
    }

</script>

<div id="inventory">

    <div class="heading clearfix">
        <h3 class="breadcrumbs float-left">Inventory</h3>
        <a href="#" class="btn btn-secondary float-right" data-toggle="modal" data-target="#modal_import-inventory"><i class="fas fa-cloud-upload-alt"></i> Import</a>
    </div>

    <form id="form_filter" action="" method="get">
    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />

    <div class="filter card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <select name="category_id" class="form-control custom_control">
                        <option value="">All Categories</option>
                        <?php foreach ((array)$categories as $category) { ?>
                            <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $_REQUEST['category_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($category['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col">
                    <input type="text" name="title" class="form-control" placeholder="Item name">
                </div>
                <div class="col">
                    <input type="text" name="id" class="form-control" placeholder="ID">
                </div>
                <div class="col">
                    <input type="text" name="sku" class="form-control" placeholder="SKU">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-secondary"><i class="fas fa-search"></i> Filter</button>
                </div>
            </div>
        </div>
    </div>

    </form>

    <div class="data-loading">
        Loading..
    </div>
    <div class="data">
        <?php include(EXTENSION_ROOT. 'ajax/admin/inventory/item-list.php'); ?>
    </div>

    <div class="modal fade" id="modal_import-inventory" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Import Inventory
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-loading">
                        Loading..
                    </div>
                    <div class="modal-data"></div>
                </div>
                <div class="modal-footer">
                    <div class="templates">Templates:&nbsp;&nbsp;<a href="./template/?id=example&no_hf=true" target="_blank" class="example">Example</a>&nbsp;|&nbsp;<a href="./template/?id=items&no_hf=true" target="_blank" class="items">My Items</a></div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="save btn btn-primary" disabled="disabled">Process</button>
                </div>
            </div>
        </div>
    </div>

</div>