<?php


/*
// PAYMENT - AUTHORIZE
$data = array(
    'amount'    => 1.00,
    'tax'       => 0.00,
    'customer' => array(
        'id'            => 5,
        'name'          => 'Testing Tester',
        'email'         => 'test@there.com',
        'phone'         => '555-555-5555',
        'address'       => array(
            'street'    => '123 Main St',
            'street2'   => '',
            'city'      => 'Kitty Hawk',
            'state'     => 'NC',
            'zip'       => '92054',
            'country'   => 'US'
        ),
        'description'   => ''
    ),
    'card' => array(
        //'id'          => 15,
        'name'          => 'Testing Tester',
        'number'        => '4242424242424242',
        'expiration'    => '05/2017',
        'zip'           => '92054',
        'cvv'           => '123'
    ),
    'note'  => 'Order ID: 1'
);
$result = $_uccms['_account']->payment_authorize($data);
echo print_r($result);
*/

/*
// PAYMENT - CAPTURE
$transaction_log_id = 2;
$result = $_uccms['_account']->payment_capture($transaction_log_id);
echo print_r($result);
*/


// PAYMENT - CHARGE
$data = array(
    'amount'    => 8.00,
    'tax'       => 2.00,
    'customer' => array(
        'id'            => 5,
        'name'          => 'Testing Tester',
        'email'         => 'test@there.com',
        'phone'         => '555-555-5555',
        'address'       => array(
            'street'    => '123 Main St',
            'street2'   => '',
            'city'      => 'Kitty Hawk',
            'state'     => 'NC',
            'zip'       => '92054',
            'country'   => 'US'
        ),
        'description'   => ''
    ),
    'card' => array(
        //'id'            => 15,
        'name'          => 'Testing Tester',
        'number'        => '4111111111111111',
        'expiration'    => '05/2017',
        'zip'           => '92054',
        'cvv'           => '123'
    ),
    'note'  => 'Order ID: 7'
);
$result = $_uccms['_account']->payment_charge($data);
echo print_r($result);


/*
// PAYMENT - REFUND
$transaction_log_id = 3;
$amount = false;
$result = $_uccms['_account']->payment_refund($transaction_log_id, $amount);
echo print_r($result);
*/

/*
// PAYMENT - VOID
$transaction_log_id = 3;
$amount = false;
$result = $_uccms['_account']->payment_void($transaction_log_id, $amount);
echo print_r($result);
*/

/*
// CREATE CUSTOMER (IF DOESN'T EXIST) & PAYMENT PROFILE
$data = array(
    'customer' => array(
        'description'   => '',
        'email'         => 'test@there.com'
    ),
    'card' => array(
        'name'          => 'Testing Tester',
        'number'        => '4242424242424242',
        'expiration'    => '05/20/2017',
        'zip'           => '92054',
        'cvv'           => '123'
    )
);
$result = $_uccms['_account']->pp_createProfile($data);
echo print_r($result);
*/

/*
// UPDATE CUSTOMER
$id = 8;
$data = array(
    'customer' => array(
        'description'   => 'Website Customer',
        'email'         => 'test@there.com'
    )
);
$result = $_uccms['_account']->pp_updateCustomer($id, $data);
echo print_r($result);
*/

/*
// DELETE CUSTOMER
$id = 4;
$result = $_uccms['_account']->pp_deleteCustomer($id);
echo print_r($result);
*/

/*
// UPDATE PAYMENT PROFILE
$id = 3;
$data = array(
    'card' => array(
        'name'          => 'Testing Tester',
        'expiration'    => '05/20/2018',
        'zip'           => '92054'
    )
);
$result = $_uccms['_account']->pp_updateProfile($id, $data);
echo print_r($result);
*/

/*
// DELETE PAYMENT PROFILE
$id = 4;
$result = $_uccms['_account']->pp_deleteProfile($id);
echo print_r($result);
*/

?>