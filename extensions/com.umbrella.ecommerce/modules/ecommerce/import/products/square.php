<?php

// NO MAPPING YET
if (!$cfields) {

    // LOOP THROUGH HEADING COLUMNS
    foreach ($heading as $name => $key) {

        // VARIANT
        if (substr($name, 0, 8) == 'Variant ') {
            list($id, $field) = explode('-', $name);
            $id = (int)trim(str_replace('Variant ', '', $id));
            $cfields['variants'][$id][trim($field)] = $key;
            unset($heading[$key]);

        // MODIFIER
        } else if (substr($name, 0, 15) == 'Modifier Set - ') {
            list($null, $field) = explode('-', $name);
            $cfields['modifiers'][trim($field)] = $key;
            unset($heading[$key]);

        // TAX
        } else if (substr($name, 0, 6) == 'Tax - ') {
            list($null, $field) = explode('-', $name);
            list($field, $percent) = explode('(', $field);
            $cfields['tax'][trim($field)]['key'] = $key;
            $cfields['tax'][trim($field)]['percent'] = str_replace('%)', '', $percent);
            unset($heading[$key]);

        // NORMAL
        } else {
            $cfields[$name] = $key;
        }

    }

}

//echo print_r($cfields);
//exit;

$map = array(
    'active'                        => 1,
    'sku'                           => '',
    'type'                          => 0,
    'title'                         => $data[$cfields['Name']],
    'description_short'             => '',
    'description'                   => $data[$cfields['Description']],
    'manufacturer_id'               => '',
    'price'                         => '',
    'price_cost'                    => '',
    'price_msrp'                    => '',
    'price_sale'                    => '',
    'hide_price'                    => '',
    'after_tax'                     => '',
    'shipping_price'                => '',
    'shipping_cost'                 => '',
    'shipping_free'                 => '',
    'weight'                        => '',
    'dimensions_length'             => '',
    'dimensions_width'              => '',
    'dimensions_height'             => '',
    'tax_group'                     => '',
    'discount_quantity_type'        => '',
    'discount_quantity_amount'      => '',
    'discount_quantity_quantity'    => '',
    'discount_total_type'           => '',
    'discount_total_amount'         => '',
    'discount_total_total'          => '',
    'meta_title'                    => '',
    'meta_description'              => '',
    'meta_keywords'                 => '',
    'search_keywords'               => '',
    'no_search'                     => '',
    'no_purchase'                   => '',
    'featured'                      => '',
    'not_recent'                    => ''
);

// category = $data[3]

// tax

?>