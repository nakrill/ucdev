<?php

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.ecommerce/modules/ecommerce/import/inventory/manual.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INIT EVENTS CLASS
$_uccms_ecomm = new uccms_Ecommerce;

###########################################################


// DATA SOURCE
$source = ($importConfig['file'] ? $importConfig['file'] : __DIR__. '/inventory-import-template.csv');

// HAVE DATA FILE
if (file_exists($source)) {

    $row = 0;
    $headng = array();

    // GET DATA
    $data = array_map('str_getcsv', file($source));

    //print_r($data);
    //exit;

    $num_skus = count($data);
    if ($num_skus) $num_skus--;

    this_output($num_skus. ' SKU' .($num_skus != 1 ? 's' : ''). ' to process..');
    this_output();

    // LOOP
    foreach ($data as $i => $row) {

        if ($i == 0) continue;

        // CLEAN UP
        $lid = trim((int)$row[0]);
        $sku = trim(sqlescape($row[1]));
        $num_available = trim(round((float)$row[2]));
        $num_unavailable = trim(round((float)$row[3]));
        $num_backordered = trim(round((float)$row[4]));
        $oa  = trim($row[5]);

        $itema  = [];
        $vara   = [];

        // GET ITEM BY SKU
        $items_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`sku`='" .$sku. "')";
        $items_q = sqlquery($items_query);
        while ($item = sqlfetch($items_q)) {

            $itema[$item['id']] = $item['id'];

            // GET VARIANTS BY ITEM
            $vars_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_variants']. "` WHERE (`item_id`=" .$item['id']. ") AND (`sku`='')";
            $vars_q = sqlquery($vars_query);
            while ($var = sqlfetch($vars_q)) {
                $vara[$var['id']] = $var['id'];
                unset($itema[$var['item_id']]);
            }

        }

        // GET VARIANTS BY SKU
        $vars_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_variants']. "` WHERE (`sku`='" .$sku. "')";
        $vars_q = sqlquery($vars_query);
        while ($var = sqlfetch($vars_q)) {
            $vara[$var['id']] = $var['item_id'];
            unset($itema[$var['item_id']]);
        }

        $ici_changes = [
            'available'     => $num_available,
            'unavailable'   => $num_unavailable,
            'backordered'   => $num_backordered
        ];

        $ici_settings = [
            'overwrite' => ($oa == 'overwrite' ? true : false),
            'log' => [
                'source' => 'import'
            ]
        ];

        // ADJUST ITEMS
        foreach ($itema as $item_id) {

            // CHANGE INVENTORY
            $result = $_uccms_ecomm->item_changeInventory($item_id, $lid, [], $ici_changes, $ici_settings);

            if ($result['success']) {
                this_output('SKU: ' .$sku. ' (Item ID: ' .$item_id. ') - Success! Available: ' .number_format($result['inventory']['available']). ' Unavailable: ' .number_format($result['inventory']['unavailable']). ' Backordered: ' .number_format($result['inventory']['backordered']));
            } else {
                this_output('SKU: ' .$sku. ' (Item ID: ' .$item_id. ') - Failed!');
            }

        }

        // ADJUST VARIANTS
        foreach ($vara as $var_id => $var_item_id) {

            $ici_settings['variant_id'] = $var_id;

            // CHANGE INVENTORY
            $result = $_uccms_ecomm->item_changeInventory($var_item_id, $lid, [], $ici_changes, $ici_settings);

            if ($result['success']) {
                this_output('SKU: ' .$sku. ' (Variant ID: ' .$var_id. ') - Success! Available: ' .number_format($result['inventory']['available']). ' Unavailable: ' .number_format($result['inventory']['unavailable']). ' Backordered: ' .number_format($result['inventory']['backordered']));
            } else {
                this_output('SKU: ' .$sku. ' (Variant ID: ' .$var_id. ') - Failed!');
            }

        }

    }

// NO DATA file
} else {
    this_output('No data file.');
}

this_output();
this_output('All done!');
this_output();


###########################################################


function this_output($data='') {
    global $cli, $importConfig;
    if (!$importConfig['silent']) {
        if (is_array($data)) {
            print_r($data);
        } else {
            echo $data;
        }
        echo ($cli ? "\n" : '<br />');
    }
    if ($importConfig['logFile']) {
        if (is_array($data)) {
            file_put_contents($importConfig['logFile'], print_r($data, true). "\n", FILE_APPEND);
        } else {
            file_put_contents($importConfig['logFile'], $data. "\n", FILE_APPEND);
        }
    }
}

?>