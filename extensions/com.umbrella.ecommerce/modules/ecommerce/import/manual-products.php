<?php

$source = 'big-commerce';

###########################################################

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.ecommerce/modules/ecommerce/import/manual-products.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// INIT EVENTS CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$row = 0;
$heading = array();

// GET DATA
if (($handle = fopen(dirname(__FILE__). '/' .$source. '.csv', 'r')) !== FALSE) {

    $admin = new BigTreeAdmin;

    $cata = array();
    $taga = array();
    $locationa = array();

    // LOOP THROUGH DATA
    while (($data = fgetcsv($handle, 2000, ',')) !== FALSE) {

        // IS HEADINGS - SKIP
        if ($row == 0) {
            $heading = array_flip($data);
            $row++;
            continue;
        }

        // HAVE DATA
        if (count($data) > 0) {

            // SOURCE MAPPING
            include('./products/' .$source. '.php');

            $item_images = $map['images'];
            unset($map['images']);

            // CLEAN UP
            $item_title = trim(sqlescape($map['title']));

            // CHECK FOR EXISTING ITEM
            $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`title`='" .$item_title. "') ORDER BY `id` DESC LIMIT 1";
            $item_q = sqlquery($item_query);
            $item = sqlfetch($item_q);

            /*
            // REMOVE EMPTY COLUMNS
            foreach ($map as $col_name => $col_val) {
                if ($col_val === 0) {
                    continue;
                } else if ($col_val) {
                    continue;
                } else {
                    unset($map[$col_name]);
                }
            }
            */

            // HAVE ID - IS UPDATING
            if ($item['id']) {

                // DB QUERY
                $query = "UPDATE `" .$_uccms_ecomm->tables['items']. "` SET " .uccms_createSet($map). ", `updated_dt`=NOW() WHERE (`id`=" .$item['id']. ")";

            // NO ID - IS NEW
            } else {

                // SLUG
                $map['slug'] = $_uccms_ecomm->makeRewrite($item_title);

                // DB QUERY
                $query = "INSERT INTO `" .$_uccms_ecomm->tables['items']. "` SET " .uccms_createSet($map). ", `updated_dt`=NOW()";

            }

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {

                // UPDATED
                if ($item['id']) {
                    echo $item_title. ' - Updated';

                // ADDED
                } else {

                    echo $item_title. ' - Added';

                    // NEW EVENT ID
                    $item['id'] = sqlid();

                }

                /*
                ###############################
                # CATEGORIES
                ###############################

                $create_relation = false;
                unset($category_del);

                // GET CURRENT RELATIONS
                $rel_query = "SELECT `category_id` FROM `" .$_uccms_ecomm->tables['item_categories']. "` WHERE (`item_id`=" .$item['id']. ")";
                $rel_q = sqlquery($rel_query);
                while ($rel = sqlfetch($rel_q)) {
                    $category_del[$rel['category_id']] = $rel['category_id']; // SAVE TO DELETE ARRAY
                }

                // CATEGORIES
                $categorya = explode('|', $data[15]);

                // LOOP THROUGH CATEGORIES
                foreach ($categorya as $category_title) {

                    // CLEAN UP
                    $category_title = trim(sqlescape($category_title));

                    // IS A VALUE
                    if ($category_title) {

                        // NOT IN CATEGORY ARRAY
                        if (!$cata[$category_title]) {

                            // LOOK FOR MATCH
                            $category_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`title`='" .$category_title. "')";
                            $category_q = sqlquery($category_query);
                            $category = sqlfetch($category_q);

                            // ADD TO CATEGORY ARRAY
                            $cata[$category_title] = $category['id'];

                        }

                        // CATEGORY FOUND
                        if ($cata[$category_title]) {

                            // RELATION ALREADY EXISTS
                            if ($category_del[$cata[$category_title]]) {
                                unset($category_del[$cata[$category_title]]);

                            // CREATE RELATION
                            } else {
                                $create_relation = true;
                            }

                        // NO CATEGORY FOUND
                        } else {

                            // CREATE NEW CATEGORY
                            $ncat_query = "INSERT INTO `" .$_uccms_ecomm->tables['categories']. "` SET `title`='" .$category_title. "', `slug`='" .$_uccms_ecomm->makeRewrite($category_title). "'";
                            $ncat_q = sqlquery($ncat_query);

                            // NEW CATEGORY ID
                            $cata[$category_title] = sqlid();

                            $create_relation = true;

                        }

                        // CREATE RELATION
                        if (($create_relation) && ($cata[$category_title])) {

                            $rel_query = "INSERT INTO `" .$_uccms_ecomm->tables['event_categories']. "` SET `category_id`=" .$cata[$category_title]. ", `event_id`=" .$item['id'];
                            sqlquery($rel_query);

                        }

                        // HAVE ANY TO DELETE
                        if (count($category_del) > 0) {
                            foreach ($category_del as $category_id) {
                                $del_query = "DELETE FROM `" .$_uccms_ecomm->tables['event_categories']. "` WHERE (`event_id`=" .$item['id']. ") AND (`category_id`=" .$category_id. ")";
                                sqlquery($del_query);
                            }
                        }

                        unset($category);

                    }

                }
                */

                ###############################
                # IMAGES
                ###############################

                // HAVE IMAGE(S)
                if ($item_images) {

                    // NOT ARRAY
                    if (!is_array($item_images)) {
                        $item_images = explode(',', $item_images);
                    }

                    // LOOP
                    foreach ($item_images as $image) {

                        $image = trim($image);

                        // ADD EXTRA .jpg FOR EMCORSA
                        //$image = $image. '.jpg';

                        // LOCATION OF FILE
                        $file = './images/' .$image;

                        // EXISTS
                        if (file_exists($file)) {

                            // BIGTREE UPLOAD FIELD INFO
                            $field = array(
                                'type'          => 'upload',
                                'title'         => 'Image',
                                'key'           => '',
                                'options'       => array(
                                    'directory' => 'extensions/' .$_uccms_ecomm->Extension. '/files/items',
                                    'image' => true,
                                    'thumbs' => array(
                                        array(
                                            'width'     => '1024', // MATTD: make controlled through settings
                                            'height'    => '1024' // MATTD: make controlled through settings
                                        ),
                                        array(
                                            'prefix'    => 't_',
                                            'width'     => '480', // MATTD: make controlled through settings
                                            'height'    => '480' // MATTD: make controlled through settings
                                        )
                                    ),
                                    'crops' => array()
                                )
                            );

                            $pinfo = BigTree::pathInfo($file);

                            // Emulate a newly uploaded file
                            $field["file_input"] = array("name" => $pinfo["basename"],"tmp_name" => SITE_ROOT."files/".uniqid("temp-").".img","error" => false);
                            BigTree::copyFile($file,$field["file_input"]["tmp_name"]);

                            $file_path = $admin->processImageUpload($field);

                            /*
                            // DIGITAL ASSET MANAGER VARS
                            $field['dam_vars'] = array(
                                'website_extension'         => $_uccms_ecomm->Extension,
                                'website_extension_item'    => 'item',
                                'website_extension_item_id' => $item['id'],
                                'title'                     => $item_title
                            );
                            */

                            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
                            //$file_path = BigTreeAdmin::processField($field);

                            // UPLOAD SUCCESSFUL
                            if ($file) {

                                // DB COLUMNS
                                $image_columns = array(
                                    'item_id'       => $item['id'],
                                    'sort'          => 999,
                                    'image'         => $_uccms_ecomm->imageBridgeIn($file_path),
                                    'caption'       => '',
                                    'updated_by'    => $_uccms_ecomm->adminID()
                                );

                                // INSERT INTO DB
                                $query = "INSERT INTO `" .$_uccms_ecomm->tables['item_images']. "` SET " .$_uccms_ecomm->createSet($image_columns). ", `updated_dt`=NOW()";
                                sqlquery($query);

                            }

                        } else {
                            echo ' (could not find image ' .$file. ')';
                        }

                    }

                }

                ###############################

            // QUERY FAILED
            } else {

                echo $item_title. ' - Failed';

            }

            // CLEAR ARRAYS
            unset($item);
            unset($columns);

        }

        echo ($cli ? "\n" : '<br />');

        $row++;

    }

    // CLOSE DATA
    fclose($handle);

    unset($cata);

}

echo ($cli ? "\n" : '<br /><b>');
echo 'All done.';
echo ($cli ? "\n" : '</b><br />');

?>