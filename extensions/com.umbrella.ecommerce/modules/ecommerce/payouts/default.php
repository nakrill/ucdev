<style type="text/css">

    #form_payouts .search_paging a.button {
        float: right;
        height: auto;
        line-height: 24px;
        font-size: 1em;
        text-transform: uppercase;
    }

    .table .payouts_date {
        padding: 0 0 0 20px;
        text-align: left;
        width: 295px;
    }
    .table .payouts_account {
        padding: 0 0 0 20px;
        text-align: left;
        width: 295px;
    }
    .table .payouts_amount {
        padding: 0 0 0 20px;
        text-align: left;
        width: 130px;
    }
    .table .payouts_status {
        width: 120px;
        padding: 0 0 0 20px;
        text-align: left;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#payouts summary .status select').change(function() {
            $('#form_payouts').submit();
        });

    });

</script>

<form id="form_payouts" method="get">

<div class="search_paging contain">
    <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
    <span class="form_search_icon"></span>
    <a href="./edit/" class="button blue">New Payout</a>
    <nav id="view_paging" class="view_paging" style="display: none;"></nav>
</div>

<div class="table">
    <summary>
        <h2>Payouts</h2>
        <div class="status">
            <select name="status">
                <option value="">All</option>
                <?php foreach ($_uccms_ecomm->payoutStatuses() as $status => $status_title) { ?>
                    <option value="<?php echo $status; ?>" <?php if ($status == $_REQUEST['status']) { ?>selected="selected"<?php } ?>><?php echo $status_title; ?></option>
                <?php } ?>
            </select>
        </div>
    </summary>
    <header>
        <span class="view_column payouts_date"><a name="date" href="ASC" class="sort_column desc">Date <em>▼</em></a></span>
        <span class="view_column payouts_account"><a name="account" href="ASC" class="sort_column">Account <em></em></a></span>
        <span class="view_column payouts_amount"><a name="amount" href="DESC" class="sort_column">Amount <em></em></a></span>
        <span class="view_column payouts_status"><a name="status" href="DESC" class="sort_column">Status <em></em></a></span>
        <span class="payouts_edit">Edit</span>
    </header>
    <ul id="results" class="items">
        <? include(EXTENSION_ROOT. 'ajax/admin/payouts/get-page.php'); ?>
    </ul>
</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/payouts/get-page/?page=1&query=" +escape($("#query").val()));
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/payouts/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&query=" +escape($("#query").val()));
        return false;
    });
</script>