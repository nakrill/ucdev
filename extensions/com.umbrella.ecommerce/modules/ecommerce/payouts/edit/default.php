<?php

// GET PAYOUT INFO
$payout = $_uccms_ecomm->payoutInfo((int)$_REQUEST['id']);

?>

<style type="text/css">

    .uccms_breadcrumbs {
        padding-bottom: 15px;
    }

    #payout #orders {
        margin-bottom: 15px;
        padding: 15px;
        border: 1px solid #eee;
        background-color: #f5f5f5;
    }

</style>

<script type="text/javascript">

    var selectedOrders = [<?php echo implode(',', array_column((array)$payout['orders'], 'order_id')); ?>];

    $(function() {

        <?php if (($payout['build_from']) && ($payout['build_from'] != 'manual-amount')) { ?>

            //$('#payout input[name="payout[amount]"]').prop('readonly', 'readonly');

            getOrders({
                <?php if ($payout['status'] < 5) { ?>
                    account_id: <?php echo (int)$payout['account_id']; ?>,
                    what: '<?php echo $payout['build_from']; ?>',
                    orders: selectedOrders,
                <?php } else { ?>
                    payout_id: <?php echo (int)$payout['id']; ?>,
                <?php } ?>
            }, {}, function(data) {
                $('#payout #orders').html(data).show();
            });

        <?php } ?>

        $('#payout select[name="payout[build_from]"]').change(function() {
            var val = $(this).val();
            if (val == 'manual-amount') {
                $('#payout #orders').hide();
            } else {
                var account_id = $('#payout select[name="payout[account_id]"]').val();
                if (account_id > 0) {
                    getOrders({
                        account_id: account_id,
                        what: val,
                    }, {}, function(data) {
                        $('#payout #orders').html(data).show();
                    });
                } else {
                    $('#payout select[name="payout[account_id]"]').val('0');
                    alert('Please select an account first.');
                }
            }
        });

    });

    // GET ORDERS
    function getOrders(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/payouts/get-orders/', params, function(data) {
            if (typeof callback == 'function') callback(data);
        }, 'html');
    }

</script>

<div class="uccms_breadcrumbs">
    <?php echo $_uccms_ecomm->generateBreadcrumbs($catpa); ?>
</div>

<div id="payout" class="container">

    <form enctype="multipart/form-data" action="<?php if ($payout['status'] < 5) { ?>./process/<?php } ?>" method="post">
    <input type="hidden" name="payout[id]" value="<?php echo $payout['id']; ?>" />

    <header>
        <h2><?php if ($payout['id']) { ?>Edit<?php } else { ?>Create<?php } ?> Payout</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Account</label>
                    <select name="payout[account_id]" <?php if ($payout['status'] > 5) { ?>disabled="disabled"<?php } ?>>
                        <option value="0">None</option>
                        <?php foreach ($_uccms_ecomm->payoutAccounts() as $account_id => $account) { ?>
                            <option value="<?php echo $account_id; ?>" <?php if ($account_id == $payout['account_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($account['fullname']). ' (' .stripslashes($account['email']). ')'; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Build from</label>
                    <select name="payout[build_from]" <?php if ($payout['status'] > 5) { ?>disabled="disabled"<?php } ?>>
                        <option value="manual-amount" <?php if ($payout['build_from'] == 'manual-amount') { ?>selected="selected"<?php } ?>>Enter amount manually</option>
                        <option value="last-payout" <?php if ($payout['build_from'] == 'last-payout') { ?>selected="selected"<?php } ?>>Orders since last payout</option>
                        <option value="date-range" <?php if ($payout['build_from'] == 'date-range') { ?>selected="selected"<?php } ?>>Orders in date range</option>
                        <option value="unpaid" <?php if ($payout['build_from'] == 'unpaid') { ?>selected="selected"<?php } ?>>Orders not paid out</option>
                        <option value="manual-orders" <?php if ($payout['build_from'] == 'manual-orders') { ?>selected="selected"<?php } ?>>Select orders myself</option>
                    </select>
                </fieldset>

            </div>

            <div class="right">

                <fieldset>
                    <label>Notes</label>
                    <textarea name="payout[notes_account]" style="height: 64px;"><?php echo stripslashes($payout['notes_account']); ?></textarea>
                </fieldset>

                <? /*
                <fieldset>
                    <label>Notes (Internal)</label>
                    <textarea name="payout[notes_internal]" style="height: 64px;"><?php echo stripslashes($payout['notes_internal']); ?></textarea>
                </fieldset>
                */ ?>

                <? /*
                <fieldset>
                    <label>Parent payout</label>
                    <select name="payout[parent]">
                        <option value="0">Top-Level</option>
                        <?php $_uccms_ecomm->printpayoutDropdown($_uccms_ecomm->getpayoutTree(), $payout['parent'], $payout['id']); ?>
                    </select>
                </fieldset>
                */ ?>

            </div>

        </div>

        <? /*
        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="payout[slug]" value="<?php echo stripslashes($payout['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="payout[meta_title]" value="<?php echo stripslashes($payout['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="payout[meta_description]" style="height: 64px;"><?php echo stripslashes($payout['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="payout[meta_keywords]" style="height: 64px;"><?php echo stripslashes($payout['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>
        */ ?>

        <div id="orders" style="display: none;">
            Loading..
        </div>

        <fieldset>
            <label>Amount</label>
            $<input type="text" name="payout[amount]" value="<?php echo number_format((float)$payout['amount'], 2, '.', ''); ?>" class="smaller_60" />
        </fieldset>

    </section>

    <footer>
        <?php if ($payout['status'] < 5) { ?>
            <input class="blue" type="submit" name="save" value="Save" />
        <?php } ?>
        <a class="button back" href="./">&laquo; Back</a>
        <?php if ($payout['status'] < 5) { ?>
            <input class="blue" type="submit" name="save-process" value="Save & Process" style="float: right;" />
        <?php } ?>
    </footer>

    </form>

</div>