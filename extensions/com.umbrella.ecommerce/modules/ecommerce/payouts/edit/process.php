<?php

//print_r($_POST);
//exit;

// CLEAN UP
$account_id = (int)$_POST['payout']['account_id'];

// HAVE REQUIRED FIELDS
if ($account_id) {

    // CLEAN
    $id     = (int)$_POST['payout']['id'];
    $amount = $_uccms_ecomm->toFloat($_POST['payout']['amount']);

    $columns = [
        'account_id'    => $account_id,
        'admin_id'      => $admin->ID,
        'dt'            => date('Y-m-d H:i:s'),
        'status'        => 1,
        'build_from'    => $_POST['payout']['build_from'],
        'amount'        => $amount,
        'notes_account' => $_POST['payout']['notes_account'],
    ];

    //print_r($columns);
    //exit;

    // HAVE ID
    if ($id) {

        // UPDATE RECORD
        $payout_query = "UPDATE `" .$_uccms_ecomm->tables['payouts']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$id. ")";

    // NO ID
    } else {

        // CREATE RECORD
        $payout_query = "INSERT INTO `" .$_uccms_ecomm->tables['payouts']. "` SET " .$_uccms_ecomm->createSet($columns). "";

    }

    // RUN QUERY
    if (sqlquery($payout_query)) {

        if ($id) {
            $admin->growl('Success', 'Payout updated.');
        } else {
            $id = sqlid();
            $admin->growl('Success', 'Payout created.');
        }

        // GET PAYOUT INFO
        $payout = $_uccms_ecomm->payoutInfo($id);

        //echo print_r($payout);
        //exit;

        // "OLD" ORDERS
        $oldoa = array_column((array)$payout['orders'], 'order_id');

        // HAVE SELECTED ORDERS
        if (is_array($_POST['orders'])) {
            foreach ($_POST['orders'] as $order_id) {

                $order_id = (int)$order_id;

                // DON'T HAVE RELATION
                if (!in_array($order_id, $oldoa)) {

                    $ocolumns = [
                        'payout_id' => $id,
                        'order_id'  => $order_id,
                    ];

                    $order_query = "INSERT INTO `" .$_uccms_ecomm->tables['payout_orders']. "` SET " .$_uccms_ecomm->createSet($ocolumns). "";
                    sqlquery($order_query);

                }

                if (($key = array_search($order_id, $oldoa)) !== false) {
                    unset($oldoa[$key]);
                }

            }
        }

        // HAVE LEFTOVER ORDERS
        if (count($oldoa) > 0) {
            foreach ($oldoa as $order_id) {
                $order_query = "DELETE FROM `" .$_uccms_ecomm->tables['payout_orders']. "` WHERE (`payout_id`=" .$id. ") AND (`order_id`=" .$order_id. ")";
                sqlquery($order_query);
            }
        }

        // IS PROCESSING
        if ($_POST['save-process']) {

            // HAVE AMOUNT
            if ($amount > 0.00) {

                // GET ACCOUNT
                $account_query = "
                SELECT *
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$account_id. ")
                ";
                $account_q = sqlquery($account_query);
                $account = sqlfetch($account_q);

                if ($account['stripe']) {
                    $account['stripe'] = json_decode(stripslashes($account['stripe']), true);
                }

                // STRIPE CONNECT ACCOUNT ID
                $sc_account_id = $account['stripe']['connect']['account']['stripe_user_id'];

                // HAVE STRIPE CONNECT ACCOUNT ID
                if ($sc_account_id) {

                    require_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

                    \Stripe\Stripe::setApiKey('sk_live_Vv6lFYRBVZhwzzPibqBZRSDY');
                    \Stripe\Stripe::setClientId('ca_Cl8DcJM9myhoBZzUAJcuaTacKV6Z7Xuq');

                    try {

                        // GET ACCOUNT
                        $sc_account = \Stripe\Account::retrieve($sc_account_id);

                        /*
                        // MANUAL PAYOUTS
                        $sc_account->payout_schedule->interval = 'manual';
                        $sc_account->save();
                        */

                        /*
                        // GET BALANCE
                        $balance = \Stripe\Balance::retrieve();
                        //echo print_r($balance);
                        echo 'Pending Balance: $' .number_format(sprintf('%.2f', $balance->pending[0]['amount'] / 100), 2);
                        echo '&nbsp;&nbsp;&nbsp;';
                        echo 'Available Balance: $' .number_format(sprintf('%.2f', $balance->available[0]['amount'] / 100), 2);
                        */

                        /*
                        // TRANSFER
                        $transfer = \Stripe\Transfer::create([
                            "amount"            => 500,
                            "currency"          => "usd",
                            "destination"       => $sc_account_id,
                            //"transfer_group"    => "ORDER_95"
                        ]);
                        echo print_r($transfer);
                        */

                        // TRANSFER
                        $transfer = \Stripe\Transfer::create([
                            'amount'            => str_replace('.', '', $amount),
                            'currency'          => 'usd',
                            'destination'       => $sc_account_id,
                            'transfer_group'    => 'payout-' .$payout['id'],
                            'metadata'          => [
                                'notes' => stripslashes($payout['notes_account']),
                            ]
                        ]);

                        /*
                        class ttrans {
                            var $id = 3453543545;
                        }
                        $transfer = new ttrans;
                        */

                        // HAVE TRANSFER ID (SUCCESS)
                        if ($transfer->id) {

                            $admin->growl('Success', 'Payout transferred.');

                            // LOG TRANSACTION
                            $trans_id = $_uccms_ecomm->logTransaction(array(
                                'cart_id'               => 0,
                                'status'                => 'transferred',
                                'amount'                => $amount,
                                'tip'                   => 0,
                                'method'                => 'stripe',
                                'payment_profile_id'    => 0,
                                'name'                  => '',
                                'lastfour'              => '',
                                'expiration'            => '',
                                'transaction_id'        => $transfer->id,
                                'message'               => '',
                                'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                            ));

                            // UPDATE COLUMNS
                            $columns = [
                                'dt'                => date('Y-m-d H:i:s'),
                                'status'            => 5,
                                'transaction_id'    => $trans_id
                            ];

                            // UPDATE PAYOUT
                            $payout_query = "UPDATE `" .$_uccms_ecomm->tables['payouts']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$id. ")";
                            sqlquery($payout_query);


                        } else {
                            $admin->growl('Error', 'Failed to process payout.');
                        }

                    } catch (Exception $e) {

                        // SITE ERROR MESSAGE
                        $admin->growl('Error', $e->getMessage());

                    }

                // NO STRIPE ACCOUNT
                } else {
                    $admin->growl('Error', 'Account is not connected with Stripe.');
                }

            // NO AMOUNT
            } else {
                $admin->growl('Error', 'No amount to transfer.');
            }

        }

    }

// NO ORDER ID
} else {
    $admin->growl('Error', 'Please select an account first.');
}

BigTree::redirect(MODULE_ROOT. 'payouts/edit/?id=' .$id);

?>