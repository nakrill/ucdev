<?php

// BREADCRUMBS
$bigtree['breadcrumb'] = [
    [ 'title' => 'E-Commerce', 'link' => $bigtree['path'][1] ],
    [ 'title' => 'Categories', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ],
];

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET CATEGORY INFO
    $category_query = "SELECT * FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`id`=" .$id. ")";
    $category_q = sqlquery($category_query);
    $category = sqlfetch($category_q);

// NO ID SPECIFIED
} else {

    // DEFAULTS
    $category['parent'] = (int)$_REQUEST['parent'];

}

// GET CATEGORIES
//$cata = $_uccms_ecomm->getCategoryTree();

// CATEGORY PARENT(S) ARRAY
$catpa = $_uccms_ecomm->getCategoryParents($id);

?>


<style type="text/css">

    #page .left fieldset {
        padding-right: 7.5px;
    }
    #page .right fieldset {
        padding-left: 7.5px;
    }

    .uccms_breadcrumbs {
        padding-bottom: 15px;
    }

    .file_wrapper .data {
        width: 211px;
    }

    #categories .category_title {
        width: 530px;
    }
    #categories .category_subs {
        width: 100px;
    }
    #categories .category_items {
        width: 100px;
    }
    #categories .category_status {
        width: 100px;
    }
    #categories .category_edit {
        width: 55px;
    }
    #categories .category_delete {
        width: 55px;
    }

    #discount .select {
        width: auto;
    }
    #discount p {
        margin-bottom: 0px;
    }

</style>

<div class="uccms_breadcrumbs">
    <?php echo $_uccms_ecomm->generateBreadcrumbs($catpa); ?>
</div>

<?php if (($category['id']) || ($_REQUEST['do'] == 'add')) { ?>

    <div class="container legacy">

        <form enctype="multipart/form-data" action="./process/" method="post">
        <input type="hidden" name="category[id]" value="<?php echo $category['id']; ?>" />

        <header>
            <h2><?php if ($_REQUEST['do'] == 'add') { ?>Add<?php } else { ?>Edit<?php } ?> Category</h2>
        </header>

        <section>

            <div class="row">

                <div class="col-md-6">

                    <fieldset class="form-group">
                        <label>Title</label>
                        <input type="text" name="category[title]" value="<?php echo stripslashes($category['title']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Description</label>
                        <textarea name="category[description]" style="height: 64px;" class="form-control"><?php echo stripslashes($category['description']); ?></textarea>
                    </fieldset>

                    <fieldset class="form-group">
                        <input type="checkbox" name="category[active]" value="1" <?php if ($category['active']) { ?>checked="checked"<?php } ?> class="form-control" />
                        <label class="for_checkbox">Active</label>
                    </fieldset>

                </div>

                <div class="col-md-6">

                    <fieldset class="form-group">
                        <label>Parent Category</label>
                        <select name="category[parent]" class="custom_control form-control">
                            <option value="0">Top-Level</option>
                            <?php $_uccms_ecomm->printCategoryDropdown($_uccms_ecomm->getCategoryTree(), $category['parent'], $category['id']); ?>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Image</label>
                        <div>
                            <?php
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'image', // The value you should use for the "name" attribute of your form field
                                'value'     => ($category['image'] ? $_uccms_ecomm->imageBridgeOut($category['image'], 'categories') : ''), // The existing value for this form field
                                'id'        => 'category_image', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Tax Group</label>
                        <select name="category[tax_group]" class="custom_control form-control">
                            <option value="">Global tax settings</option>
                            <?php

                            // HAVE TAX GROUP SPECIFIED
                            if ($category['tax_group']) {
                                $tax_group = $category['tax_group'];
                                $tgi[$tax_group] = false;
                            // GET INHERITED TAX GROUP
                            } else {
                                $tax_group = $_uccms_ecomm->categoryTaxGroup($category['id']);
                                $tgi[$tax_group] = true;
                            }

                            $taxgroup_query = "SELECT * FROM `" .$_uccms_ecomm->tables['tax_groups']. "` ORDER BY `title` ASC, `id` ASC";
                            $taxgroup_q = sqlquery($taxgroup_query);
                            while ($taxgroup = sqlfetch($taxgroup_q)) {
                                ?>
                                <option value="<?php echo $taxgroup['id']; ?>" <?php if ($taxgroup['id'] == $tax_group) { ?>selected="selected"<?php } ?>><?php echo $_uccms_ecomm->taxGroupName($taxgroup); ?><?php if ($tgi[$taxgroup['id']]) { ?> *inherited*<?php } ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <label class="for_select"><small>Optional. Will override global setting.</small></label>
                    </fieldset>

                </div>

            </div>

            <?php
            if ($_uccms_ecomm->getSetting('pricegroups_enabled')) {

                // GET STORE PRICEGROUPS
                $pga = $_uccms_ecomm->getSetting('pricegroups');

                // GET CATEGORY PRICE GROUPS
                $cpga = $_uccms_ecomm->categoryPriceGroups($category['id']);

                // NO PRICEGROUPS
                if (((!$pga) || (count($pga) == 0)) || (count($cpga) == 0)) {
                    $cpga[1] = array(
                        'pricegroup_id' => 1,
                        'enabled'       => 1
                    );
                }

                ?>
                <h3>Price Groups</h3>
                <div class="contain" style="padding-bottom: 25px;">
                    <table border="0" cellpadding="0" cellspacing="0" class="items" style="width: auto; margin-bottom: 0px;">
                        <tr>
                            <th style="padding: 5px 10px; text-align: left;">Group</th>
                            <th style="padding: 5px 10px; text-align: left;">Enabled</th>
                        </tr>
                        <?php foreach ($_uccms_ecomm->priceGroups() as $group_id => $group) { ?>
                            <tr id="pg-<?=$group_id?>" class="item">
                                <td style="padding: 5px 10px;">
                                    <?php echo $group['title']; ?>
                                </td>
                                <td style="padding: 5px 10px;">
                                    <input type="checkbox" name="pricegroup[<?php echo $group_id; ?>][enabled]" value="1" <?php if ($cpga[$group_id]['enabled']) { ?>checked="checked"<?php } ?> />
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php } ?>

            <div class="contain">
                <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
                <div class="contain uccms_toggle-seo" style="display: none;">

                    <fieldset class="form-group">
                        <label>URL</label>
                        <input type="text" name="category[slug]" value="<?php echo stripslashes($category['slug']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Meta Title</label>
                        <input type="text" name="category[meta_title]" value="<?php echo stripslashes($category['meta_title']); ?>" class="form-control"/>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Meta Description</label>
                        <textarea name="category[meta_description]" style="height: 64px;" class="form-control"><?php echo stripslashes($category['meta_description']); ?></textarea>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Meta Keywords</label>
                        <textarea name="category[meta_keywords]" style="height: 64px;" class="form-control"><?php echo stripslashes($category['meta_keywords']); ?></textarea>
                    </fieldset>

                </div>
            </div>

        </section>

        <footer>
            <input class="btn btn-primary" type="submit" value="Save" />
            <a class="btn btn-secondary" href="./?id=<?php echo $category['parent']; ?>">&laquo; Back</a>
        </footer>

        </form>

    </div>

<?php } ?>

<?php if ($_REQUEST['do'] != 'add') { ?>

    <?php if ($category['id']) { ?>

        <div id="discount" class="container legacy">

            <form enctype="multipart/form-data" action="./process-discount/" method="post">
            <input type="hidden" name="category[id]" value="<?php echo $category['id']; ?>" />

            <header>
                <h2>Discount Settings</h2>
            </header>

            <section>

                <div class="row">

                    <div class="col-md-6">

                        <h6>By Quantity</h6>

                        <p>Apply discount of</p>

                        <div class="clearfix" style="padding: 6px 0;">
                            <select name="discount[quantity_type]" class="custom_control form-control" style="display: inline-block; width: auto;">
                                <option value="percent" <?php if ($category['discount_quantity_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                                <option value="amount" <?php if ($category['discount_quantity_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                            </select>
                            &nbsp;
                            <input type="text" name="discount[quantity_amount]" value="<?php echo number_format((float)$category['discount_quantity_amount'], 2); ?>" class="form-control" style="display: inline-block; width: 60px;" />
                        </div>

                        <p>when the number of items from this category in the cart is at least</p>

                        <div class="clearfix" style="padding: 6px 0 0;">
                            <input type="text" name="discount[quantity_quantity]" value="<?php echo (int)$category['discount_quantity_quantity']; ?>" class="form-control" style="display: inline-block; width: 60px;" />
                        </div>

                    </div>

                    <div class="col-md-6">

                        <h6>By Total</h6>

                        <p>Apply discount of</p>

                        <div class="clearfix" style="padding: 6px 0;">
                            <select name="discount[total_type]" class="custom_control form-control" style="display: inline-block; width: auto;">
                                <option value="percent" <?php if ($category['discount_total_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                                <option value="amount" <?php if ($category['discount_total_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                            </select>
                            &nbsp;
                            <input type="text" name="discount[total_amount]" value="<?php echo number_format((float)$category['discount_total_amount'], 2); ?>" class="form-control" style="display: inline-block; width: 60px;" />
                        </div>

                        <p>when the total from items in this category in the cart is at least</p>

                        <div class="clearfix" style="padding: 6px 0 0;">
                            $<input type="text" name="discount[total_total]" value="<?php echo (int)$category['discount_total_total']; ?>" class="form-control"style="display: inline-block; width: 60px;" />
                        </div>

                    </div>

                </div>

            </section>

            <footer>
                <input class="btn btn-primary" type="submit" value="Save" />
            </footer>

            </form>

        </div>

    <?php } ?>

    <a name="subs"></a>

    <div id="categories" class="table">
        <summary>
            <h2><?php if ($category['id']) { ?>Sub-<?php } ?>Categories</h2>
            <a class="add_resource add" href="./?do=add&parent=<?php echo $category['id']; ?>"><span></span>Add <?php if ($category['id']) { ?>Sub-<?php } ?>Category</a>
        </summary>
        <header style="clear: both;">
            <span class="category_title">Title / Location</span>
            <span class="category_subs">Sub-Categories</span>
            <span class="category_items">Items</span>
            <span class="category_status">Status</span>
            <span class="category_edit">Edit</span>
            <span class="category_delete">Delete</span>
        </header>

        <?php

        // GET DIRECT SUB-CATEGORIES
        $sub_query = "SELECT * FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`parent`=" .$id. ") ORDER BY `sort` ASC, `title` ASC, `id` ASC";
        $sub_q = sqlquery($sub_query);

        // NUMBER OF SUB CATEGORIES
        $num_sub = sqlrows($sub_q);

        // HAVE SUB CATEGORIES
        if ($num_sub > 0) {

            ?>

            <ul class="items">

                <?php

                // LOOP
                while ($sub = sqlfetch($sub_q)) {

                    // GET SUB-CATEGORIES OF THIS SUB-CATEGORY
                    $subsub_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`parent`=" .$sub['id']. ")";
                    $subsub_q = sqlquery($subsub_query);

                    // NUMBER OF SUB-SUB-CATEGORIES
                    $num_subsub = sqlrows($subsub_q);

                    // GET ITEMS IN CATEGORY
                    $items_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['item_categories']. "` WHERE (`category_id`=" .$sub['id']. ")";
                    $items_q = sqlquery($items_query);

                    // NUMBER OF ITEMS
                    $num_items = sqlrows($items_q);

                    ?>

                    <li id="row_<?php echo $sub['id']; ?>" class="item">
                        <section class="category_title">
                            <span class="icon_sort ui-sortable-handle"></span>
                            <a href="./?id=<?php echo $sub['id']; ?>"><?php echo stripslashes($sub['title']); ?></a>
                        </section>
                        <section class="category_subs">
                            <?php echo number_format($num_subsub, 0); ?>
                        </section>
                        <section class="category_items">
                            <?php if ($num_items > 0) { ?><a href="../items/?category_id=<?=$sub['id']?>"><?php } echo number_format($num_items, 0); ?></a>
                        </section>
                        <section class="category_status status_<?php if ($sub['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                            <?php if ($sub['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                        </section>
                        <section class="category_edit">
                            <a class="icon_edit" title="Edit Category" href="./?id=<?php echo $sub['id']; ?>"></a>
                        </section>
                        <section class="category_delete">
                            <a href="./delete/?id=<?php echo $sub['id']; ?>" class="icon_delete" title="Delete Category" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?\nItems in it will not be deleted, just removed from the category.');"></a>
                        </section>
                    </li>

                    <?php

                }

                ?>

            </ul>

            <?php

        } else {
            ?>
            <div style="padding: 10px; text-align: center; font-size: .75em;">
                No categories.
            </div>
            <?php
        }

        ?>

    </div>

    <script>
        $('#categories .items').sortable({
            axis: 'y',
            containment: 'parent',
            handle: '.icon_sort',
            items: 'li',
            placeholder: 'ui-sortable-placeholder',
            tolerance: 'pointer',
            update: function() {
                $.post('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/categories/order/', {
                    sort: $('#categories .items').sortable('serialize')
                });
            }
        });
    </script>

<?php } ?>