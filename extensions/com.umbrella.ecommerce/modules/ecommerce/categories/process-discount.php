<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['category']['id'];

    // HAVE ID
    if ($id) {

        // DB COLUMNS
        $columns = array(
            'discount_quantity_type'        => $_POST['discount']['quantity_type'],
            'discount_quantity_amount'      => $_POST['discount']['quantity_amount'],
            'discount_quantity_quantity'    => $_POST['discount']['quantity_quantity'],
            'discount_total_type'           => $_POST['discount']['total_type'],
            'discount_total_amount'         => $_POST['discount']['total_amount'],
            'discount_total_total'          => $_POST['discount']['total_total'],
            'updated_by'                    => $_uccms_ecomm->adminID()
        );

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['categories']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

        // QUERY SUCCESSFUL
        if (sqlquery($query)) {
            $admin->growl('Category Discount', 'Discount information updated!');
        } else {
            $admin->growl('Category Discount', 'Failed to update category discount information.');
        }

    // NO ID
    } else {
        $admin->growl('Category Discount', 'Category not specified.');
    }

}

BigTree::redirect(MODULE_ROOT.'categories/?id=' .$id. '#discount');

?>