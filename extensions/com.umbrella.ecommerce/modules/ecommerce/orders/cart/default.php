<style type="text/css">

    #orders .order_id {
        width: 35px;
    }
    #orders .order_dt {
        width: 170px;
        text-align: left;
    }
    #orders .order_customer {
        width: 425px;
        text-align: left;
    }
    #orders .order_items {
        width: 100px;
    }
    #orders .order_total {
        width: 100px;
    }
    #orders .order_edit {
        width: 55px;
    }
    #orders .order_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#orders summary .status select').change(function() {
            $('#form_orders').submit();
        });

    });

</script>

<form id="form_orders" method="get">

<div class="search_paging contain">
    <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
    <span class="form_search_icon"></span>
    <nav id="view_paging" class="view_paging"></nav>
</div>

<div id="orders" class="table">
    <summary>
        <h2>Carts</h2>
    </summary>
    <header style="clear: both;">
        <span class="order_id">ID</span>
        <span class="order_dt">Time</span>
        <span class="order_customer">Customer</span>
        <span class="order_items"># Items</span>
        <span class="order_total">Total</span>
        <span class="order_edit">View</span>
        <span class="order_delete">Delete</span>
    </header>
    <ul id="results" class="items">
        <? include(EXTENSION_ROOT. 'ajax/admin/orders/cart/get-page.php'); ?>
    </ul>
</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/orders/cart/get-page/?page=1&query=" + escape($("#query").val()));
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/orders/cart/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&query=" +escape($("#query").val())+ '&status=' +escape($('#orders summary .status select').val()));
        return false;
    });
</script>

</form>