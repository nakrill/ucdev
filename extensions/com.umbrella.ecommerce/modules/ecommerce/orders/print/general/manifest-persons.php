<?php

// REQUIRE ADMIN ACCESS
$admin->requireLevel(0);

// BLANK LAYOUT
$bigtree['layout'] = 'blank';

// CLEAN UP
$order_id       = (int)$_REQUEST['order_id'];
$item_id        = (int)$_REQUEST['item_id'];
$duration_id    = (int)$_REQUEST['duration_id'];

// PREMADE FIELDS
switch ($_REQUEST['premade_fields']) {
    case 'ticket':
        $person_fields  = [
            'fattribute.id=3',
            'fattribute.id=4',
            'fattribute.id=5',
            'billing.fullname',
            'fattribute.id=9',
            'fattribute.id=1',
        ];
        break;
}

// GET MANIFEST
$manifest = $_uccms_ecomm->personManifest([
    'order_id'      => $order_id,
    'item_id'       => $item_id,
    'duration_id'   => $duration_id,
    'from'          => ($_REQUEST['from'] ? $_REQUEST['from']. ' 00:00:00' : ''),
    'to'            => ($_REQUEST['to'] ? $_REQUEST['to']. ' 23:59:59' : ''),
], [
    'style'     => $_REQUEST['style'],
    'order'     => [
        'include-linked' => 'full'
    ],
    'person_fields' => ($_REQUEST['person_fields'] ? $_REQUEST['person_fields'] : $person_fields),
]);

// OUTPUTTING TO CSV
if ($_REQUEST['csv']) {

    $csva = [];

    $i = 1;
    foreach ((array)$manifest['orders'] as $order) {
        foreach ((array)$order['persons-mapped'] as $pfields) {
            foreach ((array)$pfields as $pfield) {
                if ($i == 1) { // FIRST ROW - HEADING
                    $csva[0][] = stripslashes($pfield['title']);
                }
                $csva[$i][] = stripslashes($pfield['value']);
            }
            $i++;
        }
    }

    $csv = array_to_csv($csva);

    // FILE NAME
    $filename = 'manifest';

    // HEADERS
    header("Content-type: text/csv");
    header('Content-Disposition: attachment; filename=' .$filename. '.csv');

    // OUTPUT
    echo $csv;
    exit;

}

$oitem = [];
$item = [];

// FIRST ORDER ITEM
if ($manifest['original-order']) {
    $oitem = array_pop(array_reverse((array)$manifest['orders'][$manifest['original-order']]['items']));
    $item = $oitem['item'];

// HAVE ITEM ID
} else if ($item_id) {

    // GET ITEM
    $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item_id. ")";
    $item_q = sqlquery($item_query);
    $item = sqlfetch($item_q);

}

//echo print_r($item);
//exit;

// DURATION
$duration = [];
if ((!$duration_id) && ($oitem['booking']['duration_id'])) {
    $duration_id = $oitem['booking']['duration_id'];
}
if ($duration_id) {

    // GET INFO
    $duration_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`id`=" .$duration_id. ")";
    $duration_q = sqlquery($duration_query);
    $duration = sqlfetch($duration_q);

    // NO ORDER ITEM
    if (count($oitem) == 0) {

        // USE DURATION INFO
        $oitem = [
            'booking' => [
                'duration_id'   => $duration['id'],
                'dt_from'       => ($_REQUEST['from'] ? date('Y-m-d', strtotime($_REQUEST['from'])) : date('Y-m-d')). ' ' .$duration['start'],
                'dt_to'         => ($_REQUEST['to'] ? date('Y-m-d', strtotime($_REQUEST['to'])) : date('Y-m-d')). ' ' .$duration['end'],
            ],
        ];

    }

}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Person Manifest</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />

    <? /*
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    */ ?>

    <style type="text/css">

        body {
            font-size: .9rem;
            -webkit-print-color-adjust: exact !important;
        }

        #print_button {
            position: absolute;
            top: 25px;
            right: 30px;
            display: block;
            text-transform: uppercase;
        }

        #print_container {
            margin-top: 60px;
        }
        #print_container h3 {
            text-align: center;
        }
        #print_container .main-order {
            margin-top: 15px;
        }
        #print_container .orders {
            margin-top: 15px;
        }
        #print_container .orders .order {
            margin-bottom: 30px;
        }
        #print_container .orders .order > .contain {
            padding: 15px;
            background-color: rgba(0, 0, 0, .03);
            border: 1px solid rgba(0, 0, 0, .05);
        }
        #print_container .orders .order.master > .contain {
            border-color: rgba(0, 0, 0, .4);
        }
        #print_container .orders .order.sub > .contain {
            opacity: .8;
        }
        #print_container .orders .order .info {
            margin-bottom: 15px;
        }
        #print_container .orders .order .info .id {
            font-weight: bold;
        }

        @media screen {

            .screen_hide {
                display: none !important;
            }

        }

        @media print {

            .print_hide {
                display: none !important;
            }

        }

    </style>

</head>

<body>

<a href="#" id="print_button" class="btn btn-primary print_hide" onclick="window.print(); return false;">Print</a>

<div id="print_container" class="container">

    <h3>Person Manifest</h3>

    <div class="main-order">

        <?php if ($item['title']) { ?>
            <h4><?php echo stripslashes($item['title']); ?></h4>
        <?php } ?>

        <?php if (is_array($oitem['booking'])) { ?>
            <div class="booking">
                <span class="date"><?php echo date('n/j/Y', strtotime($oitem['booking']['dt_from'])); ?></span>
                <span class="time"><?php echo date('g:i A', strtotime($oitem['booking']['dt_from'])); ?></span>
                <?php if ($duration['id']) { ?>
                    <span class="duration">(<?php echo stripslashes($duration['title']); ?>)</span>
                <?php } ?>
            </div>
        <?php } ?>

    </div>

    <?php

    // HAVE ORDERS
    if (count($manifest['orders']) > 0) {

        ?>

        <div class="orders">

            <?php

            // LOOP
            foreach ($manifest['orders'] as $ord_id => $ord) {

                // CALCULATIONS
                $ord_total = $ord['order_total'];
                $ord_balance = $_uccms_ecomm->orderBalance($ord['id']);
                $ord_paid = $ord_total - $ord_balance;

                // MASTER ORDER - SUB ORDERS
                $css_ms = '';
                if (count($manifest['orders']) > 1) {
                    if ($ord_id == $manifest['original-order']) {
                        $css_ms = 'master';
                    } else {
                        $css_ms = 'sub';
                    }
                }

                ?>

                <div class="order <?php echo $css_ms; ?>">
                    <div class="contain">

                        <div class="info row">

                            <div class="col-md-4">

                                <div class="id">Order: <?php echo $ord_id; ?></div>

                                <div class="billing name"><?php echo trim(stripslashes($ord['billing_firstname']. ' ' .$ord['billing_lastname'])); ?></div>

                            </div>

                            <div class="col-md-4">

                                <dl class="row">
                                    <dt class="col-sm-3">Total</dt>
                                    <dd class="col-sm-9">$<?php echo number_format($ord_total, 2); ?></dd>
                                    <dt class="col-sm-3">Paid</dt>
                                    <dd class="col-sm-9">$<?php echo number_format($ord_paid, 2); ?></dd>
                                    <dt class="col-sm-3">Balance</dt>
                                    <dd class="col-sm-9">$<?php echo number_format($ord_balance, 2); ?></dd>
                                </dl>

                            </div>

                            <div class="col-md-4">

                                <?php if (count($ord['totals']['attributes']) > 0) { ?>

                                    <dl class="row">
                                        <?php foreach ($ord['totals']['attributes'] as $attr_slug => $attr) { ?>
                                            <dt class="col-sm-6"><?php echo $attr['title']; ?></dt>
                                            <dd class="col-sm-6"><?php echo number_format((int)$attr['count'], 0); ?></dd>
                                        <?php } ?>
                                    </dl>

                                <?php } ?>

                            </div>

                        </div>

                        <?php

                        // HAVE PERSONS
                        if (count($ord['persons-mapped']) > 0) {

                            $first_person = array_shift($ord['persons-mapped']);

                            array_unshift($ord['persons-mapped'], $first_person);

                            ?>

                            <div class="person-list table">
                                <table class="table <? /*table-striped*/ ?>">
                                    <thead class="thead-light">
                                        <tr>
                                            <?php foreach ((array)$first_person as $fp) { ?>
                                                <th scope="col"><?php echo $fp['title']; ?></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        // LOOP THROUGH PERSONS IN ORDER
                                        foreach ($ord['persons-mapped'] as $pid => $pfields) {

                                            // GET PERSON INFO
                                            //$person = $manifest['persons'][$person_id];

                                            ?>
                                            <tr>
                                                <?php
                                                foreach ($pfields as $pfield) {

                                                    $value = $pfield['value'];

                                                    // EMPTY / NA VALUES
                                                    if ((!$value) || (strtolower($value) == 'na')) {
                                                        unset($person[$field]);
                                                    }

                                                    ?>
                                                    <td><?php echo $value; ?></td>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                            <?php

                                        }

                                        ?>
                                    </tbody>
                                </table>
                            </div>

                            <?php

                        }

                        ?>

                        <?php if (count($ord['makeup']['items']) > 0) { ?>
                            <div class="makeup all-dates">
                                <span class="label">All dates:</span>
                                <span class="value">
                                    <?php
                                    echo date('n/j/Y', strtotime($oitem['booking']['dt_from']));
                                    foreach ($ord['makeup']['items'] as $coitem) {
                                        echo ', ' .date('n/j/Y', strtotime($coitem['booking']['dt_from']));
                                    }
                                    ?>
                                </span>
                            </div>
                        <?php } ?>

                    </div>
                </div>

                <?php

            }

            ?>

        </div>

        <?php

    // NO PEOPLE
    } else {

        ?>
        No people found for manifest.
        <?php

    }

    ?>


</div>

</body>

</html>