<?php

// REQUIRE ADMIN ACCESS
$admin->requireLevel(0);

// BLANK LAYOUT
$bigtree['layout'] = 'blank';

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Order Info</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/css/main.css" type="text/css" />
    <link rel="stylesheet" href="/admin/css/uccms.css" type="text/css" />
    <link rel="stylesheet" href="/admin/*/<?=$_uccms_ecomm->Extension?>/css/master.css" type="text/css" />

    <style type="text/css">

        body {
          -webkit-print-color-adjust: exact !important;
        }

        #print_button {
            position: absolute;
            top: 25px;
            right: 30px;
            display: block;
            padding: 10px 20px 7px;
            background-color: #eee;
            border-radius: 4px;
            font-size: 1.3em;
            text-transform: uppercase;
        }

        #print_container {
            max-width: 800px;
            min-width: 400px;
            margin: 20px auto;
        }

        #print_container h1 {
            margin: 25px 0 5px 0;
            padding: 0px;
            font-size: 1.7em;
            line-height: 1em;
            font-weight: bold;
            color: #222;
        }
        #print_container h2 {
            margin: 0px 0 35px 0;
            padding: 0px;
            font-size: 1.2em;
            line-height: 1em;
            font-weight: normal;
            color: #222;
        }
        #print_container h3 {
            margin: 0 0 8px 0;
            font-size: 1.4em;
            font-weight: bold;
            text-transform: none;
            color: #222;
        }
        #print_container h3.border-bottom {
            padding: 0 0 3px 0;
            border-bottom: 1px solid #f5f5f5;
        }
        #print_container .logo {
            text-align: center;
            font-size: 2.5em;
        }
        #print_container .logo img {
            max-width: 350px;
            height: auto;
            max-height: 120px;
        }
        #print_container table tr:nth-child(2n) td {
            background-color: transparent;
        }
        #print_container table td {
            padding: 7px 0;
        }
        #print_container table .group_heading td {
            border-bottom: 1px solid #ccc;
        }
        #print_container table .item td:first-child {
            padding-left: 0px;
        }
        #print_container table .item .title {
            font-weight: bold;
        }
        #print_container table .item .booking .title {
            display: block;
            font-weight: normal;
        }
        #print_container table .item .options {
            margin-top: 2px;
        }
        #print_container table .item .options li {
            font-size: 1em;
            line-height: 1.2em;
        }
        #print_container table.charges_summary td {
            padding: 7px 5px 7px 0;
        }
        #print_container table.payment_summary td {
            font-size: 1.5em;
        }

        #print_container .big_split {
            margin: 20px 0;
            height: 2px;
            background-color: #eee;
        }

        #print_container .transactions {
            /*margin-top: 15px;*/
        }
        #print_container .transactions table {
            margin: 0px;
            border: 0px none;
        }
        #print_container .transactions table tr:first-child {
            background-color: #f5f5f5;
        }
        #print_container .transactions table th {
            padding: 5px 50px 5px 10px;
            background-color: transparent;
            font-size: 1em;
            font-weight: bold;
            color: #333;
            white-space: nowrap;
        }
        #print_container .transactions table th:last-child {
            padding-right: 10px;
            text-align: right;
        }
        #print_container .transactions table td {
            padding: 5px 50px 0 10px;
            white-space: nowrap;
        }
        #print_container .transactions table td:last-child {
            padding-right: 10px;
            text-align: right;
        }

        @media screen {

            .screen_hide {
                display: none !important;
            }

        }

        @media print {

            .print_hide {
                display: none !important;
            }

        }

    </style>

</head>

<body>

<a href="#" id="print_button" class="print_hide" onclick="window.print(); return false;">Print</a>

<div id="print_container">

    <?php

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // HAVE ID
    if ($id) {

        // GET ORDER INFO
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // ORDER FOUND
        if ($order['id']) {

            // GET EXTRA INFO
            $general_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_general']. "` WHERE (`order_id`=" .$id. ")";
            $general_q = sqlquery($general_query);
            $general = sqlfetch($general_q);

            // GET ORDER ITEMS
            $citems = $_uccms_ecomm->cartItems($order['id']);

            // GET EXTRA INFO
            $extra = $_uccms_ecomm->cartExtra($order['id']);

            // GET BALANCE
            $balance = $_uccms_ecomm->orderBalance($order['id']);

            // CALCULATE PAID
            $paid = number_format($order['order_total'] - $balance, 2);

            // STATUS COLORS
            foreach ($_uccms_ecomm->statusColors() as $type => $colors) {
                foreach ($colors as $status => $color) {
                    $scola[$status] = $color;
                }
            }

            ?>

            <div class="logo">
                <?php if ($cms->getSetting('logo')) { ?>
                    <img src="<?php echo $cms->getSetting('logo'); ?>" alt="<?php echo $cms->getSetting('site_name'); ?>" />
                <?php } else { ?>
                    <?php echo $cms->getSetting('site_name'); ?>
                <?php } ?>
            </div>

            <h1>Order #<?php echo $order['id']; ?></h1>

            <h2><?php echo trim($order['billing_firstname']. ' ' .$order['billing_lastname']); ?></h2>

            <table class="billing_shipping" width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">
                <tr>
                    <td width="50%" valign="top">
                        <h3>Billing</h3>
                        <div class="fields billing">
                            <div class="name">
                                <?php echo trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])); ?>
                            </div>
                            <?php if ($order['billing_company']) { ?>
                                <div class="company"><?php echo stripslashes($order['billing_company']);  ?></div>
                            <?php } ?>
                            <?php if ($order['billing_address1']) { ?>
                                <div class="address1"><?php echo stripslashes($order['billing_address1']);  ?></div>
                            <?php } ?>
                            <?php if ($order['billing_address2']) { ?>
                                <div class="address2"><?php echo stripslashes($order['billing_address2']);  ?></div>
                            <?php } ?>
                            <?php
                            $billing_csz = array();
                            if ($order['billing_city']) $billing_csz[] = stripslashes($order['billing_city']);
                            if ($order['billing_state']) $billing_csz[] = stripslashes($order['billing_state']);
                            if ($order['billing_zip']) $billing_csz[] = stripslashes($order['billing_zip']);
                            if (count($billing_csz) > 0) {
                                ?>
                                <div class="csz"><?php echo implode(' ', $billing_csz); ?></div>
                                <?php
                            }
                            ?>
                            <?php if ($order['billing_phone']) { ?>
                                <div class="phone"><?php echo stripslashes($order['billing_phone']);  ?></div>
                            <?php } ?>
                            <?php if ($order['billing_email']) { ?>
                                <div class="email"><?php echo stripslashes($order['billing_email']);  ?></div>
                            <?php } ?>
                        </div>
                    </td>
                    <td width="50%" valign="top">
                        <h3>Shipping</h3>
                        <div class="fields shipping">
                            <?php if ($order['shipping_same']) { ?>
                                Same as billing.
                            <?php } else { ?>
                                <div class="name">
                                    <?php echo trim(stripslashes($order['shipping_firstname']. ' ' .$order['shipping_lastname'])); ?>
                                </div>
                                <?php if ($order['shipping_company']) { ?>
                                    <div class="company"><?php echo stripslashes($order['shipping_company']);  ?></div>
                                <?php } ?>
                                <?php if ($order['shipping_address1']) { ?>
                                    <div class="address1"><?php echo stripslashes($order['shipping_address1']);  ?></div>
                                <?php } ?>
                                <?php if ($order['shipping_address2']) { ?>
                                    <div class="address2"><?php echo stripslashes($order['shipping_address2']);  ?></div>
                                <?php } ?>
                                <?php
                                $shipping_csz = array();
                                if ($order['shipping_city']) $shipping_csz[] = stripslashes($order['shipping_city']);
                                if ($order['shipping_state']) $shipping_csz[] = stripslashes($order['shipping_state']);
                                if ($order['shipping_zip']) $shipping_csz[] = stripslashes($order['shipping_zip']);
                                if (count($shipping_csz) > 0) {
                                    ?>
                                    <div class="csz"><?php echo implode(' ', $shipping_csz); ?></div>
                                    <?php
                                }
                                ?>
                                <?php if ($order['shipping_phone']) { ?>
                                    <div class="phone"><?php echo stripslashes($order['shipping_phone']);  ?></div>
                                <?php } ?>
                                <?php if ($order['shipping_email']) { ?>
                                    <div class="email"><?php echo stripslashes($order['shipping_email']);  ?></div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </td>
                </tr>
            </table>

            <table class="charges_summary" width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">
                <tr>
                    <td width="60%" valign="top">

                        <h3>Summary of Charges</h3>

                        <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">

                            <?php

                            // SUBTOTAL
                            $order['subtotal'] = 0;

                            // ATTRIBUTE ARRAY
                            $attra = array();

                            // AFTER TAX ITEM ARRAY
                            $atia = array();

                            // CURRENT DISPLAY GROUP ARRAY
                            $cdga = array();

                            // HAVE TAX OVERRIDE
                            if ($order['override_tax']) {
                                $tax['total'] = number_format($order['override_tax'], 2);

                            // NORMAL TAX CALCULATION
                            } else {

                                // HAVE BILLING STATE AND/OR ZIP
                                if (($vals['contact']['state']) || ($vals['contact']['zip'])) {
                                    $taxvals['country'] = 'United States';
                                    $taxvals['state']   = $vals['contact']['state'];
                                    $taxvals['zip']     = $vals['contact']['zip'];
                                }

                                //$taxvals['quote'] = false;

                                // GET TAX
                                $tax = $_uccms_ecomm->cartTax($order['id'], $citems, $taxvals);

                            }

                            // ORDER TAX
                            $order['tax'] = $tax['total'];

                            // LOOP THROUGH ITEMS
                            foreach ($citems as $oitem) {

                                // AFTER TAX ITEM
                                if ($oitem['after_tax']) {

                                    // ADD TO AFTER TAX ITEM ARRAY
                                    $atia[] = $oitem;

                                // NORMAL ITEM
                                } else {

                                    // GET ITEM INFO
                                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                    // DAY / TIME IS DIFFERENT
                                    if ($oitem['display_group']['day_time'] != $cdga['day_time']) {

                                        // LOOP DAY / TIME
                                        $loop_day = ($cdga['day_time'] ? date('m/d/Y', $cdga['day_time']) : '');
                                        $loop_time = ($cdga['day_time'] ? date('g:i a', $cdga['day_time']) : '');

                                        // ITEM DAY / TIME
                                        $item_day = date('m/d/Y', $oitem['display_group']['day_time']);
                                        $item_time = date('g:i a', $oitem['display_group']['day_time']);

                                        // DAY OR TIME IS DIFFERENT
                                        if (($item_day != $loop_day) || ($item_time != $loop_time)) {

                                            ?>

                                            <tr class="group_heading">
                                                <td colspan="4">
                                                    <strong><?=$item_day?></strong> <?=$item_time?>
                                                </td>
                                            </tr>

                                            <?php

                                            if ($item_day != $loop_day) {
                                                $loop_time = '';
                                            }

                                        }

                                    }

                                    // HAVE CATEGORY AND IS DIFFERENT
                                    if (($oitem['display_group']['category']) && ($oitem['display_group']['category'] != $cdga['category'])) {

                                        // DON'T HAVE CATEGORY INFO YET
                                        if (!$dgca[$oitem['display_group']['category']]) {

                                            // GET CATEGORY INFO
                                            $dgc_query = "SELECT * FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`id`=" .(int)$oitem['display_group']['category']. ")";
                                            $dgc_q = sqlquery($dgc_query);
                                            $dgca[$oitem['display_group']['category']] = sqlfetch($dgc_q);

                                        }

                                        ?>

                                        <tr class="group_heading2">
                                            <td colspan="4">
                                                <strong><?php echo stripslashes($dgca[$oitem['display_group']['category']]['title']); ?></strong>
                                            </td>
                                        </tr>

                                        <?php

                                    }

                                    ?>

                                    <tr class="item">
                                        <td valign="top">
                                            <i class="fa fa-fw fa-archive"></i>
                                        </td>
                                        <td width="100%">

                                            <div class="title">
                                                <?php echo stripslashes($item['title']); ?>
                                            </div>

                                            <?php if ($booking['id']) { ?>
                                                <div class="booking">
                                                    <?php if ($booking_duration['title']) { ?>
                                                        <span class="title"><?php echo str_replace('(no preference)', '', stripslashes($booking_duration['title'])); ?></span>
                                                    <?php } ?>
                                                    <span class="date">
                                                        <?php
                                                        $from_to = '';
                                                        if (($booking['dt_from']) && ($booking['dt_from'] != '0000-00-00 00:00:00')) {
                                                            list($booking_from_date, $booking_from_time) = explode(' ', $booking['dt_from']);
                                                            $from_to .= '
                                                            <span class="from">
                                                                <span class="date">' .date('n/j/Y', strtotime($booking_from_date)). '</span>
                                                                ';
                                                                if ($booking_from_time != '00:00:00') {
                                                                    $from_to .= '<span class="time">' .date('g:i A', strtotime($booking_from_time)). '</span>';
                                                                }
                                                                $from_to .='
                                                            </span>
                                                            ';
                                                            if (($booking['dt_to']) && ($booking['dt_to'] != '0000-00-00 00:00:00')) {
                                                                list($booking_to_date, $booking_to_time) = explode(' ', $booking['dt_to']);
                                                                $from_to .= ' to
                                                                <span class="to">';
                                                                    if ($booking_from_date != $booking_to_date) {
                                                                        $from_to .= '<span class="date">' .date('n/j/Y', strtotime($booking_to_date)). '</span>';
                                                                    }
                                                                    if ($booking_from_time != '00:00:00') {
                                                                        $from_to .= '
                                                                        <span class="time">' .date('g:i A', strtotime($booking_to_time) + 1). '</span>';
                                                                    }
                                                                    $from_to .='
                                                                </span>
                                                                ';
                                                            }
                                                        }
                                                        echo $from_to;
                                                        ?>
                                                    </span>
                                                    <?php /*if ($booking['markup'] != 0.00) { ?>
                                                        <span class="markup">(+ $<?php echo $_uccms_ecomm->toFloat($booking['markup']); ?>)</span>
                                                    <?php }*/ ?>
                                                </div>
                                            <?php } ?>

                                            <?php if ((count((array)$item['other']['options']) > 0) || (($item['type'] == 3) && (count((array)$item['package']['items']) > 0))) { ?>
                                                <ul class="options">
                                                    <?php

                                                    // IS PACKAGE AND HAVE ITEMS
                                                    if (($item['type'] == 3) && (count($item['package']['items']) > 0)) {

                                                        // LOOP THROUGH ITEMS
                                                        foreach ($item['package']['items'] as $pitem_id => $pitem) {

                                                            // GET SELECTED ATTRIBUTES / OPTIONS
                                                            $pitem_attrs = $_uccms_ecomm->item_attrAndOptionTitles($pitem, $pitem['other']['attribute_titles'], $attr_options);

                                                            ?>

                                                            <li><?php echo stripslashes($pitem['item']['title']); if ($pitem['quantity'] > 1) { echo ' (' .number_format($pitem['quantity'], 0). ')'; } ?>
                                                                <?php if (count($pitem_attrs) > 0) { ?>
                                                                    <ul>
                                                                        <?php foreach ($pitem_attrs as $attr) { ?>
                                                                            <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>
                                                            </li>

                                                            <?php

                                                        }

                                                    // OTHER TYPES
                                                    } else {
                                                        if (count($item['other']['options']) > 0) {
                                                            foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra, $attr_options) as $attr) {
                                                                ?>
                                                                <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                                <?php
                                                            }
                                                        }
                                                    }

                                                    ?>
                                                </ul>
                                            <?php } ?>

                                        </td>

                                        <td valign="top" style="padding-right: 30px; text-align: left; white-space: nowrap;">
                                            x<?php echo $oitem['quantity']; ?>
                                        </td>

                                        <td valign="top" style="text-align: right; white-space: nowrap;">
                                            <?php echo $item['other']['total_formatted']; ?>
                                        </td>
                                    </tr>

                                    <?php

                                    // SET CURRENT DISPLAY GROUP
                                    $cdga = $oitem['display_group'];

                                }

                            }

                            ?>

                            <tr style="background-color: #f5f5f5; border: 1px solid #e0e0e0;">
                                <td colspan="3" width="100%">
                                    <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Subtotal
                                </td>
                                <td style="text-align: right; white-space: nowrap;">
                                    $<span class="num"><?php echo number_format($order['subtotal'], 2); ?>
                                </td>
                            </tr>

                            <?php

                            // MAIN SERVICE FEE
                            $service_fees += $_uccms_ecomm->orderServiceFee($order);

                            // HAVE FEE(S)
                            if ($service_fees) {

                                ?>
                                <tr>
                                    <td colspan="3" width="100%">
                                        <i class="fa fa-fw fa-usd"></i>&nbsp;&nbsp;Service Fee
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<?php echo number_format($service_fees, 2); ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if (($order['order_discount']) && ($order['order_discount'] != 0.00)) { ?>
                                <tr>
                                    <td colspan="3" width="100%">
                                        <i class="fa fa-fw fa-minus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Discount
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<?php echo number_format($order['order_discount'], 2); ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <tr>
                                <td colspan="3" width="100%">
                                    <i class="fa fa-fw fa-usd"></i>&nbsp;&nbsp;Tax
                                </td>
                                <td style="text-align: right; white-space: nowrap;">
                                    $<span class="num"><?php if ($order['override_tax']) { number_format($order['override_tax'], 2); } else { echo number_format($order['tax'], 2); } ?></span>
                                </td>
                            </tr>
                            <?php if (($order['order_shipping']) && ($order['order_shipping'] != 0.00)) { ?>
                                <tr>
                                    <td colspan="3" width="100%">
                                        <i class="fa fa-truck" aria-hidden="true"></i>&nbsp;&nbsp;Shipping
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<span class="num"><?php echo number_format($order['order_shipping'], 2); ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
                                /*
                                if ((!$order['order_gratuity']) || ($order['gratuity'] == 0.00)) {
                                    if ($order['override_tax']) { $tax = $order['override_tax']; } else { $tax = $order['tax']; }
                                    $order['order_gratuity'] = round((($order['subtotal'] + $tax) * $_uccms_ecomm->getSetting('gratuity_percent')) / 100, 2);
                                    $order['gratuity'] = $order['order_gratuity'];
                                }
                                */
                                ?>
                                <tr>
                                    <td colspan="3" width="100%">
                                        <i class="fa fa-fw fa-usd"></i>&nbsp;&nbsp;Gratuity (<?php echo ($order['override_gratuity'] !== '' ? $order['override_gratuity'] : $_uccms_ecomm->getSetting('gratuity_percent')); ?>%)
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<?php echo $order['order_gratuity']; ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php

                            // HAVE AFTER TAX ITEMS
                            if (count($atia) > 0) {

                                // LOOP
                                foreach ($atia as $oitem) {

                                    // GET ITEM INFO
                                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                    ?>

                                    <tr class="item">
                                        <td valign="top">
                                            <i class="fa fa-fw fa-archive"></i>
                                        </td>
                                        <td width="100%">

                                            <div class="title">
                                                <?php echo stripslashes($item['title']); ?>
                                            </div>

                                            <?php if ($booking['id']) { ?>
                                                <div class="booking">
                                                    <?php if ($booking_duration['title']) { ?>
                                                        <span class="title"><?php echo str_replace('(no preference)', '', stripslashes($booking_duration['title'])); ?></span>
                                                    <?php } ?>
                                                    <span class="date">
                                                        <?php
                                                        $from_to = '';
                                                        if (($booking['dt_from']) && ($booking['dt_from'] != '0000-00-00 00:00:00')) {
                                                            list($booking_from_date, $booking_from_time) = explode(' ', $booking['dt_from']);
                                                            $from_to .= '
                                                            <span class="from">
                                                                <span class="date">' .date('n/j/Y', strtotime($booking_from_date)). '</span>
                                                                ';
                                                                if ($booking_from_time != '00:00:00') {
                                                                    $from_to .= '<span class="time">' .date('g:i A', strtotime($booking_from_time)). '</span>';
                                                                }
                                                                $from_to .='
                                                            </span>
                                                            ';
                                                            if (($booking['dt_to']) && ($booking['dt_to'] != '0000-00-00 00:00:00')) {
                                                                list($booking_to_date, $booking_to_time) = explode(' ', $booking['dt_to']);
                                                                $from_to .= ' to
                                                                <span class="to">';
                                                                    if ($booking_from_date != $booking_to_date) {
                                                                        $from_to .= '<span class="date">' .date('n/j/Y', strtotime($booking_to_date)). '</span>';
                                                                    }
                                                                    if ($booking_from_time != '00:00:00') {
                                                                        $from_to .= '
                                                                        <span class="time">' .date('g:i A', strtotime($booking_to_time) + 1). '</span>';
                                                                    }
                                                                    $from_to .='
                                                                </span>
                                                                ';
                                                            }
                                                        }
                                                        echo $from_to;
                                                        ?>
                                                    </span>
                                                    <?php /*if ($booking['markup'] != 0.00) { ?>
                                                        <span class="markup">(+ $<?php echo $_uccms_ecomm->toFloat($booking['markup']); ?>)</span>
                                                    <?php }*/ ?>
                                                </div>
                                            <?php } ?>

                                            <?php if ((count($item['other']['options']) > 0) || (($item['type'] == 3) && (count($item['package']['items']) > 0))) { ?>
                                                <ul class="options">
                                                    <?php

                                                    // IS PACKAGE AND HAVE ITEMS
                                                    if (($item['type'] == 3) && (count($item['package']['items']) > 0)) {

                                                        // LOOP THROUGH ITEMS
                                                        foreach ($item['package']['items'] as $pitem_id => $pitem) {

                                                            // GET SELECTED ATTRIBUTES / OPTIONS
                                                            $pitem_attrs = $_uccms_ecomm->item_attrAndOptionTitles($pitem, $pitem['other']['attribute_titles'], $attr_options);

                                                            ?>

                                                            <li><?php echo stripslashes($pitem['item']['title']); if ($pitem['quantity'] > 1) { echo ' (' .number_format($pitem['quantity'], 0). ')'; } ?>
                                                                <?php if (count($pitem_attrs) > 0) { ?>
                                                                    <ul>
                                                                        <?php foreach ($pitem_attrs as $attr) { ?>
                                                                            <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>
                                                            </li>

                                                            <?php

                                                        }

                                                    // OTHER TYPES
                                                    } else {
                                                        if (count($item['other']['options']) > 0) {
                                                            foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra, $attr_options) as $attr) {
                                                                ?>
                                                                <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                                <?php
                                                            }
                                                        }
                                                    }

                                                    ?>
                                                </ul>
                                            <?php } ?>

                                        </td>

                                        <td valign="top" style="padding-right: 30px; text-align: left; white-space: nowrap;">
                                            x<?php echo $oitem['quantity']; ?>
                                        </td>

                                        <td valign="top" style="text-align: right; white-space: nowrap;">
                                            <?php echo $item['other']['total_formatted']; ?>
                                        </td>

                                    </tr>

                                    <?php

                                }

                            }

                            ?>

                            <tr style="background-color: #f5f5f5; border: 1px solid #e0e0e0;">
                                <td colspan="3" width="100%">
                                    <i class="fa fa-fw fa-check"></i>&nbsp;&nbsp;Total
                                </td>
                                <td style="text-align: right; white-space: nowrap;">
                                    $<span class="num"><?php echo number_format($order['order_total'], 2); ?></span>
                                </td>
                            </tr>

                            <? /*
                            <div style="padding-top: 10px; text-align: right;">
                                <div style="padding-bottom: 10px; font-size: 1.2em;">
                                    Paid: $<?=$paid?>
                                </div>
                                <div style="font-size: 1.4em;">
                                    Balance: $<?=$balance?>
                                </div>
                            </div>
                            */ ?>

                        </table>

                    </td>
                    <td width="40%" valign="top" style="padding-left: 50px;">

                        <h3 class="border-bottom">Payment Summary</h3>
                        <table width="100%" cellpadding="0" cellspacing="0" class="payment_summary" style="border: 0px; margin-bottom: 20px;">
                            <tr>
                                <td width="100%">
                                    Total
                                </td>
                                <td style="text-align: right; white-space: nowrap;">
                                    $<span class="num"><?php echo number_format($order['order_total'], 2); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%">
                                    Paid
                                </td>
                                <td style="text-align: right; white-space: nowrap;">
                                    $<span class="num"><?php echo number_format(str_replace(',', '', $paid), 2); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%">
                                    Balance
                                </td>
                                <td style="text-align: right; white-space: nowrap;">
                                    $<span class="num"><?php echo number_format($balance, 2); ?></span>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

            <div class="big_split"></div>

            <h3>Notes</h3>

            <?php if ($order['notes']) { ?>
                <div style="padding-bottom: 10px;">
                    <?php echo nl2br(stripslashes($order['notes'])); ?>
                </div>
            <?php } ?>

            <div>
                <textarea style="width: 100%; height: 100px; padding: 10px; border: 1px solid #f5f5f5;"></textarea>
            </div>

            <div class="big_split"></div>

            <? /*
            <div class="status_container" style="padding: 2px; background-color: <?php echo $scola[$order['status']]; ?>;">
                <div style="padding: 5px 13px; background-color: #fff; opacity: 0.9;">
                    <table width="100%" cellpadding="0" cellspacing="0" style="margin: 0px; border: 0px;">
                        <tr>
                            <td style="vertical-align: top; white-space: nowrap;">
                                <i class="fa fa-check" style="font-size: 2em;"></i>
                            </td>
                            <td width="100%" style="padding-top: 12px; padding-left: 20px;">
                                Your order has been <?php echo $order['status']; ?>.
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            */ ?>

            <?php

            // GET TRANSACTION LOG
            $translog_query = "SELECT * FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`cart_id`='" .stripslashes($order['id']). "') ORDER BY `dt` DESC";
            $translog_q = sqlquery($translog_query);

            // HAVE TRANSACTIONS
            if (sqlrows($translog_q) > 0) {

                ?>

                <h3>Payments</h3>

                <div class="transactions">

                    <table>
                        <tr>
                            <th>Date</th>
                            <th style="width: 50%;">Status</th>
                            <th style="width: 50%;">Transaction ID</th>
                            <th>Amount</th>
                        </tr>
                        <?php while ($transaction = sqlfetch($translog_q)) { ?>
                            <tr>
                                <td><?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?></td>
                                <td><?php echo ucwords($transaction['status']); ?></td>
                                <td><?php echo $transaction['transaction_id']; ?></td>
                                <td>$<?php echo number_format($transaction['amount'], 2); ?></td>
                            </tr>
                        <?php } ?>
                    </table>

                </div>

                <?php
            }

            ?>

            <div class="big_split"></div>

            <div class="signature" style="padding-top: 10px; font-size: 1.1em; font-weight: bold;">
                Signature: _______________________________________________ &nbsp;&nbsp;&nbsp; Date: __________________
            </div>

            <?php

        // ORDER NOT FOUND
        } else {
            ?>
            Order not found.
            <?php
        }

        ?>

        <?php

    } else {
        ?>
        Order ID not specified.
        <?php
    }

    ?>

</div>

</body>

</html>