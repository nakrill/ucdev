<?php

// REQUIRE ADMIN ACCESS
$admin->requireLevel(1);

// BLANK LAYOUT
$bigtree['layout'] = 'blank';

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Client Overview</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/css/main.css" type="text/css" />
    <link rel="stylesheet" href="/admin/css/uccms.css" type="text/css" />
    <link rel="stylesheet" href="/admin/*/<?=$_uccms_ecomm->Extension?>/css/master.css" type="text/css" />

    <style type="text/css">

        body {
          -webkit-print-color-adjust: exact !important;
        }

        #print_button {
            position: absolute;
            top: 25px;
            right: 30px;
            display: block;
            padding: 10px 20px 7px;
            background-color: #eee;
            border-radius: 4px;
            font-size: 1.3em;
            text-transform: uppercase;
        }

        #print_container {
            max-width: 800px;
            min-width: 400px;
            margin: 20px auto;
        }

        #print_container h1 {
            margin: 25px 0 5px 0;
            padding: 0px;
            text-align: center;
            font-size: 1.7em;
            line-height: 1em;
            font-weight: bold;
            color: #222;
        }
        #print_container h2 {
            margin: 0px 0 35px 0;
            padding: 0px;
            text-align: center;
            font-size: 1.2em;
            line-height: 1em;
            font-weight: normal;
            color: #222;
        }
        #print_container h3 {
            margin: 0 0 8px 0;
            font-size: 1.4em;
            font-weight: bold;
            text-transform: none;
            color: #222;
        }
        #print_container h3.border-bottom {
            padding: 0 0 3px 0;
            border-bottom: 1px solid #f5f5f5;
        }
        #print_container table tr:nth-child(2n) td {
            background-color: transparent;
        }
        #print_container table td {
            padding: 7px 0;
        }
        #print_container table .group_heading td {
            border-bottom: 1px solid #ccc;
        }
        #print_container table .item td:first-child {
            padding-left: 12px;
        }
        #print_container table.charges_summary td {
            padding: 7px 5px 7px 0;
        }

        #print_container .big_split {
            margin: 20px 0;
            height: 2px;
            background-color: #eee;
        }

        #print_container .transactions {
            /*margin-top: 15px;*/
        }
        #print_container .transactions table {
            margin: 0px;
            border: 0px none;
        }
        #print_container .transactions table tr:first-child {
            background-color: #f5f5f5;
        }
        #print_container .transactions table th {
            padding: 5px 50px 5px 10px;
            background-color: transparent;
            font-size: 1em;
            font-weight: bold;
            color: #333;
            white-space: nowrap;
        }
        #print_container .transactions table th:last-child {
            padding-right: 10px;
            text-align: right;
        }
        #print_container .transactions table td {
            padding: 5px 50px 0 10px;
            white-space: nowrap;
        }
        #print_container .transactions table td:last-child {
            padding-right: 10px;
            text-align: right;
        }

        @media screen {

            .screen_hide {
                display: none !important;
            }

        }

        @media print {

            .print_hide {
                display: none !important;
            }

        }

    </style>

</head>

<body>

<a href="#" id="print_button" class="print_hide" onclick="window.print(); return false;">Print</a>

<div id="print_container">

    <?php

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // HAVE ID
    if ($id) {

        // GET ORDER INFO
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // ORDER FOUND
        if ($order['id']) {

            // GET CATERING INFO
            $catering_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_catering']. "` WHERE (`order_id`=" .$id. ")";
            $catering_q = sqlquery($catering_query);
            $catering = sqlfetch($catering_q);

            // CATERING INFO FOUND
            if ($catering['id']) {

                // GET ORDER ITEMS
                $citems = $_uccms_ecomm->cartItems($order['id']);

                // GET EXTRA INFO
                $extra = $_uccms_ecomm->cartExtra($order['id']);

                // GET BALANCE
                $balance = $_uccms_ecomm->orderBalance($order['id']);

                // CALCULATE PAID
                $paid = number_format($order['order_total'] - $balance, 2);

                // STATUS COLORS
                foreach ($_uccms_ecomm->statusColors() as $type => $colors) {
                    foreach ($colors as $status => $color) {
                        $scola[$status] = $color;
                    }
                }

                ?>

                <?php if (($catering['event_address1']) && (($catering['event_city']) || ($catering['event_state']) || ($catering['event_zip']))) { ?>
                    <iframe width="100%" height="200" frameborder="0" style="margin: 0; border: 0;" src="https://www.google.com/maps/embed/v1/place?q=<?php echo stripslashes($catering['event_address1']); if ($catering['event_address2']) { echo ',' .stripslashes($catering['event_address2']); } if ($catering['event_city']) { echo ',' .stripslashes($catering['event_city']); } if ($catering['event_state']) { echo ',' .stripslashes($catering['event_state']); } if ($catering['event_zip']) { echo ',' .stripslashes($catering['event_zip']); } ?>&key=AIzaSyDb9IKaXNmMAc8bMuRtOmmEx35cOW1HZEo" allowfullscreen></iframe>
                <?php } ?>

                <h1><?php echo trim($catering['event_location1']. ' ' .$catering['event_location2']); ?></h1>

                <h2><?php echo date('F j, Y', strtotime($catering['event_date'])); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo date('g:i A', strtotime($catering['service_time'])); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo number_format($catering['event_num_people'], 0); ?> Guests</h2>

                <table class="charges_summary" width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">
                    <tr>
                        <td width="60%" valign="top">

                            <h3>Summary of Charges</h3>

                            <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">

                                <?php

                                // SUBTOTAL
                                $order['subtotal'] = 0;

                                // ATTRIBUTE ARRAY
                                $attra = array();

                                // AFTER TAX ITEM ARRAY
                                $atia = array();

                                // CURRENT DISPLAY GROUP ARRAY
                                $cdga = array();

                                // SORT ITEMS
                                $citems = $_uccms_ecomm->stc->sortOItemsByDayTime($citems);

                                // HAVE TAX OVERRIDE
                                if ($order['override_tax']) {
                                    $tax['total'] = number_format($order['override_tax'], 2);

                                // NORMAL TAX CALCULATION
                                } else {

                                    // HAVE BILLING STATE AND/OR ZIP
                                    if (($vals['contact']['state']) || ($vals['contact']['zip'])) {
                                        $taxvals['country'] = 'United States';
                                        $taxvals['state']   = $vals['contact']['state'];
                                        $taxvals['zip']     = $vals['contact']['zip'];
                                    }

                                    //$taxvals['quote'] = false;

                                    // GET TAX
                                    $tax = $_uccms_ecomm->cartTax($order['id'], $citems, $taxvals);

                                }

                                // ORDER TAX
                                $order['tax'] = $tax['total'];

                                // LOOP THROUGH ITEMS
                                foreach ($citems as $oitem) {

                                    // AFTER TAX ITEM
                                    if ($oitem['after_tax']) {

                                        // ADD TO AFTER TAX ITEM ARRAY
                                        $atia[] = $oitem;

                                    // NORMAL ITEM
                                    } else {

                                        // GET ITEM INFO
                                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                        // DAY / TIME IS DIFFERENT
                                        if ($oitem['display_group']['day_time'] != $cdga['day_time']) {

                                            // LOOP DAY / TIME
                                            $loop_day = ($cdga['day_time'] ? date('m/d/Y', $cdga['day_time']) : '');
                                            $loop_time = ($cdga['day_time'] ? date('g:i a', $cdga['day_time']) : '');

                                            // ITEM DAY / TIME
                                            $item_day = date('m/d/Y', $oitem['display_group']['day_time']);
                                            $item_time = date('g:i a', $oitem['display_group']['day_time']);

                                            // DAY OR TIME IS DIFFERENT
                                            if (($item_day != $loop_day) || ($item_time != $loop_time)) {

                                                ?>

                                                <tr class="group_heading">
                                                    <td colspan="2">
                                                        <strong><?=$item_day?></strong> <?=$item_time?>
                                                    </td>
                                                </tr>

                                                <?php

                                                if ($item_day != $loop_day) {
                                                    $loop_time = '';
                                                }

                                            }

                                        }

                                        // HAVE CATEGORY AND IS DIFFERENT
                                        if (($oitem['display_group']['category']) && ($oitem['display_group']['category'] != $cdga['category'])) {

                                            // DON'T HAVE CATEGORY INFO YET
                                            if (!$dgca[$oitem['display_group']['category']]) {

                                                // GET CATEGORY INFO
                                                $dgc_query = "SELECT * FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`id`=" .(int)$oitem['display_group']['category']. ")";
                                                $dgc_q = sqlquery($dgc_query);
                                                $dgca[$oitem['display_group']['category']] = sqlfetch($dgc_q);

                                            }

                                            ?>

                                            <tr class="group_heading2">
                                                <td colspan="2">
                                                    <strong><?php echo stripslashes($dgca[$oitem['display_group']['category']]['title']); ?></strong>
                                                </td>
                                            </tr>

                                            <?php

                                        }

                                        ?>

                                        <tr class="item">
                                            <td width="100%">
                                                <i class="fa fa-fw fa-cutlery"></i>&nbsp;&nbsp;<?php echo stripslashes($item['title']); ?>
                                            </td>
                                            <td style="text-align: right; white-space: nowrap;">
                                                <?php echo $item['other']['total_formatted']; ?>
                                            </td>
                                        </tr>

                                        <?php

                                        // SET CURRENT DISPLAY GROUP
                                        $cdga = $oitem['display_group'];

                                    }

                                }

                                ?>

                                <tr style="background-color: #f5f5f5; border: 1px solid #e0e0e0;">
                                    <td width="100%">
                                        <i class="fa fa-fw fa-plus-square-o"></i>&nbsp;&nbsp;Subtotal
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<span class="num"><?php echo number_format($order['subtotal'], 2); ?>
                                    </td>
                                </tr>

                                <?php if ($order['discount']) { ?>
                                    <tr>
                                        <td width="100%">
                                            Discount:
                                        </td>
                                        <td style="text-align: right; white-space: nowrap;">
                                            $<?php echo number_format($order['discount'], 2); ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <tr>
                                    <td width="100%">
                                        <i class="fa fa-fw fa-usd"></i>&nbsp;&nbsp;Tax
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<span class="num"><?php if ($order['override_tax']) { number_format($order['override_tax'], 2); } else { echo number_format($order['tax'], 2); } ?></span>
                                    </td>
                                </tr>

                                <?php

                                // HAVE AFTER TAX ITEMS
                                if (count($atia) > 0) {

                                    // LOOP
                                    foreach ($atia as $oitem) {

                                        // GET ITEM INFO
                                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                        ?>

                                        <tr>
                                            <td width="100%">
                                                <i class="fa fa-fw fa-cutlery"></i>&nbsp;&nbsp;<?php echo stripslashes($item['title']); ?>
                                            </td>
                                            <td style="text-align: right; white-space: nowrap;">
                                                <?php echo $item['other']['total_formatted']; ?>
                                            </td>
                                        </tr>

                                        <?php

                                    }

                                }

                                ?>

                                <? /*
                                <tr>
                                    <td width="100%">
                                        <i class="fa fa-fw fa-cutlery"></i>&nbsp;&nbsp;Shipping
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<span class="num"><?php echo number_format($order['order_shipping'], 2); ?></span>
                                    </td>
                                </tr>
                                */ ?>

                                <?php if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
                                    $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);
                                    ?>
                                    <tr>
                                        <td width="100%">
                                            <i class="fa fa-fw fa-usd"></i>&nbsp;&nbsp;Gratuity (<?php echo ($order['override_gratuity'] ? $order['override_gratuity'] : $_uccms_ecomm->getSetting('gratuity_percent')); ?>%)
                                        </td>
                                        <td style="text-align: right; white-space: nowrap;">
                                            $<?php echo number_format($order['gratuity'], 2); ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <tr style="background-color: #f5f5f5; border: 1px solid #e0e0e0;">
                                    <td width="100%">
                                        <i class="fa fa-fw fa-check"></i>&nbsp;&nbsp;Total
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        $<span class="num"><?php echo number_format($order['order_total'], 2); ?></span>
                                    </td>
                                </tr>

                                <? /*
                                <div style="padding-top: 10px; text-align: right;">
                                    <div style="padding-bottom: 10px; font-size: 1.2em;">
                                        Paid: $<?=$paid?>
                                    </div>
                                    <div style="font-size: 1.4em;">
                                        Balance: $<?=$balance?>
                                    </div>
                                </div>
                                */ ?>

                            </table>

                        </td>
                        <td width="40%" valign="top" style="padding-left: 50px;">

                            <h3 class="border-bottom">Event Summary</h3>
                            <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px; margin-bottom: 20px;">
                                <tr>
                                    <td width="100%">
                                        <i class="fa fa-fw fa-calendar-check-o"></i>&nbsp;&nbsp;Date
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        <?php echo date('M j, Y', strtotime($catering['event_date'])); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%">
                                        <i class="fa fa-fw fa-clock-o"></i>&nbsp;&nbsp;Start Time
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        <?php echo date('g:i A', strtotime($catering['service_time'])); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%">
                                        <i class="fa fa-fw fa-users"></i>&nbsp;&nbsp;Guests
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        <?php echo number_format($catering['event_num_people'], 0); ?>
                                    </td>
                                </tr>
                            </table>

                            <h3 class="border-bottom">Event Contact</h3>
                            <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">
                                <tr>
                                    <td width="100%">
                                        <i class="fa fa-fw fa-phone"></i>&nbsp;&nbsp;<?php echo trim(stripslashes($catering['contact_firstname']). ' ' .stripslashes($catering['contact_lastname'])); ?>
                                    </td>
                                    <td style="text-align: right; white-space: nowrap;">
                                        <?php echo stripslashes($catering['contact_phone']); ?>
                                    </td>
                                </tr>
                                <?php if ($catering['contact_company']) { ?>
                                    <tr>
                                        <td colspan="2" width="100%">
                                            <i class="fa fa-fw fa-building"></i>&nbsp;&nbsp;<?php echo stripslashes($catering['contact_company']); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if ($catering['contact_email']) { ?>
                                    <tr>
                                        <td colspan="2" width="100%">
                                            <i class="fa fa-fw fa-envelope-o"></i>&nbsp;&nbsp;<?php echo stripslashes($catering['contact_email']); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>

                        </td>
                    </tr>
                </table>

                <div class="big_split"></div>

                <h3>Notes</h3>

                <?php if ($catering['event_notes']) { ?>
                    <div style="padding-bottom: 10px;">
                        <?php echo nl2br(stripslashes($catering['event_notes'])); ?>
                    </div>
                <?php } ?>

                <div>
                    <textarea style="width: 100%; height: 100px; padding: 10px; border: 1px solid #f5f5f5;"></textarea>
                </div>

                <div class="big_split"></div>

                <? /*
                <div class="status_container" style="padding: 2px; background-color: <?php echo $scola[$order['status']]; ?>;">
                    <div style="padding: 5px 13px; background-color: #fff; opacity: 0.9;">
                        <table width="100%" cellpadding="0" cellspacing="0" style="margin: 0px; border: 0px;">
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap;">
                                    <i class="fa fa-check" style="font-size: 2em;"></i>
                                </td>
                                <td width="100%" style="padding-left: 20px;">
                                    <?php if ($order['status'] == 'preparing') { ?>
                                        Your quote is being prepared.
                                    <?php } else { ?>
                                        Your quote has been <?php echo $order['status']; ?>.
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                */ ?>

                <?php

                // GET TRANSACTION LOG
                $translog_query = "SELECT * FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`cart_id`='" .stripslashes($order['id']). "') ORDER BY `dt` DESC";
                $translog_q = sqlquery($translog_query);

                // HAVE TRANSACTIONS
                if (sqlrows($translog_q) > 0) {

                    ?>

                    <h3>Payments</h3>

                    <div class="transactions">

                        <table>
                            <tr>
                                <th>Date</th>
                                <th style="width: 100%;">Status</th>
                                <th>Amount</th>
                            </tr>
                            <?php while ($transaction = sqlfetch($translog_q)) { ?>
                                <tr>
                                    <td><?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?></td>
                                    <td><?php echo ucwords($transaction['status']); ?></td>
                                    <td>$<?php echo number_format($transaction['amount'], 2); ?></td>
                                </tr>
                            <?php } ?>
                        </table>

                    </div>

                    <?php
                }

                ?>

                <div class="big_split"></div>

                <div class="signature" style="padding-top: 10px; font-size: 1.1em; font-weight: bold;">
                    Signature: _______________________________________________ &nbsp;&nbsp;&nbsp; Date: __________________
                </div>

                <?php

            // CATERING NOT FOUND
            } else {
                ?>
                Catering info not found.
                <?php
            }

        // ORDER NOT FOUND
        } else {
            ?>
            Order not found.
            <?php
        }

        ?>

        <?php

    } else {
        ?>
        Order ID not specified.
        <?php
    }

    ?>

</div>

</body>

</html>