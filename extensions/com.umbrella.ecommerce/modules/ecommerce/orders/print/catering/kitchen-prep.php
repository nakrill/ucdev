<?php

// REQUIRE ADMIN ACCESS
$admin->requireLevel(1);

// BLANK LAYOUT
$bigtree['layout'] = 'blank';

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Kitchen Prep</title>

    <link rel="stylesheet" href="/admin/css/main.css" type="text/css" />
    <link rel="stylesheet" href="/admin/css/uccms.css" type="text/css" />
    <link rel="stylesheet" href="/admin/*/<?=$_uccms_ecomm->Extension?>/css/master.css" type="text/css" />
    <link rel="stylesheet" href="/extensions/<?=$_uccms_ecomm->Extension?>/css/master/quote_request.css" type="text/css" />

    <style type="text/css">

        #print_button {
            position: absolute;
            top: 25px;
            right: 30px;
            display: block;
            padding: 10px 20px 7px;
            background-color: #eee;
            border-radius: 4px;
            font-size: 1.3em;
            text-transform: uppercase;
        }

        #print_container {
            max-width: 800px;
            min-width: 400px;
            margin: 20px auto;
        }

        #ecommContainer #review .order_id {
            padding-bottom: 15px;
            font-size: 1.5em;
        }

        #ecommContainer #review .details {
            margin-top: 0px;
            padding-top: 0px;
            border: 0px none;
        }
        #ecommContainer #review .details .left {
            float: left;
            width: 33.33%;
        }
        #ecommContainer #review .details table {
            border: 0px none;
        }
        #ecommContainer #review .details td {
            padding: 4px 0;
            color: inherit;
        }
        #ecommContainer #review .details table tr:nth-child(2n) td {
            background-color: #fff;
        }
        #ecommContainer .cart {
            padding-bottom: 0px;
        }

        #ecommContainer #cart .thumb {
            width: 15%;
            text-align: center;
        }
        #ecommContainer #cart .title {
            width: 70%;
            text-align: left;
        }
        #ecommContainer #cart .title .options {
            margin: 5px 0 0 25px;
        }
        #ecommContainer #cart .title .options li {
            font-size: .8em;
            line-height: 1.2em;
        }
        #ecommContainer #cart .quantity {
            width: 15%;
            text-align: center;
        }

        #ecommContainer #cart .thumb img {
            max-width: 60px;
        }

        @media screen {

            .screen_hide {
                display: none !important;
            }

        }

        @media print {

            .print_hide {
                display: none !important;
            }

        }

    </style>

</head>

<body>

<a href="#" id="print_button" class="print_hide" onclick="window.print(); return false;">Print</a>

<div id="print_container">

    <?php

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // HAVE ID
    if ($id) {

        // GET ORDER INFO
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // ORDER FOUND
        if ($order['id']) {

            // GET AND SORT ITEMS BY DAY AND TIME
            $citems = $_uccms_ecomm->stc->sortOItemsByDayTime($_uccms_ecomm->cartItems($order['id']));

            // NUMBER OF ITEMS
            $num_items = count($citems);

            // GET EXTRA INFO
            $extra = $_uccms_ecomm->cartExtra($order['id']);

            ?>

            <div id="ecommContainer">
                <div id="review">

                    <?php

                    // HAVE ITEMS
                    if ($num_items > 0) {

                        // ATTRIBUTE ARRAY
                        $attra = array();

                        // CURRENT DISPLAY GROUP ARRAY
                        $cdga = array();

                        $ii = 1;

                        // LOOP THROUGH ITEMS
                        foreach ($citems as $oitem) {

                            // GET ITEM INFO
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                            // IS PRODUCT
                            //if ($item['type'] == 0) {

                                // DAY / TIME IS DIFFERENT
                                if ($oitem['display_group']['day_time'] != $cdga['day_time']) {

                                    // LOOP DAY / TIME
                                    $loop_day = ($cdga['day_time'] ? date('m/d/Y', $cdga['day_time']) : '');
                                    $loop_time = ($cdga['day_time'] ? date('g:i a', $cdga['day_time']) : '');

                                    // ITEM DAY / TIME
                                    $item_day = date('m/d/Y', $oitem['display_group']['day_time']);
                                    $item_time = date('g:i a', $oitem['display_group']['day_time']);

                                    // DAY OR TIME IS DIFFERENT
                                    if (($item_day != $loop_day) || ($item_time != $loop_time)) {

                                        // IS NOT FIRST ONE
                                        if ($ii != 1) {
                                            ?>

                                                    </table>
                                                </div>

                                                <?php if ($order['notes']) { ?>
                                                    <div class="screen_hide" style="margin-bottom: 20px;">
                                                        <div style="font-weight: bold;">Notes</div>
                                                        <div style="padding: 10px 15px; border: 1px solid #ccc;">
                                                            <?php echo nl2br(stripslashes($order['notes'])); ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                            </div>

                                            <?php
                                        }

                                        ?>

                                        <div style="page-break-after: always;">

                                            <div class="order_id <?php if ($ii > 1) { ?>screen_hide<?php } ?>">
                                                Order ID: <?php echo $order['id']; ?>
                                            </div>

                                            <div class="details contain <?php if ($ii > 1) { ?>screen_hide<?php } ?>">

                                                <div class="left contact">
                                                    <h4>Contact</h4>
                                                    <div class="name">
                                                        <?php echo stripslashes($extra['contact_firstname']); ?> <?php echo stripslashes($extra['contact_lastname']); ?>
                                                    </div>
                                                    <?php if ($extra['contact_company']) { ?>
                                                        <div class="company">
                                                            <?php echo stripslashes($extra['contact_company']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($extra['contact_address1']) { ?>
                                                        <div class="address1">
                                                            <?php echo stripslashes($extra['contact_address1']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($extra['contact_address2']) { ?>
                                                        <div class="address2">
                                                            <?php echo stripslashes($extra['contact_address2']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="csz">
                                                        <?php echo stripslashes($extra['contact_city']); ?>, <?php echo stripslashes($extra['contact_state']); ?> <?php echo stripslashes($extra['contact_zip']); ?>
                                                    </div>
                                                    <?php if ($extra['contact_phone']) { ?>
                                                        <div class="phone">
                                                            <?php echo stripslashes($extra['contact_phone']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($extra['contact_email']) { ?>
                                                        <div class="email">
                                                            <a href="mailto:<?php echo stripslashes($extra['contact_email']); ?>" target="_blank"><?php echo stripslashes($extra['contact_email']); ?></a>
                                                        </div>
                                                    <?php } ?>
                                                </div>

                                                <div class="left delivery">
                                                    <h4>Delivery</h4>
                                                    <?php if ($extra['event_location1']) { ?>
                                                        <div class="location1">
                                                            <?php echo stripslashes($extra['event_location1']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($extra['event_location2']) { ?>
                                                        <div class="location2">
                                                            <?php echo stripslashes($extra['event_location2']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($extra['event_address1']) { ?>
                                                        <div class="address1">
                                                            <?php echo stripslashes($extra['event_address1']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($extra['event_address2']) { ?>
                                                        <div class="address2">
                                                            <?php echo stripslashes($extra['event_address2']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="csz">
                                                        <?php echo stripslashes($extra['event_city']); ?>, <?php echo stripslashes($extra['event_state']); ?> <?php echo stripslashes($extra['event_zip']); ?>
                                                    </div>
                                                    <?php if ($extra['event_phone']) { ?>
                                                        <div class="phone">
                                                            <?php echo stripslashes($extra['event_phone']); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($extra['event_email']) { ?>
                                                        <div class="email">
                                                            <a href="mailto:<?php echo stripslashes($extra['event_email']); ?>" target="_blank"><?php echo stripslashes($extra['event_email']); ?></a>
                                                        </div>
                                                    <?php } ?>
                                                </div>

                                                <div class="left event">
                                                    <h4>Event</h4>
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr class="date">
                                                            <td>Date:&nbsp;</td>
                                                            <td><?php echo date('n/j/Y', strtotime($extra['event_date'])); ?></td>
                                                        </tr>
                                                        <tr class="time">
                                                            <td>Service Time:&nbsp;</td>
                                                            <td><?php echo stripslashes($extra['service_time']); ?></td>
                                                        </tr>
                                                        <tr class="type">
                                                            <td>Service Type:&nbsp;</td>
                                                            <td><?php
                                                                switch ($extra['service_type']) {
                                                                    case 'catering_event':
                                                                        echo 'Event Catering';
                                                                        break;
                                                                    case 'catering_wedding':
                                                                        echo 'Wedding Catering';
                                                                        break;
                                                                    case 'chef':
                                                                        echo 'Chef';
                                                                        break;
                                                                    case 'beverage':
                                                                        echo 'Beverage';
                                                                        break;
                                                                    case 'pickup':
                                                                        echo 'Pickup';
                                                                        break;
                                                                    case 'dropoff':
                                                                        echo 'Dropoff';
                                                                        break;
                                                                    case 'dropoff_hot':
                                                                        echo 'Dropoff - Hot';
                                                                        break;
                                                                    case 'in_house':
                                                                        echo 'In-House';
                                                                        break;
                                                                    case 'menu_tasting':
                                                                        echo 'Menu Tasting';
                                                                        break;
                                                                }
                                                             ?></td>
                                                        </tr>
                                                        <tr class="num_people">
                                                            <td># People:&nbsp;</td>
                                                            <td><?php echo stripslashes($extra['event_num_people']); ?></td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>

                                            <div style="clear: both; height: 0px; overflow: hidden;"></div>

                                            <?php if (($order['notes']) && ($ii == 1)) { ?>
                                                <div class="print_hide" style="margin-bottom: 20px;">
                                                    <div style="font-weight: bold;">Notes</div>
                                                    <div style="padding: 10px 15px; border: 1px solid #ccc;">
                                                        <?php echo nl2br(stripslashes($order['notes'])); ?>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <div id="cart">

                                                <div style="font-size: 1.2em; padding-bottom: 10px;">
                                                    <strong><?=$item_day?></strong> <?=$item_time?>
                                                </div>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <th class="thumb">Picture</th>
                                                        <th class="title">Item</th>
                                                        <th class="quantity">Quantity</th>
                                                    </tr>
                                                <?php

                                                if ($item_day != $loop_day) {
                                                    $loop_time = '';
                                                }

                                    }

                                    // SET CURRENT DISPLAY GROUP
                                    $display_group = $oitem['display_group'];

                                }

                                ?>

                                <tr id="item-<?php echo $oitem['id']; ?>" class="item">
                                    <td class="thumb">
                                        <img src="<?php echo $item['other']['image_url']; ?>" alt="<?php echo stripslashes($item['title']); ?>" />
                                    </td>
                                    <td class="title">
                                        <?php echo stripslashes($item['title']); ?>
                                        <?php if (count($item['other']['options']) > 0) { ?>
                                            <ul class="options">
                                                <?php

                                                // IS PACKAGE AND HAVE ITEMS
                                                if (($item['type'] == 3) && (count($item['package']['items']) > 0)) {

                                                    // LOOP THROUGH ITEMS
                                                    foreach ($item['package']['items'] as $pitem_id => $pitem) {

                                                        // GET SELECTED ATTRIBUTES / OPTIONS
                                                        $pitem_attrs = $_uccms_ecomm->item_attrAndOptionTitles($pitem, $pitem['other']['attribute_titles'], $attr_options);

                                                        ?>

                                                        <li><?php echo stripslashes($pitem['item']['title']); if ($pitem['quantity'] > 1) { echo ' (' .number_format($pitem['quantity'], 0). ')'; } ?>
                                                            <?php if (count($pitem_attrs) > 0) { ?>
                                                                <ul>
                                                                    <?php foreach ($pitem_attrs as $attr) { ?>
                                                                        <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } ?>
                                                        </li>

                                                        <?php

                                                    }

                                                // OTHER TYPES
                                                } else {
                                                    if (count($item['other']['options']) > 0) {
                                                        foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra, $attr_options) as $attr) {
                                                            ?>
                                                            <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>
                                            </ul>
                                        <?php } ?>
                                    </td>
                                    <td class="quantity">
                                        <?php echo number_format($oitem['quantity'], 0); ?>
                                    </td>
                                </tr>

                                <?php

                                $ii++;

                                // SET CURRENT DISPLAY GROUP
                                $cdga = $oitem['display_group'];

                            //}

                        }

                                    ?>

                                </table>
                            </div>

                            <?php if ($order['notes']) { ?>
                                <div class="screen_hide">
                                    <div style="font-weight: bold;">Notes</div>
                                    <div style="padding: 10px 15px; border: 1px solid #ccc;">
                                        <?php echo nl2br(stripslashes($order['notes'])); ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                        <?php

                    // NO ITEMS
                    } else {
                        ?>
                        Order has no items.
                        <?php
                    }

                    ?>

                </div>
            </div>

            <?php

        // ORDER NOT FOUND
        } else {
            ?>
            Order not found.
            <?php
        }

        ?>

        <?php

    } else {
        ?>
        Order ID not specified.
        <?php
    }

    ?>

</div>

</body>

</html>