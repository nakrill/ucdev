<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['id'];

    // HAVE ORDER ID
    if ($id) {

        // GET ORDER INFO
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // ORDER FOUND
        if ($order['id']) {

            // CALCULATE GRATUITY
            if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
                if ($_POST['order']['override_gratuity']) $order['override_gratuity'] = (float)$_POST['order']['override_gratuity'];
                $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);
            }

            // HAVE TAX OVERRIDE
            if ($order['override_tax']) {
                $order_tax = number_format($order['override_tax'], 2);

            // NORMAL TAX CALCULATION
            } else {

                // GET TAX
                $tax = $_uccms_ecomm->cartTax($order['id']);

                $order_tax = $tax['total'];

            }

            // CUSTOMER ID
            if (
                ((!$_POST['current_customer_id']) && ($_POST['customer_id'])) // NO PREVIOUS CUSTOMER ID, NOW HAS CUSTOMER ID
                ||
                ((($_POST['current_customer_id']) && ($_POST['customer_id'])) && ($_POST['current_customer_id'] != $_POST['customer_id'])) // CHANGING CUSTOMER ID
            ) {

                // GET LATEST ORDER
                $last_order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .(int)$_POST['customer_id']. ") AND (`status`!='cart') ORDER BY `dt` DESC LIMIT 1";
                $last_order_q = sqlquery($last_order_query);
                $last_order = sqlfetch($last_order_q);

                // LAST ORDER FOUND
                if ($last_order['id']) {

                    $_POST['billing_firstname']   = stripslashes($last_order['billing_firstname']);
                    $_POST['billing_lastname']    = stripslashes($last_order['billing_lastname']);
                    $_POST['billing_company']     = stripslashes($last_order['billing_company']);
                    $_POST['billing_address1']    = stripslashes($last_order['billing_address1']);
                    $_POST['billing_address2']    = stripslashes($last_order['billing_address2']);
                    $_POST['billing_city']        = stripslashes($last_order['billing_city']);
                    $_POST['billing_state']       = stripslashes($last_order['billing_state']);
                    $_POST['billing_zip']         = stripslashes($last_order['billing_zip']);
                    $_POST['billing_phone']       = stripslashes($last_order['billing_phone']);
                    $_POST['billing_email']       = stripslashes($last_order['billing_email']);
                    $_POST['shipping_same']       = (int)$last_order['shipping_same'];
                    $_POST['shipping_firstname']  = stripslashes($last_order['shipping_firstname']);
                    $_POST['shipping_lastname']   = stripslashes($last_order['shipping_lastname']);
                    $_POST['shipping_company']    = stripslashes($last_order['shipping_company']);
                    $_POST['shipping_address1']   = stripslashes($last_order['shipping_address1']);
                    $_POST['shipping_address2']   = stripslashes($last_order['shipping_address2']);
                    $_POST['shipping_city']       = stripslashes($last_order['shipping_city']);
                    $_POST['shipping_state']      = stripslashes($last_order['shipping_state']);
                    $_POST['shipping_zip']        = stripslashes($last_order['shipping_zip']);
                    $_POST['shipping_phone']      = stripslashes($last_order['shipping_phone']);
                    $_POST['shipping_email']      = stripslashes($last_order['shipping_email']);

                    if (!is_array($_POST['contact'])) $_POST['contact'] = array();
                    if (!is_array($_POST['event'])) $_POST['event'] = array();

                    $_POST['contact']   = array_merge($_POST['contact'], $_POST['billing']);
                    $_POST['event']     = array_merge($_POST['event'], $_POST['shipping']);

                }

            // HAD CUSTOMER ID, NOW SET TO NONE
            } else if (($_POST['current_customer_id']) && (!$_POST['customer_id'])) {
                $customer_id = 0;
            }

            // DATABASE COLUMNS
            $columns = array(
                'status'                => trim(strtolower($_POST['order']['status'])),
                'billing_firstname'     => $_POST['billing_firstname'],
                'billing_lastname'      => $_POST['billing_lastname'],
                'billing_company'       => $_POST['billing_company'],
                'billing_address1'      => $_POST['billing_address1'],
                'billing_address2'      => $_POST['billing_address2'],
                'billing_city'          => $_POST['billing_city'],
                'billing_state'         => $_POST['billing_state'],
                'billing_zip'           => $_POST['billing_zip'],
                'billing_country'       => '',
                'billing_phone'         => $_POST['billing_phone'],
                'billing_email'         => $_POST['billing_email'],
                'shipping_same'         => (int)$_POST['shipping_same'],
                'shipping_firstname'    => $_POST['shipping_firstname'],
                'shipping_lastname'     => $_POST['shipping_lastname'],
                'shipping_company'      => $_POST['shipping_company'],
                'shipping_address1'     => $_POST['shipping_address1'],
                'shipping_address2'     => $_POST['shipping_address2'],
                'shipping_city'         => $_POST['shipping_city'],
                'shipping_state'        => $_POST['shipping_state'],
                'shipping_zip'          => $_POST['shipping_zip'],
                'shipping_country'      => '',
                'shipping_phone'        => $_POST['shipping_phone'],
                'shipping_email'        => $_POST['shipping_email'],
                'notes'                 => $_POST['notes'],
                'override_tax'          => ($_POST['order']['override_tax'] != '') ? number_format((float)$_POST['order']['override_tax'], 2) : '',
                'override_shipping'     => ($_POST['order']['override_shipping'] !== '') ? number_format((float)$_POST['order']['override_shipping'], 2) : '',
                'override_gratuity'     => ($_POST['order']['override_gratuity'] !== '') ? (int)$_POST['order']['override_gratuity'] : '',
                'gratuity_type'         => ($_POST['order']['gratuity_type'] ? $_POST['order']['gratuity_type'] : 'percent'),
                'order_gratuity'        => $order['gratuity'],
                'order_tax'             => $order_tax,
                'order_total'           => $_uccms_ecomm->orderTotal($id)
            );

            // CUSTOMER (ACCOUNT) ID SPECIFIED
            if ($_POST['customer_id']) {
                $customer_id = (int)$_POST['customer_id'];

            // NO CUSTOMER ID SPECIFIED
            } else {

                // IS CATERING
                if ($_uccms_ecomm->storeType() == 'catering') {
                    $account_info = [
                        'firstname' => $_POST['contact']['firstname'],
                        'lastname'  => $_POST['contact']['lastname'],
                        'email'     => $_POST['contact']['email'],
                        'password'  => $_POST['account']['password'],
                    ];
                // IS GENERAL
                } else {
                    $account_info = [
                        'firstname' => $columns['billing_firstname'],
                        'lastname'  => $columns['billing_lastname'],
                        'email'     => $columns['billing_email'],
                        'password'  => $_POST['account']['password'],
                    ];
                }

                // SAVE CUSTOMER
                $customer = $_uccms_ecomm->orderSaveCustomer(array_merge($columns, [
                    'account' => $account_info
                ]));
                $customer_id = $customer['id'];

            }

            // ORDER HASH
            if ($order['hash']) {
                $order_hash = $order['hash'];
            } else {
                $order_hash = $_uccms_ecomm->generateOrderHash($customer_id);
            }

            $columns['hash'] = $order_hash;
            $columns['customer_id'] = $customer_id;

            if ($_uccms_ecomm->getSetting('checkout_legal_enabled')) {
                $columns['legal_agree'] = (int)$_POST['legal_agree'];
            }

            // DB QUERY
            $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$id. ")";

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {

                $admin->growl('Order', 'Order updated!');

                // RECALCULATE ORDER
                $_uccms_ecomm->orderRecalculate($id);

            } else {
                $admin->growl('Order', 'Failed to update order.');
            }

            // PROCESS EXTRA
            @include(dirname(__FILE__). '/processes/store_types/' .$_uccms_ecomm->storeType(). '.php');

        // ORDER NOT FOUND
        } else {
            $admin->growl('Order', 'Order not found.');
        }

    // NO ORDER ID
    } else {
        $admin->growl('Order', 'Order ID not specified.');
    }

}

//exit;

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$id);

?>