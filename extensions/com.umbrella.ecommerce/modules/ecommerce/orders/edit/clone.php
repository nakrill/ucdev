<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        unset($order['id']);

        // ENTIRE ORDER
        if ($_REQUEST['what'] == 'entire') {


        // CART ONLY
        } else {

            // KEEP THESE FIELDS
            $keep = array(
                'quote'             => $order['quote'],
                'shipping_3rdparty' => $order['shipping_3rdparty'],
                'shipping_carrier'  => $order['shipping_carrier'],
                'shipping_service'  => $order['shipping_service']
            );

            // REMOVE FIELDS
            foreach ($order as $key => $value) {
                if ((!stristr($key, 'override_')) && (!stristr($key, 'order_'))) {
                    unset($order[$key]);
                }
            }

            unset($order['order_shipping']);

            // MERGE KEPT FIELDS BACK IN
            $order = array_merge($order, $keep);

            $order['ip']            = 0;
            $order['customer_id']   = 0;

            $tax = $_uccms_ecomm->cartTax($id);
            $order['order_tax']     = $tax['total'];

        }

        $order['hash']          = $_uccms_ecomm->generateOrderHash($order['billing_email']);
        $order['status']        = 'cloned';
        $order['dt']            = date('Y-m-d H:i:s');
        $order['order_total']   = $_uccms_ecomm->orderTotal($id);
        $order['paid_in_full']  = 0;

        if ($_REQUEST['convert']) {
            if ($_REQUEST['type'] == 'quote') {
                $order['quote'] = 2;
            } else {
                $order['quote'] = 1;
            }
        }

        // CREATE NEW ENTRY
        $new_order_query = "INSERT INTO `" .$_uccms_ecomm->tables['orders']. "` SET " .uccms_createSet($order);

        // CREATED
        if (sqlquery($new_order_query)) {

            // NEW ORDER ID
            $order['id'] = sqlid();

            // GET ORDER ITEMS
            $items_query = "SELECT * FROM `" .$_uccms_ecomm->tables['order_items']. "` WHERE (`cart_id`=" .$id. ")";
            $items_q = sqlquery($items_query);

            // LOOP
            while ($item = sqlfetch($items_q)) {

                unset($item['id']);

                $item['cart_id']    = $order['id'];
                $item['dt']         = date('Y-m-d H:i:s');

                // CREATE NEW ITEM
                $new_item_query = "INSERT INTO `" .$_uccms_ecomm->tables['order_items']. "` SET " .uccms_createSet($item);
                sqlquery($new_item_query);

            }

            // ENTIRE ORDER
            if ($_REQUEST['what'] == 'entire') {

                // QUOTE DETAILS ARRAY
                $qda = array(
                    'general',
                    'catering'
                );

                // LOOP THROUGH STORE TYPES
                foreach ($qda as $type) {

                    unset($details);

                    // GET QUOTE DETAILS (GENERAL)
                    $details_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_' .$type]. "` WHERE (`order_id`=" .$id. ")";
                    $details_q = sqlquery($details_query);
                    $details = sqlfetch($details_q);

                    // DETAILS FOUND
                    if ($details['id']) {

                        unset($details['id']);

                        $details['order_id'] = $order['id'];

                        // CREATE NEW DETAILS
                        $new_details_query = "INSERT INTO `" .$_uccms_ecomm->tables['quote_' .$type]. "` SET " .uccms_createSet($details);
                        sqlquery($new_details_query);

                    }

                }

            // CART ONLY
            } else {

                $details = array(
                    'order_id'  => $order['id']
                );

                // CREATE NEW DETAILS
                $new_details_query = "INSERT INTO `" .$_uccms_ecomm->tables['quote_' .$_uccms_ecomm->storeType()]. "` SET " .uccms_createSet($details);
                sqlquery($new_details_query);

            }

            if ($_REQUEST['type'] == 'quote') {
                 if ($_REQUEST['convert']) {
                    $admin->growl('Clone Quote', 'Quote copied and converted! **This is the new order.**');
                    BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order['id']);
                 } else {
                    $admin->growl('Clone Quote', 'Quote copied! **This is the new quote.**');
                    BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$order['id']);
                 }
            } else {
                if ($_REQUEST['convert']) {
                    $admin->growl('Clone Order', 'Order copied and converted! **This is the new quote.**');
                    BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$order['id']);
                } else {
                    $admin->growl('Clone Order', 'Order copied! **This is the new order.**');
                    BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order['id']);
                }
            }

        }

    // ORDER NOT FOUND
    } else {

        if ($_REQUEST['type'] == 'quote') {
            $admin->growl('Clone Quote', 'Quote not found.');
            BigTree::redirect(MODULE_ROOT. 'quotes/');
        } else {
            $admin->growl('Clone Order', 'Order not found.');
            BigTree::redirect(MODULE_ROOT. 'orders/');
        }

    }

// ID NOT SPECIFIED
} else {

    if ($_REQUEST['type'] == 'quote') {
        $admin->growl('Clone Quote', 'Quote ID not specified.');
        BigTree::redirect(MODULE_ROOT. 'quote/');
    } else {
        $admin->growl('Clone Order', 'Order ID not specified.');
        BigTree::redirect(MODULE_ROOT. 'orders/');
    }

}

?>