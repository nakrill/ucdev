<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        $columns = array(
            'quote'     => 1,
            'status'    => 'preparing',
        );

        // UPDATE
        $update_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$order['id']. ")";
        if (sqlquery($update_query)) {
            $admin->growl('Success', 'Order is now a quote.');
            BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$order['id']);
        } else {
            $admin->growl('Failed', 'Failed to turn order into quote.');
            BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order['id']);
        }

    } else {
        $admin->growl('Failed', 'Order not found.');
        BigTree::redirect(MODULE_ROOT. 'orders/');
    }

// ID NOT SPECIFIED
} else {
    $admin->growl('Failed', 'Order ID not specified.');
    BigTree::redirect(MODULE_ROOT. 'orders/');
}

?>