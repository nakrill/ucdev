<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['id'];

    // HAVE TRANSACTION ID
    if ($id) {

        // DB COLUMNS
        $columns = array(
            'dt'                => date('Y-m-d H:i:s', strtotime($_POST['dt'])),
            'status'            => $_POST['status'],
            'amount'            => number_format((float)str_replace(',', '', $_POST['amount']), 2, '.', ''),
            'method'            => $_POST['method'],
            'note'              => $_POST['note']
        );

        // UPDATE DB
        $update_query = "UPDATE `" .$_uccms_ecomm->tables['transaction_log']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$id. ")";
        if (sqlquery($update_query)) {

            $admin->growl('Payment Transaction', 'Transaction updated.');

        // FAILED
        } else {
            $admin->growl('Payment Transaction', 'Failed to update transaction.');
        }

    // NO ORDER ID
    } else {
        $admin->growl('Payment Transaction', 'ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$_POST['order_id']);

?>