<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $order_id = (int)$_POST['order_id'];

    // HAVE ORDER ID
    if ($order_id) {

        // CLEAN UP
        $amount             = $_uccms_ecomm->toFloat($_POST['amount']);
        $payment_method     = sqlescape(trim($_POST['payment']['method']));

        // HAVE PAYMENT METHOD
        if ($payment_method) {

            // GET PAYMENT METHOD INFO
            $pmethod_query = "SELECT * FROM `" .$_uccms_ecomm->tables['payment_methods']. "` WHERE (`id`='" .$payment_method. "') AND (`active`=1)";
            $pmethod_q = sqlquery($pmethod_query);
            $pmethod = sqlfetch($pmethod_q);

            // NOT FOUND
            if (!$pmethod['id']) {
                $payment_method = '';
            }

        }

        // HAVE REQUIRED INFO
        if (($amount > 0.00) && ($payment_method)) {

            // GET ORDER INFO
            $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$order_id. ")";
            $order_q = sqlquery($order_query);
            $order = sqlfetch($order_q);

            // ORDER FOUND
            if ($order['id']) {

                // ORDER DATA FOR REVSOCIAL API
                $rs_orderdata = array(
                    'billing'   => array(
                        'firstname'     => stripslashes($order['billing_firstname']),
                        'lastname'      => stripslashes($order['billing_lastname']),
                        'company'       => stripslashes($order['billing_company']),
                        'address1'      => stripslashes($order['billing_address1']),
                        'address2'      => stripslashes($order['billing_address2']),
                        'city'          => stripslashes($order['billing_city']),
                        'state'         => stripslashes($order['billing_state']),
                        'zip'           => stripslashes($order['billing_zip']),
                        'country'       => stripslashes($order['billing_country']),
                        'phone'         => stripslashes($order['billing_phone']),
                        'email'         => stripslashes($order['billing_email'])
                    ),
                    'shipping'  => array(
                        'firstname'     => stripslashes($order['shipping_firstname']),
                        'lastname'      => stripslashes($order['shipping_lastname']),
                        'company'       => stripslashes($order['shipping_company']),
                        'address1'      => stripslashes($order['shipping_address1']),
                        'address2'      => stripslashes($order['shipping_address2']),
                        'city'          => stripslashes($order['shipping_city']),
                        'state'         => stripslashes($order['shipping_state']),
                        'zip'           => stripslashes($order['shipping_zip']),
                        'country'       => stripslashes($order['shipping_country']),
                        'phone'         => stripslashes($order['shipping_phone']),
                        'email'         => stripslashes($order['shipping_email'])
                    ),
                    'ip'                => long2ip($order['ip'])
                );

                // QUANTITY COUNT
                $rs_items['num_products'] = count($citems);
                $rs_items['num_quantity'] = 0;

                // GET ITEMS
                $citems = $_uccms_ecomm->cartItems($order['id']);

                // HAVE ITEMS
                if (count($citems) > 0) {

                    $extended_view = true;

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // ADD TO QUANTITY COUNT
                        $rs_items['num_quantity'] += $oitem['quantity'];

                        // PRODUCT OPTIONS (ATTRIBUTES)
                        $options = array();
                        if (count($item['other']['options']) > 0) {
                            foreach ($item['other']['options'] as $option_id => $values) {
                                if ($values[0]) {
                                    $options[stripslashes($attra[$option_id]['title'])] = implode(', ', $values);
                                }
                            }
                        }

                        // ADD TO PRODUCTS ARRAY
                        $rs_items['products'][] = array(
                            'sku'       => stripslashes($item['sku']),
                            'name'      => stripslashes($item['title']),
                            'options'   => $options,
                            'quantity'  => $oitem['quantity'],
                            'total'     => $item['other']['total']
                        );

                    }

                }

                // CALCULATE BALANCE
                $balance = $_uccms_ecomm->orderBalance($order['id'], $amount);

                $success = false;

                // NOT CREDIT CARD
                if (in_array($payment_method, ['check_cash','donation','trade','waived'])) {

                    $columns = array(
                        'status'    => 'charged'
                    );

                    if ($balance == 0.00) {
                        $columns['paid_in_full'] = 1;
                    }

                    // UPDATE INFO
                    $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order['id']. ")";
                    sqlquery($cart_query);

                    // LOG TRANSACTION
                    $_uccms_ecomm->logTransaction(array(
                        'cart_id'   => $order['id'],
                        'status'    => 'charged',
                        'amount'    => $amount,
                        'tip'       => (int)$_POST['tip'],
                        'method'    => $payment_method,
                        'note'      => $_POST['payment'][$payment_method]['note'],
                        'ip'        => ip2long($_SERVER['REMOTE_ADDR'])
                    ));

                    $success = true;
                    $admin->growl('Payment', 'Payment processed.');

                // PAYING OTHER WAY
                } else {

                    $payment_missing = false;

                    // PAYMENT PROFILE SPECIFIED
                    if ($_POST['payment']['profile_id']) {

                        // GET PAYMENT PROFILE
                        $pp = $_uccms['_account']->pp_getProfile((int)$_POST['payment']['profile_id'], $_uccms['_account']->userID());

                        // PAYMENT PROFILE FOUND
                        if ($pp['id']) {

                            $payment_method     = 'profile';
                            $payment_name       = stripslashes($pp['name']);
                            $payment_lastfour   = $pp['lastfour'];
                            $payment_expiration = $pp['expires'];

                        // PAYMENT PROFILE NOT FOUND
                        } else {
                            $payment_missing = true;
                            $_uccms['_site-message']->set('error', 'Payment method not found.');
                        }

                    // USE CREDIT CARD INFO
                    } else {

                        // REQUIRE PAYMENT FIELDS
                        $reqfa = array(
                            'name'          => 'Name On Card',
                            'number'        => 'Card Number',
                            'expiration'    => 'Card Expiration',
                            'zip'           => 'Card Zip',
                            'cvv'           => 'Card CVV'
                        );

                        // CHECK REQUIRED
                        foreach ($reqfa as $name => $title) {
                            if (!$_uccms_ecomm->checkRequired($_POST['payment'][$payment_method][$name])) {
                                $payment_missing = true;
                                $_uccms['_site-message']->set('error', 'Missing / Incorrect: Payment - ' .$title);
                            }
                        }

                        $payment_name = $_POST['payment'][$payment_method]['name'];

                        // PAYMENT LAST FOUR
                        $payment_lastfour = substr(preg_replace("/[^0-9]/", '', $_POST['payment'][$payment_method]['number']), -4);

                        // PAYMENT EXPIRATION DATE FORMATTING
                        if ($_POST['payment'][$payment_method]['expiration']) {
                            $payment_expiration = date('Y-m-d', strtotime(str_replace('/', '/01/', $_POST['payment'][$payment_method]['expiration'])));
                        } else {
                            $payment_expiration = '0000-00-00';
                        }

                    }

                    // NO MISSING FIELDS
                    if (!$payment_missing) {

                        // PAYMENT - CHARGE
                        $payment_data = array(
                            'amount'    => $amount,
                            'customer' => array(
                                'id'            => ($order['customer_id'] ? $order['customer_id'] : $_uccms['_account']->userID()),
                                'name'          => $payment_name,
                                'email'         => $order['billing_email'],
                                'phone'         => $order['billing_phone'],
                                'address'       => array(
                                    'street'    => $order['billing_address1'],
                                    'street2'   => $order['billing_address2'],
                                    'city'      => $order['billing_city'],
                                    'state'     => $order['billing_state'],
                                    'zip'       => $order['billing_zip'],
                                    'country'   => $order['billing_city']
                                ),
                                'description'   => ''
                            ),
                            'note'  => 'Payment on order ID: ' .$order['id']
                        );

                        // HAVE PAYMENT PROFILE
                        if ($pp['id']) {
                            $payment_data['card'] = array(
                                'id'            => $pp['id'],
                                'name'          => $payment_name,
                                'number'        => $payment_lastfour,
                                'expiration'    => $payment_expiration
                            );

                        // USE CARD
                        } else {
                            $payment_data['card'] = array(
                                'name'          => $payment_name,
                                'number'        => $_POST['payment'][$payment_method]['number'],
                                'expiration'    => $payment_expiration,
                                'zip'           => $_POST['payment'][$payment_method]['zip'],
                                'cvv'           => $_POST['payment'][$payment_method]['cvv']
                            );
                        }

                        // PROCESS PAYMENT
                        $payment_result = $_uccms['_account']->payment_charge($payment_data);

                        // TRANSACTION ID
                        $transaction_id = $payment_result['transaction_id'];

                        // TRANSACTION MESSAGE (ERROR)
                        $transaction_message = $payment_result['error'];

                    // PAYMENT INFO MISSING
                    } else {
                        BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order['id']);
                        exit;
                    }

                    // LOG TRANSACTION
                    $_uccms_ecomm->logTransaction(array(
                        'cart_id'               => $order['id'],
                        'status'                => ($transaction_id ? 'charged' : 'failed'),
                        'amount'                => $amount,
                        'tip'                   => (int)$_POST['tip'],
                        'method'                => $payment_method,
                        'payment_profile_id'    => $payment_result['profile_id'],
                        'name'                  => $_POST['payment'][$payment_method]['name'],
                        'lastfour'              => $payment_lastfour,
                        'expiration'            => $payment_expiration,
                        'transaction_id'        => $transaction_id,
                        'message'               => $transaction_message,
                        'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                    ));

                    // CHARGE SUCCESSFUL
                    if ($transaction_id) {

                        // ADD TO ORDER INFO
                        $order['transaction_id'] = $transaction_id;

                        $columns = array(
                            'status'    => 'charged'
                        );

                        if ($balance == 0.00) {
                            $columns['paid_in_full'] = 1;
                        }

                        // UPDATE ORDER
                        $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order['id']. ")";
                        sqlquery($cart_query);

                        $success = true;
                        $admin->growl('Payment', 'Payment processed.');

                    // CHARGE FAILED
                    } else {

                        // UPDATE ORDER
                        $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='failed' WHERE (`id`=" .$order['id']. ")";
                        sqlquery($cart_query);

                        $admin->growl('Payment', 'Payment failed. (' .$gateway->Message. ')');

                    }

                }

                // WAS SUCCESS
                if ($success) {

                    // EMAIL SETTINGS
                    $esettings = array(
                        'template'      => 'admin_payment-processed.html',
                        'subject'       => 'Payment Processed',
                        'order_id'      => $order['id'],
                        'from_name'     => stripslashes($_uccms_ecomm->getSetting('email_from_name')),
                        'from_email'    => stripslashes($_uccms_ecomm->getSetting('email_from_email')),
                        'to_name'       => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                        'to_email'      => stripslashes($order['billing_email']),
                        'sent_by'       => 0
                    );

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'order_id'              => $order['id'],
                        'order_hash'            => $order['hash'],
                        'firstname'             => stripslashes($order['billing_firstname']),
                        'amount'                => number_format($_uccms_ecomm->toFloat($amount), 2),
                        'order_link'            => WWW_ROOT . $_uccms_ecomm->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash'])
                    );

                    // SEND EMAIL
                    $eresult = $_uccms_ecomm->sendEmail($esettings, $evars);

                    if ($payment_method == 'check_cash') {
                        $pm = 'Check / Cash';
                    } else {
                        $pm = 'credit card ending in ' .$payment_lastfour;
                    }

                    // LOG MESSAGE
                    $_uccms_ecomm->logQuoteMessage(array(
                        'order_id'      => $order['id'],
                        'by'            => 0,
                        'message'       => 'Paid $' .number_format($amount, 2). ' with ' .$pm. '.',
                        'status'        => 'charged',
                        'email_log_id'  => $eresult['log_id']
                    ));

                    $order['is_admin_order'] = true;

                    // ORDER DATA FOR REVSOCIAL API
                    $rs_orderdata['order']          = $order;
                    $rs_orderdata['order']['total'] = number_format($_uccms_ecomm->toFloat($order['order_total']), 2);
                    $rs_orderdata['order']['paid']  = number_format($_uccms_ecomm->toFloat($amount), 2);
                    $rs_orderdata['items']          = $rs_items;

                    // RECORD ORDER WITH REVSOCIAL
                    $_uccms_ecomm->revsocial_recordOrder($order['id'], $order['customer_id'], $rs_orderdata, true);

                }

            // ORDER NOT FOUND
            } else {
                $admin->growl('Payment', 'Order not found.');
            }

        // MISSING
        } else {
            $admin->growl('Payment', 'Amount and method required.');
        }

    // NO ORDER ID
    } else {
        $admin->growl('Shipping', 'Order ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order_id);

?>