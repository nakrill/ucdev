<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $order_id = (int)$_POST['order_id'];

    // HAVE ORDER ID
    if ($order_id) {

        // HAVE DATE AND AMOUNT
        if (($_POST['dt']) && ($_POST['amount'])) {

            if ((int)$_POST['email_days_before'] > 0) {
                $dt_email = date('Y-m-d', strtotime('-' .(int)$_POST['email_days_before']. ' Days', strtotime($_POST['dt'])));
            } else {
                $dt_email = date('Y-m-d', strtotime($_POST['dt']));
            }

            // DB COLUMNS
            $columns = array(
                'order_id'              => $order_id,
                'dt_due'                => date('Y-m-d', strtotime($_POST['dt'])),
                'amount'                => number_format((float)str_replace(',', '', $_POST['amount']), 2, '.', ''),
                'payment_profile_id'    => (int)$_POST['payment_profile_id'],
                'title'                 => $_POST['title'],
                'email_days_before'     => (int)$_POST['email_days_before'],
                'dt_email'              => $dt_email
            );

            // INSERT INTO DB
            $insert_query = "INSERT INTO `" .$_uccms_ecomm->tables['scheduled_payments']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW()";
            if (sqlquery($insert_query)) {

                $admin->growl('Scheduled Payment', 'Scheduled payment added.');

            // FAILED
            } else {
                $admin->growl('Scheduled Payment', 'Failed to create scheduled payment.');
            }

        // MISSING
        } else {
            $admin->growl('Scheduled Payment', 'Date and amount required.');
        }

    // NO ORDER ID
    } else {
        $admin->growl('Scheduled Payment', 'Order ID not specified.');
    }

}

if ($_REQUEST['from'] == 'quote') {
    BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$order_id);
} else {
    BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order_id);
}

?>