<?php

// IS QUOTE
if ($is_quote) {

    // DATABASE COLUMNS
    $columns = array(
        'billing_firstname'     => $_POST['contact']['firstname'],
        'billing_lastname'      => $_POST['contact']['lastname'],
        'billing_company'       => $_POST['contact']['company'],
        'billing_address1'      => $_POST['contact']['address1'],
        'billing_address2'      => $_POST['contact']['address2'],
        'billing_city'          => $_POST['contact']['city'],
        'billing_state'         => $_POST['contact']['state'],
        'billing_zip'           => $_POST['contact']['zip'],
        'billing_country'       => '',
        'billing_phone'         => $_POST['contact']['phone'],
        'billing_email'         => $_POST['contact']['email'],
        'shipping_same'         => 0,
        'shipping_firstname'    => $_POST['contact']['firstname'],
        'shipping_lastname'     => $_POST['contact']['lastname'],
        'shipping_address1'     => $_POST['event']['address1'],
        'shipping_address2'     => $_POST['event']['address2'],
        'shipping_city'         => $_POST['event']['city'],
        'shipping_state'        => $_POST['event']['state'],
        'shipping_zip'          => $_POST['event']['zip'],
        'shipping_country'      => '',
        'shipping_phone'        => $_POST['event']['phone'],
        'notes'                 => $_POST['event']['notes']
    );

    // CUSTOMER (ACCOUNT) ID SPECIFIED
    if ($_POST['customer_id']) {
        $customer_id = (int)$_POST['customer_id'];

    // NO CUSTOMER (ACCOUNT) ID SPECIFIED
    } else {

        // IS CATERING
        if ($_uccms_ecomm->storeType() == 'catering') {
            $account_info = [
                'email'     => $_POST['contact']['email'],
                'firstname' => $_POST['contact']['firstname'],
                'lastname'  => $_POST['contact']['lastname'],
                'password'  => $_POST['account']['password'],
            ];
        // IS GENERAL
        } else {
            $account_info = [
                'email'     => $columns['billing_email'],
                'firstname' => $columns['billing_firstname'],
                'lastname'  => $columns['billing']['lastname'],
                'password'  => $columns['account']['password'],
            ];
        }

        // SAVE CUSTOMER
        $customer = $_uccms_ecomm->orderSaveCustomer(array_merge($columns, [
            'account' => $account_info
        ]));
        $customer_id = $customer['id'];

    }

    // ORDER HASH
    if ($order['hash']) {
        $order_hash = $order['hash'];
    } else {
        $order_hash = $_uccms_ecomm->generateOrderHash($customer_id);
    }

    $columns['hash'] = $order_hash;
    $columns['customer_id'] = $customer_id;

// IS ORDER
} else {

    // DATABASE COLUMNS
    $columns = array(
        'notes' => $_POST['event']['notes']
    );

}

// UPDATE DB
$query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order['id']. ")";
sqlquery($query);

// EXTRA DATABASE COLUMNS
$details_columns = array(
    'event_location1'       => $_POST['event']['location1'],
    'event_location2'       => $_POST['event']['location2'],
    'event_address1'        => $_POST['event']['address1'],
    'event_address2'        => $_POST['event']['address2'],
    'event_city'            => $_POST['event']['city'],
    'event_state'           => $_POST['event']['state'],
    'event_zip'             => $_POST['event']['zip'],
    'event_phone'           => $_POST['event']['phone'],
    'event_date'            => date('Y-m-d', strtotime($_POST['event']['date'])),
    'event_num_people'      => number_format($_POST['event']['num_people'], 0, '', ''),
    'event_notes'           => $_POST['event']['notes'],
    'service_time'          => date('H:i:00', strtotime($_POST['service']['time'])),
    'service_type'          => $_POST['service']['type'],
    'contact_firstname'     => $_POST['contact']['firstname'],
    'contact_lastname'      => $_POST['contact']['lastname'],
    'contact_company'       => $_POST['contact']['company'],
    'contact_address1'      => $_POST['contact']['address1'],
    'contact_address2'      => $_POST['contact']['address2'],
    'contact_city'          => $_POST['contact']['city'],
    'contact_state'         => $_POST['contact']['state'],
    'contact_zip'           => $_POST['contact']['zip'],
    'contact_phone'         => $_POST['contact']['phone'],
    'contact_email'         => $_POST['contact']['email']
);

// UPDATE DB
$query = "UPDATE `" .$_uccms_ecomm->tables['quote_catering']. "` SET " .$_uccms_ecomm->createSet($details_columns). " WHERE (`order_id`=" .$order['id']. ")";
sqlquery($query);

?>