<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $trans_id = (int)$_POST['transaction_id'];

    // HAVE TRANSACTION ID
    if ($trans_id) {

        // GET TRANSACTION INFO
        $transaction_query = "SELECT * FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`id`=" .$trans_id. ")";
        $transaction_q = sqlquery($transaction_query);
        $transaction = sqlfetch($transaction_q);

        // TRANSACTION FOUND
        if ($transaction['id']) {

            // AMOUNT
            $amount = number_format((float)$_POST['amount'], 2, '.', '');

            // PROCESS REFUND
            $result = $_uccms['_account']->payment_refund($transaction, $amount, array(
                'note'  => $_POST['note']
            ));

            // TRANSACTION ID
            $transaction_id = $result['transaction_id'];

            // TRANSACTION ERROR
            $transaction_message = $result['error'];

            // LOG TRANSACTION
            $_uccms_ecomm->logTransaction(array(
                'cart_id'               => $transaction['cart_id'],
                'status'                => ($transaction_id ? 'refunded' : 'failed'),
                'amount'                => $amount,
                'tip'                   => $transaction['tip'],
                'method'                => $_uccms['_account']->gateway->Service, //$transaction['method'],
                'payment_profile_id'    => $transaction['payment_profile_id'],
                'name'                  => $transaction['name'],
                'lastfour'              => $transaction['lastfour'],
                'expiration'            => $transaction['expiration'],
                'note'                  => $_POST['note'],
                'transaction_id'        => $transaction_id,
                'message'               => $transaction_message,
                'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
            ));

            // REFUND SUCCESSFUL
            if ($transaction_id) {

                $columns = array(
                    'status'    => 'charged'
                );

                $total = $_uccms_ecomm->orderTotal($transaction['cart_id']);
                $balance = $_uccms_ecomm->orderBalance($transaction['cart_id']);

                if ($balance == 0.00) {
                    $columns['paid_in_full'] = 1;
                } else {
                    $columns['paid_in_full'] = 0;
                }

                if ($balance == $total) {
                    $columns['status'] = 'refunded';
                }

                // UPDATE ORDER
                $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$transaction['cart_id']. ")";
                sqlquery($cart_query);

                $success = true;
                $admin->growl('Refund', 'Refund processed.');

            // REFUND FAILED
            } else {

                $admin->growl('Refund', 'Refund failed. (' .$transaction_message. ')');

                /*
                // UPDATE ORDER
                $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='failed' WHERE (`id`=" .$order['id']. ")";
                sqlquery($cart_query);

                $admin->growl('Payment', 'Payment failed. (' .$gateway->Message. ')');
                */

            }

        // TRANSACTION NOT FOUND
        } else {
            $admin->growl('Refund', 'Transaction not found.');
        }

    // NO TRANSACTION ID
    } else {
        $admin->growl('Refund', 'Transaction ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$transaction['cart_id']. '#transactions');

?>