<?php

// FORM SUBMITTED
if (is_array($_REQUEST)) {

    // LINK THE ORDERS
    $result = $_uccms_ecomm->unlinkOrders($_REQUEST['id']);

    if ($result['success']) {
        $admin->growl('Linked Order', 'Removed!');
    } else {
        if (!$result['error']) $result['error'] = 'Failed to unlink orders.';
        $admin->growl('Linked Order', $result['error']);
    }

}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$_REQUEST['order_id']. '#linked-orders');

?>