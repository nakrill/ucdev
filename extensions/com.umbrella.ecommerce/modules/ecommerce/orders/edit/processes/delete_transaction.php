<?php

// HAVE TRANSACTION ID
if ($_GET['id']) {

    // DELETE RECORD
    $delete_query = "DELETE FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`id`=" .(int)$_GET['id']. ")";
    if (sqlquery($delete_query)) {
        $admin->growl('Payment Transaction', 'Transaction deleted.');
    } else {
        $admin->growl('Payment Transaction', 'Failed to delete transaction.');
    }

// NOT SHIPMENT ID
} else {
    $admin->growl('Payment Transaction', 'ID not specified.');
}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$_GET['order_id']. '#transactions');

?>