<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $order_id = (int)$_POST['order_id'];

    // HAVE ORDER ID
    if ($order_id) {

        // HAVE SERVICE & TRACKING NUMBER
        if (($_POST['carrier']) && ($_POST['tracking'])) {

            // DB COLUMNS
            $columns = array(
                'order_id'  => $order_id,
                'carrier'   => $_POST['carrier'],
                'service'   => $_POST['service'][$_POST['carrier']],
                'tracking'  => $_POST['tracking'],
                'ship_date' => ($_POST['ship_date'] ? date('Y-m-d', strtotime($_POST['ship_date'])) : '0000-00-00'),
                'cost'      => ($_POST['cost'] ? number_format($_POST['cost'], 2) : '0.00')
            );

            // INSERT INTO DB
            $insert_query = "INSERT INTO `" .$_uccms_ecomm->tables['order_shipments']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW()";
            if (sqlquery($insert_query)) {

                $admin->growl('Shipping', 'Tracking number added.');

                // UPDATE STATUS
                $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='" .trim(strtolower($_POST['status'])). "' WHERE (`id`=" .$order_id. ")";
                sqlquery($query);

                // SENDING EMAIL TO CUSTOMER
                if ($_POST['email']) {

                    // GET ORDER INFO
                    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$order_id. ")";
                    $order_q = sqlquery($order_query);
                    $order = sqlfetch($order_q);

                    // ORDER FOUND
                    if ($order['id']) {

                        // LINK TO VIEW QUOTE ONLINE
                        //$link = '<a href="' .WWW_ROOT . $_uccms_ecomm->storePath(). '/quote/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']). '" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">View Quote</a>';

                        // EMAIL SETTINGS
                        $esettings = array(
                            'template'      => 'order_shipped.html',
                            'subject'       => 'Order Shipped',
                            'order_id'      => $order['id'],
                            'from_name'     => stripslashes($_uccms_ecomm->getSetting('email_from_name')),
                            'from_email'    => stripslashes($_uccms_ecomm->getSetting('email_from_email')),
                            'to_name'       => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                            'to_email'      => stripslashes($order['billing_email']),
                            'sent_by'       => $_uccms_ecomm->adminID()
                        );

                        // EMAIL VARIABLES
                        $evars = array(
                            'date'                  => date('n/j/Y'),
                            'time'                  => date('g:i A T'),
                            'order_id'              => $order['id'],
                            'order_hash'            => $order['hash'],
                            'carrier'               => strtoupper($_POST['carrier']),
                            'tracking'              => $_POST['tracking'],
                            'tracking_link'         => $_uccms_ecomm->trackingURL($_POST['carrier'], $_POST['tracking'])
                            //'quote_link'            => $link
                        );

                        // SEND EMAIL
                        $eresult = $_uccms_ecomm->sendEmail($esettings, $evars);

                    }

                }

            // FAILED
            } else {
                $admin->growl('Shipping', 'Failed to record shipping info.');
            }

        // MISSING
        } else {
            $admin->growl('Shipping', 'Carrier & Tracking Number required.');
        }

    // NO ORDER ID
    } else {
        $admin->growl('Shipping', 'Order ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order_id);

?>