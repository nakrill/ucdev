<?php

// HAVE SHIPMENT ID
if ($_GET['id']) {

    // GET INFO
    $shipment_query = "SELECT * FROM `" .$_uccms_ecomm->tables['order_shipments']. "` WHERE (`id`=" .(int)$_GET['id']. ")";
    $shipment_q = sqlquery($shipment_query);
    $shipment = sqlfetch($shipment_q);

    // SHIPMENT FOUND
    if ($shipment['id']) {

        // IS THIRD PARTY SHIPMENT
        if ($shipment['3rdparty']) {

            // IS SHIPSTATION
            if ($shipment['3rdparty'] == 'shipstation') {

                // LOAD REQUIRED LIBRARIES
                require_once(SERVER_ROOT. 'uccms/includes/libs/unirest/Unirest.php');
                require_once(EXTENSION_ROOT. 'classes/shipstation.php');

                // DISABLE UNIREST SSL VALIDATION
                Unirest::verifyPeer(false);

                // INITIALIZE SHIPSTATION
                $shipStation = new Shipstation();
                $shipStation->setSsApiKey($_uccms_ecomm->getSetting('shipstation_api_key'));
                $shipStation->setSsApiSecret($_uccms_ecomm->getSetting('shipstation_api_secret'));

                // VOID LABEL
                $void = $shipStation->voidLabel($shipment['3rdparty_id']);

                // VOIDING LABEL SUCCESS
                if ($void['success']) {

                    $admin->growl('Shipping', 'Shipment voided & deleted.');

                    // RECORD VOIDED
                    $update_query = "UPDATE `" .$_uccms_ecomm->tables['order_shipments']. "` SET `voided`=1, `dt_voided`=NOW() WHERE (`id`=" .$shipment['id']. ")";
                    sqlquery($update_query);

                // ERROR
                } else {
                    $admin->growl('Shipping', $void['error']['message']);
                }

            }

        // BASIC TRACKING
        } else {

            // DELETE RECORD
            $delete_query = "DELETE FROM `" .$_uccms_ecomm->tables['order_shipments']. "` WHERE (`id`=" .$shipment['id']. ")";
            if (sqlquery($delete_query)) {
                $admin->growl('Shipping', 'Shipment deleted.');
            } else {
                $admin->growl('Shipping', 'Failed to delete shipment.');
            }

        }

    // SHIPMENT NOT FOUND
    } else {
        $admin->growl('Shipping', 'Shipment not found.');
    }

// NOT SHIPMENT ID
} else {
    $admin->growl('Shipping', 'Shipment ID not specified.');
}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$_GET['order_id']);

?>