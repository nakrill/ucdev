<?php

// HAVE SCHEDULED PAYMENT ID
if ($_GET['id']) {

    // DELETE RECORD
    $delete_query = "DELETE FROM `" .$_uccms_ecomm->tables['scheduled_payments']. "` WHERE (`id`=" .(int)$_GET['id']. ")";
    if (sqlquery($delete_query)) {
        $admin->growl('Scheduled Payment', 'Scheduled payment deleted.');
    } else {
        $admin->growl('Scheduled Payment', 'Failed to delete scheduled payment.');
    }

// NOT SHIPMENT ID
} else {
    $admin->growl('Scheduled Payment', 'Scheduled Payment ID not specified.');
}

if ($_REQUEST['from'] == 'quote') {
    BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$_GET['order_id']);
} else {
    BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$_GET['order_id']);
}

?>