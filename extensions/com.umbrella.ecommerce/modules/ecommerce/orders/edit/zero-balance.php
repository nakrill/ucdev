<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        $columns = array(
            'zero_balance'  => (int)$_REQUEST['active'],
        );

        // UPDATE
        $update_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$order['id']. ")";
        if (sqlquery($update_query)) {
            $admin->growl('Success', 'Balance updated.');
        } else {
            $admin->growl('Error', 'Failed to update balance.');
        }

    } else {
        $admin->growl('Error', 'Order not found.');
    }

// ID NOT SPECIFIED
} else {
    $admin->growl('Error', 'Order ID not specified.');
}

BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order['id']);

?>