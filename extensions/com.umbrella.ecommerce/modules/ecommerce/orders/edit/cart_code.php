<div class="contain box">

    <table id="cart" cellpadding="0" cellspacing="0">

        <tr>
            <th class="thumb"></th>
            <th class="title">Item</th>
            <th class="quantity">Quantity</th>
            <th class="price">Price</th>
            <th class="total">Total</th>
        </tr>

        <?php

        // HAVE ITEMS
        if (count($citems) > 0) {

            // STORE IS CATERING
            if ($_uccms_ecomm->storeType() == 'catering') {

                // SORT ITEMS BY DAY AND TIME
                $citems = $_uccms_ecomm->stc->sortOItemsByDayTime($citems);

            }

            // HAVE TAX OVERRIDE
            if ($order['override_tax']) {
                $tax['total'] = number_format($order['override_tax'], 2);

            // NORMAL TAX CALCULATION
            } else {

                // HAVE BILLING STATE AND/OR ZIP
                if (($vals['contact']['state']) || ($vals['contact']['zip'])) {
                    $taxvals['country'] = 'United States';
                    $taxvals['state']   = $vals['contact']['state'];
                    $taxvals['zip']     = $vals['contact']['zip'];
                }

                //$taxvals['quote'] = false;

                // GET TAX
                $tax = $_uccms_ecomm->cartTax($order['id'], $citems, $taxvals);

            }

            // HAVE SHIPPING OVERRIDE
            if ($order['override_shipping']) {
                $shipping['markup'] = number_format($order['override_shipping'], 2);
            } else {
                $shipping['markup'] = 0.00;
            }

            // ATTRIBUTE ARRAY
            $attra = array();

            // DISPLAY GROUP
            $display_group = '';

            // LOOP THROUGH ITEMS
            foreach ($citems as $oitem) {

                // GET ITEM INFO
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                // IS PRODUCT & AFTER TAX ITEM
                if (($item['type'] == 0) && ($oitem['after_tax'])) {
                    $atia[] = $oitem;
                    continue;

                // IS SERVICE
                } else if ($item['type'] == 1) {
                    $service_fees += $item['other']['total'];

                // IS RENTAL
                } else if ($item['type'] == 2) {
                    $rental_fees += $item['other']['total'];
                }

                // IS CATERING
                if ($_uccms_ecomm->storeType() == 'catering') {

                    // GROUP HEADINGS
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/catering/cart_group-heading.php');

                }

                ?>

                <tr id="item-<?php echo $oitem['id']; ?>" class="item">
                    <td class="thumb">
                        <img src="<?php echo $item['other']['image_url']; ?>" alt="<?php echo stripslashes($item['title']); ?>" />
                    </td>
                    <td class="title">
                        <?php echo stripslashes($item['title']); ?>
                        <?php if (count($item['other']['options']) > 0) { ?>
                            <ul class="options">
                                <?php
                                foreach ($item['other']['options'] as $option_id => $values) {
                                    if ($values[0]) {
                                        ?>
                                        <li>
                                            <?php echo stripslashes($attra[$option_id]['title']);  ?>: <?php echo implode(', ', $values); ?>
                                        </li>
                                        <?php
                                    }
                                } ?>
                            </ul>
                        <?php } ?>
                    </td>
                    <td class="quantity">
                        <?php echo number_format($oitem['quantity'], 0); ?>
                    </td>
                    <td class="price">
                        <?php if ($extended_view) { ?>
                            <?php echo $item['other']['price_formatted']; ?>
                        <?php } else { ?>
                            -
                        <?php } ?>
                    </td>
                    <td class="total" style="position: relative;">
                        <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
                            <div class="actions">
                                <a href="#" class="edit" title="Edit" data-id="<?=$oitem['id']?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href="#" class="delete" title="Delete"><i class="fa fa-times" data-id="<?=$oitem['id']?>"></i></a>
                            </div>
                        <?php } ?>
                        <?php if ($extended_view) { ?>
                            <?php echo $item['other']['total_formatted']; ?>
                        <?php } else { ?>
                            -
                        <?php } ?>
                    </td>
                </tr>

                <?php

            }

            ?>

            <?php if ($extended_view) { ?>

                <tr class="sub right subtotal">
                    <td colspan="4" class="title">
                        Subtotal:
                    </td>
                    <td class="value">
                        $<?php echo number_format($order['subtotal'], 2); ?>
                    </td>
                </tr>

                <tr class="sub right tax">
                    <td colspan="4" class="title">
                        Tax:
                    </td>
                    <td class="value">
                        <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
                            $<input type="text" name="order[override_tax]" value="<?=$order['override_tax']?>" placeholder="<?php echo number_format($tax['total'], 2); ?>" />
                        <?php } else { ?>
                            $<?php
                            if ($order['override_tax']) {
                                echo number_format((float)$order['override_tax'], 2);
                            } else {
                                echo number_format($tax['total'], 2);
                            }
                        } ?>
                    </td>
                </tr>

                <tr class="sub right shipping <?php if (count($atia) > 0) { echo 'with_after_tax'; } ?>">
                    <td colspan="4" class="title">
                        Shipping:
                    </td>
                    <td class="value">
                        <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
                            $<input type="text" name="order[override_shipping]" value="<?=$order['override_shipping']?>" placeholder="<?php echo number_format($order['shipping'], 2); ?>" />
                        <?php } else { ?>
                            $<?php
                            if ($order['override_shipping']) {
                                echo number_format((float)$order['override_shipping'], 2);
                            } else {
                                echo number_format($order['shipping'], 2);
                            }
                        } ?>
                    </td>
                </tr>

                <?php if ($service_fees) { ?>
                    <tr class="sub right service_fees">
                        <td colspan="4" class="title">
                            Service Fees:
                        </td>
                        <td class="value">
                            $<?php echo number_format($service_fees, 2); ?>
                        </td>
                    </tr>
                <?php } ?>

                <?php if ($rental_fees) { ?>
                    <tr class="sub right rental_fees">
                        <td colspan="4" class="title">
                            Rental Fees:
                        </td>
                        <td class="value">
                            $<?php echo number_format($rental_fees, 2); ?>
                        </td>
                    </tr>
                <?php } ?>

            <?php } ?>

            <?php

            // HAVE AFTER TAX ITEMS
            if (count($atia) > 0) {

                // RESET DISPLAY GROUP
                $display_group = '';

                // LOOP
                foreach ($atia as $oitem) {

                    // GET ITEM INFO
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                    // IS CATERING
                    if ($_uccms_ecomm->storeType() == 'catering') {

                        // GROUP HEADINGS
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/catering/cart_group-heading.php');

                    }

                    ?>

                    <tr id="item-<?php echo $oitem['id']; ?>" class="item aftertax <?php if ($item['quote']) { echo 'quote'; } ?>">
                        <td class="thumb">
                            <img src="<?php echo $item['other']['image_url']; ?>" alt="<?php echo stripslashes($item['title']); ?>" />
                        </td>
                        <td class="title">
                            <?php echo stripslashes($item['title']); ?>
                            <?php if (count($item['other']['options']) > 0) { ?>
                                <ul class="options">
                                    <?php
                                    foreach ($item['other']['options'] as $option_id => $values) {
                                        if ($values[0]) {
                                            ?>
                                            <li>
                                                <?php echo stripslashes($attra[$option_id]['title']);  ?>: <?php echo implode(', ', $values); ?>
                                            </li>
                                            <?php
                                        }
                                    } ?>
                                </ul>
                            <?php } ?>
                        </td>
                        <td class="quantity">
                            <?php echo number_format($oitem['quantity'], 0); ?>
                        </td>
                        <td class="price">
                            <?php if ($extended_view) { ?>
                                <?php echo $item['other']['price_formatted']; ?>
                            <?php } else { ?>
                                -
                            <?php } ?>
                        </td>
                        <td class="total" style="position: relative;">
                            <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
                                <div class="actions">
                                    <a href="#" class="edit" title="Edit" data-id="<?=$oitem['id']?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href="#" class="delete" title="Delete"><i class="fa fa-times" data-id="<?=$oitem['id']?>"></i></a>
                                </div>
                            <?php } ?>
                            <?php if ($extended_view) { ?>
                                <?php echo $item['other']['total_formatted']; ?>
                            <?php } else { ?>
                                -
                            <?php } ?>
                        </td>
                    </tr>

                    <?php

                }

            }

            ?>

            <?php if ($extended_view) { ?>

                <tr class="sub right total">
                    <td colspan="4" class="title">
                        Total:
                    </td>
                    <td class="value">
                        $<?php echo number_format($order['subtotal'] + $tax['total'] + $shipping['markup'] + $order['aftertax'], 2); ?>
                    </td>
                </tr>

            <?php } ?>

            <?php

        // NO ITEMS
        } else {
            ?>
            <tr class="item">
                <td colspan="3" style="padding: 10px 0; background-color: #fff; text-align: center;">
                    No items.
                </td>
            </tr>
            <?php
        }

        ?>

    </table>

</div>