<?php

/*
$_uccms_ecomm->runCron();
exit;
*/

$transactions = array();

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        // IS QUOTE
        if ($order['quote'] == 1) {
            $admin->growl('Order', 'Order is quote.');
            BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$order['id']);
        }

        // NO HASH YET
        if (!$order['hash']) {

            // GENERATE HASH
            $order['hash'] = $_uccms_ecomm->generateOrderHash($order['customer_id']);

            // UPDATE
            $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `hash`='" .$order['hash']. "' WHERE (`id`=" .$order['id']. ")";
            sqlquery($query);

        }

        // GET ORDER ITEMS
        $citems = $_uccms_ecomm->cartItems($order['id']);

        // SEE IF IT HAD A QUOTE TOO
        $quote_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`hash`='" .stripslashes($order['hash']). "') AND (`quote`=1) AND (`customer_id`=" .$order['customer_id']. ")";
        $quote_q = sqlquery($quote_query);
        $quote = sqlfetch($quote_q);

        // GET EXTRA INFO
        $extra = $_uccms_ecomm->cartExtra($order['id']);

        // GET BALANCE
        $balance = $_uccms_ecomm->orderBalance($order['id']);

        // CALCULATE PAID
        $paid = number_format($order['order_total'] - $balance, 2);

        // ORDER URL
        $order_url = WWW_ROOT . $_uccms_ecomm->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']);

        // GET SHIPPING METHODS
        $sma = $_uccms_ecomm->allShippingMethods();

        // DEFAULT SHIPPING TITLE
        $shipping_title = 'N/A';

        if ($order['shipping_service']) {

            // IS 3RD PARTY
            if ($order['shipping_3rdparty']) {

                // SHIPSTATION
                if ($order['shipping_3rdparty'] == 'shipstation') {

                    // GET SHIPSTATION CARRIERS & SERVICES SETTINGS
                    $csa = $_uccms_ecomm->shipstationCarriersServices();

                    $carrier_title = $csa[$order['shipping_carrier']]['title'];
                    $shipping_service = $csa[$order['shipping_carrier']]['services'][$order['shipping_service']];

                    if ($shipping_service['title']) {
                        $shipping_title = $carrier_title. ' - ' .$shipping_service['title'];
                    } else {
                        $shipping_title = $carrier_title. ' - ' .$shipping_service['title_default'];
                    }

                }

            // NORMAL SHIPPING
            } else {

                // IS SHIPPING TABLE
                if (substr($order['shipping_service'], 0, 6) == 'table-') {
                    $ta = explode('-', $order['shipping_service']);
                    $shipping_query = "
                    SELECT st.*, sm.title AS `method_title`
                    FROM `" .$_uccms_ecomm->tables['shipping_table']. "` AS `st`
                    INNER JOIN `" .$_uccms_ecomm->tables['shipping_methods']. "` AS `sm` ON sm.id='table'
                    WHERE st.id=" .intval($ta[1]). "
                    ";

                // IS OTHER
                } else {
                    $shipping_query = "
                    SELECT ss.*, sm.title AS `method_title`
                    FROM `" .$_uccms_ecomm->tables['shipping_services']. "` AS `ss`
                    INNER JOIN `" .$_uccms_ecomm->tables['shipping_methods']. "` AS `sm` ON ss.method=sm.id
                    WHERE (ss.id='" .$order['shipping_service']. "')
                    ";
                }

                // GET SHIPPING SERVICE INFO
                $shipping = sqlfetch(sqlquery($shipping_query));

                // HAVE SHIPPING TITLE
                if ($shipping['id']) {
                    $shipping_title = stripslashes($shipping['method_title']). ' - ' .stripslashes($shipping['title']);
                }

            }

        }

        // GET PAYMENT METHODS
        $payment_methods = $_uccms_ecomm->getPaymentMethods(true);

        $payment_profiles = array();

        // PAYMENT PROFILES ENABLED
        if ($_uccms['_account']->paymentProfilesEnabled()) {

            // GET PAYMENT PROFILES
            $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$order['customer_id']. ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
            $pp_q = sqlquery($pp_query);
            while ($pp = sqlfetch($pp_q)) {
                $payment_profiles[$pp['id']] = $pp;
            }

        }

        // GET TRANSACTIONS
        $transactions_query = "SELECT * FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`cart_id`='" .stripslashes($order['id']). "') ORDER BY `dt` DESC";
        $transactions_q = sqlquery($transactions_query);
        while ($transaction = sqlfetch($transactions_q)) {
            $transactions[$transaction['id']] = $transaction;
        }

    // ORDER NOT FOUND
    } else {
        $admin->growl('Order', 'Order not found.');
        BigTree::redirect(MODULE_ROOT. 'orders/');
    }

// NO ID
} else {

    // CLEAR CART ID
    $_uccms_ecomm->clearCartID(true);

    // INIT CART
    $order = $_uccms_ecomm->initCart(true, 0, true, [
        'noCookie' => true,
    ]);

    // UPDATE ORDER
    $order_update = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `created_by`=" .$admin->ID. " WHERE (`id`=" .$order['id']. ")";
    sqlquery($order_update);

    $new_order = true;

}

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Order', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => ($order['id'] ? 'Edit' : 'Add'). ' Order', 'link' => '' ];

?>

<style type="text/css">

    .container.expand header, .container.expanded header {
        cursor: pointer;
    }
    .container.expand section, .container.expand footer {
        display: none;
    }

    .container.order-details .left {
        padding-right: 7.5px;
    }
    .container.order-details .right {
        padding-left: 7.5px;
    }

    #cart {
        width: 100%;
        margin-bottom: 5px;
        border: 0px none;
    }

    #cart th {
        padding: 0px;
        border-bottom: 1px solid #eee;
        background-color: transparent;
    }

    #cart .thumb {
        width: 10%;
        text-align: center;
    }
    #cart .title {
        width: 50%;
        text-align: left;
    }
    #cart .price {
        width: 15%;
        text-align: left;
    }
    #cart .quantity {
        width: 15%;
        text-align: left;
    }
    #cart .total {
        width: 10%;
        text-align: right;
    }

    #cart .item td {
        padding: 8px;
    }

    #cart th.title {
        padding-left: 8px;
    }
    #cart th.total {
        padding-right: 8px;
    }
    #cart td.quantity {
        padding-left: 0px;
        padding-right: 0px;
    }
    #cart td.price {
        padding-left: 0px;
        padding-right: 0px;
    }

    #cart .item > td {
        border-bottom: 1px solid #eee;
    }

    #cart .item .thumb {
        vertical-align: top;
    }
    #cart .item .thumb img {
        height: 50px;
    }

    #cart .item .options {
        margin: 0px;
        padding: 2px 0 0 15px;
    }
    #cart .item .options ul {
        margin: 0px;
        padding: 0 0 0 15px;
    }
    #cart .item .options li {
        font-size: .9em;
        line-height: 1.2em;
    }
    #cart .item .options ul li {
        font-size: 1em;
    }

    #cart .item .actions {
        display: none;
        position: absolute;
        top: 4px;
        right: 8px;
        font-size: 1.4em;
    }

    #cart .sub td {
        padding: 10px 0 0;
        background-color: #fff;
        text-align: right;
    }
    #cart .sub td.title {
        padding-right: 0px;
    }

    #log .item {
        margin: 0 0 10px 0;
        padding: 0 0 10px 0;
        border-bottom: 1px solid #ccc;
    }
    #log .item:last-child {
        margin: 0px;
        padding: 0px;
        border-bottom: 0px none;
    }
    #log .item .left, #log .item .right {
        margin: 0px;
    }
    #log .item .right {
        text-align: right;
    }
    #log .item .info {
        padding-bottom: 4px;
    }

    .container footer .button.right {
        float: right;
        width: auto;
        margin: 0 0 0 15px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // SET AS FOOTER CART
        $('header .footer-cart').click(function(e) {
            e.preventDefault();
            afc_startCart({
                id: <?php echo $order['id']; ?>,
            }, function() {

            });
        });

        // ACCOUNT CHANGE
        $('#form_main select[name="customer_id"]').change(function(e) {

            var id = $(this).val();
            var cur_id = $('#form_main input[name="current_customer_id"]').val();

            <?php if ($new_order) { ?>
                this_changeCustomer(id);
            <?php } else { ?>
                if ((id) && (id != cur_id)) {
                    $('#confirm_change-customer-id').show();
                } else if (id == cur_id) {
                    $('#confirm_change-customer-id').hide();
                }
            <?php } ?>

        });

        // CONFIRM ACCOUNT CHANGE - NO
        $('#confirm_change-customer-id .no').click(function(e) {
            e.preventDefault();
            $('#confirm_change-customer-id').hide();
            $('form .container select[name="customer_id"]').val($('form .container input[name="current_customer_id"]').val()).trigger('change');
        });

        // CONFIRM ACCOUNT CHANGE - YES
        $('#confirm_change-customer-id .yes').click(function(e) {
            e.preventDefault();
            this_changeCustomer($('form .container select[name="customer_id"]').val());
        });

        <?php if ($_uccms_ecomm->storeType() == 'booking') { ?>

            $('#form_main input[type="submit"]').click(function(e) {
                e.preventDefault();

                $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/orders/store_types/booking/form-main_pre-submit_availability/', $('#form_main').serialize(), function(data) {

                    if (data) {
                        $('#booking-conflict .contain').html(data);
                        $('#booking-conflict').show();
                        location.hash = '#booking-conflict';
                        return false;
                    } else {
                        $('#form_main').submit();
                    }

                }, 'html');

            });

        <?php } ?>

        // SHIPPING SAME TOGGLE
        $('#shipments_same').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_shipping_same').hide();
            } else {
                $('#toggle_shipping_same').show();
            }
        });

        // PAY NOW
        $('.container footer .button.pay').click(function(e) {
            e.preventDefault();
            $('#payment.container.expand input[name="amount"]').val($(this).data('balance'));
            $('#payment.container.expand header').click();
            location.hash = '#payment';
        });

    });

    // GET CUSTOMER INFO
    function this_changeCustomer(id) {
        $('#form_main').submit();
    }

</script>

<form id="form_main" enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="id" value="<?php echo $order['id']; ?>" />

<div class="order-details container legacy">

    <header>
        <h2>Order Details</h2>
        <?php if (!$new_order) { ?>
            <?php if ($_uccms_ecomm->getSetting('admin_footer_cart_enabled')) { ?>
                <a href="#" class="button footer-cart">Set as Footer Cart</a>
            <?php } ?>
            <a href="#" class="button trello">Add to Trello</a>
            <a href="<?=$order_url?>" target="_blank" class="button">View Invoice</a>

            <?php

            // HAS STAFF EXTENSION
            if (class_exists('uccms_Staff')) {

                if (!$_uccms_staff) $_uccms_staff = new uccms_Staff();

                // HAS ACCESS
                if ($_uccms_staff->adminModulePermission()) {

                    // JOB DATA
                    $jobData = [
                        'channels' => [
                            [
                                'channel'   => $_uccms_ecomm->Extension,
                                'what'      => 'order',
                                'item_id'   => $order['id']
                            ]
                        ]
                    ];

                    // CHECK FOR EXISTING JOB(S)
                    $job = $_uccms_staff->getJobs(array_merge($jobData, ['limit' => 1]));
                    $job_id = key((array)$job['jobs']);

                    // LINK
                    ?>
                    <a href="#" target="_blank" class="button job modalLink-editJob" data-jobid="<?php echo $job_id; ?>" data-jobdata="<?php echo base64_encode(json_encode($jobData)); ?>"><?php echo ($job_id ? 'Edit' : 'Create'); ?> Job</a>
                    <?php

                    // CODE
                    include_once(SERVER_ROOT. 'extensions/' .$_uccms_staff->Extension. '/modules/elements/jobs/modal_add-edit.php');

                }

            }

            ?>

        <?php } ?>
    </header>

    <section>

        <div class="row">

            <div class="col-md-6">

                <?php

                // EXISTING
                if (!$new_order) {

                    // CALCULATE DAYS OLD
                    $days_old = dateDifference($order['dt'], date('Y-m-d H:i:s'), '%a');

                    ?>

                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td><strong>Order ID:</strong>&nbsp;</td>
                            <td><strong><?php echo $order['id']; ?></strong></td>
                        </tr>
                        <tr>
                            <td>Status:&nbsp;</td>
                            <td><?php echo ucwords($order['status']); ?></td>
                        </tr>
                        <tr>
                            <td>Date:&nbsp;</td>
                            <td><?php echo date('n/j/Y', strtotime($order['dt'])); ?></td>
                        </tr>
                        <tr>
                            <td>Time:&nbsp;</td>
                            <td><?php echo date('g:i A T', strtotime($order['dt'])); ?></td>
                        </tr>
                        <tr>
                            <td>Days old:&nbsp;</td>
                            <td><?php echo number_format((int)$days_old, 0); ?></td>
                        </tr>
                        <tr>
                            <td>IP:&nbsp;</td>
                            <td><?php echo long2ip($order['ip']); ?></td>
                        </tr>
                        <?php if ($order['referrer_host']) { ?>
                            <tr>
                                <td>Referrer:&nbsp;</td>
                                <td><?php echo stripslashes(urldecode($order['referrer_host'])); ?></td>
                            </tr>
                        <?php } ?>

                        <tr>
                            <td>Placed by:&nbsp;</td>
                            <td>
                                <?php
                                if ($order['created_by']) {
                                    $auser_sql = "SELECT * FROM `bigtree_users` WHERE (`id`=" .$order['created_by']. ")";
                                    $auser_query = sqlquery($auser_sql);
                                    $auser = sqlfetch($auser_query);
                                    ?>
                                    Admin<?php if ($auser['id']) { ?> (<?php echo stripslashes($auser['name']); ?>)<?php } ?>
                                    <?php
                                } else { ?>
                                    Customer
                                <?php } ?>
                            </td>
                        </tr>

                    </table>

                    <?php

                }

                ?>

                <fieldset>

                    <label>Customer Account</label>

                    <input type="hidden" name="current_customer_id" value="<?php echo $order['customer_id']; ?>" />

                    <select name="customer_id" class="custom_control form-control">
                        <option value="0">None</option>
                        <optgroup label="Accounts">
                            <?php
                            $accounts_query = "
                            SELECT a.id, a.username, a.email, CONCAT(ad.firstname, ' ', ad.lastname) AS `fullname`
                            FROM `" .$_uccms_ecomm->tables['accounts']. "` AS `a`
                            INNER JOIN `" .$_uccms_ecomm->tables['account_details']. "` AS `ad` ON a.id=ad.id
                            ORDER BY `fullname` ASC, a.username ASC
                            ";
                            $accounts_q = sqlquery($accounts_query);
                            while ($acct = sqlfetch($accounts_q)) {
                                ?>
                                <option value="<?php echo $acct['id']; ?>" <?php if ($acct['id'] == $order['customer_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($acct['fullname']); ?> (<?php echo $acct['email']; ?>)</option>
                                <?php
                            }
                            ?>
                        </optgroup>
                    </select>

                    <div id="confirm_change-customer-id" style="clear: both; display: none; padding-top: 10px;">
                        Change customer?
                        <div style="padding-top: 5px;">
                            <a href="#" class="yes button blue" style="height: 34px;">Yes</a>&nbsp;&nbsp;&nbsp;<a href="#" class="no button" style="height: 34px;">No</a>
                        </div>
                    </div>

                </fieldset>

            </div>

            <div class="col-md-6">

                <div class="row">
                    <div class="col-md-6">

                        <fieldset>
                            <label>Order Status</label>
                            <select name="order[status]" class="custom_control form-control">
                                <option value="cart" <?php if ($order['status'] == 'cart') { ?>selected="selected"<?php } ?>>Cart</option>
                                <option value="placed" <?php if ($order['status'] == 'placed') { ?>selected="selected"<?php } ?>>Placed</option>
                                <option value="charged" <?php if ($order['status'] == 'charged') { ?>selected="selected"<?php } ?>>Charged</option>
                                <option value="failed" <?php if ($order['status'] == 'failed') { ?>selected="selected"<?php } ?>>Failed</option>
                                <option value="processing" <?php if ($order['status'] == 'processing') { ?>selected="selected"<?php } ?>>Processing</option>
                                <option value="shipped" <?php if ($order['status'] == 'shipped') { ?>selected="selected"<?php } ?>>Shipped</option>
                                <option value="complete" <?php if ($order['status'] == 'complete') { ?>selected="selected"<?php } ?>>Complete</option>
                                <option value="cancelled" <?php if ($order['status'] == 'cancelled') { ?>selected="selected"<?php } ?>>Cancelled</option>
                                <option value="refunded" <?php if ($order['status'] == 'refunded') { ?>selected="selected"<?php } ?>>Refunded</option>
                                <?php if ($order['status'] == 'cloned') { ?>
                                    <option value="cloned" selected="selected">Cloned</option>
                                <?php } ?>
                                <?php if ($order['status'] == 'deleted') { ?>
                                    <option value="deleted" selected="selected">Deleted</option>
                                <?php } ?>
                            </select>
                        </fieldset>

                    </div>
                    <div class="col-md-6">

                        <?php if ($quote['id']) { ?>
                            <fieldset>
                                <label>Quote</label>
                                <div>
                                    <a href="../../quotes/edit/?id=<?php echo $quote['id']; ?>">View Quote</a>
                                </div>
                            </fieldset>
                        <?php } ?>

                    </div>
                </div>

                <fieldset class="form-group">
                    <label>Order Notes</label>
                    <textarea name="notes" class="form-control" style="height: 58px;"><?php echo stripslashes($order['notes']); ?></textarea>
                </fieldset>

                <?php if ($_uccms_ecomm->getSetting('checkout_legal_enabled')) { ?>

                    <fieldset class="form-group">
                        <label>Legal / Policy Agree</label>
                        <select name="legal_agree" class="custom_control form-control">
                            <option value="0">No</option>
                            <option value="1" <?php if ($order['legal_agree'] == 1) { ?>selected="selected"<?php } ?>>Yes</option>
                        </select>
                    </fieldset>

                <?php } ?>

            </div>

        </div>

        <div class="row">

            <div class="col-md-6">

                <a name="billing"></a>

                <h3>Billing</h3>

                <fieldset class="form-group">
                    <label>First Name</label>
                    <input type="text" name="billing_firstname" value="<?php echo stripslashes($order['billing_firstname']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="billing_lastname" value="<?php echo stripslashes($order['billing_lastname']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Company Name</label>
                    <input type="text" name="billing_company" value="<?php echo stripslashes($order['billing_company']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Address</label>
                    <input type="text" name="billing_address1" value="<?php echo stripslashes($order['billing_address1']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="billing_address2" value="<?php echo stripslashes($order['billing_address2']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>City</label>
                    <input type="text" name="billing_city" value="<?php echo stripslashes($order['billing_city']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>State</label>
                    <select name="billing_state" class="custom_control form-control">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $order['billing_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="form-group">
                    <label>Zip</label>
                    <input type="text" name="billing_zip" value="<?php echo stripslashes($order['billing_zip']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Phone</label>
                    <input type="text" name="billing_phone" value="<?php echo stripslashes($order['billing_phone']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Email</label>
                    <input type="text" name="billing_email" value="<?php echo stripslashes($order['billing_email']); ?>" class="form-control" />
                </fieldset>

            </div>

            <div class="col-md-6">

                <a name="shipping"></a>

                <h3 style="float: left;">Shipping</h3>

                <fieldset class="last" style="float: right;">
                    <input id="shipping_same" type="checkbox" name="shipping_same" value="1" <?php if ($order['shipping_same']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Same as billing</label>
                </fieldset>

                <div id="toggle_shipping_same" class="contain" style="clear: right; <?php if ($order['shipping_same']) { ?>display: none;<?php } ?> padding-top: 10px;">

                    <fieldset class="form-group">
                        <label>First Name</label>
                        <input type="text" name="shipping_firstname" value="<?php echo stripslashes($order['shipping_firstname']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="shipping_lastname" value="<?php echo stripslashes($order['shipping_lastname']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Company Name</label>
                        <input type="text" name="shipping_company" value="<?php echo stripslashes($order['shipping_company']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Address</label>
                        <input type="text" name="shipping_address1" value="<?php echo stripslashes($order['shipping_address1']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Address (Apt, Bldg)</label>
                        <input type="text" name="shipping_address2" value="<?php echo stripslashes($order['shipping_address2']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>City</label>
                        <input type="text" name="shipping_city" value="<?php echo stripslashes($order['shipping_city']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>State</label>
                        <select name="shipping_state" class="custom_control form-control">
                            <option value="">Select</option>
                            <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                <option value="<?php echo $state_code; ?>" <?php if ($state_code == $order['shipping_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Zip</label>
                        <input type="text" name="shipping_zip" value="<?php echo stripslashes($order['shipping_zip']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Phone</label>
                        <input type="text" name="shipping_phone" value="<?php echo stripslashes($order['shipping_phone']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Email</label>
                        <input type="text" name="shipping_email" value="<?php echo stripslashes($order['shipping_email']); ?>" class="form-control" />
                    </fieldset>

                </div>

            </div>

        </div>

    </section>

    <footer>
        <input id="submit_main" class="btn btn-primary" type="submit" value="Save" />
        <a class="btn btn-secondary back" href="../">&laquo; Back</a>
    </footer>

</div>

<a name="booking-conflict"></a>

<div id="booking-conflict" class="container" style="display: none;">

    <header style="background-color: #EF5350;">
        <h2 style="font-weight: bold;">Booking Conflict</h2>
    </header>

    <section>

        <div class="contain">

        </div>

    </section>

    <footer>
        <input id="submit_main" class="blue" type="submit" value="Save" />
    </footer>

</div>

<a name="cart"></a>
<div class="cart container legacy">

    <header>
        <h2>Cart</h2>
    </header>

    <?php

    // WAS A QUOTE
    //if ($order['quote'] == 2) {

        // ITEM ARRAY
        $ia = array();

        // GET ITEMS
        $item_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0) ORDER BY `title` ASC, `id` ASC";
        $item_q = sqlquery($item_query);
        while ($item = sqlfetch($item_q)) {

            // GET IMAGE
            $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC LIMIT 1";
            $image = sqlfetch(sqlquery($image_query));

            // HAVE IMAGE
            if ($image['image']) {
                $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
            } else {
                $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
            }

            // ADD TO ITEM ARRAY
            $ia[] = '{
                id: ' .$item['id']. ',
                image: \'' .$image_url. '\',
                text: \'' .str_replace("'", "\'", stripslashes($item['title'])). '\'
            }';

        }

        ?>

        <link rel="stylesheet" href="/extensions/<?=$_uccms_ecomm->Extension?>/css/master/quote_request.css" type="text/css" />
        <style type="text/css">

            #ecommContainer #review .heading {
                display: none;
            }
            #ecommContainer #review .details {
                display: none;
            }

            .selectivity-result-item img, .selectivity-single-selected-item img {
                height: 48px;
                widht: auto;
            }
            .selectivity-single-select input[type="text"] {
                height: auto;
            }

            form.bigtree_dialog_form .overflow .loading {
                padding-top: 15px;
                text-align: center;
            }
            form.bigtree_dialog_form .overflow .data {
                padding-top: 15px;
            }

        </style>

        <script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/selectivity-full.min.js"></script>
        <link rel="stylesheet" href="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/css/lib/selectivity-full.css" type="text/css" />

        <script type="text/javascript">

            $(document).ready(function() {

                // EDIT STORE-TYPE INFO CLICK
                $('#ecommContainer #review .edit_anchor').click(function(e) {
                    var c = $(this).attr('href').replace('#', '');
                    if ($('.container.' +c+ ' section').is(':hidden')) {
                        $('.container.' +c+ ' header').click();
                    }
                });

                // ADD ITEM CLICK
                $('#ecommContainer #review .cart_addItem').click(function(e) {

                    e.preventDefault();

                    // DIALOG
                    BigTreeDialog({
                        title: 'Add Item',
                        content: $('#mc_addItem').html(),
                        //preSubmissionCallback: true,
                        callback: function(params) {
                            if ((params.order_id) && (params.item_id)) {
                                $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add-to-order/', $('form.bigtree_dialog_form').serialize(), function(data) {
                                if (data['success']) {
                                    if ((data['item']['id']) && (typeof _rst == 'object')) {
                                        _rst.push(['event', 'cart-add', data['item']['title']+ ' (#' +data['item']['id']+ ')']);
                                    }
                                    setTimeout(function() {
                                        if ((data['order_id']) && (<?php echo (int)$_REQUEST['id']; ?> == 0)) {
                                            window.location.href = window.location.href+ '?id=' +data['order_id']+ '#cart';
                                        } else {
                                            location.reload();
                                        }
                                    }, 2000);
                                } else {
                                    if (data['err']) {
                                        if (typeof _rst == 'object') {
                                            _rst.push(['event', 'cart-add-error', data['err']]);
                                        }
                                        alert(data['err']);
                                    } else {
                                        alert('Adding failed.');
                                    }
                                }
                                }, 'json');
                            } else {
                                alert('Could not submit data.');
                            }
                        },
                        icon: 'add'
                    });

                    // SELECT PRODUCT
                    $('.bigtree_dialog_window .select_product').selectivity({
                        allowClear: true,
                        items:[<?php echo implode(', ', $ia); ?>],
                        placeholder: 'Select item',
                        templates: {
                            resultItem: function(item) {
                                return (
                                '<div class="selectivity-result-item" data-item-id="' +item.id+ '">' +
                                    '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                                '</div>'
                                );
                            },
                            singleSelectedItem: function(item) {
                                return (
                                    '<span class="selectivity-single-selected-item" data-item-id="' +item.id+ '">' +
                                        '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                                    '</span>'
                                );
                            }
                        }
                    }).on('selectivity-selected', function(e) {
                        $('.selectivity-single-select').height(57);
                        $('form.bigtree_dialog_form .overflow .data').html('').hide();
                        $('form.bigtree_dialog_form .overflow .loading').show();
                        $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add_interface/', {
                                order_id: <?=$order['id']?>,
                                item_id: e.id
                            },
                            function(data) {
                                $('form.bigtree_dialog_form .overflow .loading').hide();
                                $('form.bigtree_dialog_form .overflow .data').html(data).show();
                            },
                            'html'
                        );
                    });

                });

                // ITEM ROLLOVER
                $('#cart .item').hover(
                    function() {
                        $(this).find('.actions').show();
                    }, function() {
                        $(this).find('.actions').hide();
                    }
                );

                // ITEM - EDIT CLICK
                $('#cart .item .actions .edit').click(function(e) {
                    e.preventDefault();
                    $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add_interface/', {
                            order_id: <?=$order['id']?>,
                            order_item_id: $(this).attr('data-id')
                        },
                        function(data) {
                            BigTreeDialog({
                                title: 'Edit Item',
                                content: '<div class="data">' +data+ '</div>',
                                //preSubmissionCallback: true,
                                callback: function(params) {
                                    if ((params.order_id) && (params.item_id)) {
                                        $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add-to-order/', $('form.bigtree_dialog_form').serialize(), function(data2) {
                                            if (data2['success']) {
                                                if ((typeof _rst == 'object') && (data['item']['id'])) {
                                                    _rst.push(['event', 'cart-update', data2['item']['title']+ ' (#' +data2['item']['id']+ ')']);
                                                }
                                                setTimeout(function() {
                                                    location.reload();
                                                }, 2000);

                                            } else {
                                                if (data2['err']) {
                                                    if (typeof _rst == 'object') {
                                                        _rst.push(['event', 'cart-update-error', data['err']]);
                                                    }
                                                    alert(data2['err']);
                                                } else {
                                                    alert('Updating failed.');
                                                }
                                            }
                                        }, 'json');
                                    } else {
                                        alert('Could not submit data.');
                                    }
                                },
                                icon: 'edit'
                            });
                        },
                        'html'
                    );
                });

                // ITEM - DELETE CLICK
                $('#cart .item .actions .delete').click(function(e) {
                    e.preventDefault();
                    if (confirm('Are you sure you want to remove this?')) {
                        window.location.href = './delete-item/?id=' +$(this).attr('data-id')+ '&order_id=<?=$order['id']?>';
                    }
                });

            });

        </script>

        <section>

            <div id="ecommContainer" class="contain">

                <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/review.php'); ?>

                <?php if (!$new_order) { ?>

                    <div style="padding-top: 10px; text-align: right;">
                        <div style="padding-bottom: 10px; font-size: 1.2em;">
                            Paid: $<?=$paid?>
                        </div>
                        <div style="font-size: 1.4em;">
                            Balance: $<?=number_format($balance, 2)?>
                        </div>

                        <?php

                        if (count($transactions) > 0) {
                            $tips = 0;
                            foreach ($transactions as $transaction) {
                                if ($transaction['tip']) {
                                    $tips += $transaction['amount'];
                                }
                            }
                            if ($tips > 0) {
                                ?>
                                <div style="margin-top: 10px;">
                                    Tips: $<?=number_format($tips, 2)?>
                                </div>
                                <?php
                            }
                        }

                        ?>

                    </div>

                <?php } ?>

            </div>

            <div id="mc_addItem" style="display: none;">
                <div class="select_product"></div>
                <div class="loading" style="display: none;">Loading..</div>
                <div class="data" style="display: none;"></div>
            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
            <?php if ($order['zero_balance']) { ?>
                <a href="./zero-balance/?id=<?php echo $order['id']; ?>&active=0" class="button blue unzero_balance float-right">$ Balance</a>
            <?php } else { ?>
                <?php if ($balance > 0) { ?>
                    <a href="./zero-balance/?id=<?php echo $order['id']; ?>&active=1" class="button blue zero_balance float-right">$0 Balance</a>
                    <a href="#" class="button blue pay float-right" data-balance="<?php echo $balance; ?>">Pay Now</a>
                <?php } ?>
            <?php } ?>
        </footer>

    <?php /* } else { ?>

        <section>

            <div class="contain">

                <table id="cart" cellpadding="0" cellspacing="0">

                    <tr>
                        <th class="thumb"></th>
                        <th class="title">Item</th>
                        <th class="price">Price</th>
                        <th class="quantity">Quantity</th>
                        <th class="total">Total</th>
                    </tr>

                    <?php

                    // SUBTOTAL
                    $order['subtotal'] = 0;

                    // ATTRIBUTE ARRAY
                    $attra = array();

                    // AFTER TAX ITEM ARRAY
                    $atia = array();

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // AFTER TAX ITEM
                        if ($oitem['after_tax']) {

                            // ADD TO AFTER TAX ITEM ARRAY
                            $atia[] = $oitem;

                        // NORMAL ITEM
                        } else {

                            // GET ITEM INFO
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                            // ITEMS FOR TRELLO
                            $trello['items'][$oitem['id']] = stripslashes($item['title']). ' | Quantity: ' .number_format($oitem['quantity'], 0);

                            // SETTINGS FOR DISPLAYING ITEM IN CART
                            $item_cart_settings = array(
                            );

                            // DISPLAY ITEM IN CART
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                            unset($item, $oitem);

                        }

                    }

                    ?>

                    <tr class="sub subtotal">
                        <td colspan="5">
                            Subtotal: $<span class="num"><?php echo number_format($order['order_subtotal'], 2); ?></span>
                        </td>
                    </tr>

                    <?php

                    // HAVE DISCOUNT
                    if ($order['order_discount'] > 0.00) {

                        // HAVE COUPON
                        if ($order['coupon_id']) {

                            // GET COUPON INFO
                            $coupon_query = "SELECT * FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (`id`=" .$order['coupon_id']. ")";
                            $coupon_q = sqlquery($coupon_query);
                            $coupon = sqlfetch($coupon_q);

                        }

                        ?>
                        <tr class="sub discount">
                            <td colspan="5">
                                <?php if ($coupon['code']) { ?>(Coupon: <?php echo $coupon['code']; ?>)<?php } ?> Discount: $<span class="num"><?php echo number_format($order['order_discount'], 2); ?></span>
                            </td>
                        </tr>
                        <?php

                    }

                    ?>

                    <tr class="sub tax">
                        <td colspan="5">
                            Tax: $<span class="num"><?php echo number_format($order['order_tax'], 2); ?></span>
                        </td>
                    </tr>

                    <tr class="sub shipping <?php if (count($atia) > 0) { echo 'with_after_tax'; } ?>">
                        <td colspan="5">
                            Shipping: $<span class="num"><?php echo number_format($order['order_shipping'], 2); ?></span>
                        </td>
                    </tr>

                    <?php if ($_uccms_ecomm->getSetting('gratuity_enabled')) { ?>
                        <tr class="sub gratuity">
                            <td colspan="5">
                                (<?php echo ($order['override_gratuity'] ? $order['override_gratuity'] : $_uccms_ecomm->getSetting('gratuity_percent')); ?>%) Gratuity: $<span class="num"><?php echo $order['order_gratuity']; ?></span>
                            </td>
                        </tr>
                    <?php } ?>

                    <?php

                    // HAVE AFTER TAX ITEMS
                    if (count($atia) > 0) {

                        ?>
                        <tr class="sub">
                            <td colspan="5"></td>
                        </tr>
                        <?php

                        // LOOP
                        foreach ($atia as $oitem) {

                            // GET ITEM INFO
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                            // ITEMS FOR TRELLO
                            $trello['items'][$oitem['id']] = stripslashes($item['title']). ' | Quantity: ' .number_format($oitem['quantity'], 0);

                            // SETTINGS FOR DISPLAYING ITEM IN CART
                            $item_cart_settings = array(
                                'after_tax'         => true
                            );

                            // DISPLAY ITEM IN CART
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                            unset($item, $oitem);

                        }

                    }

                    ?>

                    <tr class="sub total">
                        <td colspan="5">
                            Total: $<span class="num"><?php echo number_format($order['order_total'], 2); ?></span>
                        </td>
                    </tr>

                </table>

                <div style="padding-top: 10px; text-align: right;">
                    <div style="padding-bottom: 10px; font-size: 1.2em;">
                        Paid: $<?=number_format($_uccms_ecomm->toFloat($paid), 2)?>
                    </div>
                    <div style="font-size: 1.4em;">
                        Balance: $<?=number_format($_uccms_ecomm->toFloat($balance), 2)?>
                    </div>
                </div>

            </div>

        </section>

    <?php }*/ ?>

</div>

<?php if (!$new_order) { ?>

    <?php

    // INCLUDE STORE-TYPE FORMS
    include(dirname(__FILE__). '/forms/' .$_uccms_ecomm->storeType(). '.php');

    ?>

    </form>

    <div class="container legacy expand">

        <header>
            <h2><i class="fa fa-plus plus-minus" aria-hidden="true"></i> Payment &amp; Shipping Info</h2>
        </header>

        <section>

            <div class="contain">

                <div class="left last">

                    <?php

                    // GET LAST PAYMENT
                    $last_payment = sqlfetch(sqlquery("SELECT * FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`cart_id`=" .$order['id']. ") ORDER BY `dt` DESC LIMIT 1"));

                    // DEFAULT PAYMENT TITLE
                    $payment_title = 'N/A';

                    if ($last_payment['method']) {

                        // GET PAYMENT METHOD INFO
                        $payment_query = "SELECT * FROM `" .$_uccms_ecomm->tables['payment_methods']. "` WHERE (`id`='" .stripslashes($last_payment['method']). "')";
                        $payment = sqlfetch(sqlquery($payment_query));

                        // HAVE PAYMENT TITLE
                        if ($payment['title']) {
                            $payment_title = stripslashes($payment['title']);
                        }

                    }

                    ?>

                    <h3>Payment</h3>

                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 25%;">Method:&nbsp;</td>
                            <td style="width: 75%;"><?php echo $payment_title; ?></td>
                        </tr>

                        <?php if (($last_payment['method']) && ($last_payment['method'] != 'check_cash')) { ?>

                            <tr>
                                <td>Name:&nbsp;</td>
                                <td><?php echo stripslashes($last_payment['name']); ?></td>
                            </tr>
                            <tr>
                                <td>Last Four:&nbsp;</td>
                                <td><?php echo stripslashes($last_payment['lastfour']); ?></td>
                            </tr>
                            <tr>
                                <td>Expiration:&nbsp;</td>
                                <td><?php echo date('m/Y', strtotime($last_payment['expiration'])); ?></td>
                            </tr>

                        <?php } ?>

                    </table>

                </div>

                <div class="right last">

                    <h3>Shipping</h3>

                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="25%">Service:&nbsp;</td>
                            <td width="75%"><?php echo $shipping_title; ?></td>
                        </tr>
                    </table>

                </div>

            </div>

        </section>

    </div>

    <a name="payment"></a>

    <style type="text/css">

        #payment .method {
            clear: both;
            margin-bottom: 10px;
        }
        #payment .method:last-child {
            margin-bottom: 0px;
        }
        #payment .method .radio_button {
            margin-top: -3px;
        }
        #payment .method .info {
            clear: both;
            display: none;
            padding: 10px;
        }
        #payment .method:last-child .info {
            padding-bottom: 0px;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // PAYMENT METHOD SELECT
            $('#payment .method input[name="payment[method]"]').click(function() {
                $('#payment .method .info').hide();
                $(this).closest('.method').find('.info').show();
            });

            // CARD SELECT
            $('#payment .method .payment_profile select[name="payment[profile_id]"').change(function() {
                if ($(this).val() != null) {
                    $('#card_new').hide();
                } else {
                    $('#card_new').show();
                }
            });

        });

    </script>

    <form enctype="multipart/form-data" action="./processes/payment/" method="post">
    <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />

    <div id="payment" class="container legacy expand">
        <header><h2><i class="fa fa-plus plus-minus" aria-hidden="true"></i> Record Payment</h2></header>
        <section>

            <fieldset class="form-group">
                <label>Amount</label>
                $<input type="text" name="amount" value="" class="form-control" />
            </fieldset>

            <?php

            // PAYMENT METHODS
            foreach ($payment_methods as $payment) {

                ?>

                <div class="method">
                    <div class="top">
                        <input type="radio" name="payment[method]" value="<?php echo $payment['id']; ?>" <?php if (($payment['id'] == $vals['payment']['method']) || ($num_payment_methods == 1)) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($payment['title']); ?>
                    </div>
                    <div class="info">

                        <?php if ($payment['id'] == 'check_cash') { ?>

                            <fieldset class="form-group">
                                <label>Check # / Note</label>
                                <input type="text" name="payment[<?php echo $payment['id']; ?>][note]" value="" class="form-control" />
                            </fieldset>

                        <?php } else if ($payment['hide_cc']) { ?>

                            <fieldset class="form-group">
                                <label>Note</label>
                                <input type="text" name="payment[<?php echo $payment['id']; ?>][note]" value="" class="form-control" />
                            </fieldset>

                        <?php } else { ?>

                            <?php if (count($payment_profiles) > 0) { ?>
                                <div class="payment_profile">
                                    <fieldset class="form-group">
                                        <select name="payment[profile_id]" class="custom_control form-control">
                                            <?php foreach ($payment_profiles as $pp) { ?>
                                                <option value="<?php echo $pp['id']; ?>"><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                                            <?php } ?>
                                            <option value="">New Card</option>
                                        </select>
                                    </fieldset>
                                </div>
                            <?php } ?>

                            <div id="card_new" style="<?php if (count($payment_profiles) > 0) { ?>display: none; padding-top: 15px;<?php } ?>">

                                <fieldset class="form-group">
                                    <label>Name On Card *</label>
                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][name]" value="" class="form-control" />
                                </fieldset>

                                <fieldset class="form-group">
                                    <label>Card Number *</label>
                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][number]" value="" class="form-control" />
                                </fieldset>

                                <fieldset class="form-group">
                                    <label>Card Expiration *</label>
                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][expiration]" value="" placeholder="mm/yy" class="form-control" />
                                </fieldset>

                                <fieldset class="form-group">
                                    <label>Card Zip *</label>
                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][zip]" value="" class="form-control" />
                                </fieldset>

                                <fieldset class="form-group">
                                    <label>Card CVV *</label>
                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][cvv]" value="" class="form-control" />
                                </fieldset>

                            </div>

                        <?php } ?>

                    </div>
                </div>

                <?php

            }

            ?>

            <fieldset style="margin-top: 20px;">
                <input type="checkbox" name="tip" value="1" />
                <label class="for_checkbox">This is a tip</label>
            </fieldset>

        </section>
        <footer>
            <input class="btn btn-primary" type="submit" value="Process" /> <small style="display: inline-block; margin-top: 12px;">Only click once.</small>
        </footer>
    </div>

    </form>

    <a name="scheduled_payments"></a>

    <style type="text/css">

        #scheduled_payments .scheduled_payment_dt_due {
            width: 120px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_amount {
            width: 100px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_profile {
            width: 120px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_title {
            width: 350px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_email_on {
            width: 140px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_edit {
            width: 50px;
        }
        #scheduled_payments .scheduled_payment_delete {
            width: 50px;
        }

        #scheduled_payments .scheduled_payments ul .item {
            height: auto;
        }
        #scheduled_payments .scheduled_payments ul .item .edit {
            padding: 10px 20px 0;
            border-top: 1px dashed #ddd;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // ADD TRACKING BUTTON CLICK
            $('#scheduled_payments header .button.add_scheduled_payment').click(function(e) {
                e.preventDefault();
                $('#scheduled_payments .contain.add_scheduled_payment').toggle();
            });

            // EDIT ICON CLICK
            $('#scheduled_payments .scheduled_payments ul .item .icon_edit').click(function(e) {
                e.preventDefault();
                $(this).closest('.item').find('.edit').toggle();
            });

        });

    </script>

    <div id="scheduled_payments" class="container">

        <header>
            <h2>Scheduled Payments</h2>
            <a href="#" class="button add_scheduled_payment">Add Scheduled Payment</a>
        </header>

        <section class="contain add_scheduled_payment" style="display: none;">

            <form enctype="multipart/form-data" action="./processes/add_scheduled_payment/" method="post">
            <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />

            <div class="left last">

                <fieldset class="form-group">
                    <label>Due Date</label>
                    <div>
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'add_scheduled_payment_due_date',
                            'key'       => 'dt',
                            'value'     => '',
                            'required'  => true,
                            'options'   => array(
                                'default_today' => false
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <label>Amount</label>
                    $<input type="text" name="amount" value="0.00" class="form-group" style="display: inline; width: 100px;" />
                </fieldset>

                <?php if (count($payment_profiles) > 0) { ?>
                    <fieldset class="form-group">
                        <label>Automatically Charge</label>
                        <select name="payment_profile_id" class="custom_control form-control">
                            <option value="0">Don't Auto-Charge</option>
                            <?php foreach ($payment_profiles as $pp) { ?>
                                <option value="<?php echo $pp['id']; ?>"><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                <?php } ?>

            </div>

            <div class="right last">

                <fieldset class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" value="" class="form-group" style="width: 300px;" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Send reminder email this many days before due date:</label>
                    <select name="email_days_before" class="custom_control form-control">
                        <?php for ($i=0; $i<=30; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <input class="blue" type="submit" value="Add" />

            </div>

            </form>

        </section>

        <div class="scheduled_payments table" style="margin: 0px; border: 0px none;">

            <header style="clear: both;">
                <span class="scheduled_payment_dt_due">Due Date</span>
                <span class="scheduled_payment_amount">Amount</span>
                <span class="scheduled_payment_profile">Charge</span>
                <span class="scheduled_payment_title">Title</span>
                <span class="scheduled_payment_email_on">Email On</span>
                <span class="scheduled_payment_edit">Edit</span>
                <span class="scheduled_payment_delete">Delete</span>
            </header>

            <?php

            // GET SCHEDULED_PAYMENTS
            $spayments_query = "SELECT * FROM `" .$_uccms_ecomm->tables['scheduled_payments']. "` WHERE (`order_id`=" .$order['id']. ") ORDER BY `dt_due` ASC";
            $spayments_q = sqlquery($spayments_query);

            // HAVE SCHEDULED PAYMENTS
            if (sqlrows($spayments_q) > 0) {

                ?>

                <ul class="items">

                    <?php



                    // LOOP
                    while ($spayment = sqlfetch($spayments_q)) {

                        // HAVE PAYMENT PROFILE ID

                        ?>

                        <li class="item">

                            <div class="contain">
                                <section class="scheduled_payment_dt_due">
                                    <?php echo date('n/j/Y', strtotime($spayment['dt_due'])); ?>
                                </section>
                                <section class="scheduled_payment_amount">
                                    $<?php echo number_format($spayment['amount'], 2); ?>
                                </section>
                                <section class="scheduled_payment_profile">
                                    <?php
                                    if ($spayment['payment_profile_id']) {
                                        if ($payment_profiles[$spayment['payment_profile_id']]['type']) { echo ucwords($payment_profiles[$spayment['payment_profile_id']]['type']). ' - '; } echo $payment_profiles[$spayment['payment_profile_id']]['lastfour'];
                                    } else {
                                        echo 'No';
                                    }
                                    ?>
                                </section>
                                <section class="scheduled_payment_title">
                                    <?php echo stripslashes($spayment['title']); ?>
                                </section>
                                <section class="scheduled_payment_email_on">
                                    <span style="<?php if (($spayment['dt_email'] <= date('Y-m-d')) || ($spayment['emailed'])) { echo 'opacity: .6;'; } ?>">
                                        <?php echo date('n/j/Y', strtotime($spayment['dt_email'])); if ($spayment['emailed']) { echo ' <i class="fa fa-check" title="Email sent"></i>'; } ?>
                                    </span>
                                </section>
                                <section class="scheduled_payment_edit">
                                    <a href="#" title="Edit" class="icon_edit"></a>
                                </section>
                                <section class="scheduled_payment_delete">
                                    <a onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');" title="Delete" class="icon_delete" href="./processes/delete_scheduled_payment/?order_id=<?php echo $order['id']; ?>&id=<?php echo $spayment['id']; ?>"></a>
                                </section>
                            </div>

                            <div class="edit contain" style="display: none;">

                                <form enctype="multipart/form-data" action="./processes/edit_scheduled_payment/" method="post">
                                <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
                                <input type="hidden" name="id" value="<?php echo $spayment['id']; ?>" />

                                <div class="left">

                                    <fieldset class="form-group">
                                        <label>Due Date</label>
                                        <div>
                                            <?php

                                            // FIELD VALUES
                                            $field = array(
                                                'id'        => 'scheduled_payment_due_date',
                                                'key'       => 'dt',
                                                'value'     => $spayment['dt_due'],
                                                'required'  => true,
                                                'options'   => array(
                                                    'default_today' => false
                                                )
                                            );

                                            // INCLUDE FIELD
                                            include(BigTree::path('admin/form-field-types/draw/date.php'));

                                            ?>
                                        </div>
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Amount</label>
                                        $<input type="text" name="amount" value="<?php echo $spayment['amount']; ?>" class="form-group" style="display: inline; width: 100px;" />
                                    </fieldset>

                                    <?php if (count($payment_profiles) > 0) { ?>
                                        <fieldset class="form-group">
                                            <label>Automatically Charge</label>
                                            <select name="payment_profile_id" class="custom_control form-control">
                                                <option value="0">Don't Auto-Charge</option>
                                                <?php foreach ($payment_profiles as $pp) { ?>
                                                    <option value="<?php echo $pp['id']; ?>" <?php if ($pp['id'] == $spayment['payment_profile_id']) { ?>selected="selected"<?php } ?>><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    <?php } ?>

                                </div>

                                <div class="right">

                                    <fieldset class="form-group">
                                        <label>Title</label>
                                        <input type="text" name="title" value="<?php echo stripslashes($spayment['title']); ?>" class="form-group" style="width: 300px;" />
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Send reminder email this many days before due date:</label>
                                        <select name="email_days_before" class="custom_control form-control">
                                            <?php for ($i=0; $i<=30; $i++) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($i == $spayment['email_days_before']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>

                                    <input class="blue" type="submit" value="Update" />

                                </div>

                                </form>

                            </div>

                        </li>
                        <?php
                    }

                    ?>

                </ul>

                <?php

            // NO SCHEDULED PAYMENTS
            } else {
                ?>
                <div style="padding: 10px; text-align: center; font-size: .75em;">
                    No scheduled payments.
                </div>
                <?php
            }

            ?>

        </div>

    </div>

    <a name="transactions"></a>

    <style type="text/css">

        #transactions .transaction_dt {
            width: 190px;
            text-align: left;
        }
        #transactions .transaction_amount {
            width: 100px;
            text-align: left;
        }
        #transactions .transaction_method {
            width: 120px;
            text-align: left;
        }
        #transactions .transaction_status {
            width: 120px;
            text-align: left;
        }
        #transactions .transaction_note {
            width: 250px;
            text-align: left;
        }
        #transactions .transaction_refund {
            width: 50px;
        }
        #transactions .transaction_edit {
            width: 50px;
        }
        #transactions .transaction_delete {
            width: 50px;
        }

        #transactions .transactions ul .item {
            height: auto;
        }
        #transactions .transactions ul .item .edit {
            padding: 10px 20px 0;
            border-top: 1px dashed #ddd;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // EDIT ICON CLICK
            $('#transactions .transactions ul .item .icon_edit').click(function(e) {
                e.preventDefault();
                $(this).closest('.item').find('.edit').toggle();
            });

        });

    </script>

    <div id="transactions" class="container">

        <header>
            <h2>Transactions</h2>
        </header>

        <div class="transactions table" style="margin: 0px; border: 0px none;">

            <header style="clear: both;">
                <span class="transaction_dt">Date / Time</span>
                <span class="transaction_amount">Amount</span>
                <span class="transaction_method">Method</span>
                <span class="transaction_status">Status</span>
                <span class="transaction_note">Note</span>
                <span class="transaction_refund"><?php if ($_uccms['_account']->gateway->Service == 'stripe') { ?>Refund<?php } ?></span>
                <span class="transaction_edit">Edit</span>
                <span class="transaction_delete">Delete</span>
            </header>

            <?php

            // HAVE TRANSACTIONS
            if (count($transactions) > 0) {

                ?>

                <ul class="items">

                    <?php

                    // LOOP
                    foreach ($transactions as $transaction) {

                        ?>

                        <li class="item">

                            <div class="contain">
                                <section class="transaction_dt">
                                    <?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?>
                                </section>
                                <section class="transaction_amount">
                                    $<?php echo number_format($transaction['amount'], 2); ?><?php if ($transaction['tip']) { ?> (Tip)<?php } ?>
                                </section>
                                <section class="transaction_method">
                                    <?php echo stripslashes($payment_methods[$transaction['method']]['title']); ?>
                                </section>
                                <section class="transaction_status">
                                    <?php echo ucwords($transaction['status']); ?>
                                </section>
                                <section class="transaction_note">
                                    <?php echo stripslashes($transaction['note'] ? $transaction['note'] : $transaction['message']); ?>
                                </section>
                                <section class="transaction_refund">
                                    <?php if (($transaction['status'] == 'charged') && ($transaction['method'] == 'stripe')) { ?>
                                        <a href="#" title="Refund" class="icon_restore" data-transaction_id="<?php echo $transaction['id']; ?>" data-amount="<?php echo $transaction['amount']; ?>"></a>
                                    <?php } ?>
                                </section>
                                <section class="transaction_edit">
                                    <a href="#" title="Edit" class="icon_edit"></a>
                                </section>
                                <section class="transaction_delete">
                                    <a onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');" title="Delete" class="icon_delete" href="./processes/delete_transaction/?order_id=<?php echo $order['id']; ?>&id=<?php echo $transaction['id']; ?>"></a>
                                </section>
                            </div>

                            <div class="edit contain" style="display: none;">

                                <form enctype="multipart/form-data" action="./processes/edit_transaction/" method="post">
                                <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
                                <input type="hidden" name="id" value="<?php echo $transaction['id']; ?>" />

                                <div style="width: 50%;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <?php if ($transaction['ip']) { ?>
                                            <tr>
                                                <td>IP:</td>
                                                <td><?php echo long2ip($transaction['ip']); ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($transaction['transaction_id']) { ?>
                                            <tr>
                                                <td>Transaction ID:</td>
                                                <td><?php echo $transaction['transaction_id']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($transaction['name']) { ?>
                                            <tr>
                                                <td>Name:</td>
                                                <td><?php echo $transaction['name']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($transaction['lastfour']) { ?>
                                            <tr>
                                                <td>Last Four:</td>
                                                <td><?php echo $transaction['lastfour']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if (($transaction['expiration']) && ($transaction['expiration'] != '0000-00-00')) { ?>
                                            <tr>
                                                <td>Expiration:</td>
                                                <td><?php echo date('m/d/Y', strtotime($transaction['expiration'])); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>

                                </div>

                                <div class="left">

                                    <fieldset class="form-group">
                                        <label>Date / Time</label>
                                        <div>
                                            <?php

                                            // FIELD VALUES
                                            $field = array(
                                                'id'        => 'transaction_dt',
                                                'key'       => 'dt',
                                                'value'     => $transaction['dt'],
                                                'required'  => true,
                                                'options'   => array(
                                                    'default_today' => false
                                                )
                                            );

                                            // INCLUDE FIELD
                                            include(BigTree::path('admin/form-field-types/draw/datetime.php'));

                                            ?>
                                        </div>
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Amount</label>
                                        $<input type="text" name="amount" value="<?php echo $transaction['amount']; ?>" class="form-group" style="display: inline; width: 100px;" />
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Method</label>
                                        <select name="method" class="custom_control form-control">
                                            <?php foreach ($payment_methods as $payment_method) { ?>
                                                <option value="<?php echo $payment_method['id']; ?>" <?php if ($transaction['method'] == $payment_method['id']) { ?>selected="selected"<?php } ?>><?php echo $payment_method['title']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>

                                </div>

                                <div class="right">

                                    <fieldset class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="custom_control form-control">
                                            <option value="captured" <?php if ($transaction['status'] == 'captured') { ?>selected="selected"<?php } ?>>Captured</option>
                                            <option value="charged" <?php if ($transaction['status'] == 'charged') { ?>selected="selected"<?php } ?>>Charged</option>
                                            <option value="failed" <?php if ($transaction['status'] == 'failed') { ?>selected="selected"<?php } ?>>Failed</option>
                                            <option value="voided" <?php if ($transaction['status'] == 'voided') { ?>selected="selected"<?php } ?>>Voided</option>
                                            <option value="refunded" <?php if ($transaction['status'] == 'refunded') { ?>selected="selected"<?php } ?>>Refunded</option>
                                        </select>
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label>Note</label>
                                        <textarea name="note" style="height: 40px;" class="form-group"><?php echo stripslashes($transaction['note'] ? $transaction['note'] : $transaction['message']); ?></textarea>
                                    </fieldset>

                                    <input class="blue" type="submit" value="Update" />

                                </div>

                                </form>

                            </div>

                        </li>
                        <?php
                    }

                    ?>

                </ul>

                <?php

            // NO TRANSACTIONS
            } else {
                ?>
                <div style="padding: 10px; text-align: center; font-size: .75em;">
                    No transactions.
                </div>
                <?php
            }

            ?>

        </div>

    </div>

    <?php if (count($transactions) > 0) { ?>

        <a name="refund"></a>

        <style type="text/css">

            #refund .method {
                clear: both;
                margin-bottom: 10px;
            }
            #refund .method:last-child {
                margin-bottom: 0px;
            }
            #refund .method .radio_button {
                margin-top: -3px;
            }
            #refund .method .info {
                clear: both;
                display: none;
                padding: 10px;
            }
            #refund .method:last-child .info {
                padding-bottom: 0px;
            }
            #refund footer {
                display: none;
            }

        </style>

        <script type="text/javascript">

            $(document).ready(function() {

                // TRANSACTION REFUND CLICK
                $('#transactions .transaction_refund a').click(function(e) {
                    e.preventDefault();

                    var transaction_id = $(this).attr('data-transaction_id');
                    var amount = $(this).attr('data-amount');

                    // HAVE TRANSACTION ID
                    if (transaction_id) {

                        $('#refund input[name="transaction_id"]').val(transaction_id);
                        $('#refund .refund input[name="amount"]').val(amount).attr('placeholder', amount);

                        if (!$('#refund .refund').is(':visible')) {
                            $('#refund.container.expand header').click();
                        }

                        $('#refund.container.expand section').show();

                        $('#refund .select-transaction').hide();
                        $('#refund .refund').show();
                        $('#refund footer').show();

                        location.hash = '#refund';

                    } else {
                        alert('Could not determine transaction ID.');
                    }

                });

                // REFUND HEADER CLICK
                $('#refund.container.expand header').click(function(e) {
                    var transaction_id = $('#refund input[name="transaction_id"]').val();
                    if (!transaction_id) {
                        $('#refund footer').hide();
                    }
                });

            });

        </script>

        <form enctype="multipart/form-data" action="./processes/refund/" method="post">

        <div id="refund" class="container legacy expand">
            <header><h2><i class="fa fa-plus plus-minus" aria-hidden="true"></i> Refund</h2></header>
            <section>

                <input type="hidden" name="transaction_id" value="" />

                <div class="select-transaction">
                    <?php if ($_uccms['_account']->gateway->Service == 'stripe') { ?>
                        Click "Refund" on a transaction above.
                    <?php } else { ?>
                        Refunds are currently only supported on the Stripe payment gateway.
                    <?php } ?>
                </div>

                <div class="refund" style="display: none;">

                    <fieldset class="form-group">
                        <label>Amount</label>
                        $<input type="text" name="amount" value="" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Note</label>
                        <textarea name="note" style="height: 48px;" class="form-group"></textarea>
                    </fieldset>

                </div>

            </section>
            <footer>
                <input class="blue" type="submit" value="Process" /> <small style="display: inline-block; margin-top: 12px;">Only click once.</small>
            </footer>
        </div>

        </form>

    <?php } ?>

    <?php

    // DOWNLOAD LABEL
    if ($_REQUEST['download_shipping_label']) {
        ?>
        <iframe src="../ship/generate_shipstation_label/?id=<?php echo $_REQUEST['download_shipping_label']; ?>&download=true&no_hf=true" style="width: 0px; height: 0px;"></iframe>
        <?php
    }

    ?>

    <a name="shipments"></a>

    <style type="text/css">

        #shipments .shipment_date {
            width: 100px;
            text-align: left;
        }
        #shipments .shipment_service {
            width: 300px;
            text-align: left;
        }
        #shipments .shipment_amount {
            width: 100px;
            text-align: left;
        }
        #shipments .shipment_tracking {
            width: 250px;
            text-align: left;
        }
        #shipments .shipment_label {
            width: 100px;
        }
        #shipments .shipment_delete {
            width: 90px;
        }

    </style>

    <script type="text/javascript">

        $('document').ready(function() {

            // ADD TRACKING BUTTON CLICK
            $('#shipments header .button.add_tracking').click(function(e) {
                e.preventDefault();
                $('#shipments .contain.create_shipment').hide();
                $('#shipments .contain.add_tracking').toggle();
                if ($('#shipments .contain.add_tracking').css('display') == 'block') {
                    $('#shipments section.add').show();
                } else {
                    $('#shipments section.add').hide();
                }
            });

            // CREATE SHIPMENT BUTTON CLICK
            $('#shipments header .button.create_shipment').click(function(e) {
                e.preventDefault();
                $('#shipments .contain.add_tracking').hide();
                $('#shipments .contain.create_shipment').toggle();
                if ($('#shipments .contain.create_shipment').css('display') == 'block') {
                    $('#shipments section.add').show();
                } else {
                    $('#shipments section.add').hide();
                }
            });

            // SHIPPING CARRIER CHANGE
            $('#shipments .contain select[name="carrier"]').change(function() {
                $('#shipments .contain fieldset.shipping_service').hide();
                $('#shipments .contain fieldset.shipping_service.' +$(this).val()).show();
            });

        });

    </script>

    <div id="shipments" class="container">

        <header>
            <h2>Shipments</h2>
            <a class="button add_tracking" href="#">Add Tracking #</a>
            <?php if ($_uccms_ecomm->getSetting('shipstation_enabled')) { ?>
                <a class="button create_shipment" href="#">Create Shipment & Label</a>
            <?php } ?>
        </header>

        <section class="add" style="display: none;">

            <div class="contain add_tracking" style="display: none;">

                <form enctype="multipart/form-data" action="./processes/add_tracking/" method="post">
                <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />

                <div class="left">

                    <fieldset class="form-group">
                        <label>Carrier</label>
                        <select name="carrier" class="custom_control form-control">
                            <option value="">Required</option>
                            <?php foreach ($sma as $sm_id => $sm) { ?>
                                <option value="<?php echo $sm_id; ?>" <?php if ($sm_id == $order['shipping_carrier']) { ?>selected="selected"<?php } ?>><?php echo $sm['title']; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <?php foreach ($sma as $sm_id => $sm) { ?>
                        <fieldset class="shipping_service <?php echo $sm_id; ?>" style="<?php if ($sm_id != $order['shipping_carrier']) { ?>display: none;<?php } ?>">
                            <label>Service</label>
                            <?php if (is_array($sm['services'])) { ?>
                                <select name="service[<?php echo $sm_id; ?>]" class="custom_control form-control">
                                    <?php foreach ($sm['services'] as $service_id => $service) { ?>
                                        <option value="<?php echo $service_id; ?>" <?php if ($service_id == $order['shipping_service']) { ?>selected="selected"<?php } ?>><?php echo $service['title']; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } else { ?>
                                No services for selected carrier.
                            <?php } ?>
                        </fieldset>
                    <?php } ?>

                    <fieldset class="form-group">
                        <label>Tracking Number</label>
                        <input type="text" name="tracking" value="" class="form-control" />
                    </fieldset>

                    <fieldset class="last">
                        <label>Shipped On</label>
                        <div>
                            <?php

                            // SET DEFAULT DATE FORMAT IF NONE SPECIFIED
                            if (!$bigtree["config"]["date_format"]) {
                                $bigtree["config"]["date_format"] = 'm/d/Y';
                            }

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'ship_date',
                                'key'       => 'ship_date',
                                'value'     => '',
                                'required'  => true,
                                'options'   => array(
                                    'default_today' => true
                                )
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/date.php'));

                            ?>
                        </div>
                    </fieldset>

                </div>

                <div class="right">

                    <fieldset class="form-group">
                        <label>Shipping Cost</label>
                        $<input type="text" name="cost" value="0.00" class="form-group" style="display: inline; width: 100px;" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Order Status</label>
                        <select name="status" class="custom_control form-control">
                            <option value="cart">Cart</option>
                            <option value="placed">Placed</option>
                            <option value="charged">Charged</option>
                            <option value="failed">Failed</option>
                            <option value="processing">Processing</option>
                            <option value="shipped" selected="selected">Shipped</option>
                            <option value="complete">Complete</option>
                            <option value="cancelled">Cancelled</option>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <input type="checkbox" name="email" value="1" checked="checked" />
                        <label class="for_checkbox">Email customer</label>
                    </fieldset>

                    <input class="blue" type="submit" value="Save" />

                </div>

                </form>

            </div>

            <?php

            // SHIPSTATION ENABLED
            if ($_uccms_ecomm->getSetting('shipstation_enabled')) {

                // LOAD REQUIRED LIBRARIES
                require_once(SERVER_ROOT. 'uccms/includes/libs/unirest/Unirest.php');
                require_once(EXTENSION_ROOT. 'classes/shipstation.php');

                // DISABLE UNIREST SSL VALIDATION
                Unirest::verifyPeer(false);

                // INITIALIZE SHIPSTATION
                $shipStation = new Shipstation();
                $shipStation->setSsApiKey($_uccms_ecomm->getSetting('shipstation_api_key'));
                $shipStation->setSsApiSecret($_uccms_ecomm->getSetting('shipstation_api_secret'));

                // GET CART INFO (INCLUDING WEIGHT)
                $csi = $_uccms_ecomm->cartShippingInfo($order['id'], $citems);

                ?>

                <script type="text/javascript">

                    $(document).ready(function() {

                        // CREATE SHIPMENT FORM SUBMIT
                        $('#form_create_shipment').submit(function(e) {
                            e.preventDefault();
                            $('#form_create_shipment input[type="submit"]').prop('disabled', true).val('Creating..');
                            $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension;?>/ajax/admin/orders/shipments/create_shipment-shipstation/', $('#form_create_shipment').serialize(), function(data) {
                                $('#form_create_shipment input[type="submit"]').prop('disabled', false).val('Create');
                                if (data['success']) {
                                    document.location.href = '?id=<?php echo $order['id']; ?>&download_shipping_label=' +data['shipment_id']+ '#shipments';
                                } else {
                                    if (data['error']) {
                                        alert(data['error']);
                                    } else {
                                        alert('Something went wrong. Please try again.');
                                    }
                                }
                            }, 'json');
                        });

                    });

                </script>

                <div class="contain create_shipment" style="display: none;">

                    <form id="form_create_shipment" enctype="multipart/form-data" action="" method="post">
                    <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />

                    <div class="contain">

                        <div class="left">

                            <fieldset class="form-group">
                                <label>Carrier</label>
                                <?php

                                // GET CARRIERS
                                $carriers = $shipStation->getCarriers();

                                // REQUEST SUCCESSFUL
                                if ($carriers['success']) {

                                    // HAVE CARRIERS
                                    if (count($carriers['response']) > 0) {

                                        ?>

                                        <select name="carrier" class="custom_contrl form-group">
                                            <option value="">Required</option>
                                            <?php foreach ($carriers['response'] as $carrier) { ?>
                                                <option value="<?php echo $carrier['code']; ?>" <?php if ($carrier['code'] == $order['shipping_carrier']) { ?>selected="selected"<?php } ?>><?php echo $carrier['name']; ?></option>
                                            <?php } ?>
                                        </select>

                                        <?php

                                    // NO CARRIERS
                                    } else {
                                        echo 'You must set up at least one carrier in ShipStation.';
                                    }

                                // REQUEST FAILED
                                } else {
                                    echo 'Failed to retrieve list of carriers from ShipStation.';
                                }

                                ?>

                            </fieldset>

                            <?php

                            // CARRIERS REQUEST SUCCESSFUL
                            if ($carriers['success']) {

                                // HAVE CARRIERS
                                if (count($carriers['response']) > 0) {

                                    // GET SHIPSTATION CARRIERS & SERVICES SETTINGS
                                    //$csa = $_uccms_ecomm->shipstationCarriersServices();

                                    // LOOP
                                    foreach ($carriers['response'] as $carrier) {

                                        // GET SERVICES
                                        $services = $shipStation->carrierServices($carrier['code']);

                                        ?>

                                        <fieldset class="shipping_service <?php echo $carrier['code']; ?>" style="<?php if ($carrier['code'] != $order['shipping_carrier']) { ?>display: none;<?php } ?>">
                                            <label>Service</label>

                                            <?php

                                            // REQUEST SUCCESSFUL
                                            if ($services['success']) {

                                                // HAVE SERVICES
                                                if (count($services['response']) > 0) {

                                                    ?>

                                                    <select name="service[<?php echo $carrier['code']; ?>]" class="custom_contorl form-group">

                                                        <?php

                                                        // LOOP
                                                        foreach ($services['response'] as $service) {
                                                            if ($service['international']) $service['code'] = $service['code']. '_int';
                                                            ?>
                                                            <option value="<?php echo $service['code']; ?>" <?php if ($service['code'] == $order['shipping_service']) { ?>selected="selected"<?php } ?>><?php echo $service['name']; ?></option>
                                                            <?php
                                                        }

                                                        ?>

                                                    </select>

                                                    <?php

                                                // NO SERVICES
                                                } else {
                                                    echo 'This carrier does not have any available services.';
                                                }

                                            // REQUEST FAILED
                                            } else {
                                                echo 'Failed to retrieve list of services for carrier.';
                                            }

                                            ?>

                                        </fieldset>

                                        <?php

                                    }

                                }

                            }

                            ?>

                            <fieldset class="last">
                                <label>Shipping On</label>
                                <div>
                                    <?php

                                    // SET DEFAULT DATE FORMAT IF NONE SPECIFIED
                                    if (!$bigtree["config"]["date_format"]) {
                                        $bigtree["config"]["date_format"] = 'm/d/Y';
                                    }

                                    // FIELD VALUES
                                    $field = array(
                                        'id'        => 'ship_date_ss',
                                        'key'       => 'ship_date',
                                        'value'     => '',
                                        'required'  => true,
                                        'options'   => array(
                                            'default_today' => true
                                        )
                                    );

                                    // INCLUDE FIELD
                                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                                    ?>
                                </div>
                            </fieldset>

                        </div>

                        <div class="right">

                            <fieldset class="form-group">
                                <label>Weight</label>
                                <input type="text" name="weight" value="<?php echo $csi['weight']; ?>" class="form-group" style="display: inline-block; width: 50px;" /> lbs
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Dimensions</label>
                                <input type="text" name="size_l" value="" class="form-group" style="display: inline-block; width: 50px;" /> x <input type="text" name="size_w" value="" style="display: inline-block; width: 50px;" /> x <input type="text" name="size_h" value="" style="display: inline-block; width: 50px;" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Order Status</label>
                                <select name="status" class="custom_control form-control">
                                    <option value="cart">Cart</option>
                                    <option value="placed">Placed</option>
                                    <option value="charged">Charged</option>
                                    <option value="failed">Failed</option>
                                    <option value="processing">Processing</option>
                                    <option value="shipped" selected="selected">Shipped</option>
                                    <option value="complete">Complete</option>
                                    <option value="cancelled">Cancelled</option>
                                </select>
                            </fieldset>

                            <fieldset class="form-group">
                                <input type="checkbox" name="email" value="1" checked="checked" />
                                <label class="for_checkbox">Email customer</label>
                            </fieldset>

                        </div>

                    </div>

                    <div class="contain">

                        <div class="left">

                            <h3>Ship From</h3>

                            <fieldset class="form-group">
                                <label>First Name</label>
                                <input type="text" name="from_firstname" value="<?php echo $_uccms_ecomm->getSetting('shipping_from_firstname'); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="from_lastname" value="<?php echo $_uccms_ecomm->getSetting('shipping_from_lastname'); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Company Name</label>
                                <input type="text" name="from_company" value="<?php echo $_uccms_ecomm->getSetting('shipping_from_company'); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Address</label>
                                <input type="text" name="from_address1" value="<?php echo $_uccms_ecomm->getSetting('shipping_from_address1'); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Address (Apt, Bldg)</label>
                                <input type="text" name="from_address2" value="<?php echo $_uccms_ecomm->getSetting('shipping_from_address2'); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>City</label>
                                <input type="text" name="from_city" value="<?php echo $_uccms_ecomm->getSetting('shipping_from_city'); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>State</label>
                                <select name="from_state" class="custom_control form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!$order['shipping_state']) $order['shipping_state'] = $order['billing_state'];
                                    foreach (BigTree::$StateList as $state_code => $state_name) {
                                        ?>
                                        <option value="<?php echo $state_code; ?>" <?php if ($state_code == $_uccms_ecomm->getSetting('shipping_from_state')) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Zip</label>
                                <input type="text" name="from_zip" value="<?php echo $_uccms_ecomm->getSetting('shipping_from_zip'); ?>" class="form-control" />
                            </fieldset>

                        </div>

                        <div class="right">

                            <h3>Ship To</h3>

                            <fieldset class="form-group">
                                <label>First Name</label>
                                <input type="text" name="to_firstname" value="<?php echo ($order['shipping_firstname'] ? stripslashes($order['shipping_firstname']) : stripslashes($order['billing_firstname'])); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="to_lastname" value="<?php echo ($order['shipping_lastname'] ? stripslashes($order['shipping_lastname']) : stripslashes($order['billing_lastname'])); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Company Name</label>
                                <input type="text" name="to_company" value="<?php echo ($order['shipping_company'] ? stripslashes($order['shipping_company']) : stripslashes($order['billing_company'])); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Address</label>
                                <input type="text" name="to_address1" value="<?php echo ($order['shipping_address1'] ? stripslashes($order['shipping_address1']) : stripslashes($order['billing_address1'])); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Address (Apt, Bldg)</label>
                                <input type="text" name="to_address2" value="<?php echo ($order['shipping_address2'] ? stripslashes($order['shipping_address2']) : stripslashes($order['billing_address2'])); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>City</label>
                                <input type="text" name="to_city" value="<?php echo ($order['shipping_city'] ? stripslashes($order['shipping_city']) : stripslashes($order['billing_city'])); ?>" class="form-control" />
                            </fieldset>

                            <fieldset class="form-group">
                                <label>State</label>
                                <select name="to_state" class="custom_control form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!$order['shipping_state']) $order['shipping_state'] = $order['billing_state'];
                                    foreach (BigTree::$StateList as $state_code => $state_name) {
                                        ?>
                                        <option value="<?php echo $state_code; ?>" <?php if ($state_code == $order['shipping_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </fieldset>

                            <fieldset class="form-group">
                                <label>Zip</label>
                                <input type="text" name="to_zip" value="<?php echo ($order['shipping_zip'] ? stripslashes($order['shipping_zip']) : stripslashes($order['billing_zip'])); ?>" class="form-control" />
                            </fieldset>

                        </div>

                    </div>

                    <input class="blue" type="submit" value="Create" />

                    </form>

                </div>

            <?php } ?>

        </section>

        <div class="shipments table" style="margin: 0px; border: 0px none;">

            <header style="clear: both;">
                <span class="shipment_date">Date</span>
                <span class="shipment_service">Service</span>
                <span class="shipment_amount">Amount</span>
                <span class="shipment_tracking">Tracking</span>
                <span class="shipment_label">Label</span>
                <span class="shipment_delete">Delete</span>
            </header>

            <?php

            // GET SHIPMENTS
            $shipments_query = "SELECT * FROM `" .$_uccms_ecomm->tables['order_shipments']. "` WHERE (`order_id`=" .$order['id']. ") AND (`voided`=0) ORDER BY `dt_created` ASC";
            $shipments_q = sqlquery($shipments_query);

            // HAVE SHIPMENTS
            if (sqlrows($shipments_q) > 0) {

                ?>

                <ul class="items">

                    <?php

                    // LOOP
                    while ($shipment = sqlfetch($shipments_q)) {

                        ?>
                        <li class="item">
                            <section class="shipment_date">
                                <?php if ($shipment['ship_date']) { echo date('n/j/Y', strtotime($shipment['ship_date'])); } else { echo date('n/j/Y', strtotime($shipment['dt_created'])); } ?>
                            </section>
                            <section class="shipment_service">
                                <?php echo strtoupper($shipment['carrier']); if ($shipment['service']) { echo ' (' .ucwords(str_replace('_', ' ', $shipment['service'])). ')'; } ?>
                            </section>
                            <section class="shipment_amount">
                                <?php if (($shipment['cost']) && ($shipment['cost'] != '0.00')) { echo '$' .number_format($shipment['cost'], 2); } ?>
                            </section>
                            <section class="shipment_tracking">
                                <?php if ($shipment['tracking']) { ?>
                                    <a href="<?php echo $_uccms_ecomm->trackingURL($shipment['carrier'], $shipment['tracking']); ?>" target="_blank"><?php echo $shipment['tracking']; ?></a>
                                <?php } ?>
                            </section>
                            <section class="shipment_label">
                                <?php if ($shipment['label_data']) { ?><a href="../ship/generate_shipstation_label/?id=<?php echo $shipment['id']; ?>&no_hf=true" target="_blank">View</a><?php } ?>
                            </section>
                            <section class="shipment_delete">
                                <a href="./processes/delete_shipment/?order_id=<?php echo $order['id']; ?>&id=<?php echo $shipment['id']; ?>" class="icon_delete" title="Delete" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                            </section>
                        </li>
                        <?php
                    }

                    ?>

                </ul>

                <?php

            // NO SHIPMENTS
            } else {
                ?>
                <div style="padding: 10px; text-align: center; font-size: .75em;">
                    No shipments
                </div>
                <?php
            }

            ?>

        </div>

    </div>

    <?php

    $lo_types = array('to', 'from');
    $linked_orders = array();

    foreach ($lo_types as $lo_type) {

        $sort = ($lo_type == 'to' ? 'from' : 'to');

        // GET LINKED
        $lorders_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders_rel']. "` WHERE (`" .$lo_type. "`=" .$order['id']. ") ORDER BY `" .$sort. "` ASC";
        $lorders_q = sqlquery($lorders_query);
        while ($lorder = sqlfetch($lorders_q)) {
            $linked_orders[$lo_type][] = $lorder;
        }

    }

    ?>

    <style type="text/css">

        #linked_orders .contain.create_link section {
            display: block !important;
        }

        #linked_orders header.type {
            clear: both;
            padding: 3px 15px;
            background-color: #ccc;
        }
        #linked_orders .link_id {
            width: 100px;
            text-align: left;
        }
        #linked_orders .link_customer {
            width: 400px;
            text-align: left;
        }
        #linked_orders .link_amount {
            width: 150px;
            text-align: left;
        }
        #linked_orders .link_date {
            width: 195px;
            text-align: left;
        }
        #linked_orders .link_remove {
            width: 75px;
            text-align: right;
        }
        #linked_orders .link section {
            display: block !important;
        }

    </style>

    <script type="text/javascript">

        $('document').ready(function() {

            // ADD LINK BUTTON CLICK
            $('#linked_orders header .add_link').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('#linked_orders .contain.create_link').toggle();
            });

        });

    </script>

    <a name="linked-orders"></a>
    <div id="linked_orders" class="container">

        <header>
            <h2>Linked Orders</h2>
            <a class="button add_link" href="#">Link Order</a>
        </header>

        <div class="contain create_link" style="display: none;">

            <form id="form_create_link" enctype="multipart/form-data" action="./processes/add_link/" method="post">
            <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />

                <section>

                    <fieldset class="form-group">
                        <label>Link to this order:</label>
                        <select name="from_id" class="custom_control form-control">
                            <option value="">Select</option>
                            <?php
                            $orders_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`status`!='cart') AND (`status`!='deleted') ORDER BY `id` DESC";
                            $orders_q = sqlquery($orders_query);
                            while ($ord = sqlfetch($orders_q)) {
                                ?>
                                <option value="<?php echo $ord['id']; ?>"><?php echo $ord['id']; ?> - <?php echo trim(stripslashes($ord['billing_firstname']. ' ' .$ord['billing_lastname'])); ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </fieldset>

                    <input class="blue" type="submit" value="Save" />

                </section>

            </form>

        </div>

        <div class="table" style="margin: 0px; border: 0px none;">

            <?php

            // NO LINKS
            if ((count((array)$linked_orders['to']) == 0) && (count((array)$linked_orders['from']) == 0)) {

                ?>
                <div style="padding: 10px; text-align: center; font-size: .75em;">
                    No orders linked to / from.
                </div>
                <?php

            // HAVE LINKS
            } else {

                ?>

                <header style="clear: both;">
                    <span class="link_id">Order</span>
                    <span class="link_customer">Customer</span>
                    <span class="link_amount">Amount</span>
                    <span class="link_date">Order Date</span>
                    <span class="link_remove">Unlink</span>
                </header>

                <?php

                // LOOP
                foreach ($linked_orders as $type => $links) {

                    ?>

                    <header class="type">
                        Linked <?php echo ucwords($type); ?>
                    </header>

                    <ul class="items links">

                        <?php

                        // LOOP
                        foreach ($links as $link) {

                            $order_id = ($type == 'to' ? $link['from'] : $link['to']);

                            // GET ORDER
                            $lorder_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$order_id. ")";
                            $lorder_q = sqlquery($lorder_query);
                            $lorder = sqlfetch($lorder_q);

                            ?>

                            <li class="item link">

                                <div class="contain">
                                    <section class="link_id">
                                        <a href="./?id=<?php echo $lorder['id']; ?>"><?php echo $lorder['id']; ?></a>
                                    </section>
                                    <section class="link_customer">
                                        <?php echo trim(stripslashes($lorder['billing_firstname']. ' ' .$lorder['billing_lastname'])); ?>
                                    </section>
                                    <section class="link_amount">
                                        $<?php echo number_format($lorder['order_total'], 2); ?>
                                    </section>
                                    <section class="link_date">
                                        <?php echo date('n/j/Y g:i A', strtotime($lorder['dt'])); ?>
                                    </section>
                                    <section class="link_remove">
                                        <a onclick="return confirmPrompt(this.href, 'Are you sure you want to unlink these two orders?');" title="Unlink" class="icon_delete" href="./processes/remove_link/?order_id=<?php echo $order['id']; ?>&id=<?php echo $link['id']; ?>"></a>
                                    </section>
                                </div>

                            </li>
                            <?php
                        }

                        ?>

                    </ul>

                    <?php

                }

            }

            ?>

        </div>

    </div>

    <?php if ($order['quote'] == 2) { ?>

        <a name="log"></a>
        <div id="log" class="container legacy expand">

            <header><h2><i class="fa fa-plus plus-minus" aria-hidden="true"></i> Log</h2></header>

            <section>

                <div class="contain">

                    <?php

                    // STATUS COLOR ARRAY
                    $sca = $_uccms_ecomm->statusColors()['quote'];

                    // GET MESSAGES
                    $message_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_message_log']. "` WHERE (`order_id`=" .$order['id']. ") ORDER BY `dt` DESC";
                    $message_q = sqlquery($message_query);
                    while ($message = sqlfetch($message_q)) {
                        ?>
                        <div class="item">
                            <div class="info contain">
                                <div class="left">
                                    <?=date('M j, Y g:i a', strtotime($message['dt']))?> - <?php echo ($message['by'] ? stripslashes($_uccms_ecomm->getSetting('store_name')) : 'Client') ?>
                                </div>
                                <div class="right">
                                    <?php if ($message['email_log_id']) { ?><a href="../../view-email/?id=<?=$message['email_log_id']?>" target="blank"><i class="fa fa-envelope-o"></i></a> <?php } ?><span class="status" style="<?php if ($sca[$message['status']]) { echo 'color: ' .$sca[$message['status']]; } ?>"><?=ucwords($message['status'])?></span>
                                </div>
                            </div>
                            <div class="message">
                                <?=stripslashes($message['message'])?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>

            </section>

        </div>

        <?php

    }

    ?>

    <form action="./clone/" method="get">
    <input type="hidden" name="id" value="<?php echo $order['id']; ?>" />
    <input type="hidden" name="type" value="order" />

    <div id="clone" class="container legacy expand">
        <header>
            <h2><i class="fa fa-plus plus-minus" aria-hidden="true"></i> Clone</h2>
        </header>
        <section>

            <fieldset class="form-group">
                <label>What</label>
                <select name="what" class="custom_control form-control">
                    <option value="cart">Cart only</option>
                    <option value="entire">Entire order</option>
                </select>
            </fieldset>

            <fieldset class="form-group">
                <label>Convert</label>
                <input type="checkbox" name="convert" value="1" /> Create as quote
            </fieldset>

        </section>
        <footer>
            <input class="blue" type="submit" value="Process" />
        </footer>
    </div>

    <?php

    // TRELLO TITLE
    $trello['title'] = 'Order ID: ' .$order['id']. ' (' .stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname']). ')';

    // TRELLO DESCRIPTION
    $trello['desca'][]  = 'Name: ' .stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname']);
    $trello['desca'][]  = 'Email: ' .stripslashes($order['billing_email']);
    $trello['desca'][]  = 'Phone: ' .stripslashes($order['billing_phone']);
    $trello['desca'][]  = '';
    $trello['desca'][]  = 'Date: ' .date('n/j/Y g:i A T', strtotime($order['dt']));
    $trello['desca'][]  = 'Total: $' .number_format($order['order_total'], 2);
    $trello['desca'][]  = 'Shipping: ' .$shipping_title;
    $trello['desca'][]  = '';
    if (count((array)$trello['items']) > 0) {
        $trello['desca'][]  = implode('\n ', $trello['items']);
    }
    $trello['description'] = implode('\n ', $trello['desca']);

    ?>

    <script type="text/javascript">

        $(document).ready(function() {

            // TRELLO BUTTON CLICK
            $('header .trello').click(function(e) {
                e.preventDefault();
                window.open('https://trello.com/add-card?source=' +window.location.host+ '&mode=popup&url=' +encodeURIComponent(window.location.href)+ '&name=' +encodeURIComponent('<?php echo $trello['title']; ?>')+ '&desc=' +encodeURIComponent('<?php echo $trello['description']; ?>'), 'add-trello-card', 'width=500,height=600,left='+(window.screenX+(window.outerWidth-500)/2)+',top='+(window.screenY+(window.outerHeight-740)/2));
            });

        });

    </script>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>

<script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
    $('.date_picker').datepicker({ dateFormat: 'mm/dd/yy', duration: 200, showAnim: 'slideDown' });
    $('.time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
</script>