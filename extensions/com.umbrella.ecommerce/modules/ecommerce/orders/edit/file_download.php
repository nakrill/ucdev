<?php

// HAVE INFO
if (($_REQUEST['order_id']) && ($_REQUEST['file'])) {

    // FILE
    $file = SITE_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/files/orders/' .(int)$_REQUEST['order_id']. '-' .urldecode($_REQUEST['file']);

    // FILE EXISTS
    if (is_file($file)) {

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' .basename($file). '"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' .filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;

    // FILE NOT FOUND
    } else {
        echo 'File not found.';
    }

// MISSING REQUIRED INFO
} else {
    echo 'Missing required info.';
}

?>