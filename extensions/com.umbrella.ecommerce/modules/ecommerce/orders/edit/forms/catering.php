<?php if ($order['id']) { ?>
    <div style="padding-bottom: 20px;">
        <a href="../../orders/print/catering/kitchen-prep?id=<?=$order['id']?>" target="_blank" class="button" style="line-height: 2.4em;">Kitchen Prep Sheet</a>&nbsp;&nbsp;<a href="../../orders/print/catering/client-overview?id=<?=$order['id']?>" target="_blank" class="button" style="line-height: 2.4em;">Client Overview</a>
    </div>
<?php } ?>

<a name="event"></a>
<div class="container event <?php if ($order['id']) { echo 'expand'; } ?>">

    <header>
        <h2>Event Info</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset class="service_type">
                    <label>Service Type *</label>
                    <select name="service[type]">
                        <option value="catering_event" <?php if ($extra['service_type'] == 'catering_event') { ?>selected="selected"<?php } ?>>Event Catering</option>
                        <option value="catering_wedding" <?php if ($extra['service_type'] == 'catering_wedding') { ?>selected="selected"<?php } ?>>Wedding Catering</option>
                        <option value="chef" <?php if ($extra['service_type'] == 'chef') { ?>selected="selected"<?php } ?>>Chef</option>
                        <option value="beverage" <?php if ($extra['service_type'] == 'beverage') { ?>selected="selected"<?php } ?>>Beverage</option>
                        <option value="pickup" <?php if ($extra['service_type'] == 'pickup') { ?>selected="selected"<?php } ?>>Pickup</option>
                        <option value="dropoff" <?php if ($extra['service_type'] == 'dropoff') { ?>selected="selected"<?php } ?>>Dropoff</option>
                        <option value="dropoff_hot" <?php if ($extra['service_type'] == 'dropoff_hot') { ?>selected="selected"<?php } ?>>Dropoff - Hot</option>
                        <option value="in_house" <?php if ($extra['service_type'] == 'in_house') { ?>selected="selected"<?php } ?>>In-House</option>
                        <option value="menu_tasting" <?php if ($extra['service_type'] == 'menu_tasting') { ?>selected="selected"<?php } ?>>Menu Tasting</option>
                    </select>
                </fieldset>

                <fieldset class="event_num_people">
                    <label># People *</label>
                    <input type="text" name="event[num_people]" value="<?php echo $extra['event_num_people']; ?>" style="width: 100px;" />
                </fieldset>

            </div>

            <div class="right">

                <fieldset class="event_date">
                    <label>Event Date *</label>
                    <div>
                        <?php

                        // SET DEFAULT DATE FORMAT IF NONE SPECIFIED
                        if (!$bigtree["config"]["date_format"]) {
                            $bigtree["config"]["date_format"] = 'm/d/Y';
                        }

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'event_date',
                            'key'       => 'event[date]',
                            'value'     => $extra['event_date'],
                            'required'  => true,
                            'options'   => array(
                                'default_today' => true
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>
                </fieldset>

                <fieldset class="service_time">
                    <label>Service Time *</label>
                    <div>
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'service_time',
                            'key'       => 'service[time]',
                            'value'     => $extra['service_time'],
                            'required'  => true
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/time.php'));

                        ?>
                    </div>
                </fieldset>

            </div>

        </div>

    </section>

    <?php if ($order['id']) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>

<a name="contact"></a>
<div class="container contact <?php if ($order['id']) { echo 'expand'; } ?>">

    <header>
        <h2>Contact Info</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>First Name</label>
                    <input type="text" name="contact[firstname]" value="<?php echo stripslashes($extra['contact_firstname']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Last Name</label>
                    <input type="text" name="contact[lastname]" value="<?php echo stripslashes($extra['contact_lastname']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Company Name</label>
                    <input type="text" name="contact[company]" value="<?php echo stripslashes($extra['contact_company']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Phone</label>
                    <input type="text" name="contact[phone]" value="<?php echo stripslashes($extra['contact_phone']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Email</label>
                    <input type="text" name="contact[email]" value="<?php echo stripslashes($extra['contact_email']); ?>" />
                </fieldset>

            </div>

            <div class="right">

                <fieldset>
                    <label>Address</label>
                    <input type="text" name="contact[address1]" value="<?php echo stripslashes($extra['contact_address1']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="contact[address2]" value="<?php echo stripslashes($extra['contact_address2']); ?>" />
                </fieldset>

                <fieldset>
                    <label>City</label>
                    <input type="text" name="contact[city]" value="<?php echo stripslashes($extra['contact_city']); ?>" />
                </fieldset>

                <fieldset>
                    <label>State</label>
                    <select name="contact[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $extra['contact_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Zip</label>
                    <input type="text" name="contact[zip]" value="<?php echo stripslashes($extra['contact_zip']); ?>" />
                </fieldset>

            </div>

        </div>

    </section>

    <?php if ($order['id']) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>

<a name="delivery"></a>
<div class="container delivery <?php if ($order['id']) { echo 'expand'; } ?>">

    <header>
        <h2>Delivery Info</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset class="event_location">
                    <label>Location</label>
                    <input type="text" name="event[location1]" value="<?php echo $extra['event_location1']; ?>" />
                </fieldset>

                <fieldset class="event_location">
                    <label>Location (cont)</label>
                    <input type="text" name="event[location2]" value="<?php echo $extra['event_location2']; ?>" />
                </fieldset>

                <fieldset class="event_address1">
                    <label>Address</label>
                    <input type="text" name="event[address1]" value="<?php echo $extra['event_address1']; ?>" />
                </fieldset>

                <fieldset class="event_address2">
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="event[address2]" value="<?php echo $extra['event_address2']; ?>" />
                </fieldset>

            </div>

            <div class="right">

                <fieldset class="event_city">
                    <label>City</label>
                    <input type="text" name="event[city]" value="<?php echo $extra['event_city']; ?>" />
                </fieldset>

                <fieldset class="event_state">
                    <label>State</label>
                    <select name="event[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $extra['event_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="event_zip">
                    <label>Zip</label>
                    <input type="text" name="event[zip]" value="<?php echo $extra['event_zip']; ?>" />
                </fieldset>

                <fieldset class="event_phone">
                    <label>Phone</label>
                    <input type="text" name="event[phone]" value="<?php echo $extra['event_phone']; ?>" />
                </fieldset>

            </div>

        </div>

    </section>

    <?php if ($order['id']) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>

<a name="notes"></a>
<div class="container notes">

    <header>
        <h2>Notes</h2>
    </header>

    <section>

        <fieldset>
            <textarea name="event[notes]" style="height: 58px;"><?php echo stripslashes($extra['event_notes']); ?></textarea>
        </fieldset>

    </section>

    <?php if ($order['id']) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>

<?php if (!$order['id']) { ?>
    <input class="blue" type="submit" value="Save" />
<?php } ?>