<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status'        => 'deleted',
        'deleted_by'    => $_uccms_ecomm->adminID()
    );

    // UPDATE
    $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). ", `deleted_dt`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Order', 'Order deleted.');

        // MARK ALL ORDER TRANSACTIONS IN DB AS VOIDED
        $trans_query = "UPDATE `" .$_uccms_ecomm->tables['transaction_log']. "` SET `status`='voided' WHERE (`cart_id`=" .$id. ")";
        sqlquery($query);

    // QUERY FAILED
    } else {
        $admin->growl('Delete Order', 'Failed to delete order.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Order', 'No order specified.');
}

BigTree::redirect(MODULE_ROOT.'orders/');

?>