<?php

$bigtree['breadcrumb'][] = [ 'title' => 'Orders', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #form_orders .search_filter {
        margin-bottom: 15px;
    }
    #form_orders .search_filter .dates {
        float: left;
    }
    #form_orders .search_filter .dates .from, #form_orders .search_filter .dates .to {
        float: left;
    }
    #form_orders .search_filter .dates input.date_picker {
    }
    #form_orders .search_filter .dates .icon_small {
        margin: 8px 0 0 -25px;
    }
    #form_orders .search_filter .dates .div {
        float: left;
        line-height: 30px;
        padding: 0 5px;
    }
    #form_orders .search_filter .dates .date_type, #form_orders .search_filter .dates .select {
        float: left;
        margin-left: 5px;
    }
    #form_orders .search_filter .dates .buttons {
        float: left;
        padding-left: 5px;
    }
    #form_orders .search_filter .status {
        float: right;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // REMOVE CUSTOMER
        $('#customer .remove').click(function(e) {
            e.preventDefault;
            var url = removeURLParam('customer_id', window.location.href);
            url = removeURLParam('status', url);
            window.location.href = url;
        });

        // DATE GO
        $('#form_orders .search_filter .dates input[type="submit"]').click(function(e) {
            e.preventDefault();
            $('#form_orders').submit();
        });

        // STATUS SELECT CHANGE
        $('#form_orders .search_filter .status select').change(function() {
            $('#form_orders').submit();
        });

    });

</script>

<form id="form_orders" method="get">
<input type="hidden" name="customer_id" value="<?=(int)$_REQUEST['customer_id']?>" />

<?php

// CLEAN UP
$customer_id = (int)$_REQUEST['customer_id'];

// CUSTOMER SPECIFIED
if ($customer_id > 0) {

    // ACCOUNT CLASS - UPDATE CONFIG
    $_uccms['_account']->setConfig(array(
        'admin' => array(
            'from'      => true,
            'acct_id'   => $customer_id
        ))
    );

    // GET CUSTOMER INFO
    $customer = $_uccms['_account']->getAccount('', true);

    if (($customer['firstname']) || ($customer['lastname'])) {
        $display_name = trim(stripslashes($customer['firstname']. ' ' .$customer['lastname']));
    } else {
        $display_name = stripslashes($customer['username']);
    }

    ?>

    <div id="customer" style="margin-bottom: 15px; padding: 10px; border: 1px solid #eee; background-color: #fafafa; border-radius: 5px;">

        <h4 style="margin-bottom: 0px; color: #555;">Customer: <?=$display_name?></h4>
        <div style="margin-bottom: 8px; padding-bottom: 8px; border-bottom: 1px solid #eee;">
            <a href="#" class="remove">Remove</a> | <a href="../customers/">Back to customers</a>
        </div>

        <div class="section customer_info contain" style="padding-bottom: 10px;">

            <?php

            // GET LAST ORDER
            $last_order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .$customer_id. ") ORDER BY `dt` DESC LIMIT 1";
            $last_order_q = sqlquery($last_order_query);
            $latest_order = sqlfetch($last_order_q);

            // HAVE LAST ORDER
            if ($latest_order['id']) {

                ?>

                <style type="text/css">
                    #customer .customer_info .fields.billing .phone, #customer .customer_info .fields.billing .email, #customer .customer_info .fields.shipping .phone, #customer .customer_info .fields.shipping .email {
                        padding-top: 4px;
                    }
                </style>

                <div class="left" style="width: 30%;">
                    <h3>Billing</h3>
                    <div class="fields billing">
                        <div class="name">
                            <?php echo trim(stripslashes($latest_order['billing_firstname']. ' ' .$latest_order['billing_lastname'])); ?>
                        </div>
                        <?php if ($latest_order['billing_company']) { ?>
                            <div class="company"><?php echo stripslashes($latest_order['billing_company']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['billing_address1']) { ?>
                            <div class="address1"><?php echo stripslashes($latest_order['billing_address1']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['billing_address2']) { ?>
                            <div class="address2"><?php echo stripslashes($latest_order['billing_address2']);  ?></div>
                        <?php } ?>
                        <?php
                        $billing_csz = array();
                        if ($latest_order['billing_city']) $billing_csz[] = stripslashes($latest_order['billing_city']);
                        if ($latest_order['billing_state']) $billing_csz[] = stripslashes($latest_order['billing_state']);
                        if ($latest_order['billing_zip']) $billing_csz[] = stripslashes($latest_order['billing_zip']);
                        if (count($billing_csz) > 0) {
                            ?>
                            <div class="csz"><?php echo implode(' ', $billing_csz); ?></div>
                            <?php
                        }
                        ?>
                        <?php if ($latest_order['billing_phone']) { ?>
                            <div class="phone"><?php echo stripslashes($latest_order['billing_phone']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['billing_email']) { ?>
                            <div class="email"><?php echo stripslashes($latest_order['billing_email']);  ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="left" style="width: 30%;">
                    <h3>Shipping</h3>
                    <div class="fields shipping">
                        <div class="name">
                            <?php echo trim(stripslashes($latest_order['shipping_firstname']. ' ' .$latest_order['shipping_lastname'])); ?>
                        </div>
                        <?php if ($latest_order['shipping_company']) { ?>
                            <div class="company"><?php echo stripslashes($latest_order['shipping_company']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['shipping_address1']) { ?>
                            <div class="address1"><?php echo stripslashes($latest_order['shipping_address1']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['shipping_address2']) { ?>
                            <div class="address2"><?php echo stripslashes($latest_order['shipping_address2']);  ?></div>
                        <?php } ?>
                        <?php
                        $shipping_csz = array();
                        if ($latest_order['shipping_city']) $shipping_csz[] = stripslashes($latest_order['shipping_city']);
                        if ($latest_order['shipping_state']) $shipping_csz[] = stripslashes($latest_order['shipping_state']);
                        if ($latest_order['shipping_zip']) $shipping_csz[] = stripslashes($latest_order['shipping_zip']);
                        if (count($shipping_csz) > 0) {
                            ?>
                            <div class="csz"><?php echo implode(' ', $shipping_csz); ?></div>
                            <?php
                        }
                        ?>
                        <?php if ($latest_order['shipping_phone']) { ?>
                            <div class="phone"><?php echo stripslashes($latest_order['shipping_phone']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['shipping_email']) { ?>
                            <div class="email"><?php echo stripslashes($latest_order['shipping_email']);  ?></div>
                        <?php } ?>
                    </div>
                </div>

                <?php

            }

            ?>

            <div class="left" style="width: auto;">
                <h3>Customer</h3>
                <a href="../quotes/?customer_id=<?php echo $customer_id; ?>&status=all" class="button" style="line-height: 2.4em;">Quotes</a>
                <a href="../orders/?customer_id=<?php echo $customer_id; ?>&status=all" class="button" style="background-color: #ccc; line-height: 2.4em;">Orders</a>
                <a href="../customers/edit/?id=<?php echo $customer_id; ?>" class="button" style="line-height: 2.4em;">Edit Customer</a>
            </div>

        </div>

    </div>

    <?php

}

?>

<div class="search_paging contain">
    <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
    <span class="form_search_icon"></span>
    <nav id="view_paging" class="view_paging" style="display: none;"></nav>
</div>

<div class="search_filter contain">

    <div class="dates contain">

        <div class="from contain">
            <?php

            // FIELD VALUES
            $field = array(
                'id'        => 'date_from',
                'key'       => 'date_from',
                'value'     => ($_REQUEST['date_from'] ? date('n/j/Y', strtotime($_REQUEST['date_from'])) : ''),
                'required'  => false,
                'options'   => array(
                    'default_now'   => false
                )
            );

            // INCLUDE FIELD
            include(BigTree::path('admin/form-field-types/draw/date.php'));

            ?>
        </div>

        <div class="div">-</div>

        <div class="to contain">
            <?php

            // FIELD VALUES
            $field = array(
                'id'        => 'date_to',
                'key'       => 'date_to',
                'value'     => ($_REQUEST['date_to'] ? date('n/j/Y', strtotime($_REQUEST['date_to'])) : ''),
                'required'  => false,
                'options'   => array(
                    'default_now'   => false
                )
            );

            // INCLUDE FIELD
            include(BigTree::path('admin/form-field-types/draw/date.php'));

            ?>
        </div>

        <?php if ($_uccms_ecomm->storeType() == 'booking') { ?>
            <select name="date_type" class="custom_control form-control" style="display: inline-block; float: left; width: auto; margin-left: 15px;">
                <option value="order">Order Date</option>
                <option value="booking" <?php if ($_REQUEST['date_type'] == 'booking') { ?>selected="selected"<?php } ?>>Booking Date</option>
            </select>
        <?php } ?>

        <div class="buttons">
            <input type="submit" value="Go" class="btn btn-primary" />
        </div>

    </div>

    <div class="status">
        <select name="status" class="custom_control form-control">
            <option value="">All</option>
            <option value="cart" <?php if ($_REQUEST['status'] == 'cart') { ?>selected="selected"<?php } ?>>Cart</option>
            <option value="placed" <?php if ($_REQUEST['status'] == 'placed') { ?>selected="selected"<?php } ?>>Placed</option>
            <option value="charged" <?php if ($_REQUEST['status'] == 'charged') { ?>selected="selected"<?php } ?>>Charged</option>
            <option value="failed" <?php if ($_REQUEST['status'] == 'failed') { ?>selected="selected"<?php } ?>>Failed</option>
            <option value="processing" <?php if ($_REQUEST['status'] == 'processing') { ?>selected="selected"<?php } ?>>Processing</option>
            <option value="shipped" <?php if ($_REQUEST['status'] == 'shipped') { ?>selected="selected"<?php } ?>>Shipped</option>
            <option value="complete" <?php if ($_REQUEST['status'] == 'complete') { ?>selected="selected"<?php } ?>>Complete</option>
            <option value="cancelled" <?php if ($_REQUEST['status'] == 'cancelled') { ?>selected="selected"<?php } ?>>Cancelled</option>
            <option value="cloned" <?php if ($_REQUEST['status'] == 'cloned') { ?>selected="selected"<?php } ?>>Cloned</option>
        </select>
    </div>

</div>

<div id="orders" class="table">
    <summary>
        <h2>Orders</h2>
        <? /*
        <div class="status">
            <select name="status">
                <option value="">All</option>
                <option value="placed" <?php if ($_REQUEST['status'] == 'placed') { ?>selected="selected"<?php } ?>>Placed</option>
                <option value="charged" <?php if ($_REQUEST['status'] == 'charged') { ?>selected="selected"<?php } ?>>Charged</option>
                <option value="failed" <?php if ($_REQUEST['status'] == 'failed') { ?>selected="selected"<?php } ?>>Failed</option>
                <option value="processing" <?php if ($_REQUEST['status'] == 'processing') { ?>selected="selected"<?php } ?>>Processing</option>
                <option value="shipped" <?php if ($_REQUEST['status'] == 'shipped') { ?>selected="selected"<?php } ?>>Shipped</option>
                <option value="complete" <?php if ($_REQUEST['status'] == 'complete') { ?>selected="selected"<?php } ?>>Complete</option>
                <option value="cancelled" <?php if ($_REQUEST['status'] == 'cancelled') { ?>selected="selected"<?php } ?>>Cancelled</option>
                <option value="cloned" <?php if ($_REQUEST['status'] == 'cloned') { ?>selected="selected"<?php } ?>>Cloned</option>
            </select>
        </div>
        */ ?>
    </summary>
    <header style="clear: both;">
        <?php foreach ($_uccms_ecomm->adminOQListColumns() as $col_class => $col_title) { ?>
            <span class="<?=$col_class?>"><?=$col_title?></span>
        <?php } ?>
    </header>
    <ul id="results" class="items">
        <? include(EXTENSION_ROOT. 'ajax/admin/orders/get-page.php'); ?>
    </ul>
</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function(vars) {
        if (!vars) vars = {};
        $('#results').load('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/orders/get-page/', {
            'page': vars.page,
            'query': $("#query").val(),
            'customer_id': <?php echo ($_REQUEST['customer_id'] ? (int)$_REQUEST['customer_id'] : 0); ?>,
            'date_from': $('#form_orders .search_filter input[name="date_from"]').val(),
            'date_to': $('#form_orders .search_filter input[name="date_to"]').val(),
            'status': $('#form_orders .search_filter select[name="status"]').val(),
        });
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        BigTree.localSearch({
            page: BigTree.cleanHref($(this).attr('href'))
        });
        return false;
    });
</script>


</form>