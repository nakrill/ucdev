<?php

// BLANK LAYOUT
$bigtree['layout'] = 'blank';

$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET SHIPMENT INFO
    $shipment_query = "SELECT * FROM `" .$_uccms_ecomm->tables['order_shipments']. "` WHERE (`id`=" .$id. ")";
    $shipment_q = sqlquery($shipment_query);
    $shipment = sqlfetch($shipment_q);

    // HAVE LABEL DATA
    if ($shipment['label_data']) {

        // DECODE PDF CONTENT
        $pdf_decoded = base64_decode(stripslashes($shipment['label_data']));

        // DOWNLOADING
        if ($_REQUEST['download']) {
            $dispo = 'attachment';
        } else {
            $dispo = 'inline';
        }

        header('Content-Type: application/pdf; charset=utf-8');
        header('Content-Disposition: ' .$dispo. '; filename="shipping-label-' .stripslashes($shipment['tracking']). '.pdf"');
        header('Content-Transfer-Encoding: binary');

        echo $pdf_decoded;

    // NO LABEL DATA
    } else {
        echo 'No label data available for this shipment.';
    }

// NO ID
} else {
    echo 'No shipment ID specified.';
}

?>