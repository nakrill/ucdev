<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        $columns = array(
            'quote'     => ($order['quote'] ? 2 : 0),
            'status'    => 'placed',
        );

        // UPDATE
        $update_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$order['id']. ")";
        if (sqlquery($update_query)) {
            $admin->growl('Success', 'Quote is now an order.');
            BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order['id']);
        } else {
            $admin->growl('Failed', 'Failed to turn quote into order.');
            BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$order['id']);
        }

    } else {
        $admin->growl('Failed', 'Quote not found.');
        BigTree::redirect(MODULE_ROOT. 'quotes/');
    }

// ID NOT SPECIFIED
} else {
    $admin->growl('Failed', 'Quote ID not specified.');
    BigTree::redirect(MODULE_ROOT. 'quotes/');
}

?>