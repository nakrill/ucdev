<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['id'];

    // IS/NOT NEW QUOTE
    $new_quote = $_POST['new'];

    // INIT CART
    $order = $_uccms_ecomm->initCart(true, $id, true);

    // HAVE CART ID
    if ($order['id']) {

        $id = $order['id'];

        // GET ORDER ITEMS
        $citems = $_uccms_ecomm->cartItems($order['id']);

        // HAVE ITEMS
        if (count($citems) > 0) {

            // HAVE TAX OVERRIDE
            if ($order['override_tax']) {
                $tax['total'] = number_format($order['override_tax'], 2);

            // NORMAL TAX CALCULATION
            } else {

                // HAVE BILLING STATE AND/OR ZIP
                if (($vals['billing']['state']) || ($vals['billing']['zip'])) {
                    $taxvals['country'] = 'United States';
                    $taxvals['state']   = $vals['billing']['state'];
                    $taxvals['zip']     = $vals['billing']['zip'];
                }

                //$taxvals['quote'] = false;

                // GET TAX
                $tax = $_uccms_ecomm->cartTax($order['id'], $citems, $taxvals);

            }

            // HAVE SHIPPING OVERRIDE
            if ($order['override_shipping']) {
                $shipping['markup'] = number_format($order['override_shipping'], 2);
            } else {
                $shipping['markup'] = 0.00;
            }

            // ATTRIBUTE ARRAY
            $attra = array();

            // EXTENDED VIEW
            $extended_view = true;

            // LOOP THROUGH ITEMS
            foreach ($citems as $oitem) {

                // GET ITEM INFO
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

            }

            // ADD VALUES TO ORDER ARRAY
            $order['tax']       = $tax['total'];
            $order['shipping']  = $shipping['markup'];

        }

        // CALCULATE GRATUITY
        if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
            if ($_POST['order']['override_gratuity']) $order['override_gratuity'] = (float)$_POST['order']['override_gratuity'];
            $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);
        }

        // CALCULATE TOTAL
        $order['total'] = $_uccms_ecomm->orderTotal($order);

        if (!$_POST['status']) $_POST['status'] = 'preparing';

        // CONVERTING TO ORDER
        if ($_POST['status'] == 'placed') {
            $quote = 2;
        } else {
            $quote = 1;
        }

        // DATABASE COLUMNS
        $columns = array(
            'quote'             => $quote,
            'status'            => trim(strtolower($_POST['status'])),
            'override_tax'      => ($_POST['order']['override_tax'] != '') ? number_format((float)$_POST['order']['override_tax'], 2) : '',
            'override_shipping' => ($_POST['order']['override_shipping'] !== '') ? number_format((float)$_POST['order']['override_shipping'], 2) : '',
            'override_gratuity' => ($_POST['order']['override_gratuity'] !== '') ? (int)$_POST['order']['override_gratuity'] : '',
            'order_subtotal'    => (float)$order['subtotal'],
            'order_gratuity'    => (float)$order['gratuity'],
            'order_total'       => (float)$order['total']
        );

        // CUSTOMER ID
        if (
            ((!$_POST['current_customer_id']) && ($_POST['customer_id'])) // NO PREVIOUS CUSTOMER ID, NOW HAS CUSTOMER ID
            ||
            ((($_POST['current_customer_id']) && ($_POST['customer_id'])) && ($_POST['current_customer_id'] != $_POST['customer_id'])) // CHANGING CUSTOMER ID
        ) {

            // GET LATEST ORDER
            $last_order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .(int)$_POST['customer_id']. ") AND (`status`!='cart') ORDER BY `dt` DESC LIMIT 1";
            $last_order_q = sqlquery($last_order_query);
            $last_order = sqlfetch($last_order_q);

            // LAST ORDER FOUND
            if ($last_order['id']) {

                $_POST['billing']['firstname']   = stripslashes($last_order['billing_firstname']);
                $_POST['billing']['lastname']    = stripslashes($last_order['billing_lastname']);
                $_POST['billing']['company']     = stripslashes($last_order['billing_company']);
                $_POST['billing']['address1']    = stripslashes($last_order['billing_address1']);
                $_POST['billing']['address2']    = stripslashes($last_order['billing_address2']);
                $_POST['billing']['city']        = stripslashes($last_order['billing_city']);
                $_POST['billing']['state']       = stripslashes($last_order['billing_state']);
                $_POST['billing']['zip']         = stripslashes($last_order['billing_zip']);
                $_POST['billing']['phone']       = stripslashes($last_order['billing_phone']);
                $_POST['billing']['email']       = stripslashes($last_order['billing_email']);
                $_POST['shipping']['same']       = (int)$last_order['shipping_same'];
                $_POST['shipping']['firstname']  = stripslashes($last_order['shipping_firstname']);
                $_POST['shipping']['lastname']   = stripslashes($last_order['shipping_lastname']);
                $_POST['shipping']['company']    = stripslashes($last_order['shipping_company']);
                $_POST['shipping']['address1']   = stripslashes($last_order['shipping_address1']);
                $_POST['shipping']['address2']   = stripslashes($last_order['shipping_address2']);
                $_POST['shipping']['city']       = stripslashes($last_order['shipping_city']);
                $_POST['shipping']['state']      = stripslashes($last_order['shipping_state']);
                $_POST['shipping']['zip']        = stripslashes($last_order['shipping_zip']);
                $_POST['shipping']['phone']      = stripslashes($last_order['shipping_phone']);
                $_POST['shipping']['email']      = stripslashes($last_order['shipping_email']);

                if (!is_array($_POST['contact'])) $_POST['contact'] = array();
                if (!is_array($_POST['event'])) $_POST['event'] = array();

                $_POST['contact']   = array_merge($_POST['contact'], $_POST['billing']);
                $_POST['event']     = array_merge($_POST['event'], $_POST['shipping']);

            }

        // HAD CUSTOMER ID, NOW SET TO NONE
        } else if (($_POST['current_customer_id']) && (!$_POST['customer_id'])) {
            $columns['customer_id'] = 0;
        }

        // UPDATE DB
        $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order['id']. ")";

        //echo $query;
        //exit;

        // QUERY SUCCESSFUL
        if (sqlquery($query)) {

            //echo 'done';
            //exit;

            if ($new_quote) {
                $admin->growl('Quote', 'Quote created!');
            } else {
                $admin->growl('Quote', 'Quote updated!');
            }

            // IS QUOTE
            $is_quote = true;

            // SEE IF WE HAVE AN EXTRAS RECORD YET
            $extra_sql = "SELECT `id` FROM `" .$_uccms_ecomm->tables['quote_' .$_uccms_ecomm->storeType()]. "` WHERE (`order_id`=" .$order['id']. ")";
            $extra_query = sqlquery($extra_sql);
            if (sqlrows($extra_query) == 0) {

                // CREATE RECORD
                $extra_sql = "INSERT INTO `" .$_uccms_ecomm->tables['quote_' .$_uccms_ecomm->storeType()]. "` SET `order_id`=" .$order['id']. "";
                sqlquery($extra_sql);

            }

            // PROCESS EXTRA
            @include(dirname(__FILE__). '/../../orders/edit/processes/store_types/' .$_uccms_ecomm->storeType(). '.php');

            // RECALCULATE ORDER
            $_uccms_ecomm->orderRecalculate($order['id']);

        // QUERY FAILED
        } else {
            if ($new_quote) {
                $admin->growl('Quote', 'Failed to add quote.');
            } else {
                $admin->growl('Quote', 'Failed to update quote.');
            }
        }

    // NO CART ID
    } else {
        $admin->growl('Quote', 'Quote not found.');
    }

}

// CONVERTING TO ORDER
if ($_POST['status'] == 'placed') {
    $admin->growl('Order', 'Quote converted to order.');
    BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$id);

} else {
    BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$id);
}

?>