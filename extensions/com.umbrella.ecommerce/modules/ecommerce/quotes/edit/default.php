<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        // IS QUOTE
        if ($order['quote'] == 1) {

            // NO HASH YET
            if (!$order['hash']) {

                // GENERATE HASH
                $order['hash'] = $_uccms_ecomm->generateOrderHash($order['customer_id']);

                // UPDATE
                $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `hash`='" .$order['hash']. "' WHERE (`id`=" .$order['id']. ")";
                sqlquery($query);

            }

            // GET ORDER ITEMS
            $citems = $_uccms_ecomm->cartItems($order['id']);

            // GET EXTRA INFO
            $extra = $_uccms_ecomm->cartExtra($order['id']);

            // QUOTE URL
            $quote_url = WWW_ROOT . $_uccms_ecomm->storePath(). '/quote/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']);

        // IS ORDER
        } else {
            $admin->growl('Quote', 'Quote is order.');
            BigTree::redirect(MODULE_ROOT. 'orders/edit/?id=' .$order['id']);
        }

    // ORDER NOT FOUND
    } else {
        $admin->growl('Quote', 'Quote not found.');
        BigTree::redirect(MODULE_ROOT. 'quotes/');
    }

// NO ID
} else {

    // CLEAR CART ID
    $_uccms_ecomm->clearCartID(true);

    // INIT CART
    $order = $_uccms_ecomm->initCart(true, 0, true);

    // UPDATE ORDER
    $order_update = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `created_by`=" .$admin->ID. " WHERE (`id`=" .$order['id']. ")";
    sqlquery($order_update);

    $new_quote = true;

}

// USE EMAIL INFO FROM SESSION
$send = $_SESSION['ecomm']['quote']['send'];

// NO VALUES
if (!$send['from_name']) $send['from_name']     = stripslashes($_uccms_ecomm->getSetting('email_from_name'));
if (!$send['from_email']) $send['from_email']   = stripslashes($_uccms_ecomm->getSetting('email_from_email'));
if (!$send['subject']) $send['subject']         = stripslashes($_uccms_ecomm->getSetting('quote_email_send_subject'));
if (!$send['message']) $send['message']         = stripslashes($_uccms_ecomm->getSetting('quote_email_send_message'));

include(dirname(__FILE__). '/store_types/' .$_uccms_ecomm->storeType(). '/base.php');

?>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<link rel="stylesheet" href="/extensions/<?=$_uccms_ecomm->Extension?>/css/master/quote_request.css" type="text/css" />
<style type="text/css">

    <?php /*if ($new_quote) { ?>
        #ecommContainer #review > .details {
            display: none;
        }
    <?php }*/ ?>

    #cart {
        width: 100%;
        margin-bottom: 5px;
        border: 0px none;
    }

    #cart th {
        padding: 0px;
        border-bottom: 1px solid #eee;
        background-color: transparent;
    }

    #cart .thumb {
        width: 10%;
        text-align: center;
    }
    #cart .title {
        width: 50%;
        text-align: left;
    }
    #cart .price {
        width: 15%;
        text-align: left;
    }
    #cart .quantity {
        width: 15%;
        text-align: left;
    }
    #cart .total {
        width: 10%;
        text-align: right;
    }

    #cart .item td {
        padding: 8px;
    }

    #cart th.title {
        padding-left: 8px;
    }
    #cart th.total {
        padding-right: 8px;
    }
    #cart td.quantity {
        padding-left: 0px;
        padding-right: 0px;
    }
    #cart td.price {
        padding-left: 0px;
        padding-right: 0px;
    }

    #cart .item > td {
        border-bottom: 1px solid #eee;
    }

    #cart .item .thumb {
        vertical-align: top;
    }
    #cart .item .thumb img {
        height: 50px;
    }

    #cart .item .options {
        list-style: none;
        margin: 0px;
        padding: 2px 0 0 10px;
    }
    #cart .item .options li {
        font-size: .8em;
        line-height: 1.2em;
    }

    #cart .item .actions {
        display: none;
        position: absolute;
        top: 4px;
        right: 8px;
        font-size: 1.4em;
    }

    #cart .sub td {
        padding: 10px 0 0;
        background-color: #fff;
        text-align: right;
    }
    #cart .sub td.title {
        padding-right: 0px;
    }

    #transactions .transaction_dt {
        width: 200px;
        text-align: left;
    }
    #transactions .transaction_id {
        width: 200px;
        text-align: left;
    }
    #transactions .transaction_message {
        width: 420px;
        text-align: left;
    }
    #transactions .transaction_ip {
        width: 120px;
        text-align: left;
    }

    #log .item {
        margin: 0 0 10px 0;
        padding: 0 0 10px 0;
        border-bottom: 1px solid #ccc;
    }
    #log .item:last-child {
        margin: 0px;
        padding: 0px;
        border-bottom: 0px none;
    }
    #log .item .left, #log .item .right {
        margin: 0px;
    }
    #log .item .right {
        text-align: right;
    }
    #log .item .info {
        padding-bottom: 4px;
    }

    .selectivity-result-item img, .selectivity-single-selected-item img {
        height: 48px;
        widht: auto;
    }
    .selectivity-single-select input[type="text"] {
        height: auto;
    }

    form.bigtree_dialog_form .overflow {
        width: 540px;
    }
    form.bigtree_dialog_form .overflow .loading {
        padding-top: 15px;
        text-align: center;
    }
    form.bigtree_dialog_form .overflow .data {
        padding-top: 15px;
    }

    .container footer .button.right {
        float: right;
        width: auto;
    }

    .select2-container .select2-results__message {
        /*cursor: pointer;*/
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // SEARCH SELECT
        var $select2 = $('form .container select[name="customer_id"]').select2({
            language: {
                noResults: function() {
                    $('#no-account-match_create').show();
                    return 'No results.';
                }
            }
        });

        // NO RESULTS CLICK
        //$('body').on('click', '.select2-container .select2-results__message', function(e) {
        $('#no-account-match_create').click(function(e) {
            e.preventDefault();
            $select2.select2('close');
            $('.container.billing.expand header').click();
            $('.container.billing.expand input[name="billing[firstname]"]').focus();
            location.hash = '#billing';
        });

        // ACCOUNT CHANGE
        $select2.on('select2:select', function(e) {

            var id = e.params.data.id;
            var cur_id = $('form .container input[name="current_customer_id"]').val();

            <?php if ($new_quote) { ?>
                this_changeCustomer(id);
            <?php } else { ?>
                if ((id) && (id != cur_id)) {
                    $('#confirm_change-customer-id').show();
                } else if (id == cur_id) {
                    $('#confirm_change-customer-id').hide();
                }
            <?php } ?>

        });

        // CONFIRM ACCOUNT CHANGE - NO
        $('#confirm_change-customer-id .no').click(function(e) {
            e.preventDefault();
            $('#confirm_change-customer-id').hide();
            $('form .container select[name="customer_id"]').val($('form .container input[name="current_customer_id"]').val()).trigger('change');
        });

        // CONFIRM ACCOUNT CHANGE - YES
        $('#confirm_change-customer-id .yes').click(function(e) {
            e.preventDefault();
            this_changeCustomer($('form .container select[name="customer_id"]').val());
        });

        <?php if ($_uccms_ecomm->storeType() == 'booking') { ?>

            // BOOK NOW
            $('.container footer .button.book').click(function(e) {
                e.preventDefault();

                $('#ecommContainer #review .heading select[name="status"]').val('placed');

                $(this).closest('form').submit();

            });

        <?php } ?>

    });

    // GET CUSTOMER INFO
    function this_changeCustomer(id) {
        $('form.main').submit();
    }

</script>

<?php

// EXISTING
if ($order['id']) {

    // CALCULATE DAYS OLD
    $days_old = dateDifference($order['dt'], date('Y-m-d H:i:s'), '%a');

    ?>

    <div class="contain">
        <h6 class="uccms_toggle" data-what="order_details"><span>Quote Details</span><span class="icon_small icon_small_caret_down"></span></h6>
        <div class="contain uccms_toggle-order_details" style="display: none;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="20%"><strong>Quote ID:</strong>&nbsp;</td>
                    <td><strong><?php echo $order['id']; ?></strong></td>
                </tr>
                <tr>
                    <td>Status:&nbsp;</td>
                    <td><?php echo ucwords($order['status']); ?></td>
                </tr>
                <tr>
                    <td>Date:&nbsp;</td>
                    <td><?php echo date('n/j/Y', strtotime($order['dt'])); ?></td>
                </tr>
                <tr>
                    <td>Time:&nbsp;</td>
                    <td><?php echo date('g:i A T', strtotime($order['dt'])); ?></td>
                </tr>
                <tr>
                    <td>Days old:&nbsp;</td>
                    <td><?php echo number_format((int)$days_old, 0); ?></td>
                </tr>
                <tr>
                    <td>IP:&nbsp;</td>
                    <td><?php echo long2ip($order['ip']); ?></td>
                </tr>
            </table>
        </div>
    </div>

    <?php

    // NOT "CART STATUS
    if (($order['status']) && ($order['status'] != 'cart')) {

        ?>

        <script type="text/javascript">
            $(function() {
                $('#ecommContainer .heading .send_quote').click(function(e) {
                    $('#sendQuote').show();
                });
                $('#sendQuote .cancel').click(function(e) {
                    $('#sendQuote').hide();
                });
            });
        </script>

        <a name="send"></a>
        <div id="sendQuote" class="container legacy" style="display: none;">

            <header>
                <h2>Send Quote</h2>
            </header>

            <form enctype="multipart/form-data" action="./send/" method="post">
            <input type="hidden" name="id" value="<?php echo $order['id']; ?>" />

            <section>

                <div style="padding-bottom: 20px;">
                    This will send your message and the link below to view the quote online to the email address specified.
                    <div style="padding-top: 10px;">
                        Quote URL: <a href="<?=$quote_url?>" target="_blank"><?=$quote_url?></a>
                    </div>
                </div>

                <div class="contain">

                    <div class="left">

                        <fieldset>
                            <label>From Name</label>
                            <input type="text" name="send[from_name]" value="<?=$send['from_name']?>" placeholder="<?=stripslashes($_uccms_ecomm->getSetting('email_from_name'))?>" />
                        </fieldset>

                        <fieldset>
                            <label>From Email</label>
                            <input type="text" name="send[from_email]" value="<?=$send['from_email']?>" placeholder="<?=stripslashes($_uccms_ecomm->getSetting('email_from_email'))?>" />
                        </fieldset>

                    </div>

                    <div class="right">

                        <fieldset>
                            <label>To Name</label>
                            <input type="text" name="send[to_name]" value="<?=$send['to_name']?>" placeholder="<?php echo trim(stripslashes($extra['contact_firstname']. ' ' .$extra['contact_lastname'])); ?>" />
                        </fieldset>

                        <fieldset>
                            <label>To Email</label>
                            <input type="text" name="send[to_email]" value="<?=$send['to_email']?>" placeholder="<?php echo stripslashes($extra['contact_email']); ?>" />
                        </fieldset>

                    </div>

                </div>

                <fieldset>
                    <label>Subject</label>
                    <input type="text" name="send[subject]" value="<?=$send['subject']?>" placeholder="<?php echo ($_uccms_ecomm->getSetting('quote_email_send_subject') ? stripslashes($_uccms_ecomm->getSetting('quote_email_send_subject')) : 'Quote Available'); ?>" />
                </fieldset>

                <fieldset>
                    <label>Message</label>
                    <div>
                        <?php
                        $field = array(
                            'key'       => 'send[message]', // The value you should use for the "name" attribute of your form field
                            'value'     => ($_uccms_ecomm->getSetting('quote_email_send_message') ? stripslashes($_uccms_ecomm->getSetting('quote_email_send_message')) : 'Your quote is ready to view!'), // The existing value for this form field
                            'id'        => 'send_message', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'simple' => true
                            )
                        );
                        include(BigTree::path('admin/form-field-types/draw/html.php'));
                        ?>
                    </div>
                </fieldset>

                <fieldset class="last">
                    <label>Send me a copy</label>
                    <input type="text" name="send[copy]" value="<?=$send['copy']?>" placeholder="" />
                </fieldset>

            </section>

            <footer>
                <input class="blue" type="submit" value="Send" />
                <a class="button cancel" href="#quote">Cancel</a>
            </footer>

            </form>

        </div>

    <?php } ?>

<?php } else { ?>

    <h3>Add Quote</h3>

<?php } ?>

<form enctype="multipart/form-data" action="./process/" method="post" class="main">
<input type="hidden" name="id" value="<?php echo $order['id']; ?>" />
<?php if ($new_quote) { ?>
    <input type="hidden" name="new" value="true" />
<?php } ?>

<?php

// HAVE ORDER ID
if ($order['id']) {

    // ITEM ARRAY
    $ia = array();

    // GET ITEMS
    $item_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0) ORDER BY `title` ASC, `id` ASC";
    $item_q = sqlquery($item_query);
    while ($item = sqlfetch($item_q)) {

        // GET IMAGE
        $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC LIMIT 1";
        $image = sqlfetch(sqlquery($image_query));

        // HAVE IMAGE
        if ($image['image']) {
            $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
        } else {
            $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
        }

        // ADD TO ITEM ARRAY
        $ia[] = '{
            id: ' .$item['id']. ',
            image: \'' .$image_url. '\',
            text: \'' .str_replace("'", "\'", stripslashes($item['title'])). '\'
        }';

    }

    ?>

    <script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/selectivity-full.min.js"></script>
    <link rel="stylesheet" href="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/css/lib/selectivity-full.css" type="text/css" />

    <script type="text/javascript">

        $(document).ready(function() {

            // SET AS FOOTER CART
            $('header .footer-cart').click(function(e) {
                e.preventDefault();
                afc_startCart({
                    id: <?php echo $order['id']; ?>,
                }, function() {

                });
            });

            // EDIT STORE-TYPE INFO CLICK
            $('#ecommContainer #review .edit_anchor').click(function(e) {
                var c = $(this).attr('href').replace('#', '');
                if ($('.container.' +c+ ' section').is(':hidden')) {
                    $('.container.' +c+ ' header').click();
                }
            });

            // ADD ITEM CLICK
            $('#ecommContainer #review .cart_addItem').click(function(e) {

                e.preventDefault();

                // DIALOG
                BigTreeDialog({
                    title: 'Add Item',
                    content: $('#mc_addItem').html(),
                    //preSubmissionCallback: true,
                    callback: function(params) {
                        if ((params.order_id) && (params.item_id)) {
                            $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add-to-order/', $('form.bigtree_dialog_form').serialize(), function(data) {
                            if (data['success']) {
                                if ((data['item']['id']) && (typeof _rst == 'object')) {
                                    _rst.push(['event', 'cart-add', data['item']['title']+ ' (#' +data['item']['id']+ ')']);
                                }
                                setTimeout(function() {
                                    if ((data['order_id']) && (<?php echo (int)$_REQUEST['id']; ?> == 0)) {
                                        window.location.href = window.location.href+ '?id=' +data['order_id']+ '#cart';
                                    } else {
                                        location.reload();
                                    }
                                }, 2000);
                            } else {
                                if (data['err']) {
                                    if (typeof _rst == 'object') {
                                        _rst.push(['event', 'cart-add-error', data['err']]);
                                    }
                                    alert(data['err']);
                                } else {
                                    alert('Adding failed.');
                                }
                            }
                            }, 'json');
                        } else {
                            alert('Could not submit data.');
                        }
                    },
                    icon: 'add'
                });

                // SELECT PRODUCT
                $('.bigtree_dialog_window .select_product').selectivity({
                    allowClear: true,
                    items:[<?php echo implode(', ', $ia); ?>],
                    placeholder: 'Select item',
                    templates: {
                        resultItem: function(item) {
                            return (
                            '<div class="selectivity-result-item" data-item-id="' +item.id+ '">' +
                                '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                            '</div>'
                            );
                        },
                        singleSelectedItem: function(item) {
                            return (
                                '<span class="selectivity-single-selected-item" data-item-id="' +item.id+ '">' +
                                    '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                                '</span>'
                            );
                        }
                    }
                }).on('selectivity-selected', function(e) {
                    $('.selectivity-single-select').height(57);
                    $('form.bigtree_dialog_form .overflow .data').html('').hide();
                    $('form.bigtree_dialog_form .overflow .loading').show();
                    $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add_interface/', {
                            order_id: <?=$order['id']?>,
                            item_id: e.id<?php if ($order['status'] == 'cart') { ?>,
                            quote: 1<?php } ?>
                        },
                        function(data) {
                            $('form.bigtree_dialog_form .overflow .loading').hide();
                            $('form.bigtree_dialog_form .overflow .data').html(data).show();
                        },
                        'html'
                    );
                });

            });

            // ITEM ROLLOVER
            $('#cart .item').hover(
                function() {
                    $(this).find('.actions').show();
                }, function() {
                    $(this).find('.actions').hide();
                }
            );

            // ITEM - EDIT CLICK
            $('#cart .item .actions .edit').click(function(e) {
                e.preventDefault();
                $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add_interface/', {
                        order_id: <?=$order['id']?>,
                        order_item_id: $(this).attr('data-id')
                    },
                    function(data) {
                        BigTreeDialog({
                            title: 'Edit Item',
                            content: '<div class="data">' +data+ '</div>',
                            //preSubmissionCallback: true,
                            callback: function(params) {
                                if ((params.order_id) && (params.item_id)) {
                                    $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add-to-order/', $('form.bigtree_dialog_form').serialize(), function(data2) {
                                        if (data2['success']) {
                                            if ((data2['item']['id']) && (typeof _rst == 'object')) {
                                                _rst.push(['event', 'cart-update', data2['item']['title']+ ' (#' +data2['item']['id']+ ')']);
                                            }
                                            setTimeout(function() {
                                                location.reload();
                                            }, 2000);

                                        } else {
                                            if (data2['err']) {
                                                if (typeof _rst == 'object') {
                                                    _rst.push(['event', 'cart-update-error', data['err']]);
                                                }
                                                alert(data2['err']);
                                            } else {
                                                alert('Updating failed.');
                                            }
                                        }
                                    }, 'json');
                                } else {
                                    alert('Could not submit data.');
                                }
                            },
                            icon: 'edit'
                        });
                    },
                    'html'
                );
            });

            // ITEM - DELETE CLICK
            $('#cart .item .actions .delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to remove this?')) {
                    window.location.href = './delete-item/?id=' +$(this).attr('data-id')+ '&order_id=<?=$order['id']?>';
                }
            });

        });

    </script>

    <a name="quote"></a>
    <div class="container legacy">

        <header>
            <h2>Quote</h2>
            <?php if ($_uccms_ecomm->getSetting('admin_footer_cart_enabled')) { ?>
                <a href="#" class="button footer-cart">Set as Footer Cart</a>
            <?php } ?>
            <?php if (($order['status']) && ($order['status'] != 'cart')) { ?><a href="<?=$quote_url?>" target="_blank" class="button">View Quote</a><?php } ?>
        </header>

        <section>

            <fieldset style="margin-bottom: 25px; padding-bottom: 20px; border-bottom: 1px solid #eee;">

                <label>Customer Account</label>

                <input type="hidden" name="current_customer_id" value="<?php echo $order['customer_id']; ?>" />

                <select name="customer_id" class="custom_control" style="padding: 5px; border-color: #ccc; border-radius: 3px;">
                    <option value="0">None</option>
                    <optgroup label="Accounts">
                        <?php
                        $accounts_query = "
                        SELECT a.id, a.username, a.email, CONCAT(ad.firstname, ' ', ad.lastname) AS `fullname`
                        FROM `" .$_uccms_ecomm->tables['accounts']. "` AS `a`
                        INNER JOIN `" .$_uccms_ecomm->tables['account_details']. "` AS `ad` ON a.id=ad.id
                        ORDER BY `fullname` ASC, a.username ASC
                        ";
                        $accounts_q = sqlquery($accounts_query);
                        while ($acct = sqlfetch($accounts_q)) {
                            ?>
                            <option value="<?php echo $acct['id']; ?>" <?php if ($acct['id'] == $order['customer_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($acct['fullname']); ?> (<?php echo $acct['email']; ?>)</option>
                            <?php
                        }
                        ?>
                    </optgroup>
                </select>

                <a id="no-account-match_create" href="#" class="button blue" style="display: none; height: 24px; line-height: 23px; margin-left: 5px;">Create account</a>

                <div id="confirm_change-customer-id" style="clear: both; display: none; padding-top: 10px;">
                    Change customer?
                    <div style="padding-top: 5px;">
                        <a href="#" class="yes button blue" style="height: 34px;">Yes</a>&nbsp;&nbsp;&nbsp;<a href="#" class="no button" style="height: 34px;">No</a>
                    </div>
                </div>

            </fieldset>

            <div id="ecommContainer" class="contain">

                <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/review.php'); ?>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
            <a class="button back" href="../">&laquo; Back</a>
            <?php if (($_uccms_ecomm->storeType() == 'booking') && (!$new_quote)) { ?>
                <a href="#" class="button blue book right">Book Now</a>
            <?php } ?>
        </footer>

    </div>

    <div id="mc_addItem" style="display: none;">
        <div class="select_product"></div>
        <div class="loading" style="display: none;">Loading..</div>
        <div class="data" style="display: none;"></div>
    </div>

<?php } ?>

<?php

// ORDER EDIT FIELDS
if ($_uccms_ecomm->storeType() != 'general') {
    include(dirname(__FILE__). '/../../orders/edit/forms/' .$_uccms_ecomm->storeType(). '.php');
}

// QUOTE EDIT FIELDS
include(dirname(__FILE__). '/store_types/' .$_uccms_ecomm->storeType(). '/edit_fields.php');

?>

</form>

<?php

// HAVE ORDER ID
if ($order['id']) {

    ?>

    <a name="scheduled_payments"></a>

    <style type="text/css">

        #scheduled_payments .scheduled_payment_dt_due {
            width: 120px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_amount {
            width: 150px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_title {
            width: 350px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_email_on {
            width: 210px;
            text-align: left;
        }
        #scheduled_payments .scheduled_payment_edit {
            width: 50px;
        }
        #scheduled_payments .scheduled_payment_delete {
            width: 50px;
        }

        #scheduled_payments .scheduled_payments ul .item {
            height: auto;
        }
        #scheduled_payments .scheduled_payments ul .item .edit {
            padding: 10px 20px 0;
            border-top: 1px dashed #ddd;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // ADD TRACKING BUTTON CLICK
            $('#scheduled_payments header .button.add_scheduled_payment').click(function(e) {
                e.preventDefault();
                $('#scheduled_payments .contain.add_scheduled_payment').toggle();
            });

            // EDIT ICON CLICK
            $('#scheduled_payments .scheduled_payments ul .item .icon_edit').click(function(e) {
                e.preventDefault();
                $(this).closest('.item').find('.edit').toggle();
            });

        });

    </script>

    <div id="scheduled_payments" class="container legacy">

        <header>
            <h2>Scheduled Payments</h2>
            <a href="#" class="button add_scheduled_payment">Add Scheduled Payment</a>
        </header>

        <section class="contain add_scheduled_payment" style="display: none;">

            <form enctype="multipart/form-data" action="../../orders/edit/processes/add_scheduled_payment/" method="post">
            <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
            <input type="hidden" name="from" value="quote" />

            <div class="left last">

                <fieldset>
                    <label>Due Date</label>
                    <div>
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'add_scheduled_payment_due_date',
                            'key'       => 'dt',
                            'value'     => '',
                            'required'  => true,
                            'options'   => array(
                                'default_today' => false
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>
                </fieldset>

                <fieldset>
                    <label style="display: block;">Amount</label>
                    $<input type="text" name="amount" value="0.00" style="display: inline; width: 100px;" />
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <label>Title</label>
                    <input type="text" name="title" value="" />
                </fieldset>

                <fieldset>
                    <label>Send reminder email this many days before due date:</label>
                    <select name="email_days_before">
                        <?php for ($i=0; $i<=30; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <input class="blue" type="submit" value="Add" />

            </div>

            </form>

        </section>

        <div class="scheduled_payments table" style="margin: 0px; border: 0px none;">

            <header style="clear: both;">
                <span class="scheduled_payment_dt_due">Due Date</span>
                <span class="scheduled_payment_amount">Amount</span>
                <span class="scheduled_payment_title">Title</span>
                <span class="scheduled_payment_email_on">Email On</span>
                <span class="scheduled_payment_edit">Edit</span>
                <span class="scheduled_payment_delete">Delete</span>
            </header>

            <?php

            // GET SCHEDULED_PAYMENTS
            $spayments_query = "SELECT * FROM `" .$_uccms_ecomm->tables['scheduled_payments']. "` WHERE (`order_id`=" .$order['id']. ") ORDER BY `dt_due` ASC";
            $spayments_q = sqlquery($spayments_query);

            // HAVE SCHEDULED PAYMENTS
            if (sqlrows($spayments_q) > 0) {

                ?>

                <ul class="items">

                    <?php

                    // LOOP
                    while ($spayment = sqlfetch($spayments_q)) {

                        ?>

                        <li class="item">

                            <div class="contain">
                                <section class="scheduled_payment_dt_due">
                                    <?php echo date('n/j/Y', strtotime($spayment['dt_due'])); ?>
                                </section>
                                <section class="scheduled_payment_amount">
                                    $<?php echo number_format($spayment['amount'], 2); ?>
                                </section>
                                <section class="scheduled_payment_title">
                                    <?php echo stripslashes($spayment['title']); ?>
                                </section>
                                <section class="scheduled_payment_email_on">
                                    <span style="<?php if (($spayment['dt_email'] <= date('Y-m-d')) || ($spayment['emailed'])) { echo 'opacity: .6;'; } ?>">
                                        <?php echo date('n/j/Y', strtotime($spayment['dt_email'])); if ($spayment['emailed']) { echo ' <i class="fa fa-check" title="Email sent"></i>'; } ?>
                                    </span>
                                </section>
                                <section class="scheduled_payment_edit">
                                    <a href="#" title="Edit" class="icon_edit"></a>
                                </section>
                                <section class="scheduled_payment_delete">
                                    <a onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');" title="Delete" class="icon_delete" href="../../orders/edit/processes/delete_scheduled_payment/?order_id=<?php echo $order['id']; ?>&id=<?php echo $spayment['id']; ?>&from=quote"></a>
                                </section>
                            </div>

                            <div class="edit contain" style="display: none;">

                                <form enctype="multipart/form-data" action="../../orders/edit/processes/edit_scheduled_payment/" method="post">
                                <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
                                <input type="hidden" name="id" value="<?php echo $spayment['id']; ?>" />
                                <input type="hidden" name="from" value="quote" />

                                <div class="left">

                                    <fieldset>
                                        <label>Due Date</label>
                                        <div>
                                            <?php

                                            // FIELD VALUES
                                            $field = array(
                                                'id'        => 'scheduled_payment_due_date',
                                                'key'       => 'dt',
                                                'value'     => $spayment['dt_due'],
                                                'required'  => true,
                                                'options'   => array(
                                                    'default_today' => false
                                                )
                                            );

                                            // INCLUDE FIELD
                                            include(BigTree::path('admin/form-field-types/draw/date.php'));

                                            ?>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <label style="display: block;">Amount</label>
                                        $<input type="text" name="amount" value="<?php echo $spayment['amount']; ?>" style="display: inline; width: 100px;" />
                                    </fieldset>

                                </div>

                                <div class="right">

                                    <fieldset>
                                        <label>Title</label>
                                        <input type="text" name="title" value="<?php echo stripslashes($spayment['title']); ?>" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Send reminder email this many days before due date:</label>
                                        <select name="email_days_before">
                                            <?php for ($i=0; $i<=30; $i++) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($i == $spayment['email_days_before']) { ?>selected="selected"<?php } ?>><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>

                                    <input class="blue" type="submit" value="Update" />

                                </div>

                                </form>

                            </div>

                        </li>
                        <?php
                    }

                    ?>

                </ul>

                <?php

            // NO SCHEDULED PAYMENTS
            } else {
                ?>
                <div style="padding: 10px; text-align: center; font-size: .75em;">
                    No scheduled payments.
                </div>
                <?php
            }

            ?>

        </div>

    </div>

    <a name="log"></a>
    <div id="log" class="container legacy expand">

        <header>
            <h2><?php if ($order['id']) { ?><i class="fa fa-plus plus-minus" aria-hidden="true"></i> <?php } ?>Log</h2>
        </header>

        <section>

            <div class="contain">

                <?php

                // STATUS COLOR ARRAY
                $sca = $_uccms_ecomm->statusColors()['quote'];

                // GET MESSAGES
                $message_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_message_log']. "` WHERE (`order_id`=" .$order['id']. ") ORDER BY `dt` DESC";
                $message_q = sqlquery($message_query);
                while ($message = sqlfetch($message_q)) {
                    ?>
                    <div class="item">
                        <div class="info contain">
                            <div class="left">
                                <?=date('M j, Y g:i a', strtotime($message['dt']))?> - <?php echo ($message['by'] ? stripslashes($_uccms_ecomm->getSetting('store_name')) : 'Client') ?>
                            </div>
                            <div class="right">
                                <?php if ($message['email_log_id']) { ?><a href="../../view-email/?id=<?=$message['email_log_id']?>" target="blank"><i class="fa fa-envelope-o"></i></a> <?php } ?><span class="status" style="<?php if ($sca[$message['status']]) { echo 'color: ' .$sca[$message['status']]; } ?>"><?=ucwords($message['status'])?></span>
                            </div>
                        </div>
                        <div class="message">
                            <?=stripslashes($message['message'])?>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>

        </section>

    </div>

    <form action="../../orders/edit/clone/" method="get">
    <input type="hidden" name="id" value="<?php echo $order['id']; ?>" />
    <input type="hidden" name="type" value="quote" />

    <div id="clone" class="container legacy expand">
        <header><h2><i class="fa fa-plus plus-minus" aria-hidden="true"></i> Clone</h2></header>
        <section>

            <fieldset>
                <label>What</label>
                <select name="what">
                    <option value="cart">Cart only</option>
                    <option value="entire">Entire order</option>
                </select>
            </fieldset>

            <fieldset>
                <label>Convert</label>
                <input type="checkbox" name="convert" value="1" /> Create as quote
            </fieldset>

        </section>
        <footer>
            <input class="blue" type="submit" value="Process" />
        </footer>
    </div>

    <?php

}

?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>