<?php

// CLEAN UP
$id         = (int)$_REQUEST['id'];
$order_id   = (int)$_REQUEST['order_id'];

// HAVE REQUIRED INFO
if (($id) && ($order_id)) {

    // REMOVE ITEM
    $result = $_uccms_ecomm->cartRemoveItem($id, $order_id);

    // SUCCESS
    if ($result['success']) {

        $admin->growl('Quote', 'Item removed!');

        if ($result['item']['id']) {
            $_SESSION['uccmsAcct_webTracking']['events']['cart-remove'] = trim(stripslashes($result['item']['title']). ' (#' .$result['item']['id']. ')');
        }

        // UPDATE ORDER TOTAL
        $update_sql = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `order_total`=" .$_uccms_ecomm->orderTotal($order_id). " WHERE (`id`=" .$order_id. ")";
        sqlquery($update_sql);

    // FAILED
    } else {

        $admin->growl('Quote', 'Failed to remove item.');

        if ($result['item']['id']) {
            $_SESSION['uccmsAcct_webTracking']['events']['cart-remove-error'] = $result['message'];
        }

    }

// MISSING REQUIRED INFO
} else {
    $admin->growl('Quote', 'Missing info to remove item.');
}

BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$order_id. '#cart');

?>