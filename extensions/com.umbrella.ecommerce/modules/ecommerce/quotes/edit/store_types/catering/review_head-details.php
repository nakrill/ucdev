<?php

// MAP FIELDS
if ($extra['id']) {
    $vals['event']['location1'] = stripslashes($extra['event_location1']);
    $vals['event']['location2'] = stripslashes($extra['event_location2']);
    $vals['event']['address1'] = stripslashes($extra['event_address1']);
    $vals['event']['address2'] = stripslashes($extra['event_address2']);
    $vals['event']['city'] = stripslashes($extra['event_city']);
    $vals['event']['state'] = stripslashes($extra['event_state']);
    $vals['event']['zip'] = stripslashes($extra['event_zip']);
    $vals['event']['phone'] = stripslashes($extra['event_phone']);
    $vals['event']['date'] = stripslashes($extra['event_date']);
    $vals['event']['num_people'] = stripslashes($extra['event_num_people']);
    $vals['event']['notes'] = stripslashes($extra['event_notes']);
    $vals['service']['time'] = date('g:i a', strtotime(stripslashes($extra['service_time'])));
    $vals['service']['type'] = stripslashes($extra['service_type']);
    $vals['contact']['firstname'] = stripslashes($extra['contact_firstname']);
    $vals['contact']['lastname'] = stripslashes($extra['contact_lastname']);
    $vals['contact']['company'] = stripslashes($extra['contact_company']);
    $vals['contact']['address1'] = stripslashes($extra['contact_address1']);
    $vals['contact']['address2'] = stripslashes($extra['contact_address2']);
    $vals['contact']['city'] = stripslashes($extra['contact_city']);
    $vals['contact']['state'] = stripslashes($extra['contact_state']);
    $vals['contact']['zip'] = stripslashes($extra['contact_zip']);
    $vals['contact']['phone'] = stripslashes($extra['contact_phone']);
    $vals['contact']['email'] = stripslashes($extra['contact_email']);
}

// HAVE BILLING STATE AND/OR ZIP
if (($vals['contact']['state']) || ($vals['contact']['zip'])) {
    $taxvals['country'] = 'United States';
    $taxvals['state']   = $vals['contact']['state'];
    $taxvals['zip']     = $vals['contact']['zip'];
}

?>

<div class="details contain clearfix row">

    <div class="left contact col-md-4">
        <h6>Contact<?php if ((defined('BIGTREE_ADMIN_ROUTED')) || ($rhd_settings['edit_icon'])) { ?> <a href="<?php if ($rhd_settings['edit_icon_link']) { echo $rhd_settings['edit_icon_link']; } else { echo '#contact'; } ?>" title="Edit" class="edit_anchor"><i class="fa fa-pencil"></i></a><?php } ?></h6>
        <div class="name">
            <?php echo $vals['contact']['firstname']; ?> <?php echo $vals['contact']['lastname']; ?>
        </div>
        <?php if ($vals['contact']['company']) { ?>
            <div class="company">
                <?php echo $vals['contact']['company']; ?>
            </div>
        <?php } ?>
        <?php if ($vals['contact']['address1']) { ?>
            <div class="address1">
                <?php echo $vals['contact']['address1']; ?>
            </div>
        <?php } ?>
        <?php if ($vals['contact']['address2']) { ?>
            <div class="address2">
                <?php echo $vals['contact']['address2']; ?>
            </div>
        <?php } ?>
        <div class="csz">
            <?php echo $vals['contact']['city']; ?>, <?php echo $vals['contact']['state']; ?> <?php echo $vals['contact']['zip']; ?>
        </div>
        <?php if ($vals['contact']['phone']) { ?>
            <div class="phone">
                <?php echo $vals['contact']['phone']; ?>
            </div>
        <?php } ?>
        <?php if ($vals['contact']['email']) { ?>
            <div class="email">
                <a href="mailto:<?php echo $vals['contact']['email']; ?>" target="_blank"><?php echo $vals['contact']['email']; ?></a>
            </div>
        <?php } ?>
    </div>

    <div class="left delivery col-md-4">
        <h6>Delivery<?php if ((defined('BIGTREE_ADMIN_ROUTED')) || ($rhd_settings['edit_icon'])) { ?> <a href="<?php if ($rhd_settings['edit_icon_link']) { echo $rhd_settings['edit_icon_link']; } else { echo '#delivery'; } ?>" title="Edit" class="edit_anchor"><i class="fa fa-pencil"></i></a><?php } ?></h6>
        <?php if ($vals['event']['location1']) { ?>
            <div class="location1">
                <?php echo stripslashes($extra['event_location1']); ?>
            </div>
        <?php } ?>
        <?php if ($vals['event']['location2']) { ?>
            <div class="location2">
                <?php echo $vals['event']['location2']; ?>
            </div>
        <?php } ?>
        <?php if ($vals['event']['address1']) { ?>
            <div class="address1">
                <?php echo $vals['event']['address1']; ?>
            </div>
        <?php } ?>
        <?php if ($vals['event']['address2']) { ?>
            <div class="address2">
                <?php echo $vals['event']['address2']; ?>
            </div>
        <?php } ?>
        <div class="csz">
            <?php echo $vals['event']['city']; ?>, <?php echo $vals['event']['state']; ?> <?php echo $vals['event']['zip']; ?>
        </div>
        <?php if ($vals['event']['phone']) { ?>
            <div class="phone">
                <?php echo $vals['event']['phone']; ?>
            </div>
        <?php } ?>
        <?php if ($vals['event']['email']) { ?>
            <div class="email">
                <a href="mailto:<?php echo $vals['event']['email']; ?>" target="_blank"><?php echo $vals['event']['email']; ?></a>
            </div>
        <?php } ?>
    </div>

    <div class="left event col-md-4">
        <h6>Event<?php if ((defined('BIGTREE_ADMIN_ROUTED')) || ($rhd_settings['edit_icon'])) { ?> <a href="<?php if ($rhd_settings['edit_icon_link']) { echo $rhd_settings['edit_icon_link']; } else { echo '#event'; } ?>" title="Edit" class="edit_anchor"><i class="fa fa-pencil"></i></a><?php } ?></h6>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr class="date">
                <td>Date:&nbsp;</td>
                <td><?php echo date('n/j/Y', strtotime($vals['event']['date'])); ?></td>
            </tr>
            <tr class="time">
                <td>Service Time:&nbsp;</td>
                <td><?php echo $vals['service']['time']; ?></td>
            </tr>
            <tr class="type">
                <td>Service Type:&nbsp;</td>
                <td><?php
                    switch ($vals['service']['type']) {
                        case 'catering_event':
                            echo 'Event Catering';
                            break;
                        case 'catering_wedding':
                            echo 'Wedding Catering';
                            break;
                        case 'chef':
                            echo 'Chef';
                            break;
                        case 'beverage':
                            echo 'Beverage';
                            break;
                        case 'pickup':
                            echo 'Pickup';
                            break;
                        case 'dropoff':
                            echo 'Dropoff';
                            break;
                        case 'in_house':
                            echo 'In-House';
                            break;
                        case 'menu_tasting':
                            echo 'Menu Tasting';
                            break;
                    }
                 ?></td>
            </tr>
            <tr class="num_people">
                <td># People:&nbsp;</td>
                <td><?php echo $vals['event']['num_people']; ?></td>
            </tr>
        </table>
    </div>

</div>

<?php if ($vals['event']['notes']) { ?>
    <div class="notes">
        <h6>Notes<?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?> <a href="#notes" title="Edit" class="edit_anchor"><i class="fa fa-pencil"></i></a><?php } ?></h6>
        <?php echo nl2br($vals['event']['notes']); ?>
    </div>
<?php } ?>