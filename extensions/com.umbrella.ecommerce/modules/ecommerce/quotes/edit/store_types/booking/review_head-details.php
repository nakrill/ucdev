<?php

// MAP FIELDS
if ($vals['do'] == 'review') {
    $order['billing_firstname'] = stripslashes($vals['billing']['firstname']);
    $order['billing_lastname'] = stripslashes($vals['billing']['lastname']);
    $order['billing_company'] = stripslashes($vals['billing']['company']);
    $order['billing_address1'] = stripslashes($vals['billing']['address1']);
    $order['billing_address2'] = stripslashes($vals['billing']['address2']);
    $order['billing_city'] = stripslashes($vals['billing']['city']);
    $order['billing_state'] = stripslashes($vals['billing']['state']);
    $order['billing_zip'] = stripslashes($vals['billing']['zip']);
    $order['billing_phone'] = stripslashes($vals['billing']['phone']);
    $order['billing_email'] = stripslashes($vals['billing']['email']);
    $order['shipping_same'] = stripslashes($vals['shipping']['same']);
    $order['shipping_firstname'] = stripslashes($vals['shipping']['firstname']);
    $order['shipping_lastname'] = stripslashes($vals['shipping']['lastname']);
    $order['shipping_company'] = stripslashes($vals['shipping']['company']);
    $order['shipping_address1'] = stripslashes($vals['shipping']['address1']);
    $order['shipping_address2'] = stripslashes($vals['shipping']['address2']);
    $order['shipping_city'] = stripslashes($vals['shipping']['city']);
    $order['shipping_state'] = stripslashes($vals['shipping']['state']);
    $order['shipping_zip'] = stripslashes($vals['shipping']['zip']);
    $order['shipping_phone'] = stripslashes($vals['shipping']['phone']);
    $order['shipping_email'] = stripslashes($vals['shipping']['email']);
    $order['notes'] = stripslashes($vals['order']['notes']);
}

// HAVE BILLING STATE AND/OR ZIP
if (($order['billing_state']) || ($order['billing_zip'])) {
    $taxvals['country'] = 'United States';
    $taxvals['state']   = $order['billing_state'];
    $taxvals['zip']     = $order['billing_zip'];
}

?>

<div class="details contain clearfix row">

    <div class="left billing col-md-6">
        <h6>Billing<?php if ((defined('BIGTREE_ADMIN_ROUTED')) || ($rhd_settings['edit_icon'])) { ?> <a href="<?php if ($rhd_settings['edit_icon_link']) { echo $rhd_settings['edit_icon_link']; } else { echo '#billing'; } ?>" title="Edit" class="edit_anchor"><i class="fa fa-pencil"></i></a><?php } ?></h6>
        <div class="name">
            <?php echo $order['billing_firstname']; ?> <?php echo $order['billing_lastname']; ?>
        </div>
        <?php if ($order['billing_company']) { ?>
            <div class="company">
                <?php echo $order['billing_company']; ?>
            </div>
        <?php } ?>
        <?php if ($order['billing_address1']) { ?>
            <div class="address1">
                <?php echo $order['billing_address1']; ?>
            </div>
        <?php } ?>
        <?php if ($order['billing_address2']) { ?>
            <div class="address2">
                <?php echo $order['billing_address2']; ?>
            </div>
        <?php } ?>
        <div class="csz">
            <?php echo $order['billing_city']; ?>, <?php echo $order['billing_state']; ?> <?php echo $order['billing_zip']; ?>
        </div>
        <?php if ($order['billing_phone']) { ?>
            <div class="phone">
                <?php echo $order['billing_phone']; ?>
            </div>
        <?php } ?>
        <?php if ($order['billing_email']) { ?>
            <div class="email">
                <a href="mailto:<?php echo $order['billing_email']; ?>" target="_blank"><?php echo $order['billing_email']; ?></a>
            </div>
        <?php } ?>
    </div>

    <div class="left shipping col-md-6">
        <h6>Contact<?php if ((defined('BIGTREE_ADMIN_ROUTED')) || ($rhd_settings['edit_icon'])) { ?> <a href="<?php if ($rhd_settings['edit_icon_link']) { echo $rhd_settings['edit_icon_link']; } else { echo '#contact'; } ?>" title="Edit" class="edit_anchor"><i class="fa fa-pencil"></i></a><?php } ?></h6>
        <?php if ($order['shipping_same']) { ?>
            Same as billing.
        <?php } else { ?>
            <?php if ($order['shipping_location1']) { ?>
                <div class="location1">
                    <?php echo $order['shipping_location1']; ?>
                </div>
            <?php } ?>
            <?php if ($order['shipping_location2']) { ?>
                <div class="location2">
                    <?php echo $order['shipping_location2']; ?>
                </div>
            <?php } ?>
            <?php if ($order['shipping_address1']) { ?>
                <div class="address1">
                    <?php echo $order['shipping_address1']; ?>
                </div>
            <?php } ?>
            <?php if ($order['shipping_address2']) { ?>
                <div class="address2">
                    <?php echo $order['shipping_address2']; ?>
                </div>
            <?php } ?>
            <div class="csz">
                <?php echo $order['shipping_city']; ?>, <?php echo $order['shipping_state']; ?> <?php echo $order['shipping_zip']; ?>
            </div>
            <?php if ($order['shipping_phone']) { ?>
                <div class="phone">
                    <?php echo $order['shipping_phone']; ?>
                </div>
            <?php } ?>
            <?php if ($order['shipping_email']) { ?>
                <div class="email">
                    <a href="mailto:<?php echo $order['shipping_email']; ?>" target="_blank"><?php echo $order['shipping_email']; ?></a>
                </div>
            <?php } ?>
        <?php } ?>
    </div>

</div>

<?php if ($order['notes']) { ?>
    <div class="notes">
        <h6>Notes<?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?> <a href="#notes" title="Edit" class="edit_anchor"><i class="fa fa-pencil"></i></a><?php } ?></h6>
        <?php echo nl2br($order['notes']); ?>
    </div>
<?php } ?>