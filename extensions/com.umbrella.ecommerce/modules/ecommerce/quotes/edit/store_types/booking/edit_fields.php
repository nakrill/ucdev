<script type="text/javascript">

    $(document).ready(function() {

        // SHIPPING SAME TOGGLE
        $('#shipping_same').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_shipping_same').hide();
            } else {
                $('#toggle_shipping_same').show();
            }
        });

    });

</script>

<a name="billing"></a>
<div class="container legacy billing <?php if ($order['id']) { echo 'expand'; } ?>">

    <header>
        <h2>Billing Info</h2>
    </header>

    <section>

        <fieldset>
            <label>First Name</label>
            <input type="text" name="billing[firstname]" value="<?php echo stripslashes($order['billing_firstname']); ?>" />
        </fieldset>

        <fieldset>
            <label>Last Name</label>
            <input type="text" name="billing[lastname]" value="<?php echo stripslashes($order['billing_lastname']); ?>" />
        </fieldset>

        <fieldset>
            <label>Company Name</label>
            <input type="text" name="billing[company]" value="<?php echo stripslashes($order['billing_company']); ?>" />
        </fieldset>

        <fieldset>
            <label>Address</label>
            <input type="text" name="billing[address1]" value="<?php echo stripslashes($order['billing_address1']); ?>" />
        </fieldset>

        <fieldset>
            <label>Address (Apt, Bldg)</label>
            <input type="text" name="billing[address2]" value="<?php echo stripslashes($order['billing_address2']); ?>" />
        </fieldset>

        <fieldset>
            <label>City</label>
            <input type="text" name="billing[city]" value="<?php echo stripslashes($order['billing_city']); ?>" />
        </fieldset>

        <fieldset>
            <label>State</label>
            <select name="billing[state]">
                <option value="">Select</option>
                <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                    <option value="<?php echo $state_code; ?>" <?php if ($state_code == $order['billing_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                <?php } ?>
            </select>
        </fieldset>

        <fieldset>
            <label>Zip</label>
            <input type="text" name="billing[zip]" value="<?php echo stripslashes($order['billing_zip']); ?>" />
        </fieldset>

        <fieldset>
            <label>Phone</label>
            <input type="text" name="billing[phone]" value="<?php echo stripslashes($order['billing_phone']); ?>" />
        </fieldset>

        <fieldset>
            <label>Email</label>
            <input type="text" name="billing[email]" value="<?php echo stripslashes($order['billing_email']); ?>" />
        </fieldset>

    </section>

    <?php if ($order['id']) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>

<a name="shipping"></a>
<div class="container legacy shipping <?php if ($order['id']) { echo 'expand'; } ?>">

    <header>
        <h2>Shipping Info</h2>
    </header>

    <section>

        <fieldset>
            <input id="shipping_same" type="checkbox" name="shipping[same]" value="1" <?php if ($order['shipping_same']) { ?>checked="checked"<?php } ?> />
            <label class="for_checkbox">Same as Billing</label>
        </fieldset>

        <div id="toggle_shipping_same" class="contain" style="clear: right; <?php if ($order['shipping_same']) { ?>display: none;<?php } ?> padding-top: 10px;">

            <fieldset>
                <label>First Name</label>
                <input type="text" name="shipping[firstname]" value="<?php echo stripslashes($order['shipping_firstname']); ?>" />
            </fieldset>

            <fieldset>
                <label>Last Name</label>
                <input type="text" name="shipping[lastname]" value="<?php echo stripslashes($order['shipping_lastname']); ?>" />
            </fieldset>

            <fieldset>
                <label>Company Name</label>
                <input type="text" name="shipping[company]" value="<?php echo stripslashes($order['shipping_company']); ?>" />
            </fieldset>

            <fieldset>
                <label>Address</label>
                <input type="text" name="shipping[address1]" value="<?php echo stripslashes($order['shipping_address1']); ?>" />
            </fieldset>

            <fieldset>
                <label>Address (Apt, Bldg)</label>
                <input type="text" name="shipping[address2]" value="<?php echo stripslashes($order['shipping_address2']); ?>" />
            </fieldset>

            <fieldset>
                <label>City</label>
                <input type="text" name="shipping[city]" value="<?php echo stripslashes($order['shipping_city']); ?>" />
            </fieldset>

            <fieldset>
                <label>State</label>
                <select name="shipping[state]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?php echo $state_code; ?>" <?php if ($state_code == $order['shipping_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset>
                <label>Zip</label>
                <input type="text" name="shipping[zip]" value="<?php echo stripslashes($order['shipping_zip']); ?>" />
            </fieldset>

            <fieldset>
                <label>Phone</label>
                <input type="text" name="shipping[phone]" value="<?php echo stripslashes($order['shipping_phone']); ?>" />
            </fieldset>

            <fieldset>
                <label>Email</label>
                <input type="text" name="shipping[email]" value="<?php echo stripslashes($order['shipping_email']); ?>" />
            </fieldset>

    </section>

    <?php if ($order['id']) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>

<a name="notes"></a>
<div class="container legacy notes">

    <header>
        <h2>Notes</h2>
    </header>

    <section>

        <fieldset>
            <textarea name="order[notes]" style="height: 58px;"><?php echo stripslashes($order['notes']); ?></textarea>
        </fieldset>

    </section>

    <?php if ($order['id']) { ?>
        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>
    <?php } ?>

</div>

<?php if (!$order['id']) { ?>
    <input class="blue" type="submit" value="Save" />
<?php } ?>