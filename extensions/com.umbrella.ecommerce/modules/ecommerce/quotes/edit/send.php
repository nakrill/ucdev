<?php

// CLEAN UP
$id = (int)$_POST['id'];

// HAVE ORDER ID
if ($id) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        // GET EXTRA INFO
        $extra = $_uccms_ecomm->cartExtra($id);

        // DEFAULTS
        if (!$_POST['send']['from_name']) $_POST['send']['from_name'] = stripslashes($_uccms_ecomm->getSetting('email_from_name'));
        if (!$_POST['send']['from_email']) $_POST['send']['from_email'] = stripslashes($_uccms_ecomm->getSetting('email_from_email'));
        if (!$_POST['send']['to_name']) $_POST['send']['to_name'] = trim(stripslashes($extra['contact_firstname']. ' ' .$extra['contact_lastname']));
        if (!$_POST['send']['to_email']) $_POST['send']['to_email'] = stripslashes($extra['contact_email']);
        if (!$_POST['send']['subject']) $_POST['send']['subject'] = 'Quote Available';
        if (!$_POST['send']['message']) $_POST['send']['message'] = 'Your quote is ready to view!';

        // LINK TO VIEW QUOTE ONLINE
        $link = '<a href="' .WWW_ROOT . $_uccms_ecomm->storePath(). '/quote/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']). '&uutp_email=' .$_POST['send']['to_email']. '" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">View Quote</a>';

        // EMAIL SETTINGS
        $esettings = array(
            'template'      => 'quote_send.html',
            'subject'       => $_POST['send']['subject'],
            'order_id'      => $order['id'],
            'from_name'     => $_POST['send']['from_name'],
            'from_email'    => $_POST['send']['from_email'],
            'to_name'       => $_POST['send']['to_name'],
            'to_email'      => $_POST['send']['to_email'],
            'sent_by'       => $_uccms_ecomm->adminID()
        );

        // EMAIL VARIABLES
        $evars = array(
            'date'                  => date('n/j/Y'),
            'time'                  => date('g:i A T'),
            'order_id'              => $order['id'],
            'order_hash'            => $order['hash'],
            'message'               => $_POST['send']['message'],
            'quote_link'            => $link
        );

        // SEND EMAIL
        $eresult = $_uccms_ecomm->sendEmail($esettings, $evars);

        // HAVE ERROR
        if ($eresult['err']) {

            $admin->growl('Error', 'Failed to send quote. (Error: ' .$eresult['err']. ')');

        // NO ERROR
        } else {

            $admin->growl('Quote Sent', 'Quote sent to ' .$_POST['send']['to_email']. '!');

            // SENDING COPY
            if ($_POST['send']['copy']) {

                // EMAIL SETTINGS
                $esettings['to_email'] = $_POST['send']['copy'];

                // SEND EMAIL
                $_uccms_ecomm->sendEmail($esettings, $evars);

            }

            // LOG MESSAGE
            $_uccms_ecomm->logQuoteMessage(array(
                'order_id'      => $order['id'],
                'by'            => $_uccms_ecomm->adminID(),
                'message'       => $_POST['send']['message'],
                'status'        => 'presented',
                'email_log_id'  => $eresult['log_id']
            ));

            // UPDATE QUOTE
            $update_sql = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='presented' WHERE (`id`=" .$order['id']. ")";
            sqlquery($update_sql);

        }

    // ORDER NOT FOUND
    } else {
        $admin->growl('Quote', 'Quote not found.');
    }

// NO ORDER ID
} else {
    $admin->growl('Quote', 'Quote ID not specified.');
}

BigTree::redirect(MODULE_ROOT. 'quotes/edit/?id=' .$id);

?>