<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status'        => 'deleted',
        'deleted_by'    => $_uccms_ecomm->adminID()
    );

    // UPDATE
    $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). ", `deleted_dt`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Quote', 'Quote deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Quote', 'Failed to delete quote.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Quote', 'No quote specified.');
}

BigTree::redirect(MODULE_ROOT.'quotes/');

?>