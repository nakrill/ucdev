<?php

$bigtree['breadcrumb'][] = [ 'title' => 'Quotes', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<script type="text/javascript">

    $(document).ready(function() {

        // STATUS SELECT CHANGE
        $('#orders summary .status select').change(function() {
            $('#form_orders').submit();
        });

    });

</script>

<form id="form_orders" method="get">
<input type="hidden" name="customer_id" value="<?=(int)$_REQUEST['customer_id']?>" />

<?php

// CLEAN UP
$customer_id = (int)$_REQUEST['customer_id'];

// CUSTOMER SPECIFIED
if ($customer_id > 0) {

    // ACCOUNT CLASS - UPDATE CONFIG
    $_uccms['_account']->setConfig(array(
        'admin' => array(
            'from'      => true,
            'acct_id'   => $customer_id
        ))
    );

    // GET CUSTOMER INFO
    $customer = $_uccms['_account']->getAccount('', true);

    if (($customer['firstname']) || ($customer['lastname'])) {
        $display_name = trim(stripslashes($customer['firstname']. ' ' .$customer['lastname']));
    } else {
        $display_name = stripslashes($customer['username']);
    }

    ?>

    <div id="customer" style="margin-bottom: 15px; padding: 10px; border: 1px solid #eee; background-color: #fafafa; border-radius: 5px;">

        <h4 style="margin-bottom: 0px; color: #555;">Customer: <?=$display_name?></h4>
        <div style="margin-bottom: 8px; padding-bottom: 8px; border-bottom: 1px solid #eee;">
            <a href="#" class="remove">Remove</a> | <a href="../customers/">Back to customers</a>
        </div>

        <div class="section customer_info contain" style="padding-bottom: 10px;">

            <?php

            // GET LAST ORDER
            $last_order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .$customer_id. ") ORDER BY `dt` DESC LIMIT 1";
            $last_order_q = sqlquery($last_order_query);
            $latest_order = sqlfetch($last_order_q);

            // HAVE LAST ORDER
            if ($latest_order['id']) {

                ?>

                <style type="text/css">
                    #customer .customer_info .fields.billing .phone, #customer .customer_info .fields.billing .email, #customer .customer_info .fields.shipping .phone, #customer .customer_info .fields.shipping .email {
                        padding-top: 4px;
                    }
                </style>

                <div class="left" style="width: 30%;">
                    <h3>Billing</h3>
                    <div class="fields billing">
                        <div class="name">
                            <?php echo trim(stripslashes($latest_order['billing_firstname']. ' ' .$latest_order['billing_lastname'])); ?>
                        </div>
                        <?php if ($latest_order['billing_company']) { ?>
                            <div class="company"><?php echo stripslashes($latest_order['billing_company']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['billing_address1']) { ?>
                            <div class="address1"><?php echo stripslashes($latest_order['billing_address1']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['billing_address2']) { ?>
                            <div class="address2"><?php echo stripslashes($latest_order['billing_address2']);  ?></div>
                        <?php } ?>
                        <?php
                        $billing_csz = array();
                        if ($latest_order['billing_city']) $billing_csz[] = stripslashes($latest_order['billing_city']);
                        if ($latest_order['billing_state']) $billing_csz[] = stripslashes($latest_order['billing_state']);
                        if ($latest_order['billing_zip']) $billing_csz[] = stripslashes($latest_order['billing_zip']);
                        if (count($billing_csz) > 0) {
                            ?>
                            <div class="csz"><?php echo implode(' ', $billing_csz); ?></div>
                            <?php
                        }
                        ?>
                        <?php if ($latest_order['billing_phone']) { ?>
                            <div class="phone"><?php echo stripslashes($latest_order['billing_phone']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['billing_email']) { ?>
                            <div class="email"><?php echo stripslashes($latest_order['billing_email']);  ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="left" style="width: 30%;">
                    <h3>Shipping</h3>
                    <div class="fields shipping">
                        <div class="name">
                            <?php echo trim(stripslashes($latest_order['shipping_firstname']. ' ' .$latest_order['shipping_lastname'])); ?>
                        </div>
                        <?php if ($latest_order['shipping_company']) { ?>
                            <div class="company"><?php echo stripslashes($latest_order['shipping_company']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['shipping_address1']) { ?>
                            <div class="address1"><?php echo stripslashes($latest_order['shipping_address1']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['shipping_address2']) { ?>
                            <div class="address2"><?php echo stripslashes($latest_order['shipping_address2']);  ?></div>
                        <?php } ?>
                        <?php
                        $shipping_csz = array();
                        if ($latest_order['shipping_city']) $shipping_csz[] = stripslashes($latest_order['shipping_city']);
                        if ($latest_order['shipping_state']) $shipping_csz[] = stripslashes($latest_order['shipping_state']);
                        if ($latest_order['shipping_zip']) $shipping_csz[] = stripslashes($latest_order['shipping_zip']);
                        if (count($shipping_csz) > 0) {
                            ?>
                            <div class="csz"><?php echo implode(' ', $shipping_csz); ?></div>
                            <?php
                        }
                        ?>
                        <?php if ($latest_order['shipping_phone']) { ?>
                            <div class="phone"><?php echo stripslashes($latest_order['shipping_phone']);  ?></div>
                        <?php } ?>
                        <?php if ($latest_order['shipping_email']) { ?>
                            <div class="email"><?php echo stripslashes($latest_order['shipping_email']);  ?></div>
                        <?php } ?>
                    </div>
                </div>

                <?php

            }

            ?>

            <div class="left" style="width: auto;">
                <h3>Customer</h3>
                <a href="../quotes/?customer_id=<?php echo $customer_id; ?>&status=all" class="button" style="background-color: #ccc; line-height: 2.4em;">Quotes</a>
                <a href="../orders/?customer_id=<?php echo $customer_id; ?>&status=all" class="button" style="line-height: 2.4em;">Orders</a>
                <a href="../customers/edit/?id=<?php echo $customer_id; ?>" class="button" style="line-height: 2.4em;">Edit Customer</a>
            </div>

        </div>

    </div>

    <?php

}

?>

<div class="search_paging contain">
    <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
    <span class="form_search_icon"></span>
    <nav id="view_paging" class="view_paging" style="display: none;"></nav>
</div>

<div id="orders" class="table">
    <summary>
        <h2>Quotes</h2>
        <div class="status">
            <select name="status" class="custom_control">
                <option value="">All</option>
                <option value="requested" <?php if ($_REQUEST['status'] == 'reqested') { ?>selected="selected"<?php } ?>>Requested</option>
                <option value="preparing" <?php if ($_REQUEST['status'] == 'preparing') { ?>selected="selected"<?php } ?>>Preparing</option>
                <option value="presented" <?php if ($_REQUEST['status'] == 'presented') { ?>selected="selected"<?php } ?>>Presented</option>
                <option value="declined" <?php if ($_REQUEST['status'] == 'declined') { ?>selected="selected"<?php } ?>>Declined</option>
                <option value="accepted" <?php if ($_REQUEST['status'] == 'accepted') { ?>selected="selected"<?php } ?>>Accepted</option>
                <option value="cloned" <?php if ($_REQUEST['status'] == 'cloned') { ?>selected="selected"<?php } ?>>Cloned</option>
            </select>
        </div>
    </summary>
    <header style="clear: both;">
        <?php foreach ($_uccms_ecomm->adminOQListColumns() as $col_class => $col_title) { ?>
            <span class="<?=$col_class?>"><?=$col_title?></span>
        <?php } ?>
    </header>
    <ul id="results" class="items">
        <?php if ($_REQUEST['status'] == 'accepted') { ?>
            <li style="padding: 10px 0; text-align: center;">
                Accepted quotes are turned into <a href="../orders/">orders</a>.
            </li>
        <?php } else {
            $_REQUEST['quote'] = 1;
            include(EXTENSION_ROOT. 'ajax/admin/orders/get-page.php');
        } ?>
    </ul>
</div>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/orders/get-page/?page=1&quote=1&query=" + escape($("#query").val()));
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/orders/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&quote=1&query=" +escape($("#query").val())+ '&status=' +escape($('#orders summary .status select').val()));
        return false;
    });
</script>

</form>