<?php

// PAGE TITLE
$bigtree['admin_title'] = 'Customers - Ecommerce';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Customers', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #customers > .summary {
        display: block;
    }
    #customers .loading, #customers .no-results {
        padding: 10px;
        text-align: center;
        font-size: .9em;
    }

</style>

<script type="text/javascript">

    $(function() {

        getCustomersList();

        // FILTER - KEYWORD
        $('#customers .summary .filter input[name="q"]').keyup(function(e) {
            delay(function() {
                getCustomersList($('#customers .summary .filter form').serialize());
            }, 400);
        });

        // STATUS CLICK
        $('#customers .summary .filter .status a').click(function(e) {
            $('#customers .summary .filter .status a').removeClass('active');
            $(this).addClass('active');
            $('#customers .summary .filter input[name="status"]').val($(this).data('id'));
            getCustomersList($('#customers .summary .filter form').serialize());
        });

        // PAGING CLICK
        $('#customers .summary .view_paging').on('click', 'a', function(e) {
            $('#customers .summary .filter input[name="page"]').val($(this).attr('href').replace('#', ''));
            getCustomersList($('#customers .summary .filter form').serialize());
        });

    });

    // GET THE CUSTOMER LIST
    function getCustomersList(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/customers/list/', params, function(data) {
            $('#customers .customer-list').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<div id="customers" class="table">

    <div class="summary clearfix">

        <div class="filter">
            <form>
            <input type="hidden" name="page" value="" />
            <input type="hidden" name="status" value="" />

            <div class="control search">
                <input type="text" name="q" value="" class="form-control" placeholder="Search" />
            </div>

            <div class="control status links">
                <a href="#" data-id="" class="active">All</a>
                <?php foreach ($_uccms_ecomm->customerStatuses() as $status_id => $status) { ?>
                    <a href="#" data-id="<?php echo $status_id; ?>"><?php echo $status['title']; ?></a>
                <?php } ?>
            </div>

            </form>
        </div>

        <a class="btn btn-primary action" href="./edit/" <? /*data-toggle="modal" data-target="#modal-editCustomers"*/ ?> data-id="0"><i class="fas fa-plus"></i>Add customer</a>

        <div class="paging ajax">
            <nav class="view_paging"></nav>
        </div>

    </div>

    <div class="customer-list table-responsive">
        <div class="loading">Loading..</div>
    </div>

</div>