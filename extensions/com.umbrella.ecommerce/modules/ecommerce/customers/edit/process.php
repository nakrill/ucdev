<?php

$customer_id = (int)$_POST['customer']['id'];

// FORM SUBMITTED
if ($customer_id) {

    $_POST['pricegroup'][1] = 1; // ALWAYS RETAIL

    // GET PRICEGROUPS
    $pga = array_keys($_POST['pricegroup']);
    asort($pga);

    // COMBINE PRICEGROUPS
    $pricegroups = implode(',', $pga);

    // DB COLUMNS
    $columns = array(
        'id'            => $customer_id,
        'pricegroups'   => $pricegroups
    );

    // UPDATE CUSTOMER
    $update_query = "
    INSERT INTO `" .$_uccms_ecomm->tables['customers']. "`
    SET " .uccms_createSet($columns). "
    ON DUPLICATE KEY UPDATE " .uccms_createSet($columns). "
    ";
    if (sqlquery($update_query)) {
        $admin->growl('Customers', 'Customer updated.');
    } else {
        $admin->growl('Customers', 'Failed to update customer.');
    }

}

BigTree::redirect(MODULE_ROOT.'customers/edit/?id=' .$customer_id);

?>