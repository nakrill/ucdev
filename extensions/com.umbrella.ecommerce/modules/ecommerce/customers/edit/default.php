<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

$customer = [];

// ID SPECIFIED
if ($id) {

    // GET CUSTOMER
    $customer = $_uccms_ecomm->getCustomer($id);

    // NO CUSTOMER ID
    if (!$customer['id']) {
        $admin->growl('Error', 'Customer not found.');
        BigTree::redirect(MODULE_ROOT.'customers/');
    }

    // CONTACTS
    $contacts = $_uccms_ecomm->customerContacts($customer['id']);

}

// PAGE TITLE
$bigtree['admin_title'] = ($customer['id'] ? 'Edit' : 'Add'). ' Customer - Ecommerce';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Customers', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => ($customer['id'] ? 'Edit' : 'Add'). ' Customer', 'link' => '' ];

// MODAL - EDIT JOB
include_once(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/modules/elements/contacts/modal_add-edit.php');

// ACCOUNT CLASS - UPDATE CONFIG
$_uccms['_account']->setConfig(array(
    'admin' => array(
        'from'      => true,
        'acct_id'   => $customer['id']
    ))
);

if (($customer['firstname']) || ($customer['lastname'])) {
    $display_name = trim(stripslashes($customer['firstname']. ' ' .$customer['lastname']));
} else {
    $display_name = stripslashes($customer['email']);
}

$editContactData = [
    'customer_id' => $customer['id'],
];

?>


<style type="text/css">

    #form_customer {
        margin-top: 30px;
    }

    #customer .contactInfo span {
        display: block;
    }

</style>

<h2><?php echo $display_name; ?></h2>

<div id="customer">

    <div class="customer_contacts contain">
        <div class="row">

            <div class="billing col-md-4">
                <h3>Billing <a href="#" class="modalLink-editContact" data-contactid="<?php echo $contacts['default']['billing']; ?>" data-contactdata="<?php echo base64_encode(json_encode(array_merge($editContactData, ['default' => ['billing']]))); ?>"><i class="edit fas fa-pencil-alt"></i></a></h3>
                <?php if ($contacts['default']['billing']) { ?>
                    <div class="contactInfo">
                        <?php echo $_uccms_ecomm->formatContactInfo($contacts['contacts'][$contacts['default']['billing']]); ?>
                    </div>
                <?php } ?>
            </div>

            <div class="billing col-md-4">
                <h3>Shipping <a href="#" class="modalLink-editContact" data-contactid="<?php echo $contacts['default']['shipping']; ?>" data-contactdata="<?php echo base64_encode(json_encode(array_merge($editContactData, ['default' => ['shipping']]))); ?>"><i class="edit fas fa-pencil-alt"></i></a></h3>
                <?php if ($contacts['default']['shipping']) { ?>
                    <div class="contactInfo">
                        <?php echo $_uccms_ecomm->formatContactInfo($contacts['contacts'][$contacts['default']['shipping']]); ?>
                    </div>
                <?php } ?>
            </div>

            <div class="delivery col-md-4">
                <h3>Delivery <a href="#" class="modalLink-editContact" data-contactid="<?php echo $contacts['default']['delivery']; ?>" data-contactdata="<?php echo base64_encode(json_encode(array_merge($editContactData, ['default' => ['delivery']]))); ?>"><i class="edit fas fa-pencil-alt"></i></a></h3>
                <?php if ($contacts['default']['delivery']) { ?>
                    <div class="contactInfo">
                        <?php echo $_uccms_ecomm->formatContactInfo($contacts['contacts'][$contacts['default']['delivery']]); ?>
                    </div>
                <?php } ?>
            </div>

        </div>

    </div>

    <form id="form_customer" enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="customer[id]" value="<?php echo $customer['id']; ?>" />

    <div class="container legacy">

        <header>
            <h2>General</h2>
            <a href="<?=ADMIN_ROOT?>accounts/edit/?id=<?=$customer['id']?>" target="_blank" class="button">Edit Account</a>
        </header>

        <section>

            <div class="contain">

                <div class="left last">

                    <fieldset>
                        <label>Price Groups</label>
                        <div>
                            <?php foreach ($_uccms_ecomm->priceGroups() as $group_id => $group) { ?>
                                <fieldset style="margin-bottom: 4px;">
                                    <input type="checkbox" name="pricegroup[<?php echo $group_id; ?>]" value="1" <?php if (in_array($group_id, (array)$customer['pricegroups'])) { ?>checked="checked"<?php } ?> /><label class="for_checkbox"><?php echo $group['title']; ?></label>
                                </fieldset>
                            <?php } ?>
                        </div>
                    </fieldset>

                </div>

            </div>

        </section>

        <footer style="clear: both;">
            <input class="blue" type="submit" value="Save" />
        </footer>

    </div>

    </form>

</div>