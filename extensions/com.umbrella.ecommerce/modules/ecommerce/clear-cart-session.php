<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// CLEAR CART ID
$_uccms_ecomm->clearCartID(true);

$admin->growl('Cart', 'Session cleared.');

if ($_REQUEST['redirect']) {
    $redir = urldecode($_REQUEST['redirect']);
}

// SEND BACK
BigTree::redirect(MODULE_ROOT . $redir);

?>