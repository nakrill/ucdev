<?php

echo 'un-exit';
exit;

//echo 'Memory Limit: ' .ini_get('memory_limit'). "\n";
/*
    - If limit is too low, edit /site/php.ini and set memory_limit = 512M
    - Running from command line: php -d memory_limit=512M manual.php
*/

##########################################################

// BEING RUN FROM COMMAND LINE OR NOT
//$cli = (php_sapi_name() == 'cli' ? true : false);
$cli = true;

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('extensions/com.umbrella.ecommerce/modules/ecommerce/export/revsocial.php', '', strtr(__FILE__, "\\", "/"));
    require_once($server_root. 'custom/environment.php');
    require_once($server_root. 'custom/settings.php');
    require_once($server_root. 'core/bootstrap.php');

}

// INIT CLASSES
$_uccms_ecomm   = new uccms_Ecommerce;


// GET ORDERS
$orders_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE ((`status`='charged') OR (`status`='processing') OR (`status`='shipped') OR (`status`='complete'))";
$orders_q = sqlquery($orders_query);

echo sqlrows($orders_q). ' orders found.';
echo ($cli ? "\n" : '<br />');

$num_success    = 0;
$num_failed     = 0;

$site = parse_url(WWW_ROOT);

// LOOP
while ($order = sqlfetch($orders_q)) {

    $rs_items = array();

    // GET EXTRA INFO
    //$extra = $_uccms_ecomm->cartExtra($order['id']);

    $subtotal = 0;

    // GET ORDER ITEMS
    $citems = $_uccms_ecomm->cartItems($order['id']);

    // HAVE ITEMS
    if (count($citems) > 0) {

        // ATTRIBUTE ARRAY
        $attra = array();

        // LOOP THROUGH ITEMS
        foreach ($citems as $oitem) {

            // GET ITEM INFO
            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

            // ADD TO QUANTITY COUNT
            $rs_items['num_quantity'] += $oitem['quantity'];

            // PRODUCT OPTIONS (ATTRIBUTES)
            $options = array();
            if (count($item['other']['options']) > 0) {
                foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra) as $attr) {
                    $options[$attr['title']] = $attr['options'];
                }
            }

            // ADD TO PRODUCTS ARRAY
            $rs_items['products'][] = array(
                'sku'       => stripslashes($item['sku']),
                'name'      => stripslashes($item['title']),
                'options'   => $options,
                'quantity'  => $oitem['quantity'],
                'total'     => $item['other']['total']
            );

            $subtotal += $item['other']['total'];

        }

        // HAVE TAX OVERRIDE
        if ($order['override_tax']) {
            $tax['total'] = number_format($order['override_tax'], 2);

        // NORMAL TAX CALCULATION
        } else {

            // HAVE BILLING STATE AND/OR ZIP
            if (($vals['contact']['state']) || ($vals['contact']['zip'])) {
                $taxvals['country'] = 'United States';
                $taxvals['state']   = $vals['contact']['state'];
                $taxvals['zip']     = $vals['contact']['zip'];
            }

            //$taxvals['quote'] = false;

            // GET TAX
            $tax = $_uccms_ecomm->cartTax($order['id'], $citems, $taxvals);

        }

        // HAVE SHIPPING OVERRIDE
        if ($order['override_shipping']) {
            $shipping['markup'] = number_format($order['override_shipping'], 2);
        } else {
            $shipping['markup'] = 0.00;
        }

        // ADD VALUES TO ORDER ARRAY
        $order['tax']       = $tax['total'];
        $order['shipping']  = $shipping['markup'];

    }

    // CALCULATE GRATUITY
    if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
        $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);
    }

    // GET DISCOUNT
    $discount = $_uccms_ecomm->cartDiscount($order, $citems);

    // HAVE DISCOUNT
    if ($discount['discount']) {
        $order['discount'] = $discount['discount'];
    }

    // CALCULATE TOTAL
    $order['total'] = $_uccms_ecomm->orderTotal($order);

    // GET BALANCE
    $balance = $_uccms_ecomm->orderBalance($order['id']);

    // CALCULATE PAID
    $paid = number_format($order['total'] - $balance, 2);

    // QUANTITY COUNT
    $rs_items['num_products'] = count($citems);
    $rs_items['num_quantity'] = $rs_items['num_quantity'];

    // ORDER DATA FOR REVSOCIAL API
    $rs_orderdata = array(
        'billing'           => array(
            'firstname' => stripslashes($order['billing_firstname']),
            'lastname'  => stripslashes($order['billing_lastname']),
            'address1'  => stripslashes($order['billing_address1']),
            'address2'  => stripslashes($order['billing_address2']),
            'city'      => stripslashes($order['billing_city']),
            'state'     => stripslashes($order['billing_state']),
            'zip'       => stripslashes($order['billing_zip']),
            'phone'     => stripslashes($order['billing_phone']),
            'email'     => stripslashes($order['billing_email'])
        ),
        'shipping'      => array(
            'firstname' => stripslashes($order['shipping_firstname']),
            'lastname'  => stripslashes($order['shipping_lastname']),
            'address1'  => stripslashes($order['shipping_address1']),
            'address2'  => stripslashes($order['shipping_address2']),
            'city'      => stripslashes($order['shipping_city']),
            'state'     => stripslashes($order['shipping_state']),
            'zip'       => stripslashes($order['shipping_zip']),
            'phone'     => stripslashes($order['shipping_phone']),
            'email'     => stripslashes($order['shipping_email'])
        )
    );

    // ORDER DATA FOR REVSOCIAL API
    $rs_orderdata['order']              = $order;
    $rs_orderdata['order']['subtotal']  = number_format($_uccms_ecomm->toFloat($subtotal), 2);
    $rs_orderdata['order']['total']     = number_format($_uccms_ecomm->toFloat($order['total']), 2);
    $rs_orderdata['order']['paid']      = $paid;
    $rs_orderdata['items']              = $rs_items;
    $rs_orderdata['timestamp']          = $order['dt'];

    //echo print_r($rs_orderdata);
    //exit;

    // RECORD ORDER WITH REVSOCIAL
    $response = $_uccms_ecomm->revsocial_recordOrder($order['id'], $order['customer_id'], $rs_orderdata);

    // DECODE RESULT
    $result = json_decode(json_decode($response->response), true);

    //echo print_r($result);
    //exit;

    file_put_contents('revsocial-order-export.txt', print_r($rs_orderdata, true). "\n" .print_r($result, true). "\n\n--------------------------------\n\n", FILE_APPEND);

    if ($result['success']) {
        $num_success++;
    } else {
        $num_failed++;
    }

}

//echo ($cli ? "\n" : '<br />');

echo ($cli ? "\n" : '<br />');
echo '<b>All done. (' .$num_success. ' Success. ' .$num_failed. ' Failed.)</b>';

?>