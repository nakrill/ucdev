<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Calendar', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

if ($_uccms_ecomm->storeType() == 'booking') {
    if (file_exists(dirname(__FILE__). '/store_types/booking/custom/default.php')) {
        include(dirname(__FILE__). '/store_types/booking/custom/default.php');
    } else {
        include(dirname(__FILE__). '/store_types/booking/default.php');
    }
} else {
    if (file_exists(dirname(__FILE__). '/store_types/general/custom/default.php')) {
        include(dirname(__FILE__). '/store_types/general/custom/default.php');
    } else {
        include(dirname(__FILE__). '/store_types/general/default.php');
    }
}

?>