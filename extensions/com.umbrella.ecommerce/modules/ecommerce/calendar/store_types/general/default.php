<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" media="screen" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.css" media="print" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>

<style type="text/css">
    .fc table {
        margin: 0px;
    }
    .fc-widget-header table {
        margin: 4px 0;
    }
    .fc-event {
        background-color: #ccc;
        border-color: #888;
    }
    .fc-day-grid-event .fc-content {
        padding: 2px;
        white-space: normal;
    }
    <?php
    foreach ($_uccms_ecomm->statusColors() as $type => $statuses) {
        foreach ($statuses as $status => $color) {
            if ($color) {
                echo '.fc-event.' .$type. '.' .$status. ' { background-color: ' .$color. '; }';
            }
        }
    }
    ?>
</style>

<script type="text/javascript">
    $(function() {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            aspectRatio: 1,
            defaultView: 'month',
            events: '<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/calendar/store_types/general/events/',
            eventRender: function(event, element, view) {
                element.find('.fc-title').html(event.title.replace(' - ', '<br />'));
                if (event.icon) {
                    element.find('.fc-title').prepend(event.icon);
                }
            },
            eventClick: function(calEvent, jsEvent, view) {
                //console.log(calEvent);
                //console.log(view);
            }
        });

        // STATUS CLICK
        $('#statusLegend span a').click(function(e) {
            e.preventDefault();
            var status = $(this).attr('data-status');
            if (status == 'all') {
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('addEventSource', '<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/calendar/store_types/general/events/');
                $('#calendar').fullCalendar('rerenderEvents');
            } else {
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('addEventSource', '<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/calendar/store_types/general/events/?status=' +status);
                $('#calendar').fullCalendar('rerenderEvents');
            }
        });

    });
</script>

<table width="100%" style="border: 0px;">
    <tr>
        <td valign="top" style="padding: 0px; white-space: nowrap;">
            <div style="margin-right: 20px; padding-right: 20px; border-right: 1px solid #eee;">
                <div id="statusLegend">
                    <div style="margin-bottom: 6px;">
                        <span><a href="#" data-status="all" style="color: #222;">All</a></span>
                    </div>
                    <?php
                    foreach ($_uccms_ecomm->statusColors() as $type => $colors) {
                        foreach ($colors as $status => $color) {
                            $cola[$status] = $color;
                        }
                    }
                    foreach ($cola as $status => $color) {
                       ?>
                       <div style="margin-bottom: 5px;">
                            <span style="display: inline-block; padding: 4px 6px; background-color: <?php echo $color; ?>; line-height: 1em; border-radius: 2px;"><a href="#" data-status="<?php echo $status; ?>" style="color: #111;"><?php echo ucwords($status); ?></a></span>
                       </div>
                       <?php
                    }
                    ?>
                </div>
            </div>
        </td>
        <td valign="top" width="100%" style="padding: 0px;">
            <div id="calendar"></div>
        </td>
    </tr>
</table>

<div style="padding-top: 10px;">
    <fieldset>
        <label>iCal URL</label>
        <input type="text" value="<?php echo WWW_ROOT . $_uccms_ecomm->storePath(); ?>/ical/admin/?key=<?php echo md5($bigtree['config']['db']['name']); ?>&nocache=true" />
    </fieldset>
</div>