<?php

$bigtree['breadcrumb'][] = [ 'title' => 'Reports', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

$has_custom = false;

// CUSTOM
$custom_file = __DIR__. '/custom/default.php';

if (file_exists($custom_file)) {
    $has_custom = true;
}

?>

<style type="text/css">

#reports .filter {
    position: relative;
    margin-bottom: 30px;
}
#reports .filter .datepicker {
    float: left;
    margin-right: 15px;
}
#reports .filter .submit {
    float: left;
}
#reports .filter .date_picker_icon {
    margin: 8px 0 0 -24px;
}

#reports .reports .report {
    float: left;
    width: 30%;
    margin: 0 15px 15px 0;
    padding-bottom: 12px;
    border: 1px solid #e0e0e0;
    background-color: #f5f5f5;
    border-radius: 5px;
}
#reports .reports .report h2 {
    margin: 0px;
    padding: 10px 15px;
    font-size: 1.2em;
    line-height: 1em;
    font-weight: bold;
    text-transform: uppercase;
    opacity: .8;
}
#reports .reports .report .contain {
    position: relative;
}
#reports .reports .report .value {
    float: left;
    margin-top: 8px;
    padding: 0 0 0 15px;
}
#reports .reports .report .num {
    vertical-align: sub;
    font-size: 2.4em;
    line-height: 36px;
    opacity: .6;
}
#reports .reports .report .label {
    display: inline-block;
    padding-left: 5px;
    font-size: 1em;
    line-height: 1em;
    opacity: .6;
}
#reports .reports .report .value .select {
    margin-top: 4px;
}
#reports .reports .report .action {
    float: right;
    margin: 8px 0 0;
    padding: 0 15px 0 0;
}
#reports .reports .report .action a.button {
    line-height: 34px;
}
#reports .reports .report .action .select {
    margin-top: 4px;
}
#reports .reports .report .action .select > span {
    max-width: 100px;
}

</style>

<script type="text/javascript">

</script>

<div id="reports">

    <h3 class="breadcrumbs">Reports</h3>

    <div class="container tabbed-header legacy">

        <header>
            <div class="sticky_controls">
                <div class="shadow">
                    <nav class="left">
                        <a href="#tab-general" data-tab="general" class="active">General</a>
                        <?php if ($has_custom) { ?>
                            <a href="#" data-tab="custom">Custom</a>
                        <?php } ?>
                    </nav>
                </div>
            </div>
        </header>

        <section class="tab tab-general active">
            <a name="tab-general"></a>

            <?php include(__DIR__. '/general/tab.php'); ?>

        </section>

        <?php if ($has_custom) { ?>
            <section class="tab tab-custom">
                <a name="tab-custom"></a>

                <?php include(__DIR__. '/custom/tab.php'); ?>

            </section>
        <?php } ?>

    </div>

</div>