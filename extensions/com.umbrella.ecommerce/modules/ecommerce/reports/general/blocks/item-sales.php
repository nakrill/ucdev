<script type="text/javascript">

    $(document).ready(function() {
        $('#reports .report.item-sales .action select').change(function() {
            if ($(this).val()) {
                $(this).closest('form').submit();
            }
        });
    });

</script>

<?php

// SELECT ITEMS ARRAY
$sia = array();

// BOOKING
if ($_uccms_ecomm->storeType() == 'booking') {

    $ia = array();

    // GET BOOKINGS
    $bookings = $_uccms_ecomm->stc->getBookings(strtotime($date_start), strtotime($date_end), array());

    // LOOP
    foreach ($bookings as $booking) {
        $ia[$booking['item_id']] = $booking['item_id'];
    }

    // LOOP
    foreach ($ia as $item_id) {

        // GET ITEM
        $item_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE `id`=" .$item_id. "";
        $item_q = sqlquery($item_query);
        $item = sqlfetch($item_q);

        // ITEM FOUND
        if ($item['id']) {
            $sia[$item['id']] = $item;
        }

    }

} else {

    unset($wa);

    // WHERE ARRAY
    $wa[] = "i.deleted=0";
    $wa[] = "o.status='charged'";

    /*
    // DATE - START
    if ($_REQUEST['date_start']) {
        $wa[] = "o.dt>='" .date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start'])). "'";
    }

    // DATE - END
    if ($_REQUEST['date_end']) {
        $wa[] = "o.dt<='" .date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end'])). "'";
    }
    */

    $wa[] = "o.dt>='" .$date_start. "'";
    $wa[] = "o.dt<='" .$date_end. "'";

    // PUT WHERE TOGETHER
    $where_sql = "(" .implode(") AND (", $wa). ")";

    // GET ITEMS
    $items_query = "
    SELECT i.id, i.title
    FROM `" .$_uccms_ecomm->tables['order_items']. "` AS `oi`
    INNER JOIN `" .$_uccms_ecomm->tables['items']. "` AS `i` ON oi.item_id=i.id
    INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON oi.cart_id=o.id
    WHERE " .$where_sql. "
    GROUP BY i.id
    ORDER BY i.title, i.id ASC
    ";
    $items_q = sqlquery($items_query);
    while ($item = sqlfetch($items_q)) {
        $sia[$item['id']] = $item;
    }

}

$num_items = count($sia);

?>

<div class="report item-sales">
    <form action="./general/item/sales/" method="get">
    <input type="hidden" name="date_start" value="<?php echo $date_start; ?>" />
    <input type="hidden" name="date_end" value="<?php echo $date_end; ?>" />
    <h2>Sales by Item</h2>
    <div class="contain">
        <div class="value">
            <span class="num"><?php echo number_format($num_items, 0); ?></span><span class="label">Items sold in timeframe</span>
        </div>
        <?php if ($num_items > 0) { ?>
            <div class="action">
                <select name="item_id" class="custom_control form-control">
                    <option value="">Select</option>
                    <?php foreach ($sia as $item) { ?>
                        <option value="<?php echo $item['id']; ?>"><?php echo stripslashes($item['title']); ?></option>
                    <?php } ?>
                </select>
            </div>
        <?php } ?>
    </div>
    </form>
</div>