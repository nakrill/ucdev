<style type="text/css">

    #report .filter {
        position: relative;
        margin-bottom: 20px;
        padding: 20px 20px 5px;
        background-color: #f5f5f5;
    }
    #report .filter .left {
        float: left;
        width: auto;
        margin-right: 30px;
    }
    #report .filter .submit {
        position: absolute;
        bottom: 20px;
        right: 20px;
    }
    #report .filter .date_picker_icon {
        margin: 8px 0 0 -24px;
    }

    #report .stats_text {
        margin-bottom: 20px;
        padding: 15px 15px 5px;
        background-color: #f5f5f5;
        font-size: 1.2em;
        line-height: 1em;
        color: #888;
    }
    #report .stats_text .important {
        display: inline-block;
        margin-bottom: 10px;
        padding: 6px 8px;
        border: 2px solid #eee;
        background-color: #fff;
        color: #555;
    }

    #report .stats {
        display: none;
        margin-bottom: 20px;
    }
    #report .stats .left {
        float: left;
        width: 50%;
    }
    #report .stats .right {
        float: left;
        width: 50%;
    }
    #report .stats .item {
        margin-bottom: 15px;
    }
    #report .stats .item:last-child {
        margin-bottom: 0px;
    }
    #report .stats .item .label {
        opacity: .7;
    }
    #report .stats .item .value {
        font-size: 1.2em;
    }

    #report .results .result_order-id {
        width: 60px;
        text-align: left;
    }
    #report .results .result_dt {
        width: 180px;
        text-align: left;
    }
    #report .results .result_customer {
        width: 210px;
        text-align: left;
    }
    #report .results .result_this-item {
        width: 90px;
        text-align: left;
    }
    #report .results .result_item-amt {
        width: 130px;
        text-align: left;
    }
    #report .results .result_total-items {
        width: 90px;
        text-align: left;
    }
    #report .results .result_order-total {
        width: 130px;
        text-align: left;
    }
    #report .results .result_view {
        width: 50px;
        text-align: left;
    }

    #report .no_results {
        padding: 20px;
        text-align: center;
    }

    @media print {

        #report .filter, #report .stats_text {
            margin-bottom: 15px;
            padding: 0px;
        }

    }

</style>

<?php

// SEARCHING
if ($_REQUEST['item_id']) {

    // GET ITEM
    $this_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .(int)$_REQUEST['item_id']. ")";
    $this_item_q = sqlquery($this_item_query);
    $this_item = sqlfetch($this_item_q);

    // ITEM FOUND
    if ($this_item['id']) {

        // IS SEARCHING
        $searching = true;

        // $_GET VALUES
        $query_vars = array();
        if (is_array($_GET)) {
            $qv_ignore = array('bigtree_htaccess_url');
            $ga = array();
            foreach ($_GET as $key => $val) {
                if (!in_array($key, $qv_ignore)) {
                    $query_vars[$key] = $val;
                }
            }
        }

        // TOTALS
        $totals = array();

        // RESULTS ARRAY
        $ra = array();

        // BOOKING
        if ($_uccms_ecomm->storeType() == 'booking') {

            $start  = date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start']));
            $end    = date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end']));

            $bookings_vars = array(
                'item_id'   => $this_item['id']
            );

            // GET BOOKINGS
            $bookings = $_uccms_ecomm->stc->getBookings(strtotime($start), strtotime($end), $bookings_vars);

            //echo print_r($bookings);

            // LOOP
            foreach ($bookings as $booking) {

                // GET ORDER
                $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$booking['order_id']. ")";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                // ORDER FOUND
                if ($order['id']) {

                    $item_num = 0;
                    $item_total = 0;

                    // ADD ORDER TOTAL TO TOTALS
                    $totals['order']['total'] += $order['order_total'];

                    // GET ALL ORDER ITEMS
                    $oitems = $_uccms_ecomm->cartItems($order['id']);

                    // ADD TOTAL ORDER ITEMS TO TOTALS
                    $totals['order']['items'] += count($oitems);

                    // LOOP THROUGH ITEMS
                    foreach ($oitems as $oitem) {

                        // IS ITEM
                        if ($oitem['item_id'] == $_REQUEST['item_id']) {

                            // GET ITEM INFO
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                            // ADD TO THIS ITEM NUMBER
                            $item_num += $oitem['quantity'];

                            // ADD THIS ITEM NUMBER TO TOTALS
                            $totals['item']['num'] += $oitem['quantity'];

                            // ADD TO THIS ITEM TOTAL
                            $item_total += str_replace(',', '', $item['other']['total']);

                            // ADD THIS ITEM TOTAL TO TOTALS
                            $totals['item']['total'] += str_replace(',', '', $item['other']['total']);

                        }

                    }

                    $ra[$booking['order_id']] = array(
                        'order'             => $order,
                        'date'              => date('n/j/Y g:i A T', strtotime($booking['dt_from'])),
                        'item_num'          => $item_num,
                        'item_total'        => $item_total,
                        'total_items_num'   => count($oitems),
                    );

                }

            }

            //echo print_r($ra);

        } else {

            // WHERE ARRAY
            $wa = array();

            $wa[] = "oi.item_id=" .$this_item['id'];
            $wa[] = "(o.status='charged') OR (o.status='processing') OR (o.status='shipped') OR (o.status='complete')";

            // DATE - START
            if ($_REQUEST['date_start']) {
                $wa[] = "o.dt>='" .date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start'])). "'";
            }

            // DATE - END
            if ($_REQUEST['date_end']) {
                $wa[] = "o.dt<='" .date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end'])). "'";
            }

            // PUT WHERE TOGETHER
            $where_sql = "(" .implode(") AND (", $wa). ")";

            // FIND MATCHING ORDERS
            $orders_query = "
            SELECT o.*
            FROM `" .$_uccms_ecomm->tables['order_items']. "` AS `oi`
            INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON oi.cart_id=o.id
            WHERE " .$where_sql. "
            GROUP BY o.id
            ";
            $orders_q = sqlquery($orders_query);
            while ($order = sqlfetch($orders_q)) {

                // INCLUDE ORDER INFO
                $ra[$order['id']]['order'] = $order;

                $ra[$order['id']]['date'] = date('n/j/Y g:i A T', strtotime($order['dt']));

                // ADD ORDER TOTAL TO TOTALS
                $totals['order']['total'] += $order['order_total'];

                // GET ALL ORDER ITEMS
                $oitems = $_uccms_ecomm->cartItems($order['id']);

                // TOTAL NUMBER OF ORDER ITEMS
                $ra[$order['id']]['total_items_num'] = count($oitems);

                // ADD TOTAL ORDER ITEMS TO TOTALS
                $totals['order']['items'] += count($oitems);

                // NUMBER OF THIS ITEM
                $ra[$order['id']]['item_num'] = 0;

                // TOTAL FOR THIS ITEM
                $ra[$order['id']]['item_total'] = 0;

                // LOOP THROUGH ITEMS
                foreach ($oitems as $oitem) {

                    // IS ITEM
                    if ($oitem['item_id'] == $_REQUEST['item_id']) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // ADD TO THIS ITEM NUMBER
                        $ra[$order['id']]['item_num'] += $oitem['quantity'];

                        // ADD THIS ITEM NUMBER TO TOTALS
                        $totals['item']['num'] += $oitem['quantity'];

                        // ADD TO THIS ITEM TOTAL
                        $ra[$order['id']]['item_total'] += str_replace(',', '', $item['other']['total']);

                        // ADD THIS ITEM TOTAL TO TOTALS
                        $totals['item']['total'] += str_replace(',', '', $item['other']['total']);

                    }

                }

            }

        }

    }

}

?>

<div id="report">

    <div class="report_heading clearfix">
        <h3 class="breadcrumbs"><a href="../../../">Reports</a> > <a href="../../../#tab-general">General</a> > Item Sales</h3>
        <a href="#" class="print button print-hidden"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
    </div>

    <div class="filter contain">
        <form action="" method="get">

        <fieldset class="left">
            <label>Item</label>
            <select name="item_id">
                <?php
                $item_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0) ORDER BY `title`, `id` ASC";
                $item_q = sqlquery($item_query);
                while ($item = sqlfetch($item_q)) {
                    ?>
                    <option value="<?php echo $item['id']; ?>" <?php if ($item['id'] == $_REQUEST['item_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($item['title']); ?></option>
                    <?php
                }
                ?>
            </select>
        </fieldset>

        <div class="datepicker left">
            <label>Date Range</label>
            <div class="contain">
                <div style="float: left;">
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'date_start',
                        'key'       => 'date_start',
                        'value'     => $_REQUEST['date_start'],
                        'required'  => false,
                        'options'   => array(
                            'default_today' => false
                        )
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                    ?>
                </div>
                <div style="float: left; padding: 0 10px; line-height: 32px;">to</div>
                <div style="float: left;">
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'date_end',
                        'key'       => 'date_end',
                        'value'     => $_REQUEST['date_end'],
                        'required'  => false,
                        'options'   => array(
                            'default_today' => true
                        )
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                    ?>
                </div>
            </div>
        </div>

        <div class="submit">
            <input type="submit" value="Go" class="print-hidden" />
        </div>

        </form>
    </div>

    <?php if ($searching) { ?>

        <div class="stats_text">

            <span class="important"><?php echo number_format($totals['item']['num'], 0); ?> <?php echo stripslashes($this_item['title']); ?><?php if (($totals['item']['num'] != 1) && (substr($this_item['title'], -1, 1) != 's')) { echo 's'; } ?></span> <?php if ($totals['item']['num'] == 1) { echo 'was'; } else { echo 'were'; } ?> sold for <span class="important">$<?php echo number_format($totals['item']['total'], 2); ?> in revenue</span> through <span class="important"><?php echo number_format(count($ra), 0); ?> orders</span> with <span class="important"><?php echo number_format($totals['order']['items'], 0); ?> total items</span> that generated <span class="important">$<?php echo number_format($totals['order']['total'], 2); ?> total revenue</span>

        </div>

        <div class="stats contain">

            <div class="left">

                <div class="item">
                    <div class="label">This Item Sold</div>
                    <div class="important"><?php echo number_format($totals['item']['num'], 0); ?></div>
                </div>

                <div class="item">
                    <div class="label">This Item Sales</div>
                    <div class="important">$<?php echo number_format($totals['item']['total'], 2); ?></div>
                </div>

            </div>

            <div class="right">

                <div class="item">
                    <div class="label">Total Orders</div>
                    <div class="important"><?php echo number_format(count($ra), 0); ?></div>
                </div>

                <div class="item">
                    <div class="label">Total Items</div>
                    <div class="important"><?php echo number_format($totals['order']['items'], 0); ?></div>
                </div>

                <div class="item">
                    <div class="label">Total Sales</div>
                    <div class="important">$<?php echo number_format($totals['order']['total'], 2); ?></div>
                </div>

            </div>

        </div>

    <?php } ?>

    <div class="results table">

        <summary>
            <h2><?php if ($searching) { echo number_format(count($ra), 0). ' '; } ?>Result<?php if (count($ra) != 1) { echo 's'; } ?></h2>
            <?php if ($searching) { ?><a href="../sales-export/?<?php echo http_build_query($query_vars); ?>" class="more print-hidden">Export</a><?php } ?>
        </summary>

        <header style="clear: both;">
            <span class="result_order-id">Order ID</span>
            <span class="result_dt">Date</span>
            <span class="result_customer">Customer</span>
            <span class="result_this-item">This Item</span>
            <span class="result_item-amt">Item Total</span>
            <span class="result_total-items">Total Items</span>
            <span class="result_order-total">Order Total</span>
            <span class="result_view">View</span>
        </header>

        <?php if (count($ra) > 0) { ?>

            <ul class="items">

                <?php foreach ($ra as $result) { ?>

                    <li class="item contain">
                        <section class="result_order-id"><?php echo $result['order']['id']; ?></section>
                        <section class="result_dt"><?php echo $result['date']; ?></section>
                        <section class="result_customer"><?php echo trim(stripslashes($result['order']['billing_firstname']. ' ' .$result['order']['billing_lastname'])); ?></section>
                        <section class="result_this-item"><?php echo number_format($result['item_num'], 0); ?></section>
                        <section class="result_item-amt">$<?php echo number_format($result['item_total'], 2); ?></section>
                        <section class="result_total-items"><?php echo number_format($result['total_items_num'], 0); ?></section>
                        <section class="result_order-total">$<?php echo number_format($result['order']['order_total'], 2); ?></section>
                        <section class="result_view"><a class="icon_edit" title="View / Edit" href="../../../../orders/edit/?id=<?php echo $result['order']['id']; ?>" target="_blank"></a></section>
                    </li>

                <?php } ?>

            </ul>

        <?php } else { ?>

            <div class="no_results">
                <?php if ($_REQUEST['item_id']) { ?>
                    No results found.
                <?php } else { ?>
                    Please select an item.
                <?php } ?>
            </div>

        <?php } ?>

    </div>

</div>