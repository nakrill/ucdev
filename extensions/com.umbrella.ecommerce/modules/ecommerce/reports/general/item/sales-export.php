<?php

// CLEAR OUTPUT BUFFER
ob_clean();

// CLEAN UP
$item_id = (int)$_REQUEST['item_id'];

// ITEM ID SPECIFIED
if ($item_id) {

    // GET ITEM INFO
    $this_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item_id. ")";
    $this_item_q = sqlquery($this_item_query);
    $this_item = sqlfetch($this_item_q);

    // ITEM FOUND
    if ($this_item['id']) {

        $ri = 0;

        // TOTALS
        $totals = array();

        // RESULTS ARRAY
        $ra = array();

        // BOOKING
        if ($_uccms_ecomm->storeType() == 'booking') {

            $start  = date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start']));
            $end    = date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end']));

            $bookings_vars = array(
                'item_id'   => $this_item['id']
            );

            // GET BOOKINGS
            $bookings = $_uccms_ecomm->stc->getBookings(strtotime($start), strtotime($end), $bookings_vars);

            //echo print_r($bookings);

            // LOOP
            foreach ($bookings as $booking) {

                // GET ORDER
                $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$booking['order_id']. ")";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                // ORDER FOUND
                if ($order['id']) {

                    $item_num = 0;
                    $item_total = 0;

                    // ADD ORDER TOTAL TO TOTALS
                    $totals['order']['total'] += $order['order_total'];

                    // GET ALL ORDER ITEMS
                    $oitems = $_uccms_ecomm->cartItems($order['id']);

                    // ADD TOTAL ORDER ITEMS TO TOTALS
                    $totals['order']['items'] += count($oitems);

                    // LOOP THROUGH ITEMS
                    foreach ($oitems as $oitem) {

                        // IS ITEM
                        if ($oitem['item_id'] == $_REQUEST['item_id']) {

                            // GET ITEM INFO
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                            // ADD TO THIS ITEM NUMBER
                            $item_num += $oitem['quantity'];

                            // ADD THIS ITEM NUMBER TO TOTALS
                            $totals['item']['num'] += $oitem['quantity'];

                            // ADD TO THIS ITEM TOTAL
                            $item_total += str_replace(',', '', $item['other']['total']);

                            // ADD THIS ITEM TOTAL TO TOTALS
                            $totals['item']['total'] += str_replace(',', '', $item['other']['total']);

                        }

                    }

                    $ra[$booking['order_id']] = array(
                        'order'             => $order,
                        'date'              => date('n/j/Y g:i A T', strtotime($booking['dt_from'])). ' - ' .date('n/j/Y g:i A T', strtotime($booking['dt_to'])),
                        'item_num'          => $item_num,
                        'item_total'        => $item_total,
                        'total_items_num'   => count($oitems),
                    );

                }

            }

            //echo print_r($ra);

        } else {

            // WHERE ARRAY
            $wa = array();

            $wa[] = "oi.item_id=" .(int)$_REQUEST['item_id'];
            $wa[] = "(o.status='charged') OR (o.status='processing') OR (o.status='shipped') OR (o.status='complete')";

            // DATE - START
            if ($_REQUEST['date_start']) {
                $wa[] = "o.dt>='" .date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start'])). "'";
            }

            // DATE - END
            if ($_REQUEST['date_end']) {
                $wa[] = "o.dt<='" .date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end'])). "'";
            }

            // PUT WHERE TOGETHER
            $where_sql = "(" .implode(") AND (", $wa). ")";

            // FIND MATCHING ORDERS
            $orders_query = "
            SELECT o.*
            FROM `" .$_uccms_ecomm->tables['order_items']. "` AS `oi`
            INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON oi.cart_id=o.id
            WHERE " .$where_sql. "
            GROUP BY o.id
            ";
            $orders_q = sqlquery($orders_query);
            while ($order = sqlfetch($orders_q)) {

                // INCLUDE ORDER INFO
                $ra[$order['id']]['order'] = $order;

                $ra[$order['id']]['date'] = date('n/j/Y g:i A T', strtotime($order['dt']));

                // ADD ORDER TOTAL TO TOTALS
                $totals['order']['total'] += $order['order_total'];

                // GET ALL ORDER ITEMS
                $oitems = $_uccms_ecomm->cartItems($order['id']);

                // TOTAL NUMBER OF ORDER ITEMS
                $ra[$order['id']]['total_items_num'] = count($oitems);

                // ADD TOTAL ORDER ITEMS TO TOTALS
                $totals['order']['items'] += count($oitems);

                // NUMBER OF THIS ITEM
                $ra[$order['id']]['item_num'] = 0;

                // TOTAL FOR THIS ITEM
                $ra[$order['id']]['item_total'] = 0;

                // TEMP ORDER ITEM STRING ARRAY
                $toisa = array();

                // LOOP THROUGH ITEMS
                foreach ($oitems as $oitem) {

                    // GET ITEM INFO
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                    /*
                    if ($_SERVER['REMOTE_ADDR'] == '68.7.113.116') {
                        echo print_r($oitem);
                        echo print_r($item);
                        exit;
                    }
                    */

                    // PUT OPTIONS INTO STRING
                    unset($options);
                    if (is_array($item['other']['options'])) {
                        $osa = array();
                        foreach ($item['other']['options'] as $attr_id => $opt) {
                            if (is_array($opt)) {
                                foreach ($opt as $opt2) {
                                    if ((is_numeric($opt2)) && ($item['other']['attributes'][$attr_id]['options'][$opt2]['title'])) {
                                        $otitle = $item['other']['attributes'][$attr_id]['title']. ' - ' .$item['other']['attributes'][$attr_id]['options'][$opt2]['title'];
                                        if ($item['other']['attributes'][$attr_id]['options'][$opt2]['markup']) {
                                            $otitle .= ' (+$' .$item['other']['attributes'][$attr_id]['options'][$opt2]['markup']. ')';
                                        }
                                        $osa[] = $otitle;
                                    } else {
                                        $osa[] = $item['other']['attributes'][$attr_id]['title']. ' - ' .$opt2;
                                    }
                                }
                            } else {
                                $osa[] = $item['other']['attributes'][$attr_id]['title']. ' - ' .$opt;
                            }
                        }
                        $options = ' Options: ' .implode(', ', $osa). ';';
                        unset($osa);
                    }

                    // TEMP ORDER ITEM STRING ARRAY
                    $toisa[] = stripslashes($item['title']). '; Qty: ' .$oitem['quantity']. ';' .$options. ' Total: $' .number_format($item['other']['total'], 2);

                    // IS ITEM
                    if ($oitem['item_id'] == $_REQUEST['item_id']) {

                        // ADD TO THIS ITEM NUMBER
                        $ra[$order['id']]['item_num'] += $oitem['quantity'];

                        // ADD THIS ITEM NUMBER TO TOTALS
                        $totals['item']['num'] += $oitem['quantity'];

                        // ADD TO THIS ITEM TOTAL
                        $ra[$order['id']]['item_total'] += str_replace(',', '', $item['other']['total']);

                        // ADD THIS ITEM TOTAL TO TOTALS
                        $totals['item']['total'] += str_replace(',', '', $item['other']['total']);

                    }

                }

                // ITEMS STRING
                $ra[$order['id']]['items_string'] = implode(' | ', $toisa);

            }

        }

        /*
        $row[$ri][] = 'Sales by Item: ' .stripslashes($item['title']);
        $ri++;

        if (($_REQUEST['date_start']) || ($_REQUEST['date_end'])) {
            $row[$ri][] = 'Date: ' .$_REQUEST['date_start']. ' - ' .$_REQUEST['date_end'];
            $ri++;
        }

        $row[$ri][] = '';
        $ri++;

        */

        // HEADER ROW
        $row[$ri] = array(
            'Order ID',
            'Date & Time',
            'Status',
            'Billing Firstname',
            'Billing Lastname',
            'Billing Company',
            'Billing Address',
            'Billing Address 2',
            'Billing City',
            'Billing State',
            'Billing Zip',
            'Billing Phone',
            'Billing Email',
            'Shipping Same',
            'Shipping Firstname',
            'Shipping Lastname',
            'Shipping Company',
            'Shipping Address',
            'Shipping Address 2',
            'Shipping City',
            'Shipping State',
            'Shipping Zip',
            'Shipping Phone',
            'Shipping Email',
            'Shipping Carrier',
            'Shipping Service',
            'Override Tax',
            'Override Shipping',
            'Override Gratuity',
            'Order Subtotal',
            'Order Discount',
            'Order Tax',
            'Order Shipping',
            'Order Gratuity',
            'Order Total',
            'Notes',
            'This Item Sold',
            'This Item Sales',
            'Total Items',
            'Order Total',
            'Items',
            'Order URL'
        );
        $ri++;

        // HAVE RESULTS
        if (count($ra) > 0) {

            // LOOP
            foreach ($ra as $result) {

                $items = '';

                // ROW
                $row[$ri] = array(
                    $result['order']['id'],
                    $result['date'],
                    ucwords($result['order']['status']),
                    stripslashes($result['order']['billing_firstname']),
                    stripslashes($result['order']['billing_lastname']),
                    stripslashes($result['order']['billing_company']),
                    stripslashes($result['order']['billing_address1']),
                    stripslashes($result['order']['billing_address2']),
                    stripslashes($result['order']['billing_city']),
                    stripslashes($result['order']['billing_state']),
                    stripslashes($result['order']['billing_zip']),
                    stripslashes($result['order']['billing_phone']),
                    stripslashes($result['order']['billing_email']),
                    stripslashes($result['order']['shipping_same']),
                    stripslashes($result['order']['shipping_firstname']),
                    stripslashes($result['order']['shipping_lastname']),
                    stripslashes($result['order']['shipping_company']),
                    stripslashes($result['order']['shipping_address1']),
                    stripslashes($result['order']['shipping_address2']),
                    stripslashes($result['order']['shipping_city']),
                    stripslashes($result['order']['shipping_state']),
                    stripslashes($result['order']['shipping_zip']),
                    stripslashes($result['order']['shipping_phone']),
                    stripslashes($result['order']['shipping_email']),
                    stripslashes($result['order']['shipping_carrier']),
                    stripslashes($result['order']['shipping_service']),
                    ($result['order']['override_tax'] ? '$' .$result['order']['override_tax'] : ''),
                    ($result['order']['override_shipping'] ? '$' .$result['order']['override_shipping'] : ''),
                    ($result['order']['override_gratuity'] ? '$' .$result['order']['override_gratuity'] : ''),
                    '$' .$result['order']['order_subtotal'],
                    '$' .$result['order']['order_discount'],
                    '$' .$result['order']['order_tax'],
                    '$' .$result['order']['order_shipping'],
                    '$' .$result['order']['order_gratuity'],
                    '$' .$result['order']['order_total'],
                    stripslashes($result['order']['notes']),
                    number_format($result['item_num'], 0),
                    '$' .number_format($result['item_total'], 2),
                    number_format($result['total_items_num'], 0),
                    '$' .number_format($result['order']['order_total'], 2),
                    $result['items_string'],
                    ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$result['order']['id']
                );

                $ri++;

            }

        }

        // CONVERT TO CSV
        $csv = array_to_csv($row);

        // FILE NAME
        $filename = uccms_makeRewrite(stripslashes($this_item['title']));

        if ($_REQUEST['date_start']) {
            $filename .= '_' .date('Y-m-d', strtotime($_REQUEST['date_start']));
        }
        if ($_REQUEST['date_end']) {
            $filename .= '_' .date('Y-m-d', strtotime($_REQUEST['date_end']));
        }

        // HEADERS
        header("Content-type: text/csv");
        header('Content-Disposition: attachment; filename=' .$filename. '.csv');

        // OUTPUT
        echo $csv;
        exit;

    // ITEM NOT FOUND
    } else {
        echo 'Item not found.';
    }

// ICON FILE NOT SPECIFIED
} else {
    echo 'Item ID not specified.';
}

?>