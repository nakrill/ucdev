<?php

$bigtree['breadcrumb'][] = [ 'title' => 'Reports', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => 'General', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2]. '/' .$bigtree['path'][3] ];
$bigtree['breadcrumb'][] = [ 'title' => 'Status Flow', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2]. '/' .$bigtree['path'][3]. '/' .$bigtree['path'][4] ];

?>

<style type="text/css">

    #report .report_heading {
        margin-bottom: 30px;
    }
    #report .filter label {
        display: none;
    }
    #report .filter .left {
        width: auto;
        margin: 0 15px 0 0;
    }
    #report .filter .submit {

    }
    #report .filter .date_picker_icon {
        margin: 8px 0 0 -24px;
    }

    #report .stats_text {
        margin-bottom: 20px;
        padding: 15px 15px 5px;
        background-color: #f5f5f5;
        font-size: 1.2em;
        line-height: 1em;
        color: #888;
    }
    #report .stats_text .important {
        display: inline-block;
        margin-bottom: 10px;
        padding: 6px 8px;
        border: 2px solid #eee;
        background-color: #fff;
        color: #555;
    }

    /*
    #report .stats {
        display: none;
        margin-bottom: 20px;
    }
    #report .stats .left {
        float: left;
        width: 50%;
    }
    #report .stats .right {
        float: left;
        width: 50%;
    }
    #report .stats .item {
        margin-bottom: 15px;
    }
    #report .stats .item:last-child {
        margin-bottom: 0px;
    }
    #report .stats .item .label {
        opacity: .7;
    }
    #report .stats .item .value {
        font-size: 1.2em;
    }
    */

    #report .results {
        margin: 0 -10px 15px;
        overflow-x: scroll;
    }
    #report .results .column {
        width: 220px;
        float: left;
        margin: 0 10px;
    }
    #report .results .column .padding {
        background-color: #f5f5f5;
    }
    #report .results .column h1 {
        margin: 0px;
        padding: 10px;
        background-color: #ddd;
        text-align: center;
        font-size: 22px;
        font-weight: bold;
    }
    #report .results .column .stats {
        background-color: #e9e9e9;
        padding: 10px;
        text-align: center;
        font-size: 1.1em;
    }
    #report .results .column .stats .stat {
        margin-top: 5px;
    }
    #report .results .column .stats .stat:first-child {
        margin-top: 0px;
    }
    #report .results .column .stats .stat .value {
        font-weight: bold;
        opacity: .9;
    }
    #report .results .column .orders {
        margin-top: 15px;
        padding: 0 10px 10px;
    }

    #report .results .column .orders .order {
        margin-top: 10px;
        padding: 10px;
        background-color: #fff;
    }
    #report .results .column .orders .order .name {
        float: left;
    }
    #report .results .column .orders .order .order_id {
        float: right;
    }
    #report .results .column .orders .order .num_items {
        clear: both;
        float: left;
        margin-top: 5px;
    }
    #report .results .column .orders .order .total {
        float: right;
        margin-top: 5px;
    }
    #report .results .column .orders .order .days_old {
        clear: both;
        float: left;
        margin-top: 5px;
    }

    #report .no_results {
        padding: 20px;
        text-align: center;
    }

    @media print {

        #report .filter, #report .stats_text {
            margin-bottom: 15px;
            padding: 0px;
        }

    }

</style>

<?php

// STORE IN COOKIE? / WEB STORAGE
$columns = array('requested', 'preparing', 'presented', 'charged');

// TOTALS
$totals = array();

// RESULTS ARRAY
$ra = array();

// HAVE COLUMNS
if (count($columns) > 0) {

    // BOOKING
    if ($_uccms_ecomm->storeType() == 'booking') {

        echo '<b>Bookings not supported yet.</b><br /><br />';

        /*
        $start  = date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start']));
        $end    = date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end']));

        $bookings_vars = array(
            'item_id'   => $this_item['id']
        );

        // GET BOOKINGS
        $bookings = $_uccms_ecomm->stc->getBookings(strtotime($start), strtotime($end), $bookings_vars);

        //echo print_r($bookings);

        // LOOP
        foreach ($bookings as $booking) {

            // GET ORDER
            $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$booking['order_id']. ")";
            $order_q = sqlquery($order_query);
            $order = sqlfetch($order_q);

            // ORDER FOUND
            if ($order['id']) {

                $item_num = 0;
                $item_total = 0;

                // ADD ORDER TOTAL TO TOTALS
                $totals['order']['total'] += $order['order_total'];

                // GET ALL ORDER ITEMS
                $oitems = $_uccms_ecomm->cartItems($order['id']);

                // ADD TOTAL ORDER ITEMS TO TOTALS
                $totals['order']['items'] += count($oitems);

                // LOOP THROUGH ITEMS
                foreach ($oitems as $oitem) {

                    // IS ITEM
                    if ($oitem['item_id'] == $_REQUEST['item_id']) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // ADD TO THIS ITEM NUMBER
                        $item_num += $oitem['quantity'];

                        // ADD THIS ITEM NUMBER TO TOTALS
                        $totals['item']['num'] += $oitem['quantity'];

                        // ADD TO THIS ITEM TOTAL
                        $item_total += str_replace(',', '', $item['other']['total']);

                        // ADD THIS ITEM TOTAL TO TOTALS
                        $totals['item']['total'] += str_replace(',', '', $item['other']['total']);

                    }

                }

                $ra[$booking['order_id']] = array(
                    'order'             => $order,
                    'date'              => date('n/j/Y g:i A T', strtotime($booking['dt_from'])),
                    'item_num'          => $item_num,
                    'item_total'        => $item_total,
                    'total_items_num'   => count($oitems),
                );

            }

        }

        //echo print_r($ra);
        */

    } else {

        // LOOP THROUGH STATUSES
        foreach ($columns as $status) {

            // WHERE ARRAY
            $wa = array();

            //$wa[] = "(o.status='charged') OR (o.status='complete')";

            // STATUS
            $wa[] = "(o.status='" .$status. "')";

            // BY ITEM
            if ($_REQUEST['item_id']) {
                $wa[] = "oi.item_id=" .(int)$_REQUEST['item_id'];
            }

            // DATE - START
            if ($_REQUEST['date_start']) {
                $wa[] = "o.dt>='" .date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start'])). "'";
            }

            // DATE - END
            if ($_REQUEST['date_end']) {
                $wa[] = "o.dt<='" .date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end'])). "'";
            }

            // PUT WHERE TOGETHER
            $where_sql = "(" .implode(") AND (", $wa). ")";

            // FIND MATCHING ORDERS
            $orders_query = "
            SELECT o.*
            FROM `" .$_uccms_ecomm->tables['orders']. "` AS `o`
            LEFT JOIN `" .$_uccms_ecomm->tables['order_items']. "` AS `oi` ON oi.cart_id=o.id
            WHERE " .$where_sql. "
            GROUP BY o.id
            ";
            /*
            $orders_query = "
            SELECT o.*
            FROM `" .$_uccms_ecomm->tables['order_items']. "` AS `oi`
            INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON oi.cart_id=o.id
            WHERE " .$where_sql. "
            GROUP BY o.id
            ";
            */
            $orders_q = sqlquery($orders_query);
            while ($order = sqlfetch($orders_q)) {

                // INCLUDE ORDER INFO
                $ra[$status][$order['id']]['order'] = $order;

                $ra[$status][$order['id']]['date'] = date('n/j/Y g:i A T', strtotime($order['dt']));

                // ADD ORDER TOTAL TO TOTALS
                $totals[$status]['order']['total'] += $order['order_total'];

                // GET ALL ORDER ITEMS
                $oitems = $_uccms_ecomm->cartItems($order['id']);

                // TOTAL NUMBER OF ORDER ITEMS
                $ra[$status][$order['id']]['total_items_num'] = count((array)$oitems);

                // ADD TOTAL ORDER ITEMS TO TOTALS
                $totals[$status]['order']['items'] += count((array)$oitems);

                // NUMBER OF THIS ITEM
                $ra[$status][$order['id']]['item_num'] = 0;

                // TOTAL FOR THIS ITEM
                $ra[$status][$order['id']]['item_total'] = 0;

                // LOOP THROUGH ITEMS
                foreach ($oitems as $oitem) {

                    // IS ITEM
                    if ($oitem['item_id'] == $_REQUEST['item_id']) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // ADD TO THIS ITEM NUMBER
                        $ra[$status][$order['id']]['item_num'] += $oitem['quantity'];

                        // ADD THIS ITEM NUMBER TO TOTALS
                        $totals[$status]['item']['num'] += $oitem['quantity'];

                        // ADD TO THIS ITEM TOTAL
                        $ra[$status][$order['id']]['item_total'] += str_replace(',', '', $item['other']['total']);

                        // ADD THIS ITEM TOTAL TO TOTALS
                        $totals[$status]['item']['total'] += str_replace(',', '', $item['other']['total']);

                    }

                }

            }

        }

    }

}

//echo print_r($ra);
//echo print_r($totals);

?>

<div id="report">

    <div class="report_heading row">

        <div class="col-md-10">

            <div class="filter clearfix">
                <form action="" method="get">

                <fieldset class="left">
                    <label>Item</label>
                    <select name="item_id" class="custom_control form-control">
                        <option value="">All</option>
                        <?php
                        $item_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0) ORDER BY `title`, `id` ASC";
                        $item_q = sqlquery($item_query);
                        while ($item = sqlfetch($item_q)) {
                            ?>
                            <option value="<?php echo $item['id']; ?>" <?php if ($item['id'] == $_REQUEST['item_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($item['title']); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </fieldset>

                <div class="datepicker left">
                    <label>Date Range</label>
                    <div class="contain">
                        <div style="float: left;">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'date_start',
                                'key'       => 'date_start',
                                'value'     => $_REQUEST['date_start'],
                                'required'  => false,
                                'options'   => array(
                                    'default_today' => false
                                )
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/date.php'));

                            ?>
                        </div>
                        <div style="float: left; padding: 0 10px; line-height: 32px;">to</div>
                        <div style="float: left;">
                            <?php

                            // FIELD VALUES
                            $field = array(
                                'id'        => 'date_end',
                                'key'       => 'date_end',
                                'value'     => $_REQUEST['date_end'],
                                'required'  => false,
                                'options'   => array(
                                    'default_today' => true
                                )
                            );

                            // INCLUDE FIELD
                            include(BigTree::path('admin/form-field-types/draw/date.php'));

                            ?>
                        </div>
                    </div>
                </div>

                <div class="submit left">
                    <input type="submit" value="Go" class="print-hidden btn btn-primary" />
                </div>

                </form>
            </div>

        </div>

        <div class="col-md-2">

            <a href="#" class="print button print-hidden"><i class="fa fa-print" aria-hidden="true"></i>Print</a>

        </div>

    </div>

    <div class="results clearfix">

        <?php foreach ($columns as $column) { ?>

            <div class="column">
                <div class="padding">

                    <h1><?php echo ucwords($column); ?></h1>

                    <div class="stats">
                        <div class="stat num_orders">
                            <span class="title"># Orders</span>
                            <span class="value"><?php echo number_format(count((array)$ra[$column]), 0); ?></span>
                        </div>
                        <div class="stat num_items">
                            <span class="title"># Items</span>
                            <span class="value"><?php echo number_format((int)$totals[$column]['order']['items'], 0); ?></span>
                        </div>
                        <div class="stat num_amount">
                            <span class="title">Amount</span>
                            <span class="value">$<?php echo number_format((float)$totals[$column]['order']['total'], 2); ?></span>
                        </div>
                    </div>

                    <div class="orders">

                        <?php

                        // HAVE ORDERS
                        if (count((array)$ra[$column]) > 0) {

                            // LOOP
                            foreach ($ra[$column] as $order) {

                                // CALCULATE DAYS OLD
                                $days_old = dateDifference($order['order']['dt'], date('Y-m-d H:i:s'), '%a');

                                ?>
                                <div class="order clearfix">
                                    <div class="name">
                                        <?php echo trim(stripslashes($order['order']['billing_firstname']. ' ' .$order['order']['billing_lastname'])); ?>
                                    </div>
                                    <div class="order_id">
                                        <a href="../../../<?php if ($order['order']['quote']) { echo 'quotes'; } else { echo 'orders'; } ?>/edit/?id=<?php echo $order['order']['id']; ?>" target="_blank">#<?php echo $order['order']['id']; ?></a>
                                    </div>
                                    <div class="num_items">
                                        <i class="fa fa-fw fa-shopping-basket" aria-hidden="true"></i> <?php echo number_format((int)$order['total_items_num'], 0); ?>
                                    </div>
                                    <div class="total">
                                        $<?php echo number_format($order['order']['order_total'], 2); ?>
                                    </div>
                                    <div class="days_old">
                                        <i class="fa fa-fw fa-clock-o"></i> <?php echo number_format((int)$days_old, 0); ?>d
                                    </div>
                                </div>
                                <?php

                            }

                        } else {
                            ?>
                            <div class="no_results">
                                No quotes / orders.
                            </div>
                            <?php
                        }

                        ?>

                    </div>

                 </div>
            </div>

        <?php } ?>

    </div>

</div>