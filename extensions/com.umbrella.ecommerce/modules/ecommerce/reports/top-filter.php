<?php

$date_start = date('Y-m-d 00:00:00', strtotime($_REQUEST['date_start'] ? $_REQUEST['date_start'] : '-6 Days'));
$date_end = date('Y-m-d 23:59:59', strtotime($_REQUEST['date_end'] ? $_REQUEST['date_end'] : 'Today'));

?>

<div class="filter contain">
    <form action="" method="get">

    <div class="datepicker">
        <div class="clearfix">
            <div style="float: left;">
                <?php

                // FIELD VALUES
                $field = array(
                    'id'        => 'date_start',
                    'key'       => 'date_start',
                    'value'     => $date_start,
                    'required'  => false,
                    'options'   => array(
                        'default_today' => false
                    )
                );

                // INCLUDE FIELD
                include(BigTree::path('admin/form-field-types/draw/date.php'));

                ?>
            </div>
            <div style="float: left; padding: 0 10px; line-height: 32px;">to</div>
            <div style="float: left;">
                <?php

                // FIELD VALUES
                $field = array(
                    'id'        => 'date_end',
                    'key'       => 'date_end',
                    'value'     => $date_end,
                    'required'  => false,
                    'options'   => array(
                        'default_today' => true
                    )
                );

                // INCLUDE FIELD
                include(BigTree::path('admin/form-field-types/draw/date.php'));

                ?>
            </div>
        </div>
    </div>

    <div class="submit">
        <input type="submit" class="btn btn-primary" value="Go" />
    </div>

    </form>
</div>