<?php

$bigtree['breadcrumb'][] = [ 'title' => 'Settings', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET SETTINGS
$settings = $_uccms_ecomm->getSettings();

?>

<style type="text/css">

    .general.container .service-fee .select {
        float: none;
        display: inline-block;
        width: auto;
        vertical-align: middle;
    }
    .general.container .service-fee label {
        display: block;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // GENERAL - STOREFRONT ENABLED TOGGLE
        $('#storefront_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#storefront_disabled_content').hide();
            } else {
                $('#storefront_disabled_content').show();
            }
        });

        // GENERAL - AUTOMATICALLY CREATE ACCOUNTS TOGGLE
        $('#accounts_auto_create').change(function(e) {
            if ($(this).prop('checked')) {
                $('.accounts_auto_create_toggle').show();
            } else {
                $('.accounts_auto_create_toggle').hide();
            }
        });

        // CHECKOUT - NOTES TOGGLE
        $('#checkout_legal_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_checkout_legal').show();
            } else {
                $('#toggle_checkout_legal').hide();
            }
        });

        // CHECKOUT - NOTES TOGGLE
        $('#checkout_notes').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_checkout_notes').show();
            } else {
                $('#toggle_checkout_notes').hide();
            }
        });

        // INVENTORY - TRACK TOGGLE
        $('#inventory_track').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_inventory_track').show();
            } else {
                $('#toggle_inventory_track').hide();
            }
        });


        // ITEM - REVIEW CRITERIA TOGGLE
        $('#item_ratings_reviews_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_item_reviews').show();
            } else {
                $('#toggle_item_reviews').hide();
            }
        });

        // ITEM - REVIEW CRITERIA - SORTABLE
        $('form .item_review-criteria table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // ITEM - REVIEW CRITERIA - ADD
        $('form .item_review-criteria .add').click(function(e) {
            e.preventDefault();
            $('#add_item_review-criteria').toggle();
        });

        // ITEM - REVIEW CRITERIA - REMOVE
        $('form .item_review-criteria .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="general container legacy">

    <header>
        <h2>General</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Store Name</label>
                    <input type="text" name="setting[store_name]" value="<?php echo stripslashes($settings['store_name']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Store Address</label>
                    <textarea name="setting[store_address]" style="height: 64px;"><?php echo stripslashes($settings['store_address']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Store Phone</label>
                    <input type="text" name="setting[store_phone]" value="<?php echo stripslashes($settings['store_phone']); ?>" />
                </fieldset>

                <fieldset>
                    <label style="display: block;">Hours of Operation</label>
                    <select name="setting[hours_of_operation-start]" class="custom_control">
                        <?php
                        for ($i=0; $i<=23; $i++) {
                            for ($j=0; $j<=45; $j+=15) {
                                if (date('H:i:s', strtotime($i. ':' .$j)) == $settings['hours_of_operation-start']) {
                                    $selected = true;
                                } else {
                                    $selected = false;
                                }
                                ?>
                                <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    &nbsp;to&nbsp;
                    <select name="setting[hours_of_operation-end]" class="custom_control">
                        <?php
                        for ($i=0; $i<=23; $i++) {
                            for ($j=0; $j<=45; $j+=15) {
                                if (date('H:i:s', strtotime($i. ':' .$j)) == $settings['hours_of_operation-end']) {
                                    $selected = true;
                                } else {
                                    $selected = false;
                                }
                                ?>
                                <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </fieldset>

            </div>

            <div class="right last">

                <?php if ($admin->Level == 2) { ?>
                    <fieldset>
                        <label>Store Type</label>
                        <select name="setting[store_type]">
                            <option value="general" <?php if ($settings['store_type'] == 'general') { ?>selected="selected"<?php } ?>>General</option>
                            <option value="catering" <?php if ($settings['store_type'] == 'catering') { ?>selected="selected"<?php } ?>>Catering</option>
                            <option value="booking" <?php if ($settings['store_type'] == 'booking') { ?>selected="selected"<?php } ?>>Booking</option>
                        </select>
                    </fieldset>
                <?php } ?>

                <fieldset>
                    <input id="storefront_enabled" type="checkbox" name="setting[storefront_enabled]" value="1" <?php if ($settings['storefront_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Storefront enabled <small>Visitors may access your shopping area.</small></label>
                </fieldset>

                <fieldset>
                    <input type="checkbox" name="setting[purchasing_enabled]" value="1" <?php if ($settings['purchasing_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Purchasing enabled <small>Visitors may place orders.</small></label>
                </fieldset>

                <fieldset>
                    <input type="checkbox" name="setting[ssl_enabled]" value="1" <?php if ($settings['ssl_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">SSL enabled</label>
                </fieldset>

                <?php

                // ACCOUNTS ARE ENABLED
                if ($_uccms['_account']->isEnabled()) {

                    ?>

                    <fieldset>
                        <input id="accounts_auto_create" type="checkbox" name="setting[accounts_auto_create]" value="1" <?php if ($settings['accounts_auto_create']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Automatically create accounts</label>
                    </fieldset>

                    <fieldset class="accounts_auto_create_toggle" style="<?php if (!$settings['accounts_auto_create']) { ?>display: none;<?php } ?>">
                        <input type="checkbox" name="setting[accounts_auto_email]" value="1" <?php if ($settings['accounts_auto_email']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Email automatically created accounts with account info</label>
                    </fieldset>

                    <?php

                }

                ?>

                <fieldset>
                    <input type="checkbox" name="setting[admin_footer_cart_enabled]" value="1" <?php if ($settings['admin_footer_cart_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Enable admin footer cart</label>
                </fieldset>

                <fieldset class="service-fee clearfix">
                    <label>Service / Convenience Fee</label>
                    <select name="setting[servicefee_type]">
                        <option value="percent" <?php if ($settings['servicefee_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                        <option value="amount" <?php if ($settings['servicefee_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                    </select>
                    &nbsp;
                    <input type="text" name="setting[servicefee_amount]" value="<?php echo number_format((float)$settings['servicefee_amount'], 2); ?>" style="display: inline-block; width: 60px;" />
                </fieldset>

            </div>

        </div>

        <fieldset id="storefront_disabled_content" style="<?php if ($settings['storefront_enabled']) { ?>display: none; <?php } ?>padding-top: 15px;">
            <label>Store Disabled Content</label>
            <div>
                <?php
                $field = array(
                    'key'       => 'setting[store_disabled_content]', // The value you should use for the "name" attribute of your form field
                    'value'     => stripslashes($settings['store_disabled_content']), // The existing value for this form field
                    'id'        => 'store_disabled_content', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'simple' => false
                    )
                );
                include(BigTree::path('admin/form-field-types/draw/html.php'));
                ?>
            </div>
        </fieldset>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Home</h2>
    </header>

    <section>

        <fieldset>
            <label>Content</label>
            <div>
                <?php
                $field = array(
                    'key'       => 'setting[home_content]', // The value you should use for the "name" attribute of your form field
                    'value'     => stripslashes($settings['home_content']), // The existing value for this form field
                    'id'        => 'home_content', // A unique ID you can assign to your form field for use in JavaScript
                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                    'options'   => array(
                        'simple' => false
                    )
                );
                include(BigTree::path('admin/form-field-types/draw/html.php'));
                ?>
            </div>
        </fieldset>

        <div class="contain" style="padding-bottom: 20px;">

            <div class="left last">

                <fieldset>
                    <label>Number of Featured Items</label>
                    <input type="text" name="setting[home_featured_num]" value="<?php echo (int)$settings['home_featured_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset>
                    <label>Featured Items Title</label>
                    <input type="text" name="setting[home_featured_title]" value="<?php echo stripslashes($settings['home_featured_title']); ?>" placeholder="Featured Items" />
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <label>Number of Recent Items</label>
                    <input type="text" name="setting[home_recent_num]" value="<?php echo (int)$settings['home_recent_num']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset>
                    <label>Recent Items Title</label>
                    <input type="text" name="setting[home_recent_title]" value="<?php echo stripslashes($settings['home_recent_title']); ?>" placeholder="Recent Items" />
                </fieldset>

            </div>

        </div>

        <fieldset>
            <input type="checkbox" name="setting[home_hide_categories]" value="1" <?php if ($settings['home_hide_categories']) { ?>checked="checked"<?php } ?> />
            <label class="for_checkbox">Hide the categories section.</small></label>
        </fieldset>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="home_seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-home_seo" style="display: none;">

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="setting[home_meta_title]" value="<?php echo stripslashes($settings['home_meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="setting[home_meta_description]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="setting[home_meta_keywords]" style="height: 64px;"><?php echo stripslashes($settings['home_meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Categories</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Number of Items Per Page</label>
                    <input type="text" name="setting[items_per_page]" value="<?php echo (int)$settings['items_per_page']; ?>" class="smaller_60" />
                </fieldset>

            </div>

            <div class="right last">

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Items</h2>
    </header>

    <section>

        <div class="contain" style="margin-top: 15px;">

            <fieldset>
                <input id="item_ratings_reviews_enabled" type="checkbox" name="setting[item_ratings_reviews_enabled]" value="1" <?php if ($settings['item_ratings_reviews_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Ratings & Reviews Enabled</label>
            </fieldset>

            <div id="toggle_item_reviews" class="contain" style="<?php if (!$settings['item_ratings_reviews_enabled']) { ?>display: none;<?php } ?>">

                <div class="left">

                    <fieldset class="item_review-criteria">
                        <div class="contain">
                            <div class="left inner_quarter last">
                                <label>Review Criteria</label>
                            </div>
                            <div class="left inner_quarter last" style="margin: 0px; text-align: right;">
                                <a href="#" class="add button_small">Add</a>
                            </div>
                        </div>
                        <div id="add_item_review-criteria" style="display: none; padding: 5px 0 10px 0;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px;">
                                <tr>
                                    <td style="padding-right: 0px;">
                                        <input type="text" name="item_review-criteria_title[0]" value="" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td style="padding-right: 0px;">
                                        <input type="checkbox" name="item_review-criteria_required[0]" value="1" /> Req
                                    </td>
                                    <td>
                                        <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                            <?php foreach ($_uccms_ecomm->review_criteria('item') as $criteria_id => $criteria) { ?>
                                <tr id="prc-<?=$criteria_id?>" class="item">
                                    <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                                    <td width="60%" style="padding-right: 0px;">
                                        <input type="text" name="item_review-criteria_title[<?=$criteria_id?>]" value="<?=$criteria['title']?>" placeholder="Title" style="width: 250px;" />
                                    </td>
                                    <td width="40%" style="padding-right: 0px; white-space: nowrap;">
                                         <input type="checkbox" name="item_review-criteria_required[<?=$criteria_id?>]" value="1" <?php if ($criteria['required'] == 1) { ?>checked="checked"<?php } ?> /> Req
                                    </td>
                                    <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </fieldset>

                </div>

                <div class="right">

                </div>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>


<script type="text/javascript">

    $(document).ready(function() {

        // PRICE GROUPS - SORTABLE
        $('form .pricegroups table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // PRICE GROUPS - ADD
        $('form .pricegroups .add').click(function(e) {
            e.preventDefault();
            $('#add_pricegroup').toggle();
        });

        // PRICE GROUPS - REMOVE
        $('form .pricegroups .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

        // PRICE GROUPS TOGGLE
        $('#pricegroups_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_pricegroups').show();
            } else {
                $('#toggle_pricegroups').hide();
            }
        });

        // GRATUITY TOGGLE
        $('#gratuity_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_gratuity').show();
            } else {
                $('#toggle_gratuity').hide();
            }
        });

    });

</script>

<div class="container legacy">

    <header>
        <h2>Cart</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>"After Tax" Title</label>
                    <input type="text" name="setting[cart_after_tax_title]" value="<?php echo $settings['cart_after_tax_title']; ?>" />
                </fieldset>

                <fieldset>
                    <label>Requires Quote Message</label>
                    <div>
                        <?php
                        $field = array(
                            'key'       => 'setting[cart_requires_quote_message]', // The value you should use for the "name" attribute of your form field
                            'value'     => stripslashes($settings['cart_requires_quote_message']), // The existing value for this form field
                            'id'        => 'cart_requires_quote_message', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'simple' => false
                            )
                        );
                        include(BigTree::path('admin/form-field-types/draw/html.php'));
                        ?>
                    </div>
                </fieldset>

            </div>

            <div class="right last">

                <?php if ($_uccms['_account']->isEnabled()) { ?>

                    <fieldset>
                        <input id="pricegroups_enabled" type="checkbox" name="setting[pricegroups_enabled]" value="1" <?php if ($settings['pricegroups_enabled']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Enable price groups <small>Retail, wholesale, etc.</small></label>
                    </fieldset>

                    <div id="toggle_pricegroups" class="contain" style="<?php if (!$settings['pricegroups_enabled']) { ?>display: none;<?php } ?> padding-bottom: 15px;">

                        <fieldset class="pricegroups">
                            <div class="contain">
                                <div class="left inner_quarter last">
                                    <label>Price Groups</label>
                                </div>
                                <div class="left inner_quarter last" style="margin: 0px; text-align: right;">
                                    <a href="#" class="add button_small">Add</a>
                                </div>
                            </div>
                            <div id="add_pricegroup" style="display: none; padding: 5px 0 10px 0;">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px;">
                                    <tr>
                                        <td>
                                            <input type="text" name="pricegroup_title[0]" value="" placeholder="Title" style="width: 300px;" />
                                        </td>
                                        <td>
                                            <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                                <?php foreach ($_uccms_ecomm->priceGroups() as $group_id => $group) { ?>
                                    <tr id="pg-<?=$group_id?>" class="item">
                                        <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                                        <td style="padding-right: 0px;" width="80%"><input type="text" name="pricegroup_title[<?=$group_id?>]" value="<?=$group['title']?>" placeholder="Title" style="width: 300px;" /></td>
                                        <td><?php if ($group_id != 1) { ?><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a><?php } ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </fieldset>

                    </div>

                <?php } ?>

                <fieldset>
                    <input id="gratuity_enabled" type="checkbox" name="setting[gratuity_enabled]" value="1" <?php if ($settings['gratuity_enabled']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Enable gratuity</label>
                </fieldset>

                <div id="toggle_gratuity" class="contain" style="padding-bottom: 15px; <?php if (!$settings['gratuity_enabled']) { ?>display: none;<?php } ?>">
                    <fieldset>
                        %<input type="text" name="setting[gratuity_percent]" value="<?php echo $settings['gratuity_percent']; ?>" class="smaller_60" />
                        <small style="color: #999;">Default percent</small>
                    </fieldset>
                </div>

                <fieldset class="last">
                    <label>Temporary Hold</label>
                    <input type="text" name="setting[cart_temp_hold]" value="<?php echo (int)$settings['cart_temp_hold']; ?>" class="smaller_60" /> seconds
                    <small style="display: block; color: #999;">When an item is added to the cart make it's added quantity unavailable for this time period.</small>
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Checkout - Legal / Policies</h2>
    </header>

    <section>

        <div class="contain">

            <fieldset>
                <input id="checkout_legal_enabled" type="checkbox" name="setting[checkout_legal_enabled]" value="1" <?php if ($settings['checkout_legal_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Step Enabled <small>Display before checkout page.</small></label>
            </fieldset>

            <div id="toggle_checkout_legal" class="contain" style="<?php if (!$settings['checkout_legal_enabled']) { ?>display: none;<?php } ?> padding-top: 10px;">

                <fieldset>
                    <label>Content</label>
                    <div>
                        <?php
                        $field = array(
                            'key'       => 'setting[checkout_legal_content]', // The value you should use for the "name" attribute of your form field
                            'value'     => stripslashes($settings['checkout_legal_content']), // The existing value for this form field
                            'id'        => 'checkout_legal_content', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'simple' => false
                            )
                        );
                        include(BigTree::path('admin/form-field-types/draw/html.php'));
                        ?>
                    </div>
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="setting[checkout_legal_required]" value="1" <?php if ($settings['checkout_legal_required']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Agree Required <small>Customer must agree before continuing to checkout.</small></label>
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Checkout</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <input id="checkout_notes" type="checkbox" name="setting[checkout_notes]" value="1" <?php if ($settings['checkout_notes']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Order Notes <small>Display on checkout page</small></label>
                </fieldset>

                <div id="toggle_checkout_notes" class="contain" style="<?php if (!$settings['checkout_notes']) { ?>display: none;<?php } ?> padding-top: 10px;">

                    <fieldset class="last">
                        <label>Order Notes Description / Instructions</label>
                        <textarea name="setting[checkout_notes_description]" style="height: 64px;"><?php echo stripslashes($settings['checkout_notes_description']); ?></textarea>
                    </fieldset>

                </div>

            </div>

            <div class="right">
            </div>

        </div>

        <div class="contain last">

            <fieldset>
                <label>Custom Thank You Title</label>
                <input type="text" name="setting[checkout_thanks_title]" value="<?php echo stripslashes($settings['checkout_thanks_title']); ?>" />
            </fieldset>

            <fieldset>
                <label>Custom Thank You Message</label>
                <div>
                    <?php
                    $field = array(
                        'key'       => 'setting[checkout_thanks_content]', // The value you should use for the "name" attribute of your form field
                        'value'     => stripslashes($settings['checkout_thanks_content']), // The existing value for this form field
                        'id'        => 'checkout_thanks_content', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'simple' => false
                        )
                    );
                    include(BigTree::path('admin/form-field-types/draw/html.php'));
                    ?>
                </div>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<?php

// DEFAULTS
if (!$settings['quote_email_send_subject']) $settings['quote_email_send_subject'] = 'Quote Available';
if (!$settings['quote_email_send_body']) $settings['quote_email_send_body'] = 'Your quote is available to view!';

?>

<div class="container legacy">

    <header>
        <h2>Quote</h2>
    </header>

    <section>

        <div class="contain last">

            <fieldset>
                <label>Logo <small>(overrides store logo)</small></label>
                <div>
                    <?php
                    $field = array(
                        'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                        'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                        'key'       => 'quote_logo', // The value you should use for the "name" attribute of your form field
                        'value'     => $_uccms_ecomm->bigtreeFileURL($settings['quote_logo']), // The existing value for this form field
                        'id'        => 'quote_logo', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'image' => true
                        ),
                        'required'  => false // A boolean value of whether this form field is required or not
                    );
                    $bigtree['form']['embedded'] = true; // HIDE BROWSE BUTTON
                    include(BigTree::path('admin/form-field-types/draw/upload.php'));
                    ?>
                    <input type="hidden" name="quote_logo_remove" value="" />
                </div>
                <script type="text/javascript">
                    $(function() {
                        $('#quote_logo .remove_resource').click(function(e) {
                            e.preventDefault();
                            $('input[name="quote_logo_remove"]').val('1');
                            $('#quote_logo').remove();
                        });
                    });
                </script>
            </fieldset>

            <fieldset>
                <label>Request: Thank You Heading</label>
                <input type="text" name="setting[quote_request_thanks_title]" value="<?php echo stripslashes($settings['quote_request_thanks_title']); ?>" />
            </fieldset>

            <fieldset>
                <label>Request: Thank You Message</label>
                <div>
                    <?php
                    $field = array(
                        'key'       => 'setting[quote_request_thanks_content]', // The value you should use for the "name" attribute of your form field
                        'value'     => stripslashes($settings['quote_request_thanks_content']), // The existing value for this form field
                        'id'        => 'quote_request_thanks_content', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'simple' => false
                        )
                    );
                    include(BigTree::path('admin/form-field-types/draw/html.php'));
                    ?>
                </div>
            </fieldset>

            <fieldset>
                <label>Send: Default Email Subject</label>
                <input type="text" name="setting[quote_email_send_subject]" value="<?php echo stripslashes($settings['quote_email_send_subject']); ?>" />
            </fieldset>

            <fieldset>
                <label>Send: Default Email Message</label>
                <div>
                    <?php
                    $field = array(
                        'key'       => 'setting[quote_email_send_message]', // The value you should use for the "name" attribute of your form field
                        'value'     => stripslashes($settings['quote_email_send_message']), // The existing value for this form field
                        'id'        => 'quote_email_send_message', // A unique ID you can assign to your form field for use in JavaScript
                        'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                        'options'   => array(
                            'simple' => true
                        )
                    );
                    include(BigTree::path('admin/form-field-types/draw/html.php'));
                    ?>
                </div>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy">

    <header>
        <h2>Inventory</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <input id="inventory_track" type="checkbox" name="setting[inventory_track]" value="1" <?php if ($settings['inventory_track']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Track Inventory</label>
                </fieldset>

                <div id="toggle_inventory_track" class="contain" style="<?php if (!$settings['inventory_track']) { ?>display: none;<?php } ?> padding-top: 10px;">

                    <fieldset>
                        <input type="checkbox" name="setting[inventory_backorder]" value="1" <?php if ($settings['inventory_backorder']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Allow Backordering</label>
                    </fieldset>

                </div>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>

</form>