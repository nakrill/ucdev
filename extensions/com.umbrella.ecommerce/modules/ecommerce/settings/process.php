<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    $old['inventory_track'] = $_uccms_ecomm->getSetting('inventory_track');

    if (!$_POST['setting']['cart_requires_quote_message']) {
        $_POST['setting']['cart_requires_quote_message'] = 'You have items in your cart that require a quote.<br />After checkout (if applicable) you will be taken to a page to request a quote on these items.';
    }

    // ACCOUNTS NOT ENABLED
    if (!$_uccms['_account']->isEnabled()) {
        $_POST['setting']['pricegroups_enabled'] = 0;
    }

    $_POST['setting']['hours_of_operation-start'] = date('H:i:00', strtotime($_POST['setting']['hours_of_operation-start']));
    $_POST['setting']['hours_of_operation-end'] = date('H:i:00', strtotime($_POST['setting']['hours_of_operation-end']));

    // SETTINGS ARRAY
    $seta = array(
        'store_name',
        'store_address',
        'store_phone',
        'hours_of_operation-start',
        'hours_of_operation-end',
        'storefront_enabled',
        'purchasing_enabled',
        'ssl_enabled',
        'accounts_auto_create',
        'accounts_auto_email',
        'admin_footer_cart_enabled',
        'servicefee_type',
        'servicefee_amount',
        'store_disabled_content',
        'home_content',
        'home_featured_num',
        'home_featured_title',
        'home_recent_num',
        'home_recent_title',
        'home_meta_title',
        'home_meta_description',
        'home_meta_keywords',
        'home_hide_categories',
        'pricegroups_enabled',
        'gratuity_enabled',
        'gratuity_percent',
        'items_per_page',
        'item_ratings_reviews_enabled',
        'cart_after_tax_title',
        'cart_requires_quote_message',
        'cart_temp_hold',
        'checkout_legal_enabled',
        'checkout_legal_content',
        'checkout_legal_required',
        'checkout_notes',
        'checkout_notes_description',
        'checkout_thanks_title',
        'checkout_thanks_content',
        'quote_request_thanks_title',
        'quote_request_thanks_content',
        'quote_email_send_subject',
        'quote_email_send_message',
        'inventory_track',
        'inventory_backorder'
    );

    // DEVELOPER
    if ($admin->Level == 2) {
        array_unshift($seta, 'store_type');
    }

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_ecomm->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    // REMOVING QUOTE LOGO
    if ($_POST['quote_logo_remove']) {
        $_uccms_ecomm->setSetting('quote_logo', ''); // SAVE SETTING
    }

    // QUOTE LOGO UPLOAD
    $field = array(
        'file_input'    => $_FILES['quote_logo'],
        'title'         => 'Quote Logo', // The title given by the developer to draw as the label (drawn automatically)
        'subtitle'      => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
        'options'       => array(
            'image'     => true
        ),
        'required'      => false // A boolean value of whether this form field is required or not
    );
    include(BigTree::path('admin/form-field-types/process/upload.php'));

    // HAVE A RESULT
    if ($field['output']) {

        // SAVE SETTING
        $_uccms_ecomm->setSetting('quote_logo', $field['output']);

    }

    ########################
    # ITEM - REVIEW CRITERIA
    ########################

    // NEW FIELD
    $new_field['title']     = $_POST['item_review-criteria_title'][0];
    $new_field['required']  = $_POST['item_review-criteria_required'][0];
    unset($_POST['item_review-criteria_title'][0]);
    unset($_POST['item_review-criteria_required'][0]);

    // REVIEW CRITERIA FIELD ARRAY
    $prca = array();

    // TOP FIELD ID
    $top_field_id = 0;

    // REVIEW CRITERIA FIELDS SPECIFIED
    if (count($_POST['item_review-criteria_title']) > 0) {

        // LOOP
        foreach ($_POST['item_review-criteria_title'] as $field_id => $field_title) {
            $prca[$field_id] = array(
                'title'     => $field_title,
                'required'  => (int)$_POST['item_review-criteria_required'][$field_id]
            );
            if ($field_id > $top_field_id) $top_field_id = $field_id;
        }

    }

    // HAVE NEW FIELD
    if ($new_field['title']) {
        $prca[$top_field_id+1] = array(
            'title'     => $new_field['title'],
            'required'  => $new_field['required']
        );
    }

    // SAVE REVIEW CRITERIA FIELDS
    $_uccms_ecomm->setSetting('review_criteria-item', $prca);

    ########################
    # PRICE GROUPS
    ########################

    // NEW PRICE GROUP
    $new_pricegroup['title']    = $_POST['pricegroup_title'][0];
    $new_pricegroup['time']     = $_POST['pricegroup_time'][0];
    unset($_POST['pricegroup_title'][0]);
    unset($_POST['pricegroup_time'][0]);

    // PRICE GROUP ARRAY
    $pga = array();

    // TOP PRICEGROUP ID
    $top_pricegroup_id = 1;

    // PRICE GROUPS SPECIFIED
    if (count($_POST['pricegroup_title']) > 0) {

        // LOOP
        foreach ($_POST['pricegroup_title'] as $group_id => $group_title) {
            $pga[$group_id] = array(
                'title' => $group_title
            );
            if ($group_id > $top_pricegroup_id) $top_pricegroup_id = $group_id;
        }

    // NO PRICEGROUPS SPECIFIED
    } else {

        // USE DEFAULTS
        foreach ($_uccms_ecomm->priceGroups() as $group_id => $group) {
            $pga[$group_id] = $group;
            if ($group_id > $top_pricegroup_id) $top_pricegroup_id = $group_id;
        }

    }

    // HAVE NEW PRICE GROUP
    if ($new_pricegroup['title']) {
        $pga[$top_pricegroup_id+1] = array(
            'title' => $new_pricegroup['title']
        );
    }

    // SAVE PRICE GROUPS
    $_uccms_ecomm->setSetting('pricegroups', $pga);

    ########################
    # INVENTORY
    ########################

    // INVENTORY TRACKING WAS DISABLED, NOW ENABLED
    if ((!$old['inventory_track']) && ($_POST['setting']['inventory_track'])) {

        // GET ALL ITEMS
        $items_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0)";
        $items_q = sqlquery($items_query);
        while ($item = sqlfetch($items_q)) {
            $_uccms_ecomm->item_updateVariants($item['id']); // UPDATE VARIANTS FOR ITEM
        }

    }

    ########################


    $admin->growl('General Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/');

?>