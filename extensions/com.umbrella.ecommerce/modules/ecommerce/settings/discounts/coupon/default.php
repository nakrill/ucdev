<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $coupon_query = "SELECT * FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (`id`=" .$id. ")";
    $coupon_q = sqlquery($coupon_query);
    $coupon = sqlfetch($coupon_q);

}

?>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="coupon[id]" value="<?php echo $coupon['id']; ?>" />

<div class="container legacy legacy">

    <header>
        <h2>Coupon Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>Code</label>
                <input type="text" name="coupon[code]" value="<?php echo stripslashes($coupon['code']); ?>" />
            </fieldset>

            <fieldset>
                <input type="checkbox" name="coupon[active]" value="1" <?php if ($coupon['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

            <fieldset>
                <label>Description</label>
                <textarea name="coupon[description]"><?php echo stripslashes($coupon['description']); ?></textarea>
            </fieldset>

        </div>

        <div class="right last">

            <fieldset>
                <label>Amount</label>
                <div class="clearfix">
                    <select name="coupon[type]">
                        <option value="percent" <?php if ($coupon['type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                        <option value="amount" <?php if ($coupon['type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                    </select>
                    &nbsp;
                    <input type="text" name="coupon[amount]" value="<?php echo number_format((float)$coupon['amount'], 2); ?>" style="display: inline-block; width: 60px;" />
                </div>
            </fieldset>

            <fieldset class="coupon_date">
                <label>Start</label>
                <div style="float: left; width: 50%;">
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'coupon_start_date',
                        'key'       => 'coupon[start_date]',
                        'value'     => $coupon['dt_start'],
                        'required'  => false,
                        'options'   => array(
                            'default_today' => false
                        )
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                    ?>
                </div>
                <div style="float: right; width: 50%;">
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'coupon_start_time',
                        'key'       => 'coupon[start_time]',
                        'value'     => $coupon['dt_start'],
                        'required'  => false
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/time.php'));

                    ?>
                </div>
            </fieldset>

            <fieldset class="coupon_date">
                <label>End</label>
                <div style="float: left; width: 50%;">
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'coupon_end_date',
                        'key'       => 'coupon[end_date]',
                        'value'     => $coupon['dt_end'],
                        'required'  => false,
                        'options'   => array(
                            'default_today' => false
                        )
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                    ?>
                </div>
                <div style="float: right; width: 50%;">
                    <?php

                    // FIELD VALUES
                    $field = array(
                        'id'        => 'coupon_end_time',
                        'key'       => 'coupon[end_time]',
                        'value'     => $coupon['dt_end'],
                        'required'  => false
                    );

                    // INCLUDE FIELD
                    include(BigTree::path('admin/form-field-types/draw/time.php'));

                    ?>
                </div>
            </fieldset>

            <div class="contain">

                <fieldset class="left last inner_quarter">
                    <label>Limit Per Customer</label>
                    <input type="text" name="coupon[limit_customer]" value="<?php echo (int)$coupon['limit_customer']; ?>" class="smaller_60" />
                </fieldset>

                <fieldset class="right last inner_quarter">
                    <label>Total Limit</label>
                    <input type="text" name="coupon[limit_total]" value="<?php echo (int)$coupon['limit_total']; ?>" class="smaller_60" />
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../#coupons">&laquo; Back</a>
    </footer>

</div>

<div class="container legacy legacy" style="margin-bottom: 0px; border: 0px none;">

    <style type="text/css">
        #categories > ul {
            max-height: 250px;
            overflow-y: scroll;
        }
        #categories.table > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }
        #categories.table > ul li li {
            padding-bottom: 0px;
            padding-left: 20px;
            border: 0px none;
            background-color: transparent;
        }
        #categories.table > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }
        #categories.table > ul li section:first-child {
            padding-left: 0px;
        }
        #categories ul .checkbox {
            margin-top: -1px;
        }
    </style>

    <a name="categories"></a>

    <div id="categories" class="table">

        <summary>
            <h2>Categories</h2>
        </summary>

        <div style="padding: 10px 0; border-bottom: 1px solid #ccc; text-align: center;">
            If this coupon only applies to specific categories, select them below.
        </div>

        <?php

        // ARRAY OF SELECTED CATEGORIES
        $cata = array();

        // CURRENT SELECTED
        if ($coupon['categories']) {
            $cata = explode(',', $coupon['categories']);
        }

        // OUTPUT LIST
        function this_listElements($array) {
            global $cata;
            if (count($array) > 0) {
                echo '<ul>';
                foreach ($array as $element) {
                    if (in_array($element['id'], $cata)) {
                        $checked = 'checked="checked"';
                    } else {
                        $checked = '';
                    }
                    echo '<li><input type="checkbox" name="category[' .$element['id']. ']" value="1" ' .$checked. ' /> ' .stripslashes($element['title']);
                    if (!empty($element['children'])) {
                        this_listElements($element['children']);
                    }
                    echo '</li>';
                }
                echo '</ul>';
            }
        }

        // BUILD LIST OFF ALL CATEGORIES
        echo this_listElements($_uccms_ecomm->getCategoryTree());

        ?>

        <footer>
            <input class="blue" type="submit" value="Save" name="section_categories" />
        </footer>

    </div>

    <style type="text/css">
        #items > ul {
            max-height: 250px;
            overflow-y: scroll;
        }
        #items > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }
        #items li a {
            color: #333;
        }
        #items li.inactive a {
            color: #999;
        }
        #items ul .checkbox {
            margin-top: -1px;
        }
    </style>

    <a name="items"></a>

    <div id="items" class="table">

        <summary>
            <h2>Items</h2>
        </summary>

        <div style="padding: 10px 0; border-bottom: 1px solid #ccc; text-align: center;">
            If this coupon only applies to specific items, select them below.
        </div>

        <?php

        // ARRAY OF SELECTED ITEMS
        $itema = array();

        // CURRENT SELECTED
        if ($coupon['items']) {
            $itema = explode(',', $coupon['items']);
        }

        // GET ITEMS
        $item_query = "SELECT `id`, `active`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0) ORDER BY `title` ASC";
        $item_q = sqlquery($item_query);

        // HAVE ITEMS
        if (sqlrows($item_q) > 0) {
            ?>
            <ul>
                <?php while ($item = sqlfetch($item_q)) { ?>
                    <li class="<?php if (!$item['active']) { echo 'inactive'; } ?>"><input type="checkbox" name="item[<?php echo $item['id']; ?>]" value="1" <?php if (in_array($item['id'], $itema)) { echo 'checked="checked"'; } ?> /> <a href="../../../items/edit/?id=<?php echo $item['id']; ?>" target="blank"><?php echo stripslashes($item['title']); ?></a></li>
                <?php } ?>
            </ul>

            <?php
        } else {
            echo 'No items.';
        }

        ?>

        <footer>
            <input class="blue" type="submit" value="Save" name="section_items" />
        </footer>

    </div>

</div>

</form>