<?php

//echo print_r($_POST);
//exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['coupon']['id'];

    // START DATE & TIME
    if ($_POST['coupon']['start_date']) {
        $start = date('Y-m-d H:i:s', strtotime($_POST['coupon']['start_date']. ' ' .$_POST['coupon']['start_time']));
    } else {
        $start = '0000-00-00 00:00:00';
    }

    // END DATE & TIME
    if ($_POST['coupon']['end_date']) {
        $end = date('Y-m-d H:i:s', strtotime($_POST['coupon']['end_date']. ' ' .$_POST['coupon']['end_time']));
    } else {
        $end = '0000-00-00 00:00:00';
    }

    // DB COLUMNS
    $columns = array(
        'active'            => (int)$_POST['coupon']['active'],
        'code'              => trim($_POST['coupon']['code']),
        'description'       => $_POST['coupon']['description'],
        'type'              => $_POST['coupon']['type'],
        'amount'            => (float)$_POST['coupon']['amount'],
        'dt_start'          => $start,
        'dt_end'            => $end,
        'limit_customer'    => (int)$_POST['coupon']['limit_customer'],
        'limit_total'       => (int)$_POST['coupon']['limit_total'],
        'categories'        => implode(',', array_keys($_POST['category'])),
        'items'             => implode(',', array_keys($_POST['item'])),
        'updated_by'        => $_uccms_ecomm->adminID()
    );

    // HAVE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['coupons']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['coupons']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Coupon Settings', 'Settings saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Coupon Settings', 'Failed to save settings.');
    }

}

if ($_POST['section_categories']) {
    BigTree::redirect(MODULE_ROOT.'settings/discounts/coupon/?id=' .$id. '#categories');
} else if ($_POST['section_items']) {
    BigTree::redirect(MODULE_ROOT.'settings/discounts/coupon/?id=' .$id. '#items');
} else {
    BigTree::redirect(MODULE_ROOT.'settings/discounts/coupon/?id=' .$id);
}

?>