<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB QUERY
    $query = "UPDATE `" .$_uccms_ecomm->tables['coupons']. "` SET `active`=0, `deleted_dt`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Coupon', 'Coupon deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Coupon', 'Failed to delete coupon.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Coupon', 'No coupon specified.');
}

BigTree::redirect(MODULE_ROOT.'settings/discounts/#coupons');

?>