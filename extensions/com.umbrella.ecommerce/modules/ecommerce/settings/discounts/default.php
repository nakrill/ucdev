<?php

// GET SETTINGS
$settings = $_uccms_ecomm->getSettings();

?>


<script type="text/javascript">

    $(document).ready(function() {

        /*
        // GLOBAL DISCOUNT TOGGLE
        $('#discount_global').change(function(e) {
            if ($(this).attr('checked')) {
                $('#toggle_discount_global').show();
            } else {
                $('#toggle_discount_global').hide();
            }
        });

        // GLOBAL DISCOUNT TYPE TOGGLE
        $('#select_discount_global_type').change(function(e) {
            $('#toggle_discount_global fieldset.discount div').hide();
            $('#toggle_discount_global fieldset.discount #discount_global-' +$(this).val()).show();
        });
        */

        $('select[name="setting[discount_global_quantity_type]"]').change(function(e) {
            if ($(this).val() == 'amount') {
                $('#discount_quantity-repeat').show();
            } else {
                $('#discount_quantity-repeat').hide();
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy legacy">

    <header>
        <h2>Discount Settings</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <h3>By Quantity</h3>

                Apply discount of

                <br />

                <div class="clearfix" style="padding: 6px 0;">
                    <select name="setting[discount_global_quantity_type]">
                        <option value="percent" <?php if ($settings['discount_global_quantity_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                        <option value="amount" <?php if ($settings['discount_global_quantity_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                    </select>
                    &nbsp;
                    <input type="text" name="setting[discount_global_quantity_amount]" value="<?php echo number_format((float)$settings['discount_global_quantity_amount'], 2); ?>" style="display: inline-block; width: 60px;" />
                </div>

                when the number of items in the cart is at least

                <div class="clearfix" style="padding: 6px 0 0;">
                    <input type="text" name="setting[discount_global_quantity_quantity]" value="<?php echo (int)$settings['discount_global_quantity_quantity']; ?>" style="display: inline-block; width: 60px;" />
                </div>

                <div id="discount_quantity-repeat" class="repeat form-check" style="<?php if ($settings['discount_global_quantity_type'] != 'amount') { ?>display: none;<?php } ?>margin-top: 8px;">
                    <input type="checkbox" name="setting[discount_global_quantity_repeat]" value="1" <?php if ($settings['discount_global_quantity_repeat']) { ?>checked="checked"<?php } ?> /> As many times as applicable
                </div>

            </div>

            <div class="right">

                <h3>By Total</h3>

                Apply discount of

                <br />

                <div class="clearfix" style="padding: 6px 0;">
                    <select name="setting[discount_global_total_type]">
                        <option value="percent" <?php if ($settings['discount_global_total_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                        <option value="amount" <?php if ($settings['discount_global_total_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                    </select>
                    &nbsp;
                    <input type="text" name="setting[discount_global_total_amount]" value="<?php echo number_format((float)$settings['discount_global_total_amount'], 2); ?>" style="display: inline-block; width: 60px;" />
                </div>

                when the total in the cart is at least

                <div class="clearfix" style="padding: 6px 0 0;">
                    $<input type="text" name="setting[discount_global_total_total]" value="<?php echo (int)$settings['discount_global_total_total']; ?>" style="display: inline-block; width: 60px;" />
                </div>

            </div>

        </div>

        <div class="contain" style="margin-top: 10px;">

            <div class="left last">
                <h3>By Category</h3>
                Category discounts managed when <a href="<?=MODULE_ROOT?>categories/">editing a category</a>.
            </div>

            <div class="right last">
                <h3>By Item</h3>
                Item discounts managed when <a href="<?=MODULE_ROOT?>items/">editing an item</a>.
            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<style type="text/css">

    #coupons .coupon_code {
        width: 150px;
    }
    #coupons .coupon_amount {
        width: 150px;
    }
    #coupons .coupon_status {
        width: 100px;
    }
    #coupons .coupon_start {
        width: 175px;
    }
    #coupons .coupon_end {
        width: 175px;
    }
    #coupons .coupon_uses {
        width: 80px;
    }
    #coupons .coupon_edit {
        width: 55px;
    }
    #coupons .coupon_delete {
        width: 55px;
    }

</style>

<?php

// GET COUPONS
$coupon_query = "SELECT * FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (`deleted_dt`='0000-00-00 00:00:00') ORDER BY `id` DESC";
$coupon_q = sqlquery($coupon_query);

?>

<a name="coupons"></a>

<div id="coupons" class="table">
    <summary>
        <h2>Coupon Codes</h2>
        <a class="add_resource add" href="./coupon/"><span></span>Add Coupon</a>
    </summary>
    <header style="clear: both;">
        <span class="coupon_code">Code</span>
        <span class="coupon_amount">Amount</span>
        <span class="coupon_status">Status</span>
        <span class="coupon_start">Start</span>
        <span class="coupon_end">End</span>
        <span class="coupon_uses">Uses</span>
        <span class="coupon_edit">Edit</span>
        <span class="coupon_delete">Delete</span>
    </header>

    <?php if (sqlrows($coupon_q) > 0) { ?>

        <ul class="items">

            <?php while ($coupon = sqlfetch($coupon_q)) { ?>

                <li class="item">
                    <section class="coupon_code">
                        <?php echo stripslashes($coupon['code']); ?>
                    </section>
                    <section class="coupon_amount">
                        <?php if ($coupon['type'] == 'amount') { echo '$'; } echo $coupon['amount']; if ($coupon['type'] == 'percent') { echo '%'; } ?>
                    </section>
                    <section class="coupon_status status_<?php if ($coupon['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                        <?php if ($coupon['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                    </section>
                    <section class="coupon_start">
                        <?php if (($coupon['dt_start']) && ($coupon['dt_start'] != '0000-00-00 00:00:00')) { echo date('n/j/Y g:i A', strtotime($coupon['dt_start'])); } ?>
                    </section>
                    <section class="coupon_end">
                        <?php if (($coupon['dt_end']) && ($coupon['dt_end'] != '0000-00-00 00:00:00')) { echo date('n/j/Y g:i A', strtotime($coupon['dt_end'])); } ?>
                    </section>
                    <section class="coupon_uses">
                        <?php echo number_format($coupon['uses'], 0); ?>
                    </section>
                    <section class="coupon_edit">
                        <a class="icon_edit" title="Edit Coupon" href="./coupon/?id=<?php echo $coupon['id']; ?>"></a>
                    </section>
                    <section class="coupon_delete">
                        <a href="./coupon/delete/?id=<?php echo $coupon['id']; ?>" class="icon_delete" title="Delete Coupon" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                    </section>
                </li>

            <?php } ?>

        </ul>

    <?php } else { ?>

        <div style="padding: 10px 0; text-align: center;">
            No coupons created yet.
        </div>

    <?php } ?>

</div>

</form>