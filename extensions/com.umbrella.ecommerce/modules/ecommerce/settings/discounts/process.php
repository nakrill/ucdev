<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SETTINGS ARRAY
    $seta = array(
        'discount_global_quantity_type',
        'discount_global_quantity_amount',
        'discount_global_quantity_quantity',
        'discount_global_quantity_repeat',
        'discount_global_total_type',
        'discount_global_total_amount',
        'discount_global_total_total'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_ecomm->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('Discount Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/discounts/');

?>