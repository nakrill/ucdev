<?php

// GET LOCATIONS
$location_query = "SELECT * FROM `" .$_uccms_ecomm->tables['locations']. "` ORDER BY `sort` ASC, `title` ASC, `id` DESC";
$location_q = sqlquery($location_query);

// NUMBER OF LOCATIONS
$num_loc = sqlrows($location_q);

?>

<style type="text/css">

    #locations .location_title {
        width: 300px;
        text-align: left;
    }
    #locations .location_address {
        width: 230px;
        text-align: left;
    }
    #locations .location_city {
        width: 200px;
        text-align: left;
    }
    #locations .location_status {
        width: 100px;
    }
    #locations .location_edit {
        width: 55px;
    }
    #locations .location_delete {
        width: 55px;
    }

</style>

<div id="locations" class="table">
    <summary>
        <h2>Locations</h2>
        <a class="add_resource add" href="./edit/"><span></span>Add Location</a>
    </summary>
    <header style="clear: both;">
        <span class="location_title">Title</span>
        <span class="location_address">Address</span>
        <span class="location_city">City</span>
        <span class="location_status">Status</span>
        <span class="location_edit">Edit</span>
        <span class="location_delete">Delete</span>
    </header>

    <?php

    // HAVE LOCATIONS
    if ($num_loc > 0) {

        ?>

        <ul class="items">

            <?php

            // LOOP
            while ($location = sqlfetch($location_q)) {

                ?>

                <li id="row_<?php echo $location['id']; ?>" class="item">
                    <section class="location_title">
                        <span class="icon_sort ui-sortable-handle"></span>
                        <a href="./?id=<?php echo $location['id']; ?>"><?php echo stripslashes($location['title']); ?></a>
                    </section>
                    <section class="location_address">
                        <?php echo stripslashes($location['address1']); ?>
                    </section>
                    <section class="location_city">
                        <?php echo stripslashes($location['city']); ?>
                    </section>
                    <section class="location_status status_<?php if ($location['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                        <?php if ($location['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                    </section>
                    <section class="location_edit">
                        <a class="icon_edit" title="Edit Category" href="./?id=<?php echo $location['id']; ?>"></a>
                    </section>
                    <section class="location_delete">
                        <a href="./delete/?id=<?php echo $location['id']; ?>" class="icon_delete" title="Delete Category" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?\nItems in it will not be deleted, just removed from the category.');"></a>
                    </section>
                </li>

                <?php

            }

            ?>

        </ul>

        <?php

    } else {
        ?>
        <div style="padding: 10px; text-align: center;">
            No locations.
        </div>
        <?php
    }

    ?>

</div>

<script>
    $('#locations .items').sortable({
        axis: 'y',
        containment: 'parent',
        handle: '.icon_sort',
        items: 'li',
        placeholder: 'ui-sortable-placeholder',
        tolerance: 'pointer',
        update: function() {
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/locations/order/', {
                sort: $('#locations .items').sortable('serialize')
            });
        }
    });
</script>