<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['location']['id'];

    // USE LOCATION TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['location']['slug']) {
        $_POST['location']['slug'] = $_POST['location']['title'];
    }

    // DB COLUMNS
    $columns = array(
        'slug'              => $_uccms_ecomm->makeRewrite($_POST['location']['slug']),
        'parent'            => (int)$_POST['location']['parent'],
        'active'            => (int)$_POST['location']['active'],
        'title'             => $_POST['location']['title'],
        'description'       => $_POST['location']['description'],
        'address1'          => $_POST['location']['address1'],
        'address2'          => $_POST['location']['address2'],
        'city'              => $_POST['location']['city'],
        'state'             => $_POST['location']['state'],
        'zip'               => $_POST['location']['zip'],
        'phone'             => $_POST['location']['phone'],
        'email'             => $_POST['location']['email'],
        'url'               => $_POST['location']['url'],
        'meta_title'        => $_POST['location']['meta_title'],
        'meta_description'  => $_POST['location']['meta_description'],
        'meta_keywords'     => $_POST['location']['meta_keywords'],
        'updated_by'        => $_uccms_ecomm->adminID()
    );

    // HAVE LOCATION ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['locations']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO LOCATION ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['locations']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {

            $id = sqlid();

            $admin->growl('Location', 'Location added!');

            // GET ALL ITEMS
            $items_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0)";
            $items_q = sqlquery($items_query);
            while ($item = sqlfetch($items_q)) {
                $_uccms_ecomm->item_updateVariants($item['id']); // UPDATE VARIANTS FOR ITEM
            }

        // WAS UPDATING
        } else {
            $admin->growl('Location', 'Location updated!');
        }

        // FILE UPLOADED, DELETING EXISTING OR NEW SELECTED FROM MEDIA BROWSER
        if (($_FILES['image']['name']) || ((!$_POST['image']) || (substr($_POST['image'], 0, 11) == 'resource://'))) {

            // GET CURRENT IMAGE
            $ex_query = "SELECT `image` FROM `" .$_uccms_ecomm->tables['locations']. "` WHERE (`id`=" .$id. ")";
            $ex = sqlfetch(sqlquery($ex_query));

            // THERE'S AN EXISTING IMAGE
            if ($ex['image']) {

                // REMOVE IMAGE
                @unlink($_uccms_ecomm->imageBridgeOut($ex['image'], 'locations', true));

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_ecomm->tables['locations']. "` SET `image`='' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            }

        }

        // FILE UPLOADED / SELECTED
        if (($_FILES['image']['name']) || ($_POST['image'])) {

            // BIGTREE UPLOAD FIELD INFO
            $field = array(
                'type'          => 'upload',
                'title'         => 'Location Image',
                'key'           => 'image',
                'options'       => array(
                    'directory' => 'extensions/' .$_uccms_ecomm->Extension. '/files/locations',
                    'image' => true,
                    'thumbs' => array(
                        array(
                            'width'     => '480', // MATTD: make controlled through settings
                            'height'    => '480' // MATTD: make controlled through settings
                        )
                    ),
                    'crops' => array()
                )
            );

            // UPLOADED FILE
            if ($_FILES['image']['name']) {
                $field['file_input'] = $_FILES['image'];

            // FILE FROM MEDIA BROWSER
            } else if ($_POST['image']) {
                $field['input'] = $_POST['image'];
            }

            // DIGITAL ASSET MANAGER VARS
            $field['dam_vars'] = array(
                'website_extension'         => $_uccms_ecomm->Extension,
                'website_extension_item'    => 'location',
                'website_extension_item_id' => $id
            );

            // UPLOAD FILE AND GET PATH BACK (IF SUCCESSFUL)
            $file_path = BigTreeAdmin::processField($field);

            // UPLOAD SUCCESSFUL
            if ($file_path) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_ecomm->tables['locations']. "` SET `image`='" .sqlescape($_uccms_ecomm->imageBridgeIn($file_path)). "' WHERE (`id`=" .$id. ")";
                sqlquery($query);

            // UPLOAD FAILED
            } else {

                echo print_r($bigtree['errors']);
                exit;

                $admin->growl($bigtree['errors'][0]['field'], $bigtree['errors'][0]['error']);

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Location', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT.'/settings/locations/edit/?id=' .$id);

?>