<?php

$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET LOCATION
    $location_query = "SELECT * FROM `" .$_uccms_ecomm->tables['locations']. "` WHERE (`id`=" .$id. ")";
    $location_q = sqlquery($location_query);
    $location = sqlfetch($location_q);

}

?>

<div class="container legacy legacy">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="location[id]" value="<?php echo $location['id']; ?>" />

    <header>
        <h2><?php if ($location['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Location</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Title</label>
                    <input type="text" name="location[title]" value="<?php echo stripslashes($location['title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Description</label>
                    <textarea name="location[description]" style="height: 64px;"><?php echo stripslashes($location['description']); ?></textarea>
                </fieldset>

                <fieldset class="last">
                    <input type="checkbox" name="location[active]" value="1" <?php if ($location['active']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

                <? /*
                <fieldset>
                    <label>Image</label>
                    <div>
                        <?php
                        $field = array(
                            'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                            'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                            'key'       => 'image', // The value you should use for the "name" attribute of your form field
                            'value'     => ($location['image'] ? $_uccms_ecomm->imageBridgeOut($location['image'], 'locations') : ''), // The existing value for this form field
                            'id'        => 'location_image', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'image' => true
                            ),
                            'required'  => false // A boolean value of whether this form field is required or not
                        );
                        include(BigTree::path('admin/form-field-types/draw/upload.php'));
                        ?>
                    </div>
                </fieldset>
                */ ?>

            </div>

            <div class="right">

                <fieldset>
                    <label>Address</label>
                    <input type="text" name="location[address1]" value="<?php echo stripslashes($location['address1']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="location[address2]" value="<?php echo stripslashes($location['address2']); ?>" />
                </fieldset>

                <fieldset>
                    <label>City</label>
                    <input type="text" name="location[city]" value="<?php echo stripslashes($location['city']); ?>" />
                </fieldset>

                <fieldset>
                    <label>State</label>
                    <select name="location[state]">
                        <option value="">Select</option>
                        <?php
                        foreach (BigTree::$StateList as $state_code => $state_name) {
                            ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $location['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Zip</label>
                    <input type="text" name="location[zip]" value="<?php echo stripslashes($location['zip']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Phone</label>
                    <input type="text" name="location[phone]" value="<?php echo stripslashes($location['phone']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Email</label>
                    <input type="text" name="location[email]" value="<?php echo stripslashes($location['email']); ?>" />
                </fieldset>

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="location[url]" value="<?php echo stripslashes($location['url']); ?>" />
                </fieldset>

            </div>

        </div>

        <? /*
        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="location[slug]" value="<?php echo stripslashes($location['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="location[meta_title]" value="<?php echo stripslashes($location['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="location[meta_description]" style="height: 64px;"><?php echo stripslashes($location['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="location[meta_keywords]" style="height: 64px;"><?php echo stripslashes($location['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>
        */ ?>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>