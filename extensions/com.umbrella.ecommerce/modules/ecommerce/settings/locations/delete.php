<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // GET LOCATION INFO
    $location_query = "SELECT `image` FROM `" .$_uccms_ecomm->tables['locations']. "` WHERE (`id`=" .$id. ")";
    $location_q = sqlquery($location_query);
    $location = sqlfetch($location_q);

    // THERE'S AN EXISTING IMAGE
    if ($location['image']) {

        // REMOVE IMAGE
        @unlink($_uccms_ecomm->imageBridgeOut($location['image'], 'locations', true));

    }

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['locations']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Location', 'Location deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Location', 'Failed to delete location.');
    }

    // DELETE ALL ITEM VARIANTS AT LOCATION
    $var_query = "DELETE FROM `" .$_uccms_ecomm->tables['item_variants']. "` WHERE (`location_id`=" .$id. ")";
    sqlquery($var_query);

    // GET ALL ITEMS
    $items_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0)";
    $items_q = sqlquery($items_query);
    while ($item = sqlfetch($items_q)) {
        $_uccms_ecomm->item_updateVariants($item['id']); // UPDATE VARIANTS FOR ITEM
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Location', 'No location specified.');
}

BigTree::redirect(MODULE_ROOT.'settings/locations/');

?>