<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET CALENDAR INFO
    $calendar_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendars']. "` WHERE (`id`=" .$id. ")";
    $calendar_q = sqlquery($calendar_query);
    $calendar = sqlfetch($calendar_q);

    // CALENDAR FOUND
    if ($calendar['id']) {

        // GET DAY START / STOP TIMES
        $dse_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_days']. "` WHERE (`calendar_id`=" .$calendar['id']. ")";
        $dse_q = sqlquery($dse_query);
        while ($day = sqlfetch($dse_q)) {
            $dse[$day['day']] = $day;
        }

    }

// NO ID SPECIFIED
} else {

    // DEFAULTS
    $calendar['status'] = 1;

}

if (!is_array($dse)) $dse = array();

?>

<script type="text/javascript">

    $(document).ready(function() {

        // DAY - BOOKABLE CLICK
        $('#booking_days .day input.bookable').click(function() {
            $(this).closest('.day').find('select').prop('disabled', !$(this).prop('checked'));
        });

    });

</script>

<div style="padding-bottom: 5px;">
    &laquo; <a href="../">Calendars</a>
</div>

<div class="container legacy legacy">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="calendar[id]" value="<?php echo $calendar['id']; ?>" />

    <header>
        <h2><?php if ($calendar['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Calendar</h2>
    </header>

    <section>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label>Title</label>
                    <input type="text" name="calendar[title]" value="<?php echo stripslashes($calendar['title']); ?>" />
                </fieldset>

                <fieldset>
                    <input type="checkbox" name="calendar[status]" value="1" <?php if ($calendar['status'] == 1) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

            </div>

            <div class="right">

                <fieldset id="booking_days">
                    <label>Days:</label>
                    <div class="contain clearfix">

                        <table style="margin: 0px; border: 0px;">
                            <?php

                            // LOOP THROUGH DAYS OF WEEK
                            foreach ($_uccms_ecomm->daysOfWeek() as $day_id => $day) {

                                // THIS DAY
                                $this_day = $dse[$day_id];

                                // BOOKABLE OR NOT
                                $bookable = $this_day['bookable'];

                                ?>
                                <tr class="day">
                                    <td style="white-space: nowrap; font-weight: bold;"><?php echo $day['title']; ?></td>
                                    <td style="width: 100%; padding-top: 10px;"><input type="checkbox" name="calendar[dse][<?=$day_id?>][bookable]" value="1" <?php if ($bookable) { ?>checked="checked"<?php } ?>class="bookable" /> Bookable</td>
                                    <td style="white-space: nowrap;">
                                        <select name="calendar[dse][<?=$day_id?>][start]" <?php if (!$bookable) { ?>disabled="disabled"<?php } ?>class="custom_control">
                                            <?php
                                            for ($i=0; $i<=23; $i++) {
                                                for ($j=0; $j<=45; $j+=15) {
                                                    if (!$bookable) {
                                                        $selected = false;
                                                    } else {
                                                        if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['start']) {
                                                            $selected = true;
                                                        } else {
                                                            $selected = false;
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        &nbsp;-&nbsp;
                                        <select name="calendar[dse][<?=$day_id?>][end]" <?php if (!$bookable) { ?>disabled="disabled"<?php } ?> class="custom_control">
                                            <?php
                                            for ($i=0; $i<=23; $i++) {
                                                for ($j=0; $j<=45; $j+=15) {
                                                    if (!$bookable) {
                                                        $selected = false;
                                                    } else {
                                                        if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['end']) {
                                                            $selected = true;
                                                        } else {
                                                            $selected = false;
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>

                    </div>
                </fieldset>

            </div>

        </div>

        <div class="contain">
            <h3 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h3>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset>
                    <label>URL</label>
                    <input type="text" name="calendar[slug]" value="<?php echo stripslashes($calendar['slug']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Title</label>
                    <input type="text" name="calendar[meta_title]" value="<?php echo stripslashes($calendar['meta_title']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Meta Description</label>
                    <textarea name="calendar[meta_description]" style="height: 64px;"><?php echo stripslashes($calendar['meta_description']); ?></textarea>
                </fieldset>

                <fieldset>
                    <label>Meta Keywords</label>
                    <textarea name="calendar[meta_keywords]" style="height: 64px;"><?php echo stripslashes($calendar['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>

<?php

if ($calendar['id']) {

    // BLACKOUT REASONS
    $blackout_reasons = $_uccms_ecomm->blackoutReasons();

    ?>

    <link rel="stylesheet" href="/css/lib/daterangepicker.css">
    <script type="text/javascript" src="/js/lib/moment.js"></script>
    <script type="text/javascript" src="/js/lib/daterangepicker.js"></script>

    <style type="text/css">

        #blackout_dates .input {
            display: none;
        }
        #blackout_dates .calendar .blackout_dates_calendar {
            display: block;
        }
        #blackout_dates .calendar .blackout_dates_calendar .month-wrapper table .day {
            padding: 20px 0;
        }
        #blackout_dates .calendar .blackout_dates_calendar .month-wrapper table .day.off {
            color: #ccc;
        }
        <?php foreach ($_uccms_ecomm->blackoutReasons() as $reason_id => $reason) {
            if ($reason['color']) {
                ?>
                #blackout_dates .calendar .blackout_dates_calendar .month-wrapper table .day.off.reason-<?php echo $reason_id; ?> {
                    background-color: <?php echo $reason['color']; ?>;
                }
                <?php
            }
        }
        ?>
        #blackout_dates .add {
            float: right;
            margin-top: 10px;
        }
        #blackout_dates .dates_list {
            margin-top: 15px;
        }
        #blackout_dates .dates_list .item {
            margin-bottom: 5px;
            padding: 10px;
            background-color: #f5f5f5;
            line-height: 18px;
        }
        #blackout_dates .dates_list .item:last-child {
            margin-bottom: 0px;
        }
        #blackout_dates .dates_list .item .date {
            float: left;
        }
        #blackout_dates .dates_list .item .date .from, #blackout_dates .dates_list .item .date .to {
            font-weight: bold;
        }
        #blackout_dates .dates_list .item .reason {
            float: right;
            margin-right: 20px;
        }
        #blackout_dates .dates_list .item .notes {
            float: right;
            margin-right: 20px;
        }
        #blackout_dates .dates_list .item .remove {
            float: right;
            font-weight: bold;
        }
        #blackout_dates .dates_list .item .notes_toggle {
            display: none;
            clear: both;
            padding-top: 8px;
        }
        #blackout_dates .dates_list .item.template {
            display: none;
        }

    </style>

    <script type="text/javascript">

        var booking_avail = {};

        $(document).ready(function() {

            // CALENDAR
            $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                calendar_id: <?php echo $calendar['id']; ?>,
                from: '<?php echo date('Y-m-01'); ?>'
            }, function(data) {
                $.each(data, function(date, avail) {
                    booking_avail[date] = avail;
                });
                $('#blackout_dates .input').dateRangePicker({
                    inline: true,
                    container: '#blackout_dates .calendar',
                    alwaysOpen: true,
                    startDate: '<?php echo date('Y-m-d'); ?>',
                    extraClass: 'blackout_dates_calendar',
                    singleMonth: true,
                    customTopBar: 'Select the day(s) and then click Add button.',
                    beforeShowDay: function(t) {
                        var dt = moment(t).format('YYYY-MM-DD').toString();
                        var valid = 1;
                        var avail = 0;
                        var _class = '';
                        var ba = booking_avail[dt];
                        if (typeof ba === 'object') {
                            var av = ba.available;
                            if (typeof av === 'undefined') av = 0;
                            avail = av;
                            if (avail == 0) {
                                if (ba.reason_id) {
                                    _class = 'off reason-' +ba.reason_id;
                                } else {
                                    _class = 'off';
                                }
                            }
                        }
                        var _tooltip = '';
                        return [valid,_class,_tooltip];
                    },
                    beforeNextMonth: function(month, callback) {
                        $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                            calendar_id: <?php echo $calendar['id']; ?>,
                            from: month,
                            <?php if ($booking['calendar_id']) { ?>
                                calendar_id: <?php echo $booking['calendar_id']; ?>
                            <?php } ?>
                        }, function(data) {
                            $.each(data, function(date, avail) {
                                booking_avail[date] = avail;
                            });
                            callback();
                        }, 'json');
                    },
                    beforePrevMonth: function(month, callback) {
                        $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                            calendar_id: <?php echo $calendar['id']; ?>,
                            from: month,
                        }, function(data) {
                            $.each(data, function(date, avail) {
                                booking_avail[date] = avail;
                            });
                            callback();
                        }, 'json');
                    }
                });
            }, 'json');

            // ADD DATE(S)
            $('#blackout_dates .add').click(function(e) {
                e.preventDefault();
                var val = $('#blackout_dates .input').val();
                if (val) {
                    var date = val.split(' to ');
                    var clone = $('#blackout_dates .dates_list .item.template').clone().appendTo('#blackout_dates .dates_list');
                    clone.removeClass('template');
                    clone.find('input[name="date[]"]').val(val);
                    clone.find('.date .from').text(date[0]);
                    if (date[0] != date[1]) {
                        clone.find('.date .from').after(' to <span class="to">' +date[1]+ '</span>');
                    }
                    clone.show();
                } else {
                    alert('Please select your day(s).');
                }
            });

            // NOTES EXPAND
            $('#blackout_dates .dates_list').on('click', '.item .notes a', function(e) {
                e.preventDefault();
                var item = $(this).closest('.item');
                item.find('.notes_toggle').toggle();
            });

            // REMOVE DATE
            $('#blackout_dates .dates_list').on('click', '.item .remove a', function(e) {
                e.preventDefault();
                var item = $(this).closest('.item');
                item.fadeOut(500, function() {
                    item.remove();
                });
            });

        });

    </script>

    <a name="blackout"></a>

    <div class="container legacy legacy">

        <form enctype="multipart/form-data" action="./blackout/process/" method="post">
        <input type="hidden" name="calendar[id]" value="<?php echo $calendar['id']; ?>" />

        <header>
            <h2>Blackout</h2>
        </header>

        <section>

            <div id="blackout_dates" class="contain">

                <div class="left last">

                    <input value="" class="input" />

                    <div class="calendar"></div>

                    <a href="#" class="button add">Add</a>

                </div>

                <div class="right last">

                    <fieldset>
                        <label>Dates:</label>
                        <div class="contain">

                            <div class="dates_list">

                                <div class="item template clearfix">
                                    <input type="hidden" name="date[]" value="" />
                                    <div class="date">
                                        <span class="from"></span>
                                    </div>
                                    <div class="remove"><a href="#"><i class="fa fa-times"></i></a></div>
                                    <div class="notes">
                                        <a href="#">notes</a>
                                    </div>
                                    <?php if (count($blackout_reasons) > 0) { ?>
                                        <div class="reason">
                                            <select name="reason[]" class="custom_control">
                                                <option value="0">No reason</option>
                                                <?php foreach ($blackout_reasons as $reason_id => $reason) { ?>
                                                    <option value="<?php echo $reason_id; ?>"><?php echo $reason['title']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <?php } ?>
                                    <div class="notes_toggle">
                                        <textarea name="notes[]" style="width: 393px; height: 48px;"></textarea>
                                    </div>
                                </div>

                                <?php

                                // GET DATES
                                $bd_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_blackouts']. "` WHERE (`calendar_id`=" .$calendar['id']. ") AND (`dt_from`>='" .date('Y-m-d H:i:s'). "') ORDER BY `dt_from` ASC";
                                $bd_q = sqlquery($bd_query);
                                while ($bd = sqlfetch($bd_q)) {

                                    $dt_from    = date('Y-m-d', strtotime($bd['dt_from']));
                                    $dt_to      = date('Y-m-d', strtotime($bd['dt_to']));

                                    ?>

                                    <div class="item clearfix">
                                        <input type="hidden" name="date[]" value="<?php echo $dt_from; ?> to <?php echo $dt_to; ?>" />
                                        <div class="date">
                                            <span class="from"><?php echo $dt_from; ?></span><?php if ($dt_from != $dt_to) { ?> to <span class="to"><?php echo $dt_to; ?></span><?php } ?>
                                        </div>
                                        <div class="remove"><a href="#"><i class="fa fa-times"></i></a></div>
                                        <div class="notes">
                                            <a href="#">notes</a>
                                        </div>
                                        <?php if (count($blackout_reasons) > 0) { ?>
                                            <div class="reason">
                                                <select name="reason[]" class="custom_control">
                                                    <option value="0">No reason</option>
                                                    <?php foreach ($blackout_reasons as $reason_id => $reason) { ?>
                                                        <option value="<?php echo $reason_id; ?>" <?php if ($reason_id == $bd['reason_id']) { ?>selected="selected"<?php } ?>><?php echo $reason['title']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        <?php } ?>
                                        <div class="notes_toggle">
                                            <textarea name="notes[]" style="width: 393px; height: 48px;"><?php echo stripslashes($bd['notes']); ?></textarea>
                                        </div>
                                    </div>

                                    <?php

                                }

                                ?>

                            </div>
                        </div>
                    </fieldset>

                </div>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>

        </form>

    </div>

    <a name="ical"></a>

    <div class="container legacy legacy">

        <header>
            <h2>iCal</h2>
        </header>

        <section>

            <div class="ical_code">
                <p>Copy & paste the URL below into your calendar program to display bookings for this calendar.</p>
                <input type="text" value="<?php echo WWW_ROOT . $_uccms_ecomm->storePath(); ?>/ical/calendar/?calendar_id=<?php echo $calendar['id']; ?>&key=<?php echo md5($bigtree['config']['domain'] . $calendar['id']); ?>" onclick="$(this).select();" />
            </div>

        </section>

    </div>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>