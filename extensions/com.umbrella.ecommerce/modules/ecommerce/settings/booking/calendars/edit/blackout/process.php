<?php

//echo print_r($_POST);
//exit;

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['calendar']['id'];

    // HAVE CALENDAR ID
    if ($id) {

        /*
        if (is_array($_POST['calendar']['disable_select_days'])) {
            $disable_select_days = implode(',', $_POST['calendar']['disable_select_days']);
        } else {
            $disable_select_days = '';
        }

        // DB COLUMNS
        $columns = array(
            'disable_select_days' => $disable_select_days
        );

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendars']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$id. ")";
        sqlquery($query);
        */

        // CURRENT BLACKOUTS
        $cba = array();

        // GET CURRENT BLACKOUT DATES
        $cb_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_blackouts']. "` WHERE (`calendar_id`=" .$id. ")";
        $cb_q = sqlquery($cb_query);
        while ($cb = sqlfetch($cb_q)) {
            $hash = md5(date('Y-m-d', strtotime($cb['dt_from'])). '|' .date('Y-m-d', strtotime($cb['dt_to'])));
            $cba[$hash] = $cb;
        }

        // BLACKOUT DATES AREAY
        $bda = array();

        // HAVE BLACKOUT DATES SPECIFIED
        if (count($_POST['date']) > 0) {

            // LOOP
            foreach ($_POST['date'] as $di => $date) {

                // HAVE DATE
                if ($date) {

                    $dp = explode(' to ', $date);

                    $hash = md5($dp[0]. '|' .$dp[1]);

                    $bdda = array(
                        'from'      => $dp[0],
                        'to'        => $dp[1],
                        'reason'    => $_POST['reason'][$di],
                        'notes'     => $_POST['notes'][$di],
                    );

                    $bda[$hash] = $bdda;

                }

            }

        }

        //echo print_r($bda);
        //exit;

        // HAVE BLACKOUT DATES
        if (count($bda) > 0) {

            // LOOP
            foreach ($bda as $hash => $bd) {

                // ALREADY IN CURRENT BLACKOUT DATES
                if ($cba[$hash]) {

                    $cbd = $cba[$hash];

                    // UPDATE
                    $columns = array(
                        'reason_id'     => $bd['reason'],
                        'notes'         => $bd['notes'],
                    );

                    // UPDATE RECORD
                    $bd_query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendar_blackouts']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$cbd['id']. ")";
                    sqlquery($bd_query);

                    unset($cba[$hash]);

                // NOT IN CURRENT BLACKOUT DATES
                } else {

                    $columns = array(
                        'calendar_id'   => $id,
                        'dt_from'       => date('Y-m-d H:i:s', strtotime($bd['from'])),
                        'dt_to'         => date('Y-m-d 23:59:59', strtotime($bd['to'])),
                        'reason_id'     => $bd['reason'],
                        'notes'         => $bd['notes'],
                    );

                    // CREATE RECORD
                    $bd_query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_calendar_blackouts']. "` SET " .$_uccms_ecomm->createSet($columns);
                    sqlquery($bd_query);

                }

            }

        }

        // HAVE LEFTOVERS
        if (count($cba) > 0) {

            // LOOP
            foreach ($cba as $bd) {

                // DELETE RECORD
                $delete_query = "DELETE FROM `" .$_uccms_ecomm->tables['booking_calendar_blackouts']. "` WHERE (`id`=" .$bd['id']. ")";
                sqlquery($delete_query);

            }

        }

        $admin->growl('Calendar', 'Calendar blackout info saved!');

    } else {
        $admin->growl('Calendar', 'Calendar ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'settings/booking/calendars/edit/?id=' .$id. '#blackout');

?>