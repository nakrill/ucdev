<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['calendar']['id'];

    // USE calendar TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['calendar']['slug']) {
        $_POST['calendar']['slug'] = $_POST['calendar']['title'];
    }

    if (is_array($_POST['calendar']['disable_select_days'])) {
        $disable_select_days = implode(',', $_POST['calendar']['disable_select_days']);
    } else {
        $disable_select_days = '';
    }

    // DB COLUMNS
    $columns = array(
        'slug'                  => $_uccms_ecomm->makeRewrite($_POST['calendar']['slug']),
        'status'                => (int)$_POST['calendar']['status'],
        'title'                 => $_POST['calendar']['title'],
        'meta_title'            => $_POST['calendar']['meta_title'],
        'meta_description'      => $_POST['calendar']['meta_description'],
        'meta_keywords'         => $_POST['calendar']['meta_keywords']
    );

    // HAVE CALENDAR ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendars']. "` SET " .$_uccms_ecomm->createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";

    // NO CALENDAR ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_calendars']. "` SET " .$_uccms_ecomm->createSet($columns). ", `dt_updated`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Calendar', 'Calendar added!');
        } else {
            $admin->growl('Calendar', 'Calendar updated!');
        }

        // EXISTING DAY START / END
        $edse = array();

        // GET EXISTING DAY START / END
        $edse_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_days']. "` WHERE (`calendar_id`=" .$id. ")";
        $edse_q = sqlquery($edse_query);
        while ($day = sqlfetch($edse_q)) {
            $edse[$day['day']] = $day;
        }

        // HAVE DAY START / END
        if (is_array($_POST['calendar']['dse'])) {

            // LOOP
            foreach ($_POST['calendar']['dse'] as $day => $hours) {

                $day = (int)$day;

                // DB COLUMNS
                $columns = array(
                    'day'       => $day,
                    'bookable'  => (int)$hours['bookable'],
                    'start'     => date('H:i', strtotime($hours['start'])),
                    'end'       => date('H:i', strtotime($hours['end']))
                );

                // ALREADY HAVE DAY RECORD
                if ($edse[$day]) {

                    $query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendar_days']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$edse[$day]['id']. ")";

                    unset($edse[$day]);

                // NO RECORD
                } else {

                    $columns['calendar_id'] = $id;

                    $query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_calendar_days']. "` SET " .uccms_createSet($columns). "";

                }

                sqlquery($query);

            }

        }

        // HAVE LEFTOVER
        if (count($edse) > 0) {

            // LOOP
            foreach ($edse as $day) {

                // UPDATE - SET NOT BOOKABLE
                $update_query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendar_days']. "` SET `bookable`=0 WHERE (`id`=" .$day['id']. ")";
                sqlquery($update_query);

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Calendar', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT. 'settings/booking/calendars/edit/?id=' .$id);

?>