<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'status' => 9
    );

    // UPDATE ITEM
    $query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendars']. "` SET " .$_uccms_ecomm->createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Calendar', 'Calendar deleted.');

        // UPDATE ITEMS
        $item_query = "UPDATE `" .$_uccms_ecomm->tables['items']. "` SET `booking_calendar_id`=0 WHERE (`booking_calendar_id`=" .$id. ")";
        sqlquery($item_query);

    // QUERY FAILED
    } else {
        $admin->growl('Delete Calendar', 'Failed to delete calendar.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Calendar', 'No calendar specified.');
}

BigTree::redirect(MODULE_ROOT.'settings/booking/calendars/');

?>