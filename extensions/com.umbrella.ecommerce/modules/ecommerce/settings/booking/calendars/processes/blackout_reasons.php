<?php


// FORM SUBMITTED
if (is_array($_POST)) {

    // NEW BLACKOUT REASON
    $new_reason['title'] = $_POST['title'][0];
    unset($_POST['title'][0]);

    // BLACKOUT REASON ARRAY
    $bra = array();

    // TOP REASON ID
    $top_reason_id = 1;

    // BLACKOUT REASONS SPECIFIED
    if (count($_POST['title']) > 0) {

        // LOOP
        foreach ($_POST['title'] as $reason_id => $reason_title) {
            $bra[$reason_id] = array(
                'title' => $reason_title,
                'color' => $_POST['color'][$reason_id]
            );
            if ($reason_id > $top_reason_id) $top_reason_id = $reason_id;
        }

    // NO REASONS SPECIFIED
    } else {

        // USE DEFAULTS
        foreach ($_uccms_ecomm->blackoutReasons() as $reason_id => $reason) {
            $bra[$reason_id] = $reason;
            if ($reason_id > $top_reason_id) $top_reason_id = $reason_id;
        }

    }

    // HAVE NEW BLACKOUT REASON
    if ($new_reason['title']) {
        $bra[$top_reason_id+1] = array(
            'title' => $new_reason['title']
        );
    }

    // SAVE BLACKOUT REASONS
    $_uccms_ecomm->setSetting('blackout_reasons', $bra);

    $admin->growl('Blackout Reasons', 'Saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/booking/calendars/#blackout-reasons');

?>