<style type="text/css">

    #calendars .calendar_title {
        width: 500px;
    }
    #calendars .calendar_items {
        width: 330px;
    }
    #calendars .calendar_edit {
        width: 55px;
    }
    #calendars .calendar_delete {
        width: 55px;
    }

</style>

<form>

<div class="search_paging contain">
    <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
    <span class="form_search_icon"></span>
    <nav id="view_paging" class="view_paging"></nav>
</div>

<div id="calendars" class="table">
    <summary>
        <h2>Calendars</h2>
        <a class="add_resource add" href="./edit/"><span></span>Add Calendar</a>
    </summary>
    <header style="clear: both;">
        <span class="calendar_title">Name</span>
        <span class="calendar_items">Items</span>
        <span class="calendar_edit">Edit</span>
        <span class="calendar_delete">Delete</span>
    </header>
    <ul id="results" class="items">
        <? include(EXTENSION_ROOT. 'ajax/admin/settings/booking/calendars/get-page.php'); ?>
    </ul>
</div>

</form>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/settings/booking/calendars/get-page/?page=1&query=" +escape($("#query").val()));
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/settings/booking/calendars/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&query=" +escape($("#query").val()));
        return false;
    });
</script>


<script type="text/javascript">

    $(document).ready(function() {

        // BLACKOUT REASONS - SORTABLE
        $('#blackout_reasons table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // BLACKOUT REASONS - ADD
        $('#blackout_reasons .add').click(function(e) {
            e.preventDefault();
            $('#add_blackout_reason').toggle();
            $('#add_blackout_reason input[name="title[0]"]').focus();
        });

        // BLACKOUT REASONS - REMOVE
        $('#blackout_reasons .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

    });

</script>

<a name="blackout-reasons"></a>

<div id="blackout_reasons" class="container legacy legacy">

    <form action="./processes/blackout_reasons/" method="post" enctype="multipart/form-data">

    <div class="table" style="margin: 0px; border: 0px none;">

        <summary>
            <h2>Blackout Reasons</h2>
            <a class="add_resource add" href="#"><span></span>Add</a>
        </summary>

        <div id="ui-sortable-placeholder" class="ui-sortable-placeholder" style="display: none; height: 30px; border: 1px dashed #ccc;"></div>

        <div id="add_blackout_reason" style="display: none; padding: 5px 0 10px 0;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px; border: 0px; border-bottom: 1px solid #ccc;">
                <tr>
                    <td>
                        <input type="text" name="title[0]" value="" placeholder="Title" style="width: 300px;" />
                    </td>
                    <td>
                        <input type="text" name="color[0]" value="" placeholder="Color" style="width: 300px;" />
                    </td>
                    <td style="text-align: right;">
                        <input type="submit" value="Add" class="blue" style="height: auto; padding: 7px 10px;" />
                    </td>
                </tr>
            </table>
        </div>

        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin: 0px; border: 0px none;">
            <?php foreach ($_uccms_ecomm->blackoutReasons() as $reason_id => $reason) { ?>
                <tr id="pg-<?=$reason_id?>" class="item">
                    <? /*<td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>*/ ?>
                    <td style="padding-right: 0px;" width="60%"><input type="text" name="title[<?=$reason_id?>]" value="<?=$reason['title']?>" placeholder="Title" style="width: 300px;" /></td>
                    <td style="padding-right: 0px;" width="40%"><input type="text" name="color[<?php echo $reason_id; ?>]" value="<?php echo $reason['color']; ?>" placeholder="Color" style="width: 60px;" /></td>
                    <td style="text-align: right;">
                        <a href="#" class="icon_delete remove" title="Delete reason"></a>
                    </td>
                </tr>
            <?php } ?>
        </table>

        <footer>
            <input class="blue" type="submit" value="Save">
        </footer>

    </div>

    </form>

</div>
