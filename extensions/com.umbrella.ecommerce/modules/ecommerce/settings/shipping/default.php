<?php

// GET SETTINGS
$settings = $_uccms_ecomm->getSettings();

// GET PAYMENT METHODS
$sma = $_uccms_ecomm->getShippingMethods();

/*
echo print_r($sma);
exit;
*/

/*
https://www.usps.com/business/web-tools-apis/price-calculators.htm
https://www.ups.com/upsdeveloperkit/downloadresource?loc=en_US
*/

?>


<style type="text/css">

    .shipping_methods .method {
        margin-bottom: 10px;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    .shipping_methods .method:last-child {
        margin-bottom: 0px;
    }

    .shipping_methods .method .top .icon {
        float: left;
        display: inline-block;
        width: 32px;
        height: 32px;
        text-align: center;
    }

    .shipping_methods .method .top .icon img {
        width: 32px;
        height: auto;
    }

    .shipping_methods .method .top p {
        float: left;
        margin: 0px;
        padding: 8px 0 0 10px;
    }

    .shipping_methods .method .top fieldset.right {
        width: auto;
        margin: 0px;
        padding: 8px 0 0 0;
    }

    .shipping_methods .method .options {
        margin-top: 10px;
        padding-top: 10px;
        border-top: 1px solid #ccc;
    }

    .shipping_methods .method .options h3 {
        cursor: pointer;
    }

    .shipping_methods .method .options h3 span {
        float: left;
        line-height: 20px;
    }

    .shipping_methods .method .options .uccms_toggle-settings {
        margin-bottom: 15px;
        padding-bottom: 15px;
        border-bottom: 1px solid #ccc;
    }

    .shipping_methods .method .options .left {
        margin-right: 0px
    }

    .shipping_methods .method .options textarea {
        height: 47px;
    }

    .shipping_methods .method .options .service_title {
        width: 545px;
    }
    .shipping_methods .method .options .service_markup {
        width: 100px;
    }
    .shipping_methods .method .options .service_status {
        width: 100px;
    }
    .shipping_methods .method .options .service_edit {
        width: 55px;
    }
    .shipping_methods .method .options .service_delete {
        width: 55px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // SHIPSTATION ENABLED
        $('#check_use_shipstation').change(function(e) {
            if ($(this).prop('checked')) {
                $('#settings_default').hide();
                $('#settings_shipstation').show();
            } else {
                $('#settings_shipstation').hide();
                $('#settings_default').show();
            }
        });

        // SHIPPING METHOD ENABLE TOGGLE
        $('.shipping_methods .method .top .sm_enable').change(function(e) {
            var what = $(this).attr('id').replace('sm_', '');
            if ($(this).prop('checked')) {
                $('.shipping_methods .method.' +what+ ' .options').show();
            } else {
                $('.shipping_methods .method.' +what+ ' .options').hide();
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy legacy">

    <header>
        <h2>Shipping Settings</h2>
    </header>

    <section>

        <div class="left last">

            <h3>Shipping From</h3>

            <fieldset>
                <label>First Name</label>
                <input type="text" name="setting[shipping_from_firstname]" value="<?php echo $settings['shipping_from_firstname']; ?>" />
            </fieldset>

            <fieldset>
                <label>Last Name</label>
                <input type="text" name="setting[shipping_from_lastname]" value="<?php echo $settings['shipping_from_lastname']; ?>" />
            </fieldset>

            <fieldset>
                <label>Company Name</label>
                <input type="text" name="setting[shipping_from_company]" value="<?php echo $settings['shipping_from_company']; ?>" />
            </fieldset>

            <fieldset>
                <label>Address</label>
                <input type="text" name="setting[shipping_from_address1]" value="<?php echo $settings['shipping_from_address1']; ?>" />
            </fieldset>

            <fieldset>
                <label>Address (Apt, Bldg)</label>
                <input type="text" name="setting[shipping_from_address2]" value="<?php echo $settings['shipping_from_address2']; ?>" />
            </fieldset>

            <fieldset>
                <label>City</label>
                <input type="text" name="setting[shipping_from_city]" value="<?php echo $settings['shipping_from_city']; ?>" />
            </fieldset>

            <fieldset>
                <label>State</label>
                <select name="setting[shipping_from_state]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?php echo $state_code; ?>" <?php if ($state_code == $settings['shipping_from_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset>
                <label>Zip</label>
                <input type="text" name="setting[shipping_from_zip]" value="<?php echo $settings['shipping_from_zip']; ?>" />
            </fieldset>

        </div>

        <div class="right last">

            <fieldset>
                <input type="checkbox" name="setting[shipping_free_global]" value="1" <?php if ($settings['shipping_free_global']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Global free shipping <small>Free shipping on everything in your store.</small></label>
            </fieldset>

            <fieldset>
                <input id="check_use_shipstation" type="checkbox" name="setting[shipstation_enabled]" value="1" <?php if ($settings['shipstation_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Use ShipStation</label>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

</form>

<a name="methods"></a>

<div id="settings_default" style="<?php if ($settings['shipstation_enabled']) { ?>display: none;<?php } ?>">

    <form enctype="multipart/form-data" action="./process-methods/" method="post">

    <div class="container legacy legacy">

        <header>
            <h2>Shipping Methods</h2>
        </header>

        <section class="shipping_methods">

            <?php foreach ($_uccms_ecomm->allShippingMethods() as $sm_id => $sm) { ?>

                <a name="method-<?=$sm_id?>"></a>
                <div class="method <?=$sm_id?>">
                    <div class="contain top">
                        <span class="icon"><img src="<?=ADMIN_ROOT?>images/modules/ecommerce/shipping/logos/<?=$sm_id?>.png" alt="" /></span>
                        <p><?=$sm['title']?></p>
                        <fieldset class="right">
                            <input id="sm_<?=$sm_id?>" type="checkbox" name="sm[<?=$sm_id?>]" value="1" <?php if ($sma[$sm_id]['active']) { ?>checked="checked"<?php } ?> class="sm_enable" />
                            <label class="for_checkbox">Enabled</label>
                        </fieldset>
                    </div>
                    <div class="options" style="<?php if (!$sma[$sm_id]['active']) { ?>display: none;<?php } ?>">

                        <?php if (count((array)$sm['settings']) > 0) { ?>

                            <div class="contain">
                                <h3 class="uccms_toggle" data-what="settings"><span>Settings</span><span class="icon_small icon_small_caret_right"></span></h3>
                                <div class="contain uccms_toggle-settings" style="display: none;">
                                    <div class="left last">

                                        <?php foreach ($sm['settings'] as $setting_key => $setting) { ?>

                                            <fieldset>
                                                <?php if ($setting['field_type'] != 'checkbox') { ?><label><?php echo $setting['title']; ?></label><?php } ?>
                                                <?php

                                                // FIELD TYPE
                                                switch ($setting['field_type']) {
                                                    case 'text':
                                                        if (!$sma[$sm_id]['settings'][$setting_key]) $sma[$sm_id]['settings'][$setting_key] = stripslashes($setting['default_value']);
                                                        ?>
                                                        <input type="text" name="setting[<?=$sm_id?>][<?=$setting_key?>]" value="<?php echo stripslashes($sma[$sm_id]['settings'][$setting_key]); ?>" />
                                                        <?php
                                                        break;
                                                    case 'textarea':
                                                        if (!$sma[$sm_id]['settings'][$setting_key]) $sma[$sm_id]['settings'][$setting_key] = stripslashes($setting['default_value']);
                                                        ?>
                                                        <textarea name="setting[<?=$sm_id?>][<?=$setting_key?>]"><?php echo stripslashes($sma[$sm_id]['settings'][$setting_key]); ?></textarea>
                                                        <?php
                                                        break;
                                                    case 'checkbox':
                                                        if (count($setting['field_options']) > 0) {
                                                            foreach ($setting['field_options'] as $option_key => $option_title) {
                                                                ?>
                                                                <input type="checkbox" name="setting[<?=$sm_id?>][<?=$setting_key?>][<?=$option_key?>]" value="1" <?php if ($sma[$sm_id]['settings'][$setting_key][$option_key]) { ?>checked="checked"<?php } ?> />
                                                                <label class="for_checkbox"><?php echo $option_title; ?></label>
                                                                <br />
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <input type="checkbox" name="setting[<?=$sm_id?>][<?=$setting_key?>]" value="1" <?php if ($sma[$sm_id]['settings'][$setting_key]) { ?>checked="checked"<?php } ?> />
                                                            <label class="for_checkbox"><?php echo $setting['title']; ?></label>
                                                            <?php
                                                        }
                                                        break;
                                                    case 'select':
                                                        if (count($setting['field_options']) > 0) {
                                                            ?>
                                                            <select name="setting[<?=$sm_id?>][<?=$setting_key?>]">
                                                                <?php foreach ($setting['field_options'] as $option_key => $option_title) { ?>
                                                                    <option value="<?php echo $option_key; ?>" <?php if ($sma[$sm_id]['settings'][$setting_key] == $option_key) { ?>selected="selected"<?php } ?>><?php echo $option_title; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            <?php
                                                        }
                                                        break;
                                                }

                                                // NOTE
                                                if ($setting['note']) {
                                                    ?>
                                                    <div class="note" style="color: #999;">(<?php echo stripslashes($setting['note']); ?>)</div>
                                                    <?php
                                                }

                                                ?>
                                            </fieldset>

                                        <?php } ?>

                                    </div>
                                    <div class="right last">
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                        <?php if ($sm_id != 'pickup') { ?>

                            <div class="contain">
                                <h3 class="uccms_toggle" data-what="services"><span>Services</span><span class="icon_small icon_small_caret_down"></span></h3>
                                <div class="contain uccms_toggle-services">

                                    <?php

                                    // TABLE / FLAT RATE
                                    if ($sm_id == 'table') {

                                        // GET RATES FROM DB
                                        $rate_query = "SELECT * FROM `" .$_uccms_ecomm->tables['shipping_table']. "` ORDER BY `sort` ASC";
                                        $rate_q = sqlquery($rate_query);

                                        // NUMBER OF SAVED RATES
                                        $rate_n = sqlrows($rate_q);

                                        ?>

                                        <div class="table rates">
                                            <summary>
                                                <a class="add_resource add" href="./table/"><span></span>Add Rate</a>
                                            </summary>

                                            <?php

                                            // HAVE SAVED RATES
                                            if ($rate_n > 0) {

                                                ?>
                                                <header>
                                                    <span class="service_title">Title</span>
                                                    <span class="service_markup">Markup</span>
                                                    <span class="service_status">Status</span>
                                                    <span class="service_edit">Edit</span>
                                                    <span class="service_delete">Delete</span>
                                                </header>
                                                <ul class="items">
                                                    <?php

                                                    // LOOP
                                                    while ($rate = sqlfetch($rate_q)) {

                                                        $title = stripslashes($rate['title']);

                                                        // MIN MAX ARRAY
                                                        $mma = array();

                                                        // HAVE MIN (OR MAX)
                                                        if (($rate['min'] > 0.00) || ($rate['max'] > 0.00)) {
                                                            switch ($rate['what']) {
                                                                case 'weight':
                                                                    $mma[] = $rate['min']. ' lbs';
                                                                    break;
                                                                case 'price':
                                                                    $mma[] = '$' .$rate['min'];
                                                                    break;
                                                                case 'items':
                                                                    $mma[] = $rate['min']. ' items';
                                                                    break;
                                                            }
                                                        }

                                                        // HAVE MAX
                                                        if ($rate['max'] > 0.00) {
                                                            switch ($rate['what']) {
                                                                case 'weight':
                                                                    $mma[] = $rate['max']. ' lbs';
                                                                    break;
                                                                case 'price':
                                                                    $mma[] = '$' .$rate['max'];
                                                                    break;
                                                                case 'items':
                                                                    $mma[] = $rate['max']. ' items';
                                                                    break;
                                                            }
                                                        }

                                                        // HAVE MIN AND/OR MAX
                                                        if (count($mma) > 0) {
                                                            $title .= ' (' .implode(' - ', $mma). ')';
                                                        }

                                                        ?>

                                                        <li class="item">
                                                            <section class="service_title">
                                                                <?php echo $title; ?>
                                                            </section>
                                                            <section class="service_markup">
                                                                <?php if ($rate['markup_type'] == 'amount') { echo '$'; } echo $rate['markup']; if ($rate['markup_type'] == 'percent') { echo '%'; } ?>
                                                            </section>
                                                            <section class="service_status status_<?php if ($rate['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                                                                <?php if ($rate['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                                                            </section>
                                                            <section class="service_edit">
                                                                <a class="icon_edit" title="Edit Service" href="./table/?id=<?php echo $rate['id']; ?>"></a>
                                                            </section>
                                                            <section class="service_delete">
                                                                <a href="./table/delete/?id=<?php echo $rate['id']; ?>" class="icon_delete" title="Delete Rate" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                                                            </section>
                                                        </li>

                                                        <?php

                                                    }

                                                    ?>
                                                </ul>

                                                <?php

                                            }

                                            ?>

                                        </div>

                                        <?php

                                    // EVERYTHING ELSE
                                    } else {

                                        // GET SERVICES FROM DB
                                        $service_query = "SELECT * FROM `" .$_uccms_ecomm->tables['shipping_services']. "` WHERE (`method`='" .$sm_id. "')";
                                        $service_q = sqlquery($service_query);

                                        // NUMBER OF SAVED SERVICES
                                        $service_n = sqlrows($service_q);

                                        ?>

                                        <div class="table services">
                                            <summary>
                                                <a class="add_resource add" href="./service/?method=<?php echo $sm_id; ?>"><span></span>Add Service</a>
                                            </summary>

                                            <?php

                                            // HAVE SAVED SERVICES
                                            if ($service_n > 0) {

                                                ?>
                                                <header>
                                                    <span class="service_title">Title</span>
                                                    <span class="service_markup">Markup</span>
                                                    <span class="service_status">Status</span>
                                                    <span class="service_edit">Edit</span>
                                                    <span class="service_delete">Delete</span>
                                                </header>
                                                <ul class="items">
                                                    <?php

                                                    // LOOP
                                                    while ($service = sqlfetch($service_q)) {

                                                        ?>

                                                        <li class="item">
                                                            <section class="service_title">
                                                                <?php echo stripslashes($service['title']); ?>
                                                            </section>
                                                            <section class="service_markup">
                                                                <?php if ($service['markup_type'] == 'amount') { echo '$'; } echo $service['markup']; if ($service['markup_type'] == 'percent') { echo '%'; } ?>
                                                            </section>
                                                            <section class="service_status status_<?php if ($service['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                                                                <?php if ($service['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                                                            </section>
                                                            <section class="service_edit">
                                                                <a class="icon_edit" title="Edit Service" href="./service/?id=<?php echo $service['id']; ?>"></a>
                                                            </section>
                                                            <section class="service_delete">
                                                                <a href="./service/delete/?id=<?php echo $service['id']; ?>" class="icon_delete" title="Delete Service" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                                                            </section>
                                                        </li>

                                                        <?php

                                                    }

                                                    ?>
                                                </ul>

                                                <?php

                                            }

                                            ?>

                                        </div>

                                        <?php

                                    }

                                    ?>

                                </div>
                            </div>

                        <?php } ?>

                    </div>
                </div>

            <?php } ?>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>

    </div>

    </form>

</div>

<div id="settings_shipstation" style="<?php if (!$settings['shipstation_enabled']) { ?>display: none;<?php } ?>">

    <a name="shipstation"></a>

    <h2>ShipStation</h2>

    <form enctype="multipart/form-data" action="./process-shipstation-api/" method="post">

    <div>
        <table>
            <tr>
                <td nowrap="nowrap">API Key: </td>
                <td width="100%;"><input type="text" name="setting[shipstation_api_key]" value="<?php echo $settings['shipstation_api_key']; ?>" style="width: 300px;" /></td>
            </tr>
            <tr>
                <td nowrap="nowrap">API Secret: </td>
                <td width="100%;"><input type="text" name="setting[shipstation_api_secret]" value="<?php echo $settings['shipstation_api_secret']; ?>" style="width: 300px;" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input class="blue" type="submit" value="Save" />
                </td>
            </tr>
        </table>
    </div>

    </form>

    <form enctype="multipart/form-data" action="./process-shipstation/" method="post">

    <?php

    // HAVE SHIPSTATION API INFO
    if (($settings['shipstation_api_key']) && ($settings['shipstation_api_secret'])) {

        // LOAD REQUIRED LIBRARIES
        require_once(SERVER_ROOT. 'uccms/includes/libs/unirest/Unirest.php');
        require_once(EXTENSION_ROOT. 'classes/shipstation.php');

        // DISABLE UNIREST SSL VALIDATION
        Unirest::verifyPeer(false);

        // INITIALIZE SHIPSTATION
        $shipStation = new Shipstation();
        $shipStation->setSsApiKey($settings['shipstation_api_key']); // make setting in admin
        $shipStation->setSsApiSecret($settings['shipstation_api_secret']);

        ?>

        <div class="container legacy legacy">

            <header>
                <h2>Shipping Methods</h2>
            </header>

            <section class="shipping_methods">

                <?php

                $have_services = false;

                // GET CARRIERS
                $carriers = $shipStation->getCarriers();

                // REQUEST SUCCESSFUL
                if ($carriers['success']) {

                    // HAVE CARRIERS
                    if (count($carriers['response']) > 0) {

                        // GET SHIPSTATION CARRIERS & SERVICES SETTINGS
                        $csa = $_uccms_ecomm->shipstationCarriersServices();

                        // LOOP
                        foreach ($carriers['response'] as $carrier) {

                            ?>

                            <input type="hidden" name="sm[<?=$carrier['code']?>][title]" value="<?=$carrier['name']?>" />

                            <a name="method-<?=$carrier['code']?>"></a>
                            <div class="method <?=$carrier['code']?>">
                                <div class="contain top">
                                    <span class="icon"><img src="/extensions/<?=$_uccms_ecomm->Extension?>/images/admin/shipping/logos/<?=$carrier['code']?>.png" alt="" /></span>
                                    <p><?=$carrier['name']?></p>
                                    <fieldset class="right">
                                        <input id="sm_<?=$carrier['code']?>" type="checkbox" name="sm[<?=$carrier['code']?>][active]" value="1" <?php if ($sma[$carrier['code']]['active']) { ?>checked="checked"<?php } ?> class="sm_enable" />
                                        <label class="for_checkbox">Enabled</label>
                                    </fieldset>
                                </div>
                                <div class="options" style="<?php if (!$sma[$carrier['code']]['active']) { ?>display: none;<?php } ?>">

                                    <?php

                                    // GET SERVICES
                                    $services = $shipStation->carrierServices($carrier['code']);

                                    // REQUEST SUCCESSFUL
                                    if ($services['success']) {

                                        $have_services = true;

                                        // HAVE SERVICES
                                        if (count($services['response']) > 0) {

                                            // LOOP
                                            foreach ($services['response'] as $service) {
                                                if ($service['international']) $service['code'] = $service['code']. '_int';
                                                ?>
                                                <input type="hidden" name="sm[<?=$carrier['code']?>][services][<?php echo $service['code']; ?>][title_default]" value="<?php echo $service['name']; ?>" />
                                                <table style="margin: 0px; border: 0px none;">
                                                    <tr>
                                                        <td style="padding-right: 0px;"><input type="checkbox" name="sm[<?=$carrier['code']?>][services][<?php echo $service['code']; ?>][active]" value="1" <?php if ($csa[$carrier['code']]['services'][$service['code']]['active']) { ?>checked="checked"<?php } ?> /></td>
                                                        <td width="30%"><?php echo $service['name']; ?></td>
                                                        <td width="70%">Display as: <input type="text" name="sm[<?=$carrier['code']?>][services][<?php echo $service['code']; ?>][title]" value="<?php echo $csa[$carrier['code']]['services'][$service['code']]['title']; ?>" placeholder="<?php echo $service['name']; ?>" style="display: inline; width: 350px;" /></td>
                                                    </tr>
                                                </table>
                                                <?php
                                            }

                                        // NO SERVICES
                                        } else {
                                            ?>
                                            No services available.
                                            <?php
                                        }

                                    // ERROR
                                    } else {
                                        echo $services['error']['message'];
                                    }

                                    ?>

                                </div>
                            </div>

                            <?php

                        }

                        // HAVE AT LEAST ONE SERVICE
                        if ($have_services) {
                            ?>
                            <div>
                                <input class="blue" type="submit" value="Save" />
                            </div>
                            <?php
                        }

                    // NO CARRIERS
                    } else {
                        ?>
                        At least one carrier must be set up in your <a href="https://ss.shipstation.com/" target="_blank">ShipStation account</a>.
                        <?php
                    }

                // ERROR
                } else {
                    echo $carriers['error']['message'];
                }

                ?>

            </section>

        </div>

        <?php

    }

    ?>

    </form>

</div>