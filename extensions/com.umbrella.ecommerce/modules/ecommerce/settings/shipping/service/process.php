<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['service']['id'];

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['service']['active'],
        'method'        => $_POST['service']['method'],
        'service'       => $_POST['service']['service'],
        'title'         => $_POST['service']['title'],
        'markup_type'   => $_POST['service']['markup_type'],
        'markup'        => $_POST['service']['markup'],
        'country'       => $_POST['service']['country'],
        'state'         => $_POST['service']['state'],
        'weight_min'    => $_POST['service']['weight_min'],
        'weight_max'    => $_POST['service']['weight_max'],
        'updated_by'    => $_uccms_ecomm->adminID()
    );

    // HAVE SERVICE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['shipping_services']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO SERVICE ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['shipping_services']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Shipping Service Settings', 'Settings saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Shipping Service Settings', 'Failed to save settings.');
    }

}

BigTree::redirect(MODULE_ROOT.'settings/shipping/service/?id=' .$id);

?>