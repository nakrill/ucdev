<?php

// CLEAN UP
$method = trim($_REQUEST['method']);
$id     = (int)$_REQUEST['id'];

// GET AVAILABLE SHIPPING METHODS
$asma = $_uccms_ecomm->allShippingMethods();

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $service_query = "SELECT * FROM `" .$_uccms_ecomm->tables['shipping_services']. "` WHERE (`id`=" .$id. ")";
    $service_q = sqlquery($service_query);
    $service = sqlfetch($service_q);

    $method = $service['method'];

}

// IS AVAILABLE SHIPPING METHOD
if (($method) && (is_array($asma[$method]))) {

    // SHIPPING METHOD DOES NOT HAS SERVICES
    if (!is_array($asma[$method]['services'])) {
        $admin->growl('Shipping Service Settings', 'Shipping method has no services available.');
        BigTree::redirect(MODULE_ROOT.'settings/shipping/#methods');
    }

// IS NOT AVAILABLE SHIPPING METHOD
} else {
    $admin->growl('Shipping Service Settings', 'Shipping method not specified / available.');
    BigTree::redirect(MODULE_ROOT.'settings/shipping/#methods');
}

?>


<style type="text/css">

    fieldset.method .icon {
        float: left;
        display: inline-block;
        width: 32px;
        height: 32px;
        text-align: center;
    }

    fieldset.method .icon img {
        width: 32px;
        height: auto;
    }

    fieldset.method p {
        float: left;
        margin: 0px;
        padding: 8px 0 0 10px;
    }

</style>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="service[id]" value="<?php echo $service['id']; ?>" />
<input type="hidden" name="service[method]" value="<?php echo $method; ?>" />

<div class="container legacy legacy">

    <header>
        <h2>Shipping Service Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset class="method">
                <span class="icon"><img src="<?=ADMIN_ROOT?>images/modules/ecommerce/shipping/logos/<?=$method?>.png" alt="" /></span>
                <p><?php echo $asma[$method]['title']; ?></p>
            </fieldset>

            <fieldset>
                <label>Service</label>
                <select name="service[service]">
                    <?php foreach ($asma[$method]['services'] as $key => $serv) { ?>
                        <option value="<?php echo $key; ?>" <?php if ($key == stripslashes($service['service'])) { ?>selected="selected"<?php } ?>><?php echo $serv['title']; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset>
                <label>Title</label>
                <input type="text" name="service[title]" value="<?php echo stripslashes($service['title']); ?>" />
            </fieldset>

            <fieldset>
                <label>Markup</label>
                <select name="service[markup_type]">
                    <option value="amount" <?php if ($service['markup_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                    <option value="percent" <?php if ($service['markup_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                </select>&nbsp;<input type="text" name="service[markup]" value="<?php echo $service['markup']; ?>" class="smaller_60" />
            </fieldset>

            <fieldset>
                <input type="checkbox" name="service[active]" value="1" <?php if ($service['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

        </div>

        <div class="right last">

            <fieldset>
                <label>Country</label>
                <select name="service[country]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$CountryList as $country_name) { ?>
                        <option value="<?php echo $country_name; ?>" <?php if ($country_name == stripslashes($service['country'])) { ?>selected="selected"<?php } ?>><?php echo $country_name; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset>
                <label>State</label>
                <select name="service[state]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?php echo $state_code; ?>" <?php if ($state_code == stripslashes($service['state'])) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                    <?php } ?>
                </select>
                <label style="padding-top: 8px;"><small>Leave blank for whole country.</small></label>
            </fieldset>

            <div class="contain">

                <fieldset class="left last inner_quarter">
                    <label>Min Weight</label>
                    <input type="text" name="service[weight_min]" value="<?php echo stripslashes($service['weight_min']); ?>" class="smaller_60" />
                    <label><small>Leave blank for no limit.</small></label>
                </fieldset>

                <fieldset class="right last inner_quarter">
                    <label>Max Weight</label>
                    <input type="text" name="service[weight_max]" value="<?php echo stripslashes($service['weight_max']); ?>" class="smaller_60" />
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../#method-<?php echo $method; ?>">&laquo; Back</a>
    </footer>

</div>

</form>