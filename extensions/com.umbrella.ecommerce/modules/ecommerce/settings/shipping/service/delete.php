<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['shipping_services']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Service', 'Service deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Service', 'Failed to delete service.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Service', 'No service specified.');
}

BigTree::redirect(MODULE_ROOT.'settings/shipping/#methods');

?>