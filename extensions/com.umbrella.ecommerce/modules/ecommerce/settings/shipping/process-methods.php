<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SET ALL TO INACTIVE QUICKLY
    $query = "UPDATE `" .$_uccms_ecomm->tables['shipping_methods']. "` SET `active`=0";
    sqlquery($query);

    // HAVE ONES TO UPDATE
    if (count($_POST['sm']) > 0) {

        // LOOP
        foreach ($_POST['sm'] as $sm => $active) {

            // DB COLUMNS
            $columns = array(
                'id'                => $sm,
                'active'            => (int)$active,
                'settings'          => json_encode($_POST['setting'][$sm]),
                'updated_by'        => $_uccms_ecomm->adminID()
            );

            $fields = $_uccms_ecomm->createSet($columns);

            // ADD / UPDATE
            $query = "
            INSERT INTO `" .$_uccms_ecomm->tables['shipping_methods']. "`
            SET " .$fields. ", `updated_dt`=NOW()
            ON DUPLICATE KEY UPDATE " .$fields. ", `updated_dt`=NOW()
            ";
            sqlquery($query);

        }

    }

    $admin->growl('Shipping Method Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/shipping/#methods');

?>