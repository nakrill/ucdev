<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SETTINGS ARRAY
    $seta = array(
        'shipping_from_firstname',
        'shipping_from_lastname',
        'shipping_from_company',
        'shipping_from_address1',
        'shipping_from_address2',
        'shipping_from_city',
        'shipping_from_state',
        'shipping_from_zip',
        'shipping_free_global',
        'shipstation_enabled'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_ecomm->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('Shipping Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/shipping/');

?>