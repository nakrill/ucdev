<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['shipping_table']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Rate', 'Rate deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Rate', 'Failed to delete rate.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Rate', 'No rate specified.');
}

BigTree::redirect(MODULE_ROOT.'settings/shipping/#method-table');

?>