<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['rate']['id'];

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['rate']['active'],
        'what'          => $_POST['rate']['what'],
        'title'         => $_POST['rate']['title'],
        'markup_type'   => $_POST['rate']['markup_type'],
        'markup'        => $_POST['rate']['markup'],
        'min'           => $_POST['rate']['min'],
        'max'           => $_POST['rate']['max'],
        'updated_by'    => $_uccms_ecomm->adminID()
    );

    // HAVE RATE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['shipping_table']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO RATE ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['shipping_table']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Shipping Table Settings', 'Settings saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Shipping Table Settings', 'Failed to save settings.');
    }

}

BigTree::redirect(MODULE_ROOT.'settings/shipping/table/?id=' .$id);

?>