<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $rate_query = "SELECT * FROM `" .$_uccms_ecomm->tables['shipping_table']. "` WHERE (`id`=" .$id. ")";
    $rate_q = sqlquery($rate_query);
    $rate = sqlfetch($rate_q);

// NO ID
} else {
    $rate['what'] = 'weight';
}

?>


<style type="text/css">

    fieldset.method .icon {
        float: left;
        display: inline-block;
        width: 32px;
        height: 32px;
        text-align: center;
    }

    fieldset.method .icon img {
        width: 32px;
        height: auto;
    }

    fieldset.method p {
        float: left;
        margin: 0px;
        padding: 8px 0 0 10px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // BASED ON TOGGLE
        $('#based_on').change(function(e) {
            $('.right .min_max .lbl').hide();
            $('.right .min_max .lbl.' +$(this).val()).show();
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="rate[id]" value="<?php echo $rate['id']; ?>" />

<div class="container legacy legacy">

    <header>
        <h2>Shipping Table Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset class="method">
                <span class="icon"><img src="<?=ADMIN_ROOT?>images/modules/ecommerce/shipping/logos/table.png" alt="" /></span>
                <p>Table / Flat Rate</p>
            </fieldset>

            <fieldset>
                <label>Title</label>
                <input type="text" name="rate[title]" value="<?php echo stripslashes($rate['title']); ?>" />
            </fieldset>

            <fieldset>
                <label>Based On</label>
                <select id="based_on" name="rate[what]">
                    <option value="weight" <?php if ($rate['what'] == 'weight') { ?>selected="selected"<?php } ?>>Order Weight</option>
                    <option value="price" <?php if ($rate['what'] == 'price') { ?>selected="selected"<?php } ?>>Order Price</option>
                    <option value="quantity" <?php if ($rate['what'] == 'quantity') { ?>selected="selected"<?php } ?>>Order Quantity</option>
                </select>
            </fieldset>

            <fieldset>
                <label>Markup</label>
                <select name="rate[markup_type]">
                    <option value="amount" <?php if ($rate['markup_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                    <option value="percent" <?php if ($rate['markup_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                </select>&nbsp;<input type="text" name="rate[markup]" value="<?php echo $rate['markup']; ?>" class="smaller_60" />
            </fieldset>

            <fieldset>
                <input type="checkbox" name="rate[active]" value="1" <?php if ($rate['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

        </div>

        <div class="right last">

            <div class="contain min_max">

                <fieldset class="left last inner_quarter">
                    <label>From</label>
                    <span class="lbl price" style="<?php if ($rate['what'] != 'price') { ?>display: none;<?php } ?>">$</span><input type="text" name="rate[min]" value="<?php echo stripslashes($rate['min']); ?>" class="smaller_60" /> <span class="lbl weight" style="<?php if ($rate['what'] != 'weight') { ?>display: none;<?php } ?>">lbs</span><span class="lbl items" style="<?php if ($rate['what'] != 'items') { ?>display: none;<?php } ?>">items</span>
                    <label><small>Leave blank for no limit.</small></label>
                </fieldset>

                <fieldset class="right last inner_quarter">
                    <label>To</label>
                    <span class="lbl price" style="<?php if ($rate['what'] != 'price') { ?>display: none;<?php } ?>">$</span><input type="text" name="rate[max]" value="<?php echo stripslashes($rate['max']); ?>" class="smaller_60" /> <span class="lbl weight" style="<?php if ($rate['what'] != 'weight') { ?>display: none;<?php } ?>">lbs</span><span class="lbl items" style="<?php if ($rate['what'] != 'items') { ?>display: none;<?php } ?>">items</span>
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../#method-table">&laquo; Back</a>
    </footer>

</div>

</form>