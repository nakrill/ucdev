<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SAVE SETTING
    $_uccms_ecomm->setSetting('shipstation_carriers_services', json_encode($_POST['sm'])); // SAVE SETTING

    $admin->growl('ShipStation Services', 'Services saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/shipping/#shipstation');

?>