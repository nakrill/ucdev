<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SETTINGS ARRAY
    $seta = array(
        'shipstation_api_key',
        'shipstation_api_secret'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_ecomm->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('ShipStation Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/shipping/#shipstation');

?>