<?php

// GET SETTINGS
$settings = $_uccms_ecomm->getSettings();

?>


<style type="text/css">

    form .left .meals_hours input.time_picker {
        width: 100px !important;
    }
    form .meals_hours td {
        padding: 5px 15px;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // MEAL HOURS - SORTABLE
        $('form .meals_hours table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // MEAL HOURS - ADD
        $('form .meals_hours .add').click(function(e) {
            e.preventDefault();
            $('#add_meal_time').toggle();
        });

        // MEAL HOURS - REMOVE
        $('form .meals_hours .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

        /*
        // GRATUITY TOGGLE
        $('#gratuity_enabled').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_gratuity').show();
            } else {
                $('#toggle_gratuity').hide();
            }
        });
        */

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy legacy">

    <header>
        <h2>Catering Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset class="meals_hours">
                <div class="contain">
                    <div class="left inner_quarter last">
                        <label>Meals & Times</label>
                    </div>
                    <div class="left inner_quarter last" style="margin: 0px; text-align: right;">
                        <a href="#" class="add button_small">Add</a>
                    </div>
                </div>
                <div id="add_meal_time" style="display: none; padding: 5px 0 10px 0;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px;">
                        <tr>
                            <td>
                                <input type="text" name="meal_title[0]" value="" placeholder="Meal Title" style="width: 150px;" />
                            </td>
                            <td>
                                <?php

                                // FIELD VALUES
                                $field = array(
                                    'id'        => 'meal_time-0',
                                    'key'       => 'meal_time[0]',
                                    'value'     => date('g:i a'),
                                    'required'  => true
                                );

                                // INCLUDE FIELD
                                include(BigTree::path('admin/form-field-types/draw/time.php'));

                                ?>
                            </td>
                            <td>
                                <input type="submit" value="Add" style="height: auto; padding: 7px 10px;" />
                            </td>
                        </tr>
                    </table>
                </div>

                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-bottom: 0px;">
                    <?php foreach ($_uccms_ecomm->stc->mealTimes() as $meal_id => $meal) { ?>
                        <tr id="mt-<?=$meal_id?>" class="item">
                            <td style="padding-right: 0px;"><span class="icon_sort ui-sortable-handle"></span></td>
                            <td style="padding-right: 0px;" width="60%"><input type="text" name="meal_title[<?=$meal_id?>]" value="<?=$meal['title']?>" placeholder="Meal Title" style="width: 180px;" /></td>
                            <td style="padding-right: 0px;" width="30%">
                                <?php

                                // FIELD VALUES
                                $field = array(
                                    'id'        => 'meal_time-' .$meal_id,
                                    'key'       => 'meal_time[' .$meal_id. ']',
                                    'value'     => date('g:i a', strtotime($meal['time'])),
                                    'required'  => true
                                );

                                // INCLUDE FIELD
                                include(BigTree::path('admin/form-field-types/draw/time.php'));

                                ?>
                            </td>
                            <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </fieldset>

        </div>

        <div class="right last">

            <? /*
            <fieldset class="last">
                <input id="gratuity_enabled" type="checkbox" name="setting[gratuity_enabled]" value="1" <?php if ($settings['gratuity_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Enable gratuity</label>
            </fieldset>

            <div id="toggle_gratuity" class="contain" style="<?php if (!$settings['gratuity_enabled']) { ?>display: none;<?php } ?> padding-top: 10px;">
                <fieldset class="last">
                    %<input type="text" name="setting[gratuity_percent]" value="<?php echo $settings['gratuity_percent']; ?>" class="smaller_60" />
                    <small style="color: #999;">Default percent</small>
                </fieldset>
            </div>
            */ ?>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

</form>

<script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
    $('.time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
</script>