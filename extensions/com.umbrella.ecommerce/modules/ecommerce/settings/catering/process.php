<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // NEW MEALTIME
    $new_mealtime['title'] = $_POST['meal_title'][0];
    $new_mealtime['time'] = $_POST['meal_time'][0];
    unset($_POST['meal_title'][0]);
    unset($_POST['meal_time'][0]);

    // MEALTIME ARRAY
    $mta = array();

    // TOP MEALTIME ID
    $top_mealtime_id = 1;

    // MEALTIMES SPECIFIED
    if (count($_POST['meal_title']) > 0) {

        // LOOP
        foreach ($_POST['meal_title'] as $meal_id => $title) {
            $mta[$meal_id] = array(
                'title' => $title,
                'time'  => date('H:i:00', strtotime($_POST['meal_time'][$meal_id]))
            );
            if ($meal_id > $top_mealtime_id) $top_mealtime_id = $meal_id;
        }

    // NO MEALTIMES SPECIFIED
    } else {

        // USE DEFAULTS
        foreach ($_uccms_ecomm->stc->mealTimes() as $meal_id => $meal) {
            $mta[$meal_id] = $meal;
            if ($meal_id > $top_mealtime_id) $top_mealtime_id = $meal_id;
        }

    }

    // HAVE NEW MEAL TIME
    if ($new_mealtime['title']) {
        $mta[$top_mealtime_id+1] = array(
            'title' => $new_mealtime['title'],
            'time'  => date('H:i:00', strtotime($new_mealtime['time']))
        );
    }

    // SAVE MEALTIMES
    $_uccms_ecomm->setSetting('catering_mealtimes', $mta);

    /*
    // SETTINGS ARRAY
    $seta = array(
        'gratuity_enabled',
        'gratuity_percent'
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_ecomm->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }
    */

    $admin->growl('Catering Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/catering/');

?>