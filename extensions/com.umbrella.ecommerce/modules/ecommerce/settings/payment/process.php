<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SET ALL TO INACTIVE QUICKLY
    $query = "UPDATE `" .$_uccms_ecomm->tables['payment_methods']. "` SET `active`=0";
    sqlquery($query);

    // HAVE ONES TO UPDATE
    if (count($_POST['pm']) > 0) {

        // LOOP
        foreach ($_POST['pm'] as $pm => $active) {

            // DB COLUMNS
            $columns = array(
                'id'                => $pm,
                'active'            => (int)$active,
                'frontend'          => (int)$_POST['frontend'][$pm],
                'title'             => $_POST['title'][$pm],
                'description'       => $_POST['description'][$pm],
                'hide_cc'           => (int)$_POST['hide_cc'][$pm],
                'updated_by'        => $_uccms_ecomm->adminID()
            );

            $fields = $_uccms_ecomm->createSet($columns);

            // ADD / UPDATE
            $query = "
            INSERT INTO `" .$_uccms_ecomm->tables['payment_methods']. "`
            SET " .$fields. ", `updated_dt`=NOW()
            ON DUPLICATE KEY UPDATE " .$fields. ", `updated_dt`=NOW()
            ";
            sqlquery($query);

        }

    }

    $admin->growl('Payment Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/payment/');

?>