<?php

// GET SETTINGS
$settings = $_uccms_ecomm->getSettings();

?>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy legacy">

    <header>
        <h2>Email Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>From Name</label>
                <input type="text" name="setting[email_from_name]" value="<?php echo $settings['email_from_name']; ?>" />
            </fieldset>

            <fieldset>
                <label>From Email</label>
                <input type="text" name="setting[email_from_email]" value="<?php echo $settings['email_from_email']; ?>" />
            </fieldset>

        </div>

        <div class="right last">

            <fieldset>
                <label>Send copy of new order emails to:</label>
                <input type="text" name="setting[email_order_copy]" value="<?php echo $settings['email_order_copy']; ?>" />
                <label><small>(Seperate email addresses by comma.)</small></label>
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

<div class="container legacy legacy">

    <header>
        <h2>Email Templates</h2>
    </header>

    <section>

        V2 Build out

    </section>

</div>

</form>