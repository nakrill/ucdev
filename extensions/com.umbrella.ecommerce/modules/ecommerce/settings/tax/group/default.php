<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $group_query = "SELECT * FROM `" .$_uccms_ecomm->tables['tax_groups']. "` WHERE (`id`=" .$id. ")";
    $group_q = sqlquery($group_query);
    $group = sqlfetch($group_q);

}

?>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="group[id]" value="<?php echo $group['id']; ?>" />

<div class="container legacy legacy">

    <header>
        <h2>Tax Group Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset>
                <label>Title</label>
                <input type="text" name="group[title]" value="<?php echo stripslashes($group['title']); ?>" />
            </fieldset>

            <fieldset>
                <label>Tax</label>
                %<input type="text" name="group[tax]" value="<?php echo $group['tax']; ?>" class="smaller_60" />
            </fieldset>

            <fieldset>
                <input id="discount_global" type="checkbox" name="group[active]" value="1" <?php if ($group['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

        </div>

        <div class="right last">

            <fieldset>
                <label>Country</label>
                <select name="group[country]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$CountryList as $country_name) { ?>
                        <option value="<?php echo $country_name; ?>" <?php if ($country_name == stripslashes($group['country'])) { ?>selected="selected"<?php } ?>><?php echo $country_name; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <fieldset>
                <label>State</label>
                <select name="group[state]">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?php echo $state_code; ?>" <?php if ($state_code == stripslashes($group['state'])) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                    <?php } ?>
                </select>
                <label style="padding-top: 8px;"><small>Leave blank for whole country.</small></label>
            </fieldset>

            <div class="contain">

                <fieldset class="left last inner_quarter">
                    <label>Zip From</label>
                    <input type="text" name="group[zip_from]" value="<?php echo stripslashes($group['zip_from']); ?>" class="smaller_60" />
                    <label><small>Leave blank for whole state.</small></label>
                </fieldset>

                <fieldset class="right last inner_quarter">
                    <label>Zip To</label>
                    <input type="text" name="group[zip_to]" value="<?php echo stripslashes($group['zip_to']); ?>" class="smaller_60" />
                    <label><small>Leave blank for single zip.</small></label>
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

</div>

</form>