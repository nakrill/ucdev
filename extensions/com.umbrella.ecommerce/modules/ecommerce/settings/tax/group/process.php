<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['group']['id'];

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['group']['active'],
        'title'         => $_POST['group']['title'],
        'country'       => $_POST['group']['country'],
        'state'         => $_POST['group']['state'],
        'zip_from'      => $_POST['group']['zip_from'],
        'zip_to'        => $_POST['group']['zip_to'],
        'tax'           => $_POST['group']['tax'],
        'updated_by'    => $_uccms_ecomm->adminID()
    );

    // HAVE GROUP ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['tax_groups']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO GROUP ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['tax_groups']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Tax Group Settings', 'Settings saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Tax Group Settings', 'Failed to save settings.');
    }

}

BigTree::redirect(MODULE_ROOT.'settings/tax/group/?id=' .$id);

?>