<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // SETTINGS ARRAY
    $seta = array(
        'tax_global',
        'tax_global_percent',
        'tax_billing',
    );

    // LOOP THROUGH SETTINGS
    foreach ($seta as $setting) {
        $_uccms_ecomm->setSetting($setting, $_POST['setting'][$setting]); // SAVE SETTING
    }

    $admin->growl('Tax Settings', 'Settings saved!');

}

BigTree::redirect(MODULE_ROOT.'settings/tax/');

?>