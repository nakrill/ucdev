<?php

// GET SETTINGS
$settings = $_uccms_ecomm->getSettings();

?>

<style type="text/css">

    .tax-address .radio_button {
        display: inline-block;
        float: none;
        margin: -2px 0 0 0;
        vertical-align: middle;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // GLOBAL TAX TOGGLE
        $('#tax_global').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_tax_global').show();
            } else {
                $('#toggle_tax_global').hide();
            }
        });

    });

</script>

<form enctype="multipart/form-data" action="./process/" method="post">

<div class="container legacy legacy">

    <header>
        <h2>Tax Settings</h2>
    </header>

    <section>

        <div class="left last">

            <fieldset class="last">
                <input id="tax_global" type="checkbox" name="setting[tax_global]" value="1" <?php if ($settings['tax_global']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Global tax (optional) <small>Apply tax on everything in your store.</small></label>
            </fieldset>

            <div id="toggle_tax_global" class="contain" style="<?php if (!$settings['tax_global']) { ?>display: none;<?php } ?> padding-top: 10px;">
                <fieldset class="last">
                    %<input type="text" name="setting[tax_global_percent]" value="<?php echo $settings['tax_global_percent']; ?>" class="smaller_60" />
                </fieldset>
            </div>

        </div>

        <div class="right last">

            <fieldset class="tax-address last">
                <label>Tax address</label>
                <input type="radio" name="setting[tax_billing]" value="0" <?php if (!$settings['tax_billing']) { ?>checked="checked"<?php } ?> /> Shipping
                &nbsp;&nbsp;
                <input type="radio" name="setting[tax_billing]" value="1" <?php if ($settings['tax_billing']) { ?>checked="checked"<?php } ?> /> Billing
            </fieldset>

        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

</div>

</form>

<style type="text/css">

    #tax_groups .tax_title {
        width: 630px;
    }
    #tax_groups .tax_amount {
        width: 100px;
    }
    #tax_groups .tax_status {
        width: 100px;
    }
    #tax_groups .tax_edit {
        width: 55px;
    }
    #tax_groups .tax_delete {
        width: 55px;
    }

</style>

<a name="groups"></a>

<div id="tax_groups" class="table">
    <summary>
        <h2>Groups</h2>
        <a class="add_resource add" href="./group/"><span></span>Add Tax</a>
    </summary>
    <header style="clear: both;">
        <span class="tax_title">Title / Location</span>
        <span class="tax_amount">Amount</span>
        <span class="tax_status">Status</span>
        <span class="tax_edit">Edit</span>
        <span class="tax_delete">Delete</span>
    </header>
    <ul class="items">

        <?php

        // GET GROUPS
        $group_query = "SELECT * FROM `" .$_uccms_ecomm->tables['tax_groups']. "` ORDER BY `title` ASC, `id` ASC";
        $group_q = sqlquery($group_query);

        // LOOP
        while ($group = sqlfetch($group_q)) {

            ?>

            <li class="item">
                <section class="tax_title">
                    <?php echo $_uccms_ecomm->taxGroupName($group); ?>
                </section>
                <section class="tax_amount">
                    <?php echo $group['tax']; ?>%
                </section>
                <section class="tax_status status_<?php if ($group['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php if ($group['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                </section>
                <section class="tax_edit">
                    <a class="icon_edit" title="Edit Tax" href="./group/?id=<?php echo $group['id']; ?>"></a>
                </section>
                <section class="tax_delete">
                    <a href="./group/delete/?id=<?php echo $group['id']; ?>" class="icon_delete" title="Delete Tax" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                </section>
            </li>

            <?php

        }

        ?>

    </ul>
</div>