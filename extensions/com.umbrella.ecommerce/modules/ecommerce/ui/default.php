<?php

// REQUIRE ADMIN ACCESS
$admin->requireLevel(0);

// BLANK LAYOUT
$bigtree['layout'] = 'blank';

// LAYOUT
$layout = preg_replace('/[^A-Za-z0-9_-]/', '', $bigtree['path'][3]);

// NOT AJAX REQUEST
if (!$_REQUEST['ajax']) {

    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UI</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />

        <link rel="stylesheet" href="/admin/*/<?=$_uccms_ecomm->Extension?>/css/ui/base.css" type="text/css" />

        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="/admin/js/uccms.js"></script>

    </head>

    <body>

    <?php

}

// FILE OF LAYOUT
$layout_file = dirname(__FILE__). '/layouts/' .$layout. '/default.php';

// FILE EXISTS
if (file_exists($layout_file)) {

    include($layout_file);

// FILE DOES NOT EXIST
} else {
    ?>
    <div class="error">
        Layout not found.
    </div>
    <?php
}

// NOT AJAX REQUEST
if (!$_REQUEST['ajax']) {

    ?>

    </body>
    </html>

    <?php

}

?>