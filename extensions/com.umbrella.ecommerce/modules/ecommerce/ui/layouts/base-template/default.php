<style type="text/css">

    #contentContainer {
        margin-top: 60px;
    }

    footer.footer {
        margin: 30px 0 0 0;
        padding: 15px 0 5px;
        background-color: #f5f5f5;
    }

</style>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Link 1</a></li>
                <li><a href="#">Link 2</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link 1</a></li>
                <li><a href="#">Link 2</a></li>
                <li class="active"><a href="#">Link 3</a></li>
            </ul>
        </div>
    </div>
</nav>

<div id="contentContainer">
    <div class="container">
        <div class="page-header">
            <h1>Heading</h1>
        </div>
        <p class="lead">Lead paragraph.</p>
        <p>Normal paragraph.</p>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted">Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.umbrellaconsultants.com" target="_blank">Umbrella Consultants</a></p>
    </div>
</footer>