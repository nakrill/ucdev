<?php

// AJAX REQUEST
if ($_REQUEST['ajax']) {
    include(dirname(__FILE__). '/ajax.php');
    exit;
}

// CLEAN UP
$item_id        = (int)$_REQUEST['item_id'];
$duration_id    = (int)$_REQUEST['duration_id'];

/*
$item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item_id. ")";
$item_q = sqlquery($item_query);
$item = sqlfetch($item_q);
*/

// GET ITEM
$item = $_uccms_ecomm->getItem($item_id);

// ITEM NOT FOUND
if (!$item['id']) exit('Item not found.');

// GET BOOKING ITEM INFO
$booking_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
$booking_item_q = sqlquery($booking_item_query);
$booking_item = sqlfetch($booking_item_q);

// DATE SPECIFIED
if ($_REQUEST['date']) {
    $date = date('Y-m-d', strtotime($_REQUEST['date']));
} else {
    $date = date('Y-m-d');
}

$dt_from = $date. ' 00:00:00';
$dt_to = $date. ' 23:59:59';

// ITEM DURATIONS
$dura = [];
$durations_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`item_id`=" .$item_id. ") ORDER BY `sort` ASC, `id` ASC";
$durations_q = sqlquery($durations_query);
while ($duration = sqlfetch($durations_q)) {
    $dura[$duration['id']] = $duration;
}

// CURRENT DURATION
$curdur = [];
if ($duration_id) {
    $curdur = $dura[$duration_id];
}
if (!$curdur['id']) {
    $curdur = reset($dura);
}

?>

<style type="text/css">

    a, .btn {
        transition: all 0.2s ease;
    }

    #contentContainer {
        margin-top: 51px;
    }
    #contentContainer.manifest > .container-fluid {
        padding-left: 0px;
        padding-right: 0px;
    }
    #contentContainer.manifest .mainCol.left {
        position: relative;
        background-color: #eee;
        overflow: hidden;
        overflow-y: scroll;
    }

    #contentContainer.manifest .mainCol.left > .item-info {
        padding-top: 10px;
        text-align: center;
    }
    #contentContainer.manifest .mainCol.left > .item-info .title {
        font-size: 1.5em;
        font-weight: bold;
    }
    #contentContainer.manifest .mainCol.left .filter-date {
        padding: 15px 0 10px;
        border-bottom: 2px solid #ddd;
        text-align: center;
        font-size: 1.2em;
    }
    #contentContainer.manifest .mainCol.left .filter-date .current {
        font-weight: bold;
    }
    #contentContainer.manifest .mainCol.left .filter-date .direction a {
        padding: 10px 12px;
    }
    #contentContainer.manifest .mainCol.left .filter-trip {
        padding: 15px 0 10px;
        text-align: center;
    }
    #contentContainer.manifest .mainCol.left .filter-trip select {
        max-width: 350px;
        margin: 0 auto;
    }

    #contentContainer.manifest .mainCol.left .orders {
        margin: 20px 0 15px;
    }
    #contentContainer.manifest .mainCol.left .orders .loading {
        text-align: center;
    }
    #contentContainer.manifest .mainCol.left .orders .ajax-data {
        display: none;
    }
    #contentContainer.manifest .mainCol.left .orders .ajax-data .no-bookings {
        text-align: center;
    }
    #contentContainer.manifest .mainCol.left .orders .order {
        position: relative;
        margin-bottom: 15px;
    }
    #contentContainer.manifest .mainCol.left .orders .order .contain {
        border: 1px solid rgba(0, 0, 0, .3);
        border-radius: 4px;
        background-color: #fff;
        box-shadow: 0 0 5px rgba(0, 0, 0, .1);
        overflow: hidden;
        transition: all 0.2s ease;
    }
    #contentContainer.manifest .mainCol.left .orders .order:hover .contain {
        box-shadow: 0 0 5px rgba(0, 0, 0, .3);
    }
    #contentContainer.manifest .mainCol.left .orders .order .same-height {
        height: 80px;
    }
    #contentContainer.manifest .mainCol.left .orders .order .info {
        position: relative;
        text-align: left;
    }
    #contentContainer.manifest .mainCol.left .orders .order .info .name {
        margin-top: 10px;
        margin-left: -5px;
    }
    #contentContainer.manifest .mainCol.left .orders .order .info .trip-type {
        position: absolute;
        bottom: 10px;
        left: 10px;
        font-weight: bold;
    }
    #contentContainer.manifest .mainCol.left .orders .order .info .trip-time {
        position: absolute;
        bottom: 10px;
        right: 10px;
        text-align: right;
    }
    #contentContainer.manifest .mainCol.left .orders .order .bignum {
        text-align: center;
        color: #fff;
        cursor: pointer;
    }
    #contentContainer.manifest .mainCol.left .orders .order .bignum .title {
        display: block;
        margin-top: 15px;
        font-size: .9em;
        line-height: 1em;
        text-transform: uppercase;
        opacity: .7;
    }
    #contentContainer.manifest .mainCol.left .orders .order .bignum .num {
        display: block;
        margin-top: 5px;
        font-size: 2.5em;
        line-height: 1em;
    }

    #contentContainer.manifest .mainCol.left .orders .order .bignum {
        background-color: #4caf50;
    }

    #contentContainer.manifest .mainCol.left .orders .order .bignum .add {
        z-index: 5;
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        text-align: center;
        font-size: 3em;
        font-weight: bold;
        line-height: 80px;
        text-decoration: none;
        cursor: pointer;
    }

    #contentContainer.manifest .mainCol.left .selected-options {
        display: none;
        position: absolute;
        top: 120px;
        right: 15px;
    }

    #contentContainer.manifest .mainCol.right {
        padding-left: 0px;
        padding-right: 0px;
        border-left: 2px solid #ccc;
    }

    #contentContainer.manifest .mainCol.right .buttons.top {
        text-align: center;
    }
    #contentContainer.manifest .mainCol.right .buttons.top .btn {
        display: inline-block;
        width: 50%;
        margin: 15px 15px 0;
    }
    #contentContainer.manifest .mainCol.right .buttons.top .btn i {
        margin-right: 8px;
        opacity: .7;
    }

    #contentContainer.manifest .mainCol.right .stats {
        margin-bottom: 15px;
        padding: 15px;
        border-bottom: 1px solid #eee;
    }
    #contentContainer.manifest .mainCol.right .stats .stat {
        text-align: center;
    }
    #contentContainer.manifest .mainCol.right .stats .stat .title {
        margin: 0px;
        font-size: .9em;
        line-height: 1em;
        text-transform: uppercase;
        opacity: .8;
    }
    #contentContainer.manifest .mainCol.right .stats .stat .value {
        margin: 5px 0 0;
        font-size: 2em;
        line-height: 1em;
    }
    #contentContainer.manifest .mainCol.right .stats .stat .value .left {
        color: #3FDB44;
    }
    #contentContainer.manifest .mainCol.right .stats .stat .value .left.full {
        color: #DF715B;
    }

    footer.footer {
        padding: 15px 0 5px;
        background-color: #f5f5f5;
    }

    @media (max-width: 991px) {

        /*
        #contentContainer.manifest .mainCol.left .orders .order .same-height {
            height: auto;
        }
        */

        #contentContainer.manifest .mainCol.left {
            height: auto !important;
            padding-bottom: 1px;
        }
        #contentContainer.manifest .mainCol.right {
            padding-right: 0px;
        }

    }

    @media (max-width: 765px) {

    }

</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>

<script type="text/javascript">

    // CURRENT DATE
    itemID = <?php echo $item['id']; ?>;
    currentDate = '<?php echo $date; ?>';
    durationID = <?php echo $curdur['id']; ?>;
    totalSpots = <?php echo (int)$booking_item['max_simultaneous_bookings']; ?>;
    takenSpots = 0;
    availSpots = 0;

    // SET TITLE
    $('title').html('Manifest');

    $(function() {

        $(window).trigger('resize');

        // GET BOOKINGS
        getOrders();

        // PREV / NEXT DAY CLICK
        $('#contentContainer.manifest .mainCol.left .filter-date .direction a').click(function(e) {
            e.preventDefault();
            var direction = $(this).attr('data-direction');
            if (direction == 'prev') {
                new_date = moment(currentDate, 'YYYY-MM-DD').subtract(1, 'day');
            } else if (direction == 'next') {
                new_date = moment(currentDate, 'YYYY-MM-DD').add(1, 'day');
            }
            $('#contentContainer.manifest .mainCol.left .filter-date .current').text(new_date.format('dddd, MMMM Do'));
            //$('#contentContainer.manifest .mainCol.right .date .value').text(new_date.format('dddd, MMMM Do'));
            currentDate = new_date.format('YYYY-MM-DD');
            getOrders();
        });

        // TRIP (DURATION) CHANGE
        $('#contentContainer.manifest .mainCol.left .filter-trip select').change(function() {
            durationID = $(this).val();
            getOrders();
            $('#contentContainer.manifest .mainCol.right .buttons.top .btn.export').attr('href', '../../orders/print/general/manifest-persons/?no_hf=true&item_id=<?php echo $item['id']; ?>&from=<?php echo $date; ?>&to=<?php echo $date; ?>&duration_id=' +durationID);
            $('#contentContainer.manifest .mainCol.right .buttons.top .btn.export').attr('href', '../../orders/print/general/manifest-persons/?no_hf=true&item_id=<?php echo $item['id']; ?>&from=<?php echo $date; ?>&to=<?php echo $date; ?>&duration_id=' +durationID+ '&csv=true');
        });

        // BOOKING SELECT
        $('#contentContainer.manifest .mainCol.left .orders .ajax-data').on('click', 'tr.booking td.select a', function(e) {
            e.preventDefault();
            var $person = $(this).closest('tr.booking');
            var booking_id = $person.data('booking-id');
            persons = [];
            $('#contentContainer.manifest .mainCol.left .orders .ajax-data tr.person[data-booking-id="' +booking_id+ '"]').each(function(i, el) {
                persons.push($(this).data('person-id'));
            });
            var selectedPersons = getSelectedPersons(booking_id);
            if (selectedPersons.length == persons.length) {
                $('#contentContainer.manifest .mainCol.left .orders .ajax-data tr.person[data-booking-id="' +booking_id+ '"] td.select a').click();
            } else {
                $('#contentContainer.manifest .mainCol.left .orders .ajax-data tr.person[data-booking-id="' +booking_id+ '"]:not(.selected) td.select a').click();
            }
        });

        // PERSON SELECT
        $('#contentContainer.manifest .mainCol.left .orders .ajax-data').on('click', 'tr.person td.select a', function(e) {
            e.preventDefault();
            var $person = $(this).closest('tr.person');
            if ($person.hasClass('selected')) {
                $person.removeClass('selected');
            } else {
                $person.addClass('selected');
            }
            getSelectedPersons();
        });

        // TEMPORARY
        $('#contentContainer.manifest .mainCol.left .selected-options .dropdown-menu a').click(function(e) {
            e.preventDefault();
            alert('Coming soon!');
        });

    });

    // 100% HEIGHT COLUMNS ON WINDOW RESIZE
    $(window).resize(function() {
        $('#contentContainer.manifest .mainCol').height($(window).height() - 101);
    });

    // GET ORDERS
    function getOrders() {
        $('#contentContainer.manifest .mainCol.left .selected-options').hide();
        $('#contentContainer.manifest .mainCol.left .orders .ajax-data').hide();
        $('#contentContainer.manifest .mainCol.left .orders .loading').show();
        $.get('/admin/<?php echo $_uccms_ecomm->Extension; ?>*ecommerce/ui/manifest/', {
            no_hf: true,
            ajax: true,
            what: 'bookings',
            format: 'html',
            item_id: <?php echo $item['id']; ?>,
            date: currentDate,
            duration_id: durationID
        }, function(data) {
            $('#contentContainer.manifest .mainCol.left .orders .loading').hide();
            $('#contentContainer.manifest .mainCol.left .orders .ajax-data').html(data).show();
            updateSpotAvailability();
            getSelectedPersons();
        }, 'html');
    }

    // GET SELECTED PERSONS
    function getSelectedPersons(booking_id) {
        var selected = [];
        if (booking_id) {
            var sel = '[data-booking-id="' +booking_id+ '"]';
        } else {
            var sel = '';
        }
        $('#contentContainer.manifest .mainCol.left .orders .ajax-data tr.person.selected' +sel).each(function(i, el) {
            selected.push($(this).data('person-id'));
        });
        numSelected = selected.length;
        if (numSelected > 0) {
            $('#contentContainer.manifest .mainCol.left .selected-options .num').text(numSelected);
            $('#contentContainer.manifest .mainCol.left .selected-options').show();
        } else {
            $('#contentContainer.manifest .mainCol.left .selected-options').hide();
            $('#contentContainer.manifest .mainCol.left .selected-options .num').text('');
        }
        return selected;
    }

    // UPDATE SPOT AVAILABILITY
    function updateSpotAvailability() {
        var takenSpots = 0;
        $('#contentContainer.manifest .mainCol.left .orders .ajax-data tr.person').each(function(i, v) {
            takenSpots++;
        });
        $('#contentContainer.manifest .mainCol.right .stats .stat .value .total').text(totalSpots);
        availSpots = totalSpots - takenSpots;
        $('#contentContainer.manifest .mainCol.right .stats .stat .value .left').text(availSpots);
        if (availSpots > 0) {
            $('#contentContainer.manifest .mainCol.right .stats .stat .value .left').removeClass('full');
        } else {
            $('#contentContainer.manifest .mainCol.right .stats .stat .value .left').addClass('full');
        }
    }

</script>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Manifest</a>
        </div>
    </div>
</nav>

<div id="contentContainer" class="manifest">
    <div class="container-fluid"">

        <div class="clearfix">

            <div class="mainCol left col-md-8">

                <div class="item-info">
                    <span class="title"><?php echo stripslashes($item['title']); ?></span>
                </div>

                <div class="filter-date clearfix">
                    <span class="calendar"></span>
                    <div class="direction prev col-sm-2">
                        <a href="#" data-direction="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                    </div>
                    <div class="current col-sm-8">
                        <?php echo date('l, F jS', strtotime($date)); ?>
                    </div>
                    <div class="direction next col-sm-2">
                        <a href="#" data-direction="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>

                <div class="filter-trip clearfix">
                    <select name="duration_id" class="form-control">
                        <?php foreach ($dura as $duration) { ?>
                            <option value="<?php echo $duration['id']; ?>" <?php if ($duration['id'] == $curdur['id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($duration['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="orders">

                    <div class="loading">
                        Loading..
                    </div>

                    <div class="ajax-data clearfix"></div>

                </div>

                <div class="selected-options">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="withSelected" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            With <span class="num"></span> selected <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="withSelected">
                            <li><a href="#">Check In</a></li>
                            <li><a href="#">Re-assign</a></li>
                        </ul>
                    </div>
                </div>

            </div>

            <form id="form_manifest" action="#" method="post">
            <input type="hidden" name="order_id" value="<?php echo $item['id']; ?>" />
            <input type="hidden" name="date" value="<?php echo urldecode($date); ?>" />
            <input type="hidden" name="duration_id" value="<?php echo $curdur['id']; ?>" />

            <div class="mainCol right col-md-4">

                <div class="stats clearfix">

                    <div class="stat available col-md-12">
                        <div class="title">Available</div>
                        <div class="value">
                            <span class="left full">0</span> of <span class="total">0</span>
                        </div>
                    </div>

                </div>

                <div class="buttons top">
                    <a href="#" target="_blank" class="export btn btn-default"><i class="fa fa-download" aria-hidden="true"></i>Export</a>
                    <a href="#" target="_blank" class="print btn btn-default"><i class="fa fa-print" aria-hidden="true"></i>Print</a>
                </div>

            </div>

            </form>

        </div>

    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted">Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.umbrellaconsultants.com" target="_blank">Umbrella Consultants</a></p>
    </div>
</footer>