<?php

// DATE SPECIFIED
if ($_REQUEST['date']) {
    $date = date('Y-m-d', strtotime($_REQUEST['date']));
} else {
    $date = date('Y-m-d');
}

$dt_from = $date. ' 00:00:00';
$dt_to = $date. ' 23:59:59';

$bookings_params = array(
    'item_id'   => (int)$_REQUEST['item_id'],
    'status'    => array('placed', 'charged', 'processing','complete')
);

// DURATION SPECIFIED
if ($_REQUEST['duration_id']) {
    $bookings_params['duration_id'] = (int)$_REQUEST['duration_id'];
}

// GET BOOKINGS
$bookings = $_uccms_ecomm->stc->getBookings(strtotime($dt_from), strtotime($dt_to), $bookings_params);

// HAVE BOOKINGS
if (count($bookings) > 0) {

    ?>

    <style type="text/css">

        #contentContainer.manifest .mainCol.left .orders .ajax-data th.select, #contentContainer.manifest .mainCol.left .orders .ajax-data td.select {
            padding: 8px 15px 8px 20px;
        }

        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.booking {
            background-color: #eee;
        }
        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.booking td.select a {
            display: block;
            width: 20px;
            height: 20px;
            font-size: 1.5em;
            line-height: 1em;
            color: rgba(0, 0, 0, .5);
            outline: none;
        }
        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.booking td.select a:hover {
            color: #337ab7;
        }
        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.booking td.name .name {
            font-weight: bold;
            opacity: .7;
        }

        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.person.selected {
            background-color: #FEFBE5;
        }
        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.person:hover td {
            background-color: rgba(0, 0, 0, .03);
        }
        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.person td.select a {
            display: block;
            width: 20px;
            height: 20px;
            font-size: 1.6em;
            line-height: 1em;
            color: rgba(0, 0, 0, .1);
            outline: none;
        }
        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.person:hover td.select a {
            color: rgba(0, 0, 0, .3);
        }
        #contentContainer.manifest .mainCol.left .orders .ajax-data tr.person.selected td.select a {
            color: #337ab7;
        }

    </style>

    <div class="table">
        <table class="table">

            <thead class="thead-light">
                <tr>
                    <th class="select" scope="col"></th>
                    <th class="name col-md-6" scope="col">Name</th>
                    <th class="dob col-md-3" scope="col">DOB</th>
                    <th class="phone col-md-3" scope="col">Phone</th>
                </tr>
            </thead>

            <tbody>

                <?php

                $dia = array();

                // LOOP
                foreach ($bookings as $booking) {

                    // GET ORDER INFO
                    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$booking['order_id']. ")";
                    $order_q = sqlquery($order_query);
                    $order = sqlfetch($order_q);

                    // NOT DELETED
                    if ($order['status'] != 'deleted') {

                        $order_name = trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname']));
                        if (!$order_name) $order_name = trim(stripslashes($order['shipping_firstname']. ' ' .$order['shipping_lastname']));

                        $order_phone = $order['billing_phone'];
                        if (!$order_phone) $order_phone = $order['shipping_phone'];

                        ?>
                        <tr class="booking" data-booking-id="<?php echo $booking['id']; ?>">
                            <td class="select">
                                <a href="#" title="Select / Unselect all in this booking"><i class="fa fa-copyright" aria-hidden="true"></i></a>
                            </td>
                            <td class="name" colspan="2">
                                <a href="/admin/<?php echo $_uccms_ecomm->Extension; ?>*ecommerce/orders/edit/?id=<?php echo $booking['order_id']; ?>" target="_blank"><?php echo $order['id']; ?></a> <span class="name"><?php echo $order_name; ?></span>
                            </td>
                            <td class="phone">
                                <?php echo $order_phone; ?>
                            </td>
                        </tr>
                        <?php

                        // DON'T HAVE DURATION INFO YET
                        if (!$dia[$booking['duration_id']]['id']) {

                            // GET DURATION
                            $duration_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`id`=" .$booking['duration_id']. ")";
                            $duration_q = sqlquery($duration_query);
                            $duration = sqlfetch($duration_q);

                            $dia[$duration['id']] = $duration;

                        }

                        // SET DURATION
                        $duration = $dia[$booking['duration_id']];

                        // PERSONS
                        $persons = $_uccms_ecomm->stc->bookingPersons($booking['oitem_id']);

                        // LOOP THROUGH PEOPLE
                        foreach ($persons as $person) {

                            ?>

                            <tr class="person" data-booking-id="<?php echo $booking['id']; ?>" data-person-id="<?php echo $person['id']; ?>">
                                <td class="select"><a href="#" title="Select / Unselect"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a></td>
                                <td class="name"><?php echo trim(stripslashes($person['firstname']. ' ' .$person['lastname'])); ?></td>
                                <td class="dob"><?php echo $person['dob']; ?></td>
                                <td class="phone"><?php echo $person['phone']; ?></td>
                            </tr>

                            <?php

                        }

                    }

                }

                ?>

            </tbody>

        </table>
    </div>

    <?php

// NO BOOKINGS
} else {

    ?>
    <div class="no-bookings">
        No bookings for this day & time.
    </div>
    <?php

}

?>