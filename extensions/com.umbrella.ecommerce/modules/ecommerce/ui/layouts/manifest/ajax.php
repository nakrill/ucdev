<?php

$out = array();

// WHAT
$what = preg_replace('/[^A-Za-z0-9_-]/', '', $_REQUEST['what']);

// FILE
$file = dirname(__FILE__). '/ajax/' .$what. '.php';

// FILE EXISTS
if (file_exists($file)) {

    // INCLUDE FILE
    include($file);

// FILE DOESN'T EXIST
} else {
    $out['error'] = 'Endpoint not found.';
}

// JSON FORMAT
if ($_REQUEST['format'] == 'json') {
    echo json_encode($out);
}

?>