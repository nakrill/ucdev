<?php

// PAGE TITLE
$bigtree['admin_title'] = 'E-Commerce';

// BREADCRUMBS
$bigtree['breadcrumb'] = [
    [ 'title' => 'E-Commerce', 'link' => $bigtree['path'][1] ],
];

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

// CUSTOM STUFF
include('custom.php');

// NO HEADER/FOOTER NOT SPECIFIED
if (!$_REQUEST['no_hf']) {

    // BIGTREE
    $bigtree['css'][]   = 'master.css';
    $bigtree['js'][]    = 'master.js';

    // MODULE MAIN NAV SELECTED ARRAY
    $btp = $bigtree['path'][2];
    if ($btp) {
        if ($btp == 'inventory') $btp = 'items';
        $mmnsa[$btp] = 'active';
    } else {
        $mmnsa['dashboard'] = 'active';
    }

    // GET PAYMENT METHODS
    $payment_methods = $_uccms_ecomm->getPaymentMethods(true);

    ?>

    <link rel="stylesheet" href="/admin/*/<?php echo $_uccms_ecomm->Extension; ?>/css/master.css" type="text/css" media="print" />
    <link rel="stylesheet" href="/admin/*/<?php echo $_uccms_ecomm->Extension; ?>/css/print.css" type="text/css" media="print" />

    <style type="text/css">

        <?php
        // STATUS COLORS
        foreach ($_uccms_ecomm->statusColors() as $type => $statuses) {
            foreach ($statuses as $status => $color) {
                if ($color) {
                    echo '.status-' .$status. ' { color: ' .$color. '; }'. "\n";
                    echo '.status-bg-' .$status. ' { background-color: ' .$color. '; }'. "\n";
                }
            }
        }
        ?>

        /** REPORTS **/

        #report .report_heading h3.breadcrumbs {
            float: left;
        }
        #report .report_heading .print.button {
            float: right;
            height: auto;
            margin: -5px 7px 0 0;
            padding: 4px 8px;
            line-height: 1em;
        }
        #report .report_heading .print.button i {
            margin-right: 5px;
        }
        #report .report_heading .export.button {
            float: right;
            height: auto;
            margin: -5px 0 0;
            padding: 4px 8px;
            line-height: 1em;
        }
        #report .report_heading .export.button i {
            margin-right: 5px;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // PRINT BUTTON CLICK
            $('#page .button.print').click(function(e) {
                e.preventDefault();
                window.print();
            });

        });

    </script>

    <nav class="main">
        <section>
            <ul>
                <li class="<?php echo $mmnsa['dashboard']; ?>">
                    <a href="<?=MODULE_ROOT;?>"><span class="dashboard"></span>Dashboard</a>
                    <ul>
                        <li><a href="<?=MODULE_ROOT;?>calendar/">Calendar</a></li>
                        <li><a href="<?=MODULE_ROOT;?>reports/">Reports</a></li>
                    </ul>
                </li>
                <li class="<?php echo $mmnsa['categories']; ?>">
                    <a href="<?=MODULE_ROOT;?>categories/"><span class="pages"></span>Categories</a>
                    <ul>
                        <li><a href="<?=MODULE_ROOT;?>categories/">All</a></li>
                        <li><a href="<?=MODULE_ROOT;?>categories/?do=add">New Category</a></li>
                    </ul>
                </li>
                <li class="<?php echo $mmnsa['items']; ?>">
                    <a href="<?=MODULE_ROOT;?>items/"><span class="modules"></span>Items</a>
                    <ul>
                        <li><a href="<?=MODULE_ROOT;?>items/">All</a></li>
                        <li class="split-top"><a href="<?=MODULE_ROOT;?>items/edit/">New Item</a></li>
                        <li class="split-top"><a href="<?=MODULE_ROOT;?>items/attributes/">Attributes</a></li>
                        <li class="split-top"><a href="<?=MODULE_ROOT;?>inventory/">Inventory</a></li>
                        <li class="split-top"><a href="<?=MODULE_ROOT;?>assets/">Assets</a></li>
                    </ul>
                </li>
                <li class="<?php echo $mmnsa['quotes']; ?>">
                    <a href="<?=MODULE_ROOT;?>quotes/"><span class="developer"></span>Quotes</a>
                    <ul>
                        <li><a href="<?=MODULE_ROOT;?>quotes/">All</a></li>
                        <li class="split-top"><a href="<?=MODULE_ROOT;?>quotes/edit/">Create New</a></li>
                    </ul>
                </li>
                <li class="<?php echo $mmnsa['orders']; ?>">
                    <a href="<?=MODULE_ROOT;?>orders/"><span class="developer"></span>Orders</a>
                    <ul>
                        <li><a href="<?=MODULE_ROOT;?>orders/">All</a></li>
                        <li class="split-top"><a href="<?=MODULE_ROOT;?>orders/edit/">Create New</a></li>
                    </ul>
                </li>
                <?php /*if ($payment_methods['stripe']['active']) { ?>
                    <li class="<?php echo $mmnsa['payouts']; ?>">
                        <a href="<?=MODULE_ROOT;?>payouts/"><span class="developer"></span>Payouts</a>
                        <ul>
                            <li><a href="<?=MODULE_ROOT;?>payouts/">All</a></li>
                            <li><a href="<?=MODULE_ROOT;?>payouts/?status=preparing">Preparing</a></li>
                            <li><a href="<?=MODULE_ROOT;?>payouts/?status=scheduled">Scheduled</a></li>
                            <li><a href="<?=MODULE_ROOT;?>payouts/?status=sent">Sent</a></li>
                            <li><a href="<?=MODULE_ROOT;?>payouts/?status=cancelled">Cancelled</a></li>
                            <li class="split-top"><a href="<?=MODULE_ROOT;?>payouts/edit/">Create New</a></li>
                        </ul>
                    </li>
                <?php }*/ ?>
                <?php if ($_uccms['_account']->isEnabled()) { ?>
                    <li class="<?php echo $mmnsa['customers']; ?>">
                        <a href="<?=MODULE_ROOT;?>customers/"><span class="users"></span>Customers</a>
                        <ul>
                            <li><a href="<?=MODULE_ROOT;?>customers/">All</a></li>
                            <? /*
                            <li><a href="<?=MODULE_ROOT;?>contacts/?status=customer">Customers</a></li>
                            <li><a href="<?=MODULE_ROOT;?>contacts/edit/">New Contact</a></li>
                            */ ?>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($_uccms_ecomm->adminModulePermission() == 'p') { ?>
                    <li class="<?php echo $mmnsa['settings']; ?>">
                        <a href="<?=MODULE_ROOT;?>settings/"><span class="settings"></span>Settings</a>
                        <ul>
                            <li><a href="<?=MODULE_ROOT;?>settings/">General</a></li>
                            <?php if ($_uccms_ecomm->storeType() == 'booking') { ?>
                                <li><a href="<?=MODULE_ROOT;?>settings/booking/calendars/">Calendars</a></li>
                            <?php } ?>
                            <?php if ($_uccms_ecomm->storeType() == 'catering') { ?>
                                <li><a href="<?=MODULE_ROOT;?>settings/catering/">Catering</a></li>
                            <?php } ?>
                            <li><a href="<?=MODULE_ROOT;?>settings/tax/">Tax</a></li>
                            <li><a href="<?=MODULE_ROOT;?>settings/shipping/">Shipping</a></li>
                            <li><a href="<?=MODULE_ROOT;?>settings/payment/">Payment</a></li>
                            <li><a href="<?=MODULE_ROOT;?>settings/locations/">Locations</a></li>
                            <li><a href="<?=MODULE_ROOT;?>settings/discounts/">Discounts</a></li>
                            <li><a href="<?=MODULE_ROOT;?>settings/email/">Email</a></li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </section>
    </nav>

    <?php

}

?>