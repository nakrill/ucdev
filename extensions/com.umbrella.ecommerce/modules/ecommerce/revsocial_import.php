<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

// REVSOCIAL API KEY
$api_key = $cms->getSetting('rs_api-key');

// ONLY IMPORT ORDER ID'S ABOVE THIS
$order_id_above = 0;

// GET ORDERS
$order_sql = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`>" .$order_id_above. ") AND (`quote`!=1) AND ((`status`!='cart') AND (`status`!='failed') AND (`status`!='cancelled'))";
$order_query = sqlquery($order_sql);
while ($order = sqlfetch($order_query)) {

    // GET ORDER ITEMS
    $citems = $_uccms_ecomm->cartItems($order['id']);

    // HAVE ITEMS
    if (is_array($citems)) {

        unset($items);

        // LOOP THROUGH ITEMS
        foreach ($citems as $oitem) {

            // GET ITEM INFO
            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

            // ADD TO QUANTITY COUNT
            $items['num_quantity'] += $oitem['quantity'];

            // PRODUCT OPTIONS (ATTRIBUTES)
            $options = array();
            if ($oitem['options']) {
                $opts = json_decode($oitem['options'], true);
                foreach ($opts as $option_id => $values) {
                    if ($values[0]) {
                        $options[stripslashes($attra[$option_id]['title'])] = implode(', ', $values);
                    }
                }
            }

            // ADD TO PRODUCTS ARRAY
            $items['products'][] = array(
                'sku'       => stripslashes($item['sku']),
                'name'      => stripslashes($item['title']),
                'options'   => $options,
                'quantity'  => $oitem['quantity'],
                'total'     => $item['other']['total']
            );

        }

    }

    // ORDER INFO
    $data = array(
        'order' => array(
            'id' => $order['id'],
            'timestamp' => date('c', strtotime($order['dt'])), // ISO 8601 formatted date
            'customer' => array(
                'id'            => $order['customer_id'],
                'firstname'     => $order['billing_firstname'],
                'lastname'      => $order['billing_lastname'],
                //'middlename'    => 'Middle',
                //'prefix'        => 'Mr',
                //'suffix'        => 'Jr',
                'email'         => $order['billing_email'],
                'phone'         => $order['billing_phone'],
                //'dob'           => '05/19/1985',
                //'gender'        => 'male',
                //'country'       => 'US',
                'state'         => $order['billing_state'],
                'city'          => $order['billing_city'],
                'zip'           => $order['billing_zip'],
                'street'        => trim($order['billing_address1']. ' ' .$order['billing_address2']),
                'company'       => $order['billing_company']
            ),
            'billing' => array(
                'firstname'     => $order['billing_firstname'],
                'lastname'      => $order['billing_lastname'],
                //'middlename'    => 'Middle',
                //'prefix'        => 'Mr',
                //'suffix'        => 'Jr',
                'email'         => $order['billing_email'],
                'phone'         => $order['billing_phone'],
                //'dob'           => '05/19/1985',
                //'gender'        => 'male',
                //'country'       => 'US',
                'state'         => $order['billing_state'],
                'city'          => $order['billing_city'],
                'zip'           => $order['billing_zip'],
                'street'        => trim($order['billing_address1']. ' ' .$order['billing_address2']),
                'company'       => $order['billing_company']
            ),
            'shipping' => array(
                'firstname'     => $order['shipping_firstname'],
                'lastname'      => $order['shipping_lastname'],
                //'middlename'    => 'Middle',
                //'prefix'        => 'Mr',
                //'suffix'        => 'Jr',
                'email'         => $order['shipping_email'],
                'phone'         => $order['shipping_phone'],
                //'dob'           => '05/19/1985',
                //'gender'        => 'male',
                //'country'       => 'US',
                'state'         => $order['shipping_state'],
                'city'          => $order['shipping_city'],
                'zip'           => $order['shipping_zip'],
                'street'        => trim($order['shipping_address1']. ' ' .$order['shipping_address2']),
                'company'       => $order['shipping_company']
            ),
            'subtotal'          => $order['order_subtotal'],
            //'coupon_code'       => 'fall1234',
            //'discount_amount'   => '5.00',
            'shipping_amount'   => $order['order_shipping'],
            'tax_amount'        => $order['order_tax'],
            'order_total'       => $order['order_total'],
            'currency_code'     => 'USD',
            'shipping_method'   => $order['shipping_service'],
            'transaction_id'    => '',
            'total_products'    => count($items['products']),
            'total_items'       => $items['num_quantity'],
            'products'          => $items['products']
        ),
        'ip' => long2ip($order['ip'])
    );

    //echo print_r($data);
    //exit;

    // REST CLIENT
    require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

    // INIT REST API CLASS
    $_api = new RestClient(array(
        'base_url' => 'https://api.revsocial.com/v1'
    ));

    // MAKE API CALL
    $result = $_api->post('ecommerce/addOrder', array(
        'api_key'   => $api_key,
        'data'      => $data
    ));

    echo print_r($result). "\n\n";
    //exit;

}

echo 'Done!';

?>