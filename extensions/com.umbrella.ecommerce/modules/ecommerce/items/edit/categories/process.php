<?php

// CLEAN UP
$id = (int)$_POST['item']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE ITEM ID
    if ($id) {

        $cata = array();

        // GET CURRENT RELATIONS
        $query = "SELECT `category_id` FROM `" .$_uccms_ecomm->tables['item_categories']. "` WHERE (`item_id`=" .$id. ")";
        $q = sqlquery($query);
        while ($row = sqlfetch($q)) {
            $cata[$row['category_id']] = $row['category_id'];
        }

        // HAVE CATEGORIES
        if (count($_POST['category']) > 0) {

            // LOOP
            foreach ($_POST['category'] as $category_id => $selected) {

                // CLEAN UP
                $category_id = (int)$category_id;

                // HAVE CATEGORY ID & WAS SELECTED
                if (($category_id) && ($selected)) {

                    // NOT IN EXISTING CATEGORY RELATIONS
                    if (!$cata[$category_id]) {

                        // DB COLUMNS
                        $columns = array(
                            'item_id'       => $id,
                            'category_id'   => $category_id,
                            'updated_by'    => $_uccms_ecomm->adminID()
                        );

                        // ADD RELATIONSHIP TO DB
                        $query = "INSERT INTO `" .$_uccms_ecomm->tables['item_categories']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";
                        sqlquery($query);

                    }

                    // REMOVE FROM RELATIONS
                    unset($cata[$category_id]);

                }

            }

        }

        // HAVE LEFT OVER (OLD) RELATIONS
        if (count($cata) > 0) {

            // LOOP
            foreach ($cata as $category_id) {

                // REMOVE RELATIONSHIP FROM DB
                $query = "DELETE FROM `" .$_uccms_ecomm->tables['item_categories']. "` WHERE (`item_id`=" .$id. ") AND (`category_id`=" .$category_id. ")";
                sqlquery($query);

            }

        }

        $admin->growl('Item', 'Categories updated.');

    // ITEM ID NOT SPECIFIED
    } else {
        $admin->growl('Item', 'Item ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id. '#categories');

?>