<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

/*
if ($_SERVER['REMOTE_ADDR'] == '68.203.17.61') {
    $_uccms_ecomm->item_updateVariants($id);
    exit;
}
*/

//$_uccms_ecomm->item_updateVariants($id);

/*
$attrs = array( // NORMAL
    1 => 3,
    3 => 2,
    //24 => array(3,1)
);
$attrs = array( // PACKAGE
    13 => array(
        26 => 3
    )
);
$inventory = $_uccms_ecomm->item_getInventory($id, $_uccms_ecomm->cart_locationID(), $attrs);
echo print_r($inventory);
exit;
*/

/*
$attrs = array(
    //3 => 1
);
$changes = array( // NORMAL
    'available' => -1
);
$attrs = array( // PACKAGE
    13 => array(
        26 => 3
    )
);
$result = $_uccms_ecomm->item_changeInventory($id, $_uccms_ecomm->cart_locationID(), $attrs, $changes, array(
    'log'   => array(
        'source'    => 'checkout'
    )
));
echo print_r($result);
exit;
*/

// ITEM PRICEGROUPS
$ipga = array();

// VARIATIONS ARRAY
$va = array();

// ID SPECIFIED
if ($id) {

    // GET ITEM INFO
    $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$id. ")";
    $item_q = sqlquery($item_query);
    $item = sqlfetch($item_q);

    // ITEM FOUND
    if ($item['id']) {

        // PRICE GROUPS ENABLED
        if ($_uccms_ecomm->getSetting('pricegroups_enabled')) {

            // GET ITEM'S PRICE GROUPS
            $ipga = $_uccms_ecomm->itemPriceGroups($item['id']);

        }

        // GET ITEM VARIANTS
        $va = $_uccms_ecomm->item_getVariants($item['id']);

    }

// NO ID SPECIFIED
} else {

    // DEFAULTS
    $item['active'] = 1;

}

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Items', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];
$bigtree['breadcrumb'][] = [ 'title' => ($item['id'] ? 'Edit' : 'Add'). ' Item', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

// GET STORE PRICEGROUPS
$pga = $_uccms_ecomm->getSetting('pricegroups');

// LOCATIONS ARRAY
$la = array();

// GET LOCATIONS
$loc_query = "SELECT * FROM `" .$_uccms_ecomm->tables['locations']. "`";
$loc_q = sqlquery($loc_query);
while ($loc = sqlfetch($loc_q)) {
    $la[$loc['id']] = $loc;
}

if (count($la) == 0) $la[0] = array('id' => 0);

$accta = array();

// GET FRONTEND ACCOUNTS
$account_query = "
SELECT CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, a.*, ad.*
FROM `uccms_accounts` AS `a`
INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
ORDER BY `fullname` ASC, `email` ASC, a.id ASC
";
$account_q = sqlquery($account_query);
while ($account = sqlfetch($account_q)) {
    $accta[$account['id']] = $account;
}

?>

<script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/selectivity-full.min.js"></script>
<link rel="stylesheet" href="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/css/lib/selectivity-full.css" type="text/css" />

<style type="text/css">

    #page .left fieldset {
        padding-right: 7.5px;
    }
    #page .right fieldset {
        padding-left: 7.5px;
    }

    #page .add_container table {
        width: auto;
    }
    #page .add_container td {
        border: 0px none;
    }
    #page .add_container fieldset {
        margin: 0px;
    }
    #page .add_container input[type="submit"] {
        height: 30px;
        line-height: 0;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        $('form select[name="item[account_id]"]').change(function(e) {
            if ($(this).val() == 0) {
                $('#toggle_email-account-on-order').hide();
            } else {
                $('#toggle_email-account-on-order').show();
            }
        });

    });

</script>

<div class="container legacy">

    <form enctype="multipart/form-data" action="./process/" method="post">
    <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

    <header>
        <h2><?php if ($item['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Item</h2>
    </header>

    <section>

        <div class="row">

            <div class="col-md-6">

                <fieldset class="form-group">
                    <label>Title</label>
                    <input type="text" name="item[title]" value="<?php echo stripslashes($item['title']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Short Description</label>
                    <div>
                        <?php
                        $field = array(
                            'key'       => 'item[description_short]', // The value you should use for the "name" attribute of your form field
                            'value'     => stripslashes($item['description_short']), // The existing value for this form field
                            'id'        => 'item_description_short', // A unique ID you can assign to your form field for use in JavaScript
                            'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                            'options'   => array(
                                'simple' => true
                            )
                        );
                        include(BigTree::path('admin/form-field-types/draw/html.php'));
                        ?>
                    </div>
                </fieldset>

                <? /*

                - Manufacturer (dropdown, need section to manage)
                - Additional search keywords

                */ ?>

            </div>

            <div class="col-md-6">

                <fieldset class="form-group">
                    <label>SKU</label>
                    <input type="text" name="item[sku]" value="<?php echo stripslashes($item['sku']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Tax Group</label>
                    <select name="item[tax_group]" class="custom_control form-control">
                        <option value="">Category / Global tax settings</option>
                        <?php
                        $taxgroup_query = "SELECT * FROM `" .$_uccms_ecomm->tables['tax_groups']. "` ORDER BY `title` ASC, `id` ASC";
                        $taxgroup_q = sqlquery($taxgroup_query);
                        while ($taxgroup = sqlfetch($taxgroup_q)) {
                            ?>
                            <option value="<?php echo $taxgroup['id']; ?>" <?php if ($taxgroup['id'] == $item['tax_group']) { ?>selected="selected"<?php } ?>><?php echo $_uccms_ecomm->taxGroupName($taxgroup); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </fieldset>

                <div class="row">

                    <div class="col-md-6">

                        <fieldset class="form-group">
                            <label>Type</label>
                            <select name="item[type]" class="custom_control form-control">
                                <?php foreach ($_uccms_ecomm->itemTypes() as $type_id => $type) { ?>
                                    <option value="<?php echo $type_id; ?>" <?php if ($type_id == $item['type']) { ?>selected="selected"<?php } ?>><?php echo $type['title']; ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>

                        <fieldset class="form-group">
                            <input type="checkbox" name="item[active]" value="1" <?php if ($item['active']) { ?>checked="checked"<?php } ?> />
                            <label class="for_checkbox">Active</label>
                        </fieldset>

                        <fieldset class="form-group">
                            <input type="checkbox" name="item[featured]" value="1" <?php if ($item['featured']) { ?>checked="checked"<?php } ?> />
                            <label class="for_checkbox">Featured</label>
                        </fieldset>

                    </div>

                    <div class="col-md-6">

                        <fieldset class="form-group">
                            <input type="checkbox" name="item[no_purchase]" value="1" <?php if ($item['no_purchase']) { ?>checked="checked"<?php } ?> />
                            <label class="for_checkbox">Not available for purchase<br /><small>(no "Add To Cart" button)</small></label>
                        </fieldset>

                        <fieldset class="form-group">
                            <input type="checkbox" name="item[not_recent]" value="1" <?php if ($item['not_recent']) { ?>checked="checked"<?php } ?> />
                            <label class="for_checkbox">Exclude from recent items list</label>
                        </fieldset>

                        <fieldset class="last">
                            <input type="checkbox" name="item[no_search]" value="1" <?php if ($item['no_search']) { ?>checked="checked"<?php } ?> />
                            <label class="for_checkbox">Not searchable</label>
                        </fieldset>

                    </div>

                </div>

                <fieldset style="margin-bottom: 5px;">
                    <label>Account</label>
                    <select name="item[account_id]" class="custom_control form-control">
                        <option value="0">None</option>
                        <?php foreach ($accta as $account_id => $account) { ?>
                            <option value="<?php echo $account_id; ?>" <?php if ($account_id == $item['account_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($account['fullname']). ' (' .stripslashes($account['email']). ')'; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <div id="toggle_email-account-on-order" style="<?php if (!$item['account_id']) { ?>display: none;<?php } ?>">

                    <div class="row">

                        <div class="col-md-6">

                            <fieldset style="margin-top: 5px;">
                                <input type="checkbox" name="item[email_account_on_order]" value="1" <?php if ($item['email_account_on_order']) { ?>checked="checked"<?php } ?> />
                                <label class="for_checkbox">Email account when item is purchased</label>
                            </fieldset>

                        </div>

                        <div class="col-md-6">

                            <fieldset class="form-group">
                                <label style="margin-bottom: 2px;">Payout</label>
                                <input type="text" name="item[payout_calc_amount]" value="<?php echo (float)$item['payout_calc_amount']; ?>" class="form-control" />%
                                <label class="last"><small style="margin: 0px;">How much the account gets from the sale of this item.</small></label>
                            </fieldset>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <fieldset class="form-group">
            <label>Description</label>
            <div>
            <?php
            $field = array(
                'key'       => 'item[description]', // The value you should use for the "name" attribute of your form field
                'value'     => stripslashes($item['description']), // The existing value for this form field
                'id'        => 'item_description', // A unique ID you can assign to your form field for use in JavaScript
                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                'options'   => array(
                    'simple' => false
                )
            );
            include(BigTree::path('admin/form-field-types/draw/html.php'));
            ?>
            </div>
        </fieldset>

        <div class="contain">
            <h6><span>Price</span></h6>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Price</label>
                                <?php if ($_uccms_ecomm->getSetting('pricegroups_enabled')) { ?>
                                    <?php if ($ipga[1]['price']) { ?>
                                        <input type="hidden" name="item[price]" value="<?php echo $ipga[1]['price']; ?>" />
                                    <?php } else { ?>
                                        <input type="hidden" name="item[price]" value="<?php echo stripslashes($item['price']); ?>" />
                                    <?php } ?>
                                    Price Groups (below)
                                <?php } else { ?>
                                    $<input type="text" name="item[price]" value="<?php echo stripslashes($item['price']); ?>" class="form-control" />
                                    <label class="last"><small>What to charge customers.</small></label>
                                <?php } ?>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Cost</label>
                                $<input type="text" name="item[price_cost]" value="<?php echo stripslashes($item['price_cost']); ?>" class="form-control" />
                                <label class="last"><small>Your cost from supplier.</small></label>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>MSRP</label>
                                $<input type="text" name="item[price_msrp]" value="<?php echo stripslashes($item['price_msrp']); ?>" class="form-control" />
                                <label class="last"><small>Mfg. Suggested Retail Price.</small></label>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Sale</label>
                                $<input type="text" name="item[price_sale]" value="<?php echo stripslashes($item['price_sale']); ?>" class="form-control" />
                                <label class="last"><small>Value above 0.00 if on sale.</small></label>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            if ($_uccms_ecomm->getSetting('pricegroups_enabled')) {

                // NO PRICEGROUPS
                if (((!$pga) || (count($pga) == 0)) || (count($ipga) == 0)) {
                    $ipga[1] = array(
                        'pricegroup_id' => 1,
                        'enabled'       => 1,
                        'price'         => $item['price'],
                        'qty_min'       => 0
                    );
                }

                ?>
                <div>
                    <h6>Price Groups</h6>
                    <div class="contain" style="padding-bottom: 25px;">
                        <table border="0" cellpadding="0" cellspacing="0" class="items" style="width: auto; margin-bottom: 0px;">
                            <tr>
                                <th style="padding: 5px 10px; text-align: left;">Group</th>
                                <th style="padding: 5px 10px; text-align: left;">Enabled</th>
                                <th style="padding: 5px 10px; text-align: left;">Price</th>
                                <th style="padding: 5px 10px; text-align: left;">Min Qty</th>
                            </tr>
                            <?php foreach ($_uccms_ecomm->priceGroups() as $group_id => $group) { ?>
                                <tr id="pg-<?=$group_id?>" class="item">
                                    <td style="padding: 5px 10px;">
                                        <?php echo $group['title']; ?>
                                    </td>
                                    <td style="padding: 5px 10px;">
                                        <input type="checkbox" name="pricegroup[<?php echo $group_id; ?>][enabled]" value="1" <?php if ($ipga[$group_id]['enabled']) { ?>checked="checked"<?php } ?> />
                                    </td>
                                    <td style="padding: 5px 10px;">
                                        $<input type="text" name="pricegroup[<?php echo $group_id; ?>][price]" value="<?php echo number_format($ipga[$group_id]['price'], 2, '.', ''); ?>" style="display: inline-block; width: 50px;" />
                                    </td>
                                    <td style="padding: 5px 10px;">
                                        <input type="text" name="pricegroup[<?php echo $group_id; ?>][qty_min]" value="<?php echo (int)$ipga[$group_id]['qty_min']; ?>" style="width: 50px;" />
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            <?php } ?>

            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <input type="checkbox" name="item[hide_price]" value="1" <?php if ($item['hide_price']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Hide price <small>(until added to cart)</small></label>
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="checkbox" name="item[quote]" value="1" <?php if ($item['quote']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Quote required</label>
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="checkbox" name="item[after_tax]" value="1" <?php if ($item['after_tax']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Add price after tax</label>
                    </fieldset>
                </div>
            </div>

        </div>

        <div class="contain">
            <h6><span>Shipping</span></h6>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Price</label>
                                $<input type="text" name="item[shipping_price]" value="<?php echo stripslashes($item['shipping_price']); ?>" class="form-control" />
                                <label><small>Base price for shipping.</small></label>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Cost</label>
                                $<input type="text" name="item[shipping_cost]" value="<?php echo stripslashes($item['shipping_cost']); ?>" class="form-control" />
                                <label><small>How much it costs you to ship.</small></label>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Weight</label>
                                <input type="text" name="item[weight]" value="<?php echo stripslashes($item['weight']); ?>" class="form-control" />lbs
                                <label><small>The weight of the item.</small></label>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <input type="checkbox" name="item[shipping_free]" value="1" <?php if ($item['shipping_free']) { ?>checked="checked"<?php } ?> />
                                <label class="for_checkbox">Free Shipping</label>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Length</label>
                                <input type="text" name="item[dimensions_length]" value="<?php echo stripslashes($item['dimensions_length']); ?>" class="form-control" />inches
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Width</label>
                                <input type="text" name="item[dimensions_width]" value="<?php echo stripslashes($item['dimensions_width']); ?>" class="form-control" />inches
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Height</label>
                                <input type="text" name="item[dimensions_height]" value="<?php echo stripslashes($item['dimensions_height']); ?>" class="form-control" />inches
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="contain">
            <h6 class="uccms_toggle" data-what="seo"><span>SEO</span><span class="icon_small icon_small_caret_down"></span></h6>
            <div class="contain uccms_toggle-seo" style="display: none;">

                <fieldset class="form-group">
                    <label>URL</label>
                    <input type="text" name="item[slug]" value="<?php echo stripslashes($item['slug']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Title</label>
                    <input type="text" name="item[meta_title]" value="<?php echo stripslashes($item['meta_title']); ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Description</label>
                    <textarea name="item[meta_description]" class="form-control" style="height: 64px;"><?php echo stripslashes($item['meta_description']); ?></textarea>
                </fieldset>

                <fieldset class="form-group">
                    <label>Meta Keywords</label>
                    <textarea name="item[meta_keywords]" class="form-control" style="height: 64px;"><?php echo stripslashes($item['meta_keywords']); ?></textarea>
                </fieldset>

            </div>
        </div>

    </section>

    <footer>
        <input class="btn btn-primary" type="submit" value="Save" />
        <a class="btn btn-secondary" href="../">&laquo; Back</a>
    </footer>

    </form>

</div>

<?php if ($item['id']) { ?>

    <?php

    // STORE TYPE
    include(dirname(__FILE__). '/store_types/' .$_uccms_ecomm->storeType(). '/fields.php');

    // PACKAGE
    if ($item['type'] == 3) {
        include(dirname(__FILE__). '/item_types/package/items.php');
    }

    ?>

    <style type="text/css">

        #images .add_container, #videos .add_container {
            padding: 10px;
            background-color: #fafafa;
            border-bottom: 1px solid #ccc;
        }

        #images .add_container td, #videos .add_container td {
            vertical-align: top;
        }

        #images.table section a, #categories.table section a, #videos.table section a, #videos.table section a {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #images.table > ul li, #videos.table > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }

        #images.table > ul li section, #videos.table > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }

        #images.table > ul li section:first-child, #videos.table > ul li section:first-child {
        }

        #images .items .item .image, #videos .items .item .video {
            width: 500px;
        }
        #images .items .item .title, #videos .items .item .title {
        }

        #images.table .currently .remove_resource, #videos.table .currently .remove_resource {
            margin-top: -3px;
            margin-bottom: -10px;
        }

        #images .items .item .title fieldset label, #videos .items .item .title fieldset label {
            margin-bottom: 2px;
            text-align: left;
        }

        #categories.table > ul li {
            height: auto;
            padding: 10px;
            line-height: 1.2em;
        }

        #categories.table > ul li li {
            padding-bottom: 0px;
            padding-left: 20px;
            border: 0px none;
            background-color: transparent;
        }

        #categories.table > ul li section {
            height: auto;
            overflow: visible;
            white-space: normal;
        }

        #categories.table > ul li section:first-child {
            padding-left: 0px;
        }

        #categories ul .checkbox {
            margin-top: -1px;
        }

        #attributes .add_container {
            padding: 6px 10px;
            background-color: #fafafa;
            border-bottom: 1px solid #ccc;
        }

        #attributes section {
            padding-top: 0px;
            padding-bottom: 0px;
        }
        #attributes .attribute_sort {
            width: 55px;
        }
        #attributes .attribute_title {
            width: 675px;
            text-align: left;
        }
        #attributes .attribute_status {
            width: 100px;
        }
        #attributes .attribute_edit {
            width: 55px;
        }
        #attributes .attribute_delete {
            width: 55px;
        }

        #variants {
            padding-bottom: 15px;
        }
        #variants .location {
            margin: 15px 15px 0;
            border: 1px solid #ccc;
        }
        #variants .location .heading {
            position: relative;
            display: block;
            margin: 0px;
            padding: 8px 10px;
            background-color: #ccc;
            color: #444;
            font-weight: bold;
        }
        #variants .location .heading .arrow {
            position: absolute;
            top: 8px;
            right: 8px;
            font-size: 1.2em;
        }
        #variants .location header {
            height: auto;
            padding: 8px 10px 0;
        }
        #variants .location header span {
            float: left;
            font-size: 9px !important;
            height: 20px;
            line-height: 21px;
            text-transform: uppercase;
        }
        #variants .location .variant_title {
            width: 400px;
            text-align: left;
        }
        #variants .location .variant_sku {
            width: 200px;
            text-align: left;
        }
        #variants .location .variant_available {
            width: 167px;
            text-align: left;
        }
        #variants .location .variant_alert {
            width: 50px;
            text-align: left;
        }
        #variants .location .variant_alert-at {
            width: 88px;
            text-align: left;
        }
        #variants .location .items {
            padding-bottom: 10px;
        }
        #variants .location .items .item {
            padding: 8px 10px 0;
        }
        #variants .location .items .item:last-child {
            border-bottom: 0px none;
        }
        #variants .location .items .item section {
            float: left;
            height: 32px;
            padding: 0px;
        }
        #variants .location .items .item section input[type="text"] {
            width: 100%;
        }
        #variants .location .items .item .variant_available input[type="text"] {
            width: 65px;
        }
        #variants .location .items .item .variant_alert-at input[type="text"] {
            width: 65px;
        }

        #discount .select {
            width: auto;
        }
        #discount p {
            margin-bottom: 0px;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function() {

            // IMAGES - SORTABLE
            $('#images ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('id').replace('row_', ''));
                    });
                    $('#images input[name="sort"]').val(sort.join());
                }
            });

            // IMAGES - ADD CLICK
            $('#images .add_resource').click(function(e) {
                e.preventDefault();
                $('#images .add_container').toggle();
            });

            // IMAGES - REMOVE CLICK
            $('#images .icon_delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var id = $(this).attr('data-id');
                    var del = $('#images input[name="delete"]').val();
                    if (del) del += ',';
                    del += id;
                    $('#images input[name="delete"]').val(del);
                    $('#images li#row_' +id).fadeOut(500);
                }
            });

            // VIDEOS - SORTABLE
            $('#videos ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('id').replace('row_', ''));
                    });
                    $('#videos input[name="sort"]').val(sort.join());
                }
            });

            // VIDEOS - ADD CLICK
            $('#videos .add_resource').click(function(e) {
                e.preventDefault();
                $('#videos .add_container').toggle();
            });

            // VIDEOS - REMOVE CLICK
            $('#videos .icon_delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var id = $(this).attr('data-id');
                    var del = $('#videos input[name="delete"]').val();
                    if (del) del += ',';
                    del += id;
                    $('#videos input[name="delete"]').val(del);
                    $('#videos li#row_' +id).fadeOut(500);
                }
            });

            // ATTRIBUTES - HIDE GROUP SELECT
            $('#select_attribute-group').hide();

            // ATTRIBUTES - ADD CLICK
            $('#attributes .add_resource').click(function(e) {
                e.preventDefault();
                $('#attributes .add_container').toggle();
            });

            // NEW ATTRIBUTE - WHAT CHANGE
            $('#select_attribute-what').change(function(e) {
                var val = $(this).val();
                $('#attributes .whats .option').hide();
                if (val == 'custom') {
                    window.location.href = './attributes/edit/?item_id=<?php echo $item['id']; ?>';
                } else {
                    $('#attributes .whats #select_attribute-' +val).show();
                }
            });

            // ATTRIBUTES - SORTABLE
            $('#attributes ul.ui-sortable').sortable({
                handle: '.icon_sort',
                axis: 'y',
                containment: 'parent',
                items: 'li',
                //placeholder: 'ui-sortable-placeholder',
                update: function() {
                    var rows = $(this).find('li');
                    var sort = [];
                    rows.each(function() {
                        sort.push($(this).attr('data-id'));
                    });
                    $('#attributes input[name="sort"]').val(sort.join());
                }
            });

            // ATTRIBUTES - DELETE CLICK
            $('#attributes .items .item .icon_delete').click(function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this?')) {
                    var id = $(this).attr('data-id');
                    var del = $('#attributes input[name="delete"]').val();
                    if (del) del += ',';
                    del += id;
                    $('#attributes input[name="delete"]').val(del);
                    $('#attributes li[data-id=' +id+ ']').fadeOut(500);
                }
            });

            // VARIANTS - LOCATION EXPAND
            $('#variants .location .heading').click(function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                $('#variants .loc-' +id+ ' .toggle-content').toggle(0, function() {
                    if ($(this).is(':visible')) {
                        $('#variants .loc-' +id+ ' .heading .arrow').removeClass('fa-caret-down').addClass('fa-caret-up');
                    } else {
                        $('#variants .loc-' +id+ ' .heading .arrow').removeClass('fa-caret-up').addClass('fa-caret-down');
                    }
                });
            });

        });
    </script>

    <a name="images"></a>

    <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

        <form enctype="multipart/form-data" action="./images/process/" method="post">
        <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

        <div id="images" class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Images</h2>
                <a class="add_resource add" href="#"><span></span>Add Image</a>
            </summary>

            <div class="add_container" style="display: none;">
                <table style="margin-bottom: 0px; border: 0px;">
                    <tr>
                        <td width="480">
                            <?php
                            $field = array(
                                'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                'key'       => 'new', // The value you should use for the "name" attribute of your form field
                                'value'     => '', // The existing value for this form field
                                'id'        => 'image-new', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => array(
                                    'image' => true
                                ),
                                'required'  => false // A boolean value of whether this form field is required or not
                            );
                            include(BigTree::path('admin/form-field-types/draw/upload.php'));
                            ?>
                        </td>
                        <td>
                            <fieldset class="form-group">
                                <input type="text" name="new-caption" value="" placeholder="Caption / Alt Text" class="form-control" style="width: 300px;" />
                            </fieldset>
                        </td>
                        <td style="text-align: right;">
                            <input class="blue" type="submit" value="Add" />
                        </td>
                    </tr>
                </table>
            </div>

            <?php

            // GET IMAGES
            $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC, `id` ASC";
            $image_q = sqlquery($image_query);

            // NUMBER OF IMAGES
            $num_images = sqlrows($image_q);

            // HAVE IMAGES
            if ($num_images > 0) {

                ?>

                <ul class="items ui-sortable">

                    <?php

                    // LOOP
                    while ($image = sqlfetch($image_q)) {

                        ?>

                        <li id="row_<?php echo $image['id']; ?>" class="item contain">
                            <section class="sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="image">
                                <?php
                                $field = array(
                                    'title'     => '', // The title given by the developer to draw as the label (drawn automatically)
                                    'subtitle'  => '', // The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
                                    'key'       => 'image-' .$image['id'], // The value you should use for the "name" attribute of your form field
                                    'value'     => ($image['image'] ? $_uccms_ecomm->imageBridgeOut($image['image'], 'items') : ''), // The existing value for this form field
                                    'id'        => 'image-' .$image['id'], // A unique ID you can assign to your form field for use in JavaScript
                                    'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                    'options'   => array(
                                        'image' => true
                                    ),
                                    'required'  => false // A boolean value of whether this form field is required or not
                                );
                                include(BigTree::path('admin/form-field-types/draw/upload.php'));
                                ?>
                            </section>
                            <section class="title">
                                <fieldset class="form-group">
                                    <input type="text" name="image-<?php echo $image['id']; ?>-caption" value="<?php echo stripslashes($image['caption']); ?>" placeholder="Caption / Alt Text" class="form-control" style="width: 300px;" />
                                </fieldset>
                            </section>
                            <section class="view_action">
                                <a class="icon_delete" href="#" data-id="<?php echo $image['id']; ?>"></a>
                            </section>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <footer>
                    <input class="blue" type="submit" value="Save" />
                </footer>

                <?php

            } else {

                ?>

                <div style="padding: 10px; text-align: center; font-size: .75em;">
                    No item images added yet.
                </div>

                <?php

            }

            ?>

        </div>

        </form>

    </div>

    <a name="videos"></a>

    <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

        <form enctype="multipart/form-data" action="./videos/process/" method="post">
        <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

        <div id="videos" class="table">

            <input type="hidden" name="sort" value="" />
            <input type="hidden" name="delete" value="" />

            <summary>
                <h2>Videos</h2>
                <a class="add_resource add" href="#"><span></span>Add Video</a>
            </summary>

            <div class="add_container" style="display: none;">
                <table style="margin-bottom: 0px; border: 0px;">
                    <tr>
                        <td width="480">
                            <input id="video-new" type="text" name="video[new][url]" value="" placeholder="YouTube / Vimeo URL" class="form-control" />
                        </td>
                        <td>
                            <fieldset class="form-group">
                                <input type="text" name="video[new][caption]" value="" placeholder="Title" class="form-control"style="width: 300px;" />
                            </fieldset>
                        </td>
                        <td style="text-align: right;">
                            <input class="blue" type="submit" value="Add" />
                        </td>
                    </tr>
                </table>
            </div>

            <?php

            // GET VIDEOS
            $video_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_videos']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC, `id` ASC";
            $video_q = sqlquery($video_query);

            // NUMBER OF VIDEOS
            $num_videos = sqlrows($video_q);

            // HAVE VIDEOS
            if ($num_videos > 0) {

                ?>

                <ul class="items ui-sortable">

                    <?php

                    // LOOP
                    while ($video = sqlfetch($video_q)) {

                        // GET VIDEO INFO
                        $vi = videoInfo(stripslashes($video['video']));

                        ?>

                        <li id="row_<?php echo $video['id']; ?>" class="item contain">
                            <section class="sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="video">
                                <input type="text" name="video[<?php echo $video['id']; ?>][url]" value="<?php echo stripslashes($video['video']); ?>" placeholder="YouTube / Vimeo URL" class="form-control"style="width: 400px;" />
                            </section>
                            <section class="title">
                                <fieldset class="form-group">
                                    <input type="text" name="video[<?php echo $video['id']; ?>][caption]" value="<?php echo stripslashes($video['caption']); ?>" placeholder="Title" class="form-control" style="width: 300px;" />
                                </fieldset>
                            </section>
                            <section class="view_action">
                                <a class="icon_delete" href="#" data-id="<?php echo $video['id']; ?>"></a>
                            </section>
                            <?php if ($vi['thumb']) { ?>
                                <div style="clear: both; padding: 20px 0 0 42px;">
                                    <a href="<?php echo $video['video']; ?>" target="_blank"><img src="<?php echo $vi['thumb']; ?>" alt="" style="max-width: 150px;" /></a>
                                </div>
                            <?php } ?>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <footer>
                    <input class="blue" type="submit" value="Save" />
                </footer>

                <?php

            } else {

                ?>

                <div style="padding: 10px; text-align: center; font-size: .75em;">
                    No item videos added yet.
                </div>

                <?php

            }

            ?>

        </div>

        </form>

    </div>

    <a name="categories"></a>

    <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

        <form enctype="multipart/form-data" action="./categories/process/" method="post">
        <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

        <div id="categories" class="table">

            <summary>
                <h2>Categories</h2>
            </summary>

            <?php

            // ARRAY OF CATEGORIES
            $cata = array();

            // GET CURRENT RELATIONS
            $query = "SELECT `category_id` FROM `" .$_uccms_ecomm->tables['item_categories']. "` WHERE (`item_id`=" .$id. ")";
            $q = sqlquery($query);
            while ($row = sqlfetch($q)) {
                $cata[$row['category_id']] = $row['category_id'];
            }

            // OUTPUT LIST
            function this_listElements($array) {
                global $cata;
                if (count($array) > 0) {
                    echo '<ul>';
                    foreach ($array as $element) {
                        if ($cata[$element['id']]) {
                            $checked = 'checked="checked"';
                        } else {
                            $checked = '';
                        }
                        echo '<li><input type="checkbox" name="category[' .$element['id']. ']" value="1" ' .$checked. ' /> ' .stripslashes($element['title']);
                        if (!empty($element['children'])) {
                            this_listElements($element['children']);
                        }
                        echo '</li>';
                    }
                    echo '</ul>';
                }
            }

            // BUILD LIST OFF ALL CATEGORIES
            echo this_listElements($_uccms_ecomm->getCategoryTree());

            ?>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

        </div>

        </form>

    </div>

    <?php

    // NOT A PACKAGE
    if ($item['type'] != 3) {

        // ATTRIBUTE ARRAY
        $aa = array();

        // EXISTING ATTRIBUTE ARRAY
        $eaa = array();

        // ATTRIBUTES FOR VARIANTS
        $vaa = [];

        // EXISTING GROUP ARRAY
        $ega = array();

        // GET ITEM ATTRIBUTES
        $ia_query = "
        SELECT ia.id AS `rel_id`, a.id, a.title, a.active, a.checkout, 'attribute' AS `what`
        FROM `" .$_uccms_ecomm->tables['item_attributes']. "` AS `ia`
        INNER JOIN `" .$_uccms_ecomm->tables['attributes']. "` AS `a` ON ia.attribute_id=a.id
        WHERE (ia.item_id=" .$item['id']. ") AND (ia.attribute_id!=0)
        ORDER BY ia.sort ASC, a.sort ASC, a.title ASC, ia.id ASC
        ";
        $ia_q = sqlquery($ia_query);
        if (sqlrows($ia_q) > 0) {
            while ($ia = sqlfetch($ia_q)) {
                $aa[$ia['rel_id']] = $ia;
                $eaa[$ia['id']] = $ia['id'];
                if (!$ia['checkout']) {
                    $vaa[$ia['rel_id']] = $ia;
                }
            }
        }

        // GET ITEM ATTRIBUTE GROUPS
        $iag_query = "
        SELECT ia.id AS `rel_id`, ag.id, ag.title, ag.active, 'group' AS `what`
        FROM `" .$_uccms_ecomm->tables['item_attributes']. "` AS `ia`
        INNER JOIN `" .$_uccms_ecomm->tables['attribute_groups']. "` AS `ag` ON ia.group_id=ag.id
        WHERE (ia.item_id=" .$item['id']. ") AND (ia.group_id!=0)
        ORDER BY ia.sort ASC, ag.sort ASC, ag.title ASC, ia.id ASC
        ";
        $iag_q = sqlquery($iag_query);
        if (sqlrows($iag_q) > 0) {
            while ($iag = sqlfetch($iag_q)) {
                $aa[$iag['rel_id']] = $iag;
                $ega[$iag['id']] = $iag['id'];
            }
        }

        ?>

        <a name="attributes"></a>

        <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

            <form id="form_attributes" enctype="multipart/form-data" action="./attributes/process/" method="post">
            <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

            <div id="attributes" class="table">

                <input type="hidden" name="sort" value="" />
                <input type="hidden" name="delete" value="" />

                <summary>
                    <h2>Attributes</h2>
                    <a class="add_resource add" href="#"><span></span>Add Attribute</a>
                </summary>

                <div class="add_container" style="display: none;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: auto; margin-bottom: 0px; border: 0px;">
                        <tr>
                            <td style="line-height: 30px;">
                                Add:&nbsp;
                            </td>
                            <td>
                                <select name="new_attribute-what" id="select_attribute-what">
                                    <option value="attribute">Attribute</option>
                                    <option value="group">Attribute Group</option>
                                    <option value="custom">Custom Attribute</option>
                                </select>
                            </td>
                            <td class="whats" style="padding-left: 15px;">

                                <?php

                                // AVAILABLE ATTRIBUTES ARRAY
                                $aaa = array();

                                // GET ALL ATTRIBUTES
                                $a_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`item_id`=0) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                                $a_q = sqlquery($a_query);
                                while ($a = sqlfetch($a_q)) {
                                    if (!in_array($a['id'], $eaa)) {
                                        $aaa[$a['id']] = $a;
                                    }
                                }

                                ?>

                                <div id="select_attribute-attribute" class="option">
                                    <?php if (count($aaa) > 0) { ?>
                                        <select name="new_attribute">
                                            <?php foreach ($aaa as $a) { ?>
                                                <option value="<?php echo $a['id']; ?>"><?php echo stripslashes($a['title']); ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } else { ?>
                                        <span style="line-height: 30px;">No available attributes.</span>
                                    <?php } ?>
                                </div>

                                <?php

                                // AVAILABLE GROUPS ARRAY
                                $aga = array();

                                // GET ALL GROUPS
                                $ag_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['attribute_groups']. "` ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                                $ag_q = sqlquery($ag_query);
                                while ($ag = sqlfetch($ag_q)) {
                                    if (!in_array($ag['id'], $ega)) {
                                        $aga[$ag['id']] = $ag;
                                    }
                                }

                                ?>

                                <div id="select_attribute-group" class="option">
                                    <?php if (count($aga) > 0) { ?>
                                        <select name="new_attribute-group">
                                            <?php foreach ($aga as $ag) { ?>
                                                <option value="<?php echo $ag['id']; ?>"><?php echo stripslashes($ag['title']); ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } else { ?>
                                        <span style="line-height: 30px;">No available groups.</span>
                                    <?php } ?>
                                </div>

                            </td>
                            <td>
                                <input class="blue" type="submit" name="do" value="Add" />
                            </td>
                        </tr>
                    </table>

                </div>

                <header style="clear: both;">
                    <span class="attribute_sort"></span>
                    <span class="attribute_title">Title</span>
                    <span class="attribute_status">Status</span>
                    <span class="attribute_edit">Edit</span>
                    <span class="attribute_delete">Delete</span>
                </header>

                <?php

                // HAVE ATTRIBUTES
                if (count($aa) > 0) {

                    ?>

                    <ul class="items ui-sortable">

                        <?php

                        // LOOP
                        foreach ($aa as $rel_id => $attribute) {
                            ?>

                            <li class="item contain" data-id="<?php echo $rel_id; ?>">
                                <section class="attribute_sort sort">
                                    <span class="icon_sort ui-sortable-handle"></span>
                                </section>
                                <section class="attribute_title">
                                    <?php if ($attribute['what'] == 'group') { ?>Group:&nbsp;<?php } ?><?php echo stripslashes($attribute['title']); ?>
                                </section>
                                <section class="attribute_status status_<?php if ($attribute['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                                    <?php if ($attribute['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                                </section>
                                <section class="attribute_edit">
                                    <?php if ($attribute['what'] == 'attribute') { ?><a class="icon_edit" title="Edit Attribute" href="./attributes/edit/?item_id=<?php echo $item['id']; ?>&id=<?php echo $rel_id; ?>"></a><?php } ?>
                                </section>
                                <section class="attribute_delete">
                                    <a href="#" class="icon_delete" title="Delete Attribute" data-id="<?php echo $rel_id; ?>"></a>
                                </section>
                            </li>

                            <?php
                        }

                        ?>

                    </ul>

                    <?php

                // NO ATTRIBUTES
                } else {
                    ?>
                    <div style="padding: 10px; text-align: center; font-size: .75em;">
                        No attributes added yet.
                    </div>
                    <?php
                }

                ?>

                <footer style="clear: both;">
                    <input class="blue" type="submit" value="Save" />
                </footer>

            </div>

            </form>

        </div>

        <?php

    }

    ?>

    <?php

    if ($_uccms_ecomm->getSetting('inventory_track')) { ?>

        <a name="inventory"></a>

        <div class="container legacy" style="margin-bottom: 0px; border: 0px none;">

            <?php if (count($aa) > 0) { ?>

                <form enctype="multipart/form-data" action="./variants/process/" method="post">
                <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

                <div id="variants" class="table">

                    <summary>
                        <h2>Variants & Inventory</h2>
                    </summary>

                    <?php foreach ($la as $location) { ?>

                        <div class="location loc-<?php echo $location['id']; ?>">

                            <?php if ($location['id'] != 0) { ?>
                                <a href="#" data-id="<?php echo $location['id']; ?>" class="heading">
                                    <?php echo stripslashes($location['title']); ?>
                                    <i class="arrow fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                            <?php } ?>

                            <div class="toggle-content" <?php if ($location['id'] != 0) { ?>style="display: none;"<?php } ?>>

                                <header class="clearfix" style="clear: both;">
                                    <span class="variant_title">Variant</span>
                                    <span class="variant_sku">SKU</span>
                                    <span class="variant_available">In Stock</span>
                                    <span class="variant_alert">Alert</span>
                                    <span class="variant_alert-at">Alert At</span>
                                </header>

                                <?php if (count((array)$va[$location['id']]) > 0) { ?>

                                    <ul class="items">
                                        <?php
                                        foreach ($va[$location['id']] as $var) {
                                            $vdata = json_decode(stripslashes($var['data']), true);
                                            ?>
                                            <input type="hidden" name="variant[<?php echo $var['id']; ?>][original_title]" value="<?php echo $vdata['original_title']; ?>" />
                                            <li class="item contain clearfix" data-id="<?php echo $var['id']; ?>">
                                                <section class="variant_title">
                                                    <input type="text" name="variant[<?php echo $var['id']; ?>][title]" value="<?php echo stripslashes($var['title']); ?>" placeholder="<?php echo $vdata['original_title']; ?>" class="form-control" />
                                                </section>
                                                <section class="variant_sku">
                                                    <input type="text" name="variant[<?php echo $var['id']; ?>][sku]" value="<?php echo stripslashes($var['sku']); ?>" placeholder="<?php echo stripslashes($item['sku']); ?>" class="form-control" />
                                                </section>
                                                <section class="variant_available">
                                                    <input type="text" name="variant[<?php echo $var['id']; ?>][inventory_available]" value="<?php echo (int)$var['inventory_available']; ?>" class="form-control" />
                                                </section>
                                                <section class="variant_alert">
                                                    <input type="checkbox" name="variant[<?php echo $var['id']; ?>][inventory_alert]" value="1" />
                                                </section>
                                                <section class="variant_alert-at">
                                                    <input type="text" name="variant[<?php echo $var['id']; ?>][inventory_alert_at]" value="<?php echo (int)$var['inventory_alert_at']; ?>" class="form-control" />
                                                </section>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <footer style="clear: both;">
                                        <input class="blue" type="submit" value="Save" />
                                    </footer>

                                <?php } else { ?>
                                    <div style="padding: 15px; text-align: center;">
                                        No variants for location. <a href="#" onclick="javascript:$('#form_attributes').submit();" class="button">Generate</a>
                                    </div>
                                <?php } ?>

                            </div>

                        </div>

                    <?php } ?>

                </div>

                </form>

            <?php } else { ?>

                <script type="text/javascript">

                    $(document).ready(function() {

                        // INVENTORY - TRACK TOGGLE
                        $('#package_track_items_inventory').change(function(e) {
                            if ($(this).prop('checked')) {
                                $('#toggle_package_track_items_inventory').hide();
                            } else {
                                $('#toggle_package_track_items_inventory').show();
                            }
                        });

                    });

                </script>

                <div class="container legacy">

                    <form enctype="multipart/form-data" action="./inventory/process/" method="post">
                    <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

                    <header>
                        <h2>Inventory</h2>
                    </header>

                    <section>

                        <div class="row">

                            <div class="col-md-6">

                                <?php

                                // PACKAGE
                                if ($item['type'] == 3) {

                                    ?>

                                    <fieldset class="form-group">
                                        <input id="package_track_items_inventory" type="checkbox" name="package_track_items_inventory" value="1" <?php if ($item['package_track_items_inventory']) { ?>checked="checked"<?php } ?> />
                                        <label class="for_checkbox">Track package items inventory</label>
                                    </fieldset>

                                    <?php

                                }

                                ?>

                                <div id="toggle_package_track_items_inventory" class="contain" style="<?php if (($item['type'] == 3) && ($item['package_track_items_inventory'])) { ?>display: none;<?php } ?> padding-top: 10px;">

                                    <fieldset class="form-group">
                                        <label>In Stock</label>
                                        <input type="text" name="inventory_available" value="<?php echo (int)$item['inventory_available']; ?>" class="form-control" style="width: 65px;" />
                                    </fieldset>

                                </div>

                            </div>

                            <div class="col-md-6">

                                <fieldset class="form-group">
                                    <input type="checkbox" name="inventory_alert" value="1" <?php if ($item['inventory_alert']) { ?>checked="checked"<?php } ?> />
                                    <label class="for_checkbox">Alert</label>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label>Alert At</label>
                                    <input type="text" name="inventory_alert_at" value="<?php echo (int)$item['inventory_alert_at']; ?>" class="form-control" style="width: 65px;" />
                                </fieldset>

                            </div>

                        </div>

                    </section>

                    <footer>
                        <input class="blue" type="submit" value="Save" />
                    </footer>

                    </form>

                </div>

            <?php } ?>

        </div>

        <?php

    }

    ?>

    <a name="discount"></a>

    <div id="discount" class="container legacy">

        <form enctype="multipart/form-data" action="./process-discount/" method="post">
        <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

        <header>
            <h2>Discount Settings</h2>
        </header>

        <section>

            <div class="row">

                <div class="col-md-6">

                    <h6>By Quantity</h6>

                    <p>Apply discount of</p>

                    <div class="clearfix" style="padding: 6px 0;">
                        <select name="discount[quantity_type]" class="custom_control form-control" style="display: inline-block; width: auto;">
                            <option value="percent" <?php if ($item['discount_quantity_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                            <option value="amount" <?php if ($item['discount_quantity_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                        </select>
                        &nbsp;
                        <input type="text" name="discount[quantity_amount]" value="<?php echo number_format((float)$item['discount_quantity_amount'], 2); ?>" class="form-control"style="display: inline-block; width: 80px;" />
                    </div>

                    <p>when the number of this item in the cart is at least</p>

                    <div class="clearfix" style="padding: 6px 0 0;">
                        <input type="text" name="discount[quantity_quantity]" value="<?php echo (int)$item['discount_quantity_quantity']; ?>" class="form-control"style="display: inline-block; width: 80px;" />
                    </div>

                </div>

                <div class="col-md-6">

                    <h6>By Total</h6>

                    <p>Apply discount of</p>

                    <div class="clearfix" style="padding: 6px 0;">
                        <select name="discount[total_type]" class="custom_control form-control" style="display: inline-block; width: auto;">
                            <option value="percent" <?php if ($item['discount_total_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                            <option value="amount" <?php if ($item['discount_total_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                        </select>
                        &nbsp;
                        <input type="text" name="discount[total_amount]" value="<?php echo number_format((float)$item['discount_total_amount'], 2); ?>" class="form-control" style="display: inline-block; width: 80px;" />
                    </div>

                    <p>when the total of this item in the cart is at least</p>

                    <div class="clearfix" style="padding: 6px 0 0;">
                        $<input type="text" name="discount[total_total]" value="<?php echo (int)$item['discount_total_total']; ?>" class="form-control" style="display: inline-block; width: 80px;" />
                    </div>

                </div>

            </div>

        </section>

        <footer>
            <input class="blue" type="submit" value="Save" />
        </footer>

        </form>

    </div>

<?php } ?>

<? include BigTree::path("admin/layouts/_html-field-loader.php") ?>