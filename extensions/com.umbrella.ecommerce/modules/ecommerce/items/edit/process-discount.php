<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['item']['id'];

    // HAVE ID
    if ($id) {

        // DB COLUMNS
        $columns = array(
            'discount_quantity_type'        => $_POST['discount']['quantity_type'],
            'discount_quantity_amount'      => $_POST['discount']['quantity_amount'],
            'discount_quantity_quantity'    => $_POST['discount']['quantity_quantity'],
            'discount_total_type'           => $_POST['discount']['total_type'],
            'discount_total_amount'         => $_POST['discount']['total_amount'],
            'discount_total_total'          => $_POST['discount']['total_total'],
            'updated_by'                    => $_uccms_ecomm->adminID()
        );

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['items']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

        // QUERY SUCCESSFUL
        if (sqlquery($query)) {
            $admin->growl('Item Discount', 'Discount information updated!');
        } else {
            $admin->growl('Item Discount', 'Failed to update item discount information.');
        }

    // NO ID
    } else {
        $admin->growl('Item Discount', 'Item not specified.');
    }

}

BigTree::redirect(MODULE_ROOT.'items/edit/?id=' .$id. '#discount');

?>