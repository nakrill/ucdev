<?php

//echo print_r($_POST);
//exit;

// echo $_uccms_ecomm->imageBridgeOut($string, 'categories');

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['item']['id'];

    // DB COLUMNS
    $item_columns = array(
        'booking_enabled'       => (int)$_POST['item']['booking_enabled'],
    );

    // DB QUERY
    $query = "UPDATE `" .$_uccms_ecomm->tables['items']. "` SET " .$_uccms_ecomm->createSet($item_columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";
    sqlquery($query);

    // UPDATE BOOKING INFO
    $booking_columns = array(
        'calendar_id'               => (int)$_POST['booking']['calendar_id'],
        'max_simultaneous_bookings' => (int)$_POST['booking']['max_simultaneous_bookings']
    );

    // SEE IF BOOKING RECORD EXISTS
    $booking_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$id. ")";
    $booking_q = sqlquery($booking_query);

    if (sqlrows($booking_q) > 0) {

        $booking = sqlfetch($booking_q);

        // UPDATE
        $query = "UPDATE `" .$_uccms_ecomm->tables['booking_items']. "` SET " .$_uccms_ecomm->createSet($booking_columns). " WHERE (`id`=" .$id. ")";
        sqlquery($query);

    } else {

        $booking_columns['id'] = $id;

        // CREATE RECORD
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_items']. "` SET " .$_uccms_ecomm->createSet($booking_columns). "";
        sqlquery($query);

        $booking['id'] = sqlid();

    }

    // HAVE BOOKING ID
    if ($booking['id']) {

        $admin->growl('Item', 'Booking settings updated!');

        // EXISTING DAY START / END
        $edse = array();

        // GET EXISTING DAY START / END
        $edse_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_days']. "` WHERE (`item_id`=" .$id. ")";
        $edse_q = sqlquery($edse_query);
        while ($day = sqlfetch($edse_q)) {
            $edse[$day['day']] = $day;
        }

        // HAVE DAY START / END
        if (is_array($_POST['item']['dse'])) {

            // LOOP
            foreach ($_POST['item']['dse'] as $day => $hours) {

                $day = (int)$day;

                // DB COLUMNS
                $columns = array(
                    'day'       => $day,
                    'bookable'  => (int)$hours['bookable'],
                    'start'     => date('H:i', strtotime($hours['start'])),
                    'end'       => date('H:i', strtotime($hours['end']))
                );

                // ALREADY HAVE DAY RECORD
                if ($edse[$day]) {

                    $query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendar_days']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$edse[$day]['id']. ")";

                    unset($edse[$day]);

                // NO RECORD
                } else {

                    $columns['item_id'] = $id;

                    $query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_calendar_days']. "` SET " .uccms_createSet($columns). "";

                }

                sqlquery($query);

            }

        }

        // HAVE LEFTOVER DAY START / END
        if (count($edse) > 0) {

            // LOOP
            foreach ($edse as $day) {

                // UPDATE - SET NOT BOOKABLE
                $update_query = "UPDATE `" .$_uccms_ecomm->tables['booking_calendar_days']. "` SET `bookable`=0 WHERE (`id`=" .$day['id']. ")";
                sqlquery($update_query);

            }

        }

        $eda = array(); // EXISTING DURATION ARRAY

        // GET EXISTING DURATIONS
        $duration_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`item_id`=" .$id. ")";
        $duration_q = sqlquery($duration_query);
        while ($duration = sqlfetch($duration_q)) {
            $eda[$duration['id']] = $duration;
        }

        // HAVE DURATIONS
        if (count($_POST['duration']) > 0) {

            $i = 0;

            // LOOP
            foreach ($_POST['duration'] as $duration_id => $duration) {

                $duration_id = (int)$duration_id;

                if ($duration['start']) {
                    $start = date('H:i', strtotime($duration['start']));
                } else {
                    $start = '00:00:00';
                }

                if ($duration['end']) {
                    $end = date('H:i', strtotime($duration['end']));
                } else {
                    $end = '00:00:00';
                }

                $columns = array(
                    'item_id'   => $id,
                    'visible'   => (int)$duration['visible'],
                    'title'     => $duration['title'],
                    'start'     => $start,
                    'end'       => $end,
                    'num'       => (int)$duration['num'],
                    'unit'      => $duration['unit'],
                    'days'      => implode(',', (array)$duration[days]),
                    'markup'    => number_format((float)$duration['markup'], 2, '.', '')
                );

                // IS NEW
                if (($duration_id === 0) && ($duration['num'])) {

                    $columns['sort'] = 999;

                    $new_query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_item_durations']. "` SET " .$_uccms_ecomm->createSet($columns). "";
                    sqlquery($new_query);

                // EXISTS
                } else {

                    $columns['sort'] = $i;

                    $update_query = "UPDATE `" .$_uccms_ecomm->tables['booking_item_durations']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$duration_id. ")";
                    sqlquery($update_query);

                    $i++;

                }

                unset($eda[$duration_id]);

            }

        }

        // HAVE REMAINING (OLD) DURATIONS
        if (count($eda) > 0) {

            // LOOP
            foreach ($eda as $duration_id => $duration) {

                // DELETE
                $delete_query = "DELETE FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`id`=" .$duration_id. ")";
                sqlquery($delete_query);

            }

        }

    }

}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id. '#booking');

?>