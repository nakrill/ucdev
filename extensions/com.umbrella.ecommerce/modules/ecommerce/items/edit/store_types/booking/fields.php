<?php

// GET BOOKING INFO
$booking_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
$booking_q = sqlquery($booking_query);
$booking = sqlfetch($booking_q);

if (!isset($booking['max_simultaneous_bookings'])) $booking['max_simultaneous_bookings'] = 1;

$dse = array();

// GET DAY START / STOP TIMES
$dse_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_days']. "` WHERE (`item_id`=" .$item['id']. ")";
$dse_q = sqlquery($dse_query);
while ($day = sqlfetch($dse_q)) {
    $dse[$day['day']] = $day;
}

?>

<style type="text/css">

    #booking .durations tr.item-extra {
        display: none;
    }
    #booking .durations td {
        padding-left: 8px;
        padding-right: 8px;
    }
    #booking .durations select.custom_control {
        display: none;
    }
    #booking .durations .icon_sort {
        float: none;
        width: 16px;
    }
    #booking .durations .icon_small {
        float: none;
        margin: 0;
        display: block;
    }
    #booking .durations .visibility a {
        font-size: 1.3em;
    }
    #booking .durations .visibility.invisible a {
        color: #ccc;
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        // DAY - BOOKABLE CLICK
        $('#booking_days .day input.bookable').click(function() {
            $(this).closest('.day').find('select').prop('disabled', !$(this).prop('checked'));
        });

        // DURATION TYPE TOGGLE
        $('#booking select[name="booking[duration_type]"]').change(function(e) {
            if ($(this).val() == 1) {
                $('#booking .durations').hide();
            } else {
                $('#booking .durations').show();
            }
        });

        // DURATIONS - SORTABLE
        $('#booking .durations table.items').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'tr',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
            }
        });

        // DURATIONS - ADD
        $('#booking .durations .add').click(function(e) {
            e.preventDefault();
            $('#add_booking_duration').toggle();
        });

        // DURATIONS - REMOVE
        $('#booking .durations .item .remove').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to remove this?')) {
                var el = $(this).closest('tr');
                $('#booking_duration-extra-' +el.data('duration-id')).remove();
                el.fadeOut(400, function() {
                    el.remove();
                });
            }
        });

        // DURATIONS - VISIBILITY
        $('#booking .durations .items .item .visibility a, #add_booking_duration .visibility a').click(function(e) {
            e.preventDefault();
            var id = $(this).closest('tr.item').data('duration-id');
            $input = $(this).prev('input');
            if ($input.val()) {
                $input.val('');
                $(this).closest('td').addClass('invisible');
                $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
                $(this).attr('title', 'Is invisible');
            } else {
                $input.val('1');
                $(this).closest('td').removeClass('invisible');
                $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
                $(this).attr('title', 'Is visible');
            }
        });

        // DURATIONS - DAYS AVAILABLE
        $('#booking .durations .items .item a.days, #add_booking_duration a.days').click(function(e) {
            e.preventDefault();
            var id = $(this).closest('tr.item').data('duration-id');
            $('#booking_duration-extra-' +id).toggle();
        });

    });

</script>

<a name="booking"></a>

<div id="booking" class="container">

    <form enctype="multipart/form-data" action="./store_types/booking/process/" method="post">
    <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

    <header>
        <h2>Booking</h2>
    </header>

    <section>

        <div class="contain">

            <fieldset>
                <input type="checkbox" name="item[booking_enabled]" value="1" <?php if ($item['booking_enabled']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Booking Enabled</label>
            </fieldset>

        </div>

        <?php if ($item['booking_enabled']) { ?>

            <div class="contain" style="margin-top: 15px;">

                <div class="left last">

                    <?php

                    // GET CALENDARS
                    $calendar_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['booking_calendars']. "` WHERE (`status`=1) ORDER BY `title` ASC";
                    $calendar_q = sqlquery($calendar_query);

                    ?>

                    <fieldset>
                        <label>Calendar</label>
                        <select name="booking[calendar_id]">
                            <option value="0">Self</option>
                            <?php if (sqlrows($calendar_q) > 0) { ?>
                                <option value="0" disabled="disabled">------------------------</option>
                                <?php while ($calendar = sqlfetch($calendar_q)) { ?>
                                    <option value="<?php echo $calendar['id']; ?>" <?php if ($calendar['id'] == $booking['calendar_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($calendar['title']); ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Max simultaneous bookings</label>
                        <input type="text" name="booking[max_simultaneous_bookings]" value="<?php echo (int)$booking['max_simultaneous_bookings']; ?>" style="width: 60px;" />
                    </fieldset>

                </div>

                <div class="right last">

                    <?php if (!$booking['calendar_id']) { ?>

                        <fieldset id="booking_days">
                            <label>Days:</label>
                            <div class="contain clearfix">

                                <table style="margin: 0px; border: 0px;">
                                    <?php

                                    // LOOP THROUGH DAYS OF WEEK
                                    foreach ($_uccms_ecomm->daysOfWeek() as $day_id => $day) {

                                        // THIS DAY
                                        $this_day = $dse[$day_id];

                                        // BOOKABLE OR NOT
                                        $bookable = $this_day['bookable'];

                                        ?>
                                        <tr class="day">
                                            <td style="white-space: nowrap; font-weight: bold;"><?php echo $day['title']; ?></td>
                                            <td style="width: 100%; padding-top: 10px;"><input type="checkbox" name="item[dse][<?=$day_id?>][bookable]" value="1" <?php if ($bookable) { ?>checked="checked"<?php } ?>class="bookable" /> Bookable</td>
                                            <td style="white-space: nowrap;">
                                                <select name="item[dse][<?=$day_id?>][start]" <?php if (!$bookable) { ?>disabled="disabled"<?php } ?>class="custom_control">
                                                    <?php
                                                    for ($i=0; $i<=23; $i++) {
                                                        for ($j=0; $j<=45; $j+=15) {
                                                            if (!$bookable) {
                                                                $selected = false;
                                                            } else {
                                                                if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['start']) {
                                                                    $selected = true;
                                                                } else {
                                                                    $selected = false;
                                                                }
                                                            }
                                                            ?>
                                                            <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                &nbsp;-&nbsp;
                                                <select name="item[dse][<?=$day_id?>][end]" <?php if (!$bookable) { ?>disabled="disabled"<?php } ?> class="custom_control">
                                                    <?php
                                                    for ($i=0; $i<=23; $i++) {
                                                        for ($j=0; $j<=45; $j+=15) {
                                                            if (!$bookable) {
                                                                $selected = false;
                                                            } else {
                                                                if (date('H:i:s', strtotime($i. ':' .$j)) == $this_day['end']) {
                                                                    $selected = true;
                                                                } else {
                                                                    $selected = false;
                                                                }
                                                            }
                                                            ?>
                                                            <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>

                            </div>
                        </fieldset>

                    <?php } ?>

                </div>

            </div>

            <div class="contain" style="margin-top: 15px;">

                <fieldset style="margin-bottom: 0px;">
                    <label>Durations</label>
                    <select name="booking[duration_type]">
                        <option value="0">Defined</option>
                        <? /*<option value="1" <?php if ($booking['duration_type'] == 1) { ?>selected="selected"<?php } ?>>Customer specified</option>*/ ?>
                    </select>
                </fieldset>

                <fieldset class="durations" style="margin-top: 10px;">

                    <div class="contain">
                        <div class="left last" style="margin-top: 9px;">
                            Durations
                        </div>
                        <div class="right last" style="margin: 0px; text-align: right;">
                            <a href="#" class="add button_small">Add</a>
                        </div>
                    </div>

                    <div id="add_booking_duration" style="display: none; padding: 5px 0 10px 0;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px; border: 0px none; background-color: #f5f5f5;">
                            <tr class="item" data-duration-id="0">
                                <td style="width: 100%;">
                                    <input type="text" name="duration[0][title]" value="" placeholder="Title" style="width: 204px;" />
                                </td>
                                <td style="padding-right: 5px;">
                                    <select name="duration[0][start]">
                                        <option value="">Day start</option>
                                        <?php
                                        for ($i=0; $i<=23; $i++) {
                                            for ($j=0; $j<=45; $j+=15) {
                                                ?>
                                                <option value="<?=$i. ':' .$j?>"><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td style="padding: 0px;">
                                    to
                                </td>
                                <td style="padding-left: 5px;">
                                    <select name="duration[0][end]">
                                        <option value="">Day end</option>
                                        <?php
                                        for ($i=0; $i<=23; $i++) {
                                            for ($j=0; $j<=45; $j+=15) {
                                                ?>
                                                <option value="<?=$i. ':' .$j?>"><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td style="padding-right: 5px;">
                                    <input type="text" name="duration[0][num]" value="" style="width: 34px;" />
                                </td>
                                <td style="padding-left: 0px;">
                                    <select name="duration[0][unit]">
                                        <option value="m">Minutes</option>
                                        <option value="h">Hours</option>
                                        <option value="d">Days</option>
                                    </select>
                                </td>
                                <td>
                                    <a href="#" title="Edit days available" class="days"><span class="icon_small icon_small_calendar"></span></a>
                                </td>
                                <td style="white-space: nowrap;">
                                    + $<input type="text" name="duration[0][markup]" value="0.00" style="display: inline-block; width: 45px;" />
                                </td>
                                <td class="visibility">
                                    <input type="hidden" name="duration[0][visible]" value="1" />
                                    <a href="#" title="Is visible"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
                                <td>
                                    <input type="submit" value="Add" class="button blue" style="height: auto; padding: 7px 10px;" />
                                </td>
                            </tr>
                            <tr id="booking_duration-extra-0" class="item-extra" data-duration-id="<?php echo $duration['id']?>">
                                <td colspan="10">

                                    <div class="days" style="display: inline-block; float: right; margin-right: 141px;">
                                        <?php
                                        $seldaya = explode(',', stripslashes($duration['days']));
                                        foreach ($_uccms_ecomm->daysOfWeek() as $day_id => $day) {
                                            ?>
                                            <div style="float: left; margin-left: 20px; line-height: 17px;">
                                                <input type="checkbox" name="duration[0][days][]" value="<?php echo $day['code']; ?>" checked="checked" /> <?php echo ucwords(strtolower($day['code'])); ?>
                                            </div>
                                        <?php } ?>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </div>

                    <?php

                    // GET ITEM DURATIONS
                    $duration_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC, `id` ASC";
                    $duration_q = sqlquery($duration_query);

                    // HAVE DURATIONS
                    if (sqlrows($duration_q) > 0) {

                        ?>

                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="items" style="margin-top: 5px; margin-bottom: 0px;">

                            <?php while ($duration = sqlfetch($duration_q)) { ?>

                                <tr id="booking_duration-<?php echo $duration['id']?>" class="item" data-duration-id="<?php echo $duration['id']?>">
                                    <td style="padding-right: 0px;">
                                        <span class="icon_sort ui-sortable-handle"></span>
                                    </td>
                                    <td style="width: 100%;">
                                        <input type="text" name="duration[<?php echo $duration['id']; ?>][title]" value="<?php echo stripslashes($duration['title']); ?>" placeholder="Title" style="width: 224px;" />
                                    </td>
                                    <td style="padding-right: 5px;">
                                        <select name="duration[<?php echo $duration['id']; ?>][start]">
                                            <option value="">Day start</option>
                                            <?php
                                            for ($i=0; $i<=23; $i++) {
                                                for ($j=0; $j<=45; $j+=15) {
                                                    $selected = false;
                                                    if ($duration['start'] != '00:00:00') {
                                                        if (date('H:i:00', strtotime($i. ':' .$j)) == $duration['start']) {
                                                            $selected = true;
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td style="padding: 0px;">
                                        to
                                    </td>
                                    <td style="padding-left: 5px;">
                                        <select name="duration[<?php echo $duration['id']; ?>][end]">
                                            <option value="">Day end</option>
                                            <?php
                                            for ($i=0; $i<=23; $i++) {
                                                for ($j=0; $j<=45; $j+=15) {
                                                    $selected = false;
                                                    if ($duration['end'] != '00:00:00') {
                                                        if (date('H:i:00', strtotime($i. ':' .$j)) == $duration['end']) {
                                                            $selected = true;
                                                        } else {
                                                            $selected = false;
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td style="padding-right: 5px;">
                                        <input type="text" name="duration[<?php echo $duration['id']; ?>][num]" value="<?php echo (int)$duration['num']; ?>" style="width: 34px;" />
                                    </td>
                                    <td style="padding-left: 0px;">
                                        <select name="duration[<?php echo $duration['id']; ?>][unit]">
                                            <option value="m" <?php if ($duration['unit'] == 'm') { ?>selected="selected"<?php } ?>>Minutes</option>
                                            <option value="h" <?php if ($duration['unit'] == 'h') { ?>selected="selected"<?php } ?>>Hours</option>
                                            <option value="d" <?php if ($duration['unit'] == 'd') { ?>selected="selected"<?php } ?>>Days</option>
                                        </select>
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="#" title="Edit days available" class="days"><span class="icon_small icon_small_calendar"></span></a>
                                    </td>
                                    <td style="white-space: nowrap;">
                                        + $<input type="text" name="duration[<?php echo $duration['id']; ?>][markup]" value="<?php echo $_uccms_ecomm->dbNumDecimal($duration['markup']); ?>" style="display: inline-block; width: 45px;" />
                                    </td>
                                    <td class="visibility <?php if (!$duration['visible']) { echo 'invisible'; } ?>">
                                        <input type="hidden" name="duration[<?php echo $duration['id']; ?>][visible]" value="<?php echo $duration['visible']; ?>" />
                                        <a href="#" title="Is <?php if (!$duration['visible']) { echo 'in'; } ?>visible"><i class="fa fa-eye<?php if (!$duration['visible']) { echo '-slash'; } ?>" aria-hidden="true"></i></a>
                                    </td>
                                    <td><a href="#" class="remove" title="Remove"><i class="fa fa-times"></i></a></td>
                                </tr>

                                <tr id="booking_duration-extra-<?php echo $duration['id']?>" class="item-extra" data-duration-id="<?php echo $duration['id']?>">
                                    <td colspan="10">

                                        <div class="days" style="display: inline-block; float: right; margin-right: 141px;">
                                            <?php
                                            $seldaya = explode(',', stripslashes($duration['days']));
                                            foreach ($_uccms_ecomm->daysOfWeek() as $day_id => $day) {
                                                ?>
                                                <div style="float: left; margin-left: 20px; line-height: 17px;">
                                                    <input type="checkbox" name="duration[<?php echo $duration['id']; ?>][days][]" value="<?php echo $day['code']; ?>" <?php if (in_array($day['code'], $seldaya)) { ?>checked="checked"<?php } ?> /> <?php echo ucwords(strtolower($day['code'])); ?>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    </td>
                                </tr>

                            <?php } ?>

                        </table>

                        <?php

                    }

                    ?>

                </fieldset>

            </div>

            <div class="contain" style="margin-top: 15px;">

                <fieldset>
                    <label>iCal</label>
                    <?php if ($booking['calendar_id']) { ?>
                        <input type="text" value="<?php echo WWW_ROOT . $_uccms_ecomm->storePath(); ?>/ical/calendar/?calendar_id=<?php echo $booking['calendar_id']; ?>&key=<?php echo md5($bigtree['config']['domain'] . $booking['calendar_id']); ?>" onclick="$(this).select();" />
                    <?php } else { ?>
                    <input type="text" value="<?php echo WWW_ROOT . $_uccms_ecomm->storePath(); ?>/ical/calendar/?item_id=<?php echo $item['id']; ?>&key=<?php echo md5($bigtree['config']['domain'] . $item['id']); ?>" onclick="$(this).select();" />
                        <?php } ?>
                    <label style="margin-bottom: 0px;"><small>Copy & paste the URL above into your calendar program to display bookings for this calendar.</small></label>
                </fieldset>

            </div>

            <div class="contain" style="margin-top: 15px;">

                <fieldset>
                    <label>Booking Widget Embed Code</label>
                    <textarea style="height: 45px;"><script src="<?php echo WWW_ROOT . $_uccms_ecomm->storePath(); ?>/widget/embed.js?store_type=booking&what=booking-calendar&vars[item_id]=<?php echo $item['id']; ?>" type="text/javascript"></script>
<div id="uccms-widget-container"></div></textarea>
                    <label style="margin-bottom: 0px;"><small>Code for embedded booking widget on another site.</small></label>
                </fieldset>

            </div>

        <?php } ?>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
    </footer>

    </form>

</div>

<?php if ($item['booking_enabled']) { ?>

    <?php

    // IS OWN CALENDAR
    if ($booking['calendar_id'] == 0) {

        ?>

        <link rel="stylesheet" href="/css/lib/daterangepicker.css">
        <script type="text/javascript" src="/js/lib/moment.js"></script>
        <script type="text/javascript" src="/js/lib/daterangepicker.js"></script>

        <style type="text/css">

            #blackout_dates .input {
                display: none;
            }
            #blackout_dates .calendar .blackout_dates_calendar {
                display: block;
            }
            #blackout_dates .calendar .blackout_dates_calendar .month-wrapper table .day {
                padding: 20px 0;
            }
            #blackout_dates .calendar .blackout_dates_calendar .month-wrapper table .day.off {
                color: #ccc;
            }
            <?php foreach ($_uccms_ecomm->blackoutReasons() as $reason_id => $reason) {
                if ($reason['color']) {
                    ?>
                    #blackout_dates .calendar .blackout_dates_calendar .month-wrapper table .day.off.reason-<?php echo $reason_id; ?> {
                        background-color: <?php echo $reason['color']; ?>;
                    }
                    <?php
                }
            }
            ?>
            #blackout_dates .add {
                float: right;
                margin-top: 10px;
            }
            #blackout_dates .dates_list {
                margin-top: 15px;
            }
            #blackout_dates .dates_list .item {
                margin-bottom: 5px;
                padding: 10px;
                background-color: #f5f5f5;
                line-height: 18px;
            }
            #blackout_dates .dates_list .item:last-child {
                margin-bottom: 0px;
            }
            #blackout_dates .dates_list .item .date {
                float: left;
            }
            #blackout_dates .dates_list .item .date .from, #blackout_dates .dates_list .item .date .to {
                font-weight: bold;
            }
            #blackout_dates .dates_list .item .reason {
                float: right;
                margin-right: 20px;
            }
            #blackout_dates .dates_list .item .notes {
                float: right;
                margin-right: 20px;
            }
            #blackout_dates .dates_list .item .remove {
                float: right;
                font-weight: bold;
            }
            #blackout_dates .dates_list .item .notes_toggle {
                display: none;
                clear: both;
                padding-top: 8px;
            }
            #blackout_dates .dates_list .item.template {
                display: none;
            }

        </style>

        <script type="text/javascript">

            var booking_avail = {};

            $(document).ready(function() {

                // CALENDAR
                $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                    item_id: <?php echo $item['id']; ?>,
                    from: '<?php echo date('Y-m-01'); ?>',
                    <?php if ($booking['calendar_id']) { ?>
                        calendar_id: <?php echo $booking['calendar_id']; ?>
                    <?php } ?>
                }, function(data) {
                    $.each(data, function(date, avail) {
                        booking_avail[date] = avail;
                    });
                    $('#blackout_dates .input').dateRangePicker({
                        inline: true,
                        container: '#blackout_dates .calendar',
                        alwaysOpen: true,
                        startDate: '<?php echo date('Y-m-d'); ?>',
                        extraClass: 'blackout_dates_calendar',
                        singleMonth: true,
                        customTopBar: 'Select the day(s) and then click Add button.',
                        beforeShowDay: function(t) {
                            var dt = moment(t).format('YYYY-MM-DD').toString();
                            var valid = 1;
                            var avail = 0;
                            var _class = '';
                            var ba = booking_avail[dt];
                            if (typeof ba === 'object') {
                                var av = ba.available;
                                if (typeof av === 'undefined') av = 0;
                                avail = av;
                                if (avail == 0) {
                                    console.log(ba);
                                    if (ba.reason_id) {
                                        _class = 'off reason-' +ba.reason_id;
                                    } else {
                                        _class = 'off';
                                    }
                                }
                            }
                            var _tooltip = '';
                            return [valid,_class,_tooltip];
                        },
                        beforeNextMonth: function(month, callback) {
                            $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                                item_id: <?php echo $item['id']; ?>,
                                from: month,
                                <?php if ($booking['calendar_id']) { ?>
                                    calendar_id: <?php echo $booking['calendar_id']; ?>
                                <?php } ?>
                            }, function(data) {
                                $.each(data, function(date, avail) {
                                    booking_avail[date] = avail;
                                });
                                callback();
                            }, 'json');
                        },
                        beforePrevMonth: function(month, callback) {
                            $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                                item_id: <?php echo $item['id']; ?>,
                                from: month,
                                <?php if ($booking['calendar_id']) { ?>
                                    calendar_id: <?php echo $booking['calendar_id']; ?>
                                <?php } ?>
                            }, function(data) {
                                $.each(data, function(date, avail) {
                                    booking_avail[date] = avail;
                                });
                                callback();
                            }, 'json');
                        }
                    });
                }, 'json');

                // ADD DATE(S)
                $('#blackout_dates .add').click(function(e) {
                    e.preventDefault();
                    var val = $('#blackout_dates .input').val();
                    if (val) {
                        var date = val.split(' to ');
                        var clone = $('#blackout_dates .dates_list .item.template').clone().appendTo('#blackout_dates .dates_list');
                        clone.removeClass('template');
                        clone.find('input[name="date[]"]').val(val);
                        clone.find('.date .from').text(date[0]);
                        if (date[0] != date[1]) {
                            clone.find('.date .from').after(' to <span class="to">' +date[1]+ '</span>');
                        }
                        clone.show();
                    } else {
                        alert('Please select your day(s).');
                    }
                });

                // NOTES EXPAND
                $('#blackout_dates .dates_list').on('click', '.item .notes a', function(e) {
                    e.preventDefault();
                    var item = $(this).closest('.item');
                    item.find('.notes_toggle').toggle();
                });

                // REMOVE DATE
                $('#blackout_dates .dates_list').on('click', '.item .remove a', function(e) {
                    e.preventDefault();
                    var item = $(this).closest('.item');
                    item.fadeOut(500, function() {
                        item.remove();
                    });
                });

            });

        </script>

        <?php

    }

    ?>

    <a name="booking_blackout"></a>

    <div id="booking_blackout" class="container">

        <form enctype="multipart/form-data" action="./store_types/booking/blackout/process/" method="post">
        <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

        <header>
            <h2>Booking - Blackout</h2>
        </header>

        <?php

        // IS OWN CALENDAR
        if ($booking['calendar_id'] == 0) {

            // BLACKOUT REASONS
            $blackout_reasons = $_uccms_ecomm->blackoutReasons();

            ?>

            <section>

                <div id="blackout_dates" class="contain">

                    <div class="left last">

                        <input value="" class="input" />

                        <div class="calendar"></div>

                        <a href="#" class="button add">Add</a>

                    </div>

                    <div class="right last">

                        <fieldset id="blackout_dates">
                            <label>Dates:</label>
                            <div class="contain">

                                <div class="dates_list">

                                    <div class="item template clearfix">
                                        <input type="hidden" name="date[]" value="" />
                                        <div class="date">
                                            <span class="from"></span>
                                        </div>
                                        <div class="remove"><a href="#"><i class="fa fa-times"></i></a></div>
                                        <div class="notes">
                                            <a href="#">notes</a>
                                        </div>
                                        <?php if (count($blackout_reasons) > 0) { ?>
                                            <div class="reason">
                                                <select name="reason[]" class="custom_control">
                                                    <option value="0">No reason</option>
                                                    <?php foreach ($blackout_reasons as $reason_id => $reason) { ?>
                                                        <option value="<?php echo $reason_id; ?>"><?php echo $reason['title']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        <?php } ?>
                                        <div class="notes_toggle">
                                            <textarea name="notes[]" style="width: 393px; height: 48px;"></textarea>
                                        </div>
                                    </div>

                                    <?php

                                    // GET DATES
                                    $bd_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_blackouts']. "` WHERE (`item_id`=" .$item['id']. ") AND (`dt_to`>='" .date('Y-m-d H:i:s'). "') ORDER BY `dt_from` ASC";
                                    $bd_q = sqlquery($bd_query);
                                    while ($bd = sqlfetch($bd_q)) {

                                        $dt_from    = date('Y-m-d', strtotime($bd['dt_from']));
                                        $dt_to      = date('Y-m-d', strtotime($bd['dt_to']));

                                        ?>

                                        <div class="item clearfix">
                                            <input type="hidden" name="date[]" value="<?php echo $dt_from; ?> to <?php echo $dt_to; ?>" />
                                            <div class="date">
                                                <span class="from"><?php echo $dt_from; ?></span><?php if ($dt_from != $dt_to) { ?> to <span class="to"><?php echo $dt_to; ?></span><?php } ?>
                                            </div>
                                            <div class="remove"><a href="#"><i class="fa fa-times"></i></a></div>
                                            <div class="notes">
                                                <a href="#">notes</a>
                                            </div>
                                            <?php if (count($blackout_reasons) > 0) { ?>
                                                <div class="reason">
                                                    <select name="reason[]" class="custom_control">
                                                        <option value="0">No reason</option>
                                                        <?php foreach ($blackout_reasons as $reason_id => $reason) { ?>
                                                            <option value="<?php echo $reason_id; ?>" <?php if ($reason_id == $bd['reason_id']) { ?>selected="selected"<?php } ?>><?php echo $reason['title']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            <?php } ?>
                                            <div class="notes_toggle">
                                                <textarea name="notes[]" style="width: 393px; height: 48px;"><?php echo stripslashes($bd['notes']); ?></textarea>
                                            </div>
                                        </div>

                                        <?php

                                    }

                                    ?>

                                </div>
                            </div>
                        </fieldset>

                    </div>

                </div>

            </section>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

            <?php

        // HAS CALENDAR
        } else {
            ?>
            <div style="padding: 15px; text-align: center;">
                Controlled by <a href="../../settings/booking/calendars/edit/?id=1#blackout">calendar</a>.
            </div>
            <?php
        }

        ?>

        </form>

    </div>

<?php } ?>