<?php

// echo $_uccms_ecomm->imageBridgeOut($string, 'categories');

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['item']['id'];

    // USE ITEM TITLE FOR SLUG IF NOT SPECIFIED
    if (!$_POST['item']['slug']) {
        $_POST['item']['slug'] = $_POST['item']['title'];
    }

    // NEW ITEM
    if (!$id) {

        // PRICE GROUPS ENABLED
        if ($_uccms_ecomm->getSetting('pricegroups_enabled')) {

            // USE RETAIL PRICE
            $_POST['item']['price'] = $_POST['pricegroup'][1]['price'];
        }

    }

    // DB COLUMNS
    $columns = array(
        'slug'                      => $_uccms_ecomm->makeRewrite($_POST['item']['slug']),
        'account_id'                => (int)$_POST['item']['account_id'],
        'active'                    => (int)$_POST['item']['active'],
        'sku'                       => $_POST['item']['sku'],
        'type'                      => (int)$_POST['item']['type'],
        'title'                     => $_POST['item']['title'],
        'description_short'         => $_POST['item']['description_short'],
        'description'               => $_POST['item']['description'],
        'price'                     => $_POST['item']['price'],
        'price_cost'                => $_POST['item']['price_cost'],
        'price_msrp'                => $_POST['item']['price_msrp'],
        'price_sale'                => $_POST['item']['price_sale'],
        'hide_price'                => (int)$_POST['item']['hide_price'],
        'after_tax'                 => (int)$_POST['item']['after_tax'],
        'quote'                     => (int)$_POST['item']['quote'],
        'shipping_price'            => $_POST['item']['shipping_price'],
        'shipping_cost'             => $_POST['item']['shipping_cost'],
        'shipping_free'             => (int)$_POST['item']['shipping_free'],
        'weight'                    => $_POST['item']['weight'],
        'dimensions_length'         => $_POST['item']['dimensions_length'],
        'dimensions_width'          => $_POST['item']['dimensions_width'],
        'dimensions_height'         => $_POST['item']['dimensions_height'],
        'meta_title'                => $_POST['item']['meta_title'],
        'meta_description'          => $_POST['item']['meta_description'],
        'meta_keywords'             => $_POST['item']['meta_keywords'],
        'tax_group'                 => $_POST['item']['tax_group'],
        'no_purchase'               => (int)$_POST['item']['no_purchase'],
        'no_search'                 => (int)$_POST['item']['no_search'],
        'featured'                  => (int)$_POST['item']['featured'],
        'not_recent'                => (int)$_POST['item']['not_recent'],
        'payout_calc_type'          => 'percent',
        'payout_calc_amount'        => (float)$_POST['item']['payout_calc_amount'],
        'updated_by'                => $_uccms_ecomm->adminID(),
        'email_account_on_order'    => (int)$_POST['item']['email_account_on_order'],
    );

    // HAVE ITEM ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['items']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO ITEM ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['items']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
            $admin->growl('Item', 'Item added!');
        } else {
            $admin->growl('Item', 'Item updated!');
        }

        // PRICE GROUPS ENABLED
        if ($_uccms_ecomm->getSetting('pricegroups_enabled')) {

            // GET STORE PRICEGROUPS
            $pga = $_uccms_ecomm->getSetting('pricegroups');

            // HAVE PRICE GROUPS
            if ((is_array($pga)) && (count($pga) > 0)) {

                // LOOP
                foreach ($pga as $group_id => $group) {

                    // DB COLUMNS
                    $pg_columns = array(
                        'item_id'       => $id,
                        'pricegroup_id' => (int)$group_id,
                        'enabled'       => (int)$_POST['pricegroup'][$group_id]['enabled'],
                        'price'         => (float)$_POST['pricegroup'][$group_id]['price'],
                        'qty_min'       => (int)$_POST['pricegroup'][$group_id]['qty_min']
                    );

                    unset($pgc);

                    // SEE IF WE HAVE RELATION
                    $pgc_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['item_pricegroups']. "` WHERE (`item_id`=" .$id. ") AND (`pricegroup_id`=" .(int)$group_id. ")";
                    $pgc_q = sqlquery($pgc_query);
                    $pgc = sqlfetch($pgc_q);

                    // HAVE RELATION
                    if ($pgc['id']) {
                        $pgu_query = "UPDATE `" .$_uccms_ecomm->tables['item_pricegroups']. "` SET " .uccms_createSet($pg_columns). " WHERE (`id`=" .$pgc['id']. ")";

                    // NO RELATION
                    } else {
                        $pgu_query = "INSERT INTO `" .$_uccms_ecomm->tables['item_pricegroups']. "` SET " .uccms_createSet($pg_columns). "";
                    }

                    // RUN QUERY
                    sqlquery($pgu_query);

                }

            }

        }

    // QUERY FAILED
    } else {
        $admin->growl('Item', 'Failed to save.');
    }

}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id);

?>