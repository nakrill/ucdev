<?php

/*
echo print_r($_FILES);
echo print_r($_POST);
exit;
*/

// CLEAN UP
$id = (int)$_POST['item']['id'];

// FORM SUBMITTED
if (is_array($_POST)) {

    // HAVE ITEM ID
    if ($id) {

        // HAVE VIDEOS
        if (is_array($_POST['video'])) {

            // LOOP
            foreach ($_POST['video'] as $video_id => $video) {

                /*
                // NOT NEW
                if ($video_id != 'new') {

                    // CLEAN UP
                    $video_id = (int)$video_id;

                    // HAVE VIDEO ID
                    if ($video_id) {

                        // DELETING EXISTING
                        if (!$_POST[$video_field]) {

                            // GET CURRENT VIDEO
                            $ex_query = "SELECT `video` FROM `" .$_uccms_ecomm->tables['item_videos']. "` WHERE (`id`=" .$video_id. ")";
                            $ex = sqlfetch(sqlquery($ex_query));

                            // THERE'S AN EXISTING VIDEO
                            if ($ex['video']) {

                                // UPDATE DATABASE
                                $query = "UPDATE `" .$_uccms_ecomm->tables['item_videos']. "` SET `video`='' WHERE (`id`=" .$video_id. ")";
                                sqlquery($query);

                            }

                        }

                    }

                }
                */

                // DB COLUMNS
                $columns = array(
                    'item_id'       => $id,
                    'sort'          => 999,
                    'video'         => $video['url'],
                    'caption'       => $video['caption'],
                    'updated_by'    => $_uccms_ecomm->adminID()
                );

                // NEW VIDEO
                if (($video_id == 'new') && ($columns['video'])) {

                    // INSERT INTO DB
                    $query = "INSERT INTO `" .$_uccms_ecomm->tables['item_videos']. "` SET " .uccms_createSet($columns). ", `updated_dt`=NOW()";

                // EXISTING VIDEO
                } else {

                    // UPDATE DB
                    $query = "UPDATE `" .$_uccms_ecomm->tables['item_videos']. "` SET " .uccms_createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$video_id. ")";

                }

                // RUN QUERY
                if ($query) {

                    sqlquery($query);

                    $admin->growl('Video', 'Video added!');

                }

            }

        }

        // SORT ARRAY
        $sorta = explode(',', $_POST['sort']);

        // ARE DELETING
        if ($_POST['delete']) {

            // GET ID'S TO DELETE
            $ida = explode(',', $_POST['delete']);

            // HAVE ID'S
            if (count($ida) > 0) {

                // LOOP
                foreach ($ida as $video_id) {

                    // CLEAN UP
                    $video_id = (int)$video_id;

                    // HAVE VALID ID
                    if ($video_id) {

                        // REMOVE FROM DATABASE
                        $query = "DELETE FROM `" .$_uccms_ecomm->tables['item_videos']. "` WHERE (`id`=" .$video_id. ")";
                        sqlquery($query);

                        // REMOVE FROM SORT ARRAY
                        if (($key = array_search($video_id, $sorta)) !== false) {
                            unset($sorta[$key]);
                        }


                    }

                }

            }

        }

        // HAVE VIDEOS TO SORT
        if (count($sorta) > 0) {

            $i = 0;

            // LOOP
            foreach ($sorta as $video_id) {

                // CLEAN UP
                $video_id = (int)$video_id;

                // HAVE VALID ID
                if ($video_id) {

                    // UPDATE DATABASE
                    $query = "UPDATE `" .$_uccms_ecomm->tables['item_videos']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$video_id. ")";
                    sqlquery($query);

                    $i++;

                }

            }

        }

    // ITEM ID NOT SPECIFIED
    } else {
        $admin->growl('Item', 'Item ID not specified.');
    }

}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id. '#videos');

?>