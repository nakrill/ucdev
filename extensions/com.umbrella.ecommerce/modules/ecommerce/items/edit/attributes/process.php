<?php

// CLEAN UP
$id = (int)$_POST['item']['id'];

// HAVE ITEM ID
if ($id) {

    // ADDING
    if ($_POST['do'] == 'Add') {

        unset($columns);

        // ADDING NEW ATTRIBUTE
        if ($_POST['new_attribute-what'] == 'attribute') {

            // CLEAN UP
            $attr_id = (int)$_POST['new_attribute'];

            // HAVE NEW ATTRIBUTE ID
            if ($attr_id) {

                // DB COLUMNS
                $columns = array(
                    'item_id'           => $id,
                    'sort'              => 999,
                    'attribute_id'      => $attr_id,
                    'updated_by'        => $_uccms_ecomm->adminID()
                );

            // NO ATTRIBUTE ID
            } else {
                $admin->growl('Item Attribute', 'No attribute specified to add.');
            }

        // ADDING NEW GROUP
        } else if ($_POST['new_attribute-what'] == 'group') {

            // CLEAN UP
            $group_id = (int)$_POST['new_attribute-group'];

            // HAVE NEW GROUP ID
            if ($group_id) {

                // DB COLUMNS
                $columns = array(
                    'item_id'           => $id,
                    'sort'              => 999,
                    'group_id'          => $group_id,
                    'updated_by'        => $_uccms_ecomm->adminID()
                );

            // NO ATTRIBUTE ID
            } else {
                $admin->growl('Item Attribute Group', 'No attribute group specified to add.');
            }

        // ADDING CUSTOM
        } else if ($_POST['new_attribute-what'] == 'custom') {

            // REDIRECT TO ATTRIBUTE ADD / EDIT PAGE
            BigTree::redirect(MODULE_ROOT. 'items/edit/attributes/edit/?item_id=' .$id);

        }

        // HAVE DB COLUMNS
        if (is_array($columns)) {

            // DB QUERY
            $query = "INSERT INTO `" .$_uccms_ecomm->tables['item_attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

            // QUERY SUCCESSFUL
            if (sqlquery($query)) {
                $admin->growl('Item Attribute', 'Attribute added!');

            // QUERY FAILED
            } else {
                $admin->growl('Item Attribute', 'Failed to add.');
            }

        }

    // CUSTOM ADD / UPDATE
    } else if ($_POST['attribute']) {

        // REQUIRED
        if ($_POST['attribute']['required_checkbox']) {
            $required = $_POST['attribute']['required'];
        } else {
            $required = '';
        }

        // FILE TYPES
        if ($_POST['attribute']['file_types']) {
            $ffta = array();
            $tfta = explode(',', $_POST['attribute']['file_types']);
            foreach ($tfta as $ft) {
                $ffta[] = trim(str_replace('.', '', $ft));
            }
            if (count($ffta) > 0) {
                $file_types = implode(',', $ffta);
            }
        }

        // DB COLUMNS
        $columns = array(
            'active'        => (int)$_POST['attribute']['active'],
            'title'         => $_POST['attribute']['title'],
            'description'   => $_POST['attribute']['description'],
            'type'          => $_POST['attribute']['type'],
            'limit'         => (int)$_POST['attribute']['limit'],
            'required'      => $required,
            'file_types'    => $file_types,
            'checkout'      => (int)$_POST['attribute']['checkout'],
            'value_type'    => $_POST['attribute']['value_type'],
            'updated_by'    => $_uccms_ecomm->adminID()
        );

        // DON'T INSERT BY DEFAULT
        $insert = false;

        // CLEAN UP
        $rel_id = (int)$_POST['rel']['id'];

        // HAVE RELATIONSHIP ID (EXISTING)
        if ($rel_id > 0) {

            // GET RELATIONSHIP INFO
            $rel_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`id`=" .$rel_id. ")";
            $rel = sqlfetch(sqlquery($rel_query));

            // GET ATTRIBUTE INFO FROM DB
            $attribute_query = "SELECT * FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`id`=" .$rel['attribute_id']. ")";
            $attribute_q = sqlquery($attribute_query);
            $attribute = sqlfetch($attribute_q);

            // IS ALREADY CUSTOM
            if ($attribute['item_id'] == $id) {

                // UPDATE CUSTOM ATTRIBUTE
                $query = "UPDATE `" .$_uccms_ecomm->tables['attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$attribute['id']. ")";

                // UPDATE SUCCESSFUL
                if (sqlquery($query)) {
                    $admin->growl('Item Attribute', 'Updated custom attribute.');
                } else {
                    $admin->growl('Item Attribute', 'Failed to update custom attribute.');
                }

            // IS GLOBAL
            } else {
                $insert = true;
            }

        // NO RELATIONSHIP ID (NEW)
        } else {
            $insert = true;
        }

        // IS INSERTING
        if ($insert) {

            // ADD TO DB COLUMNS
            $columns['item_id'] = $id;
            if ($rel_id > 0) $columns['options'] = $attribute['options']; // COPY OPTIONS FROM GLOBAL

            // CREATE CUSTOM ATTRIBUTE
            $query = "INSERT INTO `" .$_uccms_ecomm->tables['attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

            // CUSTOM ATTRIBUTE CREATED
            if (sqlquery($query)) {

                // NEW ATTRIBUTE ID
                $attribute['id'] = sqlid();

                // DB COLUMNS
                $columns = array(
                    'item_id'       => $id,
                    'attribute_id'  => $attribute['id'],
                    'updated_by'    => $_uccms_ecomm->adminID()
                );

                // HAVE RELATIONSHIP ID - UPDATE
                if ($rel_id > 0) {

                    // UPDATE RELATIONSHIP
                    $query = "UPDATE `" .$_uccms_ecomm->tables['item_attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$rel_id. ")";

                // NO RELATIONSHIP ID - CREATE
                } else {

                    $columns['sort'] = 999;

                    // CREATE RELATIONSHIP
                    $query = "INSERT INTO `" .$_uccms_ecomm->tables['item_attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

                }

                // RELATIONSHIP CREATE SUCCESSFUL
                if (sqlquery($query)) {
                    $admin->growl('Item Attribute', 'Created custom attribute.');

                    if (!$rel_id) $rel_id = sqlid();

                // CUSTOM ATTRIBUTE NOT CREATED
                } else {
                    $admin->growl('Item Attribute', 'Failed to create custom attribute relationship.');
                }

            // CUSTOM ATTRIBUTE CREATE FAILED
            } else {
                $admin->growl('Item Attribute', 'Failed to create custom attribute.');
            }

        }

        BigTree::redirect(MODULE_ROOT. 'items/edit/attributes/edit/?item_id=' .$id. '&id=' .$rel_id);
        exit;

    }

    // SORT ARRAY
    $sorta = explode(',', $_POST['sort']);

    // ARE DELETING
    if ($_POST['delete']) {

        // GET ID'S TO DELETE
        $ida = explode(',', $_POST['delete']);

        // HAVE ID'S
        if (count($ida) > 0) {

            // LOOP
            foreach ($ida as $rel_id) {

                // CLEAN UP
                $rel_id = (int)$rel_id;

                // HAVE VALID ID
                if ($rel_id) {

                    // GET RELATIONSHIP INFO
                    $rel_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`id`=" .$rel_id. ")";
                    $rel = sqlfetch(sqlquery($rel_query));

                    // REMOVE RELATIONSHIP FROM DATABASE
                    $query = "DELETE FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`id`=" .$rel_id. ")";
                    sqlquery($query);

                    // REMOVE FROM SORT ARRAY
                    if (($key = array_search($rel_id, $sorta)) !== false) {
                        unset($sorta[$key]);
                    }

                    // IS NORMAL ATTRIBUTE (NOT A GROUP)
                    if ($rel['attribute_id'] > 0) {

                        // DELETE ATTRIBUTE IF CUSTOM FOR ITEM
                        $query = "DELETE FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`id`=" .$rel['attribute_id']. ") AND (`item_id`=" .$id. ")";
                        sqlquery($query);

                    }

                }

            }

        }

    }

    // HAVE SORT ORDER
    if (count($sorta) > 0) {

        $i = 0;

        // LOOP
        foreach ($sorta as $rel_id) {

            // CLEAN UP
            $rel_id = (int)$rel_id;

            // HAVE VALID ID
            if ($rel_id) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_ecomm->tables['item_attributes']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$rel_id. ")";
                sqlquery($query);

                $i++;

            }

        }

    }

    // UPDATE VARIANTS FOR ITEM
    $_uccms_ecomm->item_updateVariants($id);

}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id. '#attributes');

?>