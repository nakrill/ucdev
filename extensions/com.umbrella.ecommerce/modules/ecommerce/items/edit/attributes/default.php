<?php

// CLEAN UP
$item_id    = (int)$_REQUEST['item_id'];
$rel_id     = (int)$_REQUEST['id'];

// HAVE ITEM ID
if ($item_id) {

    $aoa = array();

    // HAVE RELATIONSHIP ID
    if ($rel_id) {

        // GET INFO FROM DB
        $rel_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`id`=" .$rel_id. ")";
        $rel_q = sqlquery($rel_query);
        $rel = sqlfetch($rel_q);

        // HAVE ATTRIBUTE ID
        if ($rel['attribute_id']) {

            // GET INFO FROM DB
            $attribute_query = "SELECT * FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`id`=" .$rel['attribute_id']. ")";
            $attribute_q = sqlquery($attribute_query);
            $attribute = sqlfetch($attribute_q);

            // ATTRIBUTE OPTION ARRAY
            if ($attribute['options']) {
                $aoa = json_decode($attribute['options'], true);
            }

        }

    }

    ?>


    <style type="text/css">

        #options header {
            clear: both;
        }

        #options.table > ul li {
            height: auto;
            padding: 5px 0;
            line-height: inherit;
        }

        #options.table > ul li section {
            height: auto;
            overflow: visible;
        }

        #options .option_sort {
            width: 55px;
        }
        #options .option_title {
            width: 480px;
            text-align: left;
        }
        #options .option_markup {
            width: 200px;
            text-align: left;
        }
        #options .option_default {
            width: 150px;
            text-align: left;
        }
        #options .option_delete {
            width: 55px;
        }

        #options .option_default .radio_button {
            margin-top: 3px;
            margin-left: 12px;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // TYPE SELECT
            $('#select_type').change(function(e) {
                if (($(this).val() == 'checkbox') || ($(this).val() == 'people')) {
                    $('#attribute .limit').show();
                } else {
                    $('#attribute .limit').hide();
                }
                if ($(this).val() == 'file') {
                    $('#attribute .file_types').show();
                } else {
                    $('#attribute .file_types').hide();
                }
            });

            // REQUIRED TOGGLE
            $('#required').change(function(e) {
                if ($(this).prop('checked')) {
                    if ($('#select_type').val() != 'file') {
                        $('#toggle_required').show();
                    }
                } else {
                    $('#toggle_required').hide();
                }
            });

        });

    </script>

    <form enctype="multipart/form-data" action="../process/" method="post">
    <input type="hidden" name="item[id]" value="<?php echo $item_id; ?>" />
    <input type="hidden" name="rel[id]" value="<?php echo $rel['id']; ?>" />
    <input type="hidden" name="attribute" value="true" />

    <div id="attribute" class="container legacy">

        <header>
            <h2><?php if ($attribute['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Attribute</h2>
        </header>

        <section>

            <?php if (($attribute['id']) && ($attribute['item_id'] != $item_id)) { ?>
                <div style="padding-bottom: 15px;">
                    <span style="color: #FF0000;">You are editing a global attribute.</span>
                    <br />
                    If you save this, it will become a copy of the original attribute (any changes you make to the global attribute will not affect this anymore).
                </div>
            <?php } ?>

            <div class="row">

                <div class="col-md-6">

                    <fieldset class="form-group">
                        <label>Title</label>
                        <input type="text" name="attribute[title]" value="<?php echo stripslashes($attribute['title']); ?>" class="form-control" />
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Description</label>
                        <textarea name="attribute[description]" class="form-control" style="height: 60px;"><?php echo stripslashes($attribute['description']); ?></textarea>
                    </fieldset>

                </div>

                <div class="col-md-6">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="contain">

                                <fieldset class="form-group">
                                    <input type="checkbox" name="attribute[active]" value="1" <?php if ($attribute['active']) { ?>checked="checked"<?php } ?> />
                                    <label class="for_checkbox">Active</label>
                                </fieldset>

                                <div class="left inner_quarter type">
                                    <fieldset class="form-group">
                                        <label>Type</label>
                                        <select id="select_type" name="attribute[type]" class="custom_control form-control">
                                            <option value="dropdown" <?php if ($attribute['type'] == 'dropdown') { ?>selected="selected"<?php } ?>>Dropdown</option>
                                            <option value="checkbox" <?php if ($attribute['type'] == 'checkbox') { ?>selected="selected"<?php } ?>>Checkbox(s)</option>
                                            <option value="select_box" <?php if ($attribute['type'] == 'select_box') { ?>selected="selected"<?php } ?>>Select Box(s)</option>
                                            <option value="text" <?php if ($attribute['type'] == 'text') { ?>selected="selected"<?php } ?>>Text</option>
                                            <option value="textarea" <?php if ($attribute['type'] == 'textarea') { ?>selected="selected"<?php } ?>>Textarea</option>
                                            <option value="date" <?php if ($attribute['type'] == 'date') { ?>selected="selected"<?php } ?>>Date</option>
                                            <option value="time" <?php if ($attribute['type'] == 'time') { ?>selected="selected"<?php } ?>>Time</option>
                                            <option value="file" <?php if ($attribute['type'] == 'file') { ?>selected="selected"<?php } ?>>File</option>
                                            <option value="width_height" <?php if ($attribute['type'] == 'width_height') { ?>selected="selected"<?php } ?>>Width x Height</option>
                                            <option value="people" <?php if ($attribute['type'] == 'people') { ?>selected="selected"<?php } ?>>People</option>
                                        </select>
                                    </fieldset>
                                </div>

                                <div class="right inner_quarter limit" style="<?php if (($attribute['type'] != 'checkbox') && ($attribute['type'] != 'people')) { ?>display: none;<?php } ?>">
                                    <fieldset class="form-group">
                                        <label>Limit</label>
                                        <input type="text" name="attribute[limit]" value="<?php echo (int)$attribute['limit']; ?>" class="form-control" />
                                        <label><small>The number of options a person may select. 0 for no limit.</small></label>
                                    </fieldset>
                                </div>

                                <div class="right inner_quarter file_types" style="<?php if ($attribute['type'] != 'file') { ?>display: none;<?php } ?>">
                                    <fieldset class="form-group">
                                        <label>Allowed File Types</label>
                                        <input type="text" name="attribute[file_types]" value="<?php echo $attribute['file_types']; ?>" class="form-control" style="width: 180px;" />
                                        <label><small>Separate by comma. Ex: jpg, png, pdf</small></label>
                                    </fieldset>
                                </div>

                            </div>

                            <fieldset class="form-group">
                                <input type="checkbox" name="attribute[checkout]" value="1" <?php if ($attribute['checkout']) { ?>checked="checked"<?php } ?> />
                                <label class="for_checkbox">Checkout question</label>
                            </fieldset>

                        </div>

                        <div class="col-md-6">

                            <div class="contain">

                                <fieldset class="form-group">
                                    <input id="required" type="checkbox" name="attribute[required_checkbox]" value="1" <?php if ($attribute['required']) { ?>checked="checked"<?php } ?> />
                                    <label class="for_checkbox">Required</label>
                                </fieldset>

                                <fieldset id="toggle_required" style="<?php if ((!$attribute['required']) || ($attribute['type'] == 'file')) { ?>display: none;<?php } ?>">
                                    <label>Required Type</label>
                                    <select id="select_type" name="attribute[required]" class="custom_contorl form-control">
                                        <option value="general" <?php if ($attribute['required'] == 'general') { ?>selected="selected"<?php } ?>>General</option>
                                        <option value="number" <?php if ($attribute['required'] == 'number') { ?>selected="selected"<?php } ?>>Number</option>
                                        <option value="email" <?php if ($attribute['required'] == 'email') { ?>selected="selected"<?php } ?>>Email</option>
                                        <option value="url" <?php if ($attribute['required'] == 'url') { ?>selected="selected"<?php } ?>>URL</option>
                                    </select>
                                </fieldset>

                            <fieldset class="form-group">
                                <label>Value Type</label>
                                <select id="select_type" name="attribute[value_type]" class="custom_control form-control">
                                    <option value="">None</option>
                                    <option value="firstname" <?php if ($attribute['value_type'] == 'firstname') { ?>selected="selected"<?php } ?>>First Name</option>
                                    <option value="lastname" <?php if ($attribute['value_type'] == 'lastname') { ?>selected="selected"<?php } ?>>Last Name</option>
                                    <option value="name" <?php if ($attribute['value_type'] == 'name') { ?>selected="selected"<?php } ?>>Name</option>
                                    <option value="address" <?php if ($attribute['value_type'] == 'address') { ?>selected="selected"<?php } ?>>Address</option>
                                    <option value="city" <?php if ($attribute['value_type'] == 'city') { ?>selected="selected"<?php } ?>>City</option>
                                    <option value="state" <?php if ($attribute['value_type'] == 'state') { ?>selected="selected"<?php } ?>>State</option>
                                    <option value="zip" <?php if ($attribute['value_type'] == 'zip') { ?>selected="selected"<?php } ?>>Zip</option>
                                    <option value="phone" <?php if ($attribute['value_type'] == 'phone') { ?>selected="selected"<?php } ?>>Phone</option>
                                    <option value="email" <?php if ($attribute['value_type'] == 'email') { ?>selected="selected"<?php } ?>>Email</option>
                                    <option value="age" <?php if ($attribute['value_type'] == 'age') { ?>selected="selected"<?php } ?>>Age</option>
                                    <option value="dob" <?php if ($attribute['value_type'] == 'dob') { ?>selected="selected"<?php } ?>>Date of Birth</option>
                                    <option value="gender" <?php if ($attribute['value_type'] == 'gender') { ?>selected="selected"<?php } ?>>Gender</option>
                                    <option value="pg" <?php if ($attribute['value_type'] == 'pg') { ?>selected="selected"<?php } ?>>Parent / Guardian</option>
                                    <option value="pg-name" <?php if ($attribute['value_type'] == 'pg-name') { ?>selected="selected"<?php } ?>>Parent / Guardian - Name</option>
                                    <option value="pg-phone" <?php if ($attribute['value_type'] == 'pg-phone') { ?>selected="selected"<?php } ?>>Parent / Guardian - Phone</option>
                                    <option value="ec" <?php if ($attribute['value_type'] == 'ec') { ?>selected="selected"<?php } ?>>Emergency Contact</option>
                                    <option value="ec-name" <?php if ($attribute['value_type'] == 'ec-name') { ?>selected="selected"<?php } ?>>Emergency Contact - Name</option>
                                    <option value="ec-phone" <?php if ($attribute['value_type'] == 'ec-phone') { ?>selected="selected"<?php } ?>>Emergency Contact - Phone</option>
                                    <option value="insurance" <?php if ($attribute['value_type'] == 'insurance') { ?>selected="selected"<?php } ?>>Insurance</option>
                                    <option value="agree" <?php if ($attribute['value_type'] == 'agree') { ?>selected="selected"<?php } ?>>Agree</option>
                                    <option value="notes" <?php if ($attribute['value_type'] == 'notes') { ?>selected="selected"<?php } ?>>Notes</option>
                                </select>
                            </fieldset>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <footer>
            <input class="btn btn-primary" type="submit" value="Save" />
            <a class="btn btn-secondary" href="../../?id=<?php echo $item_id; ?>#attributes">&laquo; Back</a>
        </footer>

    </div>

    </form>

    <?php if (($attribute['id']) && ($attribute['type'] != 'file')) { ?>

        <script type="text/javascript">

            $(document).ready(function() {

                // OPTIONS - SORTABLE
                $('#options ul.ui-sortable').sortable({
                    handle: '.icon_sort',
                    axis: 'y',
                    containment: 'parent',
                    items: 'li',
                    //placeholder: 'ui-sortable-placeholder',
                    update: function() {
                    }
                });

                // OPTIONS - ADD
                $('#options .add_resource').click(function(e) {
                    e.preventDefault();
                    var key = $('#options ul.ui-sortable li').length + 1;
                    var el = $('#option-new').clone();
                    el.html(el.html().replace(/###/g, key)).removeAttr('id').appendTo('#options ul.ui-sortable').show();
                    //var radio = el.find('input[type=radio]');
                    //radio.customControl = BigTreeRadioButton(radio);
                });

                // OPTIONS - DEFAULT RADIO CLICK
                $('#options .items .item .option_default input').click(function(e) {
                    console.log('click');
                    var key = $(this).attr('data-key');
                    $('#options .items .item .option_default input').each(function() {
                        if ($(this).attr('data-key') != key) {
                            $(this).removeAttr('checked');
                            $(this).next('.radio_button').find('a').removeClass('checked');
                        }
                    });
                });

                // OPTIONS - DELETE
                $('#options .items').on('click', '.item .icon_delete', function(e) {
                    e.preventDefault();
                    if (confirm('Are you sure you want to delete this?')) {
                        $(this).closest('.item').fadeOut(500, function() {
                            $(this).remove();
                        });
                    }
                });

            });

        </script>

        <a name="options"></a>

        <div class="container" style="border: 0px none;">

            <form enctype="multipart/form-data" action="../process-options/" method="post">
            <input type="hidden" name="item[id]" value="<?php echo $item_id; ?>" />
            <input type="hidden" name="rel[id]" value="<?php echo $rel['id']; ?>" />

            <div id="options" class="table">

                <input type="hidden" name="sort" value="" />
                <input type="hidden" name="delete" value="" />

                <summary>
                    <h2>Options</h2>
                    <a class="add_resource add" href="#"><span></span>Add Option</a>
                </summary>

                <header>
                    <span class="option_sort"></span>
                    <span class="option_title">Name</span>
                    <span class="option_markup">Markup</span>
                    <span class="option_default">Default</span>
                    <span class="option_delete">Delete</span>
                </header>

                <ul class="items ui-sortable">

                    <?php

                    foreach ($aoa as $key => $option) {

                        if ($option['markup']) {
                            number_format($option['markup'], 2);
                        } else {
                            $option['markup'] = '';
                        }

                        ?>

                        <li class="item contain">
                            <section class="option_sort sort">
                                <span class="icon_sort ui-sortable-handle"></span>
                            </section>
                            <section class="option_title">
                                <input type="text" name="option[<?php echo $key; ?>][title]" value="<?php echo stripslashes($option['title']); ?>" placeholder="Name" class="form-control" style="width: 300px;" />
                            </section>
                            <section class="option_markup">
                                $<input type="text" name="option[<?php echo $key; ?>][markup]" value="<?php echo $option['markup']; ?>" placeholder="0.00" class="form-control" style="display: inline-block; width: 100px;" />
                            </section>
                            <section class="option_default">
                                <input type="radio" name="option[<?php echo $key; ?>][default]" value="1" data-key="<?php echo $key; ?>" <?php if ($option['default']) { ?>checked="checked"<?php } ?> />
                            </section>
                            <section class="option_delete view_action">
                                <a class="icon_delete" href="#"></a>
                            </section>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <footer>
                    <input class="blue" type="submit" value="Save" />
                </footer>

            </div>

            </form>

            <li id="option-new" class="item contain" style="display: none;">
                <section class="option_sort sort">
                    <span class="icon_sort ui-sortable-handle"></span>
                </section>
                <section class="option_title">
                    <input type="text" name="option[###][title]" value="" placeholder="Name" class="form-control" style="width: 300px;" />
                </section>
                <section class="option_markup">
                    $<input type="text" name="option[###][markup]" value="" placeholder="0.00" class="form-control" style="display: inline-block; width: 100px;" />
                </section>
                <section class="option_default">
                    <input type="radio" name="option[###][default]" value="1" data-key="###" />
                </section>
                <section class="option_delete view_action">
                    <a class="icon_delete" href="#"></a>
                </section>
            </li>

        </div>

        <?php

    }

// NO ITEM ID SPECIFIED
} else {
    $admin->growl('Item Attribute', 'No item ID specified.');
    BigTree::redirect(MODULE_ROOT. 'items/');
}

?>