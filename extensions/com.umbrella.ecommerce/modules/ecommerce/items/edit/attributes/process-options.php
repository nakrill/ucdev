<?php

/*
echo print_r($_POST);
exit;
*/

// CLEAN UP
$id = (int)$_POST['item']['id'];

// HAVE ITEM ID
if ($id) {

    // CLEAN UP
    $rel_id = (int)$_POST['rel']['id'];

    // HAVE RELATIONSHIP ID
    if ($rel_id > 0) {

        // GET RELATIONSHIP INFO
        $rel_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`id`=" .$rel_id. ")";
        $rel = sqlfetch(sqlquery($rel_query));

        // GET ATTRIBUTE INFO FROM DB
        $attribute_query = "SELECT * FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`id`=" .$rel['attribute_id']. ")";
        $attribute_q = sqlquery($attribute_query);
        $attribute = sqlfetch($attribute_q);

        // IS ALREADY CUSTOM
        if ($attribute['item_id'] == $id) {

            // NOTHING TO DO FOR NOW?

        // IS NEW ATTRIBUTE
        } else {

            // CLEAR FIELDS WE DON'T WANT COPIED
            unset($attribute['id']);
            unset($attribute['updated_dt']);

            // DB COLUMNS
            $columns = $attribute;

            // ADD DB COLUMNS
            $columns['item_id']    = $id;
            $columns['updated_by'] = $_uccms_ecomm->adminID();

            // CREATE CUSTOM ATTRIBUTE
            $query = "INSERT INTO `" .$_uccms_ecomm->tables['attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

            // CUSTOM ATTRIBUTE CREATED
            if (sqlquery($query)) {

                // NEW ATTRIBUTE ID
                $attribute['id'] = sqlid();

                // DB COLUMNS
                $columns = array(
                    'item_id'       => $id,
                    'attribute_id'  => $attribute['id'],
                    'updated_by'    => $_uccms_ecomm->adminID()
                );

                // UPDATE RELATIONSHIP
                $query = "UPDATE `" .$_uccms_ecomm->tables['item_attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$rel_id. ")";

                // RELATIONSHIP CREATE SUCCESSFUL
                if (sqlquery($query)) {
                    $admin->growl('Item Attribute', 'Created custom attribute.');

                // CUSTOM ATTRIBUTE NOT CREATED
                } else {
                    $admin->growl('Item Attribute', 'Failed to create custom attribute relationship.');
                }

            // CUSTOM ATTRIBUTE CREATE FAILED
            } else {
                $admin->growl('Item Attribute', 'Failed to create custom attribute.');
            }

        }

        // OPTIONS
        if (count($_POST['option']) > 0) {
            $options = json_encode($_POST['option']);
        } else {
            $options = '';
        }

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['attributes']. "` SET `options`='" .$options. "', `updated_dt`=NOW() WHERE (`id`=" .$attribute['id']. ")";

        // QUERY SUCCESSFUL
        if (sqlquery($query)) {
            $admin->growl('Attribute Options', 'Saved!');

        // QUERY FAILED
        } else {
            $admin->growl('Attribute Options', 'Failed to save.');
        }

    // NO RELATIONSHIP ID
    }

    // UPDATE VARIANTS FOR ITEM
    $_uccms_ecomm->item_updateVariants($id);

// NO ITEM ID
}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id. '#attributes');

?>