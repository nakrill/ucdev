<?php

$admin->requireLevel(1);

// ITEM (PACKAGE) ID
$id = $_POST['item']['id'];

// HAVE ITEM (PACKAGE) ID
if ($id) {

    // MODULE CLASS
    if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

    // NEW ID
    if ($_POST['new']['id']) {

        $columns = array(
            'package_id'    => $id,
            'item_id'       => (int)$_POST['new']['id'],
            'variant_id'    => (int)$_POST['new']['variant_id'],
            'quantity'      => (int)$_POST['new']['quantity']
        );

        // ADD TO PACKAGE
        $add_query = "INSERT INTO `" .$_uccms_ecomm->tables['item_package_items']. "` SET " .uccms_createSet($columns);
        if (sqlquery($add_query)) {
            $admin->growl('Package', 'Item added to package!');
        } else {
            $admin->growl('Package', 'Failed to add item to package.');
        }

    }

    // HAVE ITEMS
    if (count($_POST['pitem']) > 0) {

        // LOOP
        foreach ($_POST['pitem'] as $pitem_id => $pitem) {

            $pitem_id = (int)$pitem_id;

            $columns = array(
                'quantity'  => (int)$pitem['quantity']
            );

            // UPDATE DB
            $update_query = "UPDATE `" .$_uccms_ecomm->tables['item_package_items']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$pitem_id. ")";
            sqlquery($update_query);

        }

    }

    // SORT ARRAY
    $sorta = explode(',', $_POST['sort']);

    // ARE DELETING
    if ($_POST['delete']) {

        // GET ID'S TO DELETE
        $ida = explode(',', $_POST['delete']);

        // HAVE ID'S
        if (count($ida) > 0) {

            // LOOP
            foreach ($ida as $rel_id) {

                // CLEAN UP
                $rel_id = (int)$rel_id;

                // HAVE VALID ID
                if ($rel_id) {

                    // REMOVE RELATIONSHIP FROM DATABASE
                    $query = "DELETE FROM `" .$_uccms_ecomm->tables['item_package_items']. "` WHERE (`id`=" .$rel_id. ")";
                    sqlquery($query);

                    // REMOVE FROM SORT ARRAY
                    if (($key = array_search($rel_id, $sorta)) !== false) {
                        unset($sorta[$key]);
                    }

                }

            }

        }

    }

    // HAVE SORT ORDER
    if (count($sorta) > 0) {

        $i = 0;

        // LOOP
        foreach ($sorta as $rel_id) {

            // CLEAN UP
            $rel_id = (int)$rel_id;

            // HAVE VALID ID
            if ($rel_id) {

                // UPDATE DATABASE
                $query = "UPDATE `" .$_uccms_ecomm->tables['item_package_items']. "` SET `sort`='" .$i. "' WHERE (`id`=" .$rel_id. ")";
                sqlquery($query);

                $i++;

            }

        }

    }

// MISSING PACKAGE ID
} else {
    $admin->growl('Package', 'Package ID not specified.');
}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id. '#package-items');

?>