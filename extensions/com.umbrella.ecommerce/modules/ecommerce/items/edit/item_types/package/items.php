<style type="text/css">

    #package-items .add_container {
        padding: 10px;
        background-color: #fafafa;
        border-bottom: 1px solid #ccc;
    }
    #package-items .add_container td {
        vertical-align: top;
    }

    #package-items.table section a, #categories.table section a {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    #package-items.table > ul li {
        height: auto;
        padding: 10px;
        line-height: 1.2em;
    }
    #package-items.table > ul li section {
        height: auto;
        overflow: visible;
        white-space: normal;
    }
    #package-items.table > header span:first-child {
        padding-left: 10px;
    }
    #package-items.table > ul li section:first-child {
        padding-left: 0px;
    }

    /*
    #package-items .items .item .title {
        width: 880px;
        padding-top: 10px;
        text-align: left;
    }
    #package-items .items .item .title fieldset label {
        margin-bottom: 2px;
        text-align: left;
    }
    */

    #package-items .pitem_sort {
        width: 40px;
    }
    #package-items .pitem_sort .icon_sort {
        margin: 0px;
    }
    #package-items .pitem_title {
        width: 300px;
        text-align: left;
    }
    #package-items .pitem_options {
        width: 390px;
        text-align: left;
    }
    #package-items .pitem_quantity {
        width: 150px;
        text-align: left;
    }
    #package-items .pitem_delete {
        width: 55px;
        text-align: right;
    }
    #package-items .pitem_delete a {
        display: block;
        float: right;
    }
    #package-items .items .pitem_sort, #package-items .items .pitem_delete {
        padding-top: 5px;
    }
    #package-items .items .pitem_title, #package-items .items .pitem_options {
        padding-top: 10px;
    }

    #package-items.table .currently .remove_resource {
        margin-top: -3px;
        margin-bottom: -10px;
    }

    #package-items .selectivity-result-item img, #package-items .selectivity-single-selected-item img {
        width: auto;
        height: 48px;
    }
    #package-items .selectivity-single-select input[type="text"] {
        height: auto;
    }
    #package-items .selectivity-placeholder {
        color: #666;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {

        // ITEMS - SORTABLE
        $('#package-items ul.ui-sortable').sortable({
            handle: '.icon_sort',
            axis: 'y',
            containment: 'parent',
            items: 'li',
            //placeholder: 'ui-sortable-placeholder',
            update: function() {
                var rows = $(this).find('li');
                var sort = [];
                rows.each(function() {
                    sort.push($(this).attr('id').replace('row_', ''));
                });
                $('#package-items input[name="sort"]').val(sort.join());
            }
        });

        // ITEMS - ADD CLICK
        $('#package-items .add_resource').click(function(e) {
            e.preventDefault();
            if ($('#package-items .add_container').is(':visible')) {
                $('#package-items .add_container').hide();
            } else {
                $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/item-types/package/add-item-interface/', {
                    'id': <?php echo $item['id']; ?>
                }, function(data) {
                    $('#package-items .add_container').html(data).show();
                }, 'html');
            }
        });

        // ITEMS - REMOVE CLICK
        $('#package-items .icon_delete').click(function(e) {
            e.preventDefault();
            if (confirm('Are you sure you want to delete this?')) {
                var id = $(this).attr('data-id');
                var del = $('#package-items input[name="delete"]').val();
                if (del) del += ',';
                del += id;
                $('#package-items input[name="delete"]').val(del);
                $('#package-items li#row_' +id).fadeOut(500);
            }
        });

    });
</script>

<a name="package-items"></a>

<div class="container" style="margin-bottom: 0px; border: 0px none;">

    <form enctype="multipart/form-data" action="./item_types/package/items_process/" method="post">
    <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

    <div id="package-items" class="table">

        <input type="hidden" name="sort" value="" />
        <input type="hidden" name="delete" value="" />

        <summary>
            <h2>Package Items</h2>
            <a class="add_resource add" href="#"><span></span>Add Item</a>
        </summary>

        <div class="add_container" style="display: none;"></div>

        <header style="clear: both;">
            <span class="pitem_sort"></span>
            <span class="pitem_title">Item</span>
            <span class="pitem_options">Options</span>
            <span class="pitem_quantity">Quantity</span>
            <span class="pitem_delete">Delete</span>
        </header>

        <?php

        // GET PACKAGE ITEMS
        $pitems_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_package_items']. "` WHERE (`package_id`=" .$item['id']. ") ORDER BY `sort` ASC";
        $pitems_q = sqlquery($pitems_query);

        $num_pitems = sqlrows($pitems_q);

        // HAVE PACKAGE ITEMS
        if ($num_pitems > 0) {

            ?>

            <ul class="items ui-sortable">

                <?php

                // LOOP
                while ($pitem = sqlfetch($pitems_q)) {

                    // GET ITEM
                    $pitemd = $_uccms_ecomm->getItem($pitem['item_id']);

                    ?>

                    <li id="row_<?php echo $pitem['id']; ?>" class="item contain">
                        <section class="pitem_sort">
                            <span class="icon_sort ui-sortable-handle"></span>
                        </section>
                        <section class="pitem_title">
                            <?php echo stripslashes($pitemd['title']); ?>
                        </section>
                        <section class="pitem_options">

                            <?php

                            // HAVE VARIANT
                            if ($pitem['variant_id']) {

                                // GET VARIANT INFO
                                $var_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['item_variants']. "` WHERE (`id`=" .$pitem['variant_id']. ")";
                                $var_q = sqlquery($var_query);
                                $var = sqlfetch($var_q);

                                // VARIABLE FOUND
                                if ($var['id']) {
                                    echo stripslashes($var['title']);
                                } else {
                                    echo 'VARIANT NOT FOUND';
                                }

                            // NO VARIANT
                            } else {

                                // ATTRIBUTES ARRAY
                                $attra = $_uccms_ecomm->itemAttributes($pitem['item_id']);

                                // HAVE ATTRIBUTES
                                if (count($attra) > 0) {
                                    echo 'Let customer choose';
                                }

                            }

                            ?>

                        </section>
                        <section class="pitem_quantity">

                            <input type="text" name="pitem[<?php echo $pitem['id']; ?>][quantity]" value="<?php echo $pitem['quantity']; ?>" style="width: 60px;" />

                        </section>
                        <section class="pitem_delete">
                            <a class="icon_delete" href="#" data-id="<?php echo $pitem['id']; ?>"></a>
                        </section>
                    </li>

                    <?php

                }

                ?>

            </ul>

            <footer>
                <input class="blue" type="submit" value="Save" />
            </footer>

            <?php

        } else {

            ?>

            <div style="padding: 10px; text-align: center;">
                No package items added yet.
            </div>

            <?php

        }

        ?>

    </div>

    </form>

</div>