<?php

// CLEAN UP
$id = (int)$_POST['item']['id'];

// HAVE ITEM ID
if ($id) {

    // GET ITEM
    $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$id. ")";
    $item_q = sqlquery($item_query);
    $item = sqlfetch($item_q);

    // ITEM FOUND
    if ($item['id']) {

        // DB COLUMNS
        $columns = array(
            'inventory_alert'               => (int)$_POST['inventory_alert'],
            'inventory_alert_at'            => (int)$_POST['inventory_alert_at'],
            'package_track_items_inventory' => (int)$_POST['package_track_items_inventory']
        );

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['items']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$id. ")";
        sqlquery($query);

        // INVENTORY VALUES
        $vals = array(
            'available' => ((int)$_POST['inventory_available'] - $item['inventory_available'])
        );

        // UPDATE INVENTORY
        $inventory_result = $_uccms_ecomm->item_changeInventory($id, $_uccms_ecomm->cart_locationID(), array(), $vals, array(
            'log'   => array(
                'source'    => 'admin',
                //'source_id' => $cart['id']
            )
        ));

        // INVENTORY UPDATE SUCCESSFUL
        if ($inventory_result['success']) {
            $admin->growl('Inventory', 'Updated!');
        }

    }

}

BigTree::redirect(MODULE_ROOT. 'items/edit/?id=' .$id. '#inventory');

?>