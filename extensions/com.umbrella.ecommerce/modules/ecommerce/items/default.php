<?php

// PAGE TITLE
$bigtree['admin_title'] = 'E-Commerce Items';

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Items', 'link' => $bigtree['path'][2]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #items .summary {
        display: block;
    }
    #items .summary .paging {
        margin-left: 5px;
    }

    #items .loading, #items .no-results {
        padding: 10px;
        text-align: center;
        font-size: .9em;
    }

</style>

<script type="text/javascript">

    $(function() {

        getItemsList();

        // FILTER - KEYWORD
        $('#items .summary .filter input[name="q"]').keyup(function(e) {
            delay(function() {
                getItemsList($('#items .summary .filter form').serialize());
            }, 400);
        });

        // TYPE CLICK
        $('#items .summary .filter .type a').click(function(e) {
            $('#items .summary .filter .type a').removeClass('active');
            $(this).addClass('active');
            $('#items .summary .filter input[name="type"]').val($(this).data('id'));
            getItemsList($('#items .summary .filter form').serialize());
        });

        // PAGING CLICK
        $('#items .summary .view_paging').on('click', 'a', function(e) {
            $('#items .summary .filter input[name="page"]').val($(this).attr('href').replace('#', ''));
            getItemsList($('#items .summary .filter form').serialize());
        });

    });

    // GET THE LIST
    function getItemsList(params, options, callback) {
        $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/items/list/', params, function(data) {
            $('#items .items-list').html(data);
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');
    }

</script>

<div id="items" class="table">

    <div class="summary clearfix">

        <div class="filter">

            <form>
            <input type="hidden" name="page" value="" />
            <input type="hidden" name="type" value="" />

            <div class="control search">
                <input type="text" name="q" value="" class="form-control" placeholder="Search" />
            </div>

            <div class="control type links">
                <a href="#" data-id="" class="active">All</a>
                <?php foreach ($_uccms_ecomm->itemTypes() as $type_id => $type) { ?>
                    <a href="#" data-id="<?php echo $type_id; ?>"><?php echo $type['title']; ?></a>
                <?php } ?>
            </div>

            </form>

        </div>

        <div class="paging ajax">
            <nav class="view_paging"></nav>
        </div>

        <a class="btn btn-primary action" href="./edit/"><i class="fas fa-plus"></i>Add item</a>

    </div>

    <div class="items-list table-responsive">
        <div class="loading">Loading..</div>
    </div>

</div>