<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DB COLUMNS
    $columns = array(
        'active'        => 0,
        'deleted'       => 1,
        'updated_by'    => $_uccms_ecomm->adminID()
    );

    // UPDATE ITEM
    $query = "UPDATE `" .$_uccms_ecomm->tables['items']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        $admin->growl('Delete Item', 'Item deleted.');

        // DELETE PRICE GROUP RELATIONS
        $pgd_query = "DELETE FROM `" .$_uccms_ecomm->tables['item_pricegroups']. "` WHERE (`item_id`=" .$id. ")";
        sqlquery($pgd_query);

    // QUERY FAILED
    } else {
        $admin->growl('Delete Item', 'Failed to delete item.');
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Item', 'No item specified.');
}

BigTree::redirect(MODULE_ROOT.'items/');

?>