<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Items', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>


<style type="text/css">

    #items .item_title {
        width: 480px;
    }
    #items .item_sku {
        width: 150px;
    }
    #items .item_price {
        width: 100px;
    }
    #items .item_status {
        width: 100px;
    }
    #items .item_edit {
        width: 55px;
    }
    #items .item_delete {
        width: 55px;
    }

</style>

<form>

<div class="search_paging contain">
    <input id="query" type="search" name="query" value="<?=$_GET['query']?>" placeholder="<?php if (!$_GET['query']) echo 'Search'; ?>" class="form_search" autocomplete="off" />
    <span class="form_search_icon"></span>
    <nav id="view_paging" class="view_paging"></nav>
</div>

<div id="items" class="table">
    <summary>
        <h2>Items</h2>
        <a class="add_resource add" href="./edit/"><span></span>Add Item</a>
    </summary>
    <header style="clear: both;">
        <span class="item_title">Name</span>
        <span class="item_sku">SKU</span>
        <span class="item_price">Price</span>
        <span class="item_status">Status</span>
        <span class="item_edit">Edit</span>
        <span class="item_delete">Delete</span>
    </header>
    <ul id="results" class="items">
        <? include(EXTENSION_ROOT. 'ajax/admin/items/get-page.php'); ?>
    </ul>
</div>

</form>

<script>
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/items/get-page/?page=1&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>");
    };
    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });
    $(".search_paging").on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/items/get-page/?page=" +BigTree.cleanHref($(this).attr("href"))+ "&query=" +escape($("#query").val())+ "&category_id=<?=$_REQUEST['category_id']?>");
        return false;
    });
</script>