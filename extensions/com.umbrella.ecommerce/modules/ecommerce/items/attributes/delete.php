<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`attribute_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Attribute', 'Attribute deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Attribute', 'Failed to delete attribute.');
    }

    // GET ALL ITEMS WITH ATTRIBUTE
    $items_query = "SELECT `item_id` FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`attribute_id`=" .$id. ")";
    $items_q = sqlquery($items_query);
    while ($item = sqlfetch($items_q)) {
        $_uccms_ecomm->item_updateVariants($item['item_id']); // UPDATE VARIANTS FOR ITEM
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Attribute', 'No attribute specified.');
}

BigTree::redirect(MODULE_ROOT.'items/attributes/');

?>