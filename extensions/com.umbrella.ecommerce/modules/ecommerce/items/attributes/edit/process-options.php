<?php

// CLEAN UP
$id = (int)$_POST['attribute']['id'];

// HAVE ATTRIBUTE ID
if ($id) {

    // OPTIONS
    if (count($_POST['option']) > 0) {

        $lid = 0;

        // GET HIGHEST NUMBER
        foreach ($_POST['option'] as $option_id => $option) {
            if (is_numeric($option_id)) {
                if ($option_id > $lid) $lid = $option_id;
            }
        }

        // PUT TOGETHER ARRAY / JSON
        foreach ($_POST['option'] as $option_id => $option) {
            if (stristr($option_id, 'new-')) {
                $lid++;
                $oa[$lid] = $option;
            } else {
                $oa[$option_id] = $option;
            }
        }
        $options = json_encode($oa);
    } else {
        $options = '';
    }

    // DB QUERY
    $query = "UPDATE `" .$_uccms_ecomm->tables['attributes']. "` SET `options`='" .$options. "', `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Attribute Options', 'Saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Attribute Options', 'Failed to save.');
    }

    // GET ALL ITEMS WITH ATTRIBUTE
    $items_query = "SELECT `item_id` FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`attribute_id`=" .$id. ")";
    $items_q = sqlquery($items_query);
    while ($item = sqlfetch($items_q)) {
        $_uccms_ecomm->item_updateVariants($item['item_id']); // UPDATE VARIANTS FOR ITEM
    }

}

BigTree::redirect(MODULE_ROOT.'items/attributes/edit/?id=' .$id. '#options');

?>