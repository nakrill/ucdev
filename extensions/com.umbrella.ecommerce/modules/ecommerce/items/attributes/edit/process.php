<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['attribute']['id'];

    // REQUIRED
    if ($_POST['attribute']['required_checkbox']) {
        $required = $_POST['attribute']['required'];
    } else {
        $required = '';
    }

    // FILE TYPES
    if ($_POST['attribute']['file_types']) {
        $ffta = array();
        $tfta = explode(',', $_POST['attribute']['file_types']);
        foreach ($tfta as $ft) {
            $ffta[] = trim(str_replace('.', '', $ft));
        }
        if (count($ffta) > 0) {
            $file_types = implode(',', $ffta);
        }
    }

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['attribute']['active'],
        'title'         => $_POST['attribute']['title'],
        'description'   => $_POST['attribute']['description'],
        'type'          => $_POST['attribute']['type'],
        'limit'         => (int)$_POST['attribute']['limit'],
        'required'      => $required,
        'file_types'    => $file_types,
        'checkout'      => (int)$_POST['attribute']['checkout'],
        'value_type'    => $_POST['attribute']['value_type'],
        'updated_by'    => $_uccms_ecomm->adminID()
    );

    // HAVE ATTRIBUTE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO ATTRIBUTE ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['attributes']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Attribute', 'Saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Attribute', 'Failed to save.');
    }

    // GET ALL ITEMS WITH ATTRIBUTE
    $items_query = "SELECT `item_id` FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`attribute_id`=" .$id. ")";
    $items_q = sqlquery($items_query);
    while ($item = sqlfetch($items_q)) {
        $_uccms_ecomm->item_updateVariants($item['item_id']); // UPDATE VARIANTS FOR ITEM
    }

}

BigTree::redirect(MODULE_ROOT.'items/attributes/edit/?id=' .$id);

?>