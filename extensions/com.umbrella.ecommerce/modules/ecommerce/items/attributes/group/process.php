<?php

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $id = (int)$_POST['group']['id'];

    // DB COLUMNS
    $columns = array(
        'active'        => (int)$_POST['group']['active'],
        'title'         => $_POST['group']['title'],
        'attributes'    => implode(',', array_keys($_POST['attribute'])),
        'updated_by'    => $_uccms_ecomm->adminID()
    );

    // HAVE ATTRIBUTE ID - IS UPDATING
    if ($id) {

        // DB QUERY
        $query = "UPDATE `" .$_uccms_ecomm->tables['attribute_groups']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW() WHERE (`id`=" .$id. ")";

    // NO ATTRIBUTE ID - IS NEW
    } else {

        // DB QUERY
        $query = "INSERT INTO `" .$_uccms_ecomm->tables['attribute_groups']. "` SET " .$_uccms_ecomm->createSet($columns). ", `updated_dt`=NOW()";

    }

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {

        // NO ID (WAS NEW)
        if (!$id) {
            $id = sqlid();
        }

        $admin->growl('Attribute Group', 'Saved!');

    // QUERY FAILED
    } else {
        $admin->growl('Attribute Group', 'Failed to save.');
    }

    // GET ALL ITEMS WITH ATTRIBUTE GROUP
    $items_query = "SELECT `item_id` FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`group_id`=" .$id. ")";
    $items_q = sqlquery($items_query);
    while ($item = sqlfetch($items_q)) {
        $_uccms_ecomm->item_updateVariants($item['item_id']); // UPDATE VARIANTS FOR ITEM
    }

}

BigTree::redirect(MODULE_ROOT.'items/attributes/group/?id=' .$id);

?>