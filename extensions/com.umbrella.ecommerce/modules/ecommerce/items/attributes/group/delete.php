<?php

// CLEAN UP
$id = (int)$_GET['id'];

// ID SPECIFIED
if ($id) {

    // DELETE RELATIONS
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`group_id`=" .$id. ")";
    sqlquery($query);

    // DB QUERY
    $query = "DELETE FROM `" .$_uccms_ecomm->tables['attribute_groups']. "` WHERE (`id`=" .$id. ")";

    // QUERY SUCCESSFUL
    if (sqlquery($query)) {
        $admin->growl('Delete Attribute Group', 'Attribute group deleted.');

    // QUERY FAILED
    } else {
        $admin->growl('Delete Attribute Group', 'Failed to delete attribute group.');
    }

    // GET ALL ITEMS WITH ATTRIBUTE GROUP
    $items_query = "SELECT `item_id` FROM `" .$_uccms_ecomm->tables['item_attributes']. "` WHERE (`group_id`=" .$id. ")";
    $items_q = sqlquery($items_query);
    while ($item = sqlfetch($items_q)) {
        $_uccms_ecomm->item_updateVariants($item['item_id']); // UPDATE VARIANTS FOR ITEM
    }

// NO ID SPECIFIED
} else {
    $admin->growl('Delete Attribute Group', 'No attribute group specified.');
}

BigTree::redirect(MODULE_ROOT.'items/attributes/');

?>