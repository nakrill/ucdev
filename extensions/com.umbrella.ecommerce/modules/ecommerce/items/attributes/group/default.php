<?php

// GROUP ATTRIBUTE ARRAY
$gaa = array();

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET INFO FROM DB
    $group_query = "SELECT * FROM `" .$_uccms_ecomm->tables['attribute_groups']. "` WHERE (`id`=" .$id. ")";
    $group_q = sqlquery($group_query);
    $group = sqlfetch($group_q);

    // GROUP ATTRIBUTE ARRAY
    $gaa = explode(',', stripslashes($group['attributes']));

}

?>


<style type="text/css">

    #attributes .contain .contain {
        padding-bottom: 10px;
    }

    #attributes .contain .contain:last-child {
        padding-bottom: 0px;
    }

    #attributes .contain .contain .checkbox {
        margin-top: -2px;
    }

</style>

<form enctype="multipart/form-data" action="./process/" method="post">
<input type="hidden" name="group[id]" value="<?php echo $group['id']; ?>" />

<div class="container">

    <header>
        <h2><?php if ($group['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Group</h2>
    </header>

    <section>

        <div class="left">

            <fieldset>
                <label>Title</label>
                <input type="text" name="group[title]" value="<?php echo stripslashes($group['title']); ?>" />
            </fieldset>

            <fieldset>
                <input type="checkbox" name="group[active]" value="1" <?php if ($group['active']) { ?>checked="checked"<?php } ?> />
                <label class="for_checkbox">Active</label>
            </fieldset>

        </div>

        <div class="right">

        </div>

        <div id="attributes" class="contain" style="clear: both;">
            <h3><span>Attributes</span></h3>
            <div class="contain">
                <?php

                // GET ATTRIBUTES
                $attribute_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`item_id`=0) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
                $attribute_q = sqlquery($attribute_query);

                // LOOP
                while ($attribute = sqlfetch($attribute_q)) {
                    ?>
                    <div class="contain">
                        <input type="checkbox" name="attribute[<?php echo $attribute['id']; ?>]" value="1" <?php if (in_array($attribute['id'], $gaa)) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($attribute['title']); ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </section>

    <footer>
        <input class="blue" type="submit" value="Save" />
        <a class="button back" href="../">&laquo; Back</a>
    </footer>

</div>

</form>