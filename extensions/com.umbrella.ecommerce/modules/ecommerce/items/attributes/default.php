<?php

// BREADCRUMBS
$bigtree['breadcrumb'][] = [ 'title' => 'Attributes', 'link' => $bigtree['path'][1]. '/' .$bigtree['path'][2] ];

?>

<style type="text/css">

    #attributes .attribute_title {
        width: 610px;
    }
    #attributes .attribute_type {
        width: 120px;
    }
    #attributes .attribute_status {
        width: 100px;
    }
    #attributes .attribute_edit {
        width: 55px;
    }
    #attributes .attribute_delete {
        width: 55px;
    }

    #groups .group_title {
        width: 610px;
    }
    #groups .group_attributes {
        width: 120px;
    }
    #groups .group_status {
        width: 100px;
    }
    #groups .group_edit {
        width: 55px;
    }
    #groups .group_delete {
        width: 55px;
    }

</style>

<div id="attributes" class="table">
    <summary>
        <h2>Attributes</h2>
        <a class="add_resource add" href="./edit/"><span></span>Add Attribute</a>
    </summary>
    <header style="clear: both;">
        <span class="attribute_title">Title</span>
        <span class="attribute_type">Type</span>
        <span class="attribute_status">Status</span>
        <span class="attribute_edit">Edit</span>
        <span class="attribute_delete">Delete</span>
    </header>
    <ul class="items">

        <?php

        // GET ATTRIBUTES
        $attribute_query = "SELECT * FROM `" .$_uccms_ecomm->tables['attributes']. "` WHERE (`item_id`=0) ORDER BY `sort` ASC, `title` ASC, `id` ASC";
        $attribute_q = sqlquery($attribute_query);

        // LOOP
        while ($attribute = sqlfetch($attribute_q)) {

            if ($attribute['type'] == 'dt') {
                $type = 'Date & Time';
            } else {
                $type = ucwords(str_replace('_', ' ', stripslashes($attribute['type'])));
            }

            ?>

            <li class="item">
                <section class="attribute_title">
                    <a href="./edit/?id=<?php echo $attribute['id']; ?>"><?php echo stripslashes($attribute['title']); ?></a>
                </section>
                <section class="attribute_type">
                    <?php echo $type; ?>
                </section>
                <section class="attribute_status status_<?php if ($attribute['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php if ($attribute['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                </section>
                <section class="attribute_edit">
                    <a class="icon_edit" title="Edit Attribute" href="./edit/?id=<?php echo $attribute['id']; ?>"></a>
                </section>
                <section class="attribute_delete">
                    <a href="./delete/?id=<?php echo $attribute['id']; ?>" class="icon_delete" title="Delete Attribute" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                </section>
            </li>

            <?php

        }

        ?>

    </ul>
</div>

<div id="groups" class="table">
    <summary>
        <h2>Groups</h2>
        <a class="add_resource add" href="./group/"><span></span>Add Group</a>
    </summary>
    <header style="clear: both;">
        <span class="group_title">Title</span>
        <span class="group_attributes">Attributes</span>
        <span class="group_status">Status</span>
        <span class="group_edit">Edit</span>
        <span class="group_delete">Delete</span>
    </header>
    <ul class="items">

        <?php

        // GET GROUPS
        $group_query = "SELECT * FROM `" .$_uccms_ecomm->tables['attribute_groups']. "` ORDER BY `sort`, `title` ASC, `id` ASC";
        $group_q = sqlquery($group_query);

        // LOOP
        while ($group = sqlfetch($group_q)) {

            // GET NUMBER OF ATTRIBUTES
            $num_attr = count(explode(',', stripslashes($group['attributes'])));

            ?>

            <li class="item">
                <section class="group_title">
                    <a href="./group/?id=<?php echo $group['id']; ?>"><?php echo stripslashes($group['title']); ?></a>
                </section>
                <section class="group_attributes">
                    <?php echo number_format($num_attr, 0); ?>
                </section>
                <section class="group_status status_<?php if ($group['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php if ($group['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                </section>
                <section class="group_edit">
                    <a class="icon_edit" title="Edit Group" href="./group/?id=<?php echo $group['id']; ?>"></a>
                </section>
                <section class="group_delete">
                    <a href="./group/delete/?id=<?php echo $group['id']; ?>" class="icon_delete" title="Delete Group" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?\nAttributes in this group will not be deleted.');"></a>
                </section>
            </li>

            <?php

        }

        ?>

    </ul>
</div>