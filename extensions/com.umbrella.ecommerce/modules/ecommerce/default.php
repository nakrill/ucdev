<?php

if ($_GET['date_from']) {
    $date_from = date('Y-m-d 00:00:00', strtotime($_GET['date_from']));
} else {
    $date_from = date('Y-m-d 00:00:00', strtotime('-6 Days'));
}
if ($_GET['date_to']) {
    $date_to = date('Y-m-d 23:59:59', strtotime($_GET['date_to']));
} else {
    $date_to = date('Y-m-d 23:59:59');
}

// ARRAY OF DAYS
$period = new DatePeriod(
     new DateTime($date_from),
     new DateInterval('P1D'),
     new DateTime($date_to)
);
foreach ($period as $date) {
    $daysa[] = $date->format('Y-m-d');
}

// TOP STATS - ARRAY OF ORDERS
$ts_oa = array();

// TOP STATS - SALES TOTAL
$ts_sales = 0;

// TOP STATS - ITEM TOTAL
$ts_items = 0;

// TOP STATS - ARRAY OF CUSTOMERS
$ts_ca = array();

// GET ORDERS FOR TOP STATS
$order_query = "
SELECT *
FROM `" .$_uccms_ecomm->tables['orders']. "`
WHERE ((`dt`>='" .$date_from. "') AND (`dt`<='" .$date_to. "')) AND ((`status`='placed') OR (`status`='charged'))
ORDER BY `dt` DESC
";
$order_q = sqlquery($order_query);
while ($order = sqlfetch($order_q)) {

    // ADD TO ARRAY OF TOP STATS ORDERS
    $ts_oa[] = $order;

    // ADD TO SALES
    //$ts_sales += $order['order_total'];

    // GET NUMBER OF ITEMS
    $item_q = sqlquery("SELECT SUM(`quantity`) AS `sum` FROM `" .$_uccms_ecomm->tables['order_items']. "` WHERE (`cart_id`=" .$order['id']. ")");
    $items = sqlfetch($item_q);
    $ts_items += $items['sum']; // ADD TO ts'S ITEM TOTAL

    // ADD TO ARRAY OF TOP STATS CUSTOMERS
    $ts_ca[$order['customer_id']] = $order['customer_id'];

}

// GET TRANSACTION CHARGES FOR TOP STATS
$trans_query = "
SELECT *
FROM `" .$_uccms_ecomm->tables['transaction_log']. "`
WHERE ((`dt`>='" .$date_from. "') AND (`dt`<='" .$date_to. "')) AND (`status`='charged')
";
$trans_q = sqlquery($trans_query);
while ($trans = sqlfetch($trans_q)) {

    // ADD TO TOP STATS SALES
    $ts_sales += $trans['amount'];

}

?>

<style type="text/css">

    #ecomm_dashboard {
        height: calc(100% - 72px);
    }

    #ecomm_dashboard > table {
        height: 100%;
        border: 0px none;
    }

    #ecomm_dashboard .col_left {
        width: 100%;
        padding: 0 15px 0 0;
        vertical-align: top;
    }

    #ecomm_dashboard .top_stats {
        border: 1px solid #ccc;
        overflow: hidden;
        border-radius: 2px;
    }

    #ecomm_dashboard .top_stats .head {
        padding: 10px 15px;
        background-color: #f5f5f5;
    }
    #ecomm_dashboard .top_stats .head h6 {
        float: left;
        margin: 6px 0 0 0;
        padding: 0px;
    }
    #ecomm_dashboard .top_stats .head .filter {
        float: right;
    }
    #ecomm_dashboard .top_stats .head .filter .dates .from, #ecomm_dashboard .top_stats .head .filter .dates .to {
        float: left;
    }
    #ecomm_dashboard .top_stats .head .filter .dates input.date_picker {
        width: 110px !important;
    }
    #ecomm_dashboard .top_stats .head .filter .dates .icon_small {
        margin: 8px 0 0 -25px;
    }
    #ecomm_dashboard .top_stats .head .filter .dates .div {
        float: left;
        line-height: 30px;
        padding: 0 5px;
    }
    #ecomm_dashboard .top_stats .head .filter .dates .buttons {
        float: left;
        padding-left: 5px;
    }
    #ecomm_dashboard .top_stats .head .filter .dates .buttons input[type="submit"] {
        padding: 8px;
        height: auto;
    }

    #ecomm_dashboard .top_stats .stats_box {
        background-color: #eee;
    }
    #ecomm_dashboard .top_stats .stats_box .stat {
        float: left;
        width: 25%;
        text-align: center;
    }
    #ecomm_dashboard .top_stats .stats_box .stat .padding {
        padding: 25px 10px;
        border-right: 1px solid #fff;
    }
    #ecomm_dashboard .top_stats .stats_box .stat:last-child .padding {
        border-right: 0px none;
    }
    #ecomm_dashboard .top_stats .stats_box .stat .num {
        font-size: 1.5em;
    }
    #ecomm_dashboard .top_stats .stats_box .stat .title {
        padding-top: 10px;
        color: #777;
    }

    #orders {
        margin-top: 15px;
    }
    #orders > summary {
        padding-right: 15px;
    }
    #orders > summary h6 {
        float: left;
        margin-top: 10px;
        line-height: 1em;
    }
    #orders > summary a.add {
        float: right;
        margin-top: 10px;
    }

    #ecomm_dashboard .col_right {
        margin-left: 30px;
        border-left: 1px solid #ccc;
        background-color: #f5f5f5;
    }

    #ecomm_dashboard .col_right .size {
        width: 200px;
        padding: 0 15px;
    }

    #ecomm_dashboard .activity_container {
        border-left: 2px solid #bbb;
    }

    #ecomm_dashboard .activity_container .day {
        position: relative;
        margin-top: 20px;
        padding-bottom: 20px;
    }

    #ecomm_dashboard .activity_container .day:first-child {
        margin-top: 0px;
    }

    #ecomm_dashboard .activity_container .day .icon {
        position: absolute;
        top: 5px;
        left: -7px;
        width: 0.9em;
        height: 0.9em;
        background-color: #bbb;
        border-radius: 0.9em;
    }

    #ecomm_dashboard .activity_container .day .date {
        margin-left: 20px;
        padding: 5px 10px;
        background-color: #e0e0e0;
        border-radius: 10px;
        text-align: center;
        font-weight: bold;
        color: #666;
    }

    #ecomm_dashboard .activity_container .item {
        position: relative;
    }

    #ecomm_dashboard .activity_container .item .icon {
        position: absolute;
        top: -3px;
        left: -10px;
        width: 1.6em;
        height: 1.6em;
        background-color: #aaa;
        text-align: center;
        font-size: 1em;
        line-height: 1.6em;
        color: #fff;
        border-radius: 1.6em;
    }

    #ecomm_dashboard .activity_container .item .main {
        padding-left: 20px;
    }

    #ecomm_dashboard .activity_container .item .main .title {
        padding-bottom: 3px;
        font-weight: bold;
        color: #555;
    }

    #ecomm_dashboard .activity_container .item .main .time {
        padding-bottom: 3px;
        font-size: .9em;
        color: #777;
    }

    #ecomm_dashboard .activity_container .item .main .details {
        padding-top: 1px;
        padding-bottom: 3px;
        font-size: .9em;
        color: #999;
    }

    #ecomm_dashboard .activity_container .item .main .action {
        color: #777;
    }

    #ecomm_dashboard .activity_container .split {
        height: 1px;
        margin: 15px 0;
        border-bottom: 1px dashed #ddd;
    }

    #ecomm_dashboard .graph_orders-gross {
        margin: 0;
    }
    #ecomm_dashboard .graph_orders-gross .graph {
        width: auto;
        height: 180px;
        margin: 0px;
        padding: 0px;
    }

</style>

<script src="/js/lib/highcharts.js"></script>


<div id="ecomm_dashboard">

    <table>
        <tr>

            <td class="col_left">

                <?php

                unset($tfa);
                $tfa = array();

                // LOOP THROUGH DAYS
                foreach ($daysa as $day) {
                    $tfa[] = array(
                        'from'  => date('Y-m-d 00:00:00', strtotime($day)),
                        'to'    => date('Y-m-d 23:59:59', strtotime($day))
                    );
                }

                unset($tvals);
                $tvals = array();

                // LOOP
                foreach ($tfa as $tf) {

                    // COLUMN TITLE
                    $col = strtotime($tf['from']);

                    $tvals[$col]['gross'] = 0;

                    // GET ORDERS
                    $orders_query = "
                    SELECT `id` FROM `" .$_uccms_ecomm->tables['orders']. "`
                    WHERE ((`dt`>='" .$tf['from']. "') AND (`dt`<='" .$tf['to']. "')) AND ((`status`='placed') OR (`status`='charged'))
                    ";
                    $order_q = sqlquery($orders_query);

                    $tvals[$col]['orders'] = sqlrows($order_q); // NUMBER OF ORDERS

                    // GET TRANSACTIONS
                    $trans_query = "
                    SELECT `amount` FROM `" .$_uccms_ecomm->tables['transaction_log']. "`
                    WHERE ((`dt`>='" .$tf['from']. "') AND (`dt`<='" .$tf['to']. "')) AND (`status`='charged')
                    ";
                    $trans_q = sqlquery($trans_query);
                    while ($trans = sqlfetch($trans_q)) {
                        $tvals[$col]['gross'] = $tvals[$col]['gross'] + $trans['amount'];
                    }

                }

                // LOOP THROUGH VALUES
                foreach ($tvals as $col_name => $val) {
                    $g_cola[] = date('j', $col_name);
                    $g_ordersa[] = (int)$val['orders'];
                    $g_grossa[] = $_uccms_ecomm->toFloat($val['gross']);
                }

                unset($tvals);

                // PUT GRAPH COLUMNS TOGETHER
                $g_cols = "'" .implode("','", $g_cola). "'";

                // PUT GRAPH ORDER VALUES TOGETHER
                $g_orders = implode(',', $g_ordersa);

                // PUT GRAPH GROSS VALUES TOGETHER
                $g_gross = implode(',', $g_grossa);

                unset($g_cola);
                unset($g_ordersa);
                unset($g_grossa);

                ?>

                <script type="text/javascript">
                    $(function() {
                        $('#ecomm_dashboard .graph_orders-gross .graph').highcharts({
                            chart: {
                                margin: [15, 75, 30, 60],
                                backgroundColor: '#ffffff'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: [{
                                categories: [<?php echo $g_cols; ?>]
                            }],
                            yAxis: [{ // Left yAxis
                                title: {
                                    text: 'Orders',
                                    style: {
                                        color: '#4572A7'
                                    }
                                },
                                labels: {
                                    format: '{value}',
                                    style: {
                                        color: '#4572A7'
                                    }
                                },
                                min: 0
                            }, { // Right yAxis
                                title: {
                                    text: 'Charged',
                                    style: {
                                        color: '#89A54E'
                                    }
                                },
                                labels: {
                                    format: '${value:,.0f}',
                                    style: {
                                        color: '#89A54E'
                                    }
                                },
                                min: 0,
                                opposite: true
                            }],
                            tooltip: {
                                shared: true,
                            },
                            legend: {
                                enabled: false,
                                layout: 'vertical',
                                align: 'left',
                                x: 120,
                                verticalAlign: 'top',
                                y: 100,
                                floating: true,
                                backgroundColor: '#FFFFFF'
                            },
                            series: [
                                {
                                    name: 'Orders',
                                    color: '#4572A7',
                                    type: 'column',
                                    data: [<?php echo $g_orders; ?>],
                                    tooltip: {
                                    }
                                },{
                                    name: 'Charged',
                                    color: '#89A54E',
                                    type: 'spline',
                                    yAxis: 1,
                                    data: [<?php echo $g_gross; ?>],
                                    tooltip: {
                                        valuePrefix: '$'
                                    }
                                }
                            ],
                            credits: {
                                enabled: false
                            }
                        });
                    });
                </script>

                <div class="top_stats">

                    <div class="head contain">

                        <h6><?php echo count($daysa); ?> Day<?php if (count($daysa) != 1) { echo 's'; } ?></h6>

                        <div class="filter">
                            <form action="" method="get">
                            <div class="dates contain">
                                <div class="from contain">
                                    <?php

                                    // FIELD VALUES
                                    $field = array(
                                        'id'        => 'date_from',
                                        'key'       => 'date_from',
                                        'value'     => date('n/j/Y', strtotime($date_from)),
                                        'required'  => false,
                                        'options'   => array(
                                            'default_now'   => false
                                        )
                                    );

                                    // INCLUDE FIELD
                                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                                    ?>
                                </div>
                                <div class="div">-</div>
                                <div class="to contain">
                                    <?php

                                    // FIELD VALUES
                                    $field = array(
                                        'id'        => 'date_to',
                                        'key'       => 'date_to',
                                        'value'     => date('n/j/Y', strtotime($date_to)),
                                        'required'  => false,
                                        'options'   => array(
                                            'default_now'   => false
                                        )
                                    );

                                    // INCLUDE FIELD
                                    include(BigTree::path('admin/form-field-types/draw/date.php'));

                                    ?>
                                </div>
                                <div class="buttons">
                                    <input type="submit" value="Go" />
                                </div>
                            </div>
                            </form>
                        </div>

                    </div>

                    <div class="graph_orders-gross">
                        <div class="graph"></div>
                    </div>

                    <div class="stats_box contain">
                        <div class="stat orders">
                            <div class="padding">
                                <div class="num"><?php echo number_format(count($ts_oa), 0); ?></div>
                                <div class="title">Orders</div>
                            </div>
                        </div>
                        <div class="stat sales">
                            <div class="padding">
                                <div class="num">$<?php echo number_format($ts_sales, 2); ?></div>
                                <div class="title">Charged</div>
                            </div>
                        </div>
                        <div class="stat items">
                            <div class="padding">
                                <div class="num"><?php echo number_format($ts_items, 0); ?></div>
                                <div class="title">Items</div>
                            </div>
                        </div>
                        <div class="stat customers">
                            <div class="padding">
                                <div class="num"><?php echo number_format(count($ts_ca), 0); ?></div>
                                <div class="title">Customer<?php if (count($ts_ca) != 1) { ?>s<?php } ?></div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="orders" class="table">
                    <summary class="clearfix">
                        <h6>Orders</h6>
                        <a class="add_resource add" href="./orders/?date_from=<?php echo $_REQUEST['date_from']; ?>&date_to=<?php echo $_REQUEST['date_to']; ?>">
                            View All
                        </a>
                    </summary>
                    <header style="clear: both;">
                        <?php foreach ($_uccms_ecomm->adminOQListColumns() as $col_class => $col_title) { ?>
                            <span class="<?=$col_class?>"><?=$col_title?></span>
                        <?php } ?>
                    </header>
                    <ul id="results" class="items">
                        <?php
                        $_GET['limit'] = 500;
                        $_GET['date_from'] = $date_from;
                        $_GET['date_to'] = $date_to;
                        include(EXTENSION_ROOT. 'ajax/admin/orders/get-page.php');
                        ?>
                    </ul>
                </div>

                <style type="text/css">
                    #orders .order_id {
                        width: 35px;
                    }
                    #orders .order_dt {
                        width: 160px;
                        text-align: left;
                    }
                    #orders .order_customer {
                        width: 220px;
                        text-align: left;
                    }
                    #orders .order_items {
                        display: none;
                    }
                    #orders .order_total {
                        width: 80px;
                    }
                    #orders .order_status {
                        width: 100px;
                    }
                    #orders .order_edit {
                        width: 55px;
                    }
                    #orders .order_delete {
                        display: none;
                    }

                    <?php if ($_uccms_ecomm->storeType() == 'catering') { ?>
                        #orders .catering_location {
                            display: none;
                        }
                        #orders .order_status {
                            width: 75px;
                        }
                    <?php } ?>

                </style>

                <div style="padding-bottom: 20px;">
                    <a href="./clear-cart-session/">Clear cart session</a>
                </div>

            </td>

            <td class="col_right" valign="top">
                <div class="size">

                    <h3 style="text-align: center;">Recent Activity</h3>

                    <div class="activity_container">

                        <?php

                        $ra_limit = 30;
                        $raa = array();

                        // GET ORDERS
                        $act_query = "
                        SELECT *
                        FROM `" .$_uccms_ecomm->tables['orders']. "`
                        WHERE ((`status`!='charged') AND (`status`!='failed'))
                        ORDER BY `dt` DESC
                        LIMIT 20
                        ";
                        $act_q = sqlquery($act_query);
                        while ($act = sqlfetch($act_q)) {
                            $act['type'] = 'order';
                            $raa[strtotime($act['dt'])][] = $act;
                        }

                        // GET TRANSACTIONS
                        $act_query = "
                        SELECT *
                        FROM `" .$_uccms_ecomm->tables['transaction_log']. "`
                        ORDER BY `dt` DESC
                        LIMIT 20
                        ";
                        $act_q = sqlquery($act_query);
                        while ($act = sqlfetch($act_q)) {
                            $act['type'] = 'transaction';
                            $raa[strtotime($act['dt'])][] = $act;
                        }

                        // HAVE RECENT ACTIVITY
                        if (count($raa) > 0) {

                            // SORT BY DATE (KEY) DESC
                            krsort($raa);

                            $acta = array();

                            // PUT ALL TOGETHER
                            foreach ($raa as $ra) {
                                foreach ($ra as $a) {
                                    $acta[] = $a;
                                }
                            }

                            $day = '';
                            $i = 0;

                            // LOOP
                            foreach ($acta as $act) {

                                if ($i >= $ra_limit) {
                                    break;
                                } else {
                                    $i++;
                                }

                                $color      = '';
                                $icon       = '';
                                $title      = '';
                                $details    = '';
                                $action     = '';
                                $time       = date('g:i A T', strtotime($act['dt']));

                                // ORDERS
                                if ($act['type'] == 'order') {

                                    if ($order['quote'] == 1) {
                                        $type_title = 'quote';
                                    } else {
                                        $type_title = 'order';
                                    }

                                    // GET NUMBER OF ITEMS
                                    $item_q = sqlquery("SELECT SUM(`quantity`) AS `sum` FROM `" .$_uccms_ecomm->tables['order_items']. "` WHERE (`cart_id`=" .$act['id']. ") LIMIT 10");
                                    $items = sqlfetch($item_q);

                                    // STATUS
                                    switch ($act['status']) {
                                        case 'cart':
                                            $icon       = 'fa-shopping-cart';
                                            $title      = 'Cart (#' .$act['id']. ') updated';
                                            $details    = 'Items: ' .number_format($items['sum'], 0);
                                            $action     = '<a href="./orders/edit/?id=' .$act['id']. '">View cart</a>';
                                            break;
                                        case 'placed':
                                            $color      = '#85eeb8';
                                            $icon       = 'fa-check';
                                            $title      = 'Order (#' .$act['id']. ') placed';
                                            $details    = 'Items: ' .number_format($items['sum'], 0). ' | Total: $' .number_format($act['order_total'], 2);
                                            $action     = '<a href="./orders/edit/?id=' .$act['id']. '">View order</a>';
                                            break;
                                        case 'charged':
                                            $color      = '#00c05d';
                                            $icon       = 'fa-usd';
                                            $title      = 'Order (#' .$act['id']. ') charged';
                                            $details    = 'Items: ' .number_format($items['sum'], 0). ' | Total: $' .number_format($act['order_total'], 2);
                                            $action     = '<a href="./orders/edit/?id=' .$act['id']. '">View order</a>';
                                            break;
                                        case 'cancelled':
                                            $icon       = 'fa-minus';
                                            $title      = 'Order (#' .$act['id']. ') cancelled';
                                            $details    = 'Items: ' .number_format($items['sum'], 0);
                                            $action     = '<a href="./orders/edit/?id=' .$act['id']. '">View order</a>';
                                            break;
                                        case 'deleted':
                                            $icon       = 'fa-times';
                                            $title      = 'Order (#' .$act['id']. ') deleted';
                                            $details    = '';
                                            $action     = '';
                                            break;
                                        case 'requested':
                                            $icon       = 'fa-question';
                                            $title      = 'Quote (#' .$act['id']. ') requested';
                                            $details    = 'Items: ' .number_format($items['sum'], 0);
                                            $action     = '<a href="./quotes/edit/?id=' .$act['id']. '">View quote</a>';
                                            break;
                                        case 'preparing':
                                            $color      = '#abcdec';
                                            $icon       = 'fa-cog';
                                            $title      = 'Preparing quote (#' .$act['id']. ')';
                                            $details    = 'Items: ' .number_format($items['sum'], 0);
                                            $action     = '<a href="./quotes/edit/?id=' .$act['id']. '">View quote</a>';
                                            break;
                                        case 'cloned':
                                            $icon       = 'fa-clone';
                                            $title      = 'Cloned ' .$type_title. ' (#' .$act['id']. ')';
                                            $details    = 'Items: ' .number_format($items['sum'], 0);
                                            $action     = '<a href="./' .$type_title. 's/edit/?id=' .$act['id']. '">View ' .$type_title. '</a>';
                                            break;
                                    }

                                // TRANSACTION
                                } else if ($act['type'] == 'transaction') {

                                    // STATUS
                                    switch ($act['status']) {
                                        case 'charged':
                                            $color      = '#00c05d';
                                            $icon       = 'fa-usd';
                                            $title      = 'Payment charged (#' .$act['cart_id']. ')';
                                            $details    = 'Amount: $' .number_format($act['amount'], 2);
                                            $action     = '<a href="./orders/edit/?id=' .$act['cart_id']. '#transactions">View order</a>';
                                            break;
                                        case 'failed':
                                            $color      = '#ff5722';
                                            $icon       = 'fa-usd';
                                            $title      = 'Payment failed (#' .$act['cart_id']. ')';
                                            $details    = 'Amount: $' .number_format($act['amount'], 2);
                                            $action     = '<a href="./orders/edit/?id=' .$act['cart_id']. '#transactions">View order</a>';
                                            break;
                                    }

                                }

                                // DATE
                                $act_date = date('Y-m-d', strtotime($act['dt']));

                                // DATE DOESN'T MATCH CURRENT LOOP DATE
                                if ($act_date != $day) {

                                    ?>

                                    <div class="day">
                                        <div class="icon"></div>
                                        <div class="date"><?php echo date('M j', strtotime($act['dt'])); ?></div>
                                    </div>

                                    <?php

                                    // SET CURRENT LOOP DATE
                                    $day = $act_date;

                                // SAME DAY
                                } else {
                                    ?>
                                    <div class="split"></div>
                                    <?php
                                }

                                ?>

                                <div class="item contain">
                                    <?php if ($icon) { ?>
                                        <div class="icon" style="<?php if ($color) { echo 'background-color: ' .$color; } ?>"><i class="fa fa-fw <?php echo $icon; ?>"></i></div>
                                    <?php } ?>
                                    <div class="main">
                                        <?php if ($title) { ?>
                                            <div class="title"><?php echo $title; ?></div>
                                        <?php } ?>
                                        <?php if ($time) { ?>
                                            <div class="time"><?php echo $time; ?></div>
                                        <?php } ?>
                                        <?php if ($details) { ?>
                                            <div class="details"><?php echo $details; ?></div>
                                        <?php } ?>
                                        <?php if ($action) { ?>
                                            <div class="action"><i class="fa fa-caret-right"></i> <?php echo $action; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <?php

                            }

                        }

                        ?>

                    </div>

                </div>
            </td>

        </tr>
    </table>

</div>