<?/* https://cdnjs.com/libraries/tooltipster */ ?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tooltipster@4.2.5/dist/css/tooltipster.bundle.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tooltipster@4.2.5/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css" type="text/css" media="screen" />
<script src="https://cdn.jsdelivr.net/npm/tooltipster@4.2.5/dist/js/tooltipster.bundle.min.js"></script>

<style type="text/css">

    #uccmsDashboard.footer-cart-active::after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .7);
        z-index: 100;
    }

    footer.main {
        margin-bottom: 100px;
    }

    #admin_footer-cart {
        position: fixed;
        z-index: 1000;
        bottom: 0;
        left: 0;
        width: 100%;
        height: auto;
        background: #ffffff;
        box-shadow: 0 -2px 24px rgba(0, 0, 0, 0.2);
        -webkit-transition: -webkit-transform .3s;
        transition: -webkit-transform .3s;
        transition: transform .3s;
        transition: transform .3s, -webkit-transform .3s;
    }
    #admin_footer-cart.inactive {
        background-color: rgba(255, 255, 255, .85);
    }
    #admin_footer-cart.inactive:hover {
        background-color: rgba(255, 255, 255, 1);
    }
    #admin_footer-cart.closed {
        bottom: -100px;
        box-shadow: none;
    }

    #admin_footer-cart > .hide-afc {
        display: none;
        z-index: 2000;
        position: absolute;
        top: -50px;
        right: 15px;
        padding: 15px 15px 0;
        font-size: 26px;
        color: rgba(0, 0, 0, .3);
        cursor: pointer;
    }
    #admin_footer-cart > .hide-afc:hover {
        color: rgba(0, 0, 0, .75);
    }
    #admin_footer-cart:hover > .hide-afc {
        display: block;
    }
    #admin_footer-cart.closed:hover > .hide-afc {
        display: none;
    }

    #admin_footer-cart > .show-afc {
        display: none;
        z-index: 2000;
        position: absolute;
        top: -65px;
        right: 30px;
        background-color: rgba(255, 255, 255, .5);
        padding: 15px;
        line-height: 1em;
        font-size: 36px;
        color: rgba(0, 0, 0, .5);
        cursor: pointer;
        box-shadow: 0 -2px 24px rgba(0, 0, 0, 0.2);
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }
    #admin_footer-cart > .show-afc:hover {
        background-color: rgba(255, 255, 255, 1);
        color: rgba(0, 0, 0, .7);
    }
    #admin_footer-cart.opening > .show-afc {
        background-color: transparent;
    }
    #admin_footer-cart.closed > .show-afc {
        display: block;
    }
    #admin_footer-cart > .show-afc .contain {
        position: relative;
        margin: 0px;
        padding: 0px;
    }
    #admin_footer-cart > .show-afc .num {
        position: absolute;
        bottom: -5px;
        right: -2px;
        padding: 3px 5px;
        background-color: rgba(255, 255, 255, .75);
        line-height: 1em;
        font-size: 16px;
        font-weight: bold;
        color: #54BDCC;
        border-radius: 50px;
    }
    #admin_footer-cart > .show-afc:hover .num {
        background-color: rgba(255, 255, 255, .9);
    }

    #admin_footer-cart .start_cart {
        height: 100px;
        text-align: center;
        line-height: 100px;
    }

    #admin_footer-cart .cart_container {
        padding: 0px;
    }
    #admin_footer-cart .cart_container > .loading {
        padding-left: 30px;
        line-height: 100px;
        font-size: 1.3em;
        opacity: .4;
    }

    #admin_footer-cart > .checkout {
        display: none;
        position: relative;
        max-height: 650px;
        overflow-y: scroll;
        margin: 0px;
        padding: 30px 30px 0;
        border-top: 1px solid #eee;
    }

    .tooltipster-sidetip.tooltipster-light .tooltipster-box {
        background-color: #fafafa;
        box-shadow: 0 -2px 24px rgba(0, 0, 0, 0.2);
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

        <?php if ($_uccms_ecomm->cartID()) { ?>
            afc_getCart();
        <?php } ?>
        afc_cartVisibility();

        // START A CART
        $('#admin_footer-cart .start_cart .button').click(function(e) {
            e.preventDefault();
            afc_startCart({}, function() {
                if (window.location.pathname == '/admin/com.umbrella.ecommerce*ecommerce/orders/edit/') {
                    window.location = './?id=' +data.id;
                } else {
                    $('#admin_footer-cart').removeClass('inactive');
                    $('#admin_footer-cart .start_cart').fadeOut(500, function() {
                        $('#admin_footer-cart .cart_container').fadeIn(500);
                    });
                }
            });
        });

        // SHOW CART
        $('#admin_footer-cart .show-afc').click(function(e) {
            e.preventDefault();
            //$(this).fadeOut(600);
            afc_showCart({}, function() {
                $('footer.main').css('margin-bottom', '100px');
            });
        });

        // HIDE CART
        $('#admin_footer-cart .hide-afc').click(function(e) {
            e.preventDefault();
            //$(this).fadeOut(600);
            afc_hideCart({}, function() {
                $('footer.main').css('margin-bottom', '0px');
            });
        });

        afc_resizeCheckout();
        $(window).resize(function() {
            afc_resizeCheckout();
        });

    });

    // CART VISIBILITY
    function afc_cartVisibility(state) {
        if ((!state) && (state != 'undefined')) {
            if (typeof(Storage) !== 'undefined') {
                var saved_state = localStorage.getItem('admin-footer-bar-state');
                if (saved_state) state = saved_state;
            }
        }
        if (!state) state = 'open';
        if (state == 'open') {
            $('#admin_footer-cart').removeClass('closed').addClass('open');
            $('footer.main').css('margin-bottom', '100px');
        } else {
            $('#admin_footer-cart').removeClass('open').addClass('closed');
            $('footer.main').css('margin-bottom', '0px');
        }
    }

    // START A CART
    function afc_startCart(settings, callback) {
        if (typeof settings != 'object') settings = {};
        if (settings.id) {
            $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/footer-cart/switch-to-cart/', {
                id: settings.id,
            }, function(data) {
                if (data.id) {
                    afc_getCart({}, function() {
                        $('#admin_footer-cart').effect('highlight', {}, 2000);
                    });
                    if ($('#admin_footer-cart > .checkout').is(':visible')) {
                        afc_getCheckout();
                    }
                    if (typeof callback == 'function') {
                        callback(data);
                    }
                } else {
                    if (data.error) {
                        alert(data.error);
                    } else {
                        alert('Failed to set as footer cart.');
                    }
                }
            }, 'json');
        } else {
            $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/footer-cart/init-cart/', function(data) {
                if (data.id) {
                    afc_getCart();
                    $('#admin_footer-cart').removeClass('closed').addClass('open');
                    if (typeof callback == 'function') {
                        callback(data);
                    }
                } else {
                    if (data.error) {
                        alert(data.error);
                    } else {
                        alert('Failed to start an order.');
                    }
                }

            }, 'json');
        }
    }

    // GET THE CART
    function afc_getCart(settings, callback, format) {
        if (!format) format = 'html';
        $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/footer-cart/cart-' +format+ '/', function(data) {
            if (format == 'html') {
                if (data) {
                    $('#admin_footer-cart .cart_container').html(data);
                } else {
                    $('#admin_footer-cart .start_cart').show();
                }
            }
            if (typeof callback == 'function') {
                callback();
            }
        }, format);
    }

    // SHOW CART
    function afc_showCart(settings, callback) {
        var $el = $('#admin_footer-cart');
        $el.removeClass('closed closing open').addClass('opening');
        $('#admin_footer-cart').animate({
            bottom: '0px',
        }, 500, function() {
            $el.removeClass('opening').addClass('open');
            if (typeof(Storage) !== 'undefined') {
                localStorage.setItem('admin-footer-bar-state', 'open');
            }
            if (typeof callback == 'function') {
                callback();
            }
        });
    }

    // HIDE CART
    function afc_hideCart(settings, callback) {
        var $el = $('#admin_footer-cart');
        $el.removeClass('open opening closed').addClass('closing');
        if ($('#admin_footer-cart > .checkout').is(':visible')) {
            afc_checkoutToggle();
        }
        $('#admin_footer-cart').animate({
            bottom: '-100px',
        }, 500, function() {
            $el.removeClass('closing').addClass('closed');
            if (typeof(Storage) !== 'undefined') {
                localStorage.setItem('admin-footer-bar-state', 'closed');
            }
            if (typeof callback == 'function') {
                callback();
            }
        });
    }

    // CHECKOUT - TOGGLE
    function afc_checkoutToggle() {
        var $checkout = $('#admin_footer-cart > .checkout');
        if ($checkout.is(':visible')) {
            $checkout.slideToggle(500, function() {
                $('#uccmsDashboard').removeClass('footer-cart-active');
                $('#admin_footer-cart .cart_container .right_section .actions .button').find('i').removeClass('fa-chevron-left').addClass('fa-chevron-right');
            });
        } else {
            afc_getCheckout({}, function() {
                $checkout.slideToggle(500, function() {
                    $('#uccmsDashboard').addClass('footer-cart-active');
                    $('#admin_footer-cart .cart_container .right_section .actions .button').find('i').removeClass('fa-chevron-right').addClass('fa-chevron-left');
                });
            });
        }
    }

    // CHECKOUT - GET DATA
    function afc_getCheckout(settings, callback) {
        $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/footer-cart/checkout-html/', function(data) {
            $('#admin_footer-cart > .checkout').html(data);
            if (typeof callback == 'function') {
                callback();
            }
        }, 'html');
    }

    // RESIZE THE CHECKOUT CONTAINER FOR SCROLLING
    function afc_resizeCheckout() {
        $('#admin_footer-cart > .checkout').css('max-height', $(window).height() - $('#admin_footer-cart .cart_container').height() - 85);
    }

</script>

<div id="admin_footer-cart" class="<?php if (!$_uccms_ecomm->cartID()) { echo 'inactive'; } ?> noprint no-print">

    <a class="hide-afc" title="Hide footer cart"><i class="fa fa-chevron-down"></i></a>

    <a class="show-afc" title="Show footer cart">
        <span class="contain">
            <span class="num">0</span>
            <i class="fa fa-shopping-basket" aria-hidden="true"></i>
        </span>
    </a>

    <div class="start_cart clearfix" style="<?php if ($_uccms_ecomm->cartID()) { ?>display: none;<?php } ?>">
        <a href="#" class="button">Start an Order</a>
    </div>

    <div class="cart_container clearfix" style="<?php if (!$_uccms_ecomm->cartID()) { ?>display: none;<?php } ?>">
        <div class="loading"><img src="/images/misc/loading-circle-1.gif" alt="Loading cart.." /></div>
    </div>

    <div class="checkout clearfix" style="display: none;"></div>

</div>