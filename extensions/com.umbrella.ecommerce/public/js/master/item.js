$(document).ready(function() {

    // SWIPEBOX
    $('#ecommContainer .swipebox, #ecommContainer .swipebox-video').swipebox();

    // GET ESTIMATE
    //this_getEstimate();

    /*
    // MATCH COLUMN HEIGHTS
    var img_height = $('#ecommContainer .images_container').height();
    var info_height = $('#ecommContainer .item_info').height();
    if (info_height < img_height) {
        $('#ecommContainer .item_info').height(img_height);
    } else if (img_height < info_height) {
        $('#ecommContainer .images_container').height(info_height);
    }
    */

    // IMAGE THUMB CLICK
    $('#ecommContainer .images_container .thumbs .thumb.image a').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        if (id) {
            $('#ecommContainer .images_container .main a').removeClass('active');
            $('#ecommContainer .images_container .main #item-image-' +id).addClass('active');
            $(this).closest('.thumb').addClass('active');
        } else {
            var image = $(this).attr('data-image');
            if (image) {
                $('#ecommContainer .images_container .main img').attr('src', image);
                $('#ecommContainer .images_container .thumbs .thumb').removeClass('active');
                $(this).closest('.thumb').addClass('active');
            }
        }
    });

    // ATTRIBUTE CHANGE
    $('#ecommContainer .item_container .attributes input, #ecommContainer .item_container .attributes .option').change(function() {
        //this_getEstimate();
    });
    $('#ecommContainer .item_container .attributes .option').click(function() {
        //this_getEstimate();
    });

    // SELECT BOX CLICK
    $('#ecommContainer .item_container .attributes .attribute .options.select_box .option').click(function(e) {
        e.preventDefault();
        var parent = $(this).closest('.options.select_box');
        parent.find('input[type="hidden"]').val($(this).attr('data-value'));
        parent.find('.option').removeClass('selected');
        $(this).addClass('selected');
    });

    // REMOVE FILE CLICK
    $('#ecommContainer .item_container .attributes .attribute .files .file .remove').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#ecommContainer .item_container .attributes .attribute .files input[name="attribute-' +id+ '-remove"]').val('1');
        $('#ecommContainer .item_container .attributes .attribute .files .file-' +id).remove();
    });

    // ADD TO CART CLICK
    $('#form_item input[type="submit"], #form_item button[type="submit"]').click(function(e) {
        e.preventDefault();
        $('#ecommContainer .item_container .site-messages').remove();
        $('#ecommContainer .item_container input[type="file"]').each(function() {
            var id = $(this).attr('name').replace('attribute-', '');
            if ($('#form_item input[name="attribute[' +id+ '][]"]').length) {
                if ($(this).val()) {
                    $('#form_item input[name="attribute[' +id+ '][]"]').val($(this).val());
                }
            } else {
                $('#form_item').append('<input type="hidden" name="attribute[' +id+ '][]" value="' +$(this).val()+ '" />');
            }
        });
        $.get('/*/com.umbrella.ecommerce/ajax/item/pre_add-to-cart/', $('#form_item').serialize(), function(data) {
            if (data.error) {
                $('#ecommContainer .item_container .add-to-cart').after(data.error);
            } else {
                $('#ecommContainer .item_container input[type="file"]').each(function() {
                    if ($(this).val()) {
                        var id = $(this).attr('name').replace('attribute-', '');
                        $('#form_item input[name="attribute[' +id+ '][]"]').remove();
                    }
                });
                $('#form_item').submit();
            }
        }, 'json');
        return false;
    });

});

// GET ESTIMATE
function this_getEstimate() {
    $.get('/*/com.umbrella.ecommerce/ajax/item/estimate/', $('#form_item').serialize(), function(data) {
        if ((data['price']) && (data['price'] != 'na')) {
            var price = '$' +data['price'];
        } else {
            var price = '';
        }
        if (data['error']) {
            alert(data['error']);
        }
        $('#ecommContainer .item_info .price .value').text(price);
    }, 'json');
}