$(document).ready(function() {

    // GET SHIPPING SECTION
    uccms_shippingSection();

    // SHIPPING SAME TOGGLE
    $('#ecommContainer input[name="order[shipping_same]"]').click(function() {
        if ($(this).prop('checked')) {
            $('#toggle_shipping_info').hide();
        } else {
            $('#toggle_shipping_info').show();
        }
    });

    // ADDRESS CHANGE - TAX
    $('#ecommContainer select[name="billing[state]"], #ecommContainer input[name="billing[zip]"], #ecommContainer select[name="shipping[state]"], #ecommContainer input[name="shipping[zip]"]').change(function() {
        uccms_orderTax();
    });

    // SHIPPING SAME - TAX
    $('#ecommContainer input[name="order[shipping_same]"]').click(function(e) {
        uccms_orderTax();
    });

    // ADDRESS CHANGE - SHIPPING
    $('#ecommContainer select[name="billing[state]"], #ecommContainer select[name="shipping[state]"], #ecommContainer input[name="billing[zip]"], #ecommContainer input[name="shipping[zip]"]').change(function() {
        uccms_shippingSection();
    });

    // PAYMENT METHOD SELECT
    $('#ecommContainer .payment .method input[name="payment[method]"]').click(function() {
        $('#ecommContainer .payment .method .info').hide();
        $(this).closest('.method').find('.info').show();
    });

    // CREDIT CARD EXPIRATION SLASH
    $('#ecommContainer #card_new input[name$="[expiration]"]').keyup(function(e) {
        if (e.keyCode != 8) { // backspace
            if (e.keyCode == 111) { // forward slash
                $(this).val($(this).val().substr(0, $(this).val().length - 1));
            } else if ($(this).val().length == 2) {
                $(this).val($(this).val()+ '/');
            }
        }
    });

});

// GET TAX INFO
function uccms_orderTax() {
    $.get('/*/com.umbrella.ecommerce/ajax/checkout/tax/', {
        'billing_state': $('#ecommContainer select[name="billing[state]"]').val(),
        'billing_zip': $('#ecommContainer input[name="billing[zip]"]').val(),
        'shipping_state': $('#ecommContainer select[name="shipping[state]"]').val(),
        'shipping_zip': $('#ecommContainer input[name="shipping[zip]"]').val(),
        'shipping_same': $('#ecommContainer input[name="order[shipping_same]"]').prop('checked')
    }, function(data) {
        $('#ecommContainer .cart .tax .num').text(data['tax']['total']);
        uccms_updateCartTotal();
    }, 'json');
}

// GET SHIPPING SECTION
function uccms_shippingSection() {
    $('#ecommContainer .shipping_method > .contain').html('Loading..');
    if ($('#ecommContainer input[name="order[shipping_same]"]').prop('checked')) {
        var state = $('#ecommContainer select[name="billing[state]"]').val();
        var zip = $('#ecommContainer input[name="billing[zip]"]').val();
    } else {
        var state = $('#ecommContainer select[name="shipping[state]"]').val();
        var zip = $('#ecommContainer input[name="shipping[zip]"]').val();
    }
    $.get('/*/com.umbrella.ecommerce/ajax/checkout/shipping_section/', {
        'selected': $('#ecommContainer #hv_shipping_service').text(),
        'state': state,
        'zip': zip
    }, function(data) {
        $('#ecommContainer .shipping_method > .contain').html(data);
    }, 'html');
}

// UPDATE CART TOTAL
function uccms_updateCartTotal() {
    var subtotal = 0;
    $('#ecommContainer #cart .item .total .num').each(function() {
        subtotal += parseFloat($(this).text().replace(',', ''));
    });
    var discount = 0;
    if ($('#ecommContainer #cart .sub.right.pre').length) {
        discount += parseFloat($('#ecommContainer #cart .sub.right.pre .num').text().replace(',', ''));
    }
    var tax = parseFloat($('#ecommContainer #cart .tax .num').text().replace(',', ''));
    var shipping = parseFloat($('#ecommContainer #cart .shipping .num').text().replace(',', ''));
    var total = (subtotal - discount + tax + shipping).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    $('#ecommContainer #cart .sub.total .num').text(total);
}