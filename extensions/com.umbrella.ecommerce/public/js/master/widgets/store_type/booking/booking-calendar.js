$(document).ready(function() {

    // DURATION SELECT
    $('#ecommContainer .item_container .booking .duration .options').on('click', '.option', function(e) {
        if (!$(this).hasClass('unavailable')) {
            $('#ecommContainer .item_container .attributes').show();
            $('#ecommContainer .item_container .order_info').show();
            $('#ecommContainer .item_container .buttons').show();
        }
    });

});