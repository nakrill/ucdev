$(document).ready(function() {

    // ITEM - UPDATE QUANTITY
    $('#ecommContainer #cart .quantity .update').click(function(e) {
        e.preventDefault();
        var qty = $(this).closest('.quantity').find('input').val();
        window.location.href = $(this).attr('href')+ '&quantity=' +qty;
    });

    // ITEM - REMOVE
        // UPDATE QUANTITY
    $('#ecommContainer #cart .quantity .remove').click(function(e) {
        return confirm('Are you sure you want to remove this?');
    });

});