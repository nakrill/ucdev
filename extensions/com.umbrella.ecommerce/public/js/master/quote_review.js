$(document).ready(function() {

    // RIGHT COLUMN - CLOSE
    $('#review_right_col .close a').click(function(e) {
        e.preventDefault();
        $('#review_right_col').animate({right: '-' +$('#review_right_col').width()+ 'px'}, 600, function() {
            $('#review_right_col-expand').show();
        });
    });

    // RIGHT COLUMN - EXPAND
    $('#review_right_col-expand a').click(function(e) {
        e.preventDefault();
        $('#review_right_col-expand').hide();
        $('#review_right_col').animate({right: '0px'}, 600);
    });

    // RIGHT COLUMN - BALANCE PAY BUTTON CLICK
    $('#ecommContainer #review_right_col .top_balance .button.pay').click(function(e) {
        e.preventDefault();
        $('#ecommContainer #review_right_col .payment_form .amount input').val($(this).attr('data-amount'));
        $('#ecommContainer #review_right_col .section.payment .expand_content').removeClass('closed');
        $('#ecommContainer #review_right_col .section.payment h3 i.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
    });

    // RIGHT COLUMN - SCHEDULED PAYMENT PAY BUTTON CLICK
    $('#ecommContainer #review_right_col .scheduled_payments .pay .button').click(function(e) {
        e.preventDefault();
        $('#ecommContainer #review_right_col .payment_form .amount input').val($(this).attr('data-amount'));
        $('#ecommContainer #review_right_col .section.payment .expand_content').removeClass('closed');
        $('#ecommContainer #review_right_col .section.payment h3 i.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
    });

    // RIGHT COLUMN - EXPAND SECTION
    $('#review_right_col .section.expand h3').click(function(e) {
        var closed = false;
        $(this).closest('.section').find('.expand_content').each(function(i, v) {
            if ($(this).hasClass('closed')) {
                $(this).removeClass('closed');
            } else {
                $(this).addClass('closed');
                closed = true;
            }
        });
        if (closed) {
            $(this).find('i.fa').removeClass('fa-caret-up').addClass('fa-caret-down');
        } else {
            $(this).find('i.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
        }
    });

    // PAYMENT METHOD SELECT
    $('#review_right_col .payment_form .method input[name="payment[method]"]').click(function() {
        $('#review_right_col .payment_form .method .info').hide();
        $(this).closest('.method').find('.info').show();
        $('#review_right_col .payment_form .submit').show();
    });

});