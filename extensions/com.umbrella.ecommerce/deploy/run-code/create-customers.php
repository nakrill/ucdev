<?php

// ONLY ALLOW VIA COMMAND LINE
if (isset($_SERVER['REQUEST_METHOD'])) exit;

// DISABLE TIME LIMIT
set_time_limit(0);

// RUN SCRIPT IN BACKGROUND
@ignore_user_abort(TRUE);

##########################################################


// INCLUDE BIGTREE
require_once(__DIR__. '/../../../../uccms/utils/include_bigtree.php');

// HAS E-COMMERCE
if (class_exists('uccms_Ecommerce')) {

    // QUERIES TO RUN
    $queries = [
        "ALTER TABLE `uccms_ecommerce_customers` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
        "ALTER TABLE `uccms_ecommerce_customers` ADD `account_id` INT(11) UNSIGNED NOT NULL AFTER `id`",
        "ALTER TABLE `uccms_ecommerce_customers` ADD `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `account_id`",
        "ALTER TABLE `uccms_ecommerce_customers` ADD INDEX(`account_id`)",
        "ALTER TABLE `uccms_ecommerce_customers` ADD INDEX(`status`)",
        "UPDATE `uccms_ecommerce_customers` SET `account_id`=`id`",
        "CREATE TABLE `uccms_ecommerce_customers_contacts` (
          `id` int(11) UNSIGNED NOT NULL,
          `customer_id` int(11) UNSIGNED NOT NULL,
          `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
          `default_billing` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
          `default_shipping` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
          `default_delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
          `firstname` varchar(200) NOT NULL,
          `lastname` varchar(200) NOT NULL,
          `business_name` varchar(200) NOT NULL,
          `phone_work` varchar(20) NOT NULL,
          `phone_mobile` varchar(20) NOT NULL,
          `phone_home` varchar(20) NOT NULL,
          `email_work` varchar(200) NOT NULL,
          `email_home` varchar(200) NOT NULL,
          `address1` text NOT NULL,
          `address2` text NOT NULL,
          `city` varchar(200) NOT NULL,
          `state` varchar(50) NOT NULL,
          `zip` varchar(10) NOT NULL,
          `notes` text NOT NULL,
          `route_id` int(11) UNSIGNED NOT NULL DEFAULT 0
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8",
        "ALTER TABLE `uccms_ecommerce_customers_contacts`
          ADD PRIMARY KEY (`id`),
          ADD KEY `customer_id` (`customer_id`),
          ADD KEY `firstname` (`firstname`),
          ADD KEY `lastname` (`lastname`),
          ADD KEY `route_id` (`route_id`)",
        "ALTER TABLE `uccms_ecommerce_customers_contacts`
          MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT",
    ];

    // LOOP THROUGH QUERIES
    foreach ((array)$queries as $query) {

        // FAILED
        if (!sqlquery($query)) {
            echo "\n";
            echo 'Query failed: ' .$query;
            echo "\n";
        }

    }

    echo "\n";

    if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce();

    // GET ORDERS WITH INFO
    $orders_sql = "
    SELECT *
    FROM `" .$_uccms_ecomm->tables['orders']. "`
    WHERE (`customer_id`>0) OR (`billing_firstname`!='') OR (`billing_lastname`!='') OR (`shipping_firstname`!='') OR (`shipping_lastname`!='')
    ";
    $orders_q = sqlquery($orders_sql);
    while ($order = sqlfetch($orders_q)) {

        echo 'Order ID: ' .$order['id'];
        echo "\n";

        // BILLING
        $contacts = [
            'billing' => [
                'firstname' => $order['billing_firstname'],
                'lastname' => $order['billing_lastname'],
                'firstname' => $order['billing_firstname'],
                'phone_home' => $order['billing_phone'],
                'email_home' => $order['billing_email'],
                'address1' => $order['billing_address1'],
                'address2' => $order['billing_address2'],
                'city' => $order['billing_city'],
                'state' => $order['billing_state'],
                'zip' => $order['billing_zip'],
                'default' => [
                    'billing' => 1,
                    'shipping' => $order['shipping_same'],
                ],
            ],
        ];

        // DIFFERENT SHIPPING
        if (!$order['shipping_same']) {
            if (($order['shipping_firstname']) || ($order['shipping_lastname'])) {

                $contacts['shipping'] = [
                    'firstname' => $order['shipping_firstname'],
                    'lastname' => $order['shipping_lastname'],
                    'firstname' => $order['shipping_firstname'],
                    'phone_home' => $order['shipping_phone'],
                    'email_home' => $order['shipping_email'],
                    'address1' => $order['shipping_address1'],
                    'address2' => $order['shipping_address2'],
                    'city' => $order['shipping_city'],
                    'state' => $order['shipping_state'],
                    'zip' => $order['shipping_zip'],
                    'default' => [
                        'shipping' => 1,
                    ],
                ];

            }
        }

        print_r($contacts);
        echo "\n";

        // GET CUSTOMER
        $customer = $_uccms_ecomm->getCustomer($order['customer_id']);

        // CUSTOMER NOT FOUND
        if (!$customer['id']) {

            // CREATE CUSTOMER
            $customer = $_uccms_ecomm->saveCustomer([
                'account' => [
                    'email' => ($contacts['billing']['email_home'] ? $contacts['billing']['email_home'] : $contacts['shipping']['email_home']),
                    'firstname' => ($contacts['billing']['firstname'] ? $contacts['billing']['firstname'] : $contacts['shipping']['firstname']),
                    'lastname' => ($contacts['billing']['lastname'] ? $contacts['billing']['lastname'] : $contacts['shipping']['lastname']),
                ],
                'status' => 1,
            ], [
                'noWelcomeEmail' => true,
            ]);

            print_r($customer);
            echo "\n";

            // STILL NO ID
            if (!$customer['id']) {
                $customer['id'] = $customer['existing_id'];
            }

        }

        // HAVE CUSTOMER ID
        if ($customer['id']) {

            // UPDATE ORDER
            $uorder_sql = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `customer_id`=" .$customer['id']. " WHERE (`id`=" .$order['id']. ")";
            sqlquery($uorder_sql);

            echo $uorder_sql;
            echo "\n";

            // ADD CONTACTS
            foreach ($contacts as $contact) {

                $contact['customer_id'] = $customer['id'];
                $contact['active'] = 1;

                // SAVE CONTACT
                $cont = $_uccms_ecomm->saveCustomerContact($contact);

                print_r($cont);
                echo "\n";

            }

        }

        echo "\n";

    }

// NO E-COMMERCE
} else {
    echo 'No ecommerce extension.';
    echo "\n";
}

echo "\n";
echo 'All done.';
echo "\n";

?>