<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $payout_id  = (int)$_GET['payout_id'];

    // GET PAYOUT INFO
    $payout = $_uccms_ecomm->payoutInfo($payout_id);

    // ACCOUNT ID
    $account_id = ($payout['account_id'] ? $payout['account_id'] : (int)$_GET['account_id']);

    // GET ACCOUNT ITEMS
    $aitems = $_uccms_ecomm->accountItems($account_id);

    $orders = [];
    $sorders = [];

    // SENT
    if ($payout['status'] > 5) {

        // PAYOUT ORDERS
        foreach ((array)$payout['orders'] as $porder) {

            // GET ORDER INFO
            $order_query = "
            SELECT o.*
            FROM `" .$_uccms_ecomm->tables['orders']. "` AS `o`
            INNER JOIN `" .$_uccms_ecomm->tables['order_items']. "` AS `oi` ON oi.cart_id=o.id
            WHERE (o.id=" .$porder['order_id']. ")
            GROUP BY o.id
            ";
            $order_q = sqlquery($order_query);
            $orders[] = sqlfetch($order_q);

        }

    // UNSENT
    } else {

        switch ($_REQUEST['build_from']) {

            case 'last-payout':
                $pgao_params = [
                    'item_id'   => $aitems,
                    'unpaid'    => true,
                    'date'      => [
                        'from' => '' // date of last paid order?
                    ]
                ];
                break;

            // date range

            case 'unpaid':
                $pgao_params = [
                    'item_id'   => $aitems,
                    'unpaid'    => true,
                ];
                break;

            default:
                $pgao_params = [
                    'item_id'   => $aitems,
                ];
                break;
        }

        // GET MATCHING ORDERS
        $orders = $_uccms_ecomm->payoutGetAccountOrders($account_id, $pgao_params);

        // SELECTED ORDERS
        $sorders = (array)$_GET['orders'];

    }

    // NUMBER OF SELECTED ORDERS
    $num_sorders = count($sorders);

    // HAVE ORDERS
    if (count($orders) > 0) {

        ?>

        <style type="text/css">

            #payout #orders .color-paid {
                color: #4caf50;
            }
            #payout #orders .color-unpaid {
                color: #ef5350;
            }

            #payout #orders .order {
                margin-bottom: 10px;
                padding: 15px;
                border: 1px solid #ccc;
                background-color: #fff;
            }
            #payout #orders .order .main-info > div {
                float: left;
                line-height: 24px;
                font-size: 1.1em;
            }
            #payout #orders .order .main-info > .select-order {
                padding-right: 15px;
                line-height: 1em;
            }
            #payout #orders .order .main-info > .select-order input[type="checkbox"] {
                -webkit-appearance: checkbox;
            }
            #payout #orders .order .main-info > .select-order i {
                font-size: 1.5em;
                line-height: 24px;
            }
            #payout #orders .order .main-info > .order-id {
                width: 12%;
            }
            #payout #orders .order .main-info > .customer {
                width: 30%;
            }
            #payout #orders .order .main-info > .date {
                width: 25%;
            }
            #payout #orders .order .main-info > .items {
                width: 10%;
            }
            #payout #orders .order .main-info > .total {
                float: right;
                text-align: right;
                font-weight: bold;
            }
            #payout #orders .order .review {
                display: none;
                padding-top: 15px;
            }
            #payout #orders .order .review .heading {
                display: none;
            }
            #payout #orders .order .review .details .left {
                width: 50%;
                margin: 0px;
            }
            #payout #orders .order .review .details h4 {
                margin-bottom: 5px;
            }
            #payout #orders .order .review .details .edit_anchor {
                display: none;
            }
            #payout #orders .order .review .cart_addItem {
                display: none;
            }
            #payout #orders .order .review .cart {
                margin-top: 15px;
            }
            #payout #orders .order .review .cart .item td {
                vertical-align: top;
            }
            #payout #orders .order .review .cart .item .thumb img {
                max-width: 120px;
                height: auto;
            }
            #payout #orders .order .review .cart .item .title {
                width: 55%;
            }
            #payout #orders .order .review .cart .item .title ul li {
                font-size: 11px;
                line-height: 14px;
                opacity: .7;
            }
            #payout #orders .order .review .cart .item .price {
                width: 15%;
            }
            #payout #orders .order .review .cart .item .quantity {
                width: 15%;
            }
            #payout #orders .order .review .cart .item .total {
                width: 15%;
                text-align: right;
                font-weight: bold;
            }
            #payout #orders .order .review .cart .item .total .payout {
                display: inline-block;
                margin-top: 5px;
                padding: 5px;
                border: 1px solid #333;
            }
            #payout #orders .order .review .cart th.total {
                text-align: right;
            }
            #payout #orders .order .review .cart .item .total .actions {
                display: none;
            }
            #payout #orders .order .review .cart tr.right {
                float: none;
                margin: auto;
                width: auto;
            }
            #payout #orders .order .review .cart .sub.total {
                font-weight: bold;
            }
            #payout #orders .order .review .cart .sub .value {
                text-align: right;
            }
            #payout #orders .order .review .cart input[type="text"] {
                width: 23px;
                display: inline-block;
                border: 0px none;
                margin: 0px;
                padding: 0px;
                background: transparent;
                height: auto;
                font-size: 12px;
                padding-left: .5px;
            }
            #payout #orders .order .review .cart input[type="text"]::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                color: #333;
            }
            #payout #orders .order .review .cart input[type="text"]:-ms-input-placeholder { /* Internet Explorer 10-11 */
                color: red;
            }
            #payout #orders .order .review .cart input[type="text"]::-ms-input-placeholder { /* Microsoft Edge */
                color: red;
            }

        </style>

        <script type="text/javascript">

            $(function() {

                calculatePayoutTotal();

                $('#payout #orders .order .select-order input').click(function(e) {
                    calculatePayoutTotal();
                });

                // ORDER - # Items click
                $('#payout #orders .order .items a').click(function(e) {
                    e.preventDefault();
                    var $order = $(this).closest('.order');
                    $order.find('.review').slideToggle();
                });

            });

            <?php if ($payout['status'] < 5) { ?>
                function calculatePayoutTotal() {
                    var payoutTotal = 0.00;
                    $('#payout #orders .order').each(function(i, el) {
                        if ($(el).find('.select-order input').prop('checked')) {
                            payoutTotal += parseFloat($(this).data('payout-total'));
                        }
                    });
                    $('#payout input[name="payout[amount]"]').val(payoutTotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                }
            <?php } ?>

        </script>

        <?php

        // LOOP
        foreach ($orders as $order) {

            // GET ITEMS
            $citems = $_uccms_ecomm->cartItems($order['id'], false);

            $num_items = count($citems);

            // HAVE ITEMS
            if ($num_items > 0) {

                // PAYOUT CALCULATIONS
                $pcalc = $_uccms_ecomm->payoutOrderItemsCalculate($citems, $aitems);

                //echo print_r($pcalc);

                $paid = false;

                if ($payout['status'] == 5) {
                    $paid = true;

                } else {

                    // SEE IF ORDER HAS BEEN PAID OUT
                    $opstatus_query = "
                    SELECT `status`
                    FROM `" .$_uccms_ecomm->tables['payout_orders']. "` AS `po`
                    INNER JOIN `" .$_uccms_ecomm->tables['payouts']. "` AS `p` ON po.payout_id=p.id
                    WHERE (po.order_id=" .$order['id']. ")
                    ";
                    $opstatus_q = sqlquery($opstatus_query);
                    $opstatus = sqlfetch($opstatus_q);
                    if ($opstatus['status'] == 5) {
                        $paid = true;
                    }

                }

                ?>

                <script type="text/javascript">

                    $(function() {

                        <?php foreach ((array)$pcalc['oitems'] as $oitem_id => $oitem) { ?>
                            $('#payout #orders .order[data-order-id="<?php echo $order['id']; ?>"] .review .cart #item-<?php echo $oitem_id; ?> .total').append('<span class="payout color-unsent">$<?php echo number_format((float)$oitem['total'], 2); ?></span>');
                        <?php } ?>

                    });

                </script>

                <div class="order <?php echo ($paid ? 'paid' : 'unpaid'); ?> clearfix" data-order-id="<?php echo $order['id']; ?>" data-order-total="<?php echo $order['order_total']; ?>" data-payout-total="<?php echo $pcalc['total']; ?>">
                    <div class="main-info clearfix">
                        <div class="select-order">
                            <?php if ($paid) { ?>
                                <i class="fa fa-check-circle-o color-paid" aria-hidden="true" title="Sent"></i>
                            <?php } else { ?>
                                <input type="checkbox" name="orders[]" value="<?php echo $order['id']; ?>" <?php if (($num_sorders == 0) || (in_array($order['id'], $sorders))) { ?>checked="checked"<?php } ?> class="custom_control" />
                            <?php } ?>
                        </div>
                        <div class="order-id">
                            Order: <a href="../../orders/edit/?id=<?php echo $order['id']; ?>" target="_blank"><?php echo $order['id']; ?></a>
                        </div>
                        <div class="customer">
                            <div class="billing">
                                <?php echo trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])); ?>
                            </div>
                        </div>
                        <div class="date">
                            <?php echo date('n/j/Y G:i A', strtotime($order['dt'])); ?>
                        </div>
                        <div class="items">
                            <a href="#"><?php echo number_format($num_items, 0). ' item' .($num_items != 1 ? 's' : ''); ?></a>
                        </div>
                        <div class="total color-<?php echo ($paid ? 'paid' : 'unpaid'); ?>">
                            $<?php echo number_format($pcalc['total'], 2); ?>
                        </div>
                    </div>
                    <div class="review">
                        <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/review.php'); ?>
                    </div>
                </div>
                <?php

            }

            //break;

        }

    // NO ORDERS
    } else {

        ?>
        <div class="no-results">
            <?php
            if ($orders['error']) {
                echo $orders['error'];
            } else {
                echo 'No matching orders found.';
            }
            ?>
        </div>
        <?php

    }

}

?>