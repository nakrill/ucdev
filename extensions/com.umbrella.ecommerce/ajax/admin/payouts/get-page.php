<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // NUMBER OF RESULTS PER PAGE
    $per_page = 20;

    $query  = isset($_GET['query']) ? $_GET['query'] : '';
    $page   = isset($_GET['page']) ? intval($_GET['page']) : 1;

    // PREVENT SQL SHENANIGANS
    $sort_by = 'p.dt';
    if (isset($_GET['sort'])) {
        $valid_columns = array(
            'date'      => 'p.dt',
            'account'   => 'p.account_id',
            'amount'    => 'p.amount',
            'status'    => 'p.status'
        );
        if ($valid_columns[$_GET['sort']]) {
            $sort_by = $valid_columns[$_GET['sort']];
        }
    }
    if (strtolower($_GET['sort_direction']) == 'asc') {
        $sort_dir = 'ASC';
    } else {
        $sort_dir = 'DESC';
    }

    /*
    if ($sort_by == 'p.date') {
        $sort_dir = (isset($_GET['sort_direction']) && $_GET['sort_direction'] == 'ASC') ? 'ASC' : 'DESC';
    } else {
        $sort_dir = (isset($_GET['sort_direction']) && $_GET['sort_direction'] == 'DESC') ? 'DESC' : 'ASC';
    }
    */

    // WHERE ARRAY
    if ($_GET['deleted']) {
        $wa[] = "p.status=9";
    } else {
        $wa[] = "p.status!=9";
    }

    /*
    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(a.username) LIKE '%$part%') OR (LOWER(a.email) LIKE '%$part%') OR (LOWER(ad.firstname) LIKE '%$part%') OR (LOWER(ad.lastname) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }
    */

    // GET PAGED ITEMS
    $payouts_query = "
    SELECT p.*, a.email, ad.firstname, ad.lastname
    FROM `" .$_uccms_ecomm->tables['payouts']. "` AS `p`
    LEFT JOIN `uccms_accounts` AS `a` ON p.account_id=a.id
    LEFT JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY " .$sort_by. " " .$sort_dir. "
    LIMIT " .(($page - 1) * $per_page). "," .$per_page;

    //echo $payouts_query. '<br />';
    //exit;

    $payouts_q = sqlquery($payouts_query);

    // NUMBER OF PAGED
    $num_paged = sqlrows($payouts_q);

    // TOTAL NUMBER
    $total_results = sqlfetch(sqlquery("
    SELECT p.*
    FROM `" .$_uccms_ecomm->tables['payouts']. "` AS `p`
    WHERE (" .implode(") AND (", $wa). ")
    "));

    // NUMBER OF PAGES
    $pages = ceil($total_results['total'] / $per_page);

    // HAVE RESULTS
    if ($num_paged > 0) {

        // LOOP
        while ($payout = sqlfetch($payouts_q)) {

            ?>

            <li id="row_<?=$payout['id']?>">
                <section class="view_column payouts_date"><?php echo date('n/j/Y g:i A', strtotime($payout['dt'])); ?></section>
                <section class="view_column payouts_account"><?php echo trim(stripslashes($payout['firstname']. ' ' .$payout['lastname'])); ?></section>
                <section class="view_column payouts_amount">$<?php echo number_format($payout['amount'], 2); ?></section>
                <section class="view_column payouts_status"><?php echo $_uccms_ecomm->payoutStatuses()[$payout['status']]; ?></section>
                <section class="payouts_edit">
                    <a class="icon_edit" title="Edit Payout" href="./edit/?id=<?php echo $payout['id']; ?>"></a>
                </section>
                <? /*
                <section class="payouts_delete">
                    <?php if ($_uccms_ecomm->adminModulePermission() == 'p') { ?>
                        <a href="./delete/?id=<?php echo $payout['id']; ?>" class="icon_delete" title="Delete Payout" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                    <?php } ?>
                </section>
                */ ?>
            </li>

            <?
        }

    // NO ACCOUNTs
    } else {
        ?>
        <li style="text-align: center;">
            No payouts.
        </li>
        <?php
    }

    ?>

    <script>
        BigTree.setPageCount("#view_paging",<?=$pages?>,<?=$page?>);
    </script>

    <?php

}

?>