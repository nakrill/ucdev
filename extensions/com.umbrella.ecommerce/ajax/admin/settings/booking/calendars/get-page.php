<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

    //$_uccms_ecomm->setPerPage(5);

    // WHERE ARRAY
    $wa[] = "bc.status!=9";

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(bc.title) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED CALENDARS
    $calendar_query = "
    SELECT bc.*
    FROM `" .$_uccms_ecomm->tables['booking_calendars']. "` AS `bc`
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY bc.title ASC, bc.id ASC
    LIMIT " .(($page - 1) * $_uccms_ecomm->perPage()). "," .$_uccms_ecomm->perPage();
    $calendar_q = sqlquery($calendar_query);

    // NUMBER OF PAGED CALENDARS
    $num_calendars = sqlrows($calendar_q);

    // TOTAL NUMBER OF CALENDARS
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ecomm->tables['booking_calendars']. "` AS `bc` WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_ecomm->pageCount($total_results['total']);

    // HAVE CALENDARS
    if ($num_calendars > 0) {

        // LOOP
        while ($calendar = sqlfetch($calendar_q)) {

            // NUMBER OF ITEMS
            $num_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`calendar_id`=" .$calendar['id']. ")";
            $num_q = sqlquery($num_query);
            $num_items = sqlrows($num_q);

            ?>

            <li class="item">
                <section class="calendar_title">
                    <a href="./edit/?id=<?php echo $calendar['id']; ?>"><?php echo stripslashes($calendar['title']); ?></a>
                </section>
                <section class="calendar_items">
                    <?php echo number_format($num_items, 0); ?>
                </section>
                <section class="calendar_edit">
                    <a class="icon_edit" title="Edit calendar" href="./edit/?id=<?php echo $calendar['id']; ?>"></a>
                </section>
                <section class="calendar_delete">
                    <a href="./delete/?id=<?php echo $calendar['id']; ?>" class="icon_delete" title="Delete Calendar" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
                </section>
            </li>

            <?php

        }

    // NO CALENDARS
    } else {
        ?>
        <li style="text-align: center;">
            No calendars.
        </li>
        <?php
    }

    ?>

    <script>
	    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>