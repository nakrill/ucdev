<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    //$_uccms_ecomm->setPerPage(1);

    $page = (is_numeric($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1);

    // CLEAN UP
    $category_id = (int)$_REQUEST['category_id'];
    $title = sqlescape(strtolower($_REQUEST['title']));
    $id = (int)$_REQUEST['id'];
    $sku = sqlescape(strtolower($_REQUEST['sku']));

    $wa = [];

    // NOT DELETED
    $wa[] = "i.deleted=0";

    // CATEGORY SPECIFIED
    if ($category_id > 0) {
        $inner_join = "INNER JOIN `" .$_uccms_ecomm->tables['item_categories']. "` AS `icr` ON i.id=icr.item_id";
        $wa[] = "icr.category_id=" .$category_id;
    } else {
        $inner_join = "";
    }

    // BY TITLE
    if ($_REQUEST['title']) {
        $wa[] = "LOWER(i.title) LIKE '%" .sqlescape(strtolower($_REQUEST['title'])). "%'";
    }

    // BY ID
    if ($_REQUEST['id']) {
        $wa[] = "i.id=" .(int)$_REQUEST['id'];
    }

    // BY SKU
    if ($_REQUEST['sku']) {
        $wa[] = "LOWER(i.sku) LIKE '%" .sqlescape(strtolower($_REQUEST['sku'])). "%'";
    }

    // GET PAGED RESULTS
    $results_query = "
    SELECT i.*
    FROM `" .$_uccms_ecomm->tables['items']. "` AS `i`
    " .$inner_join. "
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY i.title ASC, i.id ASC
    LIMIT " .(($page - 1) * $_uccms_ecomm->perPage()). "," .$_uccms_ecomm->perPage();

    $results_q = sqlquery($results_query);

    // NUMBER OF PAGED RESULTS
    $num_results = sqlrows($results_q);

    // TOTAL NUMBER OF RESULTS
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ecomm->tables['items']. "` AS `i` " .$inner_join. " WHERE (" .implode(") AND (", $wa). ")"));

    // HAVE RESULTS
    if ($num_results > 0) {

        // PAGING CLASS
        require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

        // INIT PAGING CLASS
        $_paging = new paging();

        // OPTIONAL - URL VARIABLES TO IGNORE
        $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

        // PREPARE PAGING
        $_paging->prepare($_uccms_ecomm->perPage(), 10, $_REQUEST['page'], $_REQUEST);

        // GENERATE PAGING
        $pages = $_paging->output($num_results, $total_results['total']);

        ?>

        <style type="text/css">

            #inventory .data ul {
                margin: 0px;
                padding: 0px;
                border-bottom: 1px solid rgba(0,0,0,.125);
            }
            #inventory .data li {
                max-height: 80px;
                font-size: 14px;
                transition: .3s;
            }
            #inventory .data li:hover {
                background-color: #43b4ae1a;
            }

            #inventory .data li .image {
                text-align: center;
                cursor: pointer;
            }
            #inventory .data li .image img {
                height: 50px;
                width: auto;
            }
            #inventory .data li .title, #inventory .data li .statuses {
                line-height: 50px;
                cursor: pointer;
            }
            #inventory .data li .statuses {
                font-size: 15px;
            }
            #inventory .data li .statuses .status {
                margin-right: 5px;
            }
            #inventory .data li .statuses .status .badge {
                margin-right: 2px;
                vertical-align: middle;
                font-size: .9em;
                font-weight: 600;
            }
            #inventory .data li .statuses .status .title {
                font-size: .9em;
                text-transform: uppercase;
            }
            #inventory .data li .options .btn {
                margin: 7.5px 0 0 10px;
                font-size: 14px;
            }

            #modal_edit-inventory .modal-header {
                padding: .5rem 1rem;
            }
            .modal-header .close {
                padding-top: 1.2rem;
            }
            #modal_edit-inventory .modal-title img {
                display: inline-block;
                object-fit: cover;
                object-position: center;
                height: 40px;
                width: 40px;
                margin-right: 10px;
            }
            #modal_edit-inventory .modal-title span {
                vertical-align: middle;
            }
            #modal_edit-inventory .modal-title .item-title {
                margin-right: 5px;
            }
            #modal_edit-inventory .modal-title .title {
                opacity: .7;
            }

        </style>

        <script type="text/javascript">

            var inventoryModalChanged = false;

            $(function() {

                // ON MODAL OPEN
                $('#modal_edit-inventory').on('shown.bs.modal', function(e) {
                    $('#modal_edit-inventory .modal-data').hide().html('');
                    $('#modal_edit-inventory .modal-loading').show();
                    $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/item-edit/', {
                        id: $(e.relatedTarget).closest('li').data('id')
                    }, function(data) {
                        $('#modal_edit-inventory .modal-loading').hide();
                        $('#modal_edit-inventory .modal-data').html(data).show();
                    }, 'html');
                });

                // ON MODAL CLOSE
                $('#modal_edit-inventory').on('hidden.bs.modal', function(e) {
                    this_itemList($('#form_filter').serialize());
                    inventoryModalChanged = false;
                    $('#modal_edit-inventory .modal-data').hide().html('');
                    $('#modal_edit-inventory .modal-title').html('Edit Inventory');
                });

            });

        </script>

        <div class="modal fade" id="modal_edit-inventory" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Edit Inventory
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-loading">
                            Loading..
                        </div>
                        <div class="modal-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="save btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="paging clearfix">
            <h6><?php echo number_format((int)$total_results['total'], 0); ?> Item<?php echo ($total_results['total'] != 1 ? 's' : ''); ?></h6>
            <?php echo $pages; ?>
        </div>

        <ul class="list-group list-group-flush">

            <?php

            // LOOP
            while ($result = sqlfetch($results_q)) {

                // GET IMAGE
                $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$result['id']. ") ORDER BY `sort` ASC LIMIT 1";
                $image = sqlfetch(sqlquery($image_query));

                // HAVE IMAGE
                if ($image['image']) {
                    $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
                } else {
                    $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
                }

                // GET INVENTORY
                $item_inventory = $_uccms_ecomm->item_getInventory($result['id']);

                ?>

                <li class="list-group-item" data-id="<?php echo $result['id']; ?>">
                    <div class="contain row">
                        <div class="image col-md-1" data-toggle="modal" data-target="#modal_edit-inventory">
                            <img src="<?php echo $image_url; ?>" alt="" />
                        </div>
                        <div class="title col-md-4" data-toggle="modal" data-target="#modal_edit-inventory">
                            <?php echo stripslashes($result['title']); ?>
                        </div>
                        <div class="statuses col-md-4" data-toggle="modal" data-target="#modal_edit-inventory">
                            <?php
                            if ($item_inventory['available'] > 0) {
                                ?>
                                <span class="status available">
                                    <span class="badge badge-<?php echo ($item_inventory['alerts'] ? 'warning' : 'success'); ?>"><?php echo number_format($item_inventory['available'], 0); ?></span>
                                    <span class="title text-<?php echo ($item_inventory['alerts'] ? 'warning' : 'success'); ?>">In stock</span>
                                </span>
                                <?php
                            } else {
                                ?>
                                <span class="status out-of-stock">
                                    <span class="badge badge-danger">0</span>
                                    <span class="title text-danger">Out of stock</span>
                                </span>
                                <?php
                            }
                            if ($item_inventory['unavailable'] > 0) {
                                ?>
                                <span class="status unavailable">
                                    <span class="badge badge-secondary"><?php echo number_format($item_inventory['unavailable'], 0); ?></span>
                                    <span class="title text-secondary">Unavailable</span>
                                </span>
                                <?php
                            }
                            if ($item_inventory['backordered'] > 0) {
                                ?>
                                <span class="status backordered">
                                    <span class="badge badge-info"><?php echo number_format($item_inventory['backordered'], 0); ?></span>
                                    <span class="title text-info">Backordered</span>
                                </span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="options col-md-3 align-right">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_edit-inventory"><i class="fas fa-edit"></i> Edit Inventory</a>
                            <a href="../items/edit/?id=<?php echo $result['id']; ?>" target="_blank" class="btn btn-secondary">Edit Item</a>
                        </div>
                    </div>
                </li>

                <?
            }

            ?>

        </ul>

        <?php

    // NO RESULTS
    } else {
        ?>
        <div class="alert alert-warning" role="alert">
            No items found.
        </div>
        <?php
    }

    ?>

    <?php

}

?>