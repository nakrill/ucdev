<?php

$out = [];

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $id = (int)$_POST['item']['id'];

    // HAVE ITEM ID
    if ($id) {

        // GET ITEM
        $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$id. ")";
        $item_q = sqlquery($item_query);
        $item = sqlfetch($item_q);

        // ITEM FOUND
        if ($item['id']) {

            // HAVE VARIANTS
            if (is_array($_POST['variant'])) {

                // LOOP
                foreach ($_POST['variant'] as $var_id => $var) {

                    // GET VARIANT
                    $variant_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_variants']. "` WHERE (`id`=" .$var_id. ")";
                    $variant_q = sqlquery($variant_query);
                    $variant = sqlfetch($variant_q);

                    // VARIANT FOUND
                    if ($variant['id']) {

                        // INVENTORY VALUES
                        $vals = array(
                            'available'     => ((int)$var['inventory_available'] - $variant['inventory_available']),
                            'unavailable'   => ((int)$var['inventory_unavailable'] - $variant['inventory_unavailable']),
                            'backordered'   => ((int)$var['inventory_backordered'] - $variant['inventory_backordered']),
                        );

                        // UPDATE INVENTORY
                        $inventory_result = $_uccms_ecomm->item_changeInventory($id, $_uccms_ecomm->cart_locationID(), array(), $vals, array(
                            'variant_id' => $variant['id'],
                            'log'   => array(
                                'source'    => 'admin',
                                //'source_id' => $cart['id']
                            )
                        ));

                        if (!$var['title']) $var['title'] = $var['original_title'];

                        // DB COLUMNS
                        $columns = array(
                            'sku'                   => $var['sku'],
                            'title'                 => $var['title'],
                            'inventory_alert'       => (int)$var['inventory_alert'],
                            'inventory_alert_at'    => (int)$var['inventory_alert_at']
                        );

                        // DB QUERY
                        $query = "UPDATE `" .$_uccms_ecomm->tables['item_variants']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .(int)$var_id. ")";
                        sqlquery($query);

                    }

                }

                $out['success'] = true;

            } else {
                $out['error'] = 'No variants specified.';
            }

        } else {
            $out['error'] = 'Item not found.';
        }

    } else {
        $out['error'] = 'Item not specified.';
    }

}

echo json_encode($out);

?>