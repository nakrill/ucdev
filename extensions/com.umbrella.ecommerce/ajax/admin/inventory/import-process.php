<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // HAVE FILE
    if ($_FILES['file']['name']) {

        $process_id = date('Y-m-d_H-i-s');

        // TEMPORARY
        $tmp_file = EXTENSION_ROOT. 'modules/ecommerce/import/inventory/temp-processing.csv';

        // IS PROCESSING OR NOT
        $processing = file_exists($tmp_file);

        // ALREADY PROCESSING
        if ($processing) {
            $out['error'] = 'Already processing an import.';

        // NOT PROCESSING YET
        } else {

            // GET MIME TYPE
            $mime_type = mime_content_type($_FILES['file']['tmp_name']);

            // TEXT
            if ($mime_type == 'text/plain') {

                // SAVED
                if (move_uploaded_file($_FILES['file']['tmp_name'], $tmp_file)) {

                    $importConfig = [
                        'file'      => $tmp_file,
                        'silent'    => true,
                        'logFile'   => EXTENSION_ROOT. 'modules/ecommerce/import/inventory/logs/' .$process_id. '-result.txt'
                    ];

                    // INCLUDE PROCESSING
                    require(EXTENSION_ROOT. 'modules/ecommerce/import/inventory/manual.php');

                    // MOVE INPUT FILE TO LOGS
                    rename($tmp_file, EXTENSION_ROOT. 'modules/ecommerce/import/inventory/logs/' .$process_id. '-input.txt');
                    unset($tmp_file);

                    $out['success'] = true;
                    $out['process_id'] = $process_id;

                // SAVE FAILED
                } else {
                    $out['error'] = 'Failed to save file for processing.';
                }


            // INVALID MIME TYPE
            } else {
                $out['error'] = 'File type "' .$mime_type. '" not supported.';
            }

        }

    } else {
        $out['error'] = 'No file specified.';
    }

    echo json_encode($out);

}

?>