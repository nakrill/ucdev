<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // HAVE ID
    if ($id) {

        // GET ITEM
        $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$id. ")";
        $item_q = sqlquery($item_query);
        $item = sqlfetch($item_q);

    }

    // HAVE ITEM
    if ($item['id']) {

        // NOT A PACKAGE
        if ($item['type'] != 3) {

            // LOCATIONS ARRAY
            $la = array();

            // GET LOCATIONS
            $loc_query = "SELECT * FROM `" .$_uccms_ecomm->tables['locations']. "`";
            $loc_q = sqlquery($loc_query);
            while ($loc = sqlfetch($loc_q)) {
                $la[$loc['id']] = $loc;
            }

            if (count($la) == 0) $la[0] = array('id' => 0);

            // GET ITEM VARIANTS
            $va = $_uccms_ecomm->item_getVariants($item['id']);

        }

        // HAS ATTRIBUTES
        if (count($_uccms_ecomm->itemAttributes($item['id'])) > 0) {

                // GET IMAGE
                $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC LIMIT 1";
                $image = sqlfetch(sqlquery($image_query));

                // HAVE IMAGE
                if ($image['image']) {
                    $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
                } else {
                    $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
                }

            ?>

            <style type="text/css">

                #variants .table-responsive {
                }
                #variants .table {
                    border: 0px;
                }
                #variants .table th {
                    white-space: nowrap;
                }
                #variants input {
                    font-size: 14px;
                    padding-top: .2rem;
                    padding-bottom: .2rem;
                }
                #variants input[type="checkbox"] {
                    -webkit-appearance: checkbox;
                }
                #variants tbody .variant_alert {
                    text-align: center;
                    vertical-align: middle;
                }

            </style>

            <script type="text/javascript">

                // UPDATE MODAL TITLE
                $('#modal_edit-inventory .modal-title').html('<img src="<?php echo $image_url; ?>" alt=""><span class="item-title"><?php echo addslashes(stripslashes($item['title'])); ?></span><span class="title">: Inventory</span>');

                $(function() {

                    // INPUT CHANGE
                    $('#variants input').change(function() {
                        inventoryModalChanged = true;
                    });

                    // VARIANTS - LOCATION EXPAND
                    $('#variants .location .heading').click(function(e) {
                        e.preventDefault();
                        var id = $(this).attr('data-id');
                        $('#variants .loc-' +id+ ' .toggle-content').toggle(0, function() {
                            if ($(this).is(':visible')) {
                                $('#variants .loc-' +id+ ' .heading .arrow').removeClass('fa-caret-down').addClass('fa-caret-up');
                            } else {
                                $('#variants .loc-' +id+ ' .heading .arrow').removeClass('fa-caret-up').addClass('fa-caret-down');
                            }
                        });
                    });

                    // ALERT CHECKBOX CLICK
                    $('#variants .variant_alert input[type="checkbox"]').click(function(e) {
                        var $input = $(this).closest('.item').find('.variant_alert-at input[type="text"]');
                        if ($(this).is(':checked')) {
                            $input.prop('disabled', '');
                        } else {
                            $input.prop('disabled', 'disabled');
                        }
                    });

                    // GENERATE BUTTON CLICK
                    $('#modal_edit-inventory .btn.generate').click(function(e) {
                        $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/item-generate-variants/', {
                            id: <?php echo $item['id']; ?>
                        }, function(data) {
                            if (data.success) {
                                $('#modal_edit-inventory .modal-data').hide().html('');
                                $('#modal_edit-inventory .modal-loading').show();
                                $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/item-edit/', {
                                    id: <?php echo $item['id']; ?>
                                }, function(data) {
                                    $('#modal_edit-inventory .modal-loading').hide();
                                    $('#modal_edit-inventory .modal-data').html(data).show();
                                }, 'html');
                            } else {
                                if (!data.error) data.error = 'Failed to generate variants.';
                                alert(data.error);
                            }
                        }, 'json');
                    });

                    // SAVE BUTTON CLICK
                    $('#modal_edit-inventory .btn.save').click(function(e) {
                        e.preventDefault();
                        e.stopImmediatePropagation();
                        this_saveItemVariants();
                    });

                    // ON MODAL CLOSE
                    $('#modal_edit-inventory').on('hide.bs.modal', function(e) {
                        if (inventoryModalChanged) {
                            if (confirm('Do you want to save your changes before closing?')) {
                                this_saveItemVariants({}, {}, function(data) {
                                    if (!data.success) {
                                        e.preventDefault();
                                        e.stopImmediatePropagation();
                                        if (!data.error) data.error = 'Failed to save.';
                                        alert(data.error);
                                        return false;
                                    }
                                });
                            } else {
                                 //e.preventDefault();
                                 //e.stopImmediatePropagation();
                                 //return false;
                            }
                        }
                    });

                });

                // SAVE ITEM VARIANTS
                function this_saveItemVariants(params, options, callback) {
                    $.post('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/item-save-variants/', $('#form_item-variants').serialize(), function(data) {
                        if (data.success) {
                            inventoryModalChanged = false;
                            alert('Saved!');
                        } else {
                            if (!data.error) data.error = 'Failed to save.';
                            alert(data.error);
                        }
                        if (typeof callback == 'function') {
                            callback(data);
                        }
                    }, 'json');
                }

            </script>

            <form id="form_item-variants" enctype="multipart/form-data" action="#" method="post">
            <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

            <div id="variants">

                <?php foreach ($la as $location) { ?>

                    <div class="location loc-<?php echo $location['id']; ?>">

                        <?php if ($location['id'] != 0) { ?>
                            <a href="#" data-id="<?php echo $location['id']; ?>" class="heading">
                                <?php echo stripslashes($location['title']); ?>
                                <i class="arrow fa fa-caret-down"></i>
                            </a>
                        <?php } ?>

                        <div class="toggle-content" <?php if ($location['id'] != 0) { ?>style="display: none;"<?php } ?>>

                            <div class="table-responsive">
                                <table class="table">

                                    <thead class="thead-light">
                                        <tr>
                                            <th class="variant_title">Variant</th>
                                            <th class="variant_sku">SKU</th>
                                            <th class="variant_available text-success">In Stock</th>
                                            <th class="variant_alert">Alert</th>
                                            <th class="variant_alert-at">Alert At</th>
                                            <th class="variant_unavailable text-secondary">Unavailable</th>
                                            <th class="variant_backordered text-info">Backordered</th>
                                        </tr>
                                    </thead>

                                    <?php if (count((array)$va[$location['id']]) > 0) { ?>

                                        <tbody class="items">
                                            <?php
                                            foreach ($va[$location['id']] as $var) {
                                                $vdata = json_decode(stripslashes($var['data']), true);
                                                ?>
                                                <tr class="item contain clearfix" data-id="<?php echo $var['id']; ?>">
                                                    <td class="variant_title">
                                                        <input type="hidden" name="variant[<?php echo $var['id']; ?>][original_title]" value="<?php echo $vdata['original_title']; ?>" />
                                                        <input type="text" name="variant[<?php echo $var['id']; ?>][title]" value="<?php echo stripslashes($var['title']); ?>" placeholder="<?php echo $vdata['original_title']; ?>" class="form-control" />
                                                    </td>
                                                    <td class="variant_sku">
                                                        <input type="text" name="variant[<?php echo $var['id']; ?>][sku]" value="<?php echo stripslashes($var['sku']); ?>" placeholder="<?php echo stripslashes($item['sku']); ?>" class="form-control" />
                                                    </td>
                                                    <td class="variant_available">
                                                        <input type="text" name="variant[<?php echo $var['id']; ?>][inventory_available]" value="<?php echo (int)$var['inventory_available']; ?>" class="form-control" />
                                                    </td>
                                                    <td class="variant_alert">
                                                        <input type="checkbox" name="variant[<?php echo $var['id']; ?>][inventory_alert]" value="1" <?php if ($var['inventory_alert']) { ?>checked="checked"<?php } ?> class="custom_control" />
                                                    </td>
                                                    <td class="variant_alert-at">
                                                        <input type="text" name="variant[<?php echo $var['id']; ?>][inventory_alert_at]" value="<?php echo (int)$var['inventory_alert_at']; ?>" class="form-control" <?php if (!$var['inventory_alert']) { ?>disabled="disabled"<?php } ?> />
                                                    </td>
                                                    <td class="variant_unavailable">
                                                        <input type="text" name="variant[<?php echo $var['id']; ?>][inventory_unavailable]" value="<?php echo (int)$var['inventory_unavailable']; ?>" class="form-control" />
                                                    </td>
                                                    <td class="variant_backordered">
                                                        <input type="text" name="variant[<?php echo $var['id']; ?>][inventory_backordered]" value="<?php echo (int)$var['inventory_backordered']; ?>" class="form-control" />
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="7">
                                                <div style="padding-top: 15px; text-align: center;">
                                                    <div style="margin-bottom: 10px; font-size: 14px;">No variants created yet.</div>
                                                    <a href="#" class="generate btn btn-primary">Generate</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </table>
                            </div>

                        </div>

                    </div>

                <?php } ?>

            </div>

            </form>

        <?php } else { ?>

            <script type="text/javascript">

                $(function() {

                    // INVENTORY - TRACK TOGGLE
                    $('#package_track_items_inventory').change(function(e) {
                        if ($(this).prop('checked')) {
                            $('#toggle_package_track_items_inventory').hide();
                        } else {
                            $('#toggle_package_track_items_inventory').show();
                        }
                    });

                    // SAVE BUTTON CLICK
                    $('#modal_edit-inventory .btn.save').click(function(e) {
                        this_saveItemInventory();
                    });

                });

                // SAVE ITEM INVENTORY
                function this_saveItemInventory(params, options, callback) {
                    $.post('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/item-save-inventory/', $('#form_item-inventory').serialize(), function(data) {
                        if (data.success) {
                            alert('Saved!');
                        } else {
                            if (!data.error) data.error = 'Failed to save.';
                            alert(data.error);
                        }
                        if (typeof callback == 'function') {
                            callback(data);
                        }
                    }, 'json');
                }

            </script>

            <form id="form_item-inventory" enctype="multipart/form-data" action="#" method="post">
            <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />

            <div class="row">

                <div class="col-md-6">

                    <?php

                    // PACKAGE
                    if ($item['type'] == 3) {

                        ?>

                        <fieldset class="form-group">
                            <input id="package_track_items_inventory" type="checkbox" name="package_track_items_inventory" value="1" <?php if ($item['package_track_items_inventory']) { ?>checked="checked"<?php } ?> />
                            <label class="for_checkbox">Track package items inventory</label>
                        </fieldset>

                        <?php

                    }

                    ?>

                    <div id="toggle_package_track_items_inventory" class="contain" style="<?php if (($item['type'] == 3) && ($item['package_track_items_inventory'])) { ?>display: none;<?php } ?> padding-top: 10px;">

                        <fieldset class="form-group">
                            <label>In Stock</label>
                            <input type="text" name="inventory_available" value="<?php echo (int)$item['inventory_available']; ?>" class="form-control" />
                        </fieldset>

                    </div>

                </div>

                <div class="col-md-6">

                    <fieldset class="form-group">
                        <input type="checkbox" name="inventory_alert" value="1" <?php if ($item['inventory_alert']) { ?>checked="checked"<?php } ?> />
                        <label class="for_checkbox">Alert</label>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Alert At</label>
                        <input type="text" name="inventory_alert_at" value="<?php echo (int)$item['inventory_alert_at']; ?>" class="form-control" />
                    </fieldset>

                </div>

            </div>

            </form>

            <?php

        }

    } else {
        echo 'Item not found.';
    }

} else {
    echo 'You do not have necessary permissions.';
}

?>