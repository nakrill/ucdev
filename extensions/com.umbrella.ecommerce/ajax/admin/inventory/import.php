<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // IS PROCESSING OR NOT
    $processing = file_exists(EXTENSION_ROOT. 'modules/ecommerce/import/inventory/temp-processing.csv');

    ?>

    <style type="text/css">

        #inventory-import .alert {
            display: none;
        }
        #inventory-import .log {
            display: none;
            margin-top: 15px;
        }
        #inventory-import .log .card p {
            font-size: .9em;
        }

    </style>

    <script type="text/javascript">

        processing = <?php echo ($processing ? 'true' : 'false') ?>;

        $(function() {

            // UPLOAD FILE CHANGE
            $('#inventoryImportFile:file').change(function(){
                if ($(this).val()) {
                    $('#modal_import-inventory .btn.save').attr('disabled', false);
                    $('#modal_import-inventory .btn.save').removeAttr('disabled');
                }
            });

            // SAVE BUTTON CLICK
            $('#modal_import-inventory .btn.save').click(function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                this_processImport();
            });

        });

        // PROCESS THE IMPORT
        function this_processImport() {
            if (!processing) {
                $('#modal_import-inventory .btn.save').attr('disabled', 'disabled');
                processing = true;
                $('#inventory-import .alert').hide().html('').removeClass('alert-success alert-danger');
                $('#form_import-inventory').hide();
                $('#inventory-import .log').hide().html('');
                $('#inventory-import .processing').show();
                $.ajax({
                    url: '<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/import-process/',
                    type: 'POST',
                    data: new FormData($('#form_import-inventory')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            $('#inventory-import .alert').html('Inventory successfully imported!').addClass('alert-success').show();
                        } else if (data.error) {
                            $('#inventory-import .alert').html(data.error).addClass('alert-danger').show();
                        }
                        $('#inventory-import .processing').hide();
                        $('#form_import-inventory input[type="file"]').val('');
                        $('#form_import-inventory').trigger('reset').show();
                        if (data.process_id) {
                            $.get('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/inventory/import-log/', {
                                process_id: data.process_id
                            }, function(data) {
                                $('#inventory-import .log').html(data).show();
                            }, 'html');
                        }
                    }
                });
            }
        }


    </script>

    <div id="inventory-import">

        <div class="alert fade show" role="alert"></div>

        <form id="form_import-inventory" action="" method="post" enctype="multipart/form-data" <?php if ($processing) { ?>style="display: none;"<?php } ?>>

            <input type="hidden" name="hidden" value="true" />

            <div class="form-group">
                <label for="inventoryImportFile">Choose an import file from your computer, or download a template below to start from.</label>
                <input type="file" name="file" class="form-control-file" id="inventoryImportFile" accept=".csv">
            </div>

        </form>

        <div class="processing" <?php if (!$processing) { ?>style="display: none;"<?php } ?>>
            <p><i class="fas fa-spinner fa-spin"></i> Your import is processing..</p>
        </div>

        <div class="log"></div>

    </div>

    <?php

}

?>