<?php

$out = [];

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // HAVE ITEM ID
    if ($id) {

        // UPDATE VARIANTS FOR ITEM
        if ($_uccms_ecomm->item_updateVariants($id)) {
            $out['success'] = true;
        } else {
            $out['error'] = 'Failed to generate variants.';
        }

    } else {
        $out['error'] = 'Item not specified.';
    }

}

echo json_encode($out);

?>