<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $process_id = trim(preg_replace("/[^0-9_-]/", '', $_REQUEST['process_id']));

    // HAVE PROCESS ID
    if ($process_id) {

        $log_file = EXTENSION_ROOT. 'modules/ecommerce/import/inventory/logs/' .$process_id. '-result.txt';

        // EXISTS
        if (file_exists($log_file)) {

            // GET DATA
            $data = file_get_contents($log_file);

            // HAVE DATA
            if ($data) {

                ?>

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Result</h5>
                        <p class="card-text"><?php echo nl2br($data); ?></p>
                    </div>
                </div>

                <?php

            }

        }

    }

}

?>