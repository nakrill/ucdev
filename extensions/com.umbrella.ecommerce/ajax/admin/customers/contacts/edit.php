<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    $contact = [
        'customer_id' => (int)$_REQUEST['customer_id'],
    ];

    // CLEAN
    $id = (int)$_REQUEST['id'];

    // ID SPECIFIED
    if (($contact['customer_id']) && ($id)) {

        // GET CONTACT
        $contact = $_uccms_ecomm->customerContact($contact['customer_id'], $id);

    }

    $lockedContactDefaults = [];
    foreach ((array)$_REQUEST['data']['default'] as $def) {
        $contact['default'][$def] = true;
        $lockedContactDefaults[$def] = true;
    }

    ?>

    <style type="text/css">

        #modal_editContact form .alert {
            display: none;
        }

        #modal_editContact .form-check-input {
            margin-top: 0px;
        }

    </style>

    <script type="text/javascript">

        $(function() {

            // ALERT CLOSE
            $('#modal_editContact form .alert .close').click(function(e) {
                $(this).closest('.alert').fadeOut(300);
            });

        });

        function modaleditContact_saveCallback(data) {
            if (data.success) {
                window.scrollTo(0, 0);
                window.location.reload();
            }
        }

    </script>

    <form>

    <input type="hidden" name="customer[id]" value="<?php echo (int)$contact['customer_id']; ?>" />
    <input type="hidden" name="contact[id]" value="<?php echo $contact['id']; ?>" />

    <div class="alert alert-dismissible fade show" role="alert">
        <strong></strong>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="row">

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>First Name</label>
                <input type="text" name="contact[firstname]" value="<?php echo stripslashes($contact['firstname']); ?>" class="form-control" />
            </fieldset>

        </div>

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>Last Name</label>
                <input type="text" name="contact[lastname]" value="<?php echo stripslashes($contact['lastname']); ?>" class="form-control" />
            </fieldset>

        </div>

    </div>

    <fieldset class="form-group">
        <label>Business Name</label>
        <input type="text" name="contact[business_name]" value="<?php echo stripslashes($contact['business_name']); ?>" class="form-control" />
    </fieldset>

    <div class="row">

        <div class="col-md-4">

            <fieldset class="form-group">
                <label>Phone - Mobile</label>
                <input type="text" name="contact[phone_mobile]" value="<?php echo $_uccms_ecomm->prettyPhone(stripslashes($contact['phone_mobile'])); ?>" class="form-control" />
            </fieldset>

        </div>

        <div class="col-md-4">

            <fieldset class="form-group">
                <label>Phone - Work</label>
                <input type="text" name="contact[phone_work]" value="<?php echo $_uccms_ecomm->prettyPhone(stripslashes($contact['phone_work'])); ?>" class="form-control" />
            </fieldset>

        </div>

        <div class="col-md-4">

            <fieldset class="form-group">
                <label>Phone - Home</label>
                <input type="text" name="contact[phone_home]" value="<?php echo $_uccms_ecomm->prettyPhone(stripslashes($contact['phone_home'])); ?>" class="form-control" />
            </fieldset>

        </div>

    </div>

    <div class="row">

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>Email - Work</label>
                <input type="text" name="contact[email_work]" value="<?php echo stripslashes($contact['email_work']); ?>" class="form-control" />
            </fieldset>

        </div>

        <div class="col-md-6">

            <fieldset class="form-group">
                <label>Email - Home</label>
                <input type="text" name="contact[email_home]" value="<?php echo stripslashes($contact['email_home']); ?>" class="form-control" />
            </fieldset>

        </div>

    </div>

    <fieldset class="form-group">
        <label>Address</label>
        <input type="text" name="contact[address1]" value="<?php echo stripslashes($contact['address1']); ?>" class="form-control" />
    </fieldset>

    <fieldset class="form-group">
        <label>Address cont.</label>
        <input type="text" name="contact[address2]" value="<?php echo stripslashes($contact['address2']); ?>" class="form-control" />
    </fieldset>

    <fieldset class="form-group">
        <label>City</label>
        <input type="text" name="contact[city]" value="<?php echo stripslashes($contact['city']); ?>" class="form-control" />
    </fieldset>

    <div class="row">

        <div class="col-md-9">

            <fieldset class="form-group">
                <label>State</label>
                <select name="contact[state]" class="custom_control form-control">
                    <option value="">Select</option>
                    <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                        <option value="<?php echo $state_code; ?>" <?php if ($state_code == $contact['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

        </div>

        <div class="col-md-3">

            <fieldset class="form-group">
                <label>Zip</label>
                <input type="text" name="contact[zip]" value="<?php echo stripslashes($contact['zip']); ?>" class="form-control" />
            </fieldset>

        </div>

    </div>

    <fieldset class="form-group">
        <label>Notes</label>
        <textarea name="contact[notes]" class="form-control"><?php echo nl2br(stripslashes($contact['notes'])); ?></textarea>
    </fieldset>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Defaults</h5>
            <div class="form-check">
                <label><input type="checkbox" name="contact[default][billing]" <?php if ($contact['default']['billing']) { ?>checked="checked"<?php } ?> class="form-check-input" <?php if ($lockedContactDefaults['billing']) { ?>disabled="disabled"<?php } ?>> Billing</label>
            </div>
            <div class="form-check">
                <label><input type="checkbox" name="contact[default][shipping]" <?php if ($contact['default']['shipping']) { ?>checked="checked"<?php } ?> class="form-check-input" <?php if ($lockedContactDefaults['shipping']) { ?>disabled="disabled"<?php } ?>> Shipping</label>
            </div>
            <div class="form-check">
                <label><input type="checkbox" name="contact[default][delivery]" <?php if ($contact['default']['delivery']) { ?>checked="checked"<?php } ?> class="form-check-input" <?php if ($lockedContactDefaults['delivery']) { ?>disabled="disabled"<?php } ?>> Delivery</label>
            </div>
        </div>
    </div>

    <?php

    // HAS STAFF EXTENSION
    if (class_exists('uccms_Staff')) {

        if (!$_uccms_staff) $_uccms_staff = new uccms_Staff();

        // HAS ACCESS
        if ($_uccms_staff->adminModulePermission()) {

            ?>

            <fieldset class="form-group" style="margin-top: 15px;">
                <label>Route</label>
                <select name="contact[route_id]" class="custom_control form-control">
                    <option value="0">None</option>
                    <?php foreach ($_uccms_staff->getRoutes() as $route) { ?>
                        <option value="<?php echo $route['id']; ?>" <?php if ($route['id'] == $contact['route_id']) { ?>selected="selected"<?php } ?>><?php echo $route['title']; ?></option>
                    <?php } ?>
                </select>
            </fieldset>

            <?php

        }
    }

    ?>

    </form>

    <?php

}

?>