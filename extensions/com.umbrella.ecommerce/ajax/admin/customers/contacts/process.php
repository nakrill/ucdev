<?php

$out = [];

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    $customer_id = (int)$_POST['customer']['id'];

    // NEW CUSTOMER
    if (!$customer_id) {

        if ($_POST['contact']['email_work']) {
            $email = $_POST['contact']['email_work'];
        } else if ($_POST['contact']['email_home']) {
            $email = $_POST['contact']['email_home'];
        }

        // ACCOUNT INFO
        $account = [
            'firstname' => $_POST['contact']['firstname'],
            'lastname' => $_POST['contact']['lastname'],
            'email' => $email
        ];
    }

    // SAVE CUSTOMER
    $customer = $_uccms_ecomm->saveCustomer([
        'id' => $customer_id,
        'account' => $account
    ]);

    // CUSTOMER SAVED
    if ($customer['id']) {

        $contact = $_POST['contact'];

        $contact['customer_id'] = $customer['id'];

        // SAVE CONTACT
        $contact = $_uccms_ecomm->saveCustomerContact($contact);

        // CONTACT SAVED
        if ($contact['id']) {
            $out['success'] = true;
            $out['id'] = $contact['id'];

        } else {
            $out['error'] = ($contact['error'] ? $contact['error'] : 'Failed to save contact.');
        }

    // FAILED TO SAVE CUSTOMER
    } else {
        $out['error'] = ($customer['error'] ? $customer['error'] : 'Failed to save customer.');
    }

}

echo json_encode($out);

?>