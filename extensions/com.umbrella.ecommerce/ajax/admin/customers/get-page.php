<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // NUMBER OF RESULTS PER PAGE
    $per_page = 20;

    $query  = isset($_GET['query']) ? $_GET['query'] : '';
    $page   = isset($_GET['page']) ? intval($_GET['page']) : 1;

    // PREVENT SQL SHENANIGANS
    $sort_by = 'a.created';
    if (isset($_GET['sort'])) {
        $valid_columns = array(
            'status'    => 'a.status',
            'username'  => 'a.username',
            'email'     => 'a.email',
            'created'   => 'a.created',
            'deleted'   => 'a.deleted',
            'name'      => 'fullname',
            'orders'    => 'num_orders'
        );
        if ($valid_columns[$_GET['sort']]) {
            $sort_by = $valid_columns[$_GET['sort']];
        }
    }
    if ($sort_by == 'a.created') {
        $sort_dir = (isset($_GET['sort_direction']) && $_GET['sort_direction'] == 'ASC') ? 'ASC' : 'DESC';
    } else {
        $sort_dir = (isset($_GET['sort_direction']) && $_GET['sort_direction'] == 'DESC') ? 'DESC' : 'ASC';
    }

    // WHERE ARRAY
    if ($_GET['deleted']) {
        $wa[] = "a.status=9";
    } else {
        $wa[] = "a.status=1";
    }

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(a.username) LIKE '%$part%') OR (LOWER(a.email) LIKE '%$part%') OR (LOWER(ad.firstname) LIKE '%$part%') OR (LOWER(ad.lastname) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED ACCOUNTS
    $accounts_query = "
    SELECT a.*, CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, ad.*, (
        SELECT COUNT(`id`) FROM `uccms_ecommerce_orders` WHERE (`customer_id`=a.id)
    ) AS `num_orders`
    FROM `uccms_accounts` AS `a`
    INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY " .$sort_by. " " .$sort_dir. "
    LIMIT " .(($page - 1) * $per_page). "," .$per_page;

    //echo $accounts_query. '<br />';
    //exit;

    $accounts_q = sqlquery($accounts_query);

    // NUMBER OF PAGED ACCOUNTS
    $num_accounts = sqlrows($accounts_q);

    // TOTAL NUMBER OF ACCOUNTS
    $total_results = sqlfetch(sqlquery("
    SELECT COUNT('x') AS `total`
    FROM `uccms_accounts` AS `a`
    INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
    WHERE (" .implode(") AND (", $wa). ")
    "));

    // NUMBER OF PAGES
    $pages = ceil($total_results['total'] / $per_page);

    // HAVE ACCOUNTS
    if ($num_accounts > 0) {

        // GET STORE PRICEGROUPS
        $spga = $_uccms_ecomm->getSetting('pricegroups');

        // LOOP
        while ($account = sqlfetch($accounts_q)) {

            unset($pricegroups);

            $account['num_quotes'] = 0;

            // GET ORDERS
            $order_query = "SELECT `id`, `quote`, `status` FROM `uccms_ecommerce_orders` WHERE (`customer_id`=" .$account['id']. ")";
            $order_q = sqlquery($order_query);
            while ($order = sqlfetch($order_q)) {
                if ($order['quote'] == 1) {
                    $account['num_quotes']++;
                    $account['num_orders']--;
                }
            }

            // PRICE GROUPS ENABLED
            if ($_uccms_ecomm->getSetting('pricegroups_enabled')) {

                // GET USER'S PRICE GROUPS
                $pg_query = "SELECT `pricegroups` FROM `" .$_uccms_ecomm->tables['customers']. "` WHERE (`id`=" .$account['id']. ")";
                $pg_q = sqlquery($pg_query);
                $pg = sqlfetch($pg_q);

                // HAVE PRICE GROUPS
                if ($pg['pricegroups']) {
                    $pga = explode(',', stripslashes($pg['pricegroups']));
                    if (count($pga) > 0) {
                        foreach ($pga as $pg_id) {
                            $pricegroups .= '<span class="pricegroup">' .$spga[$pg_id]['title']. '</span>';
                        }
                    }
                }
            }

            ?>

            <li id="row_<?=$account['id']?>">
                <section class="view_column customers_name"><a href="../customers/edit/?id=<?php echo $account['id']; ?>" style="display: inline;"><?php echo trim(stripslashes($account['firstname']. ' ' .$account['lastname'])); ?></a></section>
                <section class="view_column customers_email"><?php echo stripslashes($account['email']); ?></section>
                <section class="view_column customers_created"><?php echo date('n/j/Y g:i a', strtotime($account['created'])); ?></section>
                <?php if ($_uccms_ecomm->getSetting('pricegroups_enabled')) { ?>
                    <section class="view_column customers_pricegroups">
                        <?php echo $pricegroups; ?>
                    </section>
                <?php } ?>
                <section class="view_column customers_quotes">
                    <a href="../quotes/?customer_id=<?php echo $account['id']; ?>&status=all" title="Quotes" style="display: inline;"><?php echo number_format((int)$account['num_quotes'], 0); ?></a>
                </section>
                <section class="view_column customers_orders">
                    <a href="../orders/?customer_id=<?php echo $account['id']; ?>&status=all" title="Orders" style="display: inline;"><?php echo number_format((int)$account['num_orders'], 0); ?></a>
                </section>
            </li>

            <?
        }

    // NO ACCOUNTs
    } else {
        ?>
        <li style="text-align: center;">
            No customers.
        </li>
        <?php
    }

    ?>

    <script>
        BigTree.setPageCount("#view_paging",<?=$pages?>,<?=$page?>);
    </script>

    <?php

}

?>