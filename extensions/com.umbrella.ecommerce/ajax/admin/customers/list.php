<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    //$_REQUEST['limit'] = 1;

    // GET CUSTOMERS
    $customers = $_uccms_ecomm->getCustomers($_REQUEST);

    ?>

    <?php if (count((array)$customers['customers']) > 0) { ?>

        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th class="name" scope="col">Name</th>
                    <th class="status" scope="col">Status</th>
                    <th class="options" scope="col">Options</th>
                </tr>
            </thead>
            <tbody class="items">

                <?php foreach ($customers['customers'] as $customer) { ?>

                    <tr class="item" data-id="<?php echo $customer['id']; ?>">
                        <td class="name">
                            <a href="./edit/?id=<?php echo $customer['id']; ?>" class="title main"><?php echo trim(stripslashes($customer['firstname']. ' ' .$customer['lastname'])); ?></a>
                        </td>
                        <td class="status">
                            <span class="badge" style="background-color: <?php echo $_uccms_ecomm->customerStatuses($customer['status'])['color']; ?>;"><?php echo $_uccms_ecomm->customerStatuses($customer['status'])['title']; ?></span>
                        </td>
                        <td class="options">
                            <a href="./edit/?id=<?php echo $customer['id']; ?>" class="edit" title="Edit" data-id="<?php echo $customer['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                        </td>
                    </tr>

                <?php } ?>

            </tbody>
        </table>

    <?php } else { ?>

        <div class="no-results">
            No customers found.
        </div>

    <?php } ?>

    <?php

}

?>

<script>
    BigTree.setPageCount('#customers .summary .view_paging' ,<?php echo (int)$customers['pages']; ?>, <?php echo (int)$customers['page']; ?>);
</script>