<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        // GET CART ITEMS
        $citems = $_uccms_ecomm->cartItems($cart['id'], false, "`id` ASC");

        // EDIT URL
        $edit_url = '/admin/' .$_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$cart['id'];

        // HAVE COUPON
        $coupon = [];
        if ($cart['coupon_id']) {
            $coupon_query = "SELECT * FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (`id`=" .$cart['coupon_id']. ")";
            $coupon_q = sqlquery($coupon_query);
            $coupon = sqlfetch($coupon_q);
        }

        ?>

        <link rel="stylesheet" type="text/css" href="/css/lib/loading.css"/>
        <link rel="stylesheet" type="text/css" href="/css/lib/loading-btn.css"/>

        <style type="text/css">

            #admin_footer-cart > .checkout .autosaving {
                display: none;
                position: absolute;
                top: 0px;
                right: 0px;
                padding: 5px 10px;
                background-image: url('/images/misc/loading-bar-1.gif');
                background-repeat: repeat-x;
                background-size: cover;
                color: #888;
            }

            #admin_footer-cart > .checkout ul.accordion {
                list-style: none;
                margin: -.5em 0 0 0;
                padding: 0;
            }
            #admin_footer-cart > .checkout ul.accordion .inner {
                padding: 15px;
                overflow: hidden;
                display: none;
            }
            #admin_footer-cart > .checkout ul.accordion .inner.show {
                /*display: block;*/
            }
            #admin_footer-cart > .checkout ul.accordion li {
                margin: .5em 0;
            }
            #admin_footer-cart > .checkout ul.accordion li a.toggle {
                display: block;
                padding: 10px;
                background: rgba(0, 0, 0, 0.1);
                color: #333;
                font-weight: bold;
                transition: background .3s ease;
            }
            #admin_footer-cart > .checkout ul.accordion li a.toggle:hover {
                background: rgba(0, 0, 0, 0.2);
            }
            #admin_footer-cart > .checkout ul.accordion li a.toggle i {
                padding-right: 5px;
            }

            #admin_footer-cart > .checkout .checkout_columns {
                margin: 0 -15px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column {
                float: left;
            }
            #admin_footer-cart > .checkout .checkout_columns .column > .contain {
                margin: 0 15px 15px;
                padding: 0px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column .cols {
                margin: 0 -15px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column .cols .half {
                float: left;
                width: 50%;
            }
            #admin_footer-cart > .checkout .checkout_columns .column .cols .half > .contain {
                margin: 0 15px;
                padding: 0px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column fieldset {
                margin-bottom: 15px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column fieldset:last-child {
                margin-bottom: 0px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column fieldset label {
                margin-bottom: 5px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column fieldset input {
                width: 100%;
                box-sizing: border-box;
            }
            #admin_footer-cart > .checkout .checkout_columns .column fieldset select {

            }

            #admin_footer-cart > .checkout .checkout_columns .column.customer {
                width: 33%;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.customer .customer_update-success {
                display: none;
                padding-top: 5px;
                color: green;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.customer .shipping_same input {
                -webkit-appearance: checkbox;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.customer .cols.shipping {
                padding-top: 15px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.cart {
                width: 34%;
            }


            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full {
                width: 100%;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full tr:nth-child(even) td {
                background: #FFF;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full tr:nth-child(odd) td {
                background: #EEE;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full th {
                padding: 10px;
                border-bottom: 1px solid #eee;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full td {
                padding: 10px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .thumb {
                width: 10%;
                text-align: center;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .title {
                width: 50%;
                text-align: left;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .title .item_title {
                margin-bottom: 2px;
                font-weight: bold;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .price {
                width: 15%;
                text-align: left;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .quantity {
                width: 15%;
                text-align: left;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .total {
                width: 10%;
                text-align: right;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .total .actions {
                display: none;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item > td {
                border-bottom: 1px solid #eee;
            }
            /*
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item.quote {
                opacity: .5;
            }
            */
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .thumb {
                vertical-align: top;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .thumb img {
                height: 50px;
                max-width: 60px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .booking {
                margin: 2px 0 4px;
                font-size: .9em;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .options {
                margin: 0px;
                padding: 0 0 0 15px;
                font-size: .8em;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .options ul {
                margin: 0px;
                padding: 0 0 0 15px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person {
                margin-bottom: 2px;
                font-size: .8em;
                line-height: 1.2em;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person:last-child {
                margin-bottom: 0px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person .label, #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person .input {
                float: left;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person .label {
                padding-right: 5px;
                opacity: .5;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person .input {
                padding-right: 5px;
                opacity: .7;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person .input.dob {
                padding-left: 5px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person .input.email {
                padding-left: 5px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .persons .person .input.notes {
                padding-left: 5px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .all .actions, #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .all .remove {
                display: none;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .item .backordered {
                font-size: .8em;
                color: #E74541;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub {
                text-align: right;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.payment {
                width: 33%;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .contain {
                margin-right: 0px;
                margin-bottom: 0px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.payment .shipping .shipping_method {
                padding: 15px;
                background-color: #eee;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .shipping .shipping_method .services {
                margin: 0px;
                list-style: none;
                color: #333;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .shipping .shipping_method .services input {
                -webkit-appearance: radio;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.payment .notes {
                margin-top: 20px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .notes textarea {
                width: 100%;
                height: 60px;
                box-sizing: border-box;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment {
                margin-top: 20px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .methods {
                padding: 15px;
                background-color: #eee;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method {
                margin-bottom: 5px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method:last-child {
                margin-bottom: 0px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .top input {
                -webkit-appearance: radio;
                cursor: pointer;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .top label {
                display: inline-block;
                margin: 0px;
                cursor: pointer;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .info {
                display: none;
                padding: 10px 25px 0;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .new_card {
                margin: 0 -15px;
                padding-top: 15px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .new_card fieldset {
                float: left;
                width: 42%;
                margin: 0 15px 15px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .new_card .cc_icons img {
                margin-top: 15px;
            }

            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit {
                margin: 20px 0;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn {
                display: block;
                color: #fff;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn.disabled {
                background-color: #ccc;
                cursor: default;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn.running {
                background-color: #666;
                cursor: default;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn i {
                margin-right: 8px;
                font-size: 1.2em;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn.running i {
                display: none;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn .ld {
                display: none;
                margin-right: 8px;
                vertical-align: middle;
                font-size: 1.2em;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn.running .ld {
                display: inline-block;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success {
                display: none;
                margin: 15px 0 30px;
                text-align: center;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success i {
                font-size: 150px;
                color: #43A047;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success h2 {
                color: #666;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success h4 {
                color: #666;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success ul {
                margin-top: 7px;
                list-style: none;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success ul li {
                font-size: 14px;
                margin: 0 0 10px;
                line-height: 1em;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .amount {
                float: left;
                width: 50%;
                max-width: 200px;
                margin-top: 15px;
                padding-right: 15px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .discount {
                float: left;
                width: 50%;
                margin-top: 15px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .discount select {
                color: #444;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .discount input {
                display: inline-block;
                width: 50%;
                max-width: 200px;
            }
            #admin_footer-cart > .checkout .checkout_columns .column.payment .discount a {
                display: inline-block;
                padding: 0 0 0 10px;
                line-height: 30px;
                font-size: .9em;
            }

            <? /*@media (max-width: 1679px) {*/ ?>
            @media (max-width: 1400px) {

                #admin_footer-cart > .checkout .checkout_columns .column.customer, #admin_footer-cart > .checkout .checkout_columns .column.cart {
                    width: 50%;
                }
                #admin_footer-cart > .checkout .checkout_columns .column.payment {
                    clear: both;
                    float: none;
                    width: 100%;
                    margin-top: 20px;
                }

            }

            @media (max-width: 991px) {

                #admin_footer-cart > .checkout .checkout_columns .column.customer, #admin_footer-cart > .checkout .checkout_columns .column.cart {
                    clear: both;
                    float: none;
                    width: 100%;
                }
                #admin_footer-cart > .checkout .checkout_columns .column.cart {
                    margin-top: 20px;
                }

            }

        </style>

        <script type="text/javascript">
            $(document).ready(function() {

                // SHIPPING
                afc_checkout_shippingSection();

                // FIELD CHANGE
                $('#admin_footer-cart > .checkout .checkout_columns').on('change', '.autosave', function() {
                    afc_checkout_autoSave({}, function(data) {
                        afc_getCart();
                    });
                });

                // ACCORDION
                $('#admin_footer-cart > .checkout ul.accordion li a.toggle').click(function(e) {
                    e.preventDefault();
                    var $this = $(this);
                    if ($this.next().hasClass('show')) {
                        $this.next().removeClass('show');
                        $this.next().slideUp(350, function() {
                            $this.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                        });
                    } else {
                        $this.parent().parent().find('li .inner').removeClass('show');
                        $this.parent().parent().find('li .inner').slideUp(350, function() {
                            $(this).prev('.toggle').find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                        });
                        $this.next().toggleClass('show');
                        $this.next().slideToggle(350, function() {
                            $this.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
                        });
                    }
                });

                // CUSTOMER ACCOUNT CHANGE
                $('#admin_footer-cart > .checkout .checkout_columns .column.customer select[name="order[customer_id]"]').change(function() {
                    $('#admin_footer-cart > .checkout .checkout_columns .column.customer .customer_update-success').hide();
                    $.post('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/orders/set-customer/', {
                        order_id: <?php echo $cart['id']; ?>,
                        customer_id: $(this).val(),
                    }, function(data) {
                        if (data['success']) {
                            if (window.location.pathname == '/admin/com.umbrella.ecommerce*ecommerce/orders/edit/') {
                                location.reload();
                            } else {
                                $('#admin_footer-cart > .checkout .checkout_columns .column.customer .customer_update-success').show();
                                afc_getCart({}, function() {
                                    $('#admin_footer-cart .cart_container .right_section .actions .btn i').removeClass('fa-chevron-right').addClass('fa-chevron-left');
                                });
                                setTimeout(function() {
                                    afc_getCheckout();
                                }, 1000);
                            }
                        } else {
                            if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Failed to assign customer to order.');
                            }
                        }
                    }, 'json');
                });

                // ADDRESS CHANGE
                $('#admin_footer-cart > .checkout .checkout_columns .column.customer select[name="billing[state]"], #admin_footer-cart > .checkout .checkout_columns .column.customer input[name="billing[zip]"], #admin_footer-cart > .checkout .checkout_columns .column.customer select[name="shipping[state]"], #admin_footer-cart > .checkout .checkout_columns .column.customer input[name="shipping[zip]"]').change(function() {
                    afc_checkout_orderTax({}, function() {
                        afc_checkout_shippingSection({}, function() {
                            afc_checkout_updateCartTotal();
                        });
                    });
                });

                // SHIPPING SAME
                $('#admin_footer-cart > .checkout .checkout_columns .column.customer .shipping_same input').click(function() {
                    afc_checkout_orderTax();
                    afc_checkout_shippingSection();
                    afc_checkout_updateCartTotal();
                    if ($(this).is(':checked')) {
                        $('#admin_footer-cart > .checkout .checkout_columns .column.customer .cols.shipping').slideUp(350);
                    } else {
                        $('#admin_footer-cart > .checkout .checkout_columns .column.customer .cols.shipping').slideDown(350);
                    }
                });

                // PAYMENT METHOD SELECT
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method input[name="payment[method]"]').click(function() {
                    $('#admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .info').hide();
                    $(this).closest('.method').find('.info').show();
                });

                // CARD SELECT
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .payment_profile select[name="payment[profile_id]"').change(function() {
                    var $info = $(this).closest('.info');
                    if ($(this).val()) {
                        $info.find('.new_card').hide();
                    } else {
                        $info.find('.new_card').show();
                    }
                });

                // CREDIT CARD EXPIRATION SLASH
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .payment .method .new_card .expiration').keyup(function(e) {
                    if (e.keyCode != 8) { // backspace
                        if (e.keyCode == 111) { // forward slash
                            $(this).val($(this).val().substr(0, $(this).val().length - 1));
                        } else if ($(this).val().length == 2) {
                            $(this).val($(this).val()+ '/');
                        }
                    }
                });

                // DISCOUNT TYPE CHANGE
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .discount select').change(function(e) {
                    var type = $(this).val();
                    $('#admin_footer-cart > .checkout .checkout_columns .column.payment .discount input').hide();
                    $('#admin_footer-cart > .checkout .checkout_columns .column.payment .discount input[name="discount[' +type+ ']"]').show();
                });

                // APPLY DISCOUNT CLICK
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .discount a').click(function(e) {
                    e.preventDefault();
                    var type = $('#admin_footer-cart > .checkout .checkout_columns .column.payment .discount select').val();
                    $.post('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/footer-cart/apply-discount-' +type+ '/', {
                        value: $('#admin_footer-cart > .checkout .checkout_columns .column.payment .discount input[name="discount[' +type+ ']"]').val()
                    }, function(data) {
                        if (data['success']) {
                            alert(data['success']);
                            afc_getCart();
                            afc_getCheckout();
                        } else {
                            if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Failed to process discount.');
                            }
                        }
                    }, 'json');
                });

                // COMPLETE ORDER
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn').click(function(e) {
                    e.preventDefault();
                    if (!$(this).hasClass('running')) {
                        var payment_method = $('#afc_checkout input[name="payment[method]"]:checked').val();
                        if (payment_method == undefined) {
                            if (confirm('No payment method selected. Continue?')) {
                                afc_checkout_process();
                            }
                        } else {
                            afc_checkout_process();
                        }
                    }
                });

                // SUCCESS - START A NEW CART
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success ul li .new').click(function(e) {
                    e.preventDefault();
                    $('#admin_footer-cart .cart_container .right_section .actions .btn').click();
                    afc_startCart({}, function() {
                        if (window.location.pathname == '/admin/com.umbrella.ecommerce*ecommerce/orders/edit/') {
                            window.location = './?id=' +data.id;
                        }
                    });
                });

            });

            // AUTO SAVE
            function afc_checkout_autoSave(settings, callback) {
                $('#admin_footer-cart > .checkout .autosaving').show();
                $.post('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/footer-cart/save-cart/', $('#afc_checkout').serialize(), function(data) {
                    $('#admin_footer-cart > .checkout .autosaving').fadeOut(350);
                    if (typeof callback == 'function') {
                        callback(data);
                    }
                });
            }

            // GET TAX INFO
            function afc_checkout_orderTax(settings, callback) {
                $.get('/*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/checkout/tax/', {
                    'billing_state': $('#admin_footer-cart > .checkout .checkout_columns .column.customer select[name="billing[state]"]').val(),
                    'billing_zip': $('#admin_footer-cart > .checkout .checkout_columns .column.customer input[name="billing[zip]"]').val(),
                    'shipping_state': $('#admin_footer-cart > .checkout .checkout_columns .column.customer select[name="shipping[state]"]').val(),
                    'shipping_zip': $('#admin_footer-cart > .checkout .checkout_columns .column.customer input[name="shipping[zip]"]').val(),
                    'shipping_same': $('#admin_footer-cart > .checkout .checkout_columns .column.customer input[name="order[shipping_same]"]').prop('checked')
                }, function(data) {
                    $('#admin_footer-cart > .checkout .checkout_columns .column.cart .tax .num').text(data['tax']['total']);
                    if (typeof callback == 'function') {
                        callback();
                    }
                }, 'json');
            }

            // GET SHIPPING SECTION
            function afc_checkout_shippingSection(settings, callback) {
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .shipping_method').html('Loading shipping options..');
                if ($('#admin_footer-cart > .checkout .checkout_columns .column.customer .shipping_same input').is(':checked')) {
                    var state = $('#admin_footer-cart > .checkout .checkout_columns .column.customer select[name="billing[state]"]').val();
                    var zip = $('#admin_footer-cart > .checkout .checkout_columns .column.customer input[name="billing[zip]"]').val();
                } else {
                    var state = $('#admin_footer-cart > .checkout .checkout_columns .column.customer select[name="shipping[state]"]').val();
                    var zip = $('#admin_footer-cart > .checkout .checkout_columns .column.customer input[name="shipping[zip]"]').val();
                }
                $.get('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/footer-cart/shipping-html/', {
                    'selected': $('#afc_hv_shipping_service').text(),
                    'state': state,
                    'zip': zip
                }, function(data) {
                    $('#admin_footer-cart > .checkout .checkout_columns .column.payment .shipping_method').html(data);
                    if (typeof callback == 'function') {
                        callback();
                    }
                }, 'html');
            }

            // GET GRATUITY
            function afc_checkout_orderGratuity(settings, callback) {
                $.get('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/footer-cart/gratuity/', function(data) {
                    $('#admin_footer-cart > .checkout .checkout_columns .column.cart .gratuity .num').text(data['gratuity']);
                    if (typeof callback == 'function') {
                        callback();
                    }
                }, 'json');
            }

            // UPDATE CART TOTAL
            function afc_checkout_updateCartTotal() {

                afc_checkout_orderGratuity({}, function() {

                    var subtotal = Number($('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.subtotal .num').text().replace(',', ''));
                    /*
                    $('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.total .num').each(function() {
                        subtotal += parseFloat($(this).text().replace(',', ''));
                    });
                    */
                    var discount = 0;
                    if ($('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.pre').length) {
                        discount += parseFloat($('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.pre .num').text().replace(',', ''));
                    }
                    var tax = parseFloat($('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.tax .num').text().replace(',', ''));
                    var gratuity = $('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.gratuity .num').text();
                    if (gratuity) {
                        gratuity = parseFloat(gratuity.replace(',', ''));
                    } else {
                        gratuity = 0;
                    }
                    var servicefee = $('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.service_fee .num').text();
                    if (servicefee) {
                        servicefee = parseFloat(servicefee.replace(',', ''));
                    } else {
                        servicefee = 0;
                    }
                    var shipping = $('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.shipping .num').text();
                    if (shipping == 'Free') {
                        shipping = 0;
                    } else {
                        shipping = parseFloat(shipping.replace(',', ''));
                    }
                    var total = (subtotal - discount + gratuity + servicefee + tax + shipping).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    $('#admin_footer-cart .cart_container .right_section .total .num').text(total);
                    $('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.total .num').text(total);
                    $('#admin_footer-cart > .checkout .checkout_columns .column.payment .amount input').val(total);

                });

            }

            // PROCESS
            function afc_checkout_process() {
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn').addClass('running');
                $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn .title').text('Processing..');
                $.post('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/footer-cart/checkout-process/', $('#afc_checkout').serialize(), function(data) {
                    if (data.success) {
                        $('#admin_footer-cart > .checkout .checkout_columns .column.payment .payment').fadeOut(350);
                        $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit').fadeOut(350, function() {
                            $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit_success').fadeIn(350);
                        });
                    } else {
                        $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn').removeClass('running');
                        $('#admin_footer-cart > .checkout .checkout_columns .column.payment .submit .btn .title').text('Complete Order');
                        if (data.error) {
                            alert(data.error);
                        } else {
                            alert('Failed to process order.');
                        }
                    }
                }, 'json');

            }

        </script>

        <form id="afc_checkout">

        <div class="autosaving">Autosaving..</div>

        <div class="checkout_columns clearfix">

            <div class="column customer">
                <div class="contain">

                    <div class="order">

                        <h3 class="h5"><?php echo ($cart['quote'] == 1 ? 'Quote' : 'Order'); ?></h3>

                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><strong>ID:</strong>&nbsp;</td>
                                <td><strong><?php echo $cart['id']; ?></strong></td>
                            </tr>
                            <tr>
                                <td>Status:&nbsp;</td>
                                <td><?php echo ucwords($cart['status']); ?></td>
                            </tr>
                            <tr>
                                <td>Day:&nbsp;</td>
                                <td><?php echo date('n/j/Y', strtotime($cart['dt'])); ?></td>
                            </tr>
                            <tr>
                                <td>Time:&nbsp;</td>
                                <td><?php echo date('g:i A T', strtotime($cart['dt'])); ?></td>
                            </tr>
                            <tr>
                                <td>IP:&nbsp;</td>
                                <td><?php echo $cart['ip']; ?></td>
                            </tr>
                            <?php if ($cart['referrer_host']) { ?>
                                <tr>
                                    <td>Referrer:&nbsp;</td>
                                    <td><?php echo stripslashes(urldecode($cart['referrer_host'])); ?></td>
                                </tr>
                            <?php } ?>
                        </table>

                    </div>

                    <div class="customer">

                        <h3 class="h5">Customer</h3>

                        <ul class="accordion">

                            <li>
                                <a href="#" class="toggle"><i class="fa fa-chevron-up" aria-hidden="true"></i>Existing</a>
                                <div class="inner show" style="display: block;">

                                    <fieldset class="form-group">
                                        <select name="order[customer_id]" class="custom_control form-control">
                                            <option value="0">None</option>
                                            <optgroup label="Accounts">
                                                <?php
                                                $accounts_query = "
                                                SELECT a.id, a.username, a.email, CONCAT(ad.firstname, ' ', ad.lastname) AS `fullname`
                                                FROM `" .$_uccms_ecomm->tables['accounts']. "` AS `a`
                                                INNER JOIN `" .$_uccms_ecomm->tables['account_details']. "` AS `ad` ON a.id=ad.id
                                                ORDER BY `fullname` ASC, a.username ASC
                                                ";
                                                $accounts_q = sqlquery($accounts_query);
                                                while ($acct = sqlfetch($accounts_q)) {
                                                    ?>
                                                    <option value="<?php echo $acct['id']; ?>" <?php if ($acct['id'] == $cart['customer_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($acct['fullname']); ?> (<?php echo $acct['email']; ?>)</option>
                                                    <?php
                                                }
                                                ?>
                                            </optgroup>
                                        </select>
                                        <div class="customer_update-success">
                                            Account updated.
                                        </div>
                                    </fieldset>

                                </div>
                            </li>

                            <li>
                                <a href="#" class="toggle"><i class="fa fa-chevron-down" aria-hidden="true"></i>Billing</a>
                                <div class="inner">

                                    <div class="cols billing clearfix">

                                        <div class="half">
                                            <div class="contain">

                                                <fieldset class="form-group billing_firstname">
                                                    <label>First Name *</label>
                                                    <input type="text" name="billing[firstname]" value="<?php echo stripslashes($cart['billing_firstname']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group billing_lastname">
                                                    <label>Last Name *</label>
                                                    <input type="text" name="billing[lastname]" value="<?php echo stripslashes($cart['billing_lastname']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group billing_phone">
                                                    <label>Phone</label>
                                                    <input type="text" name="billing[phone]" value="<?php echo stripslashes($cart['billing_phone']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group billing_email">
                                                    <label>Email *</label>
                                                    <input type="text" name="billing[email]" value="<?php echo stripslashes($cart['billing_email']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group billing_company">
                                                    <label>Company Name</label>
                                                    <input type="text" name="billing[company]" value="<?php echo stripslashes($cart['billing_company']); ?>" class="autosave form-control" />
                                                </fieldset>

                                            </div>
                                        </div>

                                        <div class="half">
                                            <div class="contain">

                                                <fieldset class="form-group billing_address1">
                                                    <label>Address *</label>
                                                    <input type="text" name="billing[address1]" value="<?php echo stripslashes($cart['billing_address1']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group billing_address2">
                                                    <label>Address (Apt, Bldg)</label>
                                                    <input type="text" name="billing[address2]" value="<?php echo stripslashes($cart['billing_address2']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group billing_city">
                                                    <label>City *</label>
                                                    <input type="text" name="billing[city]" value="<?php echo stripslashes($cart['billing_city']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group billing_state">
                                                    <label>State *</label>
                                                    <select name="billing[state]" class="autosave form-control">
                                                        <option value="">Select</option>
                                                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $cart['billing_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>

                                                <fieldset class="form-group billing_zip">
                                                    <label>Zip *</label>
                                                    <input type="text" name="billing[zip]" value="<?php echo $cart['billing_zip']; ?>" class="autosave form-control" />
                                                </fieldset>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </li>

                            <li>
                                <a href="#" class="toggle"><i class="fa fa-chevron-down" aria-hidden="true"></i>Shipping</a>
                                <div class="inner">

                                    <div class="shipping_same">
                                        <input type="checkbox" name="order[shipping_same]" value="1" <?php if ($cart['shipping_same']) { ?>checked="checked"<?php } ?> class="autosave" /> Same as billing
                                    </div>

                                    <div class="cols shipping clearfix" <?php if ($cart['shipping_same']) { ?>style="display: none;"<?php } ?>>

                                        <div class="half">
                                            <div class="contain">

                                                <fieldset class="form-group shipping_firstname">
                                                    <label>First Name *</label>
                                                    <input type="text" name="shipping[firstname]" value="<?php echo stripslashes($cart['shipping_firstname']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group shipping_lastname">
                                                    <label>Last Name *</label>
                                                    <input type="text" name="shipping[lastname]" value="<?php echo stripslashes($cart['shipping_lastname']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group shipping_phone">
                                                    <label>Phone</label>
                                                    <input type="text" name="shipping[phone]" value="<?php echo stripslashes($cart['shipping_phone']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group shipping_email">
                                                    <label>Email *</label>
                                                    <input type="text" name="shipping[email]" value="<?php echo stripslashes($cart['shipping_email']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group shipping_company">
                                                    <label>Company Name</label>
                                                    <input type="text" name="shipping[company]" value="<?php echo stripslashes($cart['shipping_company']); ?>" class="autosave form-control" />
                                                </fieldset>

                                            </div>
                                        </div>

                                        <div class="half">
                                            <div class="contain">

                                                <fieldset class="form-group shipping_address1">
                                                    <label>Address *</label>
                                                    <input type="text" name="shipping[address1]" value="<?php echo stripslashes($cart['shipping_address1']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group shipping_address2">
                                                    <label>Address (Apt, Bldg)</label>
                                                    <input type="text" name="shipping[address2]" value="<?php echo stripslashes($cart['shipping_address2']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group shipping_city">
                                                    <label>City *</label>
                                                    <input type="text" name="shipping[city]" value="<?php echo stripslashes($cart['shipping_city']); ?>" class="autosave form-control" />
                                                </fieldset>

                                                <fieldset class="form-group shipping_state">
                                                    <label>State *</label>
                                                    <select name="shipping[state]" class="autosave form-control">
                                                        <option value="">Select</option>
                                                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $cart['shipping_state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>

                                                <fieldset class="form-group shipping_zip">
                                                    <label>Zip *</label>
                                                    <input type="text" name="shipping[zip]" value="<?php echo $cart['shipping_zip']; ?>" class="autosave form-control" />
                                                </fieldset>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </li>

                        </ul>

                    </div>

                </div>
            </div>

            <div class="column cart">
                <div class="contain">

                    <h3 class="h5">Cart</h3>

                    <table class="cart_full" cellpadding="0" cellspacing="0">

                        <tr>
                            <th class="thumb"></th>
                            <th class="title">Item</th>
                            <th class="price">Price</th>
                            <th class="quantity">Quantity</th>
                            <th class="total">Total</th>
                        </tr>

                        <?php

                        $order = $cart;

                        // SUBTOTAL
                        $order['subtotal'] = 0;

                        // ATTRIBUTE ARRAY
                        $attra = array();

                        // AFTER TAX ITEM ARRAY
                        $atia = array();

                        // LOOP THROUGH ITEMS
                        foreach ($citems as $oitem) {

                            // AFTER TAX ITEM
                            if ($oitem['after_tax']) {

                                // ADD TO AFTER TAX ITEM ARRAY
                                $atia[] = $oitem;

                            // NORMAL ITEM
                            } else {

                                // GET ITEM INFO
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                // SETTINGS FOR DISPLAYING ITEM IN CART
                                $item_cart_settings = array();

                                // DISPLAY ITEM IN CART
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                                unset($item, $oitem);

                            }

                        }

                        $taxvals = array(
                            'quote' => false
                        );

                        // HAVE BILLING STATE AND/OR ZIP
                        if (($cart['billing_state']) || ($cart['billing_zip'])) {
                            $taxvals['country'] = 'United States';
                            $taxvals['state']   = $cart['billing_state'];
                            $taxvals['zip']     = $cart['billing_zip'];
                        }

                        // GET TAX
                        $tax = $_uccms_ecomm->cartTax($cart['id'], $citems, $taxvals);

                        $order['tax'] = $tax['total'];

                        $order['order_subtotal'] = $order['subtotal'];

                        // GET DISCOUNT
                        $discount = $_uccms_ecomm->cartDiscount($order, $citems);

                        ?>

                        <tr class="sub subtotal <?php if (!$discount['discount']) { ?>bold<?php } ?>">
                            <td colspan="5">
                                Subtotal: $<span class="num"><?php echo number_format($order['subtotal'], 2); ?></span>
                            </td>
                        </tr>

                        <?php

                        // HAVE DISCOUNT
                        if ($discount['discount']) {
                            $order['discount'] = $discount['discount'];
                            $t_tax = $order['tax'];
                            unset($order['tax']);
                            ?>
                            <tr class="sub pre">
                                <td colspan="5">
                                    After discount (<?php if ($discount['coupon']['code']) { ?>Coupon: <?php echo $discount['coupon']['code']; ?> - <?php } ?>$<span class="num"><?php echo number_format($discount['discount'], 2); ?></span>): $<?php echo $_uccms_ecomm->orderTotal($order); ?>
                                </td>
                            </tr>
                            <?php
                            $order['tax'] = $t_tax;
                            $order['discount'] = $discount['discount'];
                        }

                        ?>

                        <tr class="sub tax">
                            <td colspan="5">
                                Tax: $<span class="num"><?php echo number_format($order['tax'], 2); ?></span>
                            </td>
                        </tr>

                        <tr class="sub shipping <?php if (count($atia) > 0) { echo 'with_after_tax'; } ?>">
                            <td colspan="5">
                                Shipping: $<span class="num"><?php echo number_format($order['shipping'], 2); ?></span>
                            </td>
                        </tr>

                        <?php if ($_uccms_ecomm->getSetting('gratuity_enabled')) { ?>
                            <?php
                            $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);
                            ?>
                            <tr class="sub gratuity">
                                <td colspan="5">
                                    Gratuity: $<span class="num"><?php echo $order['gratuity']; ?></span>
                                </td>
                            </tr>
                        <?php } ?>

                        <?php

                        // MAIN SERVICE FEE
                        $order['servicefee'] = $_uccms_ecomm->orderServiceFee($order);

                        // HAVE FEE(S)
                        if ($order['servicefee']) {

                            ?>
                            <tr class="sub service_fee">
                                <td colspan="5">
                                    Service Fee: $<span class="num"><?php echo number_format($order['servicefee'], 2); ?></span>
                                </td>
                            </tr>
                        <?php } ?>

                        <?php

                        // HAVE AFTER TAX ITEMS
                        if (count($atia) > 0) {

                            ?>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <?php

                            // LOOP
                            foreach ($atia as $oitem) {

                                // GET ITEM INFO
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                // SETTINGS FOR DISPLAYING ITEM IN CART
                                $item_cart_settings = array(
                                    'after_tax' => true
                                );

                                // DISPLAY ITEM IN CART
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                                unset($item, $oitem);

                            }

                        }

                        ?>

                        <tr class="sub total">
                            <td colspan="5">
                                Total: $<span class="num"><?php echo number_format($_uccms_ecomm->orderTotal($order), 2); ?></span>
                            </td>
                        </tr>

                    </table>

                </div>
            </div>

            <div class="column payment">
                <div class="contain">

                    <div id="afc_hv_shipping_service" style="display: none;"><?php echo $cart['shipping_service']; ?></div>

                    <div class="shipping">

                        <h3 class="h5">Shipping</h3>

                        <div class="shipping_method"></div>

                    </div>

                    <div class="notes">
                        <textarea name="order[notes]" placeholder="Notes" class="autosave" class="form-control"><?php echo stripslashes($cart['notes']); ?></textarea>
                    </div>

                    <div class="payment">

                        <h3 class="h5">Payment</h3>

                        <div class="methods">

                            <?php

                            // GET PAYMENT METHODS
                            $payment_methods = $_uccms_ecomm->getPaymentMethods(true);

                            /*
                            // MAKE SURE CHECK / CASH IS OPTION
                            if (!$payment_methods['check_cash']) {
                                $payment_methods['check_cash'] = array(
                                    'id'        => 'check_cash',
                                    'active'    => 1,
                                    'title'     => 'Check / Cash'
                                );
                            }
                            */

                            // PAYMENT PROFILES
                            $payment_profiles = array();

                            // PAYMENT PROFILES ENABLED
                            if ($_uccms['_account']->paymentProfilesEnabled()) {

                                // GET PAYMENT PROFILES
                                $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$cart['customer_id']. ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
                                $pp_q = sqlquery($pp_query);
                                while ($pp = sqlfetch($pp_q)) {
                                    $payment_profiles[$pp['id']] = $pp;
                                }

                            }

                            // PAYMENT METHODS
                            foreach ($payment_methods as $payment) {

                                ?>

                                <div class="method">
                                    <div class="top">
                                        <input id="afc_payment-method-<?php echo $payment['id']; ?>" type="radio" name="payment[method]" value="<?php echo $payment['id']; ?>" />
                                        <label for="afc_payment-method-<?php echo $payment['id']; ?>"><?php echo stripslashes($payment['title']); ?></label>
                                    </div>
                                    <div class="info">

                                        <?php if ($payment['id'] == 'check_cash') { ?>

                                            <fieldset class="form-group">
                                                <label>Check # / Note</label>
                                                <input type="text" name="payment[<?php echo $payment['id']; ?>][note]" value="" class="form-control" />
                                            </fieldset>

                                        <?php } else if ($payment['hide_cc']) { ?>

                                            <fieldset class="form-group">
                                                <label>Note</label>
                                                <input type="text" name="payment[<?php echo $payment['id']; ?>][note]" value="" class="form-control" />
                                            </fieldset>

                                        <?php } else { ?>

                                            <?php if (count($payment_profiles) > 0) { ?>
                                                <div class="payment_profile">
                                                    <fieldset class="form-group">
                                                        <select name="payment[profile_id]">
                                                            <?php foreach ($payment_profiles as $pp) { ?>
                                                                <option value="<?php echo $pp['id']; ?>"><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                                                            <?php } ?>
                                                            <option value="">New Card</option>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            <?php } ?>

                                            <div class="new_card clearfix" style="<?php if (count($payment_profiles) > 0) { ?>display: none;<?php } ?>">

                                                <fieldset class="form-group">
                                                    <label>Name On Card *</label>
                                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][name]" value="" class="form-control" />
                                                </fieldset>

                                                <fieldset class="form-group">
                                                    <label>Card Number *</label>
                                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][number]" value="" class="form-control" />
                                                </fieldset>

                                                <fieldset class="form-group">
                                                    <label>Card Expiration *</label>
                                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][expiration]" value="" placeholder="mm/yy" class="expiration form-control" />
                                                </fieldset>

                                                <fieldset class="form-group">
                                                    <label>Card Zip *</label>
                                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][zip]" value="" class="form-control" />
                                                </fieldset>

                                                <fieldset class="form-group">
                                                    <label>Card CVV *</label>
                                                    <input type="text" name="payment[<?php echo $payment['id']; ?>][cvv]" value="" class="form-control" />
                                                </fieldset>

                                                <fieldset class="cc_icons">
                                                    <img src="/extensions/<?php echo $_uccms_ecomm->Extension; ?>/images/credit-card-icons.png" alt="" />
                                                </fieldset>

                                            </div>

                                        <?php } ?>

                                    </div>
                                </div>

                                <?php

                            }

                            ?>

                        </div>

                        <div class="clearfix">

                            <div class="amount">

                                <h3 class="h5">Amount</h3>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input type="text" name="payment[amount]" value="<?php echo number_format($_uccms_ecomm->orderTotal($order), 2, '.', ''); ?>" placeholder="0.00" class="form-control" />
                                </div>

                            </div>

                            <div class="discount">

                                <h3 class="h5">Discount</h3>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <select name="discount[type]" class="form-control">
                                            <option value="amount">$</option>
                                            <option value="coupon" <?php if ($coupon['code']) { ?>selected="selected"<?php } ?>>Coupon</option>
                                        </select>
                                    </div>
                                    <input type="text" name="discount[amount]" value="<?php echo $order['order_discount']; ?>" placeholder="0.00" class="form-control" style="<?php if ($coupon['code']) { ?>display: none;<?php } ?>" />
                                    <input type="text" name="discount[coupon]" value="<?php echo $coupon['code']; ?>" placeholder="Coupon code" class="form-control" style="<?php if (!$coupon['code']) { ?>display: none;<?php } ?>" />
                                    <a href="#">Apply</a>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="submit">
                        <a href="#" class="btn btn-success">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <span class="ld ld-ring ld-spin"></span>
                            <span class="title">Complete Order</span>
                        </a>
                    </div>

                    <div class="submit_success">

                        <i class="fa fa-check-circle" aria-hidden="true"></i>

                        <h2>Success!</h2>

                        <h4>Order ID: <?php echo $cart['id']; ?></h4>

                        <ul>
                            <li><a href="/admin/<?php echo $_uccms_ecomm->Extension; ?>*ecommerce/orders/print/general/order-info?id=<?php echo $cart['id']; ?>" target="_blank">Print receipt</a></li>
                            <li><a href="<?php echo $edit_url; ?>">View order</a></li>
                            <li><a href="#" class="new">Start a new order</a></li>
                        </ul>

                    </div>

                </div>

            </div>

        </div>

        </form>

        <?php

    }

}

?>