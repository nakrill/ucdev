<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        // CLEAN UP
        $code = sqlescape(trim(strtolower($_REQUEST['value'])));

        // HAVE CODE
        if ($code) {

            // LOOK FOR MATCH
            $coupon_query = "SELECT * FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (LOWER(`code`)='" .$code. "')";
            $coupon_q = sqlquery($coupon_query);
            $coupon = sqlfetch($coupon_q);

            // COUPON FOUND
            if ($coupon['id']) {

                // ADD COUPON TO CART
                $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `coupon_id`=" .$coupon['id']. " WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
                if (sqlquery($cart_query)) {
                    $out['success'] = 'Coupon applied!';
                    $cart['coupon_id'] = $coupon['id'];
                } else {
                    $out['error'] = 'Failed to apply coupon.';
                }

            // COUPON NOT FOUND
            } else {
                $out['error'] = 'Coupon not found.';
            }

        // NO CODE
        } else {

            // UPDATE CART
            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `coupon_id`=0 WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
            sqlquery($cart_query);

            $out['success'] = 'Coupon removed.';

            $cart['coupon_id'] = 0;

        }

        // CALCULATE DISCOUNT
        $discount = $_uccms_ecomm->cartDiscount($cart, $_uccms_ecomm->cartItems($_uccms_ecomm->cartID()));

        // HAVE ERROR
        if ($discount['coupon']['error']) {
            unset($out['success']);
            $out['error'] = $discount['coupon']['error'];

        } else {

            // UPDATE CART
            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `order_discount`=" .(float)$discount['discount']. " WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
            sqlquery($cart_query);

        }

        // RECALCULATE ORDER
        $_uccms_ecomm->orderRecalculate($_uccms_ecomm->cartID());

    }

}

echo json_encode($out);

?>