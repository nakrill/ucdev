<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        $out['gratuity'] = 0.00;

        // GRATUITY ENABLED
        if ($_uccms_ecomm->getSetting('gratuity_enabled')) {

            // GET CART ITEMS
            $citems = $_uccms_ecomm->cartItems($cart['id']);

            $tax_vars = array(
                'country' => 'United States',
            );

            // TAX BILLING ADDRESS OR SHIPPING SAME AS BILLING
            if (($_uccms_ecomm->getSetting('tax_billing')) || ($cart['shipping_same'])) {

                $tax_vars['state'] = $cart['billing_state'];
                $tax_vars['zip']   = $cart['billing_zip'];

            // TAX SHIPPING
            } else {

                $tax_vars['state'] = $cart['shipping_state'];
                $tax_vars['zip']   = $cart['shipping_zip'];


            }

            if ($_REQUEST['quote']) $tax_vars['quote'] = true;

            // GET TAX
            $tax = $_uccms_ecomm->cartTax($cart['id'], $citems, $tax_vars);

            // SET TAX
            $cart['tax'] = $tax['total'];

            // SHIPPING VARS
            $shipvars = array(
                'country' => 'United States',
            );

            // SHIPPING SAME AS BILLING
            if ($cart['shipping_same']) {

                $ship_vars['state'] = $cart['billing_state'];
                $ship_vars['zip'] = $cart['billing_zip'];

            // SHIPPING DIFFERENT
            } else {

                $ship_vars['state'] = $cart['shipping_state'];
                $ship_vars['zip'] = $cart['shipping_zip'];

            }

            // GET SHIPPING SERVICES
            $shipserva = $_uccms_ecomm->cartShippingServices($cart['id'], $citems, $ship_vars);

            // GET SHIPPING
            $shipping = $_uccms_ecomm->cartShipping($shipserva, $cart['shipping_service']);

            // ORDER SHIPPING
            if ($shipping['markup_type'] == 'percent') {
                $cart['shipping'] = number_format($cart['order_subtotal'] * ($shipping['markup'] / 100), 2, '.', '');
            } else {
                $cart['shipping']  = $shipping['markup'];
            }

            /*
            $out['debug'] = array(
                'tax' => $cart['tax'],
                'shipping' => $cart['shipping'],
                'subtotal' => $cart['subtotal'],
                'order_subtotal' => $cart['order_subtotal']
            );
            */

            // CALCULATE GRATUITY
            $out['gratuity'] = $_uccms_ecomm->orderGratuity($cart);

        }

    // NO CART ID
    } else {
        $out['error'] = 'Cart not found.';
    }

}

echo json_encode($out);

?>