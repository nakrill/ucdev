<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        // HAVE FIELDS
        if ((is_array($_POST['order'])) || (is_array($_POST['billing'])) || (is_array($_POST['shipping']))) {

            // ORDER FIELDS
            $columns = array(
                'billing_firstname'     => $_POST['billing']['firstname'],
                'billing_lastname'      => $_POST['billing']['lastname'],
                'billing_company'       => $_POST['billing']['company'],
                'billing_address1'      => $_POST['billing']['address1'],
                'billing_address2'      => $_POST['billing']['address2'],
                'billing_city'          => $_POST['billing']['city'],
                'billing_state'         => $_POST['billing']['state'],
                'billing_zip'           => $_POST['billing']['zip'],
                'billing_country'       => '',
                'billing_phone'         => $_POST['billing']['phone'],
                'billing_email'         => $_POST['billing']['email'],
                'shipping_same'         => (int)$_POST['order']['shipping_same'],
                'shipping_firstname'    => $_POST['shipping']['firstname'],
                'shipping_lastname'     => $_POST['shipping']['lastname'],
                'shipping_company'      => $_POST['shipping']['company'],
                'shipping_address1'     => $_POST['shipping']['address1'],
                'shipping_address2'     => $_POST['shipping']['address2'],
                'shipping_city'         => $_POST['shipping']['city'],
                'shipping_state'        => $_POST['shipping']['state'],
                'shipping_zip'          => $_POST['shipping']['zip'],
                'shipping_country'      => '',
                'shipping_phone'        => $_POST['shipping']['phone'],
                'shipping_email'        => $_POST['shipping']['email'],
                'shipping_service'      => $_POST['order']['shipping_service'],
                'notes'                 => $_POST['order']['notes'],
            );

            $sql = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$cart['id']. ")";
            if (sqlquery($sql)) {
                $out['success'] = true;
            } else {
                $out['error'] = 'Failed to update field.';
            }

        }

    }

}

echo json_encode($out);

?>