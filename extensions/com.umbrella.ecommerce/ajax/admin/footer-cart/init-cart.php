<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAR CART ID
    $_uccms_ecomm->clearCartID(true);

    // INIT CART
    $cart = $_uccms_ecomm->initCart(true, 0, true);

    // HAVE ID
    if ($cart['id']) {

        $out['id'] = $cart['id'];

        // UPDATE ORDER
        $order_update = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `created_by`=" .$admin->ID. " WHERE (`id`=" .$cart['id']. ")";
        sqlquery($order_update);

    // FAILED TO CREATE CART
    } else {
        $out['error'] = 'Failed to create cart.';
    }

// NO ACCESS
} else {
    $out['error'] = 'Permission denied.';
}

echo json_encode($out);

?>