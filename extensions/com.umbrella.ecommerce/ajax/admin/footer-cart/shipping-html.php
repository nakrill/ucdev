<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        // GET GLOBAL FREE SHIPPING SETTING
        $global_free_shipping = $_uccms_ecomm->getSetting('shipping_free_global');

        // GLOBAL FREE SHIPPING
        if ($global_free_shipping) {

            ?>

            Free

            <?

        // NOT GLOBAL FREE SHIPPING
        } else {

            // GET SHIPPING SERVICES
            $shipserva = $_uccms_ecomm->cartShippingServices($cart['id'], array(), $_REQUEST);

            // HAVE SHIPPING SERVICES
            if (count($shipserva) > 0) {

                ?>

                <ul class="services">
                    <?php

                    // LOOP THROUGH SERVICES
                    foreach ($shipserva as $service) {

                        // SERVICE ID
                        $service_id = $service['id'];
                        if ($service['table']) $service_id = 'table-' .$service_id;

                        // SELECTED OR ONLY ONE
                        if (($service_id == $_REQUEST['selected']) || (count($shipserva) == 1)) {
                            $service_selected = true;
                        } else {
                            $service_selected = false;
                        }

                        // SERVICE MARKUP - PERCENT
                        if ($service['markup_type'] == 'percent') {

                            // GET ORDER TOTAL
                            $order_totals = $_uccms_ecomm->orderTotals($cart['id']);

                            $service_markup = number_format((($service['markup'] / 100) * $order_totals['subtotal']) + $shipping_info['custom_cost'], 2);

                        // SERVICE MARKUP - AMOUNT
                        } else {
                            $service_markup = number_format((float)$service['markup'] + (float)$shipping_info['custom_cost'], 2);
                        }

                        ?>

                        <li>
                            <input type="radio" name="order[shipping_service]" value="<?php echo $service_id; ?>" data-price="<?php echo $service_markup; ?>" <?php if ($service_selected) { ?>checked="checked"<?php } ?> />&nbsp;<?php if ($service['method']) { echo stripslashes($service['method']). ' - '; } echo stripslashes($service['title']); if ($service_markup > 0.00) { ?> - $<?php echo number_format($service_markup, 2); } ?>
                            <?php if ($service['description']) { ?>
                                <span class="note"><?php echo stripslashes($service['description']); ?></span>
                            <?php } ?>
                        </li>

                        <?php

                    }

                    ?>

                </ul>

                <script type="text/javascript">
                    $(document).ready(function() {

                        // SHIPPING METHOD SELECT
                        $('#admin_footer-cart > .checkout .checkout_columns .column.payment .shipping .shipping_method .services input').click(function() {
                            $('#admin_footer-cart > .checkout .checkout_columns .column.cart .cart_full .sub.shipping .num').text($(this).attr('data-price'));
                            afc_checkout_autoSave({}, function() {
                                afc_checkout_updateCartTotal();
                            });
                        });

                        // OPTION SELECTED - CLICK IT
                        $('#admin_footer-cart > .checkout .checkout_columns .column.payment .shipping .shipping_method .services input:checked').click();

                    });

                </script>

                <?php

            // NO SHIPPING SERVICES
            } else {

                if (($_REQUEST['state']) && ($_REQUEST['zip'])) {
                    ?>
                    No shipping services configured / available.
                    <?php
                } else {
                    ?>
                    Please enter the shipping state and zip for shipping options.
                    <?php
                }

            }

        }

    }

}

?>