<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        // GET CART ITEMS
        $citems = $_uccms_ecomm->cartItems($cart['id'], false, "`id` ASC");

        // EDIT URL
        $edit_url = '/admin/' .$_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$cart['id'];

        ?>

        <script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/selectivity-full.min.js"></script>
        <link rel="stylesheet" href="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/css/lib/selectivity-full.css" type="text/css" />

        <style type="text/css">

            .noselect {
              -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                 -khtml-user-select: none; /* Konqueror HTML */
                   -moz-user-select: none; /* Firefox */
                    -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                                              supported by Chrome and Opera */
            }

            #admin_footer-cart .cart_container .items_container {
                float: left;
            }
            #admin_footer-cart .cart_container .items_container .items {
                float: left;
                margin: 0 0 0 20px;
            }
            #admin_footer-cart .cart_container .items_container .item {
                float: left;
                height: 80px;
                margin: 0px;
                padding: 10px;
                border-right: 1px solid #ddd;
            }
            #admin_footer-cart .cart_container .items_container .item .thumb {
                position: relative;
                width: 80px;
                height: 80px;
                background-position: center center;
                background-size: cover;
                background-repeat: no-repeat;
                /*cursor: pointer;*/
            }
            #admin_footer-cart .cart_container .items_container .item .thumb .quantity {
                position: absolute;
                bottom: 5px;
                right: 5px;
                font-size: 16px;
                font-weight: bold;
                color: #fff;
                text-shadow: 1px 1px 2px #000;
            }
            #admin_footer-cart .cart_container .items_container .item .remove {
                display: none;
                position: absolute;
                top: 5px;
                right: 5px;
                font-size: 16px;
                color: red;
                text-shadow: 1px 1px 2px #000;
            }
            /*
            #admin_footer-cart .cart_container .items_container .item:hover .remove {
                display: block;
            }
            */

            #admin_footer-cart .cart_container .items_container .add_item {
                float: left;
                margin: 0 0 0 30px;
            }
            #admin_footer-cart .cart_container .items_container .add_item a {
                display: block;
                height: 50px;
                margin-top: 25px;
                padding: 0 15px;
                background-color: #54BDCC;
                text-align: center;
                line-height: 50px;
                color: #fff;
                border-radius: 50px;
            }
            #admin_footer-cart .cart_container .items_container .add_item a:hover {
                background-color: #666;
            }
            #admin_footer-cart .cart_container .items_container .add_item a i {
                float: left;
                font-size: 2em;
                line-height: 50px;
            }
            #admin_footer-cart .cart_container .items_container .add_item a .title {
                display: none;
            }
            #admin_footer-cart .cart_container .items_container .add_item a:hover .title {
                display: inline-block;
                float: left;
                padding-left: 10px;
                font-size: 16px;
            }

            #admin_footer-cart .cart_container .right_section {
                float: right;
                margin-right: 30px;
            }

            #admin_footer-cart .cart_container .right_section .order-details {
                position: relative;
                display: flex;
                align-items: center;
                float: left;
                height: 100px;
                margin-right: 20px;
                padding-right: 20px;
                border-right: 1px solid #ddd;
                font-size: .9em;
                opacity: .9;
            }
            #admin_footer-cart .cart_container .right_section .order-details .customer {
                float: left;
                margin-right: 20px;
                line-height: 1.3em;
            }
            #admin_footer-cart .cart_container .right_section .order-details .customer .name {
                font-weight: bold;
            }
            #admin_footer-cart .cart_container .right_section .order-details .general {
                float: right;
                text-align: center;
            }
            #admin_footer-cart .cart_container .right_section .order-details .general .id {
                font-weight: bold;
                text-transform: uppercase;
                color: rgba(0, 0, 0, .5);
            }
            #admin_footer-cart .cart_container .right_section .order-details .general .id .num {
                display: block;
                font-size: 2em;
                line-height: 1em;
            }
            #admin_footer-cart .cart_container .right_section .order-details .general .status {
                margin-top: 2px;
            }

            #admin_footer-cart .cart_container .right_section .total {
                float: left;
                line-height: 100px;
                font-size: 1.8em;
            }

            #admin_footer-cart .cart_container .right_section .actions {
                float: right;
                margin: 32px 0 0 30px;
            }
            #admin_footer-cart .cart_container .right_section .actions .btn {
                color: #fff;
            }
            #admin_footer-cart .cart_container .right_section .actions .btn i {
                padding-left: 5px;
            }
            #admin_footer-cart .cart_container .right_section .actions .options {
                display: none;
            }

            /* TOOLTIP - CART ITEM HOVER */

            .tooltipster_afc-item .tooltipster-content {
                position: relative;
                overflow: hidden;
                padding: 0;
                line-height: 1.4em;
            }

            .tooltipster_afc-item .tooltipster-content .quick_view {
                /*position: relative;
                z-index: 51;
                float: left;*/
                min-width: 200px;
                padding: 9px 0 1px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section {
                margin-bottom: 15px;
                padding: 0 10px 15px;
                border-bottom: 1px solid #ddd;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section .label {
                float: left;
                width: 40%;
                font-weight: bold;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section .value {
                float: left;
                width: 60%;
            }

            .tooltipster_afc-item .tooltipster-content .quick_view .item_title {
                margin-top: 7px;
                font-size: 1.2em;
                font-weight: bold;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .booking {
                margin: 2px 0 4px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .booking .title {
                font-weight: bold;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .booking .markup {
                display: none;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .options {
                margin: 5px 0 0 0;
                padding: 0 0 0 30px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .options li {
                font-size: 1em;
                line-height: 1.5em;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person {
                margin: 5px 0 2px 0;
                font-size: .8em;
                line-height: 1.2em;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person:last-child {
                margin-bottom: 0px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person .label, .tooltipster_afc-item .tooltipster-content .quick_view .persons .person .input {
                float: left;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person .label {
                padding-right: 5px;
                opacity: .5;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person .input {
                padding-right: 5px;
                opacity: .7;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person .input.dob {
                padding-left: 5px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person .input.email {
                padding-left: 5px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .persons .person .input.notes {
                padding-left: 5px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .backordered {
                font-size: .8em;
                color: #E74541;
            }

            .tooltipster_afc-item .tooltipster-content .quick_view .section.quantity .label {
                padding-top: 8px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.quantity .value input {
                width: 40px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions {
                margin: -15px 0 0;
                padding: 0 1px;
                border: 0px none;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions a {
                display: block;
                float: left;
                width: 50%;
                padding: 7px 0;
                text-align: center;
                font-size: 1.2em;
                color: #000;
                opacity: .7;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions a:hover {
                color: #333;
                opacity: 1;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions a i {
                padding-right: 3px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions a.edit {
                background-color: #FFCC00;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions a.remove {
                background-color: #C0362F;
                color: #fff;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions a.remove:hover {
                color: #fff;
            }

            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions_confirm-remove {
                display: none;
                margin: -15px 0 0;
                padding: 8px;
                border-bottom: 0px;
                background-color: rgba(0, 0, 0, .1);
                font-size: 1.2em;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions_confirm-remove a {
                display: inline-block;
                padding: 0 3px;
            }
            .tooltipster_afc-item .tooltipster-content .quick_view .section.actions_confirm-remove a.yes {
                color: #C0362F;
            }

            .tooltipster_afc-item .tooltipster-content .edit_item {
                display: none;
                position: relative;
                z-index: 50;
                float: left;
            }

            .selectivity-result-item img, .selectivity-single-selected-item img {
                height: 48px;
                widht: auto;
            }
            .selectivity-single-select input[type="text"] {
                height: auto;
            }

            form.bigtree_dialog_form .overflow .loading {
                padding-top: 15px;
                text-align: center;
            }
            form.bigtree_dialog_form .overflow .data {
                padding-top: 15px;
            }

            /* TOOLTIP - CART ACTIONS */

            .tooltipster_afc-actions h3 {
                text-align: center;
                border-bottom: 1px solid #ccc;
                color: #666;
            }

        </style>

        <script type="text/javascript">
            $(document).ready(function() {

                // UPDATE "SHOW" TAB COUNT
                $('#admin_footer-cart > .show-afc .num').text('<?php echo (int)count($citems); ?>');

                // CART - ADD ITEM
                $('#admin_footer-cart .cart_container .items_container .add_item a').click(function(e) {
                    e.preventDefault();

                    // DIALOG
                    BigTreeDialog({
                        title: 'Add Item',
                        content: $('#afc_btd_addItem').html(),
                        //preSubmissionCallback: true,
                        callback: function(params) {
                            if ((params.order_id) && (params.item_id)) {
                                $.post('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/items/add-to-order/', $('form.bigtree_dialog_form').serialize(), function(data) {
                                    if (data['success']) {
                                        if ((typeof _rst == 'object') && (data['item']['id'])) {
                                            _rst.push(['event', 'cart-add', data['item']['title']+ ' (#' +data['item']['id']+ ')']);
                                        }

                                        // start new order - go to order page

                                        if (window.location.pathname == '/admin/com.umbrella.ecommerce*ecommerce/orders/edit/') {
                                            setTimeout(function() {
                                                location.reload();
                                            }, 2000);
                                        } else {
                                            afc_getCart();
                                            afc_getCheckout();
                                        }
                                    } else {
                                        if (data['err']) {
                                            if (typeof _rst == 'object') {
                                                _rst.push(['event', 'cart-add-error', data['err']]);
                                            }
                                            alert(data['err']);
                                        } else {
                                            alert('Adding failed.');
                                        }
                                    }
                                }, 'json');
                            } else {
                                alert('Could not submit data.');
                            }
                        },
                        icon: 'add'
                    });

                    // GET ITEMS
                    $.get('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/items/for-dropdown/', function(data) {

                        // SELECT PRODUCT
                        $('.bigtree_dialog_window .select_product').selectivity({
                            allowClear: true,
                            items: data,
                            placeholder: 'Select item',
                            templates: {
                                resultItem: function(item) {
                                    return (
                                    '<div class="selectivity-result-item" data-item-id="' +item.id+ '">' +
                                        '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                                    '</div>'
                                    );
                                },
                                singleSelectedItem: function(item) {
                                    return (
                                        '<span class="selectivity-single-selected-item" data-item-id="' +item.id+ '">' +
                                            '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                                        '</span>'
                                    );
                                }
                            }
                        }).on('selectivity-selected', function(e) {
                            $('.selectivity-single-select').height(57);
                            $('form.bigtree_dialog_form .overflow .data').html('').hide();
                            $('form.bigtree_dialog_form .overflow .loading').show();
                            $.get('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/items/add_interface/', {
                                    order_id: <?php echo $cart['id']; ?>,
                                    item_id: e.id
                                },
                                function(data) {
                                    $('form.bigtree_dialog_form .overflow .loading').hide();
                                    $('form.bigtree_dialog_form .overflow .data').html(data).show();
                                },
                                'html'
                            );
                        });

                    }, 'json');

                });

                // ITEMS
                $('#admin_footer-cart .cart_container .items_container .item').each(function(i, el) {

                    var oitem_id = $(el).attr('data-oitem-id');

                    // TOOLTIP
                    $(el).find('.thumb').tooltipster({
                        trigger: (window.mobileAndTabletcheck() ? 'click' : 'hover'),
                        content: $('#afc_item-' +oitem_id+ '-tooltip').html(),
                        contentAsHTML: true,
                        interactive: true,
                        delayTouch: [300, 2000],
                        theme: ['tooltipster-light', 'tooltipster_afc-item'],
                        side: ['top', 'right', 'bottom', 'left'],
                        functionReady: function(instance, helper) {

                            // QUANTITY - CHANGE
                            $('.tooltipster_afc-item .tooltipster-content .quick_view .section.quantity input').change(function(e) {
                                $.post('<?php echo ADMIN_ROOT; ?>*/<?php echo $_uccms_ecomm->Extension; ?>/ajax/admin/items/change-order-quantity/', {
                                    order_id: <?php echo $cart['id']; ?>,
                                    id: oitem_id,
                                    quantity: $(this).val()
                                }, function(data) {
                                    if (data.success) {
                                        afc_getCart();
                                        afc_getCheckout();
                                    } else {
                                        if (data['err']) {
                                            if (typeof _rst == 'object') {
                                                _rst.push(['event', 'cart-quantity-error', data['err']]);
                                            }
                                            alert(data['err']);
                                        } else {
                                            alert('Failed to change quantity.');
                                        }
                                    }
                                }, 'json');
                            });

                            // REMOVE - CLICK
                            $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions a.remove').click(function(e) {
                                e.preventDefault();
                                $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions').hide();
                                $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions_confirm-remove').show();
                            });

                            // REMOVE - CONFIRM - YES
                            $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions_confirm-remove .yes').click(function(e) {
                                e.preventDefault();
                                $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/remove-from-order/', {
                                    id: oitem_id,
                                    order_id: <?php echo $cart['id']; ?>
                                }, function(data) {
                                    if ((typeof _rst == 'object') && (data['item']['id'])) {
                                        _rst.push(['event', 'cart-remove', data['item']['title']+ ' (#' +data['item']['id']+ ')']);
                                    }
                                    $('#afc_item-' +oitem_id).fadeOut(500, function() {
                                        if (window.location.pathname == '/admin/com.umbrella.ecommerce*ecommerce/orders/edit/') {
                                            setTimeout(function() {
                                                location.reload();
                                            }, 2000);
                                        } else {
                                            afc_getCart();
                                            afc_getCheckout();
                                        }
                                    });
                                }, 'json');
                            });

                            // REMOVE - CONFIRM - NO
                            $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions_confirm-remove .no').click(function(e) {
                                e.preventDefault();
                                $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions_confirm-remove').hide();
                                $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions').show();
                            });

                            // EDIT - CLICK
                            $('.tooltipster_afc-item .tooltipster-content .quick_view .section.actions a.edit').click(function(e) {
                                e.preventDefault();

                                $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add_interface/', {
                                        order_id: <?php echo $cart['id']; ?>,
                                        order_item_id: oitem_id
                                    },
                                    function(data) {
                                        BigTreeDialog({
                                            title: 'Edit Item',
                                            content: '<div class="data">' +data+ '</div>',
                                            //preSubmissionCallback: true,
                                            callback: function(params) {
                                                if ((params.order_id) && (params.item_id)) {
                                                    $.post('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/add-to-order/', $('form.bigtree_dialog_form').serialize(), function(data2) {
                                                        if (data2['success']) {
                                                            if ((typeof _rst == 'object') && (data2['item']['id'])) {
                                                                _rst.push(['event', 'cart-update', data2['item']['title']+ ' (#' +data2['item']['id']+ ')']);
                                                            }
                                                            if (window.location.pathname == '/admin/com.umbrella.ecommerce*ecommerce/orders/edit/') {
                                                                setTimeout(function() {
                                                                    location.reload();
                                                                }, 2000);
                                                            } else {
                                                                afc_getCart();
                                                                afc_getCheckout();
                                                            }
                                                        } else {
                                                            if (data2['err']) {
                                                                if (typeof _rst == 'object') {
                                                                    _rst.push(['event', 'cart-update-error', data['err']]);
                                                                }
                                                                alert(data2['err']);
                                                            } else {
                                                                alert('Updating failed.');
                                                            }
                                                        }
                                                    }, 'json');
                                                } else {
                                                    alert('Could not submit data.');
                                                }
                                            },
                                            icon: 'edit'
                                        });
                                    },
                                    'html'
                                );

                            });

                        },
                    });

                });

                // CHECKOUT BUTTON - TOOLTIP
                $('#admin_footer-cart .cart_container .right_section .actions .btn').tooltipster({
                    delay: 500,
                    content: $('#admin_footer-cart .cart_container .right_section .actions .options').html(),
                    contentAsHTML: true,
                    interactive: true,
                    theme: ['tooltipster-light', 'tooltipster_afc-actions'],
                    side: ['top', 'left', 'bottom', 'right'],
                    functionReady: function(instance, helper) {

                        // START A NEW ORDER
                        $('.tooltipster_afc-actions .new-order').click(function(e) {
                            e.preventDefault();
                            if (confirm('Leave this cart and start a new one?')) {
                                if (window.location.pathname == '/admin/com.umbrella.ecommerce*ecommerce/orders/edit/') {
                                    afc_startCart({}, function(data) {
                                        window.location = './?id=' +data.id;
                                    });
                                } else {
                                    if ($('#admin_footer-cart > .checkout').is(':visible')) {
                                        $('#admin_footer-cart .cart_container .right_section .actions .btn').click();
                                    }
                                    afc_startCart();
                                }
                            }
                        });

                    },
                });

                // CHECKOUT BUTTON - CLICK
                $('#admin_footer-cart .cart_container .right_section .actions .btn').click(function(e) {
                    e.preventDefault();
                    afc_checkoutToggle();
                });

            });

            window.mobileAndTabletcheck = function() {
                var check = false;
                (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
                return check;
            };

        </script>

        <div id="afc_btd_addItem" style="display: none;">
            <div class="select_product"></div>
            <div class="loading" style="display: none;">Loading..</div>
            <div class="data" style="display: none;"></div>
        </div>

        <div class="items_container clearfix">

            <?php

            // HAVE ITEMS IN CART
            if (count($citems) > 0) {

                ?>

                <div class="items clearfix">

                    <?php

                    // SUBTOTAL
                    $cart['subtotal'] = 0;

                    // ATTRIBUTE ARRAY
                    $attra = array();

                    // AFTER TAX ITEM ARRAY
                    $atia = array();

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        //echo print_r($item);
                        //echo print_r($oitem);

                        $classa = array('item');
                        if ($item['quote']) $classa[] = 'quote';
                        if ($item['type'] == 3) $classa[] = 'package';
                        if ($item_cart_settings['after_tax']) $classa[] = 'aftertax';
                        $item_classes = implode(' ', $classa);

                        $attr_options = array(
                            'file_download' => true,
                            'order_id' => $cart['id']
                        );

                        ?>

                        <div id="afc_item-<?php echo $oitem['id']; ?>" class="<?php echo $item_classes; ?>" data-oitem-id="<?php echo $oitem['id']; ?>">
                            <div class="thumb" style="background-image: url('<?php echo $item['other']['image_url']; ?>');" alt="<?php echo stripslashes($item['title']); ?>">
                                <div class="quantity noselect" title="Quantity: <?php echo number_format($oitem['quantity'], 0); ?>">x<?php echo number_format($oitem['quantity'], 0); ?></div>
                                <a href="#" class="remove" title="Remove from cart"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </div>
                        </div>

                        <div id="afc_item-<?php echo $oitem['id']; ?>-tooltip" style="display: none;">
                            <div class="clearfix">

                                <div class="quick_view">

                                    <div class="section title clearfix">
                                        <div class="item_title"><?php if ($item_cart_settings['edit_link']) { ?><a href="<?php echo $item_cart_settings['edit_link']; ?>"><?php } ?><?php echo stripslashes($item['title']); ?><?php if ($item_cart_settings['edit_link']) { ?></a><?php } ?></div>
                                        <?php if ($booking['id']) { ?>
                                            <div class="booking">
                                                <?php if ($booking_duration['title']) { ?>
                                                    <span class="title"><?php echo stripslashes($booking_duration['title']); ?>:</span>
                                                <?php } ?>
                                                <span class="date">
                                                    <?php
                                                    $from_to = '';
                                                    if (($booking['dt_from']) && ($booking['dt_from'] != '0000-00-00 00:00:00')) {
                                                        list($booking_from_date, $booking_from_time) = explode(' ', $booking['dt_from']);
                                                        $from_to .= '
                                                        <span class="from">
                                                            <span class="date">' .date('n/j/Y', strtotime($booking_from_date)). '</span>
                                                            ';
                                                            if ($booking_from_time != '00:00:00') {
                                                                $from_to .= '<span class="time">' .date('g:i A', strtotime($booking_from_time)). '</span>';
                                                            }
                                                            $from_to .='
                                                        </span>
                                                        ';
                                                        if (($booking['dt_to']) && ($booking['dt_to'] != '0000-00-00 00:00:00')) {
                                                            list($booking_to_date, $booking_to_time) = explode(' ', $booking['dt_to']);
                                                            $from_to .= ' to
                                                            <span class="to">';
                                                                if ($booking_from_date != $booking_to_date) {
                                                                    $from_to .= '<span class="date">' .date('n/j/Y', strtotime($booking_to_date)). '</span>';
                                                                }
                                                                if ($booking_from_time != '00:00:00') {
                                                                    $from_to .= '
                                                                    <span class="time">' .date('g:i A', strtotime($booking_to_time) + 1). '</span>';
                                                                }
                                                                $from_to .='
                                                            </span>
                                                            ';
                                                        }
                                                    }
                                                    echo $from_to;
                                                    ?>
                                                </span>
                                                <?php if ($booking['markup'] != 0.00) { ?>
                                                    <span class="markup">(+ $<?php echo $_uccms_ecomm->toFloat($booking['markup']); ?>)</span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ((count((array)$item['other']['options']) > 0) || (($item['type'] == 3) && (count((array)$item['package']['items']) > 0))) { ?>
                                            <ul class="options">
                                                <?php

                                                // IS PACKAGE AND HAVE ITEMS
                                                if (($item['type'] == 3) && (count($item['package']['items']) > 0)) {

                                                    // LOOP THROUGH ITEMS
                                                    foreach ($item['package']['items'] as $pitem_id => $pitem) {

                                                        // GET SELECTED ATTRIBUTES / OPTIONS
                                                        $pitem_attrs = $_uccms_ecomm->item_attrAndOptionTitles($pitem, $pitem['other']['attribute_titles'], $attr_options);

                                                        ?>

                                                        <li><?php echo stripslashes($pitem['item']['title']); if ($pitem['quantity'] > 1) { echo ' (' .number_format($pitem['quantity'], 0). ')'; } ?>
                                                            <?php if (count($pitem_attrs) > 0) { ?>
                                                                <ul>
                                                                    <?php foreach ($pitem_attrs as $attr) { ?>
                                                                        <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } ?>
                                                        </li>

                                                        <?php

                                                    }

                                                // OTHER TYPES
                                                } else {
                                                    if (count($item['other']['options']) > 0) {
                                                        foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra, $attr_options) as $attr) {
                                                            ?>
                                                            <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>
                                            </ul>
                                        <?php } ?>
                                        <?php
                                        if ($booking['id']) {

                                            $persons = array();

                                            // GET PERSONS INFO FOR THIS CART ITEM
                                            $persons_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` WHERE (`oitem_id`=" .$oitem['id']. ") ORDER BY `sort` ASC, `id` ASC";
                                            $persons_q = sqlquery($persons_query);

                                            // HAVE PERSONS
                                            if (sqlrows($persons_q) > 0) {
                                                $pi = 1;

                                                ?>
                                                <div class="persons">
                                                    <?php
                                                    while ($person = sqlfetch($persons_q)) {
                                                        ?>
                                                        <div class="person clearfix">
                                                            <div class="label">
                                                                Person <?php echo $pi; ?>:
                                                            </div>
                                                            <div class="input firstname">
                                                                <?php echo stripslashes($person['firstname']); ?>
                                                            </div>
                                                            <div class="input lastname">
                                                                <?php echo stripslashes($person['lastname']); ?>
                                                            </div>
                                                            <div class="input dob">
                                                                <?php if (($person['dob']) && ($person['dob'] != '0000-00-00')) { echo date('n/j/Y', strtotime($person['dob'])); } ?>
                                                            </div>
                                                            <div class="input email">
                                                                <?php echo stripslashes($person['email']); ?>
                                                            </div>
                                                            <div class="input notes">
                                                                <?php echo stripslashes($person['notes']); ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $pi++;
                                                    }
                                                    ?>
                                                </div>
                                                <?php

                                            }

                                        }
                                        ?>
                                        <?php if ($oitem['backorder_num']) { ?>
                                            <div class="backordered">
                                                Backordered: <?php echo number_format($oitem['backorder_num']); ?>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="section price clearfix">
                                        <div class="label">Price</div>
                                        <div class="value"><?php echo $item['other']['price_formatted']; ?></div>
                                    </div>

                                    <div class="section quantity clearfix">
                                        <div class="label">Quantity</div>
                                        <div class="value">
                                            <input type="text" name="quantity" value="<?php echo number_format($oitem['quantity'], 0); ?>" />
                                        </div>
                                    </div>

                                    <div class="section total clearfix" style="position: relative;">
                                        <div class="label">Total</div>
                                        <div class="value"><?php echo $item['other']['total_formatted']; ?></div>
                                    </div>

                                    <div class="section actions clearfix">
                                        <a href="#" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
                                        <a href="#" class="remove"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                    </div>

                                    <div class="section actions_confirm-remove clearfix">
                                        Are you sure? <a href="#" class="yes">Yes</a> | <a href="#" class="no">No</a>
                                    </div>

                                </div>

                                <? /*
                                <div class="edit_item">
                                    Edit item
                                </div>
                                */ ?>

                            </div>

                        </div>

                        <?php

                        unset($item, $oitem);


                    }

                    ?>

                </div>

                <?php

            }

            ?>

            <div class="add_item">
                <a href="#"><i class="fa fa-plus" aria-hidden="true"></i><span class="title">Add item</span></a>
            </div>

        </div>

        <div class="right_section clearfix">

            <div class="order-details">
                <div class="customer">
                    <div class="name"><?php echo stripslashes($cart['billing_firstname']. ' ' .$cart['billing_lastname']); ?></div>
                    <div class="address">
                        <?php
                        $aparts = [
                            $cart['billing_address1'],
                            $cart['billing_address2'],
                            $cart['billing_city']. ', ' .$cart['billing_state']. ' ' .$cart['billing_zip'],
                        ];
                        echo uccms_prettyAddress($aparts);
                        ?>
                    </div>
                    <div class="phone"><?php if ($cart['billing_phone']) { echo $_uccms_ecomm->prettyPhone($cart['billing_phone']); } ?></div>
                    <div class="email"><?php echo stripslashes($cart['billing_email']); ?></div>
                </div>
                <div class="general">
                    <div class="id">Order<span class="num"><?php echo $cart['id']; ?></span></div>
                    <div class="status"><?php echo ucwords($cart['status']); ?></div>
                </div>
            </div>

            <div class="total"><span class="usd">$</span><span class="num"><?php echo number_format($_uccms_ecomm->orderTotal($cart['id']), 2); ?></span></div>

            <div class="actions" style="<?php if (count($citems) == 0) { ?>display: none;<?php } ?>">
                <a href="#" class="btn btn-success">Check Out<i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i></a>
                <div class="options">
                    <h3>Other options</h3>
                    <ul>
                        <?php if ($cart['quote'] == 1) { ?>
                            <li><a href="<?php echo $edit_url; ?>">Edit quote</a></li>
                            <li><a href="<?php echo '/admin/' .$_uccms_ecomm->Extension. '*ecommerce/quotes/edit/convert-to-order/?id=' .$cart['id']; ?>">Convert to order</a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo $edit_url; ?>">Edit order</a></li>
                            <li><a href="<?php echo '/admin/' .$_uccms_ecomm->Extension. '*ecommerce/orders/edit/convert-to-quote/?id=' .$cart['id']; ?>">Convert to quote</a></li>

                        <?php } ?>
                        <li><a href="#" class="new-order">Start a new order</a></li>
                    </ul>
                </div>
            </div>

        </div>

        <?php

    } else {

    }

}

?>