<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        $cart['coupon_id'] = 0;

        // CLEAN UP
        $amount = round((float)$_REQUEST['value'], 2);

        // ADD DISCOUNT TO CART
        $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `coupon_id`=0, `order_discount`=" .$amount. " WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
        if (sqlquery($cart_query)) {
            $out['success'] = 'Discount applied!';
        } else {
            $out['error'] = 'Failed to apply discount.';
        }

        // RECALCULATE ORDER
        $_uccms_ecomm->orderRecalculate($_uccms_ecomm->cartID());

    }

}

echo json_encode($out);

?>