<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // INIT CART
    $cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID(), true);

    // HAVE CART ID
    if ($cart['id']) {

        // HAVE FIELDS
        if ((is_array($_POST['order'])) && (is_array($_POST['billing'])) && (is_array($_POST['shipping']))) {

            $out['order'] = $cart;

            // GET CART ITEMS
            $citems = $_uccms_ecomm->cartItems($cart['id']);

            // HAVE ITEMS IN CART
            if (count($citems) > 0) {

                // REMOVE COUPON FOR NOW
                $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `coupon_id`=0 WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
                sqlquery($cart_query);
                $cart['coupon_id'] = 0;

                // DISCOUNT - COUPON
                if ($_REQUEST['discount']['type'] == 'coupon') {

                    // CLEAN UP
                    $coupon_code = sqlescape(trim(strtolower($_REQUEST['discount']['coupon'])));

                    // HAVE COUPON CODE
                    if ($coupon_code) {

                        // LOOK FOR MATCH
                        $coupon_query = "SELECT * FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (LOWER(`code`)='" .$coupon_code. "')";
                        $coupon_q = sqlquery($coupon_query);
                        $coupon = sqlfetch($coupon_q);

                        // COUPON FOUND
                        if ($coupon['id']) {

                            // ADD COUPON TO CART
                            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `coupon_id`=" .$coupon['id']. " WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
                            if (sqlquery($cart_query)) {
                                $cart['coupon_id'] = $coupon['id'];
                            }

                        }

                    }

                // DISCOUNT - AMOUNT
                } else {

                    // CLEAN UP
                    $discount_amount = round((float)$_REQUEST['discount']['amount'], 2);

                     // ADD DISCOUNT TO CART
                    $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `order_discount`=" .$discount_amount. " WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
                    sqlquery($cart_query);

                }

                // LOOP
                foreach($citems as $cid => $oitem) {

                    // GET BOOKING INFO
                    $booking_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$oitem['id']. ")";
                    $booking_q = sqlquery($booking_query);
                    $citems[$cid]['booking'] = sqlfetch($booking_q);

                }

                // INVENTORY CONTROL ENABLED
                if ($_uccms_ecomm->getSetting('inventory_track')) {

                    // LOOP
                    foreach ($citems as $oitem) {

                        $ivars['item']          = $oitem;
                        $ivars['item']['id']    = $oitem['item_id'];

                        // ITEM OPTIONS
                        if ($oitem['options']) {
                            $ivars['attribute'] = json_decode(stripslashes($oitem['options']), true);
                        }

                        // HAVE BOOKING INFO
                        if ($oitem['booking']['id']) {

                            $ivars['booking']           = $oitem['booking'];
                            $ivars['booking']['from']   = $oitem['booking']['dt_from'];
                            $ivars['booking']['to']     = $oitem['booking']['dt_to'];

                        }

                        // SEE IF STILL GOOD IN CART
                        $check = $_uccms_ecomm->cartAddItem($cart['id'], $ivars, false, true);

                        $out['inventory_check'] = $check;

                        unset($ivars);

                    }

                }

                // GET GLOBAL FREE SHIPPING SETTING
                $global_free_shipping = $_uccms_ecomm->getSetting('shipping_free_global');

                // CLEAN UP
                $shipping_service   = sqlescape(trim($_POST['order']['shipping_service']));
                $payment_method     = sqlescape(trim($_POST['payment']['method']));

                // HAVE PAYMENT METHOD
                if ($payment_method) {

                    // GET PAYMENT METHOD INFO
                    $pmethod_query = "SELECT * FROM `" .$_uccms_ecomm->tables['payment_methods']. "` WHERE (`id`='" .$payment_method. "') AND (`active`=1)";
                    $pmethod_q = sqlquery($pmethod_query);
                    $pmethod = sqlfetch($pmethod_q);

                    // NOT FOUND
                    if (!$pmethod['id']) {
                        $payment_method = '';
                    }

                }

                //if (!$payment_method) $out['error'] = 'Payment method not specified.';

                // GET TAX
                $tax = $_uccms_ecomm->cartTax($cart['id'], $citems, array(
                    'country'   => 'United States',
                    'state'     => $_POST['billing']['state'],
                    'zip'       => $_POST['billing']['zip'],
                    'quote'     => false
                ));

                // HAVE GLOBAL FREE SHIPPING
                if ($global_free_shipping) {

                    $shipping['markup'] = 0.00;

                // NO GLOBAL FREE SHIPPING
                } else {

                    // SHIPPING VARS
                    $shipvars['country'] = 'United States';
                    if ($_POST['order']['shipping_same']) { // SHIPPING SAME AS BILLING
                        $shipvars['state'] = $_POST['billing']['state'];
                        $shipvars['zip'] = $_POST['billing']['zip'];
                    } else { // SHIPPING DIFFERENT
                        $shipvars['state'] = $_POST['shipping']['state'];
                        $shipvars['zip'] = $_POST['shipping']['zip'];
                    }

                    // GET SHIPPING SERVICES
                    $shipserva = $_uccms_ecomm->cartShippingServices($cart['id'], $citems, $shipvars);

                    // GET SHIPPING
                    $shipping = $_uccms_ecomm->cartShipping($shipserva, $shipping_service);

                    // SHIPPING ERROR
                    if ($shipping['err']) {
                        $out['error'] = $shipping['err'];
                    }

                }

                // NO ERROR
                if (!$out['error']) {

                    $order = $cart;

                    // ORDER TOTAL
                    $order['total'] = 0;

                    // AMOUNT PAYING
                    $amount = 0;

                    // ITEM OWNER EMAILS
                    $io_emails = [];

                    // ORDER DATA FOR REVSOCIAL API
                    $rs_orderdata = array(
                        'billing'           => $_POST['billing'],
                        'shipping'          => $_POST['shipping']
                    );

                    // QUANTITY COUNT
                    $rs_items['num_products'] = count($citems);
                    $rs_items['num_quantity'] = 0;

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // ADD ITEM TOTAL TO ORDER TOTAL
                        $order['total'] += $item['other']['total'];

                        // TEMP EMAIL VAR
                        $tevar = '
                        <tr>
                            <td valign="top" style="padding: 8px;">
                                <img src="' .substr(WWW_ROOT, 0, -1) . $item['other']['image_url']. '" alt="' .stripslashes($item['title']). '" height="50" />
                            </td>
                            <td width="50%" style="padding: 8px;">
                                <a href="' .$item['other']['link']. '">' .stripslashes($item['title']). '</a>';
                                if ($oitem['booking']['id']) {
                                    $tevar .= '<div class="booking">';
                                    if ($booking_duration['title']) {
                                        $tevar .= '<span class="title" style="display: block;">' .stripslashes($booking_duration['title']). '</span>';
                                    }
                                    $tevar .= '<span class="date" style="font-weight: bold;">';
                                    $from_to = '';
                                    if (($oitem['booking']['dt_from']) && ($oitem['booking']['dt_from'] != '0000-00-00 00:00:00')) {
                                        list($booking_from_date, $booking_from_time) = explode(' ', $oitem['booking']['dt_from']);
                                        $from_to .= '
                                        <span class="from">
                                            <span class="date">' .date('n/j/Y', strtotime($booking_from_date)). '</span>
                                            ';
                                            if ($booking_from_time != '00:00:00') {
                                                $from_to .= '<span class="time">' .date('g:i A', strtotime($booking_from_time)). '</span>';
                                            }
                                            $from_to .='
                                        </span>
                                        ';
                                        if (($oitem['booking']['dt_to']) && ($oitem['booking']['dt_to'] != '0000-00-00 00:00:00')) {
                                            list($booking_to_date, $booking_to_time) = explode(' ', $oitem['booking']['dt_to']);
                                            $from_to .= ' to
                                            <span class="to">';
                                                if ($booking_from_date != $booking_to_date) {
                                                    $from_to .= '<span class="date">' .date('n/j/Y', strtotime($booking_to_date)). '</span>';
                                                }
                                                if ($booking_from_time != '00:00:00') {
                                                    $from_to .= '
                                                    <span class="time">' .date('g:i A', strtotime($booking_to_time) + 1). '</span>';
                                                }
                                                $from_to .='
                                            </span>
                                            ';
                                        }
                                    }
                                    $tevar .= $from_to;
                                    $tevar .= '</span>';
                                    /*
                                    if ($oitem['booking']['markup'] != 0.00) {
                                        $tevar .= '<span class="markup">(+ $' .$_uccms_ecomm->toFloat($oitem['booking']['markup']). ')</span>';
                                    }
                                    */
                                    $tevar .= '</div>';
                                }
                                if (($item['type'] == 3) && (count($item['package']['items']) > 0)) {
                                    foreach ($item['package']['items'] as $pitem_id => $pitem) {
                                        $pitem_attrs = $_uccms_ecomm->item_attrAndOptionTitles($pitem, $pitem['other']['attribute_titles'], $attr_options);
                                        $tevar .= '<li>' .stripslashes($pitem['item']['title']); if ($pitem['quantity'] > 1) { $tevar .= ' (' .number_format($pitem['quantity'], 0). ')'; }
                                        if (count($pitem_attrs) > 0) {
                                            $tevar .= '<ul>';
                                            foreach ($pitem_attrs as $attr) {
                                                $tevar .= '<li>' .$attr['title']. ': ' .$attr['options']. '</li>';
                                            }
                                            $tevar .= '</ul>';
                                        }
                                        $tevar .= '</li>';
                                    }
                                } else {
                                    if (count($item['other']['options']) > 0) {
                                        $tevar .= '<ul style="margin: 0; padding: 0 0 0 5px; list-style: outside none none; font-size: 0.8em;">';
                                            foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra) as $attr) {
                                                $tevar .= '<li>' .$attr['title']. ': ' .$attr['options']. '</li>';
                                            }
                                        $tevar .= '</ul>';
                                    }
                                }
                            $tevar .= '
                            </td>
                            <td style="padding: 8px;">
                                ' .$item['other']['price_formatted']. '
                            </td>
                            <td style="padding: 8px; text-align: right;">
                                ' .number_format($oitem['quantity'], 0). '
                            </td>
                            <td style="padding: 8px; text-align: right;">
                                ' .$item['other']['total_formatted']. '
                            </td>
                        </tr>
                        ';

                        // IS AFTER TAX ITEM
                        if ($oitem['after_tax']) {
                            $evars_items_aftertax .= $tevar;
                        } else {
                            $evars_items .= $tevar;
                        }

                        // HAS ITEM OWNER AND GETTING EMAIL
                        if (($item['account_id']) && ($item['email_account_on_order'])) {
                            if ($oitem['after_tax']) {
                                $io_emails[$item['account_id']]['items_aftertax'][] = $tevar;
                                $io_emails[$item['account_id']]['total'] += $item['other']['total'];
                            } else {
                                $io_emails[$item['account_id']]['items'][] = $tevar;
                                $io_emails[$item['account_id']]['subtotal'] += $item['other']['total'];
                                $io_emails[$item['account_id']]['total'] += $item['other']['total'];
                            }
                        }

                        // ADD TO QUANTITY COUNT
                        $rs_items['num_quantity'] += $oitem['quantity'];

                        // PRODUCT OPTIONS (ATTRIBUTES)
                        $options = array();
                        if (count($item['other']['options']) > 0) {
                            foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra) as $attr) {
                                $options[$attr['title']] = $attr['options'];
                            }
                        }

                        // ADD TO PRODUCTS ARRAY
                        $rs_items['products'][] = array(
                            'sku'       => stripslashes($item['sku']),
                            'name'      => stripslashes($item['title']),
                            'options'   => $options,
                            'quantity'  => $oitem['quantity'],
                            'total'     => $item['other']['total']
                        );

                    }

                    //$order['id'] = $cart['id'];
                    //$order['discount'] = 0.00;
                    //$order['coupon_id'] = $cart['coupon_id'];

                    // GET DISCOUNT
                    $discount = $_uccms_ecomm->cartDiscount($order, $citems);

                    // HAVE DISCOUNT
                    if ($discount['discount']) {

                        // UPDATE ORDER
                        $uorder_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `order_discount`='" .$discount['discount']. "' WHERE (`id`=" .$cart['id']. ")";
                        sqlquery($uorder_query);

                        $order['discount'] = $discount['discount'];

                        $evars_discount = '
                            <tr>
                                <td colspan="5" align="right" style="padding-right: 10px; text-align: right;">
                                    After discount ('; if ($discount['coupon']['code']) { $evars_discount .= 'Coupon: ' .$discount['coupon']['code']. ' - '; } $evars_discount .= '$' .number_format($discount['discount'], 2). '): $' .$_uccms_ecomm->orderTotal($order). '
                                </td>
                            </tr>
                        ';

                    }

                    // ORDER TAX
                    $order['tax'] = $tax['total'];

                    // ORDER SHIPPING
                    if ($shipping['markup_type'] == 'percent') {
                        $order['shipping'] = number_format($order['subtotal'] * ($shipping['markup'] / 100), 2, '.', '');
                    } else {
                        $order['shipping']  = $shipping['markup'];
                    }

                    // ORDER GRATUITY
                    $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);

                    // ORDER TOTAL
                    $order['total'] = $_uccms_ecomm->orderTotal($order);

                    // DATABASE COLUMNS
                    $columns = array(
                        'billing_firstname'     => $_POST['billing']['firstname'],
                        'billing_lastname'      => $_POST['billing']['lastname'],
                        'billing_company'       => $_POST['billing']['company'],
                        'billing_address1'      => $_POST['billing']['address1'],
                        'billing_address2'      => $_POST['billing']['address2'],
                        'billing_city'          => $_POST['billing']['city'],
                        'billing_state'         => $_POST['billing']['state'],
                        'billing_zip'           => $_POST['billing']['zip'],
                        'billing_country'       => '',
                        'billing_phone'         => $_POST['billing']['phone'],
                        'billing_email'         => $_POST['billing']['email'],
                        'shipping_same'         => (int)$_POST['order']['shipping_same'],
                        'shipping_firstname'    => $_POST['shipping']['firstname'],
                        'shipping_lastname'     => $_POST['shipping']['lastname'],
                        'shipping_company'      => $_POST['shipping']['company'],
                        'shipping_address1'     => $_POST['shipping']['address1'],
                        'shipping_address2'     => $_POST['shipping']['address2'],
                        'shipping_city'         => $_POST['shipping']['city'],
                        'shipping_state'        => $_POST['shipping']['state'],
                        'shipping_zip'          => $_POST['shipping']['zip'],
                        'shipping_country'      => '',
                        'shipping_phone'        => $_POST['shipping']['phone'],
                        'shipping_email'        => $_POST['shipping']['email'],
                        'shipping_service'      => $_POST['order']['shipping_service'],
                        'notes'                 => $_POST['order']['notes'],
                        'order_subtotal'        => $_uccms_ecomm->toFloat($order['subtotal']),
                        'order_discount'        => $_uccms_ecomm->toFloat($order['discount']),
                        'order_discount_id'     => '',
                        'order_tax'             => $_uccms_ecomm->toFloat($order['tax']),
                        'order_aftertax'        => $_uccms_ecomm->toFloat($order['aftertax']),
                        'order_shipping'        => $_uccms_ecomm->toFloat($order['shipping']),
                        'order_gratuity'        => $order['gratuity'],
                        'order_total'           => $order['total']
                    );

                    // SAVE CUSTOMER
                    $customer = $_uccms_ecomm->orderSaveCustomer(array_merge($columns, [
                        'account' => [
                            'email'     => $columns['billing_email'],
                            'firstname' => $columns['billing_firstname'],
                            'lastname'  => $columns['billing_lastname'],
                            'password'  => $_POST['account']['password'],
                        ]
                    ]));
                    $customer_id = $customer['id'];

                    // ORDER HASH
                    $order_hash = $_uccms_ecomm->generateOrderHash($customer_id);

                    $columns['hash'] = $order_hash;
                    $columns['customer_id'] = $customer_id;

                    // SHIPSTATION ENABLED
                    if ($_uccms_ecomm->getSetting('shipstation_enabled')) {
                        $columns['shipping_3rdparty'] = 'shipstation';
                        $columns['shipping_carrier'] = $shipping['carrier'];
                    }

                    // UPDATE CART
                    $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `dt`=NOW(), " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$cart['id']. ")";

                    // UPDATE SUCCESSFUL
                    if (sqlquery($cart_query)) {

                        // AMOUNT TO PAY
                        //$amount = $order['total'];
                        $amount = $_uccms_ecomm->formatCurrency($_REQUEST['payment']['amount']);

                        // NOT CREDIT CARD
                        if (in_array($payment_method, ['check_cash','donation','trade','waived'])) {

                            // UPDATE STATUS
                            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='charged' WHERE (`id`=" .$cart['id']. ")";
                            sqlquery($cart_query);

                            // LOG TRANSACTION
                            $_uccms_ecomm->logTransaction(array(
                                'cart_id'   => $cart['id'],
                                'status'    => 'charged',
                                'amount'    => $amount,
                                'method'    => $_POST['payment']['method'],
                                'note'      => $_POST['payment'][$_POST['payment']['method']]['note'],
                                'ip'        => ip2long($_SERVER['REMOTE_ADDR']),
                            ));

                        // PAYING OTHER WAY
                        } else {

                            // HAVE AMOUNT TO CHARGE
                            if ($amount > 0.00) {

                                // PAYMENT METHOD SPECIFIED
                                if ($payment_method) {

                                    // NO CREDIT CARD FIELDS
                                    if ($pmethod['hide_cc']) {

                                    // PAYMENT PROFILE SPECIFIED
                                    } else if ($_POST['payment']['profile_id']) {

                                        // GET PAYMENT PROFILE
                                        $pp = $_uccms['_account']->pp_getProfile((int)$_POST['payment']['profile_id'], $cart['customer_id']);

                                        // PAYMENT PROFILE FOUND
                                        if ($pp['id']) {

                                            $payment_method     = 'profile';
                                            $payment_name       = stripslashes($pp['name']);
                                            $payment_lastfour   = $pp['lastfour'];
                                            $payment_expiration = $pp['expires'];

                                        // PAYMENT PROFILE NOT FOUND
                                        } else {
                                            $_uccms['_site-message']->set('error', 'Payment method not found.');
                                        }

                                    // USE CREDIT CARD INFO
                                    } else {

                                        // REQUIRE PAYMENT FIELDS
                                        $reqfa = array(
                                            'name'          => 'Name On Card',
                                            'number'        => 'Card Number',
                                            'expiration'    => 'Card Expiration',
                                            'zip'           => 'Card Zip',
                                            'cvv'           => 'Card CVV'
                                        );

                                        // CHECK REQUIRED
                                        foreach ($reqfa as $name => $title) {
                                            if (!$_uccms_ecomm->checkRequired($_POST['payment'][$payment_method][$name])) {
                                                $out['error'] = 'Missing / Incorrect: Payment - ' .$title;
                                            }
                                        }

                                        $payment_name = $_POST['payment'][$payment_method]['name'];

                                        // PAYMENT LAST FOUR
                                        $payment_lastfour = substr(preg_replace("/[^0-9]/", '', $_POST['payment'][$payment_method]['number']), -4);

                                        // PAYMENT EXPIRATION DATE FORMATTING
                                        if ($_POST['payment'][$payment_method]['expiration']) {
                                            $payment_expiration = date('Y-m-d', strtotime(str_replace('/', '/01/', $_POST['payment'][$payment_method]['expiration'])));
                                        } else {
                                            $payment_expiration = '0000-00-00';
                                        }

                                    }

                                    // NO ERROR
                                    if (!$out['error']) {

                                        // PAYMENT - CHARGE
                                        $payment_data = array(
                                            'method'    => $payment_method,
                                            'amount'    => $amount,
                                            'customer' => array(
                                                'id'            => $cart['customer_id'],
                                                'name'          => $payment_name,
                                                'email'         => $_POST['billing']['email'],
                                                'phone'         => $_POST['billing']['phone'],
                                                'address'       => array(
                                                    'street'    => $_POST['billing']['address1'],
                                                    'street2'   => $_POST['billing']['address2'],
                                                    'city'      => $_POST['billing']['city'],
                                                    'state'     => $_POST['billing']['state'],
                                                    'zip'       => $_POST['billing']['zip'],
                                                    'country'   => $_POST['billing']['city']
                                                ),
                                                'description'   => ''
                                            ),
                                            'note'  => 'Order ID: ' .$cart['id']
                                        );

                                        // HAVE PAYMENT PROFILE
                                        if ($pp['id']) {
                                            $payment_data['card'] = array(
                                                'id'            => $pp['id'],
                                                'name'          => $payment_name,
                                                'number'        => $payment_lastfour,
                                                'expiration'    => $payment_expiration
                                            );

                                        // USE CARD
                                        } else {
                                            $payment_data['card'] = array(
                                                'name'          => $payment_name,
                                                'number'        => $_POST['payment'][$payment_method]['number'],
                                                'expiration'    => $payment_expiration,
                                                'zip'           => $_POST['payment'][$payment_method]['zip'],
                                                'cvv'           => $_POST['payment'][$payment_method]['cvv']
                                            );
                                        }

                                        // PROCESS PAYMENT
                                        $payment_result = $_uccms['_account']->payment_charge($payment_data);

                                        // TRANSACTION ID
                                        $transaction_id = $payment_result['transaction_id'];

                                        // TRANSACTION MESSAGE (ERROR)
                                        $transaction_message = $payment_result['error'];

                                        // LOG TRANSACTION
                                        $_uccms_ecomm->logTransaction(array(
                                            'cart_id'               => $cart['id'],
                                            'status'                => ($transaction_id ? 'charged' : 'failed'),
                                            'amount'                => $amount,
                                            'method'                => $_POST['payment']['method'],
                                            'payment_profile_id'    => $payment_result['profile_id'],
                                            'name'                  => $_POST['payment'][$_POST['payment']['method']]['name'],
                                            'lastfour'              => $payment_lastfour,
                                            'expiration'            => $payment_expiration,
                                            'transaction_id'        => $transaction_id,
                                            'message'               => $transaction_message,
                                            'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                                        ));

                                        // CHARGE SUCCESSFUL
                                        if ($transaction_id) {

                                            // ADD TO ORDER INFO
                                            $order['transaction_id'] = $transaction_id;

                                            // GET BALANCE
                                            $balance = $_uccms_ecomm->orderBalance($cart['id']);

                                            if ((!$balance) || ($balance == 0.00)) {
                                                $paid_in_full = 1;
                                            } else {
                                                $paid_in_full = 0;
                                            }

                                            // UPDATE ORDER
                                            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='charged', `order_total`=" .$_uccms_ecomm->orderTotal($order). ", `paid_in_full`=" .$paid_in_full. " WHERE (`id`=" .$cart['id']. ")";
                                            sqlquery($cart_query);

                                        // CHARGE FAILED
                                        } else {

                                            // UPDATE ORDER
                                            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='failed' WHERE (`id`=" .$cart['id']. ")";
                                            sqlquery($cart_query);

                                            $out['error'] = 'Credit Card Error (' .$transaction_message. ')';

                                        }

                                    }

                                // NO PAYMENT METHOD SPECIFIED
                                } else {

                                    // UPDATE STATUS
                                    $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='placed' WHERE (`id`=" .$cart['id']. ")";
                                    sqlquery($cart_query);

                                }

                            // NO AMOUNT TO CHARGE
                            } else {

                                // UPDATE STATUS
                                $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='placed' WHERE (`id`=" .$cart['id']. ")";
                                sqlquery($cart_query);

                            }

                        }

                        // NO ERROR
                        if (!$out['error']) {

                            // RECALCULATE ORDER
                            $_uccms_ecomm->orderRecalculate($cart['id']);

                            // COUPON USED
                            if ($discount['coupon']['id']) {

                                // INCREMENT USES
                                $incr_query = "UPDATE `" .$_uccms_ecomm->tables['coupons']. "` SET `uses`=`uses`+1 WHERE (`id`=" .$discount['coupon']['id']. ")";
                                sqlquery($incr_query);

                            }

                            // INVENTORY CONTROL ENABLED
                            if ($_uccms_ecomm->getSetting('inventory_track')) {

                                // HAVE ITEMS IN CART
                                if (count($citems) > 0) {

                                    // LOOP
                                    foreach ($citems as $oitem) {

                                        // ITEM OPTIONS
                                        if ($oitem['options']) {
                                            $attrs = json_decode(stripslashes($oitem['options']), true);
                                        } else {
                                            $attrs = array();
                                        }

                                        $vals = array(
                                            'available' => ($oitem['quantity'] * -1)
                                        );

                                        // ADJUST INVENTORY
                                        $inventory_result = $_uccms_ecomm->item_changeInventory($oitem['item_id'], $_uccms_ecomm->cart_locationID(), $attrs, $vals, array(
                                            'log'   => array(
                                                'source'    => 'checkout',
                                                'source_id' => $cart['id']
                                            )
                                        ));

                                        unset($ivars);

                                    }

                                }

                            }

                            ##########################
                            # EMAIL
                            ##########################

                            // EMAIL SETTINGS
                            $esettings = array(
                                'template'      => 'order_placed-customer.html',
                                'subject'       => 'Order Placed',
                                'order_id'      => $cart['id']
                            );

                            // AFTER TAX ITEMS
                            if ($evars_items_aftertax) {
                                $evars_items_aftertax = '
                                <tr>
                                    <td colspan="5" align="right" style="text-align: right;">
                                        ' .$evars_items_aftertax. '
                                    </td>
                                </tr>
                                ';
                            }

                            // BILLING ADDRESS
                            $billing_address = $_POST['billing']['address1'];
                            if ($_POST['billing']['address2']) {
                                $billing_address .= '<br />' .$_POST['billing']['address2'];
                            }

                            // SHIPPING ADDRESS
                            $shipping_address = $_POST['shipping']['address1'];
                            if ($_POST['shipping']['address2']) {
                                $shipping_address .= '<br />' .$_POST['shipping']['address2'];
                            }

                            // NOTES
                            if ($_POST['order']['notes']) {
                                $order_notes = '<div style="margin: 10px 0 10px 0;"><strong style="color: #666;">Notes:</strong> ' .$_POST['order']['notes']. '</div>';
                            }

                            // EMAIL VARIABLES
                            $evars = array(
                                'date'                  => date('n/j/Y'),
                                'time'                  => date('g:i A T'),
                                'order_id'              => $cart['id'],
                                'billing_firstname'     => $_POST['billing']['firstname'],
                                'billing_lastname'      => $_POST['billing']['lastname'],
                                'billing_address'       => $billing_address,
                                'billing_city'          => $_POST['billing']['city'],
                                'billing_state'         => $_POST['billing']['state'],
                                'billing_zip'           => $_POST['billing']['zip'],
                                'billing_phone'         => $_POST['billing']['phone'],
                                'billing_email'         => $_POST['billing']['email'],
                                'shipping_firstname'    => $_POST['shipping']['firstname'],
                                'shipping_lastname'     => $_POST['shipping']['lastname'],
                                'shipping_address'      => $shipping_address,
                                'shipping_city'         => $_POST['shipping']['city'],
                                'shipping_state'        => $_POST['shipping']['state'],
                                'shipping_zip'          => $_POST['shipping']['zip'],
                                'shipping_phone'        => $_POST['shipping']['phone'],
                                'shipping_email'        => $_POST['shipping']['email'],
                                'order_notes'           => $order_notes,
                                'items'                 => $evars_items,
                                'subtotal'              => '$' .number_format($order['subtotal'], 2),
                                'discount_element'      => $evars_discount,
                                'tax'                   => '$' .number_format($order['tax'], 2),
                                'items_aftertax'        => $evars_items_aftertax,
                                'shipping'              => '$' .number_format((float)$order['shipping'], 2),
                                'total'                 => '$' .number_format($order['total'], 2),
                                'payment'               => '$' .number_format($amount, 2),
                                'balance'               => '$' .number_format($_uccms_ecomm->orderBalance($cart['id']), 2),
                                'view_url'              => '#',
                                //'store_contact_phone'   => $store_contact_phone
                            );

                            // HAVE ACCOUNT ID
                            if ($cart['customer_id']) {
                                $evars['view_url'] = WWW_ROOT . $_uccms_ecomm->storePath(). '/order/review/?id=' .$cart['id']. '&h=' .$order_hash;
                            }

                            // GRATUITY ENABLED
                            if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
                                $evars['gratuity'] = '
                                <tr>
                                    <td colspan="5" align="right" style="padding-right: 10px; text-align: right;">
                                        Gratuity: $' .number_format($order['gratuity'], 2). '
                                    </td>
                                </tr>
                                ';
                            } else {
                                $evars['gratuity'] = '';
                            }

                            // WHO TO SEND EMAILS TO
                            $emailtoa = array();
                            $emailtoa[$_POST['billing']['email']] = $_POST['billing']['email'];

                            // PRE CUSTOMER EMAIL PROCESS
                            @include(dirname(__FILE__). '/.hooks/checkout-process_pre-customer-email.php');

                            // LOOP
                            foreach ($emailtoa as $to_email => $to_name) {

                                $esettings['to_email'] = $to_email;

                                // SEND EMAIL
                                $result = $_uccms_ecomm->sendEmail($esettings, $evars);

                            }

                            // COPY OF ORDER TO ADMIN(S)
                            if ($ecomm['settings']['email_order_copy']) {

                                $emails = explode(',', stripslashes($ecomm['settings']['email_order_copy']));
                                foreach ($emails as $email) {

                                    $esettings['template'] = 'order_placed-admin.html';
                                    $esettings['to_email'] = $email;

                                    $evars['view_url'] = WWW_ROOT . 'admin/com.umbrella.ecommerce*ecommerce/orders/edit/?id=' .$cart['id'];

                                    // SEND EMAIL
                                    $result = $_uccms_ecomm->sendEmail($esettings, $evars);

                                }

                            }

                            // HAVE ITEM OWNERS TO EMAIL
                            if (count($io_emails) > 0) {
                                foreach ($io_emails as $io_account_id => $io_info) {

                                    // HAVE TYPES
                                    if (count($io_info) > 0) {

                                        // GET ACCOUNT INFO
                                        $io_account = $_uccms['_account']->getAccount('', true, $io_account_id);

                                        // HAVE EMAIL ADDRESS
                                        if ($io_account['email']) {

                                            // HAVE NORMAL ITEMS
                                            if (count($io_info['items']) > 0) {
                                                $evars['items'] = implode('', $io_info['items']);
                                            }

                                            // HAVE AFTER-TAX ITEMS
                                            if (count($io_info['items_aftertax']) > 0) {
                                                $evars['items'] .= implode('', $io_info['items_aftertax']);
                                            }

                                            $evars['subtotal']  = '$' .number_format((float)$io_info['subtotal'], 2);
                                            $evars['total']     = '$' .number_format((float)$io_info['total'], 2);

                                            $esettings['template'] = 'order_placed-item_owner.html';
                                            $esettings['to_email'] = $io_account['email'];

                                            $evars['view_url'] = '';

                                            // SEND EMAIL
                                            $result = $_uccms_ecomm->sendEmail($esettings, $evars);

                                        }

                                    }

                                }
                            }

                            // POST ADMIN EMAIL PROCESS
                            @include(dirname(__FILE__). '/.hooks/checkout-process_post-admin-email.php');

                            // HAVE GLOBAL FREE SHIPPING
                            if ($global_free_shipping) {

                                // ADDITIONAL ORDER INFO
                                $order['shipping_method'] = 'Free';

                            // NO GLOBAL FREE SHIPPING
                            } else {

                                // ADDITIONAL ORDER INFO
                                $order['shipping_method'] = stripslashes($shipping['method']). ' - ' .stripslashes($shipping['title']);

                            }

                            // ORDER DATA FOR REVSOCIAL API
                            $rs_orderdata['order']          = $order;
                            $rs_orderdata['order']['total'] = number_format($_uccms_ecomm->toFloat($order['total']), 2);
                            $rs_orderdata['order']['paid']  = number_format($_uccms_ecomm->toFloat($amount), 2);
                            $rs_orderdata['items']          = $rs_items;

                            // RECORD ORDER WITH REVSOCIAL
                            $_uccms_ecomm->revsocial_recordOrder($cart['id'], $customer_id, $rs_orderdata);

                            // CLEAR CART ID
                            $_uccms_ecomm->clearCartID(true);

                            $out['success'] = true;

                        }

                    }

                }

            // NO ITEMS IN CART
            } else {
                $out['error'] = 'No items in cart.';
            }

        // NO DATA
        } else {
            $out['error'] = 'No order data submitted.';
        }

    // NO CART
    } else {
        $out['error'] = 'No cart.';
    }

}

echo json_encode($out);

?>