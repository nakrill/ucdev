<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // DATES
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];

    // HAVE DATES
    if (($start) && ($end)) {

        // STATUS COLORS
        foreach ($_uccms_ecomm->statusColors() as $type => $colors) {
            foreach ($colors as $status => $color) {
                $status_colors[$status] = $color;
            }
        }

        // FORCE FORMAT
        $start  = date('Y-m-d', strtotime($start));
        $end    = date('Y-m-d', strtotime($end));

        // CATERING
        if ($_uccms_ecomm->storeType() == 'catering') {

            // MATTD: Expand timeframe search by two weeks, but still only return items that are within actual start and end

            // LIMIT BY STATUS TYPE
            if (($_REQUEST['status']) && ($status_colors[$_REQUEST['status']])) {

                // GET ORDERS
                $eorder_query = "
                SELECT qc.*
                FROM `" .$_uccms_ecomm->tables['quote_catering']. "` AS `qc`
                INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON qc.order_id=o.id
                WHERE (qc.event_date>='" .$start. "') AND (qc.event_date<='" .$end. "') AND (o.status='" .sqlescape($_REQUEST['status']). "')
                ";

            // GET ALL ORDERS IN TIMEFRAME
            } else {

                // GET ORDERS
                $eorder_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_catering']. "` WHERE (`event_date`>='" .$start. "') AND (`event_date`<='" .$end. "')";

            }

            $eorder_q = sqlquery($eorder_query);

            // HAVE ORDERS
            if (count(sqlrows($eorder_q)) > 0) {

                // GET MEALTIMES
                $mta = $_uccms_ecomm->stc->mealTimes();

                // LOOP
                while ($eorder = sqlfetch($eorder_q)) {

                    // GET ORDER INFO
                    $order_query = "SELECT `status`, `quote` FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$eorder['order_id']. ")";
                    $order_q = sqlquery($order_query);
                    $order = sqlfetch($order_q);

                    // NOT DELETED
                    if ($order['status'] != 'deleted') {

                        // TYPE CSS
                        $css_type = ($order['quote'] == 1 ? 'quote' : 'order');

                        // DEFAULT TITLE
                        if ($eorder['location1']) {
                            $title = stripslashes($eorder['location1']);
                        } else if ($eorder['contact_company']) {
                            $title = stripslashes($eorder['contact_company']);
                        } else {
                            $title = trim(stripslashes($eorder['contact_firstname']. ' ' .$eorder['contact_lastname']));
                        }
                        if (!$title) $title = 'Event';

                        $oc = 0;

                        // GET ORDER ITEMS
                        $citems = $_uccms_ecomm->cartItems($eorder['order_id']);

                        // HAVE ITEMS
                        if (count($citems) > 0) {

                            // ITEM DATE AND TIME ARRAY FOR ORDER
                            $idta = array();

                            // LOOP
                            foreach ($citems as $item) {

                                // HAS EXTRA
                                if ($item['extra']) {

                                    // GET EXTRA
                                    $extra = json_decode(stripslashes($item['extra']), true);

                                    // HAVE DATE
                                    if ($extra['date']) {

                                        // ITEM DATE AND TIME
                                        $dt = trim($extra['date']. ' ' .$mta[$extra['mealtime']]['time']);

                                        // NOT IN ORDER ITEM DATE AND TIME ARRAY YET
                                        if (!$idta[$dt]) {

                                            // HAVE MEALTIME
                                            if ($mta[$extra['mealtime']]['title']) {
                                                $item_title = $mta[$extra['mealtime']]['title']. ' - ' .$title;
                                            }

                                            // EVENT INFO ARRAY
                                            $out[] = array(
                                                'id'        => 'i-' .$item['id'],
                                                'title'     => $item_title,
                                                'start'     => trim($extra['date']. ' ' .$mta[$extra['mealtime']]['time']),
                                                'url'       => '/admin/' .$_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$eorder['order_id'],
                                                'className' => $css_type. ' ' .stripslashes($order['status'])
                                            );

                                            $idta[$dt] = $dt;

                                            $oc++;

                                        }

                                    }

                                }

                            }

                        }

                        // NO ITEMS - USE MAIN EVENT INFO
                        if ($oc == 0) {

                            // EVENT INFO ARRAY
                            $out[] = array(
                                'id'        => 'o-' .$eorder['id'],
                                'title'     => $title,
                                'start'     => $eorder['event_date']. ' ' .$eorder['service_time'],
                                'url'       => '/admin/' .$_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$eorder['order_id'],
                                'className' => $css_type. ' ' .stripslashes($order['status'])
                            );

                        }

                    }

                }

            }

        }

        // CUSTOM
        if (file_exists(dirname(__FILE__). '/events_custom.php')) {
            include(dirname(__FILE__). '/events_custom.php');
        }

    }

}

echo json_encode($out);

?>