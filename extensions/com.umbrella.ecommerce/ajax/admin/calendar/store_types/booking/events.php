<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CUSTOM
    if (file_exists(dirname(__FILE__). '/events_custom.php')) {
        include(dirname(__FILE__). '/events_custom.php');
        exit;
    }

    // DATES
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];

    // HAVE DATES
    if (($start) && ($end)) {

        // STATUS COLORS
        foreach ($_uccms_ecomm->statusColors() as $type => $colors) {
            foreach ($colors as $status => $color) {
                $status_colors[$status] = $color;
            }
        }

        // GET BOOKINGS
        $bookings = $_uccms_ecomm->stc->getBookings(strtotime($start), strtotime($end), array());

        // HAVE BOOKINGS
        if (count($bookings) > 0) {

            $i = 0;

            // LOOP
            foreach ($bookings as $booking) {

                // GET ORDER INFO
                $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$booking['order_id']. ")";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                // NOT DELETED
                if ($order['status'] != 'deleted') {

                    // TYPE CSS
                    $css_type = ($order['quote'] == 1 ? 'quote' : 'order');

                    // EVENT INFO ARRAY
                    $out[$i] = array(
                        'id'        => 'b-' .$booking['id'],
                        'title'     => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                        'start'     => $booking['dt_from'],
                        'url'       => '/admin/' .$_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$booking['order_id'],
                        'className' => $css_type. ' ' .stripslashes($order['status'])
                    );

                    if ($booking['dt_to'] != '0000-00-00 00:00:00') {
                        $out[$i]['end'] = $booking['dt_to'];
                    }

                }

                $i++;

            }

        }

    }

}

echo json_encode($out);

?>