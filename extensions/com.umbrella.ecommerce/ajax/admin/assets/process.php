<?php

$out = [];

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $id = (int)$_POST['id'];
    $item_id = (int)$_POST['item_id'];
    $customer_id = (int)$_POST['customer_id'];

    if (!$item_id) echo json_encode([ 'error' => 'Item required.' ]);
    if (!$customer_id) echo json_encode([ 'error' => 'Customer required.' ]);

    // START DATE / TIME
    if ($_REQUEST['service_start']) {
        $service_start = date('Y-m-d H:i:s', strtotime($_REQUEST['service_start']));
    } else {
        $service_start = '0000-00-00 00:00:00';
    }

    // END DATE / TIME
    if ($_REQUEST['service_end']) {
        $service_end = date('Y-m-d H:i:s', strtotime($_REQUEST['service_end']));
    } else {
        $service_end = '0000-00-00 00:00:00';
    }

    $recurring_byday = '';
    $recurring_bymonthday = 0;

    // FREQ - WEEKLY
    if ($_REQUEST['recurring_freq'] == 'week') {
        $recurring_byday = implode(',', $_REQUEST['recurring_days']);

    // FREQ - MONTHLY
    } else if ($_REQUEST['recurring_freq'] == 'month') {
        if ($_REQUEST['recurring_by'] == 'dotw') {
            $dowa = $_uccms_staff->daysOfWeek();
            $recurring_byday = $dowa[date('N', strtotime($service_start))]['code'];
        } else {
            $recurring_bymonthday = date('j', strtotime($service_start));
        }
    }

    // RECURRING END - ON END DATE
    if ($_REQUEST['recurring_end'] == 0) {
        if (!$_REQUEST['service_end']) {
            $service_end = $service_start;
        }
    }

    // RECURRING END - AFTER COUNT
    if ($_REQUEST['recurring_end'] == 1) {
        $service_end = '0000-00-00 ' .date('H:i:s', strtotime($_REQUEST['end']));
    } else {
        $_REQUEST['recurring_count'] = 0;
    }

    // RECURRING END - NEVER
    if ($_REQUEST['recurring_end'] == -1) {
        $service_end = '0000-00-00 ' .date('H:i:s', strtotime($_REQUEST['end']));
        $_REQUEST['recurring_count'] = -1;
    }

    // DB COLUMNS
    $columns = [
        'status'                => 1,
        'item_id'               => $item_id,
        'customer_id'           => $customer_id,
        'contact_id'            => (int)$_POST['contact_id'],
        'serial'                => $_POST['serial'],
        'service_start'         => $service_start,
        'service_end'           => $service_end,
        'recurring'             => (int)$_REQUEST['recurring'],
        'recurring_freq'        => $_REQUEST['recurring_freq'],
        'recurring_interval'    => (int)$_REQUEST['recurring_interval'],
        'recurring_count'       => (float)$_REQUEST['recurring_count'],
        'recurring_byday'       => $recurring_byday,
        'recurring_bymonthday'  => $recurring_bymonthday,
        'notes'                 => $_POST['notes']
    ];

    // EXISTING
    if ($id) {

        $query = "UPDATE `" .$_uccms_ecomm->tables['assets']. "` SET " .$_uccms_ecomm->createSet($columns). ", `dt_updated`=NOW() WHERE (`id`=" .$id. ")";
        if (sqlquery($query)) {
            $out['success'] = true;
        } else {
            $out['error'] = 'Failed to update asset.';
        }

    // NEW
    } else {

        $query = "INSERT INTO `" .$_uccms_ecomm->tables['assets']. "` SET " .$_uccms_ecomm->createSet($columns). ", `dt_created`=NOW()";
        if (sqlquery($query)) {
            $out['success'] = true;
            $out['id'] = sqlid();
        } else {
            $out['error'] = 'Failed to assign asset.';
        }

    }

}

echo json_encode($out);

?>