<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // ASSET ITEMS
    $aitems = [];
    $aitems_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`type`=4) AND (`deleted`=0) ORDER BY `title` ASC";
    $aitems_q = sqlquery($aitems_query);
    while ($aitem = sqlfetch($aitems_q)) {
        $aitems[$aitem['id']] = $aitem;
    }

    // CUSTOMERS
    $customers = $_uccms_ecomm->getCustomers([
        'limit' => 99999
    ]);

    //print_r($customers);

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // HAVE ID
    if ($id) {

        // GET ASSET
        $asset_query = "SELECT * FROM `" .$_uccms_ecomm->tables['assets']. "` WHERE (`id`=" .$id. ")";
        $asset_q = sqlquery($asset_query);
        $asset = sqlfetch($asset_q);

        // ASSET FOUND
        if ($asset['id']) {

            $asset['item'] = $aitems[$asset['item_id']];

        }

    }

    ?>

    <style type="text/css">

        #asset .alert {
            display: none;
        }

        #asset textarea {
            height: 90px;
        }

        #modal-assignAsset .repeats_enabled_content .repeat_every label {
            font-weight: 500;
        }
        #modal-assignAsset .repeats_enabled_content .repeat_every select {
            display: inline-block;
            width: auto;
        }
        #modal-assignAsset .repeats_enabled_content .repeat_every span {
            padding-left: 3px;
            font-size: 14px;
        }

        #modal-assignAsset .repeats_enabled_content .freq_content .contain {
            display: flex;
            justify-content: space-between;
        }
        #modal-assignAsset .repeats_enabled_content .freq_content .dayofweek {

        }

        #modal-assignAsset .repeats_enabled_content .repeats_end {
            margin: 0px;
        }
        #modal-assignAsset .repeats_enabled_content .repeats_end label {
            font-weight: 500;
        }
        #modal-assignAsset .repeats_enabled_content .repeats_end input[type="text"] {
            display: inline-block;
            width: 72px;
        }

    </style>

    <script type="text/javascript">

        // UPDATE MODAL TITLE
        //$('#modal-assignAsset .modal-title').html('<img src="<?php echo $image_url; ?>" alt=""><span class="item-title"><?php echo addslashes(stripslashes($item['title'])); ?></span><span class="title">: Asset</span>');

        $(function() {

            // DATE PICKER
            $('#asset .date_time_picker').datetimepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "MM/DD/YYYY HH:mm:ss" });

            // INPUT CHANGE
            $('#asset input').change(function() {
                assetModalChanged = true;
            });

            // REPEATS TOGGLE
            $('#modal-assignAsset input[name="recurring"]').change(function(e) {
                if ($(this).prop('checked')) {
                    $('#modal-assignAsset .repeats_enabled_content').show();
                } else {
                    $('#modal-assignAsset .repeats_enabled_content').hide();
                }
            });

            // REPEATS FREQUENCY CHANGE
            $('#modal-assignAsset .repeats_enabled_content select[name="recurring_freq"]').change(function() {
                var freq = $(this).val();
                $('#modal-assignAsset .freq_content_container .freq_content').hide();
                $('#modal-assignAsset .freq_content_container .freq_content.' +freq).show();
                $('#modal-assignAsset .repeats_enabled_content .repeat_every .re').hide();
                $('#modal-assignAsset .repeats_enabled_content .repeat_every .re.' +freq).show();
            });

            // SAVE BUTTON CLICK
            $('#modal-assignAsset .btn.save').click(function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                this_saveAsset();
            });

            /*
            // ON MODAL CLOSE
            $('#modal-assignAsset').on('hide.bs.modal', function(e) {
                if (assetModalChanged) {
                    if (confirm('Do you want to save your changes before closing?')) {
                        this_saveAsset({}, {}, function(data) {
                            if (!data.success) {
                                e.preventDefault();
                                e.stopImmediatePropagation();
                                if (!data.error) data.error = 'Failed to save.';
                                alert(data.error);
                                return false;
                            }
                        });
                    } else {
                         //e.preventDefault();
                         //e.stopImmediatePropagation();
                         //return false;
                    }
                }
            });
            */

        });

        // SAVE ASSET
        function this_saveAsset(params, options, callback) {
            $('#asset .alert').hide().removeClass('alert-success alert-danger');
            $.post('<?=ADMIN_ROOT?>*/com.umbrella.ecommerce/ajax/admin/assets/process/', $('#form_asset').serialize(), function(data) {
                if (data.success) {
                    $('#asset .alert strong').text('Asset saved!');
                    $('#asset .alert').addClass('alert-success').show();
                } else {
                    if (!data.error) data.error = 'Failed to save.';
                    $('#asset .alert strong').text(data.error);
                    $('#asset .alert').addClass('alert-danger').show();
                }
                if (typeof callback == 'function') {
                    callback(data);
                }
            }, 'json');
        }

    </script>

    <form id="form_asset" enctype="multipart/form-data" action="#" method="post">
    <input type="hidden" name="id" value="<?php echo $asset['id']; ?>" />

    <div id="asset">

        <div class="alert alert-dismissible fade show" role="alert">
            <strong></strong>
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="row">

            <div class="col-md-6">

                <div class="form-group">
                    <label for="modal-assignAsset-item">Item</label>
                    <select name="item_id" class="form-control" id="modal-assignAsset-item">
                        <option value="">Select</option>
                        <?php foreach ($aitems as $aitem) { ?>
                            <option value="<?php echo $aitem['id']; ?>" <?php if ($aitem['id'] == $asset['item_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($aitem['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="modal-assignAsset-customer">Customer</label>
                    <select name="customer_id" class="form-control" id="modal-assignAsset-customer">
                        <option value="">Select</option>
                        <?php foreach ($customers['customers'] as $customer) { ?>
                            <option value="<?php echo $customer['account_id']; ?>" <?php if ($customer['account_id'] == $asset['customer_id']) { ?>selected="selected"<?php } ?>><?php echo trim(stripslashes($customer['firstname']. ' ' .$customer['lastname'])); ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="modal-assignAsset-serial">Serial</label>
                    <input type="text" name="serial" value="<?php echo stripslashes($asset['serial']); ?>" class="form-control" id="modal-assignAsset-serial" placeholder="">
                </div>

                <div class="form-group">
                    <label for="modal-assignAsset-notes">Notes</label>
                    <textarea name="notes" class="form-control" id="modal-assignAsset-notes" placeholder=""><?php echo stripslashes($asset['notes']); ?></textarea>
                </div>

            </div>

            <div class="col-md-6">

                <div class="row">

                    <div class="col-md-6">

                        <div class="form-group" style="position: relative;">
                            <label for="modal-assignAsset-serviceStart">Service start</label>
                            <?php
                            $field = array(
                                'key'       => 'service_start', // The value you should use for the "name" attribute of your form field
                                'value'     => ((($asset['service_start']) && ($asset['service_start'] != '0000-00-00 00:00:00')) ? $asset['service_start'] : ''), // The existing value for this form field
                                'id'        => 'modal-assignAsset-serviceStart', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => []
                            );
                            include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                            ?>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="form-group" style="position: relative;">
                            <label for="modal-assignAsset-serviceEnd">Service end</label>
                            <?php
                            $field = array(
                                'key'       => 'service_end', // The value you should use for the "name" attribute of your form field
                                'value'     => ((($asset['service_end']) && ($asset['service_end'] != '0000-00-00 00:00:00')) ? $asset['service_end'] : ''), // The existing value for this form field
                                'id'        => 'modal-assignAsset-serviceEnd', // A unique ID you can assign to your form field for use in JavaScript
                                'tabindex'  => '', // The current tab index you can use for the "tabindex" attribute of your form field
                                'options'   => []
                            );
                            include(BigTree::path('admin/form-field-types/draw/datetime.php'));
                            ?>
                        </div>

                    </div>

                </div>

                <div class="card recurring">
                    <div class="card-body">
                        <h5 class="card-title">Recurring</h5>

                        <div class="form-group" style="margin-bottom: 0px;">
                            <label><input id="repeats_enabled" type="checkbox" name="recurring" value="1" <?php if ($asset['recurring']) { ?>checked="checked"<?php } ?> /> Repeats</label>
                        </div>

                        <div class="repeats_enabled_content" style="padding-top: 5px;<?php if (!$asset['recurring']) { echo ' display: none;'; } ?>">

                            <div class="row">

                                <div class="col-md-6">

                                    <fieldset class="form-group">
                                        <select name="recurring_freq" class="form-control">
                                            <option value="day" <?php if ($asset['recurring_freq'] == 'day') { ?>selected="selected"<?php } ?>>Daily</option>
                                            <option value="week" <?php if ($asset['recurring_freq'] == 'week') { ?>selected="selected"<?php } ?>>Weekly</option>
                                            <option value="month" <?php if ($asset['recurring_freq'] == 'month') { ?>selected="selected"<?php } ?>>Monthly</option>
                                            <option value="year" <?php if ($asset['recurring_freq'] == 'year') { ?>selected="selected"<?php } ?>>Yearly</option>
                                        </select>
                                    </fieldset>

                                </div>

                                <div class="col-md-6">

                                    <fieldset class="repeat_every form-group">
                                        <label>Every</label>
                                        <select name="recurring_interval" class="form-control">
                                            <?php for ($i=1; $i<=31; $i++) { ?>
                                                <option value="<?php echo $i; ?>" <?php if ($i == $asset['recurring_interval']) { echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select> <span class="re day" style="<?php if (($asset['recurring_freq'] != 'day') && ($asset['recurring_freq'])) { ?>display: none;<?php } ?>">days</span><span class="re week" style="<?php if ($asset['recurring_freq'] != 'week') { ?>display: none;<?php } ?>">weeks</span><span class="re month" style="<?php if ($asset['recurring_freq'] != 'month') { ?>display: none;<?php } ?>">months</span><span class="re year" style="<?php if ($asset['recurring_freq'] != 'year') { ?>display: none;<?php } ?>">years</span>
                                    </fieldset>

                                </div>

                            </div>

                            <div class="freq_content_container">

                                <div class="freq_content week form-group" style="<?php if ($asset['recurring_freq'] != 'week') { echo 'display: none;'; } ?>">
                                    <label>Repeat On</label>
                                    <div class="contain">
                                        <?php
                                        $seldaya = [];
                                        if ($asset['recurring_freq'] == 'week') {
                                            $seldaya = explode(',', stripslashes($asset['recurring_byday']));
                                        }
                                        foreach ($_uccms_ecomm->daysOfWeek() as $day_id => $day) {
                                            ?>
                                            <div class="dayofweek">
                                                <input type="checkbox" name="recurring_days[]" value="<?php echo $day['code']; ?>" <?php if (in_array($day['code'], $seldaya)) { ?>checked="checked"<?php } ?> /> <?php echo substr($day['title'], 0, 1); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="freq_content month form-group" style="<?php if ($asset['recurring_freq'] != 'month') { echo 'display: none;'; } ?>">
                                    <label>Repeat By</label>
                                    <div class="contain">
                                        <div style="float: left;">
                                            <input type="radio" name="recurring_by" value="dotw" <?php if ($asset['recurring_byday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Week</label>
                                        </div>
                                        <div style="float: left;">
                                            <input type="radio" name="recurring_by" value="dotm" <?php if ($asset['recurring_bymonthday']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Day of the Month</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <fieldset class="repeats_end form-group">
                                <label>Ends</label>
                                <div>
                                    <fieldset class="last">
                                        <input type="radio" name="recurring_end" value="0" <?php if (!$asset['recurring_count']) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">On end date</label>
                                    </fieldset>
                                    <fieldset class="last radio_padding">
                                        <input type="radio" name="recurring_end" value="1" <?php if ($asset['recurring_count'] > 0) { echo 'checked="checked"'; } ?> style="margin-top: 3px;" /><label class="for_checkbox">After <input type="text" name="route[recurring_count]" value="<?php if ($asset['recurring_count'] > 0) { echo $asset['recurring_count']; } ?>" class="form-control" /> occurances</label>
                                    </fieldset>
                                    <fieldset>
                                        <input type="radio" name="recurring_end" value="-1" <?php if ($asset['recurring_count'] == -1) { echo 'checked="checked"'; } ?> /><label class="for_checkbox">Never</label>
                                    </fieldset>
                                </div>
                            </fieldset>

                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    </form>

    <?php

} else {
    echo 'You do not have necessary permissions.';
}

?>