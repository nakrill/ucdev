<?php

// INCLUDE COMPOSER
include_once(SERVER_ROOT. 'uccms/includes/libs/vendor/autoload.php');

use When\When;

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    ?>

    <script type="text/javascript">

        var assetsModalChanged = false;

        $(function() {

            // EDIT
            $('#assets .asset .edit').click(function(e) {
                assignAsset({
                    id: $(this).closest('.asset').data('id')
                });
            });

        });

    </script>

    <?php

    /*
    $_uccms_ecomm->setPerPage(1);

    $page = (is_numeric($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1);

    $wa = [];

    // ASSET
    $wa[] = "i.type=4";

    // NOT DELETED
    $wa[] = "i.deleted=0";

    /*
    // CATEGORY SPECIFIED
    if ($category_id > 0) {
        $inner_join = "INNER JOIN `" .$_uccms_ecomm->tables['item_categories']. "` AS `icr` ON i.id=icr.item_id";
        $wa[] = "icr.category_id=" .$category_id;
    } else {
        $inner_join = "";
    }

    // BY TITLE
    if ($_REQUEST['title']) {
        $wa[] = "LOWER(i.title) LIKE '%" .sqlescape(strtolower($_REQUEST['title'])). "%'";
    }

    // BY ID
    if ($_REQUEST['id']) {
        $wa[] = "i.id=" .(int)$_REQUEST['id'];
    }

    // BY SKU
    if ($_REQUEST['sku']) {
        $wa[] = "LOWER(i.sku) LIKE '%" .sqlescape(strtolower($_REQUEST['sku'])). "%'";
    }

    // GET PAGED
    $assets_query = "
    SELECT a.*
    FROM `" .$_uccms_ecomm->tables['assets']. "` AS `a`
    INNER JOIN `" .$_uccms_ecomm->tables['items']. "` AS `i` ON i.id=a.item_id
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY i.title ASC, i.id ASC
    LIMIT " .(($page - 1) * $_uccms_ecomm->perPage()). "," .$_uccms_ecomm->perPage();

    $assets_q = sqlquery($assets_query);

    // NUMBER OF PAGED
    $num_assets = sqlrows($assets_q);

    // TOTAL NUMBER
    $total_assets = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ecomm->tables['items']. "` AS `i` " .$inner_join. " WHERE (" .implode(") AND (", $wa). ")"));

    */

    // GET ASSETS
    $assets = $_uccms_ecomm->getAssets($_REQUEST, [
        'include' => [ 'customer', 'item', 'schedule' ],
        'schedule' => [ 'limit' => 1, 'future' => true ]
    ]);

    $num_assets = count((array)$assets['assets']);

    // HAVE ASSETS
    if ($num_assets > 0) {

        /*
        // PAGING CLASS
        require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

        // INIT PAGING CLASS
        $_paging = new paging();

        // OPTIONAL - URL VARIABLES TO IGNORE
        $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

        // PREPARE PAGING
        $_paging->prepare($_uccms_ecomm->perPage(), 10, $_REQUEST['page'], $_REQUEST);

        // GENERATE PAGING
        $pages = $_paging->output($num_assets, $total_assets['total']);
        */

        ?>

        <style type="text/css">

            #assets .data .asset .item img {
                max-height: 19px;
                width: auto;
                vertical-align: text-top;
            }


        </style>

        <? /*
        <div class="paging clearfix">
            <h6><?php echo number_format($assets['total'], 0); ?> Asset<?php echo ($assets['total'] != 1 ? 's' : ''); ?></h6>
        </div>
        */ ?>

        <div class="table">
            <div class="table-responsive">

                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th class="customer" scope="col">Customer</th>
                            <th class="item" scope="col">Item</th>
                            <th class="serial" scope="col">Serial</th>
                            <th class="service" scope="col">In service</th>
                            <th class="next" scope="col">Next service</th>
                            <th class="options" scope="col">Options</th>
                        </tr>
                    </thead>
                    <tbody class="items">

                        <?php

                        // LOOP
                        foreach ($assets['assets'] as $asset) {

                            ?>

                            <tr class="asset" data-id="<?php echo $asset['id']; ?>">
                                <td class="customer">
                                    <a href="/admin/com.umbrella.ecommerce*ecommerce/customers/edit/?id=<?php echo $asset['customer']['id']; ?>" target="_blank"><?php echo trim(stripslashes($asset['customer']['firstname']. ' ' .$asset['customer']['lastname'])); ?></a>
                                </td>
                                <td class="item">
                                    <a href="/admin/com.umbrella.ecommerce*ecommerce/items/edit/?id=<?php echo $asset['item']['id']; ?>" target="_blank">
                                        <img src="<?php echo $asset['item']['image']; ?>" alt="" />
                                        <span class="title"><?php echo stripslashes($asset['item']['title']); ?></span>
                                    </a>
                                </td>
                                <td class="serial">
                                    <?php echo stripslashes($asset['serial']); ?>
                                </td>
                                <td class="service">
                                    <?php
                                    $sda = [];
                                    if (($asset['service_start']) && ($asset['service_start'] != '0000-00-00 00:00:00')) {
                                        $service_start = trim(str_replace(' 00:00:00', '', $asset['service_start']));
                                        $sda[] = date('n/j/Y' .(strlen($service_start) > 10 ? ' g:i A' : ''), strtotime($service_start));
                                    }
                                    if (($asset['service_end']) && ($asset['service_end'] != '0000-00-00 00:00:00')) {
                                        $service_end = trim(str_replace(' 00:00:00', '', $asset['service_end']));
                                        $sda[] = date('n/j/Y' .(strlen($service_end) > 10 ? ' g:i A' : ''), strtotime($service_end));
                                    }
                                    echo (count($sda) ? implode(' - ', $sda) : '&nbsp;');
                                    ?>
                                </td>
                                <td class="next">
                                    <?php if ($asset['schedule'][0]) { echo date('n/j/Y', strtotime($asset['schedule'][0])); } ?>
                                </td>
                                <td class="options">
                                    <a href="#" class="edit" title="Edit" data-id="<?php echo $asset['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="#" class="delete" title="Delete"><i class="fas fa-times"></i></a>
                                </td>
                            </tr>

                        <?php } ?>

                    </tbody>
                </table>

            </div>
        </div>

        <?php

    // NO RESULTS
    } else {
        ?>
        <div class="alert alert-warning" role="alert">
            No assigned assets found.
        </div>
        <?php
    }

    ?>

    <?php

}

?>

<script>
    BigTree.setPageCount('#jobs .summary .view_paging' ,<?php echo (int)$assets['pages']; ?>, <?php echo (int)$assets['page']; ?>);
</script>