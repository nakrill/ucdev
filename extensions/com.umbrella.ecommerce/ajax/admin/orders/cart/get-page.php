<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    $query = isset($_GET["query"]) ? $_GET["query"] : "";
    $page = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

    //$_uccms_ecomm->setPerPage(2);

    // WHERE ARRAY
    $wa = array();

    // STATUS
    $wa[] = "`status`='cart'";

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            if (is_numeric($part)) {
                $twa[] = "(`id`=" .$part. ") OR (LOWER(`billing_phone`) LIKE '%$part%') OR (LOWER(`shipping_phone`) LIKE '%$part%')";
            } else {
                $twa[] = "(LOWER(`billing_firstname`) LIKE '%$part%') OR (LOWER(`billing_lastname`) LIKE '%$part%') OR (LOWER(`billing_email`) LIKE '%$part%') OR (LOWER(`shipping_firstname`) LIKE '%$part%') OR (LOWER(`shipping_lastname`) LIKE '%$part%') OR (LOWER(`shipping_email`) LIKE '%$part%')";
            }
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // HAVE WHERE VALUES
    if (count($wa) > 0) {
        $where = "WHERE (" .implode(") AND (", $wa). ")";
    } else {
        $where = "";
    }

    // GET PAGED ORDERS
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` " .$where. " ORDER BY `dt` DESC LIMIT " .(($page - 1) * $_uccms_ecomm->perPage()). "," .$_uccms_ecomm->perPage();
    $order_q = sqlquery($order_query);

    // NUMBER OF PAGED ORDERS
    $num_orders = sqlrows($order_q);

    // TOTAL NUMBER OF ORDERS
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ecomm->tables['orders']. "` " .$where));

    // NUMBER OF PAGES
    $pages = $_uccms_ecomm->pageCount($total_results['total']);

    // HAVE ORDERS
    if ($num_orders > 0) {

        // LOOP
        while ($order = sqlfetch($order_q)) {

            // GET ITEMS
            $oitems = $_uccms_ecomm->cartItems($order['id']);

            ?>

            <li class="item">
                <section class="order_id">
                    <?php echo $order['id']; ?>
                </section>
                <section class="order_dt">
                    <?php echo date('n/j/Y g:i A T', strtotime($order['dt'])); ?>
                </section>
                <section class="order_customer">
                    <?php echo trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])); ?>
                </section>
                <section class="order_items">
                    <?php echo number_format(count($oitems), 0); ?>
                </section>
                <section class="order_total">
                    $<?php echo number_format($order['order_total'], 2); ?>
                </section>
                <section class="order_edit">
                    <a class="icon_edit" title="View / Edit Order" href="../edit/?id=<?php echo $order['id']; ?>"></a>
                </section>
                <section class="order_delete">
                    <a href="../delete/?id=<?php echo $order['id']; ?>" class="icon_delete" title="Delete Order" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
                </section>
            </li>

            <?php

            unset($oitems);

        }

    // NO ORDERS
    } else {
        ?>
        <li style="text-align: center;">
            No matching orders.
        </li>
        <?php
    }

    ?>

    <script>
        BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>