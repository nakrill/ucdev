<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    ?>

    <style type="text/css">

        #orders summary .status {
            float: right;
            padding: 2px 5px 0 0;
        }

        #orders ul.items li {
            height: auto;
        }
        #orders ul.items li section {
            height: auto;
            overflow: visible;
        }

        #orders .order_id {
            width: 100px;
        }
        #orders .order_dt {
            width: 160px;
            text-align: left;
        }
        #orders .order_customer {
            width: 300px;
            text-align: left;
        }
        #orders .order_items {
            width: 100px;
        }
        #orders .order_total {
            width: 100px;
        }
        #orders .order_status {
            width: 135px;
        }
        #orders .order_edit {
            width: 55px;
        }
        #orders .item .order_edit {
            line-height: 1em;
        }
        #orders .order_delete {
            width: 55px;
        }
        #orders .item .order_delete {
            line-height: 1em;
        }

        #orders .item .larger {
            font-size: 1.1em;
        }
        #orders .item .smaller {
            font-size: 0.9em;
        }

        #orders .catering_event {
            width: 176px;
            text-align: left;
        }
        #orders .item .catering_event {
            padding: 4px 0;
            line-height: 1.2em;
        }
        #orders .catering_contact {
            width: 176px;
            text-align: left;
        }
        #orders .item .catering_contact {
            padding: 4px 0;
            line-height: 1.2em;
        }
        #orders .catering_dt {
            width: 132px;
            text-align: left;
        }
        #orders .item .catering_dt {
            padding: 4px 0;
            line-height: 1.2em;
        }
        #orders .catering_location {
            width: 176px;
            text-align: left;
        }
        #orders .item .catering_location {
            padding: 4px 0;
            line-height: 1.2em;
        }

    </style>

    <?php

    $query      = isset($_REQUEST['query']) ? $_REQUEST['query'] : '';
    $page       = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
    $limit      = isset($_REQUEST['limit']) ? intval($_REQUEST['limit']) : $_uccms_ecomm->perPage();
    $base_url   = isset($_REQUEST['base_url']) ? $_REQUEST['base_url'] : '';
    $date_from  = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : '';
    $date_to    = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : '';

    //$_uccms_ecomm->setPerPage(1);

    // WHERE ARRAY
    unset($wa);
    $wa = array();

    // STATUS
    if ($_REQUEST['status']) {
        if ($_REQUEST['status'] != 'all') {
            $wa[] = "`status`='" .sqlescape($_REQUEST['status']). "'";
        }
    } else {
        $wa[] = "(`status`!='cart') AND (`status`!='deleted')";
    }

    // CUSTOMER ID
    if ($_REQUEST['customer_id']) {
        $wa[] = "`customer_id`=" .(int)$_REQUEST['customer_id'];
    }

    // QUOTE
    if ($_REQUEST['quote']) {
        $wa[] = "`quote`=1";
        if (!$base_url) {
            if (defined('MODULE_ROOT')) {
                $base_url = MODULE_ROOT. 'quotes';
            } else {
                $base_url = '.';
            }
        }

    // ORDER
    } else {
        $wa[] = "`quote`!=1";
        if (!$base_url) {
            if (defined('MODULE_ROOT')) {
                $base_url = MODULE_ROOT. 'orders';
            } else {
                $base_url = '.';
            }
        }
    }

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            if (is_numeric($part)) {
                $twa[] = "(`id`=" .$part. ") OR (LOWER(`billing_phone`) LIKE '%$part%') OR (LOWER(`shipping_phone`) LIKE '%$part%')";
            } else {
                $twa[] = "(LOWER(`billing_firstname`) LIKE '%$part%') OR (LOWER(`billing_lastname`) LIKE '%$part%') OR (LOWER(`billing_email`) LIKE '%$part%') OR (LOWER(`shipping_firstname`) LIKE '%$part%') OR (LOWER(`shipping_lastname`) LIKE '%$part%') OR (LOWER(`shipping_email`) LIKE '%$part%')";
            }
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // ORDERS
    $orders = array();

    // BY BOOKING DATE
    if ($_REQUEST['date_type'] == 'booking') {

        // GET BOOKINGS
        $bookings = $_uccms_ecomm->stc->getBookings(strtotime($date_from), strtotime($date_to), array());

        // HAVE BOOKINGS
        if (count($bookings) > 0) {

            $i = 0;

            // LOOP
            foreach ($bookings as $booking) {

                $bwa = $wa;

                $bwa[] = "`id`=" .$booking['order_id'];

                $where = "WHERE (" .implode(") AND (", $bwa). ")";

                // GET ORDER INFO
                $order_query = "
                SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "`
                " .$where. "
                ";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                // ORDER FOUND
                if ($order['id']) {
                    $orders[$order['id']] = $order;
                }

            }

        }

    // BY ORDER
    } else {

        // DATE - FROM
        if ($date_from) {
            $wa[] = "`dt`>='" .date('Y-m-d 00:00:00', strtotime($date_from)). "'";
        }

        // DATE - TO
        if ($date_to) {
            $wa[] = "`dt`<='" .date('Y-m-d 23:59:59', strtotime($date_to)). "'";
        }

        // HAVE WHERE VALUES
        if (count($wa) > 0) {
            $where = "WHERE (" .implode(") AND (", $wa). ")";
        } else {
            $where = "";
        }

        // GET PAGED ORDERS
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` " .$where. " ORDER BY `dt` DESC LIMIT " .(($page - 1) * $limit). "," .$limit;

        $order_q = sqlquery($order_query);

        // NUMBER OF PAGED ORDERS
        //$num_orders = sqlrows($order_q);

        // TOTAL NUMBER OF ORDERS
        $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ecomm->tables['orders']. "` " .$where));

        // NUMBER OF PAGES
        $pages = $_uccms_ecomm->pageCount($total_results['total']);

        // LOOP
        while ($order = sqlfetch($order_q)) {
            $orders[$order['id']] = $order;
        }

    }

    // NUMBER OF ORDERS
    $num_orders = count($orders);

    // HAVE ORDERS
    if ($num_orders > 0) {

        // LISTING FILE
        $listing_file = dirname(__FILE__). '/get-page_listings/custom.php';
        if (!file_exists($listing_file)) {
            $listing_file = dirname(__FILE__). '/get-page_listings/' .$_uccms_ecomm->storeType(). '.php';
        }

        // LOOP
        foreach ($orders as $order) {

            // GET ITEMS
            $oitems = $_uccms_ecomm->cartItems($order['id']);

            // LISTING OUTPUT
            include($listing_file);

            unset($oitems);

        }

    // NO ORDERS
    } else {
        ?>
        <li style="text-align: center;">
            No matching <?php if ($_REQUEST['quote']) { echo 'quotes'; } else { echo 'orders'; } ?>.
        </li>
        <?php
    }

    ?>

    <script>
        <?php if ($pages > 0) { ?>
            $('#view_paging').show();
            BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
        <?php } else { ?>
            $('#view_paging').hide();
        <?php } ?>
    </script>

    <?php

}

?>