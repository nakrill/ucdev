<li class="item contain">
    <section class="order_id">
        <?php echo $order['id']; ?>
    </section>
    <section class="order_dt">
        <?php echo date('n/j/Y g:i A T', strtotime($order['dt'])); ?>
    </section>
    <section class="order_customer">
        <?php if (($order['billing_firstname']) || ($order['billing_lastname'])) { echo trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])); } else { echo '&nbsp'; } ?>
    </section>
    <section class="order_items">
        <?php echo number_format(count($oitems), 0); ?>
    </section>
    <section class="order_total">
        $<?php echo number_format($order['order_total'], 2); ?>
    </section>
    <section class="order_status">
        <span class="status-bg-<?php echo $order['status']; ?>" style="padding: 3px 5px; border-radius: 2px;"><?php echo ucwords($order['status']); ?></span>
    </section>
    <section class="order_edit">
        <a class="icon_edit" title="View / Edit" href="<?=$base_url?>/edit/?id=<?php echo $order['id']; ?>"></a>
    </section>
    <section class="order_delete">
        <?php if ($_uccms_ecomm->adminModulePermission() == 'p') { ?>
            <a href="<?=$base_url?>/delete/?id=<?php echo $order['id']; ?>" class="icon_delete" title="Delete" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
        <?php } ?>
    </section>
</li>