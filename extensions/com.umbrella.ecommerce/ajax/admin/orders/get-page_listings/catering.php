<?php

// GET EXTRA INFO
$extra_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_catering']. "` WHERE (`order_id`=" .$order['id']. ")";
$extra_q = sqlquery($extra_query);
$order['extra'] = sqlfetch($extra_q);

?>

<li class="item contain">
    <section class="order_id">
        <?php echo $order['id']; ?>
    </section>
    <section class="catering_event">
        <div class="larger">
            <?php echo stripslashes($order['extra']['event_location1']); ?>
        </div>
        <div class="smaller">
            Guests: <?php echo number_format($order['extra']['event_num_people'], 0); ?>
        </div>
    </section>
    <section class="catering_contact">
        <div class="larger">
            <?php echo trim(stripslashes($order['extra']['contact_firstname']. ' ' .$order['extra']['contact_lastname'])); ?>
        </div>
        <div class="smaller">
            <?php echo stripslashes($order['extra']['contact_company']); ?>
        </div>
    </section>
    <section class="catering_dt">
        <div class="larger">
            <?php if (($order['extra']['event_date']) && ($order['extra']['event_date'] != '0000-00-00')) { echo date('M j, Y', strtotime($order['extra']['event_date'])); } ?>
        </div>
        <div class="smaller">
            <?php echo date('g:i a', strtotime(stripslashes($order['extra']['service_time']))); ?>
        </div>
    </section>
    <section class="catering_location">
        <div class="larger">
            <?php echo stripslashes($order['extra']['event_address1']); ?>
        </div>
        <div class="smaller">
            <?php echo stripslashes($order['extra']['event_city']); ?>
        </div>
    </section>
    <section class="order_status">
        <span class="status-bg-<?php echo $order['status']; ?>" style="padding: 3px 5px; border-radius: 2px;"><?php echo ucwords($order['status']); ?></span>
    </section>
    <section class="order_edit">
        <a class="icon_edit" title="View / Edit" href="<?=$base_url?>/edit/?id=<?php echo $order['id']; ?>"></a>
    </section>
    <section class="order_delete">
        <?php if ($_uccms_ecomm->adminModulePermission() == 'p') { ?>
            <a href="<?=$base_url?>/delete/?id=<?php echo $order['id']; ?>" class="icon_delete" title="Delete" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');"></a>
        <?php } ?>
    </section>
</li>