<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    if ($_REQUEST['order_id']) {
        $order_id = (int)$_REQUEST['order_id'];
    } else {
        $order_id = $_uccms_ecomm->cartID();
    }

    // HAVE REQUIRED INFO
    if (($order_id) && (isset($_REQUEST['customer_id']))) {

        $columns = array();

        $customer_id = (int)$_REQUEST['customer_id'];

        // HAVE CUSTOMER ID
        if ($customer_id) {

            // GET PAST ORDER
            $past_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .$customer_id. ") AND (`status`!='cart') ORDER BY `dt` DESC LIMIT 1";
            $past_q = sqlquery($past_query);
            $past = sqlfetch($past_q);

            //$out['past'] = $past;

            // FOUND
            if ($past['id']) {

                // UPDATE FIELDS
                $columns = array(
                    'billing_firstname'     => $past['billing_firstname'],
                    'billing_lastname'      => $past['billing_lastname'],
                    'billing_company'       => $past['billing_company'],
                    'billing_address1'      => $past['billing_address1'],
                    'billing_address2'      => $past['billing_address2'],
                    'billing_city'          => $past['billing_city'],
                    'billing_state'         => $past['billing_state'],
                    'billing_zip'           => $past['billing_zip'],
                    'billing_country'       => $past['billing_country'],
                    'billing_phone'         => $past['billing_phone'],
                    'billing_email'         => $past['billing_email'],
                    'shipping_same'         => (int)$past['shipping_same'],
                    'shipping_firstname'    => $past['shipping_firstname'],
                    'shipping_lastname'     => $past['shipping_lastname'],
                    'shipping_company'      => $past['shipping_company'],
                    'shipping_address1'     => $past['shipping_address1'],
                    'shipping_address2'     => $past['shipping_address2'],
                    'shipping_city'         => $past['shipping_city'],
                    'shipping_state'        => $past['shipping_state'],
                    'shipping_zip'          => $past['shipping_zip'],
                    'shipping_country'      => $past['shipping_country'],
                    'shipping_phone'        => $past['shipping_phone'],
                    'shipping_email'        => $past['shipping_email'],
                );

            }

        }

        $columns['customer_id'] = $customer_id;

        // UPDATE CUSTOMER ID
        $update_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order_id. ")";
        if (sqlquery($update_query)) {
            $out['success'] = true;
        } else {
            $out['error'] = 'Failed to assign customer to order.';
        }

    } else {
        $out['error'] = 'Required information missing.';
    }

}

echo json_encode($out);

?>