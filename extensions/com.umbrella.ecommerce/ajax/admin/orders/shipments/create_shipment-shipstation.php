<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // FORM SUBMITTED
    if (is_array($_POST)) {

        // CLEAN UP
        $order_id = (int)$_POST['order_id'];

        // HAVE ORDER ID
        if ($order_id) {

            // HAVE REQUIRED FIELDS
            if (($_POST['carrier']) && ($_POST['service'])) {

                // LOAD REQUIRED LIBRARIES
                require_once(SERVER_ROOT. 'uccms/includes/libs/unirest/Unirest.php');
                require_once(EXTENSION_ROOT. 'classes/shipstation.php');

                // DISABLE UNIREST SSL VALIDATION
                Unirest::verifyPeer(false);

                // INITIALIZE SHIPSTATION
                $shipStation = new Shipstation();
                $shipStation->setSsApiKey($_uccms_ecomm->getSetting('shipstation_api_key')); // make setting in admin
                $shipStation->setSsApiSecret($_uccms_ecomm->getSetting('shipstation_api_secret'));

                // LABEL VARS
                $label_vars = array(
                    'carrierCode' => $_POST['carrier'],
                    'serviceCode' => $_POST['service'][$_POST['carrier']],
                    'packageCode' => 'package',
                    //'confirmation' => 'delivery',
                    'shipDate' => date('Y-m-d', strtotime($_POST['ship_date'])),
                    'weight' => array(
                        'value' => (float)$_POST['weight'],
                        'units' => 'pounds'
                    ),
                    'dimensions' => array(
                        'units' => 'inches',
                        'length' => $_POST['size_l'],
                        'width' => $_POST['size_w'],
                        'height' => $_POST['size_h']
                    ),
                    'shipFrom' => array(
                        'name' => trim($_POST['from_firstname']. ' ' .$_POST['from_lastname']),
                        'company' => $_POST['from_company'],
                        'street1' => $_POST['from_address1'],
                        'street2' => $_POST['from_address2'],
                        'street3' => null,
                        'city' => $_POST['from_city'],
                        'state' => $_POST['from_state'],
                        'postalCode' => $_POST['from_zip'],
                        'country' => 'US',
                        'phone' => null,
                        'residential' => null
                    ),
                    'shipTo' => array(
                        'name' => trim($_POST['to_firstname']. ' ' .$_POST['to_lastname']),
                        'company' => $_POST['to_company'],
                        'street1' => $_POST['to_address1'],
                        'street2' => $_POST['to_address2'],
                        'street3' => null,
                        'city' => $_POST['to_city'],
                        'state' => $_POST['to_state'],
                        'postalCode' => $_POST['to_zip'],
                        'country' => 'US',
                        'phone' => null,
                        'residential' => null
                    ),
                    'insuranceOptions' => null,
                    'internationalOptions' => null,
                    'advancedOptions' => array(
                        'warehouseId'   => null
                    ),
                    'testLabel' => false
                );

                //$out['label_vars'] = $label_vars;

                // GET LABEL
                $label = $shipStation->createLabel($label_vars);

                // $out['label'] = $label;

                // CREATING LABEL SUCCESS
                if ($label['success']) {

                    // DB COLUMNS
                    $columns = array(
                        'order_id'          => $order_id,
                        'carrier'           => $_POST['carrier'],
                        'service'           => $_POST['service'][$_POST['carrier']],
                        'tracking'          => $label['response']['trackingNumber'],
                        '3rdparty'          => 'shipstation',
                        '3rdparty_id'       => $label['response']['shipmentId'],
                        'ship_date'         => ($_POST['ship_date'] ? date('Y-m-d') : '0000-00-00'),
                        'cost'              => number_format($label['response']['shipmentCost'], 2),
                        'insurance_cost'    => number_format($label['response']['insuranceCost'], 2),
                        'confirmation'      => $label['response']['confirmation'],
                        'warehouse_id'      => (int)$label['response']['warehouseId'],
                        'label_data'        => $label['response']['labelData']
                    );

                    // INSERT INTO DB
                    $insert_query = "INSERT INTO `" .$_uccms_ecomm->tables['order_shipments']. "` SET " .uccms_createSet($columns). ", `dt_created`=NOW()";
                    //$out['query'] = $insert_query;
                    if (sqlquery($insert_query)) {

                        $out['success'] = true;
                        $out['shipment_id'] = sqlid();

                        // UPDATE STATUS
                        $query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='" .trim(strtolower($_POST['status'])). "' WHERE (`id`=" .$order_id. ")";
                        sqlquery($query);

                        // SENDING EMAIL TO CUSTOMER
                        if ($_POST['email']) {

                            // GET ORDER INFO
                            $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$order_id. ")";
                            $order_q = sqlquery($order_query);
                            $order = sqlfetch($order_q);

                            // ORDER FOUND
                            if ($order['id']) {

                                // LINK TO VIEW QUOTE ONLINE
                                //$link = '<a href="' .WWW_ROOT . $_uccms_ecomm->storePath(). '/quote/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']). '" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">View Quote</a>';

                                // EMAIL SETTINGS
                                $esettings = array(
                                    'template'      => 'order_shipped.html',
                                    'subject'       => 'Order Shipped',
                                    'order_id'      => $order['id'],
                                    'from_name'     => stripslashes($_uccms_ecomm->getSetting('email_from_name')),
                                    'from_email'    => stripslashes($_uccms_ecomm->getSetting('email_from_email')),
                                    'to_name'       => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                                    'to_email'      => stripslashes($order['billing_email']),
                                    'sent_by'       => $_uccms_ecomm->adminID()
                                );

                                // EMAIL VARIABLES
                                $evars = array(
                                    'date'                  => date('n/j/Y'),
                                    'time'                  => date('g:i A T'),
                                    'order_id'              => $order['id'],
                                    'order_hash'            => $order['hash'],
                                    'carrier'               => strtoupper($_POST['carrier']),
                                    'tracking'              => $label['response']['trackingNumber'],
                                    'tracking_link'         => $_uccms_ecomm->trackingURL($_POST['carrier'], $label['response']['trackingNumber'])
                                    //'quote_link'            => $link
                                );

                                // SEND EMAIL
                                $eresult = $_uccms_ecomm->sendEmail($esettings, $evars);

                            }

                        }

                    // FAILED
                    } else {
                        $out['error'] = 'Failed to record shipment information.';
                    }

                // ERROR
                } else {
                    $out['error'] = $label['error']['message'];
                }

            // MISSING REQUIRED FIELDS
            } else {
                $out['error'] = 'Missing carrier and/or service.';
            }

        // NO ORDER ID
        } else {
            $out['error'] = 'Order ID not specified.';
        }

    } else {
        $out['error'] = 'No information submitted.';
    }

}

echo json_encode($out);

?>