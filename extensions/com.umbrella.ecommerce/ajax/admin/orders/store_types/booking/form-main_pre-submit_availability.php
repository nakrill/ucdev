<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    //echo print_r($_REQUEST);

    $order_id = (int)$_REQUEST['id'];

    // HAVE CART ID
    if ($order_id) {

        // INITIALIZE CART
        $cart = $_uccms_ecomm->initCart(false, $order_id, true);

        // IS CART / PLACED AND CHANGING TO CHARGED / COMPLETE
        if ((($cart['status'] == 'cart') || ($cart['status'] == 'placed')) && (($_REQUEST['order']['status'] == 'charged') || ($_REQUEST['order']['status'] == 'processing') || ($_REQUEST['order']['status'] == 'shipped') || ($_REQUEST['order']['status'] == 'complete'))) {

            // GET CART ITEMS
            $citems = $_uccms_ecomm->cartItems($cart['id']);

            // NUMBER OF ITEMS
            $num_items = count($citems);

            // HAVE ITEMS IN CART
            if ($num_items > 0) {

                $allow = $_REQUEST['booking_conflict']['allow'];
                if (!is_array($allow)) $allow = array();

                $itema = array();

                $num_conflict = 0;

                // LOOP
                foreach ($citems as $oitem) {

                    // NOT ALLOWING
                    if (!in_array($oitem['id'], $allow)) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // GET AVAILABILITY
                        $avail = $_uccms_ecomm->stc->getAvailability(strtotime($booking['dt_from']), strtotime($booking['dt_to']), array(
                            'item_id'       => $item['id'],
                            'duration_id'   => $booking['duration_id'],
                            'ignore_orders' => [$order_id]
                        ));

                        // ADD TO ITEMS
                        $itema[$oitem['id']] = array(
                            'oitem'     => $oitem,
                            'item'      => $item,
                            'booking'   => $booking,
                            'conflict'  => array()
                        );


                        // not this $order_id


                        // NONE AVAILABLE
                        if ($avail['available'] < $oitem['quantity']) {
                            $itema[$oitem['id']]['conflict'] = $avail;
                            $num_conflict++;
                        }

                    }

                }

                // HAVE CONFLICT
                if ($num_conflict > 0) {

                    ?>

                    <style type="text/css">

                        #booking-conflict .items_heading {
                            padding-top: 10px;
                        }

                        #booking-conflict .items_heading .item_this, #booking-conflict .items_heading .item_other, #booking-conflict .items_heading .actions {
                            float: left;
                        }
                        #booking-conflict .items_heading .item_this, #booking-conflict .items_heading .item_other {
                            width: 40%;
                            margin: 0;
                            padding: 0 15px;
                        }
                        #booking-conflict .items_heading .item_this {
                            margin-right: 15px;
                            padding-left: 0px;
                        }
                        #booking-conflict .items_heading .actions {
                            float: right;
                            text-align: right;
                        }

                        #booking-conflict .items .item {
                            position: relative;
                            margin: 0 0 15px 0;
                            padding: 15px 0;
                            border-bottom: 1px solid #ccc;
                        }
                        #booking-conflict .items .item:last-child {
                            margin-bottom: 0px;
                            border-bottom: 0px none;
                        }
                        #booking-conflict .items .item_this, #booking-conflict .items .item_other, #booking-conflict .items .actions {
                            float: left;
                        }
                        #booking-conflict .items .item_this, #booking-conflict .items .item_other {
                            position: relative;
                            width: 40%;
                            margin: 0;
                            padding: 15px;
                            background-color: #f5f5f5;
                        }
                        #booking-conflict .items .item_this {
                            margin-right: 15px;
                        }
                        #booking-conflict .items .item .person-name {
                            margin-bottom: 10px;
                            font-size: 1.1em;
                        }
                        #booking-conflict .items .item .cart > img {
                            float: left;
                            max-width: 60px;
                            height: auto;
                            margin-right: 10px;
                        }
                        #booking-conflict .items .item .cart .persons, #booking-conflict .items .item .cart .remove {
                            display: none;
                        }
                        #booking-conflict .items .item .cart .actions .edit {
                            position: absolute;
                            top: 15px;
                            right: 15px;
                            font-size: 1.4em;
                        }
                        #booking-conflict .items .item .cart .actions .delete {
                            display: none;
                        }
                        #booking-conflict .items .item_actions {
                            float: right;
                            text-align: right;
                        }
                        #booking-conflict .items .item_actions .action.allow {
                            margin-top: 50px;
                        }
                        #booking-conflict .items .item_actions .action.allow .checkbox {
                            margin-top: -2px;
                        }

                    </style>

                    <script type="text/javascript">

                        $(document).ready(function() {

                            BigTreeCustomControls('#booking-conflict');

                            $('#booking-conflict .items .item_this .cart .actions .edit').click(function(e) {
                                e.preventDefault();
                                var $cart = $(this).closest('.cart');
                                $('#cart #item-996 .actions .edit').click();

                            });

                            $('#booking-conflict .items .item_other .cart .actions .edit').click(function(e) {
                                e.preventDefault();
                                var $cart = $(this).closest('.cart');
                                window.open('?id=' +$cart.attr('data-order-id'));
                            });

                        });

                    </script>

                    <p><?php if ($num_conflict == 1) { ?>There is 1 item<?php } else { ?>There are <?php echo $num_conflict; ?> items<?php } ?> in this cart with a booking scheduling conflict.</p>

                    <div class="items_heading clearfix">
                        <div class="item_this">
                            This booking
                        </div>
                        <div class="item_other">
                            The other booking
                        </div>
                        <div class="actions">
                        </div>
                    </div>

                    <div class="items">

                        <?php

                        // LOOP THROUGH ITEMS
                        foreach ($itema as $citem) {

                            $oitem      = $citem['oitem'];
                            $item       = $citem['item'];
                            $booking    = $citem['booking'];
                            $conflict   = $citem['conflict'];

                            // GET ORDER INFO
                            $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$oitem['cart_id']. ")";
                            $order_q = sqlquery($order_query);
                            $order = sqlfetch($order_q);

                            ?>

                            <div class="item clearfix">

                                <div class="item_this">

                                    <div class="person-name">
                                        <?php echo trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])); ?>
                                    </div>

                                    <div class="cart clearfix" data-order-id="<?php echo $oitem['cart_id']; ?>" data-oitem-id="<?php echo $oitem['id']; ?>">

                                        <?php

                                        // SETTINGS FOR DISPLAYING ITEM IN CART
                                        $item_cart_settings = array(
                                            'hide_price' => true
                                        );

                                        // DISPLAY ITEM IN CART
                                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                                        unset($oitem, $item, $booking);

                                        ?>

                                    </div>

                                </div>

                                <div class="item_other">

                                    <?php

                                    // HAVE CONFLICT BOOKINGS
                                    if (count($conflict['bookings']) > 0) {

                                        // LOOP
                                        foreach ($conflict['bookings'] as $cbooking) {

                                            $oitem = array(
                                                'id'        => $cbooking['oitem_id'],
                                                'item_id'   => $cbooking['item_id']
                                            );

                                            // GET ITEM INFO
                                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                            // GET ORDER INFO
                                            $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$cbooking['order_id']. ")";
                                            $order_q = sqlquery($order_query);
                                            $order = sqlfetch($order_q);

                                            ?>

                                            <div class="person-name">
                                                <?php echo trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])); ?>
                                            </div>

                                            <div class="cart clearfix" data-order-id="<?php echo $cbooking['order_id']; ?>" data-oitem-id="<?php echo $cbooking['oitem_id']; ?>">

                                                <?php

                                                // SETTINGS FOR DISPLAYING ITEM IN CART
                                                $item_cart_settings = array(
                                                    'hide_price'    => true,
                                                    //'edit_link'     => '?id=' .$cbooking['order_id']
                                                );

                                                // DISPLAY ITEM IN CART
                                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                                                unset($oitem, $item, $booking);

                                                ?>

                                            </div>

                                            <?php

                                        }

                                    }

                                    ?>

                                </div>

                                <div class="item_actions">
                                    <div class="action allow">
                                        <input type="checkbox" name="booking_conflict[allow][]" value="<?php echo $citem['oitem']['id']; ?>" /> Allow
                                    </div>
                                </div>

                            </div>

                        <?php } ?>

                    </div>

                    <?php

                }

            }

        }

    }

}

?>