<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $query          = isset($_GET["query"]) ? $_GET["query"] : "";
    $page           = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
    $category_id    = (int)$_GET['category_id'];

    //$_uccms_ecomm->setPerPage(5);

    // WHERE ARRAY
    $wa[] = "i.deleted=0";

    // CATEGORY SPECIFIED
    if ($category_id > 0) {
        $inner_join = "INNER JOIN `" .$_uccms_ecomm->tables['item_categories']. "` AS `icr` ON i.id=icr.item_id";
        $wa[] = "icr.category_id=" .$category_id;
    } else {
        $inner_join = "";
    }

    // IS SEARCHING
    if ($query) {
        $qparts = explode(" ", $query);
        $twa = array();
        foreach ($qparts as $part) {
            $part = sqlescape(strtolower($part));
            $twa[] = "(LOWER(i.title) LIKE '%$part%')";
        }
        $wa[] = "(" .implode(" OR ", $twa). ")";
    }

    // GET PAGED ITEMS
    $item_query = "
    SELECT i.*
    FROM `" .$_uccms_ecomm->tables['items']. "` AS `i`
    " .$inner_join. "
    WHERE (" .implode(") AND (", $wa). ")
    ORDER BY i.title ASC, i.id ASC
    LIMIT " .(($page - 1) * $_uccms_ecomm->perPage()). "," .$_uccms_ecomm->perPage();
    $item_q = sqlquery($item_query);

    // NUMBER OF PAGED ITEMS
    $num_items = sqlrows($item_q);

    // TOTAL NUMBER OF ITEMS
    $total_results = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` FROM `" .$_uccms_ecomm->tables['items']. "` AS `i` " .$inner_join. " WHERE (" .implode(") AND (", $wa). ")"));

    // NUMBER OF PAGES
    $pages = $_uccms_ecomm->pageCount($total_results['total']);

    // HAVE ITEMS
    if ($num_items > 0) {

        // LOOP
        while ($item = sqlfetch($item_q)) {

            ?>

            <li class="item">
                <section class="item_title">
                    <a href="./edit/?id=<?php echo $item['id']; ?>"><?php echo stripslashes($item['title']); ?></a>
                </section>
                <section class="item_sku">
                    <?php echo stripslashes($item['sku']); ?>
                </section>
                <section class="item_price">
                    $<?php echo stripslashes($item['price']); ?>
                </section>
                <section class="item_status status_<?php if ($item['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                    <?php if ($item['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                </section>
                <section class="item_edit">
                    <a class="icon_edit" title="Edit Category" href="./edit/?id=<?php echo $item['id']; ?>"></a>
                </section>
                <section class="item_delete">
                    <?php if ($_uccms_ecomm->adminModulePermission() == 'p') { ?>
                        <a href="./delete/?id=<?php echo $item['id']; ?>" class="icon_delete" title="Delete Item" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
                    <?php } ?>
                </section>
            </li>

            <?php

        }

    // NO ITEMS
    } else {
        ?>
        <li style="text-align: center;">
            No items.
        </li>
        <?php
    }

    ?>

    <script>
	    BigTree.setPageCount("#view_paging" ,<?=$pages?>, <?=$page?>);
    </script>

    <?php

}

?>