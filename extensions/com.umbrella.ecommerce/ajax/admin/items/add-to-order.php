<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    //file_put_contents('add-to-order.txt', print_r($_POST, true));

    // HAVE REQUIRED INFO
    if (($_REQUEST['order_id']) && ($_REQUEST['item_id'])) {

        // GET ORDER
        $order = $_uccms_ecomm->getOrder((int)$_REQUEST['order_id']);

        $_REQUEST['item']['id'] = $_REQUEST['item_id'];

        // IS EDITING
        if ($_REQUEST['order_item_id']) {
            $_REQUEST['ciid'] = $_REQUEST['order_item_id'];
        }

        // ADD ITEM TO CART
        $result = $_uccms_ecomm->cartAddItem($order['id'], $_REQUEST, true);

        $out['success'] = $result['success'];

        $out['item'] = $result['item'];

        if ($result['success']) {

            $out['message'] = $result['message'];

            $out['order_id'] = $order['id'];
            $out['oitem_id'] = $result['oitem_id'];

            // RECORD ADMIN ID
            $sql = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `created_by`=" .$admin->ID. " WHERE (`id`=" .$order['id']. ")";
            sqlquery($sql);

            // IF CHANGING STATUS
            if ($_REQUEST['order_status']) {

                $sql = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='" .sqlescape($_REQUEST['order_status']). "', `quote`=" .(int)$_REQUEST['order_quote']. " WHERE (`id`=" .$order['id']. ")";
                sqlquery($sql);

            }

            // SEE IF WE HAVE AN EXTRAS RECORD YET
            $extra_sql = "SELECT `id` FROM `" .$_uccms_ecomm->tables['quote_' .$_uccms_ecomm->storeType()]. "` WHERE (`order_id`=" .$out['order_id']. ")";
            $extra_query = sqlquery($extra_sql);
            if (sqlrows($extra_query) == 0) {

                // CREATE RECORD
                $extra_sql = "INSERT INTO `" .$_uccms_ecomm->tables['quote_' .$_uccms_ecomm->storeType()]. "` SET `order_id`=" .$out['order_id']. "";
                sqlquery($extra_sql);

            }

            // STORE TYPE IS BOOKING
            if ($_uccms_ecomm->storeType() == 'booking') {

                // HAVE PERSON ARRAY
                if (count((array)$_POST['person']) > 0) {

                    /*
                    // MAKEUP ITEMS
                    $mcitems = array();

                    // GET CART ITEMS
                    $citems = $_uccms_ecomm->cartItems($_uccms_ecomm->cartID());

                    // HAVE ITEMS
                    if (count($citems) > 0) {

                        // LOOP THROUGH ITEMS
                        foreach ($citems as $oitem) {

                            // IS MAKEUP
                            if ($oitem['item_id'] == 1) {
                                $mcitems[$oitem['id']] = $oitem;
                            }

                        }

                    }
                    */

                    //echo print_r($_POST);
                    //echo print_r($mcitems);
                    //exit;

                    $mii = 0;
                    $markup_vals = array();

                    // LOOP THROUGH SUBMITTED ITEMS
                    foreach ($_POST['person'] as $oitem_id => $persons) {

                        // NEW ITEM
                        if (!$_REQUEST['order_item_id']) {
                            $oitem_id = $result['oitem_id'];
                        }

                        $oitem_id = (int)$oitem_id;

                        // HAVE PERSONS FOR CART ITEM
                        if (count($persons) > 0) {

                            // LOOP THROUGH PERSONS
                            foreach ($persons as $person_id => $person) {

                                $create = false;

                                // DB COLUMNS
                                $columns = array(
                                    'firstname' => $person['firstname'],
                                    'lastname'  => $person['lastname'],
                                    'dob'       => ($person['dob'] ? date('Y-m-d', strtotime($person['dob'])) : '0000-00-00'),
                                    'email'     => $person['email'],
                                    'phone'     => $person['phone'],
                                    'notes'     => $person['notes']
                                );

                                /*
                                // IS MAKEUP ITEM
                                if ($mcitems[$oitem_id]) {

                                    // IS FIRST
                                    if ($mii == 0) {
                                        $markup_vals[$person_id] = $columns;

                                    // ALL OTHERS
                                    } else {
                                        $columns = $markup_vals[$person_id];
                                    }

                                }
                                */

                                // IS NEW
                                if (substr($person_id, 0, 2) == 'n-') {
                                    $create = true;

                                // IS EXISTING
                                } else {

                                    $person_id = (int)$person_id;

                                    // SEE IF WE ALREADY HAVE PERSON'S RECORD
                                    $query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` WHERE (`id`=" .$person_id. ") AND (`oitem_id`=" .$oitem_id. ")";
                                    $q = sqlquery($query);

                                    // HAVE REDCORD
                                    if (sqlrows($q) > 0) {

                                        // UPDATE
                                        $query = "UPDATE `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$person_id. ")";
                                        sqlquery($query);

                                    // NO RECORD
                                    } else {
                                        $create = true;
                                    }

                                }

                                // IS CREATING
                                if ($create) {

                                    $columns['oitem_id'] = $oitem_id;

                                    // CREATE
                                    $query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` SET " .$_uccms_ecomm->createSet($columns). "";
                                    sqlquery($query);

                                }

                            }

                        // NO PERSONS FOR ORDER
                        } else {

                            // DELETE
                            $query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` WHERE (`oitem_id`=" .$oitem_id. ")";
                            sqlquery($query);

                        }

                        /*
                        // IS MAKEUP ITEM
                        if ($mcitems[$oitem_id]) {
                            $mii++;
                        }
                        */

                    }

                }

            }

            // RECALCULATE ORDER
            $_uccms_ecomm->orderRecalculate($out['order_id']);

        } else {
            $out['err'] = $result['error'];
        }

    } else {
        $out['err'] = 'Required information missing.';
    }

}

echo json_encode($out);

?>