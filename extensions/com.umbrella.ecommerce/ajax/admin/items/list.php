<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // GET ITEMS
    $items = $_uccms_ecomm->getItems([
        'vars' => $_REQUEST,
        //'limit' => 1
    ]);

    ?>

    <?php if (count((array)$items['results']) > 0) { ?>

        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th class="title" scope="col">Name</th>
                    <th class="sku" scope="col">SKU</th>
                    <th class="price" scope="col">Price</th>
                    <th class="status" scope="col">Status</th>
                    <th class="type" scope="col">Type</th>
                    <th class="options" scope="col">Options</th>
                </tr>
            </thead>
            <tbody class="items">

                <?php foreach ($items['results'] as $item) { ?>

                    <tr class="item" data-id="<?php echo $item['id']; ?>">
                        <td class="title">
                            <a href="./edit/?id=<?php echo $item['id']; ?>"><?php echo stripslashes($item['title']); ?></a>
                        </td>
                        <td class="sku">
                            <?php echo stripslashes($item['sku']); ?>
                        </td>
                        <td class="price">
                            <?php echo ($item['price'] ? '$' .number_format($item['price'], 2) : ''); ?>
                        </td>
                        <td class="status">
                            <span class="badge" style="background-color: <?php echo $_uccms_ecomm->itemStatuses($item['active'])['color']; ?>;"><?php echo $_uccms_ecomm->itemStatuses($item['active'])['title']; ?></span>
                        </td>
                        <td class="type">
                            <?php echo $_uccms_ecomm->itemTypes($item['type'])['title']; ?>
                        </td>
                        <td class="options">
                            <a href="./edit/?id=<?php echo $item['id']; ?>" class="edit" title="Edit" data-id="<?php echo $item['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                            <a href="#" class="delete" title="Delete"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>

                <?php } ?>

            </tbody>
        </table>

    <?php } else { ?>

        <div class="no-results">
            No items found.
        </div>

    <?php } ?>

    <?php

}

?>

<script>
    BigTree.setPageCount('#items .summary .view_paging' ,<?php echo (int)$items['pages']; ?>, <?php echo (int)$items['page']; ?>);
</script>