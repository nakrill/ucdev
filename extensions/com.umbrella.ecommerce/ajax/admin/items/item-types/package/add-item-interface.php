<?php

$_uccms_ecomm = new uccms_Ecommerce();

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    $id = (int)$_REQUEST['id'];
    $item_id = (int)$_REQUEST['item_id'];

    // PACKAGE ID SPECIFIED
    if ($id) {

        $pa = array(0); // PACKAGE ITEMS
        $aia = array(); // ALL ITEMS ARRAY

        // GET PACKAGE ITEMS
        $pitems_query = "SELECT `id`, `item_id` FROM `" .$_uccms_ecomm->tables['item_package_items']. "` WHERE (`package_id`=" .$id. ")";
        $pitems_q = sqlquery($pitems_query);
        while ($pitem = sqlfetch($pitems_q)) {
            $pa[$pitem['item_id']] = $pitem['item_id'];
        }

        // GET ALL ITEMS
        $aitems_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0) AND (`id` NOT IN (" .implode(',', $pa). ")) ORDER BY `title` ASC, `id` ASC";
        $aitems_q = sqlquery($aitems_query);
        while ($aitem = sqlfetch($aitems_q)) {

            // GET IMAGE
            $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$aitem['id']. ") ORDER BY `sort` ASC LIMIT 1";
            $image = sqlfetch(sqlquery($image_query));

            // HAVE IMAGE
            if ($image['image']) {
                $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
            } else {
                $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
            }

            // ADD TO ITEM ARRAY
            $aia[] = '{
                id: ' .$aitem['id']. ',
                image: \'' .$image_url. '\',
                text: \'' .str_replace("'", "\'", stripslashes($aitem['title'])). '\'
            }';

            // SELECTED ITEM
            if ($aitem['id'] == $item_id) {
                $selected_data = '{
                    id: ' .$aitem['id']. ',
                    image: \'' .$image_url. '\',
                    text: \'' .str_replace("'", "\'", stripslashes($aitem['title'])). '\'
                }';
            }

        }

        ?>

        <script type="text/javascript">
            $(document).ready(function() {

                // SELECT PRODUCT
                $('#package-items .add_container .select_product').selectivity({
                    allowClear: true,
                    items:[<?php echo implode(', ', $aia); ?>],
                    <?php if ($selected_data) { ?>
                        data: <?php echo $selected_data; ?>,
                    <?php } ?>
                    placeholder: 'Select item',
                    allowClear: true,
                    templates: {
                        resultItem: function(item) {
                            return (
                            '<div class="selectivity-result-item" data-item-id="' +item.id+ '">' +
                                '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                            '</div>'
                            );
                        },
                        singleSelectedItem: function(item) {
                            return (
                                '<span class="selectivity-single-selected-item" data-item-id="' +item.id+ '">' +
                                    '<img src="' +item.image+ '" alt="" />&nbsp;' +item.text+
                                '</span>'
                            );
                        }
                    }
                }).on('selectivity-selected', function(e) {
                    $('#package-items .add_container input[name="new[id]"]').val(e.id);
                    if (e.id) {
                        $.get('<?=ADMIN_ROOT?>*/<?=$_uccms_ecomm->Extension?>/ajax/admin/items/item-types/package/add-item-interface/', {
                            'id': <?php echo $id; ?>,
                            'item_id': e.id
                        }, function(data) {
                            $('#package-items .add_container').html(data).show();
                        }, 'html');
                        //$('#package-items .add_container .selectivity-single-select').height(58);
                        //$('#package-items .add_container input[type="submit"]').show();
                    } else {
                        $('#package-items .add_container .selectivity-single-select').height(34);
                        $('#package-items .add_container input[type="submit"]').hide();
                    }
                });

                <?php if ($selected_data) { ?>
                    $('#package-items .add_container .selectivity-single-select').height(58);
                <?php } ?>

            });

        </script>

        <input type="hidden" name="new[id]" value="<?php echo $item_id; ?>" />

        <div class="select_product"></div>

        <?php

        // ITEM SELECTED
        if ($item_id) {

            // GET ITEM
            //$titem = $_uccms_ecomm->getItem($item_id);

            // GET ITEM VARIANTS
            $va = $_uccms_ecomm->item_getVariants($item_id);

            // HAVE VARIANTS
            if (count($va) > 0) {

                // LOCATIONS ARRAY
                $la = array();

                // GET LOCATIONS
                $loc_query = "SELECT * FROM `" .$_uccms_ecomm->tables['locations']. "`";
                $loc_q = sqlquery($loc_query);
                while ($loc = sqlfetch($loc_q)) {
                    $la[$loc['id']] = $loc;
                }

                if (count($la) == 0) $la[0] = array('id' => 0);

                // NUBMER OF VARIANTS
                $nv = 0;

                ?>

                <div style="margin-top: 10px;">
                    <span style="font-weight: bold;">Variant:</span>
                    <br />
                    <select name="new[variant_id]">
                        <option value="">Let customer choose</option>
                        <option value="" disabled="disabled">--------------------------</option>
                        <?php
                        foreach ($la as $location) {
                            if ($location['id'] != 0) {
                                ?>
                                <optgroup label="<?php echo stripslashes($location['title']); ?>">
                                <?php
                            }
                            foreach ($va[$location['id']] as $var) {
                                $vdata = json_decode(stripslashes($var['data']), true);
                                ?>
                                <option value="<?php echo $var['id']; ?>"><?php echo stripslashes($var['title']); ?></option>
                                <?php
                                $nv++;
                            }
                            if ($location['id'] != 0) {
                                ?>
                                </optgroup>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>

                <?php

            }

            ?>

            <div style="margin-top: 10px;">
                <span style="font-weight: bold;">Quantity:</span>
                <br />
                <input type="text" name="new[quantity]" value="1" style="width: 60px;" />
            </div>

            <input class="blue" type="submit" value="Add" style="margin-top: 10px;" />

            <?php

        }

    // NO PACKAGE ID
    } else {
        echo 'Package ID not specified.';
    }

}

?>