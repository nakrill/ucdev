<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $id         = (int)$_REQUEST['id'];
    $order_id   = (int)$_REQUEST['order_id'];

    // HAVE REQUIRED INFO
    if (($id) && ($order_id)) {

        // REMOVE ITEM
        $result = $_uccms_ecomm->cartRemoveItem($id, $order_id);

        $out = $result;

        // SUCCESS
        if ($result['success']) {

            $out['success'] = true;

            /*
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-remove'] = trim(stripslashes($result['item']['title']). ' (#' .$result['item']['id']. ')');
            }
            */

            // RECALCULATE ORDER
            $_uccms_ecomm->orderRecalculate($order_id);

        // FAILED
        } else {

            $out['error'] = ($result['message'] || 'Failed to remove item from cart.');

            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-remove-error'] = $out['error'];
            }

        }

    // MISSING REQUIRED INFO
    } else {
        $out['error'] = 'Missing info to remove item.';
    }

}

echo json_encode($out);

?>