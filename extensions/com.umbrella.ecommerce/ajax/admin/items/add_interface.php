<?php

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // CLEAN UP
    $order_id       = (int)$_REQUEST['order_id'];
    $item_id        = (int)$_REQUEST['item_id']; // NEW ITEM
    $order_item_id  = (int)$_REQUEST['order_item_id']; // EXISTING ITEM

    // HAVE ORDER ID
    if ($order_id) {

        // GET ORDER INFO
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$order_id. ")";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // ORDER FOUND
        if ($order['id']) {

            // HAVE EXISTING ITEM ID
            if ($order_item_id) {

                // GET ITEM INFO FROM ORDER
                $oitem_query = "SELECT * FROM `" .$_uccms_ecomm->tables['order_items']. "` WHERE (`id`=" .$order_item_id. ")";
                $oitem_q = sqlquery($oitem_query);
                $oitem = sqlfetch($oitem_q);

                // ORDER ITEM FOUND
                if ($oitem['id']) {
                    $item_id = $oitem['item_id'];
                } else {
                    $err = 'Item not found.';
                }

            }

            // HAVE ITEM ID
            if ($item_id) {

                // GET ITEM
                $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item_id. ") AND (`deleted`=0)";
                $item_q = sqlquery($item_query);
                $item = sqlfetch($item_q);

                // ITEM FOUND
                if ($item['id']) {

                    // ON SALE
                    if ($item['price_sale'] != '0.00') {
                        $sale = true;
                        $price = $item['price_sale'];

                    // NOT ON SALE
                    } else {
                        $sale = false;
                        $price = $item['price'];
                    }

                    // ATTRIBUTES ARRAY
                    $attra = $_uccms_ecomm->itemAttributes($item['id']);

                    // SELECTED ATTRIBUTES
                    $selattra = json_decode(stripslashes($oitem['options']), true);

                    ?>

                    <style type="text/css">

                        form.bigtree_dialog_form .data fieldset {
                            width: 100%;
                        }
                        form.bigtree_dialog_form .data fieldset label {
                            margin-bottom: 5px;
                        }
                        form.bigtree_dialog_form .data fieldset label.title {
                            font-weight: bold;
                        }
                        form.bigtree_dialog_form .data fieldset label.description {
                            font-size: .9em;
                            opacity: .7;
                        }
                        form.bigtree_dialog_form .data fieldset label.limit {
                            display: none;
                        }
                        form.bigtree_dialog_form .data fieldset input[type="text"] {
                            width: 180px;
                        }
                        .bigtree_dialog_form textarea {
                            width: 180px;
                            height: 80px;
                        }
                        form.bigtree_dialog_form .data fieldset .icon_small {
                            float: left;
                            margin: 8px 0 0 -25px;
                        }
                        form.bigtree_dialog_form .data fieldset .options .option {
                            padding-bottom: 3px;
                            line-height: 1.3em;
                        }
                        form.bigtree_dialog_form .data fieldset .options .option::after {
                            clear: both;
                            content: "";
                            display: table;
                        }
                        form.bigtree_dialog_form .data .attribute .options.select_box .option {
                            float: left;
                            margin: 0 5px 5px 0;
                            padding: 0px;
                            border: 1px solid #aaa;
                            text-align: center;
                            cursor: pointer;
                        }
                        form.bigtree_dialog_form .data .attribute .options.select_box .option .border2 {
                            padding: 6px 10px;
                        }
                        form.bigtree_dialog_form .data .attribute .options.select_box .option.selected {
                            border-color: #777;
                        }
                        form.bigtree_dialog_form .data .attribute .options.select_box .option.selected .border2 {
                            padding: 5px 9px;
                            border: 1px solid #777;
                            background-color: rgba(0, 0, 0, .05);
                        }
                        form.bigtree_dialog_form .data .attribute .options.select_box .option .markup {
                            font-size: .8em;
                            opacity: .5;
                        }
                        form.bigtree_dialog_form .data .attribute.people .option {
                            margin-top: 10px;
                            padding: 0px;
                        }
                        form.bigtree_dialog_form .data .attribute.people .option label {
                            color: #333;
                        }
                        form.bigtree_dialog_form .data .attribute.people .option button {
                            /*display: none;*/
                            padding: 6px;
                        }
                        form.bigtree_dialog_form .data .attribute.people .option input[type="text"] {
                            display: inline-block;
                            width: 30px;
                        }
                        form.bigtree_dialog_form .data .attribute.people .option .markup {
                            opacity: .8;
                        }

                        form.bigtree_dialog_form .data fieldset.category ul {
                            list-style: none;
                        }
                        form.bigtree_dialog_form .data fieldset.category > ul {
                            margin-left: 0px;
                        }
                        #ui-datepicker-div {
                            z-index: 9000 !important;
                        }

                    </style>

                    <script type="text/javascript">

                        $(document).ready(function() {

                            // SELECT BOX CLICK
                            $('form.bigtree_dialog_form .data .attribute .options.select_box .option').click(function(e) {
                                e.preventDefault();
                                var parent = $(this).closest('.options.select_box');
                                parent.find('input[type="hidden"]').val($(this).attr('data-value'));
                                parent.find('.option').removeClass('selected');
                                $(this).addClass('selected');
                            });

                        });

                    </script>

                    <input type="hidden" name="order_id" value="<?php echo $_REQUEST['order_id']; ?>" />
                    <input type="hidden" name="order_item_id" value="<?php echo $oitem['id']; ?>" />
                    <input type="hidden" name="item_id" value="<?php echo $item['id']; ?>" />

                    <?php if ($_REQUEST['quote']) { ?>
                        <input type="hidden" name="order_quote" value="1" />
                        <input type="hidden" name="order_status" value="preparing" />
                    <?php } ?>

                    <?php

                    // IS EDITING ITEM IN ORDER/QUOTE
                    if ($oitem['id']) {

                        // GET ITEM IMAGE
                        $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$oitem['id']. ") ORDER BY `sort` ASC LIMIT 1";
                        $image = sqlfetch(sqlquery($image_query));

                        // HAVE IMAGE
                        if ($image['image']) {
                            $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
                        } else {
                            $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
                        }

                        ?>
                        <table border="0" style="border: 0px;">
                            <tr>
                                <td style="padding-top: 0px; padding-bottom: 0px;"><img src="<?=$image_url?>" alt="" style="width: 60px;" /></td>
                                <td style="width: 100%; padding-top: 0px; padding-bottom: 0px; font-size: 20px; font-weight: bold; opacity: .6;"><?=stripslashes($item['title'])?></td>
                            </tr>
                        </table>
                    <?php } ?>

                    <table width="100%" style="margin: 0px; border: 0px;">
                        <tr>

                            <td width="50%" valign="top" style="padding-bottom: 0px;">

                                <?php

                                // ARRAY OF CATEGORIES
                                $cata = array();

                                // GET CURRENT RELATIONS
                                $query = "SELECT `category_id` FROM `" .$_uccms_ecomm->tables['item_categories']. "` WHERE (`item_id`=" .$item['id']. ")";
                                $q = sqlquery($query);
                                while ($row = sqlfetch($q)) {
                                    $cata[$row['category_id']] = $row['category_id'];
                                }

                                // OUTPUT LIST
                                function this_listElements($array) {
                                    global $cata, $oitem;
                                    if (count($array) > 0) {
                                        echo '<ul>';
                                        foreach ($array as $element) {
                                            if ($cata[$element['id']]) {
                                                if ($element['id'] == $oitem['category_id']) {
                                                    $checked = 'checked="checked"';
                                                } else {
                                                    $checked = '';
                                                }
                                                echo '<li><input type="radio" name="item[category_id]" value="' .$element['id']. '" ' .$checked. ' /> ' .stripslashes($element['title']);
                                            }
                                            if (!empty($element['children'])) {
                                                this_listElements($element['children']);
                                            }
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                    }
                                }

                                ?>

                                <fieldset class="category">
                                    <label>Category <small>(for tax)</small></label>
                                    <?php echo this_listElements($_uccms_ecomm->getCategoryTree()); ?>
                                </fieldset>

                                <?php

                                // QUANTITY
                                $quantity = ($oitem['quantity'] ? $oitem['quantity'] : 1);

                                // HAVE EXTRA INFO
                                if (stripslashes($oitem['extra'])) {
                                    $extra = json_decode(stripslashes($oitem['extra']), true);
                                } else {
                                    $extra = array();
                                }

                                // STORE TYPE FIELDS
                                include(dirname(__FILE__). '/add_interface-forms/' .$_uccms_ecomm->storeType(). '.php');

                                // NO QUANTITY SET BY STORE TYPE
                                if ($quantity === null) {
                                    $quantity = ($oitem['quantity'] ? $oitem['quantity'] : 1);
                                }

                                ?>

                                <fieldset>
                                    <label>Quantity</label>
                                    <input type="text" name="item[quantity]" value="<?=$quantity?>" />
                                </fieldset>

                                <fieldset>
                                    <label>Price</label>
                                    $ <input type="text" name="override_price" value="<?php if ($oitem['override_price']) { echo number_format($_uccms_ecomm->toFloat($oitem['override_price']), 2); } ?>" placeholder="<?=number_format($_uccms_ecomm->toFloat($item['price']), 2)?>" style="display: inline-block; width: 168px;" />
                                </fieldset>

                            </td>

                            <td width="50%" valign="top" style="padding-bottom: 0px;">

                                <div class="attributes">
                                    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_attributes.php'); ?>
                                </div>

                            </td>

                        </tr>
                    </table>

                    <?php

                    // BOOKING ITEM
                    if ($item['type'] == 2) {

                        $booking = array();

                        // IS EDITING ITEM IN ORDER/QUOTE
                        if ($oitem['id']) {

                            // GET BOOKING INFO
                            $booking_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$oitem['id']. ")";
                            $booking_q = sqlquery($booking_query);
                            $booking = sqlfetch($booking_q);

                        }

                        ?>

                        <style type="text/css">

                            .bigtree_dialog_form .data td {
                                padding: 0px;
                            }

                            .bigtree_dialog_form .booking_controls {
                                /*padding: 0 25px 0 0;*/
                            }
                            .bigtree_dialog_form .booking_controls .persons {
                                clear: both;
                                padding-top: 10px;
                            }
                            .bigtree_dialog_form .booking_controls .persons h5 {
                                margin: 0 0 5px 0;
                                padding: 0px;
                                text-transform: uppercase;
                            }
                            .bigtree_dialog_form .booking_controls .persons .person {
                                margin-top: 10px;
                                padding: 8px 12px;
                            }
                            .bigtree_dialog_form .booking_controls .persons .person:nth-child(odd) {
                                background-color: rgba(0, 0, 0, .05);
                            }
                            .bigtree_dialog_form .booking_controls .persons .person .label {
                                margin-bottom: 3px;
                            }
                            .bigtree_dialog_form .booking_controls .persons .person .input {
                                float: left;
                                margin: 0 15px 5px 0;
                            }
                            .bigtree_dialog_form .booking_controls .persons .person .input input {
                                width: 180px;
                            }

                            #ecommContainer {
                                float: left;
                                width: 100%;
                            }
                            #ecommContainer .item_container .booking .date-picker-wrapper.inline-wrapper {
                                width: 496px;
                            }
                            #ecommContainer .item_container .booking .duration .options .option:hover {
                                border-color: #aaa !important;
                            }
                            #ecommContainer .item_container .booking .duration .options .option.selected {
                                border-color: #222 !important;
                                background-color: #eee !important;
                                color: #000;
                            }

                        </style>

                        <div class="booking_controls">

                            <h3>Booking</h3>

                            <script src="/js/lib/moment.js"></script>

                            <div id="ecommContainer">

                                <div class="item_container">

                                    <div class="booking">

                                        <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/store_types/booking/item_booking.php'); ?>

                                    </div>

                                </div>

                            </div>

                            <div class="persons">

                                <h5>Persons</h5>

                                <div class="people">

                                    <?php

                                    // GET BOOKING PERSONS
                                    $persons = $_uccms_ecomm->stc->bookingPersons($oitem['id']);

                                    // QUANTITY
                                    $qty = (int)$oitem['quantity'];

                                    // LOOP THROUGH QUANTITY
                                    for ($i=0; $i<=$qty - 1; $i++) {

                                        $person = array_values($persons)[$i];
                                        if (!is_array($person)) $person = array();

                                        if (!$person['id']) {
                                            $person['id'] = 'n-' .($i + 1);
                                        }

                                        if ($i == 0) {

                                            if (!$person['firstname']) $person['firstname'] = stripslashes($cart['firstname']);
                                            if (!$person['lastname']) $person['lastname'] = stripslashes($cart['lastname']);

                                            if (!$person['firstname']) $person['firstname'] = stripslashes($account['firstname']);
                                            if (!$person['lastname']) $person['lastname'] = stripslashes($account['lastname']);

                                        }

                                        $person['dob'] = '';
                                        if (($person['dob']) && ($person['dob'] != '0000-00-00')) {
                                            $person['dob'] = date('n/j/Y', strtotime($person['dob']));
                                        }

                                        ?>
                                        <div class="person clearfix">
                                            <div class="label">
                                                Person <?php echo $i + 1; ?>
                                            </div>
                                            <div class="input firstname">
                                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][firstname]" placeholder="First Name" value="<?php echo stripslashes($person['firstname']); ?>" class="form-control" />
                                            </div>
                                            <div class="input lastname">
                                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][lastname]" placeholder="Last Name" value="<?php echo stripslashes($person['lastname']); ?>" class="form-control" />
                                            </div>
                                            <div class="input email">
                                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][email]" placeholder="Email" value="<?php echo stripslashes($person['email']); ?>" class="form-control" />
                                            </div>
                                            <div class="input phone">
                                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][phone]" placeholder="Phone" value="<?php echo stripslashes($person['phone']); ?>" class="form-control" />
                                            </div>
                                            <div class="input dob">
                                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][dob]" placeholder="DOB (mm/dd/yyyy)" value="<?php echo $person['dob']; ?>" />
                                            </div>
                                            <div class="input notes">
                                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][notes]" placeholder="Notes" value="<?php echo stripslashes($person['notes']); ?>" />
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    ?>

                                </div>

                            </div>

                        </div>

                        <?php

                    }

                    ?>

                    <script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/jquery-ui-timepicker-addon.js"></script>
                    <script type="text/javascript">
                        $('.date_picker').datepicker({ dateFormat: 'mm/dd/yy', duration: 200, showAnim: 'slideDown' });
                        $('.time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
                        BigTreeCustomControls($('form.bigtree_dialog_form .data'));
                    </script>

                    <?php

                // ITEM NOT FOUND
                } else {
                    $err = 'Item not found.';
                }

            // NO ITEM ID
            } else {
                $err = 'Item ID not specified.';
            }

        // ORDER NOT FOUND
        } else {
            $err = 'Order/Quote not found.';
        }

    // NO ORDER ID
    } else {
        $err = 'Order/Quote ID not specified.';
    }

    // HAVE ERROR
    if ($err) {
        ?>
        <div class="err">
            <?php echo $err; ?>
        </div>
        <?php
    }

}

?>