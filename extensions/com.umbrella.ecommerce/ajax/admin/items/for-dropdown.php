<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // GET ITEMS
    $item_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`deleted`=0) ORDER BY `title` ASC, `id` ASC";
    $item_q = sqlquery($item_query);
    while ($item = sqlfetch($item_q)) {

        // GET IMAGE
        $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC LIMIT 1";
        $image = sqlfetch(sqlquery($image_query));

        // HAVE IMAGE
        if ($image['image']) {
            $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
        } else {
            $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
        }

        $out[] = array(
            'id'    => $item['id'],
            'image' => $image_url,
            'text'  => str_replace("'", "\'", stripslashes($item['title']))
        );

    }

}

echo json_encode($out);

?>