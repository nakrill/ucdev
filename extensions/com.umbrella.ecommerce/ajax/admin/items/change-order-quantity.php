<?php

$out = array();

// MODULE CLASS
if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce;

// HAS ACCESS
if ($_uccms_ecomm->adminModulePermission()) {

    // HAVE REQUIRED INFO
    if (($_REQUEST['id']) && ($_REQUEST['quantity'])) {

        // CHANGE QUANTITY
        $result = $_uccms_ecomm->cartItemQuantity($_REQUEST['id'], $_REQUEST['quantity'], $_REQUEST['order_id']);

        $out = $result;

        if ($result['success']) {

            /*
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-quantity-change'] = trim(stripslashes($result['item']['title']). ' (#' .$result['item']['id']. ') - ' .(int)$_REQUEST['quantity']);
            }
            */

            // RECALCULATE ORDER
            $_uccms_ecomm->orderRecalculate($result['order_id']);

        } else {

            $out['err'] = $result['message'];

            /*
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-quantity-error'] = $result['message'];
            }
            */

        }

    // REQUIRED INFO MISSING
    } else {
        $out['err'] = 'Required information missing.';
    }

}

echo json_encode($out);

?>