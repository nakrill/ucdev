<?php

// GET EXTRA ORDER INFO
$oextra_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_catering']. "` WHERE (`order_id`=" .$order['id']. ")";
$oextra_q = sqlquery($oextra_query);
$oextra = sqlfetch($oextra_q);

// EVENT DATE
if ($extra['date']) {
    $date = date('m/d/Y', strtotime($extra['date']));
} else {
    if (($oextra['event_date']) && ($oextra['event_date'] != '0000-00-00')) {
        $date = date('m/d/Y', strtotime($oextra['event_date']));
    } else {
        $date = date('m/d/Y');
    }
}

// CHANGE QUANTITY
if ((!$oitem['quantity']) && ($oextra['event_num_people'] > 0)) {
    $quantity = $oextra['event_num_people'];
}

?>

<fieldset>
    <label>Date</label>
    <div class="contain">
        <?php

        // SET DEFAULT DATE FORMAT IF NONE SPECIFIED
        if (!$bigtree["config"]["date_format"]) {
            $bigtree["config"]["date_format"] = 'm/d/Y';
        }

        // FIELD VALUES
        $field = array(
            'id'        => 'extra_date',
            'key'       => 'extra[date]',
            'value'     => $date,
            'required'  => true,
            'options'   => array(
                'default_today' => true
            )
        );

        // INCLUDE FIELD
        include(BigTree::path('admin/form-field-types/draw/date.php'));

        ?>
    </div>
</fieldset>

<fieldset>
    <label>Meal Time</label>
    <select name="extra[mealtime]">
        <?php foreach ($_uccms_ecomm->stc->mealTimes() as $meal_id => $meal) { ?>
            <option value="<?=$meal_id?>" <?php if ($meal_id == $extra['mealtime']) { ?>selected="selected"<?php } ?>><?=stripslashes($meal['title'])?> (<?=date('g:i a', strtotime($meal['time']))?>)</option>
        <?php } ?>
    </select>
</fieldset>