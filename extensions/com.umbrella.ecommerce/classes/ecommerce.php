<?
    class uccms_Ecommerce extends BigTreeModule {

        var $Table = "";
        //var $Module = "90001";

        var $Extension = "com.umbrella.ecommerce";

        var $storePath = '';

        // DB TABLES
        var $tables = array(
            'accounts'                      => 'uccms_accounts',
            'account_details'               => 'uccms_accounts_details',
            'assets'                        => 'uccms_ecommerce_assets',
            'attributes'                    => 'uccms_ecommerce_attributes',
            'attribute_groups'              => 'uccms_ecommerce_attribute_groups',
            'booking_bookings'              => 'uccms_ecommerce_booking_bookings',
            'booking_bookings_persons'      => 'uccms_ecommerce_booking_bookings_persons',
            'booking_calendars'             => 'uccms_ecommerce_booking_calendars',
            'booking_calendar_blackouts'    => 'uccms_ecommerce_booking_calendar_blackouts',
            'booking_calendar_days'         => 'uccms_ecommerce_booking_calendar_days',
            'booking_item_durations'        => 'uccms_ecommerce_booking_item_durations',
            'booking_items'                 => 'uccms_ecommerce_booking_items',
            'categories'                    => 'uccms_ecommerce_categories',
            'category_pricegroups'          => 'uccms_ecommerce_category_pricegroup_rel',
            'coupons'                       => 'uccms_ecommerce_coupons',
            'customers'                     => 'uccms_ecommerce_customers',
            'customer_contacts'             => 'uccms_ecommerce_customers_contacts',
            'email_log'                     => 'uccms_ecommerce_email_log',
            'inventory_log'                 => 'uccms_ecommerce_inventory_log',
            'items'                         => 'uccms_ecommerce_items',
            'item_attributes'               => 'uccms_ecommerce_item_attributes_rel',
            'item_categories'               => 'uccms_ecommerce_item_categories_rel',
            'item_images'                   => 'uccms_ecommerce_item_images',
            'item_package_items'            => 'uccms_ecommerce_item_package_items',
            'item_pricegroups'              => 'uccms_ecommerce_item_pricegroup_rel',
            'item_variants'                 => 'uccms_ecommerce_item_variants',
            'item_videos'                   => 'uccms_ecommerce_item_videos',
            'locations'                     => 'uccms_ecommerce_locations',
            'orders'                        => 'uccms_ecommerce_orders',
            'orders_rel'                    => 'uccms_ecommerce_orders_rel',
            'order_items'                   => 'uccms_ecommerce_order_items',
            'order_shipments'               => 'uccms_ecommerce_order_shipments',
            'payment_methods'               => 'uccms_ecommerce_settings_payment_methods',
            'payouts'                       => 'uccms_ecommerce_payouts',
            'payout_orders'                 => 'uccms_ecommerce_payout_orders',
            'quote_general'                 => 'uccms_ecommerce_quote_details_general',
            'quote_catering'                => 'uccms_ecommerce_quote_details_catering',
            'quote_booking'                 => 'uccms_ecommerce_quote_details_booking',
            'quote_message_log'             => 'uccms_ecommerce_quote_message_log',
            'search_log'                    => 'uccms_ecommerce_search_log',
            'settings'                      => 'uccms_ecommerce_settings',
            'scheduled_payments'            => 'uccms_ecommerce_scheduled_payments',
            'shipping_methods'              => 'uccms_ecommerce_settings_shipping_methods',
            'shipping_services'             => 'uccms_ecommerce_settings_shipping_services',
            'shipping_table'                => 'uccms_ecommerce_settings_shipping_table',
            'tax_groups'                    => 'uccms_ecommerce_settings_tax_groups',
            'transaction_log'               => 'uccms_ecommerce_transaction_log'
        );

        var $settings, $stc, $misc;

        // START AS 0, WILL ADJUST LATER
        static $PerPage = 0;


        function __construct() {
            include_once(dirname(__FILE__). '/store-types/' .$this->storeType(). '.php');
            $this->stc = new uccms_StoreType();
        }


        #############################
        # GENERAL
        #############################

        // STORE TYPE
        public function storeType() {
            if ($this->settings['store_type']) {
                return $this->settings['store_type'];
            } else {
                $type = $this->getSetting('store_type');
                if (!$type) $type = 'general';
                return $type;
            }
        }


        // STORE PATH
        public function storePath() {
            if (!$this->storePath) {
                $base = stripslashes(sqlfetch(sqlquery("SELECT `path` FROM `bigtree_pages` WHERE (`template`='" .$this->Extension. "*shop')"))['path']);
                if (!$base) $base = 'shop';
                $this->storePath = $base;
            }
            return $this->storePath;
        }


        // SET STORE PATH
        public function storePath_set($val) {
            $this->storePath = preg_replace('/[^\w-_\/]/', '', $val);
        }


        // MODULE ID
        public function moduleID() {
            if (!isset($this->settings['module_id'])) {
                $this->settings['module_id'] = (int)sqlfetch(sqlquery("SELECT * FROM `bigtree_modules` WHERE (`extension`='" .$this->Extension. "')"))['id'];
            }
            return $this->settings['module_id'];
        }


        // ADMIN USER ID
        public function adminID() {
            return $_SESSION['bigtree_admin']['id'];
        }


        // ADMIN ACCESS LEVEL
        public function adminModulePermission() {
            global $admin;
            if (!isset($this->settings['admin_module_permission'])) {
                $this->settings['admin_module_permission'] = $admin->getAccessLevel($this->moduleID());
            }
            return $this->settings['admin_module_permission'];
        }


        // STRING IS JSON
        public function isJson($string) {
            if (substr($string, 0, 2) == '{"') {
                return true;
            }
        }


        // PREPARE ARRAY FOR DB INSERTION
        public function createSet($columns='') {
            $set = array();
            if (is_array($columns)) {
                foreach ($columns as $name => $value) {
                    $value = trim($value);
                    $value = str_replace('*/*', '', $value);
                    if (substr($value, 0, 1) == '"') $value = substr($value, 1, strlen($value));
                    if (substr($value, -1, 1) == '"') $value = substr($value, 0, strlen($value) - 1);
                    $value = sqlescape($value);
                    $set[] = "`" .$name. "`='" .$value. "'";
                }
                $set = implode(', ', $set);
                return $set;
            }
        }


        // CONVERTS A STRING INTO A REWRITE-ABLE URL
        public function makeRewrite($string) {

            $string = strtolower(trim($string));

            // REMOVE ALL QUOTES
            $string = str_replace("'", '', $string);
            $string = str_replace('"', '', $string);

            // STRIP ALL NON WORD CHARS
            $string = preg_replace('/\W/', ' ', $string);

            // REPLACE ALL WHITE SPACE SECTIONS WITH A DASH
            $string = preg_replace('/\ +/', '-', $string);

            // TRIM DASHES
            $string = preg_replace('/\-$/', '', $string);
            $string = preg_replace('/^\-/', '', $string);

            return $string;

        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON INCOMING (UPLOADED) IMAGES
        public function imageBridgeIn($string) {
            $string = (string)trim(str_replace('{staticroot}extensions/' .$this->Extension. '/files/', '', $string));
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $dpa = explode('/', $string);
                return array_pop($dpa);
                //$what = array_pop($dpa);
            }
        }


        // MANAGE BIGTREE IMAGE LOCATION INFO ON OUTGOING (FROM DB) IMAGES
        public function imageBridgeOut($string, $what, $file=false) {
            $string = stripslashes($string);
            // IS URL (CLOUD STORAGE / EXTERNAL IMAGE)
            if (substr($string, 0, 3) == 'http') {
                return $string;
            } else {
                $base = ($file ? SITE_ROOT : STATIC_ROOT);
                return $base. 'extensions/' .$this->Extension. '/files/' .$what. '/' .$string;
            }
        }


        // CONVERT BIGTREE FILE LOCATION TO FULL URL
        public function bigtreeFileURL($file) {
            return str_replace(array("{wwwroot}", WWW_ROOT, "{staticroot}", STATIC_ROOT), STATIC_ROOT, $file);
        }


        // GET AND OUTPUT A LAYOUT ELEMENT
        public function layoutElement($what, $admin=false) {
            extract($GLOBALS);
            include(SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/routed/shop/elements/' .$what. '.php');
        }


        // INCLUDE A PROCESSING HOOK FILE
        public function includeHook($name, $vars=array()) {

        }


        // CLEAN A PHONE NUMBER (TO JUST NUMBERS)
        public function cleanPhone($number) {
            return trim(preg_replace('/\D/', '', $number));
        }


        // FORMAT PHONE NUMBER
        public function prettyPhone($number) {
            $number = preg_replace('/\D/', '', $number);
            if ($number) {
                return '(' .substr($number, 0, 3). ') ' .substr($number, 3, 3). '-' .substr($number, 6);
            }
        }


        // CHECK REQUIRED FIELD
        public function checkRequired($value='', $what='') {

            // DEFAULT TO FAIL
            $pass = false;

            // CLEAN UP
            $value = trim($value);

            // HAVE VALUE
            if ($value) {

                if (!$what) $what = 'general';

                // WHAT KIND OF DATA
                switch ($what) {

                    // TEXT - GENERAL
                    case 'general':
                        $pass = true;
                        break;

                    // TEXT - EMAIL
                    case 'email':
                        if (preg_match('/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/', $value)) {
                            $pass = true;
                        }
                        break;

                    // TEXT - WEBSITE
                    case 'url':
                        if (preg_match('/\w\.[a-zA-Z]{2}/', $value)) {
                            $pass = true;
                        }
                        break;

                    // NUMBER - GENERAL
                    case 'number':
                        if (is_numeric($value)) {
                            $pass = true;
                        }
                        break;

                    // NUMBER - DATE
                    case 'date':
                        if (strtotime($value)) {
                            $value = str_replace(' ', '/', str_replace('-', '/', $value));
                            if (strtotime($value)) {
                                $pass = true;
                            }
                        }
                        break;

                    // TEXT - ZIPCODE
                    case 'zip':
                        if (preg_match('/^[0-9]{5,5}$/', $value)) {
                            $pass = true;
                        }
                        break;

                    // NUMBER - PHONE
                    case 'phone':
                        $value = preg_replace('/[^\+0-9(x|ext)]/', '', $value);
                        if (preg_match('/^\+?[0-9]*(?:(x|ext)[0-9]*)?$/', $value)) {
                            $pass = true;
                        }
                        break;

                }

            }

            return $pass;

        }


        // SEND AN EMAIL
        public function sendEmail($settings=array(), $vars=array()) {
            global $cms;

            $out = array();

            // HAVE WHAT WE NEED
            if (($settings['template']) && ($settings['to_email']) && ($settings['subject'])) {

                // FROM NAME & EMAIL
                if (!$settings['from_name']) $settings['from_name'] = stripslashes($this->getSetting('email_from_name'));
                if (!$settings['from_name']) $settings['from_name'] = stripslashes($cms->getSetting('site_name'));
                if (!$settings['from_email']) $settings['from_email'] = stripslashes($this->getSetting('email_from_email'));

                // NEW EMAIL SERVICE
                $es = new BigTreeEmailService;

                // TEMPLATE
                $template_custom = SERVER_ROOT. 'extensions/' .$this->Extension. '/email/custom/' .$settings['template'];
                if (file_exists($template_custom)) {
                    $template = $template_custom;
                } else {
                    $template_normal = SERVER_ROOT. 'extensions/' .$this->Extension. '/email/' .$settings['template'];
                    if (file_exists($template_normal)) {
                        $template = $template_normal;
                    }
                }

                // TEMPLATE EXISTS
                if ($template) {

                    // GET BODY OF EMAIL
                    $body = file_get_contents($template);

                    // HAVE PHONE NUMBER
                    if ($this->getSetting('store_phone')) $vars['store_contact_phone'] = '<div style="margin-bottom: 10px;">Questions? Call us at ' .stripslashes($this->getSetting('store_phone')). '</div>';

                    // HAVE VARS
                    if (count($vars) > 0) {

                        // LOOP
                        foreach ($vars as $name => $value) {

                            // DO REPLACING
                            $body = str_ireplace('{' .$name. '}', $value, $body);

                        }

                    }

                    // SEND EMAIL
                    $out['result'] = $es->sendEmail($settings['subject'], $body, $settings['to_email'], $settings['from_email'], $settings['from_name']);

                    // LOG EMAIL
                    $out['log_id'] = $this->logEmail(array(
                        'order_id'      => $settings['order_id'],
                        'from_email'    => $settings['from_email'],
                        'from_name'     => $settings['from_name'],
                        'to_email'      => $settings['to_email'],
                        'to_name'       => $settings['to_name'],
                        'subject'       => $settings['subject'],
                        'content'       => $body,
                        'sent_by'       => $settings['sent_by'],
                        'error'         => $out['result']['err']
                    ));

                // TEMPLATE DOES NOT EXIST
                } else {
                    $out['err'] = 'Email template not found.';
                }

            // DON'T HAVE WHAT WE NEED
            } else {
                $out['err'] = 'Missing required settings.';
            }

            return $out;

        }


        // LOG EMAIL
        public function logEmail($vars) {
            $columns = array(
                'order_id'      => (int)$vars['order_id'],
                'from_email'    => $vars['from_email'],
                'from_name'     => $vars['from_name'],
                'to_email'      => $vars['to_email'],
                'to_name'       => $vars['to_name'],
                'subject'       => $vars['subject'],
                'content'       => $vars['content'],
                'sent_by'       => (int)$vars['sent_by'],
                'error'         => $vars['error']
            );
            $query = "INSERT INTO `" .$this->tables['email_log']. "` SET " .$this->createSet($columns). ", `dt`=NOW()";
            if (sqlquery($query)) {
                return sqlid();
            } else {
                return 0;
            }
        }


        // LOG QUOTE MESSAGE
        public function logQuoteMessage($vars) {
            $columns = array(
                'order_id'      => (int)$vars['order_id'],
                'by'            => (int)$vars['by'],
                'message'       => $vars['message'],
                'status'        => $vars['status'],
                'email_log_id'  => $vars['email_log_id']
            );
            $query = "INSERT INTO `" .$this->tables['quote_message_log']. "` SET " .$this->createSet($columns). ", `dt`=NOW()";
            if (sqlquery($query)) {
                return true;
            } else {
                return false;
            }
        }


        // SET NUMBER PER PAGE
        public function setPerPage($num) {
            static::$PerPage = (int)$num;
        }


        // NUMBER PER PAGE
        public function perPage() {
            global $cms;
            if (!static::$PerPage) $this->setPerPage($cms->getSetting('bigtree-internal-per-page'));
            if (!static::$PerPage) $this->setPerPage(15);
            return static::$PerPage;
        }


        // GET NUMBER OF PAGES FROM QUERY
        public function pageCount($num, $perpage=null) {
            if ($perpage === null) $perpage = $this->perPage();
            return ceil($num / $perpage);
        }


        // IS PURCHASING ENABLED
        public function purchasingEnabled($item=array()) {
            $pe = true; // PURCHASING ENABLED BY DEFAULT
            if ($item['id']) { // ITEM LEVEL
                if ($item['no_purchase']) $pe = false;
            }
            if (!$this->getSetting('purchasing_enabled')) $pe = false; // GLOBAL LEVEL
            return $pe;
        }


        // ADMIN ORDER/QUOTE LIST COLUMNS
        public function adminOQListColumns($what='') {
            $out = array();

            $cola = array(
                'general' => array(
                    'order_id'          => 'ID',
                    'order_dt'          => 'Time',
                    'order_customer'    => 'Customer',
                    'order_items'       => '# Items',
                    'order_total'       => 'Total',
                    'order_status'      => 'Status',
                    'order_edit'        => 'View',
                    'order_delete'      => 'Delete'
                ),
                'catering' => array(
                    'order_id'          => 'ID',
                    'catering_event'    => 'Event',
                    'catering_contact'  => 'Contact',
                    'catering_dt'       => 'Date/Time',
                    'catering_location' => 'Location',
                    'order_status'      => 'Status',
                    'order_edit'        => 'View',
                    'order_delete'      => 'Delete'
                ),
                'booking' => array(
                    'order_id'          => 'ID',
                    'order_dt'          => 'Time',
                    'order_customer'    => 'Customer',
                    'order_items'       => '# Items',
                    'order_total'       => 'Total',
                    'order_status'      => 'Status',
                    'order_edit'        => 'View',
                    'order_delete'      => 'Delete'
                ),

            );

            if (!$what) {
                if ($this->adminOQListColumns) {
                    return $this->adminOQListColumns;
                } else {
                    $what = $this->storeType();
                }
            }

            return $cola[$what];

        }


        // CREATE ORDER HASH
        public function generateOrderHash($email='') {
            return md5(time() . $email . rand(11111, 99999));
        }


        // GET CUSTOMER ID
        public function customerID($email='') {
            return ip2long($_SERVER['REMOTE_ADDR']) . strlen($email);
        }


        // FORMAT NUMBER FOR DB
        public function dbNumDecimal($num, $decimals=2) {
            return number_format((float)str_replace(',', '', $num), $decimals, '.', '');
        }


        // FORMAT FOR CURRENCY
        public function formatCurrency($amount, $decimals=2) {
            return number_format(round(floatval(str_replace(array('$', ','), '', $amount)), $decimals), $decimals, '.', '');
        }


        // STATUS COLORS
        public function statusColors($status='', $type='order') {
            $sca['quote'] = array(
                'requested'     => '#b2dfdb',
                'preparing'     => '#4db6ac',
                'presented'     => '#009688',
                'declined'      => '#f06292',
                'accepted'      => '#00695c',
                'deleted'       => '#eeeeee'
            );
            $sca['order'] = array(
                'cart'          => '#e8f5e9',
                'placed'        => '#a5d6a7',
                'charged'       => '#4caf50',
                'failed'        => '#ef5350',
                'processing'    => '#bbdefb',
                'shipped'       => '#64b5f6',
                'complete'      => '#42a5f5',
                'cancelled'     => '#e0e0e0',
                'deleted'       => '#eeeeee'
            );
            $out = array_replace_recursive($sca, $this->stc->statusColors());
            if ($status) {
                return $out[$type][$status];
            } else {
                return $out;
            }
        }


        // IMAGE FILE EXTENSIONS
        public function imageFiles() {
            return array(
                'jpg',
                'jpeg',
                'gif',
                'png',
                'tiff'
            );
        }


        // RETURN THE FLOAT VAL
        public function toFloat($str, $decimal=true) {
            $str = floatval(preg_replace("/[^-0-9\.]/", '' , $str));
            if ($decimal) {
                $str = number_format($str, 2, '.', '');
            }
            return $str;
        }


        // DAYS OF WEEK
        public function daysOfWeek() {
            return array(
                7 => array(
                    'code'  => 'SU',
                    'title' => 'Sunday'
                ),
                1 => array(
                    'code'  => 'MO',
                    'title' => 'Monday'
                ),
                2 => array(
                    'code'  => 'TU',
                    'title' => 'Tuesday'
                ),
                3 => array(
                    'code'  => 'WE',
                    'title' => 'Wednesday'
                ),
                4 => array(
                    'code'  => 'TH',
                    'title' => 'Thursday'
                ),
                5 => array(
                    'code'  => 'FR',
                    'title' => 'Friday'
                ),
                6 => array(
                    'code'  => 'SA',
                    'title' => 'Saturday'
                )
            );
        }

        // ARRAY OF DAYS IN WEEK
        public function weekDays($date='') {
            $out = [];
            if (!$date) $date = date('Y-m-d');
            $week = date('W', strtotime($date));
            $year = date('Y', strtotime($date));
            $first = strtotime($year.'W'.$week);
            $out[0] = date('Y-m-d', $first);
            for ($i=1; $i<=6; $i++) {
                $out[$i] = date('Y-m-d', strtotime('+' .$i. ' Day', $first));
            }
            return $out;
        }

        // ARRAY OF HOURS IN DAY
        public function dayHours($date='') {
            $out = [];
            if (!$date) $date = date('Y-m-d');
            $first = strtotime(date('Y-m-d 00:00:00', strtotime($date)));
            $out[0] = date('Y-m-d H:i:s', $first);
            for ($i=1; $i<=23; $i++) {
                $out[$i] = date('Y-m-d H:i:s', strtotime('+' .$i. ' Hour', $first));
            }
            return $out;
        }



        #############################
        # GENERAL - SETTINGS
        #############################

        // GET SETTING
        public function getSetting($id='') {
            $id = sqlescape($id);
            if ($id) {
                if (isset($this->settings[$id])) {
                    return $this->settings[$id];
                } else {
                    $f = sqlfetch(sqlquery("SELECT `value` FROM `" .$this->tables['settings']. "` WHERE `id`='" .$id. "'"));
                    if ($this->isJson($f['value'])) {
                        $val = json_decode($f['value'], true);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$id] = $val;
                }
                return $val;
            }
        }


        // GET SETTINGS
        public function getSettings($array='') {
            $out = array();
            if (is_array($array)) {
                $oa = array();
                foreach ($array as $id) {
                    $id = sqlescape($id);
                    if ($id) {
                        $oa[] = "`id`='" .$id. "'";
                    }
                }
                if (count($oa) > 0) {
                    $or = "(" .implode(") OR (", $oa). ")";
                }
                if ($or) {
                    $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` WHERE " .$or;
                }
            } else {
                $query = "SELECT `id`, `value` FROM `" .$this->tables['settings']. "` ORDER BY `id` ASC";
            }
            if ($query) {
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    if ($this->isJson($string)) {
                        $val = json_decode($f['value']);
                    } else {
                        $val = stripslashes($f['value']);
                    }
                    $this->settings[$f['id']] = $val;
                    $out[$f['id']] = $val;
                }
            }
            return $out;
        }


        // SET SETTING
        public function setSetting($id='', $value='') {

            // CLEAN UP
            $id = sqlescape($id);

            // HAVE ID
            if ($id) {

                // VALUE IS ARRAY
                if (is_array($value)) {
                    $value = json_encode($value);

                // VALUE IS STRING
                } else {
                    $value = sqlescape($value);
                }

                $query = "
                INSERT INTO `" .$this->tables['settings']. "`
                SET `id`='" .$id. "', `value`='" .$value. "', `updated_dt`=NOW(), `updated_by`=" .$this->adminID(). "
                ON DUPLICATE KEY UPDATE `value`='" .$value. "', `updated_dt`=NOW(), `updated_by`=" .$this->adminID(). "
                ";

                if (sqlquery($query)) {
                    return true;
                }

            }

        }


        // RETURNS ARRAY OF PRICE GROUPS
        public function priceGroups() {
            $out = $this->getSetting('pricegroups');
            if (($out) && (!is_array($out))) {
                $out = json_decode($out, true);
            }
            if ((!$out) || ((is_array($out) && (count($out) == 0)))) {
                $out = array(
                    1 => array(
                        'title' => 'Retail'
                    )
                );
            }
            return $out;
        }


        // RETURNS ARRAY OF BLACKOUT REASONS
        public function blackoutReasons() {
            $out = $this->getSetting('blackout_reasons');
            if (($out) && (!is_array($out))) {
                $out = json_decode($out, true);
            }
            if ((!$out) || (!is_array($out))) {
                $out = array();
            }
            return $out;
        }


        #############################
        # SEARCH
        #############################


        // BUILD WHERE ARRAY FOR getItems SEARCH METHOD (BELOW)
        private function giBuildWA($var, $db_field, $type) {

            // IS ARRAY
            if (is_array($var)) {

                // IS STRING
                if ($type == 'str') {

                    // APPLY TO ALL VALS
                    $vals = array_map(function($val) {
                        return sqlescape($val);
                    }, $var);

                    // QUERY STRING
                    $out = "(" .$db_field. "='" .implode("') OR (" .$db_field. "='", $vals). "')";

                // IS STRING - MULTIPLE
                } else if ($type == 'str_multi') {

                    // LOOP THROUGH VALUES
                    foreach ($var as $val) {
                        $pa[] = "(FIND_IN_SET('" .sqlescape($val). "', " .$db_field. "))";
                    }

                    // QUERY STRING
                    $out = "(" .implode(") OR (", $pa). ")";

                // IS NUMBER
                } else if ($type == 'num') {

                    // APPLY TO ALL VALS
                    $vals = array_map(function($val) {
                        return (float)$val;
                    }, $var);

                    // QUERY STRING
                    $out = "(" .$db_field. "=" .implode(") OR (" .$db_field. "=", $vals). ")";

                // IS MIN/MAX
                } else if ($type == 'min_max') {

                    // HAVE MIN AND MAX
                    if (($var[0]) && ($var[1])) {
                        $out = "(" .$db_field. ">=" .(float)$var[0]. ") AND (" .$db_field. "<=" .(int)$var[1]. ")";

                    // HAVE MIN
                    } else if ($var[0]) {
                        $out = "(" .$db_field. ">=" .(float)$var[0]. ")";

                    // HAVE MAX
                    } else if ($var[1]) {
                        $out = "(" .$db_field. "<=" .(float)$var[1]. ")";
                    }

                }

            // IS SINGLE
            } else if (trim($var) !== '') {

                // IS STRING
                if ($type == 'str') {
                    $out = $db_field. " LIKE '%" .sqlescape($var). "%'";

                // IS STRING - MULTIPLE
                } else if ($type == 'str_multi') {
                    $out = "(FIND_IN_SET('" .sqlescape($var). "', " .$db_field. "))";
                    // WHERE (CONCAT(",", `cooling_type_codes`, ",") REGEXP ",(F|W),")

                // IS NUMBER
                } else if ($type == 'num') {
                    $out = $db_field. "=" .(float)$var;

                } else if ($type == 'has') {
                    $out = $db_field. "!=''";
                }

            }

            return $out;

        }


        // GET ITEMS
        public function getItems($vars) {
            global $_uccms;

            $out = array();

            //echo print_r($vars);
            //exit;

            /*
            // STRING
            if ($vars['vars']['slug']) {
                if (is_array($vars['vars']['slug'])) {
                    $vals = array_map(function($val) {
                        return sqlescape($val);
                    }, $vars['vars']['slug']);
                    $wa[] = "(i.slug='" .implode("') OR (i.slug='", $vals). "')";
                } else {
                    $wa[] = "i.slug='" .sqlescape($vars['vars']['slug']). "'";
                }
            }

            // NUMBER
            if ($vars['vars']['id']) {
                if (is_array($vars['vars']['id'])) {
                    $vals = array_map(function($val) {
                        return (float)$val;
                    }, $vars['vars']['id']);
                    $wa[] = "(i.id=" .implode(") OR (i.id=", $vals). ")";
                } else {
                    $wa[] = "i.id=" .(float)$vars['vars']['slug'];
                }
            }
            */

            $wa = array();
            $ja = array();

            // BY ID
            if ($val = $this->giBuildWA($vars['vars']['id'], 'i.id', 'num')) {
                $wa[] = $val;
                $lva['id'] = $vars['vars']['id'];
            }

            // BY TYPE
            if ($val = $this->giBuildWA($vars['vars']['type'], 'i.type', 'num')) {
                $wa[] = $val;
                $lva['type'] = $vars['vars']['type'];
            }

            // BY SLUG
            if ($val = $this->giBuildWA($vars['vars']['slug'], 'i.slug', 'str')) {
                $wa[] = $val;
                $lva['slug'] = $vars['vars']['slug'];
            }

            // BY NAME
            if ($val = $this->giBuildWA($vars['vars']['title'], 'i.title', 'str')) {
                $wa[] = $val;
                $lva['title'] = $vars['vars']['title'];
            }

            // BY PRICE
            if ($val = $this->giBuildWA(array($vars['vars']['price_min'], $vars['vars']['price_max']), 'i.price', 'min_max')) {
                $wa[] = $val;
                $lva['price'] = $vars['vars']['title'];
            }

            // BY RATING
            if ($val = $this->giBuildWA(array($vars['vars']['rating_min'], $vars['vars']['rating_max']), 'i.rating', 'min_max')) {
                $wa[] = $val;
                $lva['name'] = $vars['vars']['name'];
            }

            // BY REVIEWS
            if ($val = $this->giBuildWA(array($vars['vars']['reviews_min'], $vars['vars']['reviews_max']), 'i.reviews', 'min_max')) {
                $wa[] = $val;
                if ($vars['vars']['reviews_min']) $lva['reviews_min'] = $vars['vars']['reviews_min'];
                if ($vars['vars']['reviews_max']) $lva['reviews_min'] = $vars['vars']['reviews_max'];
            }

            // QUERY
            if ($vars['vars']['q']) {
                $keywords = sqlescape($vars['vars']['q']);
                $wa[] = "(i.sku LIKE '%" .$keywords. "%') OR (i.title LIKE '%" .$keywords. "%') OR (i.description_short LIKE '%" .$keywords. "%') OR (i.description LIKE '%" .$keywords. "%')";
            }

            // CATEGORY
            if ($vars['vars']['category']) {
                $ja['item_categories'] = "INNER JOIN `" .$this->tables['item_categories']. "` AS `ic` ON i.id=ic.item_id";
                $ja['categories'] = "INNER JOIN `" .$this->tables['categories']. "` AS `c` ON ic.category_id=c.id";
                $wa[] = "(ic.category_id=" .(int)$vars['vars']['category']. ") AND (c.active=1)";
                $lva['category_id'] = $vars['vars']['category'];
            }

            // ATTRIBUTES
            if (count((array)$vars['vars']['attribute']) > 0) {

                // LOOP THROUGH ATTRIBUTES
                foreach ($vars['vars']['attribute'] as $attribute_id => $value) {

                    if (($attribute_id) && ($value)) {

                        $ja['item_attributes'] = "LEFT JOIN `" .$this->tables['item_attributes']. "` AS `ia` ON i.id=ia.item_id";
                        $ja['attributes'] = "LEFT JOIN `" .$this->tables['attributes']. "` AS `a` ON ia.attribute_id=a.id";

                        $lva['attribute'][$attribute_id] = $value;

                        // VALUE IS ARRAY
                        if (is_array($value)) {

                            // MIN / MAX
                            if (($value['min']) || ($value['max'])) {

                                // HAVE MIN AND MAX
                                if (($value['min']) && ($value['max'])) {
                                    $wa[] = "(ia.attribute_id=" .(int)$attribute_id. ") AND (a.options>=" .(float)$value['min']. ") AND (a.options<=" .(float)$value['max']. ")";

                                // HAVE MIN
                                } else if ($value['min']) {
                                    $wa[] = "(ia.attribute_id=" .(int)$attribute_id. ") AND (a.options>=" .(float)$value['min']. ")";

                                // HAVE MAX
                                } else if ($value['max']) {
                                    $wa[] = "(ia.attribute_id=" .(int)$attribute_id. ") AND (a.options<=" .(float)$value['max']. ")";
                                }

                            // OPTION ID(S)
                            } else {

                                $uvals = array();

                                foreach ($value as $val) {
                                    $val = trim(sqlescape($val));
                                    if ($val) {
                                        $uvals[] = $val;
                                    }
                                }

                                if (count($uvals) > 0) {
                                    $wa[] = "(ia.attribute_id=" .(int)$attribute_id. ") AND (a.options IN ('" .implode("','", $uvals). "'))";
                                }

                            }

                        // SINGLE VALUE
                        } else {
                            $wa[] = "(ia.attribute_id=" .(int)$attribute_id. ") AND (a.options LIKE '%" .sqlescape($value). "%')";
                        }

                    }

                }

            }

            // ACTIVE
            $wa[] = "i.active=1";

            // WHERE
            $where_sql = "(" .implode(") AND (", $wa). ")";

            // JOINS
            $join_sql = implode(" ", $ja);

            $order_direction = strtolower($vars['order_direction']);

            // RANDOM ORDER
            if ($order_direction == 'rand') {

                $order_sql = "RAND()";

            // SPECIFIC ORDER
            } else {

                // ORDER
                switch ($vars['order_field']) {
                    case 'sku':
                        $order_field = "i.sku";
                        break;
                    case 'title':
                        $order_field = "i.title";
                        break;
                    case 'price':
                        $order_field = "i.price";
                        break;
                }

                if (!$order_field) {
                    $order_field = "i.title";
                }

                // ORDER DIRECTION
                if (strtoupper($vars['order_direction']) == 'DESC') {
                    $order_direction = "DESC";
                } else {
                    $order_direction = "ASC";
                }

                // ORDER SQL
                $order_sql = trim($order_field. " " .$order_direction);

            }

            $page = (int)$vars['vars']['page'];
            if (!$page) $page = 1;

            // LIMIT
            if ($vars['limit']) {
                $limit_limit = (int)$vars['limit'];
            } else {
                $limit_limit = $this->perPage();
            }

            // OFFSET
            if ($vars['offset']) {
                $limit_offset = (int)$vars['offset'];
            } else {
                $limit_offset = ($page - 1) * $limit_limit;
            }

            // LIMIT SQL
            $limit_sql = $limit_offset. "," .$limit_limit;

            // GET PAGED MATCHING ITEMS
            $query = "
            SELECT i.*
            FROM `" .$this->tables['items']. "` AS `i`
            " .$join_sql. "
            WHERE " .$where_sql. "
            GROUP BY i.id
            ORDER BY " .$order_sql. "
            LIMIT " .$limit_sql . "
            ";

            //echo $query;
            //exit;

            $q = sqlquery($query);

            // NUMBER OF RESULTS
            $out['num_results'] = sqlrows($q);

            // GET ALL MATCHING ITEMS
            $tquery = "
            SELECT i.id
            FROM `" .$this->tables['items']. "` AS `i`
            " .$join_sql. "
            WHERE " .$where_sql. "
            GROUP BY i.id
            ";
            $tq = sqlquery($tquery);

            // TOTAL RESULTS
            $out['num_total'] = sqlrows($tq);
            $out['total'] = $out['num_total'];

            // PAGES
            $out['pages'] = $this->pageCount($out['total'], $limit_limit);
            $out['page'] = $page;

            // LOOP
            while ($item = sqlfetch($q)) {
                $out['results'][$item['id']] = $item;
            }

            // LOGGING SEARCH
            if ($vars['log']) {

                // LOG THE SEARCH
                $log = $this->search_log(array(
                    'keywords'      => $keywords,
                    'vars'          => $lva,
                    'num_results'   => $out['num_total'],
                    'account_id'    => $_uccms['_account']->userID(),
                    'ip'            => ip2long($_SERVER['REMOTE_ADDR'])
                ), $vars['log_params']);

                $_SESSION['_ECOMMERCE']['SEARCH_LOG']['hash'] = $log['hash'];
                if ($log['id']) $_SESSION['_ECOMMERCE']['SEARCH_LOG']['id'] = $log['id'];

            // NOT LOGGING SEARCH
            } else {
                //$_SESSION['BUSINESS-DIRECTORY']['SEARCH_LOG']['id'] = 0;
            }

            return $out;

        }


        // LOG SEARCH
        public function search_log($vars=array(), $params=array()) {

            $out = array();

            // HAVE VARS
            if (is_array($vars['vars'])) {

                // NOT LOGGING EMPTY VARS
                if (!$params['log_empty_vars']) {
                    $vars['vars'] = $this->array_removeEmpty($vars['vars']);
                }

                if (count($vars['vars']) > 0) {
                    $vars['vars'] = json_encode($vars['vars']);
                } else {
                    $vars['vars'] = '';
                }

            }

            $out['hash'] = md5(serialize($vars));

            // IS NEW / DIFFERENT SEARCH
            if ($out['hash'] != $_SESSION['_ECOMMERCE']['SEARCH_LOG']['hash']) {

                // RECORD
                $query = "INSERT INTO `" .$this->tables['search_log']. "` SET " .uccms_createSet($vars);
                if (sqlquery($query)) {
                    $out['id'] = sqlid();
                }

            }

            return $out;

        }


        // REMOVE EMPTY VALUES FROM ARRAY
        public function array_removeEmpty($haystack) {
            foreach ($haystack as $key => $value) {
                if (is_array($value)) {
                    $haystack[$key] = $this->array_removeEmpty($haystack[$key]);
                }

                if (empty($haystack[$key])) {
                    unset($haystack[$key]);
                }
            }
            return $haystack;
        }


        #############################
        # GENERAL - CATEGORIES
        #############################

        public function getCategoryTree($parent=0, $active='', $depth=100) {

            $out = array();

            // CLEAN UP
            $parent = (int)$parent;

            if ($active === true) {
                $active_sql = "WHERE (active=1)";
            } else if ($active === false) {
                $active_sql = "WHERE (active=0)";
            } else {
                $active_sql = "";
            }

            // GET CATEGORIES
            $query = "SELECT * FROM `" .$this->tables['categories']. "` " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $q = sqlquery($query);
            while ($item = sqlfetch($q)) {
                $ref = & $refs[$item['id']];
                foreach ($item as $name => $value) {
                    $ref[$name] = $value;
                }
                if ($item['parent'] == 0) {
                    $out[$item['id']] = & $ref;
                } else {
                    $refs[$item['parent']]['children'][$item['id']] = & $ref;
                }
            }

            unset($refs);

            // PARENT SPECIFIED
            if ($parent) {
                return $this->findArrayKey($out, $parent);
            // NO PARENT SPECIFIED
            } else {
                return $out;
            }

            return $out;

        }


        // OUTPUT THE CATEGORY TREE IN A DROPDOWN (TREE ARRAY, SELECTED ID,
        function printCategoryDropdown($tree, $selected=0, $id=0, $parent=0, $r=0) {
            foreach ($tree as $i => $t) {
                $dash = ($t['parent'] == 0) ? '' : str_repeat('-', $r) .' ';
                $sel = ($t['id'] == $selected ? ' selected="selected"' : '');
                $dis = ($t['id'] == $id ? ' disabled="disabled"' : '');
                echo "\t" . '<option value="' .$t['id']. '"' .$sel . $dis. '>' .$dash . stripslashes($t['title']). '</option>' . "\n";
                if (isset($t['children'])) {
                    $this->printCategoryDropdown($t['children'], $selected, $id, $t['parent'], $r+1);
                }
            }
        }


        // GET A SUB-ARRAY BY MATCHING KEY
        public function findArrayKey($array, $index) {
            if (!is_array($array)) return null;
            if (isset($array[$index])) return $array[$index];
            foreach ($array as $item) {
                $return = $this->findArrayKey($item, $index);
                if (!is_null($return)) {
                    return $return;
                }
            }
            return null;
        }


        // GET THE PARENT(S) OF A CATEGORY
        public function getCategoryParents($id=0) {
            $out = array();
            $id = (int)$id;
            if ($id > 0) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .$id. ")";
                $cat = sqlfetch(sqlquery($query));
                array_push($out, $cat);
                if ($cat['parent']) {
                    $out = array_merge($out, $this->getCategoryParents($cat['parent']));
                }
            }
            return $out;
        }


        // GET SUB-CATEGORIES
        public function subCategories($id, $active=null, $by_pricegroup=false) {
            $out = array();

            if ($active === true) {
                $active_sql = " AND (`active`=1)";
            } else if ($active === false) {
                $active_sql = " AND (`active`=0)";
            } else {
                $active_sql = "";
            }

            // GET SUB CATEGORIES FROM DB
            $sub_query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`parent`=" .(int)$id. ") " .$active_sql. " ORDER BY `sort` ASC, `title` ASC, `id` ASC";
            $sub_q = sqlquery($sub_query);

            // ONLY DISPLAYING BY AN ACCOUNT'S PRICE GROUP(S) AND PRICE GROUPS ARE ENABLED
            if (($by_pricegroup) && ($this->getSetting('pricegroups_enabled'))) {

                // ACCOUNT'S PRICE GROUPS
                $apg = $this->accountPriceGroups();

                // LOOP THROUGH SUB-CATEGORIES
                while ($sub = sqlfetch($sub_q)) {

                    // CATEGORY'S PRICE GROUPS
                    $cpg = $this->categoryPriceGroups($sub['id'], 'true');

                    // HAVE PRICE GROUPS
                    if (count($cpg) > 0) {

                        // ACCOUNT HAS ACCESS TO ONE OR MORE OF CATEGORY'S PRICE GROUPS
                        if (count(array_intersect(array_keys($cpg), $apg)) > 0) {
                            $out[] = $sub;
                        }

                    // NO PRICE GROUPS
                    } else {
                        $out[] = $sub;
                    }

                }

            // DISPLAYING ALL
            } else {
                while ($sub = sqlfetch($sub_q)) {
                    $out[] = $sub;
                }
            }

            return $out;

        }


        // GET ITEMS IN CATEGORY WITH PAGING
        public function categoryItems($id, $vars=array(), $by_pricegroup=false) {
            global $_paging;

            $out = array();

            if ($id) {

                // SORTING - BY PRICE
                if ($vars['sort'] == 'price') {

                    // CALCULATE BY PRICE GROUP & PRICE GROUPS ARE ENABLED
                    if (($by_pricegroup) && ($this->getSetting('pricegroups_enabled'))) {

                        // ACCOUNT'S PRICE GROUPS
                        $apg = $this->accountPriceGroups();

                        $pg_sql_select = ", NULLIF(i.price_sale, 0.00) AS `price_sale_string`";
                        $pg_sql_join = "LEFT JOIN `" .$this->tables['item_pricegroups']. "` AS `ipg` ON i.id=ipg.item_id";
                        $pg_sql_where = "AND (((ipg.pricegroup_id IN (" .implode(',', $apg). ")) AND (ipg.enabled=1)) OR (ipg.id IS NULL))";
                        $sql_sort = "COALESCE(ipg.price, `price_sale_string`, i.price)";

                    // NORMAL PRICE SORT
                    } else {
                        $sql_sort = "i.price ASC, i.title ASC, i.id ASC";
                    }

                // SORTING - DEFAULT
                } else {
                    $sql_sort = "i.title ASC, i.id ASC";
                }

                // HOW MANY ITEMS PER PAGE
                if ($vars['limit']) $limit = (int)$vars['limit']; // SPECIFIED DIRECTLY
                if (!$limit) $limit = $this->getSetting('items_per_page'); // USE E-COMMERCE SETTING
                if (!$limit) $limit = $this->perPage(); // USE BIGTREE / DEFAULT

                // CURRENT PAGE
                $page = isset($vars['page']) ? intval($vars['page']) : 1;

                // MATTD: Search below (categoryItemsTotal) too

                // GET ITEMS
                $item_query = "
                SELECT i.* " .$pg_sql_select. "
                FROM `" .$this->tables['items']. "` AS `i`
                INNER JOIN `" .$this->tables['item_categories']. "` AS `ic` ON i.id=ic.item_id
                " .$pg_sql_join. "
                WHERE (ic.category_id=" .$id. ") AND (i.active=1) AND (i.deleted=0) " .$pg_sql_where. "
                GROUP BY i.id
                ORDER BY " .$sql_sort. "
                LIMIT " .(($page - 1) * $limit). "," .$limit. "
                ";
                $item_q = sqlquery($item_query);
                while ($item = sqlfetch($item_q)) {
                    $out[] = $item;
                }

                // HAVE PAGING CLASS
                if ($_paging) {

                    // PREPARE PAGING (RESULTS PER PAGE, NUMBER OF PAGE NUMBERS, CURRENT PAGE, REQUEST VARIABLES (_GET, _POST), USE MOD_REWRITE)
                    $_paging->prepare($limit, 5, $page, $_GET, false);

                }

            }

            return $out;

        }


        // GET TOTAL ITEMS IN CATEGORY
        public function categoryItemsTotal($id, $vars=array()) {

            $count = 0;

            if ($id) {

                // GET ITEMS
                $item_query = "
                SELECT i.*
                FROM `" .$this->tables['items']. "` AS `i`
                INNER JOIN `" .$this->tables['item_categories']. "` AS `ic` ON i.id=ic.item_id
                WHERE (ic.category_id=" .$id. ") AND (i.active=1) AND (i.deleted=0)
                ";
                $item_q = sqlquery($item_query);
                $count = sqlrows($item_q);

            }

            return $count;

        }


        // GET THE TAX FOR A CATEGORY
        public function categoryTax($id=0) {
            $id = (int)$id;
            if ($id) {
                $query = "SELECT * FROM `" .$this->tables['tax_groups']. "` WHERE (`id`=" .$id. ")";
                $out = sqlfetch(sqlquery($query));
            }
            if (!$tax['id']) {
                $out = $this->getSettings(array('tax_global', 'tax_global_percent'));
            }
            return $out;
        }


        // GET THE TAX GROUP OF A CATEGORY
        public function categoryTaxGroup($id=0, $cats='') {
            $id = (int)$id;
            if ($id) {
                if (!is_array($cats)) {
                    $cats = $this->getCategoryParents($id);
                }
                if (count($cats) > 0) {
                    foreach ($cats as $cat) {
                        if ($cat['tax_group']) {
                            return $cat['tax_group'];
                        }
                    }
                }
            }
        }


        // GENERATE CATEGORY URL
        public function categoryURL($id, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            //return WWW_ROOT . $bigtree['path'][0]. '/' .trim($slug). '-' .$id. '/';
            return '/'. $this->storePath(). '/' .trim($slug). '-' .$id. '/';
        }


        // BREADCRUMB OUTPUT
        public function generateBreadcrumbs($array, $options=array()) {

            // OPTIONS
            if (!$options['base']) $options['base'] = './';
            if (!$options['home_title']) $options['home_title'] = 'Store'; // MATTD: Make into a configurable setting
            if (!$options['sep']) $options['sep'] = ' / '; // MATTD: Make into a configurable setting

            // ELEMENTS
            $els = array();

            // SHOW HOME LINK
            if (!$options['no_home']) {
                $els[] = '<a href="' .$options['base']. '">' .$options['home_title']. '</a>';
            }

            // NUMBER OF PARENT CATEGORIES
            $num_cat = count($array);

            // HAVE PARENT CATEGORIES
            if ($num_cat > 0) {

                // REVERSE ARRAY FOR EASIER OUTPUT
                $tcat = array_reverse($array);

                $i = 1;

                // LOOP
                foreach ($tcat as $cat) {
                    if ($options['rewrite']) {
                        $url = $this->categoryURL($cat['id'], $cat);
                    } else {
                        $url = $options['base']. '?id=' .$cat['id'];
                    }
                    $line = '';
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '<a href="' .$url. '">';
                    }
                    $line .= stripslashes($cat['title']);
                    if (($options['link_last']) || ($i != $num_cat)) {
                        $line .= '</a>';
                    }
                    $els[] = $line;
                    $i++;
                }

            }

            echo implode($options['sep'], $els);

        }


        // CATEGORY PRICE GROUPS
        public function categoryPriceGroups($category_id, $enabled='') {
            $category_id = (int)$category_id;
            $cpga = array();
            if ($category_id) {
                if ($enabled == 'true') {
                    $sql_enabled = " AND (`enabled`=1)";
                } else if ($enabled == 'false') {
                    $sql_enabled = " AND (`enabled`=0)";
                }
                $pgr_query = "SELECT * FROM `" .$this->tables['category_pricegroups']. "` WHERE (`category_id`=" .$category_id. ")" .$sql_enabled;
                $pgr_q = sqlquery($pgr_query);
                while ($pgr = sqlfetch($pgr_q)) {
                    $cpga[$pgr['pricegroup_id']] = $pgr;
                }
            }
            return $cpga;
        }


        #############################
        # CUSTOMERS
        #############################


        // RETURNS ARRAY OF CUSTOMER STATUSES
        public function customerStatuses($i=null) {
            $statuses = array(
                0 => [
                    'title' => 'Inactive',
                    'color' => '#E2E3E5',
                ],
                1 => [
                    'title' => 'Active',
                    'color' => '#28a745',
                ],
                9 => [
                    'title' => 'Deleted',
                    'color' => '#F8D7DA',
                ],
            );
            if (is_numeric($i)) {
                return $statuses[$i];
            } else {
                return $statuses;
            }
        }

        // SAVES A CUSTOMER
        public function saveCustomer($params=[], $options=[]) {
            global $_uccms;

            $out = [];

            $params['id'] = (int)$params['id'];

            // EDITING EXISTING
            if ($params['id']) {

                $out['id'] = $params['id'];

                $customer_columns = [];
                if (isset($params['account']['id'])) $customer_columns['account_id'] = (int)$params['account']['id'];
                if (isset($params['status'])) $customer_columns['status'] = (int)$params['status'];
                if (isset($params['pricegroups'])) $customer_columns['pricegroups'] = implode(',', $params['pricegroups']);

                // HAVE CUSTOMER COLUMNS
                if (count($customer_columns) > 0) {

                    // UPDATE CUSTOMER RECORD
                    $customer_sql = "UPDATE `" .$this->tables['customers']. "` SET " .$this->createSet($customer_columns). " WHERE (`id`=" .$params['id']. ")";

                    if (sqlquery($customer_sql)) {
                        $out['success'] = true;
                    } else {
                        $out['error'] = 'Failed to update customer.';
                    }

                }

            // NO CUSTOMER ID - CREATE
            } else {

                $account_id = 0;
                $customer_id = 0;

                // ACCOUNT-INVOLVED
                if ($params['account']['email']) {

                    // ACCOUNTS ARE ENABLED
                    if ($_uccms['_account']->isEnabled()) {

                        // LOOK FOR EXISTING ACCOUNT
                        $account_query = "SELECT `id` FROM `" .$_uccms['_account']->config['db']['accounts']. "` WHERE (`email`='" .sqlescape($params['account']['email']). "')";
                        $account_q = sqlquery($account_query);
                        $account = sqlfetch($account_q);

                        // ACCOUNT FOUND
                        if ($account['id']) {

                            $params['account']['id'] = $account['id'];

                        // ACCOUNT NOT FOUND
                        } else {

                            // GENERATE PASSWORD
                            if (!$params['account']['password']) $params['account']['password'] = $_uccms['_account']->rand_string(10, true);

                            // AUTOMATICALLY CREATING ACCOUNTS
                            if ($this->getSetting('accounts_auto_create')) {
                                $customer_send_email = $this->getSetting('accounts_auto_email');
                                if ($options['noWelcomeEmail']) $customer_send_email = false;
                            }

                            // ACCOUNT DETAILS
                            $customer_details = array(
                                'firstname' => $params['account']['firstname'],
                                'lastname'  => $params['account']['lastname'],
                            );

                            // CREATE ACCOUNT
                            $account = $_uccms['_account']->register($params['account']['email'], $params['account']['password'], $params['account']['email'], $customer_details, $customer_send_email);

                            // SUCCESS
                            if ($account['success'] == true) {

                                // NEW ACCOUNT
                                $params['account']['id'] = $account['id'];

                                $out['account'] = [
                                    'success' => true,
                                    'id' => $account['id'],
                                ];

                            } else {
                                $out['account']['error'] = 'Failed to create account';
                            }

                        }

                    // ACCOUNTS NOT ENABLED
                    } else {
                        $out['account']['error'] = 'Accounts not enabled.';
                    }

                }

                $customer_columns = [
                    'account_id' => (int)$params['account']['id'],
                    'status' => (int)$params['status'],
                    'pricegroups' => implode(',', (array)$params['pricegroups']),
                ];

                // CREATE CUSTOMER RECORD
                $customer_sql = "INSERT INTO `" .$this->tables['customers']. "` SET " .$this->createSet($customer_columns);
                if (sqlquery($customer_sql)) {
                    $out['success'] = true;
                    $out['id'] = sqlid();
                } else {
                    $out['error'] = 'Failed to create customer.';
                }

            }

            return $out;

        }

        // RETURNS ARRAY OF MATCHING CUSTOMERS
        public function getCustomers($params=[], $options=[]) {
            global $_uccms;

            $out = [];

            $page = (int)$params['page'];
            $limit = (int)$params['limit'];

            if (!$page) $page = 1;
            if (!$limit) $limit = $this->perPage();

            $wa = [];

            if ($params['id']) {
                $wa[] = "c.id=" .(int)$params['id'];
            }

            if ($params['status']) {
                $wa[] = "c.status=" .(int)$params['status'];
            } else {
                $wa[] = "c.status!=9";
            }

            // IS SEARCHING
            if ($params['q']) {
                $twa = [];
                foreach (explode(' ', $params['q']) as $part) {
                    $part = trim(sqlescape(strtolower($part)));
                    $twa[] = "(LOWER(cc.firstname) LIKE '%$part%') OR (LOWER(cc.lastname) LIKE '%$part%') OR (LOWER(cc.business_name) LIKE '%$part%')";
                }
                $wa[] = "(" .implode(" OR ", $twa). ")";
            }


            // DEFAULT PROFILE
            //$wa[] = "cc.default=1";

            $where = "WHERE (" .implode(") AND (", $wa). ")";

            $query = "
            FROM `" .$this->tables['customers']. "` AS `c`
            LEFT JOIN `" .$this->tables['customer_contacts']. "` AS `cc` ON cc.customer_id=c.id
            " .$where. "
            GROUP BY c.id
            ";

            // TOTAL
            $total = sqlrows(sqlquery("SELECT c.id " .$query));

            /*
            $sort_by = 'a.created';
            if (isset($params['sort'])) {
                $valid_columns = array(
                    'status'    => 'a.status',
                    'username'  => 'a.username',
                    'email'     => 'a.email',
                    'created'   => 'a.created',
                    'deleted'   => 'a.deleted',
                    'name'      => 'fullname',
                    'orders'    => 'num_orders'
                );
                if ($valid_columns[$params['sort']]) {
                    $sort_by = $valid_columns[$params['sort']];
                }
            }
            if ($sort_by == 'a.created') {
                $sort_dir = (isset($params['sort_direction']) && $params['sort_direction'] == 'ASC') ? 'ASC' : 'DESC';
            } else {
                $sort_dir = (isset($params['sort_direction']) && $params['sort_direction'] == 'DESC') ? 'DESC' : 'ASC';
            }
            */
            $sort_by = "c.id";
            $sort_dir = "ASC";

            $query_paged = "
            SELECT c.*
            " .$query. "
            ORDER BY " .$sort_by. " " .$sort_dir. "
            LIMIT " .(($page - 1) * $limit). "," .$limit. "
            ";
            $q = sqlquery($query_paged);
            while ($customer = sqlfetch($q)) {

                // PRICEGROUPS
                $customer['pricegroups'] = explode(',', stripslashes($customer['pricegroups']));

                // CONTACTS
                $customer['contacts'] = $this->customerContacts($customer['id']);

                $ccmerged = [];
                foreach ((array)$customer['contacts']['contacts'] as $contact) {
                    if ($contact['default']['billing']) {
                        $customer['default_billing'] = $contact['id'];
                        $ccmerged = array_merge($ccmerged, array_filter($contact));
                    }
                    if ($contact['default']['shipping']) {
                        $customer['default_shipping'] = $contact['id'];
                        $ccmerged = array_merge($ccmerged, array_filter($contact));
                    }
                    if ($contact['default']['delivery']) {
                        $customer['default_delivery'] = $contact['id'];
                        $ccmerged = array_merge($ccmerged, array_filter($contact));
                    }
                }
                unset($ccmerged['id']);
                unset($ccmerged['customer_id']);
                unset($ccmerged['active']);
                unset($ccmerged['default_billing']);
                unset($ccmerged['default_shipping']);
                unset($ccmerged['default_delivery']);
                unset($ccmerged['route_id']);

                $customer = array_merge_recursive($customer, $ccmerged);

                // MAIN ACCOUNT
                $customer['account'] = $_uccms['_account']->getAccount('', true, $customer['account_id']);

                $out['customers'][$customer['id']] = $customer;

            }

            // PAGES
            $out['pages'] = $this->pageCount($total, $limit);
            $out['page'] = $page;

            return $out;

        }

        // RETURNS A SPECIFIC CUSTOMER
        public function getCustomer($id) {
            $id = (int)$id;
            return ($id ? (array)$this->getCustomers([ 'id' => $id ])['customers'][$id] : []);
        }

        // SAVES A CUSTOMER'S CONTACT
        public function saveCustomerContact($params, $options=[]) {
            $out = [];

            $params['default_billing'] = (bool)$params['default']['billing'];
            $params['default_shipping'] = (bool)$params['default']['shipping'];
            $params['default_delivery'] = (bool)$params['default']['delivery'];
            unset($params['default']);

            if ($params['id']) {

                $out['id'] = (int)$params['id'];

                $params['active'] = 1;

                $clear = [];
                if ($params['default_billing']) $clear['default_billing'] = 0;
                if ($params['default_shipping']) $clear['default_shipping'] = 0;
                if ($params['default_delivery']) $clear['default_delivery'] = 0;
                if (count($clear) > 0) {
                    $sql = "UPDATE `" .$this->tables['customer_contacts']. "` SET " .$this->createSet($clear). " WHERE (`customer_id`=" .(int)$params['customer_id']. ")";
                    sqlquery($sql);
                }

                $sql = "UPDATE `" .$this->tables['customer_contacts']. "` SET " .$this->createSet($params). " WHERE (`id`=" .(int)$params['id']. ")";
                if (sqlquery($sql)) {
                    $out['success'] = true;
                } else {
                    $out['error'] = 'Failed to update contact.';
                }

            } else {

                $sql = "INSERT INTO `" .$this->tables['customer_contacts']. "` SET " .$this->createSet($params);
                if (sqlquery($sql)) {
                    $out['success'] = true;
                    $out['id'] = sqlid();
                } else {
                    $out['error'] = 'Failed to create contact.';
                }

            }

            return $out;
        }

        // GET'S CUSTOMER CONTACTS
        public function customerContacts($customer_id, $params=[], $options=[]) {
            $out = [];

            $wa = [];

            $wa[] = "cc.customer_id=" .(int)$customer_id;

            if ($params['id']) {
                $wa[] = "cc.id=" .(int)$params['id'];
            }

            /*
            if ($params['customer_id']) {
                $wa[] = "cc.customer_id=" .(int)$params['id'];
            }
            */

            if ($params['default']) {
                $default = trim(sqlescape($params['default']));
                switch ($default) {
                    case 'billing':
                        $wa[] = "cc.default_billing=1";
                        break;
                    case 'shipping':
                        $wa[] = "cc.default_shipping=1";
                        break;
                    case 'delivery':
                        $wa[] = "cc.default_delivery=1";
                        break;
                    default:
                        $out['error'] = 'Invalid default type.';
                        return $out;
                        break;
                }
            }

            if ($params['route_id']) {
                $wa[] = "cc.route_id=" .(int)$params['route_id'];
            }

            // IS SEARCHING
            if ($params['q']) {
                $twa = [];
                foreach (explode(' ', $params['q']) as $part) {
                    $part = trim(sqlescape(strtolower($part)));
                    $twa[] = "(LOWER(cc.firstname) LIKE '%$part%') OR (LOWER(cc.lastname) LIKE '%$part%') OR (LOWER(cc.business_name) LIKE '%$part%')";
                }
                $wa[] = "(" .implode(" OR ", $twa). ")";
            }

            $where = "WHERE (" .implode(") AND (", $wa). ")";

            $query = "
            SELECT cc.*
            FROM `" .$this->tables['customer_contacts']. "` AS `cc`
            " .$where. "
            ORDER BY cc.id ASC
            ";
            $q = sqlquery($query);
            while ($contact = sqlfetch($q)) {

                $contact['name'] = trim(stripslashes($contact['firstname']. ' ' .$contact['lastname']));

                if ($contact['phone_cell']) {
                    $contact['phone'] = $contact['phone_cell'];
                } else if ($contact['phone_work']) {
                    $contact['phone'] = $contact['phone_work'];
                } else if ($contact['phone_home']) {
                    $contact['phone'] = $contact['phone_home'];
                }

                $contact['address_full'] = uccms_prettyAddress([
                    $contact['address1'],
                    $contact['address2'],
                    implode(' ', [$contact['city'], $contact['state'], $contact['zip']])
                ], ' ');

                if ($contact['default_billing']) {
                    $contact['default']['billing'] = $contact['default_billing'];
                    $out['default']['billing'] = $contact['id'];
                }
                if ($contact['default_shipping']) {
                    $contact['default']['shipping'] = $contact['default_shipping'];
                    $out['default']['shipping'] = $contact['id'];
                }
                if ($contact['default_delivery']) {
                    $contact['default']['delivery'] = $contact['default_delivery'];
                    $out['default']['delivery'] = $contact['id'];
                }

                $out['contacts'][$contact['id']] = $contact;

            }

            $out['page'] = 1;
            $out['pages'] = 1;

            return $out;

        }

        // RETURNS A SPECIFIC CUSTOMER CONTACT
        public function customerContact($customer_id, $id) {
            $params = [];
            if (is_numeric($id)) {
                $id = (int)$id;
                $params['id'] = $id;
                return (array)$this->customerContacts($customer_id, $params)['contacts'][$id];
            } else {
                $params['default'] = trim(sqlescape($id));
                return array_shift($this->customerContacts($customer_id, $params)['contacts']);
            }
        }

        // FORMATS A CONTACT'S INFO NICELY
        public function formatContactInfo($contact=[], $string=true) {
            global $_uccms_ecomm;
            $cparts = [];
            $name = trim(stripslashes($contact['firstname']. ' ' .$contact['lastname']));
            if ($name) {
                $cparts['name'] = $name;
            }
            $address = uccms_prettyAddress([
                $contact['address1'],
                $contact['address2'],
                implode(' ', [$contact['city'], $contact['state'], $contact['zip']])
            ]);
            if ($address) {
                $cparts['address'] = $address;
            }
            $phone = '';
            if ($contact['phone_mobile']) {
                $phone = $_uccms_ecomm->prettyPhone($contact['phone_mobile']);
            } else if ($contact['phone_work']) {
                $phone = $_uccms_ecomm->prettyPhone($contact['phone_work']);
            } else if ($contact['phone_home']) {
                $phone = $_uccms_ecomm->prettyPhone($contact['phone_home']);
            }
            if ($phone) {
                $cparts['phone'] = $phone;
            }
            $email = '';
            if ($contact['email_work']) {
                $email = $contact['email_work'];
            } else if ($contact['email_home']) {
                $email = $contact['email_home'];
            }
            if ($email) {
                $cparts['email'] = stripslashes($email);
            }
            if ($string == true) {
                $contact_info = implode('', array_map(function($name, $val) {
                    return '<span class="' .$name. '">' .$val. '</span>';
                }, array_keys($cparts), $cparts));
                return $contact_info;
            } else {
                return $cparts;
            }
        }


        #############################
        # ITEMS
        #############################


        // RETURNS ARRAY OF ITEM TYPES
        public function itemTypes($i=null) {
            $types = array(
                0 => [
                    'title' => 'Product',
                    'color' => '',
                ],
                1 => [
                    'title' => 'Service',
                    'color' => '',
                ],
                2 => [
                    'title' => 'Rental',
                    'color' => '',
                ],
                3 => [
                    'title' => 'Package',
                    'color' => '',
                ],
                4 => [
                    'title' => 'Asset',
                    'color' => '',
                ],
            );
            if (is_numeric($i)) {
                return $types[$i];
            } else {
                return $types;
            }
        }


        // RETURNS ARRAY OF ITEM STATUSES
        public function itemStatuses($i=null) {
            $statuses = array(
                0 => [
                    'title' => 'Inactive',
                    'color' => '#E2E3E5',
                ],
                1 => [
                    'title' => 'Active',
                    'color' => '#28a745',
                ],
            );
            if (is_numeric($i)) {
                return $statuses[$i];
            } else {
                return $statuses;
            }
        }


        // GENERATE ITEM URL
        public function itemURL($id, $category_id=0, $data=array()) {
            global $bigtree;
            if (!$data['id']) {
                $query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .(int)$id. ")";
                $data = sqlfetch(sqlquery($query));
            }
            if ($data['slug']) {
                $slug = stripslashes($data['slug']);
            } else {
                $slug = $this->makeRewrite($data['title']);
            }
            $category_slug = $data['category_slug']. '-' .$category_id;
            if (!$data['category_slug']) {
                if ($category_id > 0) {
                    $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`id`=" .(int)$category_id. ")";
                    $data = sqlfetch(sqlquery($query));
                    if ($data['slug']) {
                        $category_slug = stripslashes($data['slug']). '-' .$data['id'];
                    } else {
                        $category_slug = $this->makeRewrite($data['title']). '-' .$category_id;
                    }
                } else {
                    $category_slug = 'item';
                }
            }
            return '/'. $this->storePath(). '/' .trim($category_slug). '/' .trim($slug). '-' .$id. '/';
        }


        // GET ITEM ATTRIBUTES (INCLUDING FROM WITHIN GROUPS)
        public function itemAttributes($item_id) {

            // ATTRIBUTE ARRAY
            $attra = array();

            // CLEAN UP
            $item_id = (int)$item_id;

            // HAVE ITEM ID
            if ($item_id) {

                // GET ITEM ATTRIBUTE RELATIONS
                $ia_query = "SELECT * FROM `" .$this->tables['item_attributes']. "` WHERE (`item_id`=" .$item_id. ") ORDER BY `sort` ASC, `id` ASC";
                $ia_q = sqlquery($ia_query);
                if (sqlrows($ia_q) > 0) {
                    while ($ia = sqlfetch($ia_q)) {
                        $taa = array();
                        if ($ia['attribute_id'] != 0) { // IS ATTRIBUTE
                            $taa[$ia['attribute_id']] = $ia['attribute_id'];
                        } else if ($ia['group_id'] != 0) { // IS GROUP
                            $attrgroup_query = "SELECT * FROM `" .$this->tables['attribute_groups']. "` WHERE (`id`=" .$ia['group_id']. ")";
                            $attrgroup = sqlfetch(sqlquery($attrgroup_query));
                            if ($attrgroup['attributes']) {
                                foreach (explode(',', stripslashes($attrgroup['attributes'])) as $attr) {
                                    $taa[$attr] = $attr;
                                }
                            }
                        }
                        if (count($taa) > 0) { // HAVE ATTRIBUTES
                            $attr_query = "SELECT * FROM `" .$this->tables['attributes']. "` WHERE (`id` IN(" .implode(',', array_keys($taa)). ")) AND (`active`=1) ORDER BY `sort` ASC, `id` DESC";
                            $attr_q = sqlquery($attr_query);
                            if (sqlrows($attr_q) > 0) {
                                while ($attr = sqlfetch($attr_q)) {
                                    $attra[] = $attr;
                                }
                            }
                        }
                    }
                }

            }

            return $attra;

        }


        // GET ITEM
        public function getItem($item_id, $active=true) {
            $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .(int)$item_id. ")";
            if ($active) {
                $item_query .= " AND (`active`=1)";
            }
            return sqlfetch(sqlquery($item_query));
        }


        // ITEM PRICE
        public function itemPrice($item, $pricegroup_id=0) {
            global $_uccms;

            // ON SALE
            if ($item['price_sale'] != '0.00') {
                $price = $item['price_sale'];

            // NOT ON SALE
            } else {
                $price = $item['price'];
            }

            // PRICE GROUPS ENABLED
            if ($this->getSetting('pricegroups_enabled')) {

                // PRICEGROUP TO USE SPECIFIED
                if ($pricegroup_id > 0) {

                    // GET ITEM'S PRICE GROUPS
                    $ipg = $this->itemPriceGroups($item['id']);

                    // PRICE GROUP FOUND
                    if (is_array($ipg[$pricegroup_id])) {
                        return $ipg[$pricegroup_id]['price'];
                    }

                // USER IS LOGGED IN
                } else if ($_uccms['_account']->loggedIn()) {

                    // GET STORE'S PRICE GROUPS
                    if ($this->misc['store_price_groups']) {
                        $spg = $this->misc['store_price_groups'];
                    } else {
                        $spg = $this->getSetting('pricegroups');
                        if (!is_array($spg)) {
                            $spg = json_decode($spg, true);
                        }
                        $this->misc['store_price_groups'] = $spg;
                    }

                    // GET ITEM'S PRICE GROUPS
                    $ipg = $this->itemPriceGroups($item['id'], 'true');

                    // GET ACCOUNT'S PRICE GROUPS
                    $apg = $this->accountPriceGroups();

                    // HAVE STORE PRICE GROUPS
                    if ((is_array($spg)) && (count($spg) > 0)) {

                        // LOOP
                        foreach ($spg as $spg_id => $spg_info) {

                            // ACCOUNT HAS PRICE GROUP
                            if (in_array($spg_id, $apg)) {

                                // ITEM HAS PRICE GROUP AND IS ENABLED
                                if ($ipg[$spg_id]['enabled']) {
                                    return $ipg[$spg_id]['price'];
                                }

                            }

                        }

                    }

                }

            }

            return $price;

        }


        // ITEM PRICE GROUPS
        public function itemPriceGroups($item_id, $enabled='') {
            $item_id = (int)$item_id;
            $ipga = array();
            if ($item_id) {
                if ($enabled == 'true') {
                    $sql_enabled = " AND (`enabled`=1)";
                } else if ($enabled == 'false') {
                    $sql_enabled = " AND (`enabled`=0)";
                }
                $pgr_query = "SELECT * FROM `" .$this->tables['item_pricegroups']. "` WHERE (`item_id`=" .$item_id. ")" .$sql_enabled;
                $pgr_q = sqlquery($pgr_query);
                while ($pgr = sqlfetch($pgr_q)) {
                    $ipga[$pgr['pricegroup_id']] = $pgr;
                }
            }
            return $ipga;
        }


        // ITEM MINIMUM QUANTITY
        public function itemQtyMin($item, $with_pricegroup=false) {
            global $_uccms;

            $qty_min = 0;

            // PRICE GROUPS ENABLED
            if ($this->getSetting('pricegroups_enabled')) {

                // USER IS LOGGED IN
                if ($_uccms['_account']->loggedIn()) {

                    // GET STORE'S PRICE GROUPS
                    if ($this->misc['store_price_groups']) {
                        $spg = $this->misc['store_price_groups'];
                    } else {
                        if (is_array($this->getSetting('pricegroups'))) {
                            $spg = $this->getSetting('pricegroups');
                            $this->misc['store_price_groups'] = $spg;
                        } else {
                            $spg = json_decode($this->getSetting('pricegroups'), true);
                            $this->misc['store_price_groups'] = $spg;
                        }
                    }

                    // GET ITEM'S PRICE GROUPS
                    $ipg = $this->itemPriceGroups($item['id'], 'true');

                    // GET ACCOUNT'S PRICE GROUPS
                    $apg = $this->accountPriceGroups();

                    // HAVE STORE PRICE GROUPS
                    if ((is_array($spg)) && (count($spg) > 0)) {

                        // LOOP
                        foreach ($spg as $spg_id => $spg_info) {

                            // ACCOUNT HAS PRICE GROUP
                            if (in_array($spg_id, $apg)) {

                                // ITEM HAS PRICE GROUP AND IS ENABLED
                                if ($ipg[$spg_id]['enabled']) {
                                    if ($with_pricegroup) {
                                        return $ipg[$spg_id];
                                    } else {
                                        return $ipg[$spg_id]['qty_min'];
                                    }
                                }

                            }

                        }

                    }

                }

            }

            return $price;

        }


        // GENERATE VARIANTS FOR ITEM
        public function item_updateVariants($item_id) {

            $item_id = (int)$item_id;

            if ($item_id) {

                $aa = array();

                // GET ITEM ATTRIBUTES / GROUPS
                $ia_query = "SELECT `attribute_id`, `group_id` FROM `" .$this->tables['item_attributes']. "` WHERE (`item_id`=" .$item_id. ")";
                $ia_q = sqlquery($ia_query);
                while ($ia = sqlfetch($ia_q)) {

                    // IS ATTRIBUTE
                    if ($ia['attribute_id']) {
                        $aa[$ia['attribute_id']] = $ia['attribute_id'];

                    // IS GROUP
                    } else if ($ia['group_id']) {

                        // GET GROUP
                        $ig_query = "SELECT `attributes` FROM `" .$this->tables['attribute_groups']. "` WHERE (`id`=" .$ia['group_id']. ")";
                        $ig_q = sqlquery($ig_query);
                        $ig = sqlfetch($ig_q);

                        // HAVE ATTRIBUTES - ADD THEM TO ATTRIBUTE ARRAY
                        if ($ig['attributes']) {
                            unset($iga);
                            $iga = explode(',', $ig['attributes']);
                            if (is_array($iga)) {
                                foreach ($iga as $attr_id) {
                                    $aa[$attr_id] = $attr_id;
                                }
                            }
                        }

                    }

                }

                // HAVE ATTRIBUTES
                if (count($aa) > 0) {

                    $va = array();

                    $sa = array();
                    $ma = array();
                    $raa = array();

                    // SORT ASC
                    sort($aa);

                    // LOOP THROUGH EACH ATTRIBUTE
                    foreach ($aa as $attr_id) {

                        // GET ATTRIBUTE INFO
                        $attr_query = "SELECT * FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .$attr_id. ")";
                        $attr_q = sqlquery($attr_query);
                        $attr = sqlfetch($attr_q);

                        if ($attr['checkout']) {
                            continue;
                        }

                        //echo print_r($attr);

                        // ATTRIBUTE FOUND
                        if ($attr['id']) {

                            // IS REQUIRED
                            if ($attr['required']) {
                                $raa[] = $attr['id'];
                            }

                            // DROPDOWN
                            if ($attr['type'] == 'dropdown') {

                                // HAVE OPTIONS
                                if ($attr['options']) {

                                    $opta = json_decode(stripslashes($attr['options']), true);

                                    // HAVE OPTIONS
                                    if (is_array($opta)) {

                                        ksort($opta);

                                        // LOOP
                                        foreach ($opta as $opt_id => $opt) {
                                            $sa[$attr['id']][$opt_id] = array(
                                                'title' => stripslashes($opt['title'])
                                            );
                                        }

                                        unset($opta);

                                    }

                                }

                            // CHECKBOXES
                            } else if ($attr['type'] == 'checkbox') {

                                // HAVE OPTIONS
                                if ($attr['options']) {

                                    $opta = json_decode(stripslashes($attr['options']), true);

                                    // HAVE OPTIONS
                                    if (is_array($opta)) {

                                        ksort($opta);

                                        // LOOP
                                        foreach ($opta as $opt_id => $opt) {
                                            $ma[$attr['id']][$opt_id] = array(
                                                'title' => stripslashes($opt['title'])
                                            );
                                        }

                                        unset($opta);

                                    }

                                }

                            // SELECT BOXES
                            } else if ($attr['type'] == 'select_box') {

                                // HAVE OPTIONS
                                if ($attr['options']) {

                                    $opta = json_decode(stripslashes($attr['options']), true);

                                    // HAVE OPTIONS
                                    if (is_array($opta)) {

                                        ksort($opta);

                                        // LOOP
                                        foreach ($opta as $opt_id => $opt) {
                                            $sa[$attr['id']][$opt_id] = array(
                                                'title' => stripslashes($opt['title'])
                                            );
                                        }

                                        unset($opta);

                                    }

                                }

                            // TEXT
                            } else if ($attr['type'] == 'text') {

                                $ma[$attr['id']][] = array(
                                    'title' => stripslashes($attr['title'])
                                );

                            // TEXTAREA
                            } else if ($attr['type'] == 'textarea') {

                                $ma[$attr['id']][] = array(
                                    'title' => stripslashes($attr['title'])
                                );

                            // DATE
                            } else if ($attr['type'] == 'date') {

                                $ma[$attr['id']][] = array(
                                    'title' => stripslashes($attr['title'])
                                );

                            // TIME
                            } else if ($attr['type'] == 'time') {

                                $ma[$attr['id']][] = array(
                                    'title' => stripslashes($attr['title'])
                                );

                            // FILE
                            } else if ($attr['type'] == 'file') {

                                $ma[$attr['id']][] = array(
                                    'title' => stripslashes($attr['title'])
                                );

                            // WIDTH / HEIGHT
                            } else if ($attr['type'] == 'width_height') {

                                $ma[$attr['id']][] = array(
                                    'title' => stripslashes($attr['title'])
                                );

                            }

                        }

                    }

                    /*
                    echo print_r($sa);
                    echo print_r($ma);
                    exit;
                    */

                    // HAVE SINGLES ARRAY
                    if (count($sa) > 0) {

                        // LOOP
                        foreach ($sa as $attr_id => $opts) {

                            $tva = array();

                            // ADD TO EXISTING
                            foreach ($opts as $opt_id => $opt) {
                                foreach ($va as $ta) {

                                    $tva[] = array(
                                        'code'      => $ta['code']. ',' .$attr_id. ':' .$opt_id,
                                        'title'     => $ta['title']. ', ' .$opt['title'],
                                        'attr_id'   => $attr_id
                                    );

                                }
                            }

                            $va = array_merge($va, $tva);

                            unset($tva);

                            // ADD OWN
                            foreach ($opts as $opt_id => $opt) {

                                $va[] = array(
                                    'code'      => $attr_id. ':' .$opt_id,
                                    'title'     => $opt['title'],
                                    'attr_id'   => $attr_id
                                );

                            }

                        }

                        unset($sa);

                    }

                    // HAVE MULTIPLES ARRAY
                    if (count($ma) > 0) {

                        // LOOP
                        foreach ($ma as $attr_id => $opts) {

                            $tva = array();

                            // ADD OWN
                            foreach ($opts as $opt_id => $opt) {

                                $va[] = array(
                                    'code'      => $attr_id. ':' .$opt_id,
                                    'title'     => $opt['title'],
                                    'attr_id'   => $attr_id
                                );

                            }

                            // ADD TO EXISTING
                            foreach ($opts as $opt_id => $opt) {

                                // LOOP
                                foreach ($va as $ta) {

                                    // DON'T HAVE YET
                                    if ($ta['code'] != $attr_id. ':' .$opt_id) {

                                        // GET LAST ATTR/OPT FROM CODE
                                        $tcpa = explode(',', $ta['code']);
                                        $last = array_pop($tcpa);

                                        // LAST FOUND
                                        if ($last) {

                                            // SPLIT
                                            list($last_attr_id, $last_opt_id) = explode(':', $last);

                                            // SAME ATTRIBUTE & THIS OPTION ID IS LESS THAN LAST
                                            if (($attr_id == $last_attr_id) && ($opt_id < $last_opt_id)) {
                                                continue; // SKIP
                                            }

                                        }

                                        unset($tcpa);

                                        unset($tva);

                                        $tva[] = array(
                                            'code'      => $ta['code']. ',' .$attr_id. ':' .$opt_id,
                                            'title'     => $ta['title']. ', ' .$opt['title'],
                                            'attr_id'   => $attr_id
                                        );

                                        $va = array_merge($va, $tva);

                                        unset($tva);

                                    }

                                }

                            }

                            unset($tva);

                        }

                        unset($ma);

                    }

                }

                /*
                print_r($va);
                exit;
                */

                // LOCATIONS ARRAY
                $la = array();

                // GET LOCATIONS
                $loc_query = "SELECT `id` FROM `" .$this->tables['locations']. "`";
                $loc_q = sqlquery($loc_query);
                while ($loc = sqlfetch($loc_q)) {
                    $la[$loc['id']] = $loc['id'];
                }

                if (count($la) == 0) $la[0] = 0;

                // CURRENT VARIENTS ARRAY
                $cva = array();

                // GET ITEM'S CURRENT VARIANTS
                $curvars_query = "SELECT `id`, `location_id`, `item_id`, `hash`, `code` FROM `" .$this->tables['item_variants']. "` WHERE (`item_id`=" .$item_id. ")";
                $curvars_q = sqlquery($curvars_query);
                while ($curvar = sqlfetch($curvars_q)) {
                    $cva[$curvar['location_id']][$curvar['hash']] = $curvar;
                }

                // LOOP THROUGH ALL VARIANTS
                foreach ($va as $var) {

                    $skip = false;

                    // HAVE REQUIRED FIELDS
                    if (count($raa) > 0) {

                        // LOOP
                        foreach ($raa as $attr_id) {

                            // CODE DOES NOT CONTAIN OR START WITH ATTRIBUTE
                            if ((stristr($var['code'], ',' .$attr_id. ':') === false) && (substr($var['code'], 0, strlen($attr_id)+1) != $attr_id. ':')) {
                                $skip = true;
                                break;
                            }

                        }

                        // HAVE MULTIPLE REQUIRED ATTRIBUTES
                        if (count($raa) > 1) {

                            // DOESN'T HAVE MULTIPLE ATTRIBUTES, SKIP
                            if (stristr($var['code'], ',') === false) {
                                $skip = true;
                            }

                        }

                    }

                    if (!$skip) {

                        $hash = md5($var['code']);

                        // LOOP THROUGH LOCATIONS
                        foreach ($la as $loc_id) {

                            // IN DB
                            if ($cva[$loc_id][$hash]) {

                                unset($cva[$loc_id][$hash]);

                            // NOT IN DB
                            } else {

                                $data = array(
                                    'original_title' => $var['title']
                                );

                                $columns = array(
                                    'location_id'           => $loc_id,
                                    'item_id'               => $item_id,
                                    'hash'                  => $hash,
                                    'code'                  => $var['code'],
                                    'sku'                   => '',
                                    'title'                 => $var['title'],
                                    'data'                  => json_encode($data),
                                    'inventory_available'   => 0,
                                    'inventory_unavailable' => 0,
                                    'inventory_alert'       => 0,
                                    'inventory_alert_at'    => 0
                                );

                                // CREATE RECORD
                                $new_query = "INSERT INTO `" .$this->tables['item_variants']. "` SET " .$this->createSet($columns);
                                sqlquery($new_query);

                                //echo $new_query. '<br />';

                            }

                        }

                    }

                }

                // HAVE OLD VARIANT LOCATIONS
                if (count($cva) > 0) {

                    // LOOP
                    foreach ($cva as $loc_id => $cvars) {

                        // HAVE OLD VARIANTS
                        if (count($cvars) > 0) {

                            // LOOP
                            foreach ($cvars as $cvar) {

                                // REMOVE FROM DB
                                $delete_query = "DELETE FROM `" .$this->tables['item_variants']. "` WHERE (`location_id`=" .$loc_id. ") AND (`hash`='" .$cvar['hash']. "')";
                                sqlquery($delete_query);

                                //echo $delete_query. '<br />';

                            }

                        }

                    }

                    unset($cva);

                }

                unset($va);

                return true;

            }

            return false;

        }


        // GET ITEM'S VARIANTS
        public function item_getVariants($item_id) {

            $out = [];

            $item_id = (int)$item_id;

            if ($item_id) {

                // GET ITEM'S VARIANTS
                $vars_query = "SELECT * FROM `" .$this->tables['item_variants']. "` WHERE (`item_id`=" .$item_id. ")";
                $vars_q = sqlquery($vars_query);
                while ($var = sqlfetch($vars_q)) {
                    $out[$var['location_id']][$var['hash']] = $var;
                }

            }

            return $out;

        }


        // CREATE VARIATION CODE STRING FROM $attrs ARRAY
        public function item_variantCodeFromAttrs($attrs=array()) {

            // ATTRIBUTES SPECIFIED
            if (count($attrs) > 0) {

                ksort($attrs);

                $ca = array();

                // LOOP
                foreach ($attrs as $attr_id => $opts) {

                    // GET ATTRIBUTE INFO
                    $attr_query = "SELECT `type` FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .(int)$attr_id. ")";
                    $attr_q = sqlquery($attr_query);
                    $attr = sqlfetch($attr_q);

                    // BY ID
                    if (($attr['type'] == 'checkbox') || ($attr['type'] == 'dropdown') || ($attr['type'] == 'select_box')) {

                        if (is_array($opts)) {
                            sort($opts);
                            foreach ($opts as $opt) {
                                $ca[] = $attr_id. ':' .$opt;
                            }
                        } else {
                            $ca[] = $attr_id. ':' .$opts;
                        }

                    // IS SINGLE
                    } else {
                        $ca[] = $attr_id. ':0';
                    }

                }

                // CODE
                $out = implode(',', $ca);

            }

            return $out;

        }


        // CREATE ARRAY FROM VARIATION CODE
        public function item_attrsFromVariantCode($code='') {

            $attrs = array();

            // HAVE CODE
            if ($code) {

                // BREAK CODE INTO PIECES
                $cpa = explode(',', $code);

                // LOOP THROUGH PIECES
                foreach ($cpa as $p) {

                    // BREAK PIECE
                    list($attr_id, $option_id) = explode(':', $p);

                    // PUT TOGETHR ARRAY
                    if ($attrs[$attr_id]) {
                        if (is_array($attrs[$attr_id])) {
                            $attrs[$attr_id][] = $option_id;
                        } else {
                            $val = $attrs[$attr_id];
                            $attrs[$attr_id] = array($val, $option_id);
                        }
                    } else {
                        $attrs[$attr_id] = $option_id;
                    }

                }

            }

            return $attrs;

        }


        // GET ITEM'S INVENTORY
        public function item_getInventory($item_id, $location_id=0, $attrs=array()) {

            $out = [
                'available'   => 0,
                'unavailable' => 0,
                'backordered' => 0,
                'alerts'      => 0,
            ];

            $item_id = (int)$item_id;

            if ($item_id) {

                // GET ITEM INFO
                $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$item_id. ")";
                $item_q = sqlquery($item_query);
                $item = sqlfetch($item_q);

                // ITEM FOUND
                if ($item['id']) {

                    // IS PACKAGE & TRACK / CHECK PACKAGE ITEM INVENTORY
                    if (($item['type'] == 3) && ($item['package_track_items_inventory'] == 1)) {

                        // GET PACKAGE ITEMS
                        $pitems_query = "SELECT * FROM `" .$this->tables['item_package_items']. "` WHERE (`package_id`=" .$item['id']. ")";
                        $pitems_q = sqlquery($pitems_query);
                        while ($pitem = sqlfetch($pitems_q)) {

                            unset($pitem_attrs);

                            // IS VARIANT
                            if ($pitem['variant_id']) {

                                // GET VARIANT INFO
                                $var_query = "SELECT * FROM `" .$this->tables['item_variants']. "` WHERE (`id`=" .$pitem['variant_id']. ")";
                                $var_q = sqlquery($var_query);
                                $var = sqlfetch($var_q);

                                // GENERATE ATTRS
                                $pitem_attrs = $this->item_attrsFromVariantCode($var['code']);

                            // USE SPECIFIED ATTRIBUTES
                            } else {

                                // FILE(S) SUBMITTED
                                if (is_array($_FILES)) {

                                    //echo print_r($_FILES);
                                    //exit;

                                    /*
                                    // LOOP
                                    foreach ($_FILES as $field_name => $file) {

                                        $attr_id = str_replace('attribute-', '', $field_name);

                                        if (is_numeric($attr_id)) {
                                            $ta = array(
                                                $attr_id => 0
                                            );
                                        }

                                        if (!is_array($attrs)) $attrs = array();
                                        $attrs += $ta;

                                    }
                                    */

                                }

                                // HAVE ATTRIBUTES FOR PACKAGE ITEM
                                if (is_array($attrs[$pitem['id']])) {
                                    $pitem_attrs = $attrs[$pitem['id']];
                                }

                            }

                            // GET VARIATION INVENTORY
                            $var_inventory = $this->item_getInventory($pitem['item_id'], $location_id, $pitem_attrs);

                            // RETURN INDIVIDUAL VARIANT INVENTORY INFO
                            $out['items'][$pitem['id']] = array_merge(array(
                                'item_id'       => $pitem['item_id'],
                                'variant_id'    => $pitem['variant_id'],
                                'quantity'      => $pitem['quantity']
                            ), $var_inventory);

                            if ($pitem['quantity'] > 0) {

                                $per = floor($var_inventory['available'] / $pitem['quantity']);
                                if (isset($lowest['available'])) {
                                    if ($per < $lowest['available']) {
                                        $lowest['available'] = $per;
                                    }
                                } else {
                                    $lowest['available'] = $per;
                                }

                                $per = floor($var_inventory['unavailable'] / $pitem['quantity']);
                                if (isset($lowest['unavailable'])) {
                                    if ($per < $lowest['unavailable']) {
                                        $lowest['unavailable'] = $per;
                                    }
                                } else {
                                    $lowest['unavailable'] = $per;
                                }

                                $per = floor($var_inventory['backordered'] / $pitem['quantity']);
                                if (isset($lowest['backordered'])) {
                                    if ($per < $lowest['backordered']) {
                                        $lowest['backordered'] = $per;
                                    }
                                } else {
                                    $lowest['backordered'] = $per;
                                }

                            }

                        }

                        // USE LOWEST AVAILABLE
                        $out['available']   = $lowest['available'];
                        $out['unavailable'] = $lowest['unavailable'];
                        $out['backordered'] = $lowest['backordered'];

                    // OTHER ITEM TYPES
                    } else {

                        // FILE(S) SUBMITTED
                        if (is_array($_FILES)) {

                            // LOOP
                            foreach ($_FILES as $field_name => $file) {

                                $attr_id = str_replace('attribute-', '', $field_name);

                                if (is_numeric($attr_id)) {
                                    $ta = array(
                                        $attr_id => 0
                                    );
                                }

                                if (!is_array($attrs)) $attrs = array();
                                $attrs += $ta;

                            }

                        }

                        // ATTRIBUTES SPECIFIED
                        if (count((array)$attrs) > 0) {

                            // LOOP
                            foreach ($attrs as $attr_id => $attr_val) {

                                // GET ATTRIBUTE
                                $attribute_query = "SELECT `checkout` FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .(int)$attr_id. ")";
                                $attribute_q = sqlquery($attribute_query);
                                $attribute = sqlfetch($attribute_q);

                                // IS CHECKOUT QUESTION
                                if ($attribute['checkout']) {
                                    unset($attrs[$attr_id]);
                                }

                            }

                            // GENERATE CODE FROM $attrs
                            $code = $this->item_variantCodeFromAttrs($attrs);

                            // HAVE CODE
                            if ($code) {

                                // HASH
                                $hash = md5($code);

                                // GET VARIANT
                                $var_query = "SELECT `inventory_available`, `inventory_unavailable`, `inventory_backordered` FROM `" .$this->tables['item_variants']. "` WHERE (`item_id`=" .$item_id. ") AND (`location_id`=" .(int)$location_id. ") AND (`hash`='" .$hash. "')";
                                $var_q = sqlquery($var_query);
                                $var = sqlfetch($var_q);

                                // USE VARIANT
                                $out['available']   = $var['inventory_available'];
                                $out['unavailable'] = $var['inventory_unavailable'];
                                $out['backordered'] = $var['inventory_backordered'];

                                if (($var['inventory_alert']) && ($var['inventory_available'] <= $var['inventory_alert_at'])) {
                                    $out['alerts']++;
                                }

                            // NO CODE
                            } else {

                                // USE ITEM INFO
                                $out['available']   = $item['inventory_available'];
                                $out['unavailable'] = $item['inventory_unavailable'];
                                $out['backordered'] = $item['inventory_backordered'];

                                if (($item['inventory_alert']) && ($item['inventory_available'] <= $item['inventory_alert_at'])) {
                                    $out['alerts']++;
                                }

                            }

                        // NO ATTRIBUTES SPECIFIED
                        } else {

                            // GET VARIANTS
                            $iv = $this->item_getVariants($item['id'])[$location_id];

                            // HAVE VARIANTS
                            if (count((array)$iv) > 0) {

                                foreach ($iv as $var) {
                                    $out['available']   += $var['inventory_available'];
                                    $out['unavailable'] += $var['inventory_unavailable'];
                                    $out['backordered'] += $var['inventory_backordered'];
                                    if (($var['inventory_alert']) && ($var['inventory_available'] <= $var['inventory_alert_at'])) {
                                        $out['alerts']++;
                                    }
                                }

                            // NO VARIANTS
                            } else {

                                // USE ITEM INFO
                                $out['available']   = $item['inventory_available'];
                                $out['unavailable'] = $item['inventory_unavailable'];
                                $out['backordered'] = $item['inventory_backordered'];

                                if (($item['inventory_alert']) && ($item['inventory_available'] <= $item['inventory_alert_at'])) {
                                    $out['alerts']++;
                                }

                            }

                        }

                    }

                }

            }

            return $out;

        }


        // CHANGE ITEM INVENTORY
        public function item_changeInventory($item_id, $location_id, $attrs=array(), $changes=array(), $settings=array()) {

            $out = array();

            $item_id = (int)$item_id;

            if ($item_id) {

                // GET ITEM INFO
                $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$item_id. ")";
                $item_q = sqlquery($item_query);
                $item = sqlfetch($item_q);

                // IS PACKAGE & TRACK / CHECK PACKAGE ITEM INVENTORY
                if (($item['type'] == 3) && ($item['package_track_items_inventory'] == 1)) {

                    // GET PACKAGE ITEMS
                    $pitems_query = "SELECT * FROM `" .$this->tables['item_package_items']. "` WHERE (`package_id`=" .$item['id']. ")";
                    $pitems_q = sqlquery($pitems_query);
                    while ($pitem = sqlfetch($pitems_q)) {

                        unset($pitem_attrs);

                        // IS VARIANT
                        if ($pitem['variant_id']) {

                            // GET VARIANT INFO
                            $var_query = "SELECT * FROM `" .$this->tables['item_variants']. "` WHERE (`id`=" .$pitem['variant_id']. ")";
                            $var_q = sqlquery($var_query);
                            $var = sqlfetch($var_q);

                            // GENERATE ATTRS
                            $pitem_attrs = $this->item_attrsFromVariantCode($var['code']);

                        // USE SPECIFIED ATTRIBUTES
                        } else {

                            // HAVE ATTRIBUTES FOR PACKAGE ITEM
                            if (is_array($attrs[$pitem['id']])) {
                                $pitem_attrs = $attrs[$pitem['id']];
                            }

                        }

                        // HAVE QUANTITY
                        if ($pitem['quantity'] > 0) {

                            // QUANTITY
                            $pitem_changes = array(
                                'available' => $pitem['quantity'] * -1
                            );

                        } else {
                            $pitem_changes = array();
                        }

                        // GET VARIATION INVENTORY
                        $var_new = $this->item_changeInventory($pitem['item_id'], $location_id, $pitem_attrs, $pitem_changes, $settings);

                        // RETURN INDIVIDUAL VARIANT INVENTORY INFO
                        $out['items'][$pitem['id']] = array_merge(array(
                            'item_id'       => $pitem['item_id'],
                            'variant_id'    => $pitem['variant_id'],
                            'quantity'      => $pitem['quantity']
                        ), $var_new);

                    }

                // OTHER ITEM TYPES
                } else {

                    // ATTRIBUTES SPECIFIED
                    if (count($attrs) > 0) {

                        // GENERATE CODE FROM $attrs
                        $code = $this->item_variantCodeFromAttrs($attrs);

                        // HAVE CODE
                        if ($code) {

                            // HASH
                            $hash = md5($code);

                            $var_query = "SELECT `id`, `inventory_available`, `inventory_unavailable`, `inventory_backordered` FROM `" .$this->tables['item_variants']. "` WHERE (`item_id`=" .$item_id. ") AND (`location_id`=" .(int)$location_id. ") AND (`hash`='" .$hash. "')";

                        }

                    // VARIANT ID SPECIFIED
                    } else if ($settings['variant_id']) {

                        $var_query = "SELECT `id`, `inventory_available`, `inventory_unavailable`, `inventory_backordered` FROM `" .$this->tables['item_variants']. "` WHERE (`id`=" .(int)$settings['variant_id']. ")";

                    }

                    // GOING BY VARIANT
                    if ($var_query) {

                        // GET VARIANT
                        $var_q = sqlquery($var_query);
                        $var = sqlfetch($var_q);

                        // OVERWRITING
                        if ($settings['overwrite']) {

                            $new = [
                                'available'   => $changes['available'],
                                'unavailable' => $changes['unavailable'],
                                'backordered' => $changes['backordered']
                            ];

                        // ADJUSTING
                        } else {

                            // NEW INVENTORY NUMBERS
                            $new = $this->inventory_addSubtract(array(
                                'available'     => $var['inventory_available'],
                                'unavailable'   => $var['inventory_unavailable'],
                                'backordered'   => $var['inventory_backordered']
                            ), $changes);

                        }

                        // DB COLUMNS
                        $columns = array(
                            'inventory_available'   => $new['available'],
                            'inventory_unavailable' => $new['unavailable'],
                            'inventory_backordered' => $new['backordered']
                        );

                        // UPDATE
                        $update_query = "UPDATE `" .$this->tables['item_variants']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .$var['id']. ")";
                        if (sqlquery($update_query)) {
                            $out['success'] = true;
                            $out['inventory'] = $new;
                        }

                    // NO ATTRIBUTES SPECIFIED
                    } else {

                        // OVERWRITING
                        if ($settings['overwrite']) {

                            $new = [
                                'available'   => $changes['available'],
                                'unavailable' => $changes['unavailable'],
                                'backordered' => $changes['backordered']
                            ];

                            print_r($new);
                            exit;

                        // ADJUSTING
                        } else {

                            // NEW INVENTORY NUMBERS
                            $new = $this->inventory_addSubtract(array(
                                'available'     => $item['inventory_available'],
                                'unavailable'   => $item['inventory_unavailable'],
                                'backordered'   => $item['inventory_backordered']
                            ), $changes);

                        }

                        // DB COLUMNS
                        $columns = array(
                            'inventory_available'   => $new['available'],
                            'inventory_unavailable' => $new['unavailable'],
                            'inventory_backordered' => $new['backordered']
                        );

                        // UPDATE
                        $update_query = "UPDATE `" .$this->tables['items']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .$item_id. ")";
                        if (sqlquery($update_query)) {
                            $out['success'] = true;
                            $out['inventory'] = $new;
                        }

                    }

                }

            }

            // NOTHING CHANGED
            if ((!$changes['available']) && (!$changes['unavailable']) && (!$changes['backordered'])) {
                $settings['log']['disabled'] = true; // DON'T LOG
            }

            // SUCCESSFUL AND LOGGING
            if (($out['success']) && ($settings['log']['disabled'] != true)) {

                $log_vars = array(
                    'item_id'           => $item_id,
                    'variant_id'        => (int)$var['id'],
                    'available'         => (int)$changes['available'],
                    'unavailable'       => (int)$changes['unavailable'],
                    'backordered'       => (int)$changes['backordered'],
                    'rb_available'      => (int)$new['available'],
                    'rb_unavailable'    => (int)$new['unavailable'],
                    'rb_backordered'    => (int)$new['backordered'],
                    'source'            => $settings['log']['source'],
                    'source_id'         => (int)$settings['log']['source_id']
                );

                // LOG
                $this->inventory_logChange($log_vars);

            }

            return $out;

        }


        // INVENTORY MATH
        public function inventory_addSubtract($orig=array(), $changes=array()) {

            $out = array(
                'available'   => (int)$orig['available'] + (int)$changes['available'],
                'unavailable' => (int)$orig['unavailable'] + (int)$changes['unavailable'],
                'backordered' => (int)$orig['backordered'] + (int)$changes['backordered']
            );

            // IS SUBTRACTING AVAILABLE
            if ((int)$changes['available'] < 0) {

                // MADE AVAILABLE GO NEGATIVE
                if ($out['available'] < 0) {

                    // BACKORDERING ENABLED
                    if ($this->getSetting('inventory_backorder')) {

                        // ADD DIFFERENCE TO BACKORDERED
                        $out['backordered']   += (((int)$orig['available'] + (int)$changes['available']) * -1);

                        // SET AVAILABLE TO 0
                        $out['available']     = 0;

                    }

                }

            }

            return $out;

        }


        // LOG INVENTORY
        public function inventory_logChange($vars) {
            if (!$vars['dt']) $vars['dt'] = date('Y-m-d H:i:s');
            $query = "INSERT INTO `" .$this->tables['inventory_log']. "` SET " .$this->createSet($vars);
            sqlquery($query);
        }


        // ITEM & ATTRIBUTE TITLES FOR DISPLAY
        public function item_attrAndOptionTitles($item, $attra, $config=array()) {

            $out = array();

            if (is_array($item['other']['options'])) {

                if (!$config['sep']) $config['sep'] = ', ';

                // LOOP THROUGH ATTRIBUTES
                foreach ($item['other']['options'] as $attr_id => $options) {

                    // HAVE AT LEAST ONE SELECTED VALUE
                    //if ($options[0]) {
                    if (count($options) > 0) {

                        $attr = $item['other']['attributes'][$attr_id];
                        $attr_type = $attr['type'];

                        $sep = $config['sep'];

                        $optta = array();

                        $oi = 0;
                        foreach ($options as $option_i => $option) {
                            if ($attr_type == 'people') {
                                if (!$option) continue;
                                $optta[$oi] = stripslashes($attr['options'][$option_i]['title']). ': ' .$option;
                                $sep = '<br />';
                            } else if (($attr_type == 'file') && ($config['file_download']) && ($config['order_id'])) {
                                $optta[$oi] = '<a href="./file_download/?order_id=' .$config['order_id']. '&file=' .urlencode($option). '" target="_blank">' .$option. '</a>';
                            } else if (($attr_type == 'checkbox') || ($attr_type == 'dropdown') || ($attr_type == 'select_box')) {
                                if (is_numeric($option)) {
                                    $optta[$oi] = stripslashes($attr['options'][$option]['title']);
                                } else {
                                    $optta[$oi] = $option;
                                }
                            } else {
                                $optta[$oi] = $option;
                            }
                            if ($config['show_markup']) {
                                $markup = number_format((float)$attr['options'][$option_i]['markup'], 2);
                                if ($markup == 0.00) {
                                    $optta[$oi] .= ' <span class="markup free">Free</span>';
                                } else {
                                    $optta[$oi] .= ' <span class="markup">$' .$markup . ($attr['description'] ? '<span class="description">' .stripslashes($attr['description']). '</span>' : ''). '</span>';
                                }
                            }
                            $oi++;
                        }

                        $out[$attr_id] = array(
                            'title'         => stripslashes($attra[$attr_id]['title']),
                            'options'       => implode($optta, $sep)
                        );

                        unset($optta);

                    }

                }


            }

            return $out;

        }


        #############################
        # PACKAGE
        #############################

        // GET PACKAGE ITEMS
        public function package_getItems($package_id=0) {

            $out = array();

            $package_id = (int)$package_id;

            if ($package_id) {

                // GET PACKAGE ITEMS
                $pitems_query = "SELECT * FROM `" .$this->tables['item_package_items']. "` WHERE (`package_id`=" .$package_id. ")";
                $pitems_q = sqlquery($pitems_query);
                while ($pitem = sqlfetch($pitems_q)) {
                    $out[$pitem['id']] = $pitem;
                }

            }

            return $out;

        }


        #############################
        # ASSETS
        #############################


        // RETURNS ARRAY OF MATCHING ASSETS
        public function getAssets($params=[], $options=[]) {
            $out = [];

            $page = (int)$params['page'];
            $limit = (int)$params['limit'];

            $serviceStart = ($params['serviceStart'] ? date('Y-m-d H:i:s', strtotime($params['serviceStart'])) : '');
            $serviceEnd = ($params['serviceEnd'] ? date('Y-m-d H:i:s', strtotime($params['serviceEnd'])) : '');

            if (!$page) $page = 1;
            if (!$limit) $limit = $this->perPage();

            $ij = [];
            $wa = [];

            $ij['items'] = "LEFT JOIN `" .$this->tables['items']. "` AS `i` ON a.item_id=i.id";
            $ij['customer_contacts'] = "LEFT JOIN `" .$this->tables['customer_contacts']. "` AS `cc` ON a.customer_id=cc.customer_id";

            // ID
            if ($params['id']) {
                $wa[] = "a.id=" .(int)$params['id'];
            }

            // STATUS
            if (strlen($params['status'])) {
                $wa[] = "a.status=" .(int)$params['status'];
            } else {
                $wa[] = "a.status!=9";
            }

            // START / END
            if (($serviceStart) && ($serviceEnd)) {
                $wa[] = "
                (a.service_start BETWEEN '" .$serviceStart. "' AND '" .$serviceEnd. "')
                OR
                (a.service_end BETWEEN '" .$serviceStart. "' AND '" .$serviceEnd. "')
                OR
                (('" .$serviceStart. "'>=a.service_start) AND ('" .$serviceEnd. "'<=a.service_end))
                ";
            } else if ($serviceStart) {
                $wa[] = "a.service_start>='" .$serviceStart. "'";
            } else if ($serviceEnd) {
                $wa[] = "a.service_end<='" .$serviceEnd. "'";
            }

            // KEYWORD
            if ($params['q']) {
                $q = strtolower(trim(sqlescape($params['q'])));
                $fa = [
                    'a.serial',
                    'a.notes',
                    'i.title',
                    'cc.firstname',
                    'cc.lastname',
                ];
                $wap = [];
                foreach ($fa as $field) {
                    $wap[] = "(LOWER(" .$field. ") LIKE '%" .$q. "%')";
                }
                $wa[] = implode(" OR ", $wap);
            }

            // ITEM ID
            if ($params['item_id']) {
                $wa[] = "a.item_id=" .(int)$params['item_id'];
            }

            // CUSTOMER ID
            if ($params['customer_id']) {
                $wa[] = "a.customer_id=" .(int)$params['customer_id'];
            }

            // CONTACT ID
            if ($params['contact_id']) {
                $wa[] = "a.contact_id=" .(int)$params['contact_id'];
            }

            if (count($ij) > 0) {
                $inner_join = "";
            } else {
                $inner_join = "";
            }

            $inner_join = (count($ij) > 0 ? implode(" ", $ij) : "");

            $where = "WHERE (" .implode(") AND (", $wa). ")";

            // BASE QUERY
            $query = "
            FROM `" .$this->tables['assets']. "` AS `a`
            " .$inner_join. "
            " .$where. "
            GROUP BY a.id
            ";

            // TOTAL
            $total = sqlfetch(sqlquery("SELECT COUNT('x') AS `total` " .$query))['total'];

            $customers = [];
            $items = [];

            // PAGED QUERY
            $query_paged = "
            SELECT a.*
            " .$query. "
            ORDER BY a.id ASC
            LIMIT " .(($page - 1) * $limit). "," .$limit. "
            ";
            $q = sqlquery($query_paged);
            while ($asset = sqlfetch($q)) {

                // INCLUDE CUSTOMER INFO
                if (in_array('customer', (array)$options['include'])) {

                    // DON'T HAVE CUSTOMER INFO YET
                    if (!$customers[$asset['customer_id']]['id']) {
                        $customers[$asset['customer_id']] = $this->getCustomer($asset['customer_id']);
                    }

                    $asset['customer'] = $customers[$asset['customer_id']];

                }

                // INCLUDE ITEM INFO
                if (in_array('item', (array)$options['include'])) {

                    // DON'T HAVE ITEM INFO YET
                    if (!$items[$asset['item_id']]['id']) {

                        // GET ITEM
                        $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$asset['item_id']. ")";
                        $item_q = sqlquery($item_query);
                        $item = sqlfetch($item_q);

                        if ($item['id']) {

                            // GET IMAGE
                            $image_query = "SELECT * FROM `" .$this->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC LIMIT 1";
                            $image = sqlfetch(sqlquery($image_query));

                            // HAVE IMAGE
                            if ($image['image']) {
                                $item['image'] = BigTree::prefixFile($this->imageBridgeOut($image['image'], 'items'), 't_');
                            } else {
                                $item['image'] = WWW_ROOT. 'extensions/' .$this->Extension. '/images/item_no-image.jpg';
                            }

                        }

                        $items[$asset['item_id']] = $item;

                    }

                    $asset['item'] = $items[$asset['item_id']];

                }

                // INCLUDE SCHEDULE INFO
                if (in_array('schedule', (array)$options['include'])) {

                    // GENERATE RECURRING RULE
                    $rrule = $this->rrule_generate(array_merge($asset, [
                        'end' => $asset['service_end'],
                    ]));

                    // TRY When
                    try {

                        $r = new When\When();
                        $r->RFC5545_COMPLIANT = When\When::IGNORE;

                        if ($options['schedule']['limit']) {
                            $r->count($options['schedule']['limit']);
                        }

                        if ($options['schedule']['future']) {
                            $schedule_start = date('Y-m-d H:i:s');
                        } else {
                            $schedule_start = $asset['service_start'];
                        }

                        // GET OCCURANCES
                        $r->startDate(new DateTime($schedule_start))->rrule($rrule)->generateOccurrences();

                        // HAVE OCCURRENCES
                        if (count($r->occurrences) > 0) {

                            // LOOP
                            foreach ($r->occurrences as $occ) {

                                $asset['schedule'][] = $occ->format('Y-m-d');

                            }

                        }

                    // When ERROR
                    } catch (Exception $e) {
                        //echo 'Caught exception: ',  $e->getMessage(), "\n";
                        $asset['schedule']['error'] = $e->getMessage();
                    }

                }

                $out['assets'][$asset['id']] = $asset;

            }

            $out['total'] = $total;
            $out['pages'] = $this->pageCount($total, $limit);
            $out['page'] = $page;

            return $out;
        }

        // RETURNS A SPECIFIC ASSET
        public function getAsset($id) {
            $id = (int)$id;
            return (array)$this->getAssets([ 'id' => $id ])['assets'][$id];
        }


        #############################
        # CART
        #############################


        // GET CART ID
        public function cartID() {
            return (int)$_COOKIE['ecommCartID'];
        }


        // CLEAR CART ID
        public function clearCartID($clear_session=false) {
            setcookie('ecommCartID', null, -1, '/');
            unset($_COOKIE['ecommCartID']);
            if ($clear_session) {
                unset($_SESSION['ecomm']['checkout']['fields']);
                unset($_SESSION['ecomm']['quote_request']['fields']);
            }
        }


        // INIT CART
        public function initCart($create=true, $cart_id=0, $admin=false, $options=[]) {
            global $_uccms;

            // CONVERT IP ADDRESS
            $ip = ip2long($_SERVER['REMOTE_ADDR']);

            if (!$options['noCookie']) {

                // HAVE COOKIE CART ID
                if (($cart_id == 0) && ($_COOKIE['ecommCartID'])) $cart_id = (int)$_COOKIE['ecommCartID'];

            }

            // HAVE CART ID
            if ($cart_id > 0) {

                // GET CART
                if ($admin) {
                    $cart_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$cart_id. ")";
                } else {
                    $cart_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$cart_id. ") AND ((`status`='cart') OR (`status`='failed') OR (`quote`=1)) AND (`ip`=" .$ip. ")";
                }
                $cart_q = sqlquery($cart_query);
                $cart = sqlfetch($cart_q);

            }

            // NO ORDER (CART) ID
            if ((!$cart['id']) && ($create == true)) {

                // CART DB VALUES
                $cart = array(
                    'dt'        => date('Y-m-d H:i:s'),
                    'status'    => 'cart',
                    'ip'        => $ip
                );

                // CREATE CART
                $cart_query = "INSERT INTO `" .$this->tables['orders']. "` SET " .$this->createSet($cart). "";
                if (sqlquery($cart_query)) {

                    // NEW CART ID
                    $cart['id'] = sqlid();

                }

            }

            // HAVE CART ID
            if ($cart['id']) {

                if (!$options['noCookie']) {

                    // SET COOKIE
                    setcookie('ecommCartID', $cart['id'], time() + (86400 * 7), '/'); // 7 days

                }

                // HAVE USER ID AND NOT ADMIN
                if ((!$admin) && ($_uccms['_account']->userID())) {

                    // UPDATE CART
                    $update_query = "UPDATE `" .$this->tables['orders']. "` SET `customer_id`=" .$_uccms['_account']->userID(). " WHERE (`id`=" .$cart['id']. ")";
                    sqlquery($update_query);

                }

            }

            // CONVERT IP BACK
            $cart['ip'] = long2ip($cart['ip']);

            return $cart;

        }


        // CART LOCATION ID
        public function cart_locationID() {
            $cart = $this->initCart();
            return (int)$cart['location_id'];
        }


        // GET CART EXTRA
        public function cartExtra($id) {
            $out = array();
            if ($this->tables['quote_' .$this->storeType()]) {
                $id = (int)$id;
                if ($id) {
                    $extra_q = sqlquery("SELECT * FROM `" .$this->tables['quote_' .$this->storeType()]. "` WHERE (`order_id`=" .$id. ")");
                    $out = sqlfetch($extra_q);
                }
            }
            return $out;
        }


        // CALCULATE HASH FOR ITEM IN CART
        public function cartItemHash($data) {
            if (is_array($data)) {
                $data = json_encode($data);
            }
            return md5($data);
        }


        // ADD ITEM TO CART
        public function cartAddItem($cart_id, $data, $admin=false, $test=false, $vars=[]) {
            global $_uccms;

            $out = array();

            if (($cart_id) && (is_array($data))) {

                // CLEAN UP
                $item_id        = (int)$data['item']['id'];
                $category_id    = (int)$data['item']['category_id'];
                $quantity       = (int)$data['item']['quantity'];

                // HAVE ITEM ID
                if ($item_id > 0) {

                    // GET ITEM INFO
                    $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$item_id. ")";
                    $item = sqlfetch(sqlquery($item_query));

                    $out['item'] = $item;

                    $num_people = 0;

                    // ATTRIBUTE ARRAY FOR ITEM
                    $attra = $this->itemAttributes($item_id);

                    // HAVE ATTRIBUTES
                    if (count($attra) > 0) {

                        // LOOP
                        foreach ($attra as $key => $attr) {

                            // IS PEOPLE
                            if (($attr['type'] == 'people') && (!$vars['ignore-people-quantity'])) {

                                // ADD UP PEOPLE
                                if (is_array($data['attribute'][$attr['id']])) {
                                    foreach ($data['attribute'][$attr['id']] as $opt_id => $opt_value) {
                                        $num_people += $opt_value;
                                    }
                                }

                            }

                        }

                        // USE PEOPLE FOR QUANTITY
                        if ($num_people > 0) {
                            $quantity = $num_people;
                        }

                    }

                    if (!$num_people) $num_people = $quantity;

                    // STORE TYPE IS BOOKING AND ITEM IS BOOKABLE
                    if (($this->storeType() == 'booking') && ($item['booking_enabled'])) {

                        // GET BOOKING ITEM INFO
                        $booking_item_query = "SELECT * FROM `" .$this->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
                        $booking_item_q = sqlquery($booking_item_query);
                        $booking_item = sqlfetch($booking_item_q);

                        // CUSTOMER SPECIFIED DURATION
                        if ($booking_item['duration_type'] == 1) {

                            // calculate from and to based off $data

                        // ADMIN SPECIFIED DURATION
                        } else {

                            $duration_id = (int)$data['booking']['duration_id'];

                            // HAVE DURATION ID
                            if ($duration_id) {

                                // GET DURATION
                                $booking_duration_query = "SELECT * FROM `" .$this->tables['booking_item_durations']. "` WHERE (`id`=" .$duration_id. ") AND (`item_id`=" .$item['id']. ")";
                                $booking_duration_q = sqlquery($booking_duration_query);
                                $booking_duration = sqlfetch($booking_duration_q);

                            }

                            // NO DURATION
                            if (!$booking_duration['id']) {

                                // GET FIRST DURATION
                                $booking_duration_query = "SELECT * FROM `" .$this->tables['booking_item_durations']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC, `id` ASC LIMIT 1";
                                $booking_duration_q = sqlquery($booking_duration_query);
                                $booking_duration = sqlfetch($booking_duration_q);

                            }

                            // FROM DATE / TIME SPECIFIED
                            if ($data['booking']['from']) {

                                $booking_from_timestamp = strtotime($data['booking']['from']);

                                switch ($booking_duration['unit']) {

                                    // MINUTES
                                    case 'm':
                                        $booking_to_timestamp = strtotime('+' .(int)$booking_duration['num']. ' Minutes', $booking_from_timestamp);
                                        break;

                                    // HOURS
                                    case 'h':
                                        $booking_to_timestamp = strtotime('+' .(int)$booking_duration['num']. ' Hours', $booking_from_timestamp);
                                        break;

                                    // DAYS
                                    case 'd':

                                        $booking_to_timestamp = strtotime('+' .((int)$booking_duration['num'] - 1). ' Days', $booking_from_timestamp);
                                        break;

                                }

                            } else {
                                $out['error'] = 'Date / time not specified.';
                                $missa[] = $out['error'];
                            }

                        }

                    }

                    // NOT ADMIN
                    if (!$admin) {

                        $pricegroup_id = 1;

                        $backorder_num = 0;

                        $missa = array(); // ARRAY OF MISSING FIELDS
                        $overa = array(); // ARRAY OF OVER LIMIT FIELDS

                        // PURCHASING ENABLED / DISABLED FOR ITEM
                        $purchasing_enabled = $this->purchasingEnabled($item);

                        // PURCHASING IS ENABLED
                        if ($purchasing_enabled) {

                            if (count($missa) == 0) {

                                // PRICE GROUPS ARE ENABLED
                                if ($this->getSetting('pricegroups_enabled')) {

                                    // GET ITEM'S MINIMUM QUANTITY
                                    $qtya = $this->itemQtyMin($item, true);

                                    // PRICE GROUP USED
                                    $pricegroup_id = $qtya['pricegroup_id'];

                                    // MINIMUM QUANTITY
                                    $qty_min = $qtya['qty_min'];

                                    // HAVE MINIMUM QUANTITY
                                    if ($qty_min > 0) {

                                        // QUANTITY IS LESS THAN MIN QUANTITY
                                        if ($quantity < $qty_min) {
                                            $missa[] = 'This item requires a minimum quanity of ' .number_format($qty_min, 0). '.';
                                        }

                                    }

                                }

                            }

                            if (count($missa) == 0) {

                                // INVENTORY CONTROL ENABLED
                                if ($this->getSetting('inventory_track')) {

                                    // GET INVENTORY
                                    $inventory = $this->item_getInventory($item_id, $this->cart_locationID(), $data['attribute']);

                                    // WHAT WILL INVENTORY BE AFTER THIS ITEM IS PURCHASED?
                                    $adjusted_available = $inventory['available'] - $quantity;

                                    // IS 0 OR NEGATIVE
                                    if ($adjusted_available < 0) {

                                        // BACKORDERING ENABLED
                                        if ($this->getSetting('inventory_backorder')) {
                                            $backorder_num = $adjusted_available * -1;
                                        } else {
                                            if (count((array)$data['attribute']) > 0) {
                                                $missa[] = 'Item is not available in this configuration & quantity. (In stock: ' .number_format((int)$inventory['available'], 0). ')';
                                            } else {
                                                $missa[] = 'Item is not available in this quantity. (In stock: ' .number_format((int)$inventory['available'], 0). ')';
                                            }
                                        }

                                    }

                                }

                            }

                            if (count($missa) == 0) {

                                // STORE TYPE IS BOOKING AND ITEM IS BOOKABLE
                                if (($this->storeType() == 'booking') && ($item['booking_enabled'])) {

                                    $avail_settings = array(
                                        'item_id'   => $item['id'],
                                        'from'      => $booking_from_timestamp,
                                        'to'        => $booking_to_timestamp
                                    );

                                    //echo print_r($avail_settings);
                                    //exit;

                                    if ($booking_item['calendar_id']) {
                                        $avail_settings['calendar_id'] = $booking_item['calendar_id'];
                                    }

                                    // GET AVAILABILITY
                                    $avail = $this->stc->availability($avail_settings);

                                    // SELECTED DURATION AVAILABILITY
                                    $avail_dur = $avail[date('Y-m-d', $booking_from_timestamp)]['items'][$item['id']]['durations'][$booking_duration['id']][$data['booking']['duration_i']];

                                    // AVAILABLE IS LESS THAN QUANTITY
                                    if ($avail_dur['available'] < $quantity) {
                                        if ($avail_dur['available'] > 0) {
                                            $missa[] = 'Only ' .number_format($avail_dur['available'], 0). ' ' .stripslashes($item['title']) . ($avail_dur['available'] != 1 ? 's' : ''). ' available on this day / time.';
                                        } else {
                                            $missa[] = stripslashes($item['title']). ' is not available on this day / time.';
                                        }
                                    }

                                }

                            }

                            // HAVE ATTRIBUTES
                            if (count($attra) > 0) {

                                // LOOP
                                foreach ($attra as $key => $attr) {

                                    // IS FILE
                                    if ($attr['type'] == 'file') {

                                        $remove_file = '';

                                        // UPLOAD CLASS
                                        require_once(SERVER_ROOT. 'uccms/includes/classes/upload.php');

                                        // INIT UPLOAD CLASS
                                        $_upload = new upload();

                                        // FILE NAME
                                        $file_name = $_upload->getName('attribute-' .$attr['id']);

                                        // REMOVE FILE SPECIFIED
                                        if ($data['attribute-' .$attr['id']. '-remove']) {
                                            $remove_file = str_replace(array('..', '/', '\/'), '', $data['attribute-' .$attr['id']. '-existing']);
                                        }

                                        // DEFAULT - FILE NOT MISSING
                                        $file_missing = false;

                                        // NO FILE UPLOADED
                                        if ($file_name) {
                                            if (($data['attribute-' .$attr['id']. '-existing']) && (($file_name != $data['attribute-' .$attr['id']. '-existing']))) {
                                                $remove_file = str_replace(array('..', '/', '\/'), '', $data['attribute-' .$attr['id']. '-existing']);
                                            }

                                        // NO FILE UPLOADED
                                        } else {
                                            $file_missing = true;
                                            if ($attr['required']) {
                                                $missa[$attr['id']] = 'Missing field: ' .stripslashes($attr['title']);
                                            }
                                        }

                                        // REMOVING FILE
                                        if ($remove_file) {
                                            @unlink(SITE_ROOT. 'extensions/' .$this->Extension. '/files/orders/' .$cart_id. '-' .$remove_file);
                                        }

                                        // FILE IS NOT MISSING
                                        if (!$file_missing) {

                                            // HAVE REQUIRED EXTENSIONS
                                            if ($attr['file_types']) {

                                                // ALLOWED FILE TYPES ARRAY
                                                $fta = explode(',', stripslashes($attr['file_types']));

                                                // FILE EXTENSION
                                                $ext = $_upload->getExt('attribute-' .$attr['id']);

                                                // FILE EXTENSION NOT ALLOWED
                                                if (!in_array($ext, $fta)) {
                                                    $file_missing = true;
                                                    $missa[$attr['id']] = 'File type not allowed for: ' .stripslashes($attr['title']);

                                                }

                                            }

                                            // FILE STILL NOT MISSING
                                            if (!$file_missing) {

                                                // WHERE TO PUT FILE
                                                $file_loc = SITE_ROOT. 'extensions/' .$this->Extension. '/files/orders/' .$cart_id. '-' .$file_name;

                                                // UPLOAD
                                                $result = $_upload->doUpload('attribute-' .$attr['id'], $file_loc);

                                                // UPLOAD WENT OK
                                                if ($result == 'ok') {
                                                    $data['attribute'][$attr['id']][] = $file_name;

                                                // UPLOAD FAILED
                                                } else {
                                                    $missa[$attr['id']] = 'Failed to upload file: ' .stripslashes($attr['title']). ' (' .$result. ')';
                                                }

                                            }

                                        }

                                    // IS PEOPLE
                                    } else if ($attr['type'] == 'people') {

                                        // IS LIMITED
                                        if ($attr['limit']) {

                                            // OVER LIMIT
                                            if ($num_people > $attr['limit']) {
                                                $overa[$attr['id']] = 'Over limit: ' .stripslashes($attr['title']);
                                            }

                                        }

                                        // IS REQUIRED
                                        if (($attr['required']) && ($num_people < 1)) {
                                            $missa[$attr['id']] = 'Item must include at least 1 person.';
                                        }

                                    // EVERY OTHER ATTRIBUTE
                                    } else {

                                        // IS LIMITED OR REQUIRED
                                        if (($attr['limit']) || ($attr['required'])) {

                                            // IS LIMITED
                                            if ($attr['limit']) {

                                                // OVER LIMIT
                                                if (count((array)$data['attribute'][$attr['id']]) > $attr['limit']) {
                                                    $overa[$attr['id']] = 'Over limit: ' .stripslashes($attr['title']);
                                                }

                                            }

                                            // IS REQUIRED
                                            if ($attr['required']) {

                                                /*
                                                MATTD: Not sure how to go about checking for multiple uses of the same
                                                attribute used (say they used "meat type" twice) and validating each,
                                                so defaulting to the first [0] in the array.
                                                */

                                                // FIELD VALUE
                                                if (is_array($data['attribute'][$attr['id']][0])) { // IS ARRAY
                                                    $val = $data['attribute'][$attr['id']];
                                                } else {
                                                    $val = trim($data['attribute'][$attr['id']][0]); // IS STRING
                                                }

                                                // HAS VALUE
                                                if ($val) {

                                                    // IS INCORRECT
                                                    if (!$this->checkRequired($val, $attr['required'])) {
                                                        $missa[$attr['id']] = 'Incorrect field: ' .stripslashes($attr['title']);
                                                    }

                                                // MISSING VALUE
                                                } else {
                                                    $missa[$attr['id']] = 'Missing field: ' .stripslashes($attr['title']);
                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        // PURCHASING NOT ENABLED
                        } else {
                            $missa[] = 'This item is not available for purchase.';
                        }

                        // HAVE MISSING / OVER LIMIT FIELDS
                        if ((count($missa) > 0) || (count($overa) > 0)) {
                            foreach ($missa as $attr_id => $message) {
                                $_uccms['_site-message']->set('error', $message);
                                $out['error'][] = $message;
                            }
                            foreach ($overa as $attr_id => $message) {
                                $_uccms['_site-message']->set('error', $message);
                                $out['error'][] = $message;
                            }
                            $_SESSION['ecomm']['item']['attributes'] = $data['attribute'];
                            if (!$test) {
                                echo '<script>history.back();</script>';
                                exit;
                            }
                        }

                    }

                    // HAVE ERRORS
                    if (count((array)$out['error']) > 0) {
                        return $out;
                    }

                    if ($test) return true;

                    // HAVE ORDER INFO
                    if (count((array)$data['order']) > 0) {

                        // ALLOWED FIELDS
                        $ofa = array(
                            'billing_firstname',
                            'billing_lastname',
                            'billing_company',
                            'billing_address1',
                            'billing_address2',
                            'billing_city',
                            'billing_state',
                            'billing_zip',
                            'billing_country',
                            'billing_phone',
                            'billing_email',
                            'shipping_same',
                            'shipping_firstname',
                            'shipping_lastname',
                            'shipping_company',
                            'shipping_address1',
                            'shipping_address2',
                            'shipping_city',
                            'shipping_state',
                            'shipping_zip',
                            'shipping_country',
                            'shipping_phone',
                            'shipping_email',
                            'shipping_service',
                            'notes'
                        );

                        // DB COLUMNS
                        $columns = array();

                        // LOOP THROUGH ORDER INFO
                        foreach ($data['order'] as $name => $value) {

                            // IS ALLOWED FIELD
                            if (in_array($name, $ofa)) {

                                // ADD TO DB COLUMNS
                                $columns[$name] = $data['order'][$name];

                                // WEB TRACKING
                                switch ($name) {

                                    // FIRSTNAME
                                    case 'billing_firstname':
                                        $_SESSION['uccmsAcct_webTracking']['vars']['p:firstname'] = $value;
                                        break;

                                    // LASTNAME
                                    case 'billing_lastname':
                                        $_SESSION['uccmsAcct_webTracking']['vars']['p:lastname'] = $value;
                                        break;

                                    // EMAIL
                                    case 'billing_email':
                                        $_SESSION['uccmsAcct_webTracking']['vars']['p:email'] = $value;
                                        break;

                                    // PHONE
                                    case 'billing_phone':
                                        $_SESSION['uccmsAcct_webTracking']['vars']['p:phone'] = $value;
                                        break;

                                }

                            }

                        }

                        // HAVE COLUMNS
                        if (count($columns) > 0) {

                            // UPDATE CART
                            $cart_query = "UPDATE `" .$this->tables['orders']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .$cart_id. ")";
                            sqlquery($cart_query);

                        }

                    }

                    // HAVE REFERRER HOST
                    if ($data['referrer_host']) {

                        $referrer_host = preg_replace("/[^0-9a-zA-Z-.]/", '', $data['referrer_host']);

                        // UPDATE ORDER
                        $update_query = "UPDATE `" .$this->tables['orders']. "` SET `referrer_host`='" .$referrer_host. "' WHERE (`id`=" .$cart_id. ")";
                        sqlquery($update_query);

                    }

                    $oitem_id = 0;

                    // ENCODE ATTRIBUTES
                    if (is_array($data['attribute'])) {
                        $options = json_encode($data['attribute']);
                    } else {
                        $options = '';
                    }

                    // DEFAULT TO ADDING
                    $add_item = true;

                    // DB COLUMNS
                    $columns = array(
                        'cart_id'       => $cart_id,
                        'item_id'       => $item_id,
                        'category_id'   => $category_id,
                        'pricegroup_id' => $pricegroup_id,
                        'options'       => $options,
                        'quantity'      => $quantity,
                        'after_tax'     => $item['after_tax'],
                        'quote'         => $item['quote'],
                        'backorder_num' => $backorder_num
                    );

                    // IS ADMIN
                    if ($admin) {
                        $columns['override_price'] = ($_POST['override_price'] != '') ? $this->toFloat($_POST['override_price']) : '';
                    }

                    // HAVE EXTRA
                    if (count((array)$data['extra']) > 0) {
                        $columns['extra'] = json_encode($data['extra']);
                    } else {
                        $columns['extra'] = '';
                    }

                    // WAS EXISTING ITEM IN CART
                    if ($data['ciid']) {

                        // UPDATE
                        $update_query = "UPDATE `" .$this->tables['order_items']. "` SET " .$this->createSet($columns). ", `dt`=NOW() WHERE (`id`=" .(int)sqlescape($data['ciid']). ") AND (`cart_id`=" .$cart_id. ")";
                        if (sqlquery($update_query)) {
                            $out['success'] = true;
                            $out['message'] = 'Item updated.';
                            $add_item = false;
                            $oitem_id = (int)sqlescape($data['ciid']);
                        }

                    }

                    // IS ADDING
                    if ($add_item) {

                        // CREATE HASH
                        $cart_item_hash = $this->cartItemHash(array(
                            'category_id'   => $category_id,
                            'options'       => $data['attribute'],
                            'booking'       => array(
                                'duration_id'   => $booking_duration['id'],
                                'duration_i'    => (int)$data['booking']['duration_i'],
                                'from'          => $data['booking']['from'],
                                'to'            => $data['booking']['to']
                            )
                        ));

                        // IS ADMIN
                        if ($admin) {
                            $match['id'] = false;

                        // NOT ADMIN
                        } else {

                            // SEE IF MATCHING ITEM EXISTS
                            //$match_query = "SELECT * FROM `" .$this->tables['order_items']. "` WHERE (`cart_id`=" .$cart_id. ") AND (`item_id`=" .$item_id. ") AND (`category_id`=" .$category_id. ") AND (`options`='" .$options. "')";
                            $match_query = "SELECT * FROM `" .$this->tables['order_items']. "` WHERE (`cart_id`=" .$cart_id. ") AND (`item_id`=" .$item_id. ") AND (`cart_hash`='" .$cart_item_hash. "')";
                            $match_q = sqlquery($match_query);
                            $match = sqlfetch($match_q);

                        }

                        // ITEM ALREADY IN CART
                        if ($match['id']) {

                            // DON'T CHANGE QUANTITY IF ALREADY IN CART
                            if (($data['no-quantity-if-in-cart']) || ($vars['no-quantity-if-in-cart'])) {
                                $quantity = 0;
                            }

                            // UPDATE QUANTITY
                            $update_query = "UPDATE `" .$this->tables['order_items']. "` SET `quantity`=`quantity`+" .$quantity. ", `dt`=NOW() WHERE (`id`=" .$match['id']. ")";
                            if (sqlquery($update_query)) {
                                $out['success'] = true;
                                $out['message'] = 'Item quantity updated.';
                                $oitem_id = $match['id'];
                            }

                        // ITEM NOT IN CART YET
                        } else {

                            $columns['cart_hash'] = $cart_item_hash;

                            // ADD ITEM TO CART
                            $add_query = "INSERT INTO `" .$this->tables['order_items']. "` SET " .$this->createSet($columns). ", `dt`=NOW()";
                            if (sqlquery($add_query)) {
                                $out['success'] = true;
                                $out['message'] = 'Item added to cart.';
                                $oitem_id = sqlid();
                            }

                        }

                    }

                    $out['oitem_id'] = $oitem_id;

                    // STORE TYPE IS BOOKING AND ITEM IS BOOKABLE
                    if (($this->storeType() == 'booking') && ($item['booking_enabled'])) {

                        // HAVE ORDER ITEM ID
                        if ($oitem_id) {

                            // SEE IF WE ALREADY HAVE BOOKING INFO IN DB
                            $booking_query = "SELECT * FROM `" .$this->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$oitem_id. ")";
                            $booking_q = sqlquery($booking_query);
                            $booking = sqlfetch($booking_q);

                            $booking_columns = array(
                                'order_id'      => $cart_id,
                                'oitem_id'      => $oitem_id,
                                'item_id'       => $item['id'],
                                'duration_id'   => $booking_duration['id'],
                                'duration_i'    => (int)$data['booking']['duration_i'],
                                'markup'        => $booking_duration['markup'],
                                //'cart_data' => ''
                            );

                            if ($booking_from_timestamp) {
                                $booking_columns['dt_from'] = date('Y-m-d H:i:s', $booking_from_timestamp);
                            }

                            if ($booking_to_timestamp) {
                                $booking_columns['dt_to'] = date('Y-m-d H:i:s', $booking_to_timestamp - 1);
                            }

                            // HAVE BOOKING INFO
                            if ($booking['id']) {

                                $update_query = "UPDATE `" .$this->tables['booking_bookings']. "` SET " .$this->createSet($booking_columns). " WHERE (`id`=" .$booking['id']. ")";
                                sqlquery($update_query);

                            // NEW BOOKING INFO
                            } else {

                                $create_query = "INSERT INTO `" .$this->tables['booking_bookings']. "` SET " .$this->createSet($booking_columns). "";
                                sqlquery($create_query);

                                $booking['id'] = sqlid();

                            }

                            $out['booking_id'] = $booking['id'];

                        }

                    }

                // NO ITEM ID
                } else {
                    $out['success'] = false;
                    $out['message'] = 'No item ID specified.';
                }

            // MISSING INFO
            } else {
                $out['success'] = false;
                $out['message'] = 'Information missing to add item to cart.';
            }

            return $out;

        }


        // GET CART ITEMS
        public function cartItems($cart_id, $quote=false, $order="`id` ASC") {
            $out = array();
            $_uccms_ecomm = $this;
            if ($quote) $quote_sql = " AND (`quote`=1)";
            $oitems_query = "SELECT * FROM `" .$this->tables['order_items']. "` WHERE (`cart_id`=" .(int)$cart_id. ")" .$quote_sql. " ORDER BY " .sqlescape($order);
            $oitems_q = sqlquery($oitems_query);
            $order = [];
            while ($oitem = sqlfetch($oitems_q)) {

                // GET ITEM DATA
                include(SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/routed/shop/data/item_cart.php');

                // GET OPTIONS AND TITLES
                $oitem['options-formatted'] = $_uccms_ecomm->item_attrAndOptionTitles($item, $attra);

                $out[] = $oitem;
            }
            return $out;
        }


        // UPDATE QUANTITY
        public function cartItemQuantity($record_id, $quantity=0, $cart_id=0) {
            global $_uccms;

            $out = array();

            // CLEAN UP
            $record_id  = (int)$record_id;
            $quantity   = (int)$quantity;
            $cart_id    = (int)$cart_id;

            // HAVE ITEM ID
            if ($record_id) {

                // CART ID
                if (!$cart_id) $cart_id = $this->cartID();

                // HAVE QUANTITY
                if ($quantity > 0) {

                    $ok = true;

                    $backorder_num = 0;

                    // GET RECORD'S INFO
                    $ri_query = "SELECT * FROM `" .$this->tables['order_items']. "` WHERE (`id`=" .$record_id. ") AND (`cart_id`=" .$cart_id. ")";
                    $ri_q = sqlquery($ri_query);
                    $ri = sqlfetch($ri_q);

                    // GET ITEM INFO
                    $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$ri['item_id']. ")";
                    $item = sqlfetch(sqlquery($item_query));

                    $out['item'] = $item;

                    // PRICE GROUPS ARE ENABLED
                    if ($this->getSetting('pricegroups_enabled')) {

                        // GET ITEM'S MINIMUM QUANTITY
                        $qty_min = $this->itemQtyMin($item);

                        // HAVE MINIMUM QUANTITY
                        if ($qty_min > 0) {

                            // QUANTITY IS LESS THAN MIN QUANTITY
                            if ($quantity < $qty_min) {
                                $ok = false;
                                $out['success'] = false;
                                $out['message'] = 'Item requires a minimum quanity of ' .number_format($qty_min, 0). '.';
                            }

                        }

                    }

                    // INVENTORY CONTROL ENABLED
                    if ($this->getSetting('inventory_track')) {

                        $location_id = 0;

                        if ($ri['options']) {
                            $item['attributes'] = json_decode(stripslashes($ri['options']), true);
                        }

                        // GET INVENTORY
                        $inventory = $this->item_getInventory($item['id'], $location_id, $item['attributes']);

                        // WHAT WILL INVENTORY BE AFTER THIS ITEM IS PURCHASED?
                        $adjusted_available = $inventory['available'] - $quantity;

                        // IS 0 OR NEGATIVE
                        if ($adjusted_available < 0) {

                            // BACKORDERING ENABLED
                            if ($this->getSetting('inventory_backorder')) {
                                $backorder_num = $adjusted_available * -1;

                            // BACKORDERING NOT ENABLED
                            } else {

                                $ok = false;
                                $out['success'] = false;

                                if (count($item['attributes']) > 0) {
                                    $out['message'] = 'Item is not available in this configuration & quantity. (In stock: ' .number_format((int)$inventory['available'], 0). ')';
                                } else {
                                    $out['message'] = 'Item is not available in this quantity. (In stock: ' .number_format((int)$inventory['available'], 0). ')';
                                }

                            }

                        }

                    }

                    // STORE TYPE IS BOOKING AND ITEM IS BOOKABLE
                    if (($this->storeType() == 'booking') && ($item['booking_enabled'])) {

                        // GET BOOKING INFO
                        $booking_query = "SELECT * FROM `" .$this->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$ri['id']. ")";
                        $booking_q = sqlquery($booking_query);
                        $booking = sqlfetch($booking_q);

                        // GET BOOKING ITEM INFO
                        $booking_item_query = "SELECT * FROM `" .$this->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
                        $booking_item_q = sqlquery($booking_item_query);
                        $booking_item = sqlfetch($booking_item_q);

                        // CUSTOMER SPECIFIED DURATION
                        if ($booking_item['duration_type'] == 1) {

                            // calculate from and to based off $data

                        // ADMIN SPECIFIED DURATION
                        } else {

                            // HAVE DURATION ID
                            if ($booking['duration_id']) {

                                // GET DURATION
                                $booking_duration_query = "SELECT * FROM `" .$this->tables['booking_item_durations']. "` WHERE (`id`=" .$booking['duration_id']. ") AND (`item_id`=" .$item['id']. ")";
                                $booking_duration_q = sqlquery($booking_duration_query);
                                $booking_duration = sqlfetch($booking_duration_q);

                            }

                            // NO DURATION
                            if (!$booking_duration['id']) {

                                // GET FIRST DURATION
                                $booking_duration_query = "SELECT * FROM `" .$this->tables['booking_item_durations']. "` WHERE (`item_id`=" .$booking['duration_id']. ") ORDER BY `sort` ASC, `id` ASC LIMIT 1";
                                $booking_duration_q = sqlquery($booking_duration_query);
                                $booking_duration = sqlfetch($booking_duration_q);

                            }

                            // FROM DATE / TIME SPECIFIED
                            if ($booking['dt_from']) {

                                $booking_from_timestamp = strtotime($booking['dt_from']);

                                switch ($booking_duration['unit']) {

                                    // MINUTES
                                    case 'm':
                                        $booking_to_timestamp = strtotime('+' .(int)$booking_duration['num']. ' Minutes', $booking_from_timestamp);
                                        break;

                                    // HOURS
                                    case 'h':
                                        $booking_to_timestamp = strtotime('+' .(int)$booking_duration['num']. ' Hours', $booking_from_timestamp);
                                        break;

                                    // DAYS
                                    case 'd':

                                        $booking_to_timestamp = strtotime('+' .((int)$booking_duration['num'] - 1). ' Days', $booking_from_timestamp);
                                        break;

                                }

                            } else {
                                $missa[] = 'Date / time not specified.';
                            }

                        }

                        $avail_settings = array(
                            'item_id'   => $item['id'],
                            'from'      => $booking_from_timestamp,
                            'to'        => $booking_to_timestamp
                        );

                        //echo print_r($avail_settings);
                        //exit;

                        if ($booking_item['calendar_id']) {
                            $avail_settings['calendar_id'] = $booking_item['calendar_id'];
                        }

                        // GET AVAILABILITY
                        $avail = $this->stc->availability($avail_settings);

                        //echo print_r($avail);
                        //exit;

                        // SELECTED DURATION AVAILABILITY
                        $avail_dur = $avail[date('Y-m-d', $booking_from_timestamp)]['items'][$item['id']]['durations'][$booking['duration_id']][$booking['duration_i']];

                        // AVAILABLE IS LESS THAN QUANTITY
                        if ($avail_dur['available'] < $quantity) {
                            $ok = false;
                            $out['success'] = false;
                            $out['message'] = 'Max available for this day / time is: ' .number_format((int)$avail_dur['available'], 0). '.';
                        }

                    }

                    /*
                    echo date('Y-m-d H:i:s', $booking_from_timestamp);
                    echo '<br />';
                    echo date('Y-m-d H:i:s', $booking_to_timestamp);
                    exit;
                    */

                    // OK TO PROCEED
                    if ($ok) {

                        // UPDATE QUANTITY
                        $update_query = "UPDATE `" .$this->tables['order_items']. "` SET `quantity`=" .$quantity. ", `dt`=NOW(), `backorder_num`=" .$backorder_num. " WHERE (`id`=" .$record_id. ") AND (`cart_id`=" .$cart_id. ")";
                        if (sqlquery($update_query)) {
                            $out['success'] = true;
                            $out['message'] = 'Item quantity updated.';
                        }

                    }

                // NO QUANTITY
                } else {

                    // REMOVE ITEM
                    $out = $this->cartRemoveItem($record_id, $cart_id);

                }

                $out['order_id'] = $cart_id;

            // MISSING INFO
            } else {
                $out['success'] = false;
                $out['message'] = 'Item not specified to update quantity for.';
            }

            return $out;

        }


        // PROCESS CART ITEM ATTRIBUTES
        public function cartItemAttributes($oitem, $attra=array()) {

            $out = array();
            $out['item_markup'] = 0;

            //echo print_r($oitem);

            // ATTRIBUTES
            if ($oitem['options']) {

                if (is_array($oitem['options'])) {
                    $out['item_options'] = $oitem['options'];
                } else {
                    $out['item_options'] = json_decode(stripslashes($oitem['options']), true);
                }

                // HAVE OPTIONS
                if (count($out['item_options']) > 0) {

                    // LOOP
                    foreach ($out['item_options'] as $option_id => $values) {

                        // NOT IN ATTRIBUTE ARRAY YET
                        if (!$attra[$option_id]) {

                            // GET ATTRIBUTE INFO
                            $attr_query = "SELECT * FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .(int)$option_id. ")";
                            $attr_q = sqlquery($attr_query);
                            while ($attr = sqlfetch($attr_q)) {
                                $attra[$attr['id']] = $attr;
                                $out['attra'][$attr['id']] = $attr;
                                if ($attr['options']) {
                                    $out['attra'][$attr['id']]['options'] = json_decode(stripslashes($attr['options']), true);
                                    $attra[$option_id]['options'] = $out['attra'][$attr['id']]['options'];
                                }
                            }

                        }

                        // ATTRIBUTE HAS OPTIONS
                        if (is_array($attra[$option_id]['options'])) {

                            // PEOPLE ATTRIBUTE
                            if ($attra[$option_id]['type'] == 'people') {

                                $pm = 0;
                                foreach ($values as $opt_id => $value) { // LOOP THROUGH OPTIONS & VALUES
                                    $value = (int)$value;
                                    if ($value > 0) {
                                        $opt = $attra[$option_id]['options'][$opt_id];
                                        $pm += $opt['markup'] * $value;
                                    }
                                }

                                if ($pm > 0) {
                                    $out['item_markup'] += $pm / $oitem['quantity'];
                                }


                            // ALL OTHER ATTRIBUTES
                            } else {

                                foreach ($values as $value) { // LOOP THROUGH SELECTED VALUES
                                    foreach ($attra[$option_id]['options'] as $opt_id => $opt) { // LOOP THROUGH ATTRIBUTE OPTIONS
                                        $out['attra'][$attr['id']]['option_titles'][$id] = $opt['title'];
                                        if (is_numeric($value)) {
                                            if ($opt_id == $value) { // IS MATCH
                                                if ($opt['markup']) { // HAS MARKUP
                                                    $out['item_markup'] += $opt['markup']; // ADD TO PRICE
                                                }
                                            }
                                        } else if (stripslashes($opt['title']) == stripslashes($value)) { // IS MATCH
                                            if ($opt['markup']) { // HAS MARKUP
                                                $out['item_markup'] += $opt['markup']; // ADD TO PRICE
                                            }
                                        }
                                    }
                                }

                            }

                        }

                    }

                }

            }

            return $out;

        }


        // REMOVE ITEM FROM CART
        public function cartRemoveItem($record_id, $cart_id=0) {

            $out = array();

            // CLEAN UP
            $record_id = (int)$record_id;

            // HAVE RECORD ID
            if ($record_id) {

                // CART ID
                if (!$cart_id) $cart_id = $this->cartID();

                // GET ITEM INFO
                $item_query = "
                SELECT oi.*, i.title
                FROM `" .$this->tables['order_items']. "` AS `oi`
                INNER JOIN `" .$this->tables['items']. "` AS `i` ON oi.item_id=i.id
                WHERE (oi.id=" .$record_id. ") AND (oi.cart_id=" .$cart_id. ")
                ";
                $item_q = sqlquery($item_query);
                $item = sqlfetch($item_q);

                $out['item'] = $item;

                // ITEM FOUND
                if ($item['id']) {

                    // ATTRIBUTE ARRAY FOR ITEM
                    $attra = $this->itemAttributes($item['item_id']);

                    // HAVE ATTRIBUTES
                    if (count($attra) > 0) {

                        // OPTIONS
                        $options = json_decode(stripslashes($item['options']), true);

                        // HAVE OPTIONS TOO
                        if (count($options) > 0) {

                            // LOOP
                            foreach ($attra as $attr) {

                                // IS FILE
                                if ($attr['type'] == 'file') {

                                    // HAVE OPTIONS FOR ATTRIBUTE
                                    if (is_array($options[$attr['id']])) {

                                        // LOOP
                                        foreach ($options[$attr['id']] as $file) {
                                            @unlink(SITE_ROOT. 'extensions/' .$this->Extension. '/files/orders/' .$cart_id. '-' .$file);
                                        }

                                    }

                                }

                            }

                        }

                    }

                    // REMOVE ITEM
                    $delete_query = "DELETE FROM `" .$this->tables['order_items']. "` WHERE (`id`=" .$record_id. ") AND (`cart_id`=" .$cart_id. ")";
                    if (sqlquery($delete_query)) {

                        $out['success'] = true;
                        $out['message'] = 'Item removed.';

                        // DELETE BOOKING INFO
                        $booking_query = "DELETE FROM `" .$this->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$record_id. ")";
                        sqlquery($booking_query);

                    }

                // ITEM NOT FOUND
                } else {
                    $out['success'] = false;
                    $out['message'] = 'Item not found to remove.';
                }

            // MISSING INFO
            } else {
                $out['success'] = false;
                $out['message'] = 'Item not specified to remove.';
            }

            return $out;

        }


        // CALCULATE CART DISCOUNT
        public function cartDiscount($order, $citems, $vars=array()) {

            // HAVE SET AMOUNT AND NO COUPON
            if (($order['order_discount'] && $order['order_discount'] != 0.00) && ($order['coupon_id'] == 0)) {
                return [
                    'discount' => $order['order_discount']
                ];
            }

            if (!$order['order_subtotal']) $order['order_subtotal'] = $order['subtotal'];

            // GET GLOBAL
            $global_discount = $this->getSettings(array(
                'discount_global_quantity_type',
                'discount_global_quantity_amount',
                'discount_global_quantity_quantity',
                'discount_global_quantity_repeat',
                'discount_global_total_type',
                'discount_global_total_amount',
                'discount_global_total_total'
            ));

            //echo print_r($global_discount);

            // NOT IGNORING GLOBAL DISCOUNT
            if (!$vars['ignore']['global']) {

                // DISCOUNT BY QUANTITY
                if ($global_discount['discount_global_quantity_amount'] > 0) {

                    // QUANTITY OF INDIVIDUAL ITEMS
                    $quantity = 0;
                    foreach ($citems as $item) {
                        $quantity += $item['quantity'];
                    }

                    // QUANTITY MATCHES
                    if ($quantity >= $global_discount['discount_global_quantity_quantity']) {
                        if ($global_discount['discount_global_quantity_type'] == 'amount') {
                            $out['discount'] += $global_discount['discount_global_quantity_amount'] * ($global_discount['discount_global_quantity_repeat'] ? floor($quantity / $global_discount['discount_global_quantity_quantity']) : 1);
                        } else {
                            $out['discount'] += ($order['order_subtotal'] * $global_discount['discount_global_quantity_amount']) / 100;
                        }
                    }

                }

                // DISCOUNT BY AMOUNT
                if ($global_discount['discount_global_total_amount'] > 0) {

                    // AMOUNT MATCHES
                    if ($this->toFloat($order['order_subtotal']) >= $this->toFloat($global_discount['discount_global_total_total'])) {
                        if ($global_discount['discount_global_total_type'] == 'amount') {
                            $out['discount'] += $this->toFloat($global_discount['discount_global_total_amount']);
                        } else {
                            $out['discount'] += ($this->toFloat($order['order_subtotal']) * $this->toFloat($global_discount['discount_global_total_amount'])) / 100;
                        }
                    }

                }

            }

            // CATEGORY ARRAY
            $cata = array();

            // ITEM ARRAY
            $itema = array();

            // GET INFO FROM ITEMS
            foreach ($citems as $oitem) {
                if ($oitem['category_id']) {
                    $cata[$oitem['category_id']]['id'] = $oitem['category_id'];
                    $cata[$oitem['category_id']]['quantity'] += $oitem['quantity'];
                }
                $itema[$oitem['item_id']]['id'] = $oitem['item_id'];
                $itema[$oitem['item_id']]['quantity'] += $oitem['quantity'];
                if ($oitem['override_price']) {
                    if ($oitem['category_id']) {
                        $cata[$oitem['category_id']]['total'] += $this->toFloat($oitem['override_price']) * $oitem['quantity'];
                    }
                    $itema[$oitem['item_id']]['total'] += $this->toFloat($oitem['override_price']) * $oitem['quantity'];
                } else {
                    $_uccms_ecomm = $this;
                    include(SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/routed/shop/data/item_cart.php');
                    if ($oitem['category_id']) {
                        $cata[$oitem['category_id']]['total'] += $this->toFloat($item['other']['total']);
                    }
                    $itema[$oitem['item_id']]['total'] += $this->toFloat($item['other']['total']);
                }
            }

            // HAVE CATEGORIES
            if (count($cata) > 0) {

                if (!is_array($vars['ignore']['category'])) $vars['ignore']['category'] = array();

                // LOOP
                foreach ($cata as $cat_id => $cat) {

                    // NOT IGNORING DISCOUNT FOR CATEGORY
                    if (!in_array($cat_id, $vars['ignore']['category'])) {

                        // CHECK FOR DISCOUNTS
                        $cat_dis_query = "SELECT `discount_quantity_type`, `discount_quantity_amount`, `discount_quantity_quantity`, `discount_total_type`, `discount_total_amount`, `discount_total_total` FROM `" .$this->tables['categories']. "` WHERE (`id`=" .$cat_id. ")";
                        $cat_dis_q = sqlquery($cat_dis_query);
                        $cat_dis = sqlfetch($cat_dis_q);

                        // DISCOUNT BY QUANTITY
                        if ($cat_dis['discount_quantity_amount'] > 0) {

                            // QUANTITY MATCHES
                            if ($cat['quantity'] >= $cat_dis['discount_quantity_quantity']) {
                                if ($cat_dis['discount_quantity_type'] == 'amount') {
                                    $out['discount'] += $this->toFloat($cat_dis['discount_quantity_amount']);
                                } else {
                                    //$out['discount'] += ($order['order_subtotal'] * $cat_dis['discount_quantity_amount']) / 100;
                                    $out['discount'] += ($this->toFloat($cata[$oitem['category_id']]['total']) * $this->toFloat($cat_dis['discount_quantity_amount'])) / 100;
                                }
                            }

                        }

                        // DISCOUNT BY TOTAL
                        if ($cat_dis['discount_total_amount'] > 0) {

                            // TOTAL MATCHES
                            if ($this->toFloat($cat['total']) >= $this->toFloat($cat_dis['discount_total_total'])) {
                                if ($cat_dis['discount_total_type'] == 'amount') {
                                    $out['discount'] += $this->toFloat($cat_dis['discount_total_amount']);
                                } else {
                                    //$out['discount'] += ($order['order_subtotal'] * $cat_dis['discount_total_amount']) / 100;
                                    $out['discount'] += ($this->toFloat($cata[$oitem['category_id']]['total']) * $this->toFloat($cat_dis['discount_total_amount'])) / 100;
                                }
                            }

                        }

                    }

                }

            }

            // HAVE ITEMS
            if (count($itema) > 0) {

                if (!is_array($vars['ignore']['item'])) $vars['ignore']['item'] = array();

                // LOOP
                foreach ($itema as $item_id => $item) {

                    // NOT IGNORING DISCOUNT FOR ITEM
                    if (!in_array($item_id, $vars['ignore']['item'])) {

                        // CHECK FOR DISCOUNTS
                        $item_dis_query = "SELECT `discount_quantity_type`, `discount_quantity_amount`, `discount_quantity_quantity`, `discount_total_type`, `discount_total_amount`, `discount_total_total` FROM `" .$this->tables['items']. "` WHERE (`id`=" .$item_id. ")";
                        $item_dis_q = sqlquery($item_dis_query);
                        $item_dis = sqlfetch($item_dis_q);

                        // DISCOUNT BY QUANTITY
                        if ($item_dis['discount_quantity_amount'] > 0) {

                            // QUANTITY MATCHES
                            if ($item['quantity'] >= $item_dis['discount_quantity_quantity']) {
                                if ($item_dis['discount_quantity_type'] == 'amount') {
                                    $out['discount'] += $this->toFloat($item_dis['discount_quantity_amount']);
                                } else {
                                    $out['discount'] += ($this->toFloat($order['order_subtotal']) * $this->toFloat($item_dis['discount_quantity_amount'])) / 100;
                                }
                            }

                        }

                        // DISCOUNT BY TOTAL
                        if ($item_dis['discount_total_amount'] > 0) {

                            // TOTAL MATCHES
                            if ($this->toFloat($item['total']) >= $this->toFloat($item_dis['discount_total_total'])) {
                                if ($item_dis['discount_total_type'] == 'amount') {
                                    $out['discount'] += $this->toFloat($item_dis['discount_total_amount']);
                                } else {
                                    $out['discount'] += ($this->toFloat($order['order_subtotal']) * $this->toFloat($item_dis['discount_total_amount'])) / 100;
                                }
                            }

                        }

                    }

                }

            }

            // HAVE COUPON
            if ($order['coupon_id']) {

                if (!is_array($vars['ignore']['coupon'])) $vars['ignore']['coupon'] = array();

                // NOT IGNORING DISCOUNT FOR COUPON
                if (!in_array($order['coupon_id'], $vars['ignore']['coupon'])) {

                    // GET COUPON INFO
                    $coupon_query = "SELECT * FROM `" .$this->tables['coupons']. "` WHERE (`id`=" .$order['coupon_id']. ")";
                    $coupon_q = sqlquery($coupon_query);
                    $coupon = sqlfetch($coupon_q);

                    // COUPON FOUND
                    if ($coupon['id']) {

                        // COUPON ACTIVE
                        if ($coupon['active']) {

                            $coupon_continue = true;

                            // COUPON HAS START / END
                            if (($coupon['dt_start'] != '0000-00-00 00:00:00') || ($coupon['dt_end'] != '0000-00-00 00:00:00')) {

                                // ORDER IS IN CART
                                if ((!$order['status']) || ($order['status'] == 'cart')) {
                                    $date = date('Y-m-d H:i:s');
                                } else {
                                    $date = date('Y-m-d H:i:s', strtotime($order['dt']));
                                }

                                // HAVE START
                                if ($coupon['dt_start'] != '0000-00-00 00:00:00') {
                                    if ($date < $coupon['dt_start']) {
                                        $coupon_continue = false;
                                    }
                                }

                                // HAVE END
                                if ($coupon['dt_end'] != '0000-00-00 00:00:00') {
                                    if ($date > $coupon['dt_end']) {
                                        $coupon_continue = false;
                                    }
                                }

                            }

                            // CAN CONTINUE
                            if ($coupon_continue) {

                                // HAVE LIMITS
                                if (($coupon['limit_customer']) || ($coupon['limit_total'])) {

                                    // USED ARRAY
                                    $useda['customer'] = array();
                                    $useda['ip'] = array();

                                    // GET ALL ORDERS THAT USED IT
                                    $used_query = "SELECT `id`, `status`, `ip`, `customer_id` FROM `" .$this->tables['orders']. "` WHERE (`coupon_id`=" .$coupon['id']. ") AND (`id`!=" .$order['id']. ")";
                                    $used_q = sqlquery($used_query);
                                    while ($used = sqlfetch($used_q)) {
                                        if (($used['status'] != 'cart') && ($used['status'] != 'deleted')) {
                                            if ($used['customer_id']) {
                                                $useda['customer'][$used['customer_id']]++;
                                            } else {
                                                $useda['ip'][$used['ip']]++;
                                            }
                                        }
                                    }

                                    // TOTAL USED
                                    $used_total = count($useda['customer']) + count($useda['ip']);

                                    // HAVE TOTAL LIMIT
                                    if ($coupon['limit_total']) {
                                        if ($used_total >= $coupon['limit_total']) {
                                            $coupon_continue = false;
                                            $out['coupon']['error'] = 'Coupon reached use limit.';
                                        }
                                    }

                                    // HAVE PER CUSTOMER LIMIT
                                    if ($coupon['limit_customer']) {
                                        if ($order['customer_id']) {
                                            $customer_uses = $useda['customer'][$order['customer_id']];
                                        } else {
                                            $customer_uses = $useda['customer'][$order['ip']];
                                        }
                                        if ($customer_uses >= $coupon['limit_customer']) {
                                            $coupon_continue = false;
                                            $out['coupon']['error'] = 'Coupon reached customer use limit.';
                                        }
                                    }

                                }

                                // CAN CONTINUE
                                if ($coupon_continue) {

                                    // HAVE CATEGORIES OR ITEMS IT APPLIES TO
                                    if (($coupon['categories']) || ($coupon['items'])) {

                                        // HAVE CATEGORIES IT APPLIES TO
                                        if ($coupon['categories']) {

                                            $c_continue = false;

                                            // HAVE CATEGORIES
                                            if (count($cata) > 0) {

                                                // ARRAY OF CATEGORIES IT APPLIES TO
                                                $ca = explode(',', $coupon['categories']);

                                                // LOOP THROUGH ITEM CATEGORIES
                                                foreach ($cata as $cat_id => $cat) {
                                                    if (in_array($cat_id, $ca)) {
                                                        $c_continue = true;
                                                    }
                                                }
                                            }

                                            $coupon_continue = $c_continue;

                                        }

                                        // HAVE ITEMS IT APPLIES TO
                                        if ($coupon['items']) {

                                            $i_continue = false;

                                            // HAVE ITEMS
                                            if (count($itema) > 0) {

                                                // ARRAY OF ITEMS IT APPLIES TO
                                                $ia = explode(',', $coupon['items']);

                                                // LOOP THROUGH ITEMS
                                                foreach ($itema as $item_id => $item) {
                                                    if (in_array($item_id, $ia)) {
                                                        $i_continue = true;
                                                    }
                                                }
                                            }

                                            $coupon_continue = $i_continue;

                                        }

                                        if (!$coupon_continue) {
                                            $out['coupon']['error'] = 'Cart does not contain any items coupon applies to.';
                                        }

                                    }

                                }

                                // CAN APPLY COUPON
                                if ($coupon_continue) {
                                    $out['coupon']['id'] = $coupon['id'];
                                    $out['coupon']['code'] = $coupon['code'];
                                    if ($coupon['type'] == 'amount') {
                                        $out['discount'] += $this->toFloat($coupon['amount']);
                                    } else {
                                        $out['discount'] += ($this->toFloat($order['order_subtotal']) * $this->toFloat($coupon['amount'])) / 100;
                                    }
                                }

                            // CAN'T CONTINUE
                            } else {
                                $out['coupon']['error'] = 'Coupon not available.';
                            }

                        // COUPON NOT ACTIVE
                        } else {
                            $out['coupon']['error'] = 'Coupon not active.';
                        }

                    } else {
                        $out['coupon']['error'] = 'Coupon not found.';
                    }

                } else {
                    $out['coupon']['error'] = 'Coupon ignored.';
                }

                // HAVE ERROR -  DELETE COUPON FROM ORDER
                if (($out['coupon']['error']) && (!$vars['keep_cart_coupon'])) {
                    $cart_query = "UPDATE `" .$this->tables['orders']. "` SET `coupon_id`=0 WHERE (`id`=" .$order['id']. ")";
                    sqlquery($cart_query);
                }

            }

            if ($out['discount']) {
                $out['discount'] = number_format($out['discount'], 2);
            }

            return $out;

        }


        // CALCULATE CART TAX
        public function cartTax($cart_id, $citems=array(), $vars=array()) {

            $out = array();
            $out['total'] = 0;

            // NO ITEMS SPECIFIED
            if (count($citems) == 0) {

                // GET CART ITEMS
                $citems = $this->cartItems($cart_id);

            }

            // HAVE ITEMS
            if (count($citems) > 0) {
                foreach ($citems as $item) {

                    if (!$item['after_tax']) {

                        $out['items'][$item['id']] = $this->cartItemTax($item, $vars);
                        $out['total'] += $out['items'][$item['id']]['amount'];

                    }

                }
            }

            /*
            if ($_SERVER['REMOTE_ADDR'] == '68.7.118.150') {
                echo print_r($out);
                exit;
            }
            */

            return $out;

        }


        // CALCULATE CART ITEM TAX
        public function cartItemTax($oitem, $vars=array()) {

            $out['percent'] = 0;
            $out['amount'] = 0;

            if (isset($vars['quote'])) {
                $quote_sql = " AND (`quote`=" .(int)$vars['quote']. ")";
            } else {
                $quote_sql = "";
            }

            // GET ITEM INFO
            $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .(int)$oitem['item_id']. ")" .$quote_sql;
            $item = sqlfetch(sqlquery($item_query));

            // ITEM HAS TAX GROUP
            if ($item['tax_group']) {

                // GET TAX GROUP
                $tg = $this->getTaxGroup($item['tax_group']);

            }

            // FOUND TAX GROUP
            if ($tg['id']) {

                // USE GROUP TAX
                $out['percent'] = $tg['tax'];

                $out['tax_group'] = $tg['id'];

            // NEED TO FIGURE OUT TAX STILL
            } else {

                // HAVE CATEGORY
                if ($oitem['category_id']) {

                    // GET CATEGORY & PARENTS
                    $cats = $this->getCategoryParents($oitem['category_id']);

                    // HAVE CATS
                    if (count($cats) > 0) {

                        // LOOP
                        foreach ($cats as $cat) {

                            // HAVE TAX GROUP
                            if ($cat['tax_group']) {

                                // GET TAX GROUP
                                $tg = $this->getTaxGroup($cat['tax_group']);

                                // FOUND TAX GROUP
                                if ($tg['id']) {

                                    // USE GROUP TAX
                                    $out['percent'] = $tg['tax'];

                                    $out['category'] = $cat['id'];
                                    $out['tax_group'] = $tg['id'];

                                    break;

                                }

                            }
                        }

                    }

                }

                // STILL NEED TAX
                if (!$tg['id']) {

                    // WHERE ARRAY
                    $wa = array();

                    // COUNTRY SPECIFIED
                    if ($vars['country']) {

                        $wa[] = "`country`='" .sqlescape($vars['country']). "'";

                        // STATE SPECIFIED
                        if ($vars['state']) {
                            $wa[] = "`state`='" .sqlescape($vars['state']). "'";
                        } else {
                            $wa[] = "`state`=''";
                        }

                        // ZIP SPECIFIED
                        if ($vars['zip']) {
                            $wa[] = "(('" .sqlescape($vars['zip']). "' >= `zip_from`) AND ('" .sqlescape($vars['zip']). "' <= `zip_to`)) OR ((`zip_from`='') AND (`zip_to`=''))";
                        } else {
                            $wa[] = "(`zip_from`='') AND (`zip_to`='')";
                        }

                    }

                    // HAVE WHERE ARRAY
                    if (count($wa) > 0) {

                        // ACTIVE TOO
                        $wa[] = "`active`=1";

                        // WHERE STRING
                        $where = "(" .implode(") AND (", $wa). ")";

                        // LOOK FOR MATCHING TAX GROUP
                        $tg_query = "SELECT * FROM `" .$this->tables['tax_groups']. "` WHERE " .$where. " ORDER BY `id` ASC LIMIT 1";
                        $tg = sqlfetch(sqlquery($tg_query));

                    }

                    // FOUND TAX GROUP
                    if ($tg['id']) {

                        // USE GROUP TAX
                        $out['percent'] = $tg['tax'];

                    // NO TAX GROUP
                    } else {

                        // GET GLOBAL TAX
                        $sett = $this->getSettings(array(
                            'tax_global',
                            'tax_global_percent'
                        ));

                        // HAVE GLOBAL TAX
                        if ($sett['tax_global']) {
                            $out['percent'] = $sett['tax_global_percent'];
                        }

                    }

                }

            }

            // HAVE TAX
            if ($out['percent'] > 0) {

                // HAVE PRICE OVERRIDE
                if ($oitem['override_price']) {

                    $price = (float)$oitem['override_price'];

                // NORMAL PRICING
                } else {

                    $_uccms_ecomm = $this;

                    $extended_view = true;

                    include(SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/routed/shop/data/item_cart.php');

                    $item['other']['price'] = (float)str_replace(',', '', $item['other']['price']);

                    $price = $item['other']['price'];

                }

                /*
                if ($_SERVER['REMOTE_ADDR'] == '68.7.118.150') {
                    echo print_r($item['other']);
                }
                */

                // TAX AMOUNT
                $out['amount'] = round(((($price * (int)$oitem['quantity']) * $out['percent']) / 100), 2, PHP_ROUND_HALF_UP);
                //$out['amount'] = round((($price * $out['percent']) / 100), 2, PHP_ROUND_HALF_UP);

            }

            return $out;

        }


        // GET SHIPPING INFO (# ITEMS, WEIGHT, ITEMS TOTAL PRICE) FOR CART
        public function cartShippingInfo($cart_id, $citems=array()) {

            $out = array();
            $out['free'] = 0;
            $out['custom_cost'] = 0.00;

            // NO ITEMS SPECIFIED
            if (count($citems) == 0) {

                // GET CART ITEMS
                $citems = $this->cartItems($cart_id);

            }

            // NUMBER OF ITEMS
            $num_items = count($citems);

            // HAVE ITEMS
            if ($num_items > 0) {
                foreach ($citems as $citem) {

                    // ADD TO QUANTITY
                    $out['quantity'] += $citem['quantity'];

                    // GET ITEM INFO
                    $item = $this->getItem($citem['item_id']);

                    // HAS FREE SHIPPING
                    if ($item['shipping_free'] == 1) {
                        $out['free']++;

                    // ADD TO NORMAL SHIPPING INFO
                    } else {

                        // HAS CUSTOM SHIPPING PRICE
                        if ($item['shipping_price'] > 0.00) {
                            $out['custom_cost'] += $item['shipping_price'];
                        }

                        // ADD TO WEIGHT
                        $out['weight'] += $item['weight'];

                        // ADD TO PRICE
                        $out['price'] += $this->itemPrice($item) * $citem['quantity'];

                    }

                }
            }

            if (!$out['weight']) $out['weight'] = 0.01;

            return $out;

        }


        // GET AVAILBLE SHIPPING SERVICES
        public function cartShippingServices($cart_id, $citems=array(), $vars=array()) {

            $out = array();

            // NO ITEMS SPECIFIED
            if (count($citems) == 0) {

                // GET CART ITEMS
                $citems = $this->cartItems($cart_id);

            }

            // GET SHIPPING INFO
            $shipping_info = $this->cartShippingInfo($cart_id, $citems);

            // ALL ITEMS ARE FREE SHIPPING
            if ($shipping_info['free'] == count($citems)) {

                $out[] = array(
                    'id'        => 'free',
                    'title'     => 'Free',
                    'markup'    => ''
                );

            // CALCULATE SHIPPING
            } else {

                // USING SHIPSTATION
                if ($this->getSetting('shipstation_enabled')) {

                    // HAVE STATE AND ZIP
                    if (($vars['state']) && ($vars['zip'])) {

                        // GET CARRIERS AND SERVICES
                        $carriers_services = $this->getSetting('shipstation_carriers_services');

                        // HAVE INFO
                        if ($carriers_services) {

                            // LOAD REQUIRED LIBRARIES
                            require_once(SERVER_ROOT. 'uccms/includes/libs/unirest/Unirest.php');
                            require_once(EXTENSION_ROOT. 'classes/shipstation.php');

                            // DISABLE UNIREST SSL VALIDATION
                            Unirest::verifyPeer(false);

                            // INITIALIZE SHIPSTATION
                            $shipStation = new Shipstation();
                            $shipStation->setSsApiKey($this->getSetting('shipstation_api_key'));
                            $shipStation->setSsApiSecret($this->getSetting('shipstation_api_secret'));

                            if (is_array($carriers_services)) {
                                $csa = $carriers_services;
                            } else {
                                $csa = json_decode($carriers_services, true);
                            }

                            // HAVE CARRIERS & SERVICES ARRAY
                            if (is_array($csa)) {
                                foreach ($csa as $carrier_id => $carrier) { // LOOP THROUGH CARRIERS
                                    if ($carrier['active']) { // CARRIER IS ACTIVE
                                        if (is_array($carrier['services'])) { // HAVE SERVICES

                                            // BASE MARKUP
                                            $base_markup = 0.00;

                                            // RATE VARS
                                            $rate_vars = array(
                                                'carrierCode'       => $carrier_id,
                                                'serviceCode'       => null,
                                                'packageCode'       => null,
                                                'fromPostalCode'    => $this->getSetting('shipping_from_zip'),
                                                'toCountry'         => 'US',
                                                'toState'           => $vars['state'],
                                                //'toCity'            => 'Washington',
                                                'toPostalCode'      => $vars['zip'],
                                                'weight' => array(
                                                    'value' => $shipping_info['weight'],
                                                    'units' => 'pounds'
                                                ),
                                                /*
                                                'dimensions' => array(
                                                    'units'     => 'inches',
                                                    'length'    => 7,
                                                    'width'     => 5,
                                                    'height'    => 6
                                                ),
                                                */
                                                'confirmation' => null,
                                                'residential' => null
                                            );

                                            // GET RATES
                                            $rates = $shipStation->getRates($rate_vars);

                                            // RETRIEVED RATE
                                            if ($rates['success']) {

                                                // LOOP THROUGH RATES
                                                foreach ($rates['response'] as $rate) {
                                                    $ratea[$carrier_id][$rate['serviceCode']] = $rate['shipmentCost'];
                                                }

                                                // LOOP THROUGH SERVICES
                                                foreach ($carrier['services'] as $service_id => $service) {
                                                    if ($service['active']) { // SERVICE IS ACTIVE

                                                        if ($service['title']) {
                                                            $title = $service['title'];
                                                        } else {
                                                            $title = $service['title_default'];
                                                        }

                                                        // ADD TO OUTPUT ARRAY
                                                        $out[] = array(
                                                            'id'            => $service_id,
                                                            'title'         => $title,
                                                            'description'   => '',
                                                            'carrier'       => $carrier_id,
                                                            'markup'        => number_format($base_markup + $ratea[$carrier_id][$service_id], 2)
                                                        );


                                                    }
                                                }

                                            }

                                        }
                                    }
                                }
                            }

                        }

                    }

                // DEFAULT SHIPPING
                } else {

                    // GET SHIPPING METHODS
                    $shipping_query = "SELECT * FROM `" .$this->tables['shipping_methods']. "` WHERE (`active`=1)";
                    $shipping_q = sqlquery($shipping_query);
                    while ($shipping = sqlfetch($shipping_q)) {

                        // IS PICKUP
                        if ($shipping['id'] == 'pickup') {

                            $available = false; // NOT AVAILABLE BY DEFAULT

                            // SETTINGS
                            $settings = json_decode(stripslashes($shipping['settings']), true);

                            // TITLE
                            if (!$settings['title']) $settings['title'] = 'Local Pickup';

                            // MARKUP
                            $markup = 0;
                            if ($settings['markup']) {
                                $markup = @number_format((float)preg_replace('/[^0-9.]*/', '', $settings['markup']), 2);
                            }

                            // HAVE ZIP CODES
                            if ($settings['zips']) {

                                // ZIP ARRAY
                                $zipa = explode(',', preg_replace('/[^0-9,]*/', '', $settings['zips']));

                                // IS MATCH
                                if (in_array($vars['zip'], $zipa)) {
                                    $available = true;
                                }

                            // NO ZIP CODES
                            } else {
                                $available = true;
                            }

                            // IS AVAILABLE
                            if ($available) {
                                $out[] = array(
                                    'id'            => $shipping['id'],
                                    'title'         => stripslashes($settings['title']),
                                    'description'   => stripslashes($settings['description']),
                                    'markup'        => $markup
                                );
                            }

                        // IS TABLE
                        } else if ($shipping['id'] == 'table') {

                            // WHERE ARRAY
                            $wa = array();

                            // BASE
                            $wa[] = "(`what`='weight') AND ((" .$shipping_info['weight']. " >= `min`) AND (" .$shipping_info['weight']. " <= `max`))";
                            $wa[] = "(`what`='price') AND ((" .$shipping_info['price']. " >= `min`) AND (" .$shipping_info['price']. " <= `max`))";
                            $wa[] = "(`what`='quantity') AND ((" .$shipping_info['quantity']. " >= `min`) AND (" .$shipping_info['quantity']. " <= `max`))";

                            // WHERE STRING
                            $where = "(" .implode(") OR (", $wa). ")";

                            // GET SHIPPING TABLES
                            $table_query = "SELECT * FROM `" .$this->tables['shipping_table']. "` WHERE (`active`=1) AND (" .$where. ") GROUP BY `id` ORDER BY `sort` ASC";
                            $table_q = sqlquery($table_query);

                            // HAVE SHIPPING TABLES
                            if (sqlrows($table_q) > 0) {
                                while ($table = sqlfetch($table_q)) {
                                    $out[] = array(
                                        'table'         => true,
                                        'id'            => $table['id'],
                                        'title'         => stripslashes($table['title']),
                                        'markup'        => $table['markup'],
                                        'markup_type'   => $table['markup_type']
                                    );
                                }
                            }

                        // IS NORMAL SHIPPING
                        } else {

                            // WHERE ARRAY
                            $wa = array();

                            // BASE
                            $wa[] = "`active`=1";
                            $wa[] = "`method`='" .$shipping['id']. "'";
                            $wa[] = "((" .$shipping_info['weight']. " >= `weight_min`) AND (" .$shipping_info['weight']. " <= `weight_max`) OR ((`weight_min`=0.00) AND (`weight_max`=0.00)))";

                            // STATE SPECIFIED
                            if ($vars['state']) {

                                // US FOR NOW
                                $vars['country'] = 'United States';

                                // COUNTRY SPECIFIED
                                if ($vars['country']) {
                                    $wa[] = "(`country`='" .sqlescape($vars['country']). "') OR (`country`='')";
                                }

                                $wa[] = "(`state`='" .sqlescape($vars['state']). "') OR (`state`='')";

                            }

                            // WHERE STRING
                            $where = "(" .implode(") AND (", $wa). ")";

                            // GET SHIPPING SERVICES
                            $service_query = "SELECT * FROM `" .$this->tables['shipping_services']. "` WHERE " .$where;
                            $service_q = sqlquery($service_query);

                            // HAVE SHIPPING SERVICES
                            if (sqlrows($service_q) > 0) {
                                while ($service = sqlfetch($service_q)) {
                                    if ($service['markup'] > 0) {
                                        if ($service['markup_type'] == 'amount') {
                                            $markup = $service['markup'];
                                        } else {
                                            $markup = number_format(($service['markup'] / 100) * $shipping_info['price'], 2);
                                        }
                                    } else {
                                        $markup = 0;
                                    }
                                    $out[] = array(
                                        'method'    => stripslashes($shipping['title']),
                                        'id'        => $service['id'],
                                        'title'     => stripslashes($service['title']),
                                        'markup'    => $markup
                                    );
                                }

                            }

                        }

                    }

                }

            }

            return $out;

        }


        // CALCULATE SHIPPING FOR CART
        public function cartShipping($shipserva=array(), $shipping_service='') {

            $out = array();

            // FOUND SHIPPING SERVICES
            if (count($shipserva) > 0) {

                // IS SHIPPING TABLE
                if (stristr($shipping_service, 'table-')) {
                    $shipping_table = true;
                } else {
                    $shipping_table = false;
                }

                // LOOP THROUGH SHIPPING SERVICES
                foreach ($shipserva as $service) {
                    if ($shipping_table) {
                        if ($service['table']) {
                            if ('table-' .$service['id'] == $shipping_service) {
                                $out = $service;
                            }
                        }
                    } else {
                        if ($service['id'] == $shipping_service) {
                            $out = $service;
                        }
                    }
                }

                // NO SHIPPING ID
                if (!$out['id']) {
                    $out['err'] = 'Shipping service not available.';
                }

            // NO SHIPPING SERVICES
            } else {
                $out['err'] = 'No available shipping services found.';
            }

            return $out;

        }


        // ORDER GRATUITY
        public function orderGratuity($order) {
            $out = 0.00;
            if ($order['override_gratuity'] !== '') {
                if (!$order['subtotal']) $order['subtotal'] = $order['order_subtotal'];
                if ($order['gratuity_type'] == 'amount') {
                    $out = $this->dbNumDecimal($order['override_gratuity']);
                } else {
                    $out = $this->dbNumDecimal(($order['subtotal'] + $order['tax'] + $order['shipping']) * ((float)$order['override_gratuity'] / 100));
                }
            } else if ($gratuity_percent = $this->getSetting('gratuity_percent')) {
                $out = $this->dbNumDecimal(($order['subtotal'] + $order['tax'] + $order['shipping']) * ((float)$gratuity_percent / 100));
            }
            return $out;
        }


        // ORDER SERVICE / CONVENIENCE FEE
        public function orderServiceFee($order) {
            $fee = 0.00;

            if (($order['override_servicefee'] !== '') && ($order['override_servicefee'] > 0)) {
                $fee = $order['override_servicefee'];
            } else {
                $fee_amount = $this->getSetting('servicefee_amount');
                if ($fee_amount > 0) {
                    $fee_type = $this->getSetting('servicefee_type');
                    if ($fee_type == 'amount') {
                        $fee = $fee_amount;
                    } else {
                        $fee = $this->dbNumDecimal(((float)$order['subtotal'] + (float)$order['tax'] + (float)$order['shipping']) * ((float)$fee_amount / 100));
                    }
                }
            }

            return $fee;
        }


        // ORDER TOTAL
        public function orderTotal($order) {

            // GET ORDER TOTALS
            $totals = $this->orderTotals($order);

            if ($totals['total'] > 0) {
                return $totals['total'];
            } else {
                return 0.00;
            }

        }


        // GET ORDER TOTALS
        public function orderTotals($order, $params=array()) {

            // ORDER ID
            if (is_numeric($order)) {

                // GET ORDER INFO
                $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .(int)$order. ")";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                //$order['shipping'] = $order['order_shipping'];
                //$order['tax'] = $order['order_tax'];

                $order['discount'] = $order['order_discount'];

            // ARRAY
            } else {

                if (!$order['discount']) $order['discount'] = $order['order_discount'];
                if (!$order['override_shipping']) $order['override_shipping'] = $order['shipping'];
                if (!$order['override_tax']) $order['override_tax'] = $order['tax'];

            }

            // ORDER FOUND
            if ($order['id']) {

                $order['subtotal'] = 0;

                //if (!$order['shipping']) $order['shipping'] = ($order['override_shipping'] ? $order['override_shipping'] : $order['order_shipping']);
                //if (!$order['tax']) $order['tax'] = ($order['override_tax'] ? $order['override_tax'] : $order['order_tax']);

                // GET ORDER ITEMS
                $citems = $this->cartItems($order['id']);

                // HAVE ITEMS
                if (count($citems) > 0) {

                    // HAVE TAX OVERRIDE
                    if ($order['override_tax']) {

                        $tax['total'] = number_format($order['override_tax'], 2);

                    // NORMAL TAX CALCULATION
                    } else {

                        // HAVE TAX VALUE ALREADY
                        if (($order['order_tax']) && ($order['order_tax'] != '0.00')) {
                            $tax['total'] = $order['order_tax'];

                        // CALCULATE
                        } else {

                            // HAVE BILLING STATE AND/OR ZIP
                            if (($order['billing_state']) || ($order['billing_zip'])) {
                                $taxvals['country'] = 'United States';
                                $taxvals['state']   = stripslashes($order['billing_state']);
                                $taxvals['zip']     = stripslashes($order['billing_zip']);
                            }

                            //$taxvals['quote'] = false;

                            // GET TAX
                            $tax = $this->cartTax($order['id'], $citems, $taxvals);

                        }

                    }

                    // HAVE SHIPPING OVERRIDE
                    if ($order['override_shipping']) {
                        $shipping['markup'] = number_format($order['override_shipping'], 2);

                    // GET SHIPPING
                    } else {

                        // GET SHIPPING SERVICES
                        $shipping_services = $this->cartShippingServices($order['id']);

                        // GET SHIPPING AMOUNT
                        $shipping = $this->cartShipping($shipping_services, $order['shipping_service']);

                        if (!$shipping['id']) {
                            $shipping['markup'] = 0.00;
                        }

                    }

                    // ATTRIBUTE ARRAY
                    $attra = array();

                    // EXTENDED VIEW
                    $extended_view = true;

                    $_uccms_ecomm = $this;

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/routed/shop/data/item_cart.php');

                        // IS RECALCULATING
                        if ($params['recalculate']) {

                            // IS BOOKING
                            if ($this->storeType() == 'booking') {

                                // UPDATE BOOKING
                                $booking_update = "UPDATE `" .$_uccms_ecomm->tables['booking_bookings']. "` SET `markup`=" .$booking_duration['markup']. " WHERE (`oitem_id`=" .$oitem['id']. ")";
                                sqlquery($booking_update);

                            }

                        }

                    }

                    // ADD VALUES TO ORDER ARRAY
                    $order['tax'] = $tax['total'];

                    // SHIPPING MARKUP - PERCENT
                    if ($shipping['markup_type'] == 'percent') {
                        $order['shipping'] = number_format((($shipping['markup'] / 100) * $order['subtotal']) + $shipping_info['custom_cost'], 2, '.', '');

                    // SHIPPING MARKUP - DOLLAR AMOUNT
                    } else {
                        $order['shipping']  = $shipping['markup'];
                    }

                }

                // CALCULATE GRATUITY
                if ($this->getSetting('gratuity_enabled')) {
                    if ($order['override_gratuity']) $order['override_gratuity'] = (float)$order['override_gratuity'];
                    $order['gratuity'] = $this->orderGratuity($order);
                }

                // CALCULATE SERVICE / CONVENIENCE FEES
                //if ($this->getSetting('service_fee_enabled')) {
                    if ($order['override_servicefee']) $order['servicefee'] = (float)$order['override_servicefee'];
                    $order['servicefee'] = $this->orderServiceFee($order);

                //}

            }

            $order['total'] = $this->dbNumDecimal((float)$order['subtotal'] - (float)$order['discount'] + (float)$order['tax'] + (float)$order['shipping'] + (float)$order['servicefee'] + (float)$order['gratuity'] + (float)$order['aftertax']);

            /*
            if ($_SERVER['REMOTE_ADDR'] == '24.28.79.50') {
                echo 'subtotal: ' .$order['subtotal']. '<br />';
                echo 'discount: ' .$order['discount']. '<br />';
                echo 'tax: ' .$order['tax']. '<br />';
                echo 'shipping: ' .$order['shipping']. '<br />';
                echo 'servicefee: ' .$order['servicefee']. '<br />';
                echo 'gratuity: ' .$order['gratuity']. '<br />';
                echo 'aftertax: ' .$order['aftertax']. '<br />';
                echo 'total: ' .$order['total']. '<br />';
                exit;
            }
            */

            return $order;

        }


        #############################
        # ORDER
        #############################

        // GET ORDER
        public function getOrder($id) {
            $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .(int)$id. ")";
            $order_q = sqlquery($order_query);
            return sqlfetch($order_q);
        }

        // ORDER INFO
        public function orderInfo($id, $options=[]) {
            $out = [];
            $order = $this->getOrder($id);
            if ($order['id']) {

                $out = $order;

                // ADDITIONAL FIELDS
                $out['billing_fullname']    = trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname']));
                $out['shipping_fullname']   = trim(stripslashes($order['shipping_firstname']. ' ' .$order['shipping_lastname']));

                // GET ORDER ITEMS
                $items = $this->cartItems($order['id']);

                // LOOP
                foreach ((array)$items as $oitem) {

                    // GET ITEM
                    $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$oitem['item_id']. ")";
                    $item_q = sqlquery($item_query);
                    $item = sqlfetch($item_q);

                    $oitem['item'] = $item;

                    // IS RENTAL
                    if ($item['type'] == 2) {

                        // GET BOOKING INFO
                        $booking_query = "SELECT * FROM `" .$this->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$oitem['id']. ")";
                        $booking_q = sqlquery($booking_query);
                        $booking = sqlfetch($booking_q);

                        // BOOKING FOUND
                        if ($booking['id']) {

                            $oitem['booking'] = $booking;

                            // GET PERSONS
                            $persons_query = "SELECT * FROM `" .$this->tables['booking_bookings_persons']. "` WHERE (`oitem_id`=" .$oitem['id']. ") ORDER BY `sort` ASC, `id` ASC";
                            $persons_q = sqlquery($persons_query);
                            while ($person = sqlfetch($persons_q)) {
                                $person['order_id'] = $order['id'];
                                $oitem['persons'][$person['id']] = $person;
                            }

                        }

                    }

                    $out['items'][$oitem['id']] = $oitem;

                }

                // INCLUDING LINKED ORDERS
                if ($options['include-linked'] == 'full') {

                    // GET LINKED ORDERS
                    $linked = $this->linkedOrders($order['id'], 'to');

                    // LOOP
                    foreach ((array)$linked as $lorder) {
                        $out['linked-orders'][$lorder['from']] = $this->orderInfo($lorder['from'], $options);
                    }

                }

            } else {
                $out['error'] = 'Order not found.';
            }
            return $out;
        }

        // CALCULATE BALANCE ($amount = amount being added to order)
        public function orderBalance($order_id, $amount=0) {

            // CLEAN UP
            $order_id = (int)$order_id;
            $amount = number_format((float)$amount, 2, '.', '');

            // GET ORDER INFO
            $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$order_id. ")";
            $order_q = sqlquery($order_query);
            $order = sqlfetch($order_q);

            // ORDER SET TO HAVE ZERO BALANCE
            if ($order['zero_balance']) {
                return 0.00;
            }

            // BALANCE
            $balance = $order['order_total'] - $amount;

            // GET ORDER TRANSACTIONS
            $trans_query = "SELECT * FROM `" .$this->tables['transaction_log']. "` WHERE (`cart_id`=" .$order_id. ") AND (`tip`=0)";
            $trans_q = sqlquery($trans_query);
            while ($trans = sqlfetch($trans_q)) {

                switch ($trans['status']) {
                    case 'captured':
                        // don't add or subtract
                        break;
                    case 'charged':
                        $balance = $balance - $trans['amount'];
                        break;
                    case 'failed':
                        // don't add or subtract
                        break;
                    case 'refunded':
                        $balance = $balance + $trans['amount'];
                        break;
                    case 'voided':
                        $balance = $balance + $trans['amount'];
                        break;
                }

            }

            if ($balance > $order['order_total']) {
                $balance = $order['order_total'];
            }

            return number_format(round((float)$balance, 2), 2, '.', '');

        }

        // RECALCULATE ORDER VALUES
        public function orderRecalculate($order_id, $vars=array()) {

            $order_id = (int)$order_id;

            if ($order_id) {

                // GET ORDER
                $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .(int)$order_id. ")";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                // CLEAR CURRENT VALUES
                $columns = array(
                    'order_subtotal' => 0.00,
                    //'order_discount' => 0.00,
                    'order_tax'      => 0.00,
                    'order_aftertax' => 0.00,
                    'order_gratuity' => 0.00,
                    'order_total'    => 0.00
                );

                // HAS DISCOUNT W/ ID (not set value)
                if (($order['order_discount'] > 0.00) && ($order['order_discount_id'])) {
                    $columns['order_discount'] = 0.00;
                }

                // UPDATE
                $clear_query = "UPDATE `" .$this->tables['orders']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .(int)$order_id. ")";
                sqlquery($clear_query);

                $vars['recalculate'] = true;

                // GET ORDER TOTALS
                $totals = $this->orderTotals($order['id'], $vars);

                // SET NEW VALUES
                $columns = array(
                    'order_subtotal' => $this->dbNumDecimal($totals['subtotal']),
                    'order_discount' => $this->dbNumDecimal($totals['discount']),
                    'order_tax'      => $this->dbNumDecimal($totals['tax']),
                    'order_aftertax' => $this->dbNumDecimal($totals['aftertax']),
                    'order_gratuity' => $this->dbNumDecimal($totals['gratuity']),
                    'order_total'    => $this->dbNumDecimal($totals['total'])
                );

                // ORDER SET TO HAVE ZERO BALANCE
                if ($order['zero_balance']) {
                    $columns['order_total'] = 0.00;
                }

                // UPDATE
                $update_query = "UPDATE `" .$this->tables['orders']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .(int)$order_id. ")";
                if (sqlquery($update_query)) {
                    return true;
                }

            }

        }

        // LINK TWO ORDERS
        public function linkOrders($to, $from, $vars=array()) {
            global $_uccms, $admin;

            $out = array();

            // CLEAN UP
            $to_id = (int)$to;
            $from_id = (int)$from;

            // HAVE ID'S
            if (($to_id) && ($from_id)) {

                // DB COLUMNS
                $columns = array(
                    'to'            => $to_id,
                    'from'          => $from_id,
                    'created_by'    => $admin->ID //$_uccms['_account']->userID()
                );

                if ($vars['data']) {
                    $columns['data'] = json_encode($vars['data']);
                } else {
                    $columns['data'] = '';
                }

                // CREATE RECORD
                $insert_query = "INSERT INTO `" .$this->tables['orders_rel']. "` SET " .uccms_createSet($columns). "";
                if (sqlquery($insert_query)) {

                    $out['success'] = true;

                // FAILED
                } else {
                    $out['error'] = 'Failed to create link.';
                }

            // NO ORDER ID
            } else {
                $out['error'] = 'Order IDs not specified.';
            }

            return $out;

        }

        // UNLINK ORDERS
        public function unlinkOrders($link_id) {

            $out = array();

            // CLEAN UP
            $link_id = (int)$link_id;

            // HAVE LINK ID
            if ($link_id) {

                // REMOVE FROM DB
                $remove_query = "DELETE FROM `" .$this->tables['orders_rel']. "` WHERE (`id`=" .$link_id. ")";
                if (sqlquery($remove_query)) {

                    $out['success'] = true;

                // FAILED
                } else {
                    $out['error'] = 'Failed to remove link.';
                }

            // NO ORDER ID
            } else {
                $out['error'] = 'Link ID not specified.';
            }

            return $out;

        }

        // GET LINKED ORDERS
        public function linkedOrders($order_id, $type='to') {

            $out = array();

            // CLEAN UP
            $order_id = (int)$order_id;

            $type = ($type == 'to' ? 'to' : 'from');

            // GET MATCHES
            $lorders_query = "SELECT * FROM `" .$this->tables['orders_rel']. "` WHERE (`" .$type. "`=" .$order_id. ")";
            $lorders_q = sqlquery($lorders_query);
            while ($lorder = sqlfetch($lorders_q)) {
                if ($lorder['data']) {
                    $lorder['data'] = json_decode($lorder['data'], true);
                }
                $out[$lorder['id']] = $lorder;
            }

            return $out;

        }

        // RECORD ORDER IN REVSOCIAL
        public function revsocial_recordOrder($cart_id, $customer_id, $vars, $import=false) {
            global $cms;

            // REVSOCIAL API KEY
            $api_key = $cms->getSetting('rs_api-key');

            // HAVE API KEY
            if ($api_key) {

                if (!$vars['timestamp']) {
                    $vars['timestamp'] = date('c'); // ISO 8601 formatted date
                }

                // ORDER INFO
                $data = array(
                    'order' => array(
                        'id' => $cart_id,
                        'timestamp' => $vars['timestamp'],
                        'customer' => array(
                            'id'            => $customer_id,
                            'firstname'     => $vars['billing']['firstname'],
                            'lastname'      => $vars['billing']['lastname'],
                            //'middlename'    => 'Middle',
                            //'prefix'        => 'Mr',
                            //'suffix'        => 'Jr',
                            'email'         => $vars['billing']['email'],
                            'phone'         => $vars['billing']['phone'],
                            //'dob'           => '05/19/1985',
                            //'gender'        => 'male',
                            //'country'       => 'US',
                            'state'         => $vars['billing']['state'],
                            'city'          => $vars['billing']['city'],
                            'zip'           => $vars['billing']['zip'],
                            'street'        => trim($vars['billing']['address1']. ' ' .$vars['billing']['address2']),
                            'company'       => $vars['billing']['company']
                        ),
                        'billing' => array(
                            'firstname'     => $vars['billing']['firstname'],
                            'lastname'      => $vars['billing']['lastname'],
                            //'middlename'    => 'Middle',
                            //'prefix'        => 'Mr',
                            //'suffix'        => 'Jr',
                            'email'         => $vars['billing']['email'],
                            'phone'         => $vars['billing']['phone'],
                            //'dob'           => '05/19/1985',
                            //'gender'        => 'male',
                            //'country'       => 'US',
                            'state'         => $vars['billing']['state'],
                            'city'          => $vars['billing']['city'],
                            'zip'           => $vars['billing']['zip'],
                            'street'        => trim($vars['billing']['address1']. ' ' .$vars['billing']['address2']),
                            'company'       => $vars['billing']['company']
                        ),
                        'shipping' => array(
                            'firstname'     => $vars['shipping']['firstname'],
                            'lastname'      => $vars['shipping']['lastname'],
                            //'middlename'    => 'Middle',
                            //'prefix'        => 'Mr',
                            //'suffix'        => 'Jr',
                            'email'         => $vars['shipping']['email'],
                            'phone'         => $vars['shipping']['phone'],
                            //'dob'           => '05/19/1985',
                            //'gender'        => 'male',
                            //'country'       => 'US',
                            'state'         => $vars['shipping']['state'],
                            'city'          => $vars['shipping']['city'],
                            'zip'           => $vars['shipping']['zip'],
                            'street'        => trim($vars['shipping']['address1']. ' ' .$vars['shipping']['address2']),
                            'company'       => $vars['shipping']['company']
                        ),
                        'subtotal'          => $vars['order']['subtotal'],
                        //'coupon_code'       => 'fall1234',
                        //'discount_amount'   => '5.00',
                        'shipping_amount'   => $vars['order']['shipping'],
                        'tax_amount'        => $vars['order']['tax'],
                        'order_total'       => $vars['order']['total'],
                        'paid'              => $vars['order']['paid'],
                        'currency_code'     => 'USD',
                        'shipping_method'   => $vars['order']['shipping_method'],
                        'transaction_id'    => $vars['order']['transaction_id'],
                        'total_products'    => $vars['items']['num_products'],
                        'total_items'       => $vars['items']['num_quantity'],
                        'products'          => $vars['items']['products']
                    )
                );

                // IS IMPORTING
                if ($import) {
                    $data['ip'] = $vars['order']['ip'];

                // IS NOT IMPORTING
                } else {

                    $data['ip'] = $_SERVER['REMOTE_ADDR'];
                    $data['session'] = $_COOKIE['__rstid'];

                    // IS ADMIN
                    if ($this->adminID()) {

                        // IGNORE
                        $data['associate_ignore'] = array(
                            'ip' => true,
                            'session' => true
                        );

                    }

                }

                if ($vars['created_by']) {
                    $data['order']['is_admin_order'] = true;
                }

                //file_put_contents('revsocial_data.txt', print_r($data, true));

                // REST CLIENT
                require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

                // INIT REST API CLASS
                $_api = new RestClient(array(
                    'base_url'  => 'https://api.revsocial.com/v1'
                ));

                // MAKE API CALL
                $result = $_api->post('ecommerce/addOrder', array(
                    'api_key'   => $api_key,
                    'data'      => $data
                ));

                //file_put_contents('revsocial_result.txt', print_r($result, true));

            }

            return $result;

        }


        // SAVE CUSTOMER WITH ORDER
        public function orderSaveCustomer($params, $options=[]) {
            $out = [];

            $contacts = [];

            // BILLING
            if (($params['billing_firstname']) || ($params['billing_lastname'])) {

                $contacts['billing'] = [
                    'firstname' => $params['billing_firstname'],
                    'lastname' => $params['billing_lastname'],
                    'firstname' => $params['billing_firstname'],
                    'phone_home' => $params['billing_phone'],
                    'email_home' => $params['billing_email'],
                    'address1' => $params['billing_address1'],
                    'address2' => $params['billing_address2'],
                    'city' => $params['billing_city'],
                    'state' => $params['billing_state'],
                    'zip' => $params['billing_zip'],
                    'default' => [
                        'billing' => 1,
                        'shipping' => $params['shipping_same'],
                    ],
                ];

            }

            // DIFFERENT SHIPPING
            if (!$params['shipping_same']) {
                if (($params['shipping_firstname']) || ($params['shipping_lastname'])) {

                    $contacts['shipping'] = [
                        'firstname' => $params['shipping_firstname'],
                        'lastname' => $params['shipping_lastname'],
                        'firstname' => $params['shipping_firstname'],
                        'phone_home' => $params['shipping_phone'],
                        'email_home' => $params['shipping_email'],
                        'address1' => $params['shipping_address1'],
                        'address2' => $params['shipping_address2'],
                        'city' => $params['shipping_city'],
                        'state' => $params['shipping_state'],
                        'zip' => $params['shipping_zip'],
                        'default' => [
                            'shipping' => 1,
                        ],
                    ];

                }
            }

            // GET CUSTOMER
            $customer = $this->getCustomer($params['customer_id']);

            // CUSTOMER NOT FOUND
            if (!$customer['id']) {

                // CREATE CUSTOMER
                $customer = $this->saveCustomer([
                    'account' => [
                        'email' => ($contacts['billing']['email_home'] ? $contacts['billing']['email_home'] : $contacts['shipping']['email_home']),
                        'firstname' => ($contacts['billing']['firstname'] ? $contacts['billing']['firstname'] : $contacts['shipping']['firstname']),
                        'lastname' => ($contacts['billing']['lastname'] ? $contacts['billing']['lastname'] : $contacts['shipping']['lastname']),
                    ],
                    'status' => 1,
                ], [
                    'noWelcomeEmail' => true,
                ]);

                // STILL NO ID
                if (!$customer['id']) {
                    $customer['id'] = $customer['existing_id'];
                }

            }

            // HAVE CUSTOMER ID
            if ($customer['id']) {

                // HAVE ORDER ID
                if ($params['id']) {

                    // UPDATE ORDER
                    $uorder_sql = "UPDATE `" .$this->tables['orders']. "` SET `customer_id`=" .$customer['id']. " WHERE (`id`=" .(int)$params['id']. ")";
                    sqlquery($uorder_sql);

                }

                // ADD CONTACTS
                foreach ($contacts as $contact) {

                    $contact['customer_id'] = $customer['id'];
                    $contact['active'] = 1;

                    // SAVE CONTACT
                    $cont = $this->saveCustomerContact($contact);

                    $out['contacts'][] = $cont;

                }

                $out['id'] = $customer['id'];

            }

            return $out;

        }


        #############################
        # TAX
        #############################

        // PUT TOGETHER THE NAME OF A TAX GROUP FOR DISPLAY
        public function taxGroupName($group) {

            // GROUP PARTS
            $lparts = array();

            // COUNTRY AND STATE
            $csa = array();
            if ($group['country']) $csa[] = stripslashes($group['country']);
            if ($group['state']) $csa[] = stripslashes($group['state']);
            if (count($csa) > 0) {
                $lparts[] = implode(', ', $csa);
            }

            // ZIP(S)
            $za = array();
            if ($group['zip_from']) $za[] = stripslashes($group['zip_from']);
            if ($group['zip_to']) $za[] = stripslashes($group['zip_to']);
            if (count($za) > 0) {
                $lparts[] = implode(' - ', $za);
            }

            // FULL LOCATION
            $full_location = '';
            if (count($lparts) > 0) {
                if ($group['title']) $full_location .= ' (';
                $full_location .= implode(' ', $lparts);
                if ($group['title']) $full_location .= ')';
            }

            return stripslashes($group['title']) . $full_location;

        }


        // GET GROUP TAX
        public function getTaxGroup($group_id) {
            $tg_query = "SELECT * FROM `" .$this->tables['tax_groups']. "` WHERE (`id`=" .(int)$group_id. ") AND (`active`=1)";
            return sqlfetch(sqlquery($tg_query));
        }


        #############################
        # PAYMENT
        #############################

        // ALL AVAILABLE PAYMENT METHODS
        public function allPaymentMethods() {
            $out = array(
                'authorize'     => 'Authorize.Net',
                'paypal-rest'   => 'PayPal REST API',
                'paypal'        => 'PayPal Payments Pro',
                'payflow'       => 'PayPal Payflow Gateway',
                'linkpoint'     => 'First Data / LinkPoint',
                'stripe'        => 'Stripe',
                'instapay'      => 'InstaPay',
                'basys'         => 'Basys',
                'check_cash'    => 'Check / Cash',
                'waived'        => 'Waived / Write Off',
                'donation'      => 'Donation',
                'trade'         => 'Trade',
            );
            return $out;
        }


        // GET PAYMENT METHODS FROM DB
        public function getPaymentMethods($active=false) {
            $out = array();
            if ($active) {
                $where = "WHERE (`active`=1)";
            }
            $query = "SELECT * FROM `" .$this->tables['payment_methods']. "` " .$where;
            $q = sqlquery($query);
            while ($f = sqlfetch($q)) {
                if (!$f['title']) $f['title'] = $this->allPaymentMethods()[$f['id']];
                $out[$f['id']] = $f;
            }
            return $out;
        }


        // LOG TRANSACTION
        public function logTransaction($vars) {
            if (!$vars['dt']) {
                $vars['dt'] = date('Y-m-d H:i:s');
            }
            $query = "INSERT INTO `" .$this->tables['transaction_log']. "` SET " .$this->createSet($vars);
            sqlquery($query);
            return sqlid();
        }


        #############################
        # SHIPPING
        #############################


        // ALL AVAILABLE SHIPPING METHODS
        public function allShippingMethods() {
            $out = array(
                'usps' => array(
                    'title'     => 'USPS',
                    'services'  => array(
                        'standard'    => array(
                            'title' => 'Standard'
                        ),
                        'priority'    => array(
                            'title' => 'Priority'
                        )
                    ),
                    'settings'  => array(
                        'user_id'   => array(
                            'title'         => 'User ID',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'password'  => array(
                            'title'         => 'Password',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                    ),
                    'tracking'  => array(
                        'url'   => 'https://tools.usps.com/go/TrackConfirmAction_input?tLabels={tracking}'
                    )
                ),
                'ups' => array(
                    'title'     => 'UPS',
                    'services'  => array(
                        'ground'    => array(
                            'title' => 'Ground'
                        ),
                        '3day-select' => array(
                            'title' => '3 Day Select'
                        ),
                        '2day-air'      => array(
                            'title' => '2nd Day Air'
                        ),
                        '1day-air' => array(
                            'title' => 'Next Day Air'
                        )
                    ),
                    'settings'  => array(
                        'xml_access_key' => array(
                            'title'         => 'XML Access Key',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'user_id'   => array(
                            'title'         => 'User ID',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'password'  => array(
                            'title'         => 'Password',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'account'   => array(
                            'title'         => 'Account',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'pickup_type' => array(
                            'title'         => 'Pickup Type',
                            'field_type'    => 'select',
                            'field_options' => array(
                                'daily'     => 'Daily Pickup',
                                'counter'   => 'Customer Counter'
                            ),
                            'required'      => false
                        ),
                        'use_discount' => array(
                            'title'         => 'Use account discounted rates',
                            'field_type'    => 'checkbox',
                            'required'      => false
                        )
                    ),
                    'tracking'  => array(
                        'url'   => 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums={tracking}'
                    )
                ),
                'fedex' => array(
                    'title'     => 'FedEx',
                    'services'  => array(
                        'ground'    => array(
                            'title' => 'Ground'
                        ),
                        '2day'    => array(
                            'title' => '2 Day'
                        ),
                        'overnight-standard'    => array(
                            'title' => 'Standard Overnight'
                        )
                    ),
                    'settings'  => array(
                        'account'   => array(
                            'title'         => 'Account',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'csp_user_key' => array(
                            'title'         => 'CSP User Key',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'csp_user_password' => array(
                            'title'         => 'CSP User Password',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'meter'   => array(
                            'title'         => 'Meter',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'hub_id'   => array(
                            'title'         => 'Hub ID',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'delivery_signature' => array(
                            'title'         => 'Delivery Signature',
                            'field_type'    => 'select',
                            'field_options' => array(
                                'default'   => 'Service Default',
                                'no'        => 'No Signature Required',
                                'direct'    => 'Direct Signature Required',
                                'indirect'  => 'Indirect Signature Required',
                                'adult'     => 'Adult Signature Required'
                            ),
                            'required'      => false,
                        ),
                        'pickup_type'   => array(
                            'title'         => 'Pickup Type',
                            'field_type'    => 'select',
                            'field_options' => array(
                                'regular'   => 'Regular Pickup',
                                'drop_box'  => 'Drop Box',
                                'station'   => 'Drop at Station',
                                'bsc'       => 'Drop at BSC',
                                'request'   => 'Request Courier'
                            ),
                            'required'      => false
                        ),
                        'special_service'   => array(
                            'title'         => 'Special Service',
                            'field_type'    => 'select',
                            'field_options' => array(
                                'address'   => 'Address Service Requested',
                                'carrier_leave' => 'Carrier Leave If No Response',
                                'return'    => 'Return Service Requested',
                                'change'    => 'Change Service Requested',
                                'forwarding' => 'Forwarding Service Requested'
                            ),
                            'required'      => false
                        ),
                        'use_discount' => array(
                            'title'         => 'Use account discounted rates',
                            'field_type'    => 'checkbox',
                            'required'      => false
                        )
                    ),
                    'tracking'  => array(
                        'url'   => 'http://www.fedex.com/Tracking?tracknumbers={tracking}'
                    )
                ),
                'dhl' => array(
                    'title'     => 'DHL',
                    'services'  => array(
                        'ground'    => array(
                            'title' => 'Ground'
                        ),
                        'express'   => array(
                            'title' => 'Express'
                        ),
                        '2day'      => array(
                            'title' => '2nd Day'
                        )
                    ),
                    'settings'  => array(
                        'shipping_key' => array(
                            'title'         => 'Shipping Key',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'account'   => array(
                            'title'         => 'Account #',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'api_id'   => array(
                            'title'         => 'API ID',
                            'field_type'    => 'text',
                            'required'      => true
                        ),
                        'api_password' => array(
                            'title'         => 'API Password',
                            'field_type'    => 'text',
                            'required'      => true
                        )
                    ),
                    'tracking'  => array(
                        'url'   => 'http://www.dhl-usa.com/content/us/en/express/tracking.shtml?brand=DHL&AWB={tracking}'
                    )
                ),
                'table' => array(
                    'title'     => 'Table / Flat Rate',
                    'services'  => array()
                ),
                'pickup' => array(
                    'title'     => 'Local Pickup',
                    'services'  => array(),
                    'settings'  => array(
                        'title' => array(
                            'title'         => 'Title',
                            'field_type'    => 'text',
                            'required'      => false,
                            'default_value' => 'Local Pickup'
                        ),
                        'zips' => array(
                            'title'         => 'Zip Code(s)',
                            'field_type'    => 'textarea',
                            'required'      => false,
                            'note'          => 'Separate by comma'
                        ),
                        'description' => array(
                            'title'         => 'Description / Instructions',
                            'field_type'    => 'textarea',
                            'required'      => false
                        ),
                        'markup' => array(
                            'title'         => 'Markup',
                            'field_type'    => 'text',
                            'required'      => false
                        ),
                    )
                ),
            );
            return $out;
        }


        // GET SHIPPING METHODS FROM DB
        public function getShippingMethods($active=false) {
            $out = array();

            // USING SHIPSTATION
            if ($this->getSetting('shipstation_enabled')) {

                // GET CARRIERS AND SERVICES
                $carriers_services = $this->getSetting('shipstation_carriers_services');

                // HAVE INFO
                if ($carriers_services) {
                    if (is_array($carriers_services)) {
                        $csa = $carriers_services;
                    } else {
                        $csa = json_decode($carriers_services, true);
                    }
                    if (is_array($csa)) {
                        foreach ($csa as $id => $vals) {
                            if (($active) && (!$vals['active'])) {
                                continue;
                            } else {
                                $out[$id]['id'] = $id;
                                $out[$id]['active'] = $vals['active'];
                                $out[$id]['title'] = $vals['title'];
                            }
                        }
                    }

                }

            // DEFAULT SHIPPING MANAGEMENT
            } else {
                if ($active) {
                    $where = "WHERE (`active`=1)";
                }
                $query = "SELECT * FROM `" .$this->tables['shipping_methods']. "` " .$where;
                $q = sqlquery($query);
                while ($f = sqlfetch($q)) {
                    $out[$f['id']] = $f;
                    $out[$f['id']]['settings'] = json_decode($f['settings'], true);
                }
            }

            return $out;
        }


        // GET SHIPSTATION CARRIER & SERVICE SETTINGS
        public function shipstationCarriersServices() {
            $csa = $this->getSetting('shipstation_carriers_services');
            if ($csa) {
                if (!is_array($csa)) {
                    $csa = json_decode($csa, true);
                }
            } else {
                $csa = array();
            }
            return $csa;
        }


        // GET TRACKING URL
        public function trackingURL($carrier='', $tracking='') {
            $out = '';
            if (($carrier) && ($tracking)) {
                $asm = $this->allShippingMethods();
                if ($carrier == 'stamps_com') $carrier = 'usps';
                if ($asm[$carrier]['tracking']['url']) {
                    $out = str_replace('{tracking}', str_replace(' ', '', $tracking), $asm[$carrier]['tracking']['url']);
                }
            }
            return $out;
        }


        #############################
        # ACCOUNT
        #############################


        // GET ACCOUNT'S PRICE GROUP
        public function accountPriceGroups($user_id=0) {
            global $_uccms;
            if (!$user_id) $user_id = $_uccms['_account']->userID();
            if ($this->misc['account_price_groups'][$user_id]) {
                return $this->misc['account_price_groups'][$user_id];
            } else {
                $groups[] = 1;
                if ($user_id > 0) {
                    $apg_query = "SELECT `pricegroups` FROM `" .$this->tables['customers']. "` WHERE (`id`=" .$user_id. ")";
                    $apg_q = sqlquery($apg_query);
                    $apg = sqlfetch($apg_q);
                    if ($apg['pricegroups']) {
                        $groups = explode(',', $apg['pricegroups']);
                    }
                }
                $this->misc['account_price_groups'][$user_id] = $groups;
                return $groups;
            }
        }


        #############################
        # PAYOUTS
        #############################


        // PAYOUT - Statuses
        public function payoutStatuses() {
            return [
                1   => 'Preparing',
                2   => 'Scheduled',
                5   => 'Sent',
                7   => 'Cancelled',
                9   => 'Deleted'
            ];
        }


        // PAYOUT - Get all accounts with necessary info / connections
        public function payoutAccounts() {

            $out = [];

            // GET ACCOUNTS WITH NECESSARY CONNECTION
            $accounts_query = "
            SELECT CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, a.*, ad.*
            FROM `uccms_accounts` AS `a`
            INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
            WHERE (ad.stripe LIKE '%stripe_user_id%')
            ORDER BY `fullname` ASC, `email` ASC, a.id ASC
            ";
            $accounts_q = sqlquery($accounts_query);
            while ($account = sqlfetch($accounts_q)) {
                $out[$account['id']] = $account;
            }

            return $out;

        }


        // PAYOUT - Get a payout's info
        public function payoutInfo($id) {

            // GET INFO
            $payout_query = "SELECT * FROM `" .$this->tables['payouts']. "` WHERE (`id`=" .(int)$id. ")";
            $payout_q = sqlquery($payout_query);
            $out = sqlfetch($payout_q);

            // PAYOUT FOUND
            if ($out['id']) {

                // GET PAYOUT ORDERS
                $porders_query = "SELECT * FROM `" .$this->tables['payout_orders']. "` WHERE (`payout_id`=" .$out['id']. ") ORDER BY `order_id` ASC";
                $porders_q = sqlquery($porders_query);
                while ($porder = sqlfetch($porders_q)) {
                    $out['orders'][$porder['id']] = $porder;
                }

            }

            return $out;

        }


        // ACCOUNT - Get items owned by account
        public function accountItems($account_id) {
            $out = [];
            $aitems_query = "SELECT `id` FROM `" .$this->tables['items']. "` WHERE (`account_id`=" .(int)$account_id. ")";
            $aitems_q = sqlquery($aitems_query);
            while ($aitem = sqlfetch($aitems_q)) {
                $out[$aitem[id]] = $aitem['id'];
            }
            return $out;
        }


        // PAYOUT - Get orders for items account owns
        public function payoutGetAccountOrders($account_id, $params=[], $config=[]) {

            $out = [];

            // HAVE ACCOUNT ID
            if ($account_id) {

                $wa = [];

                $aitems = [];

                // ITEM ID(S) SPECIFIED
                if ($params['item_id']) {
                    if (is_array($params['item_id'])) {
                        $aitems = $params['item_id'];
                    } else {
                        $aitems[(int)$params['item_id']] = (int)$params['item_id'];
                    }

                // GET ITEMS OWNED BY ACCOUNT
                } else {
                    $aitems = $this->accountItems($account_id);
                }

                // HAVE ITEMS
                if (count($aitems) > 0) {

                    $wa[] = "o.status IN('charged','processing','complete','shipped')";
                    $wa[] = "oi.item_id IN(" .implode(',', $aitems). ")";

                    // other params

                    // GET ORDERS WITH ITEM(S)
                    $orders_query = "
                    SELECT o.*
                    FROM `" .$this->tables['orders']. "` AS `o`
                    INNER JOIN `" .$this->tables['order_items']. "` AS `oi` ON oi.cart_id=o.id
                    WHERE (" .implode(") AND (", $wa). ")
                    GROUP BY o.id
                    ORDER BY o.id ASC
                    ";

                    //echo $orders_query. '<br />';
                    //exit;

                    $orders_q = sqlquery($orders_query);
                    while ($order = sqlfetch($orders_q)) {
                        $out[$order['id']] = $order;
                    }

                // NO ACCOUNT ITEMS
                } else {
                    $out['error'] = 'No items owned by account.';
                }

            // NO ACCOUNT SPECIFIED
            } else {
                $out['error'] = 'Please select an account first.';
            }

            return $out;

        }


        // PAYOUT - Calculate the payout for an order based on items
        public function payoutOrderItemsCalculate($citems, $aitems=[]) {

            $out = [];

            $_uccms_ecomm = $this;

            foreach ((array)$citems as $oitem) {

                if (in_array($oitem['item_id'], $aitems)) {

                    $item = $out['items'][$oitem['item_id']];

                    if (!$item) {
                        //$item = $this->getItem($oitem['item_id'], false);
                        include(SERVER_ROOT. 'extensions/' .$this->Extension. '/templates/routed/shop/data/item_cart.php');
                        $out['items'][$oitem['item_id']] = $item;
                    }

                    //echo print_r($item);

                    if ($item['id']) {

                        $payout_percent = $item['payout_calc_amount'];

                        if ($payout_percent) {

                            $payout_amount = round((($payout_percent / 100) * $item['other']['total']), 2);

                            $out['items'][$oitem['item_id']]['payout_total'] += $payout_amount;

                            $out['oitems'][$oitem['id']]['total'] += $payout_amount;

                            $out['total'] += $payout_amount;

                        }

                    }

                }

            }

            return $out;

        }






        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # CRON
        #############################


        // RUN THE MAIN CRON
        public function runCron() {
            global $_uccms;

            $out = array();

            // WITHIN 4:00 AM AND 6:00 AM
            if ((time() >= strtotime(date('n/j/Y 04:00:00'))) && (time() <= strtotime(date('n/j/Y 06:00:00')))) {

                #########################
                # SCHEDULED PAYMENT - EMAIL
                #########################

                // GET NOT-EMAILED SCHEDULED PAYMENTS FOR TODAY
                $sp_query = "SELECT * FROM `" .$this->tables['scheduled_payments']. "` WHERE (`dt_email`='" .date('Y-m-d'). "') AND (`emailed`=0)";
                $sp_q = sqlquery($sp_query);
                while ($sp = sqlfetch($sp_q)) {

                    // HAVE PAYMENT PROFILE ID AND CHARGING TODAY - DON'T EMAIL
                    if (($sp['payment_profile_id'] > 0) && ($sp['dt_due'] == date('Y-m-d'))) {
                        continue;
                    }

                    // GET ORDER INFO
                    $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$sp['order_id']. ")";
                    $order_q = sqlquery($order_query);
                    $order = sqlfetch($order_q);

                    // ORDER FOUND
                    if ($order['id']) {

                        // GET EXTRA INFO
                        $extra = $this->cartExtra($sp['order_id']);

                        if (!$_POST['send']['to_name']) $_POST['send']['to_name'] = trim(stripslashes($extra['contact_firstname']. ' ' .$extra['contact_lastname']));
                        if (!$_POST['send']['to_email']) $_POST['send']['to_email'] = stripslashes($extra['contact_email']);

                        // EMAIL SETTINGS
                        $esettings = array(
                            'template'      => 'scheduled_payment-due.html',
                            'subject'       => 'Payment Due',
                            'order_id'      => $sp['order_id'],
                            'from_name'     => stripslashes($this->getSetting('email_from_name')),
                            'from_email'    => stripslashes($this->getSetting('email_from_email')),
                            'to_name'       => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                            'to_email'      => stripslashes($order['billing_email']),
                            'sent_by'       => 0
                        );

                        if ($sp['dt_due'] == date('Y-m-d')) {
                            $sp_date = 'today';
                        } else {
                            $sp_date = ' by ' .date('n/j/Y', strtotime($sp['dt_due']));
                        }

                        if ($sp['title']) {
                            $sp_for = ' for "' .stripslashes($sp['title']). '"';
                        }

                        // EMAIL VARIABLES
                        $evars = array(
                            'date'                  => date('n/j/Y'),
                            'time'                  => date('g:i A T'),
                            'order_id'              => $sp['order_id'],
                            'order_hash'            => $order['hash'],
                            'firstname'             => stripslashes($order['billing_firstname']),
                            'message'               => '<b>A payment of $' .$sp['amount']. ' is due ' .$sp_date . $sp_for. '.</b>',
                            'order_link'            => WWW_ROOT . $this->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash'])
                        );

                        // SEND EMAIL
                        $eresult = $this->sendEmail($esettings, $evars);

                        // HAVE ERROR
                        if ($eresult['err']) {
                            $out[] = 'Failed to email for scheduled payment ID: ' .$sp['id'];

                        // NO ERROR
                        } else {

                            // UPDATE - MARK AS SENT
                            $update_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `emailed`=1 WHERE (`id`=" .$sp['id']. ")";
                            sqlquery($update_query);

                        }

                    // ORDER NOT FOUND
                    } else {

                        $out[] = 'Order (ID: ' .$sp['order_id']. ') not found for scheduled payment ID: ' .$sp['id']. '. Deleting scheduled payment..';

                        // DELETE SCHEDULED PAYMENT
                        $delete_query = "DELETE FROM `" .$this->tables['scheduled_payments']. "` WHERE (`id`=" .$sp['id']. ")";
                        sqlquery($delete_query);

                    }

                }

                #########################
                # SCHEDULED PAYMENT - CHARGE
                #########################

                // GET NOT-CHARGED SCHEDULED PAYMENTS FOR TODAY
                $sp_query = "SELECT * FROM `" .$this->tables['scheduled_payments']. "` WHERE (`dt_due`='" .date('Y-m-d'). "') AND (`payment_profile_id`>0) AND (`charged`=0)";
                $sp_q = sqlquery($sp_query);
                while ($sp = sqlfetch($sp_q)) {

                    $transaction_id = 0;

                    // GET ORDER INFO
                    $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$sp['order_id']. ")";
                    $order_q = sqlquery($order_query);
                    $order = sqlfetch($order_q);

                    // ORDER FOUND
                    if ($order['id']) {

                        // GET EXTRA INFO
                        $extra = $this->cartExtra($sp['order_id']);

                        // EMAIL SETTINGS
                        $esettings = array(
                            'order_id'      => $order['id'],
                            'from_name'     => stripslashes($this->getSetting('email_from_name')),
                            'from_email'    => stripslashes($this->getSetting('email_from_email')),
                            'to_name'       => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                            'to_email'      => stripslashes($order['billing_email']),
                            'sent_by'       => 0
                        );

                        if ($sp['title']) {
                            $payment_title = '"' .stripslashes($sp['title']). '"';
                        } else {
                            $payment_title = 'order #' .$order['id'];
                        }

                        // EMAIL VARIABLES
                        $evars = array(
                            'date'                  => date('n/j/Y'),
                            'time'                  => date('g:i A T'),
                            'order_id'              => $sp['order_id'],
                            'order_hash'            => $order['hash'],
                            'firstname'             => stripslashes($order['billing_firstname']),
                            'payment_title'         => $payment_title,
                            'amount'                => number_format($this->toFloat($sp['amount']), 2),
                            'order_link'            => WWW_ROOT . $this->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash'])
                        );

                        // GET PAYMENT PROFILE
                        $pp = $_uccms['_account']->pp_getProfile($sp['payment_profile_id'], $order['customer_id']);

                        // PAYMENT PROFILE FOUND
                        if ($pp['id']) {

                            $payment_method     = 'profile';
                            $payment_name       = stripslashes($pp['name']);
                            $payment_lastfour   = $pp['lastfour'];
                            $payment_expiration = $pp['expires'];

                            // PAYMENT - CHARGE
                            $payment_data = array(
                                'amount'    => $sp['amount'],
                                'customer' => array(
                                    'id'            => $order['customer_id'],
                                    'name'          => $payment_name,
                                    'email'         => $order['billing_email'],
                                    'phone'         => $order['billing_phone'],
                                    'address'       => array(
                                        'street'    => $order['billing_address1'],
                                        'street2'   => $order['billing_address2'],
                                        'city'      => $order['billing_city'],
                                        'state'     => $order['billing_state'],
                                        'zip'       => $order['billing_zip'],
                                        'country'   => $order['billing_city']
                                    ),
                                    'description'   => ''
                                ),
                                'note' => 'Scheduled payment on order ID: ' .$order['id'],
                                'card' => array(
                                    'id'            => $pp['id'],
                                    'name'          => $payment_name,
                                    'number'        => $payment_lastfour,
                                    'expiration'    => $payment_expiration
                                )
                            );

                            // PROCESS PAYMENT
                            $payment_result = $_uccms['_account']->payment_charge($payment_data);

                            // TRANSACTION ID
                            $transaction_id = $payment_result['transaction_id'];

                            // TRANSACTION MESSAGE (ERROR)
                            $transaction_message = $payment_result['error'];

                            // LOG TRANSACTION
                            $this->logTransaction(array(
                                'cart_id'               => $order['id'],
                                'status'                => ($transaction_id ? 'charged' : 'failed'),
                                'amount'                => $sp['amount'],
                                'method'                => 'scheduled',
                                'payment_profile_id'    => $payment_result['profile_id'],
                                'name'                  => $payment_name,
                                'lastfour'              => $payment_lastfour,
                                'expiration'            => $payment_expiration,
                                'transaction_id'        => $transaction_id,
                                'message'               => $transaction_message,
                                //'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                            ));

                            // CHARGE SUCCESSFUL
                            if ($transaction_id) {

                                // CALCULATE BALANCE
                                $balance = $this->orderBalance($order['id'], $sp['amount']);

                                // ADD TO ORDER INFO
                                $order['transaction_id'] = $transaction_id;

                                $columns = array(
                                    'status'    => 'charged'
                                );

                                if ($balance == 0.00) {
                                    $columns['paid_in_full'] = 1;
                                }

                                // UPDATE ORDER
                                $cart_query = "UPDATE `" .$this->tables['orders']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .$order['id']. ")";
                                sqlquery($cart_query);

                                // UPDATE SCHEDULED PAYMENT
                                $cart_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `charged`=1 WHERE (`id`=" .$sp['id']. ")";
                                sqlquery($cart_query);

                                $esettings['template'] = 'scheduled_payment-processed.html';
                                $esettings['subject']  = 'Scheduled Payment Processed';

                            // TRANSACTION FAILED
                            } else {

                                // UPDATE SCHEDULED PAYMENT
                                $cart_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `charged`=2 WHERE (`id`=" .$sp['id']. ")";
                                sqlquery($cart_query);

                                $esettings['template']  = 'scheduled_payment-failed.html';
                                $esettings['subject']   = 'Scheduled Payment Failed';
                                $esettings['error']     = $transaction_message;

                            }

                        // PAYMENT PROFILE NOT FOUND
                        } else {

                            // UPDATE SCHEDULED PAYMENT
                            $cart_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `charged`=2 WHERE (`id`=" .$sp['id']. ")";
                            sqlquery($cart_query);

                            $esettings['template']  = 'scheduled_payment-failed.html';
                            $esettings['subject']   = 'Scheduled Payment Failed';
                            $esettings['message']   = 'Payment method not found.';

                        }

                        // SEND EMAIL
                        $eresult = $this->sendEmail($esettings, $evars);

                        // PAYMENT SUCCESSFUL
                        if ($transaction_id) {

                            // LOG MESSAGE
                            $this->logQuoteMessage(array(
                                'order_id'      => $order['id'],
                                'by'            => 0,
                                'message'       => 'Paid $' .number_format($this->toFloat($sp['amount']), 2). ' with credit card ending in ' .$payment_lastfour. '.',
                                'status'        => 'charged',
                                'email_log_id'  => $eresult['log_id']
                            ));

                        }

                        // COPY OF EMAIL TO ADMIN(S)
                        $admin_emails = $this->getSetting('email_order_copy');
                        if ($admin_emails) {
                            $emails = explode(',', stripslashes($admin_emails));
                            foreach ($emails as $email) {
                                $esettings['to_name']   = '';
                                $esettings['to_email']  = $email;
                                $this->sendEmail($esettings, $evars);
                            }
                        }

                        /*
                        // ORDER DATA FOR REVSOCIAL API
                        $rs_orderdata = array(
                            'billing' => array(
                                'firstname'     => stripslashes($order['billing_firstname']),
                                'lastname'      => stripslashes($order['billing_lastname']),
                                'company'       => stripslashes($order['billing_company']),
                                'address1'      => stripslashes($order['billing_address1']),
                                'address2'      => stripslashes($order['billing_address2']),
                                'city'          => stripslashes($order['billing_city']),
                                'state'         => stripslashes($order['billing_state']),
                                'zip'           => stripslashes($order['billing_zip']),
                                'country'       => stripslashes($order['billing_country']),
                                'phone'         => stripslashes($order['billing_phone']),
                                'email'         => stripslashes($order['billing_email'])
                            ),
                            'shipping' => array(
                                'firstname'     => stripslashes($order['shipping_firstname']),
                                'lastname'      => stripslashes($order['shipping_lastname']),
                                'company'       => stripslashes($order['shipping_company']),
                                'address1'      => stripslashes($order['shipping_address1']),
                                'address2'      => stripslashes($order['shipping_address2']),
                                'city'          => stripslashes($order['shipping_city']),
                                'state'         => stripslashes($order['shipping_state']),
                                'zip'           => stripslashes($order['shipping_zip']),
                                'country'       => stripslashes($order['shipping_country']),
                                'phone'         => stripslashes($order['shipping_phone']),
                                'email'         => stripslashes($order['shipping_email'])
                            ),
                            'order' => $order
                        );
                        $rs_orderdata['order']['total'] = $sp['amount'];
                        $rs_orderdata['items']          = $rs_items;

                        // RECORD ORDER WITH REVSOCIAL
                        $_uccms_ecomm->revsocial_recordOrder($order['id'], $order['customer_id'], $rs_orderdata);
                        */

                    // ORDER NOT FOUND
                    } else {

                        $out[] = 'Order (ID: ' .$sp['order_id']. ') not found for scheduled payment ID: ' .$sp['id']. '. Deleting scheduled payment..';

                        // DELETE SCHEDULED PAYMENT
                        $delete_query = "DELETE FROM `" .$this->tables['scheduled_payments']. "` WHERE (`id`=" .$sp['id']. ")";
                        sqlquery($delete_query);

                    }

                }

                #########################
                # CLEAN UP OLD FILES
                #########################

                // GET NOT-PLACED ORDERS PLACED WITHIN 30 AND 40 DAYS
                $order_query = "SELECT `id` FROM `" .$this->tables['orders']. "` WHERE (`status`='cart') AND ((`dt`>='" .date('Y-m-d H:i:s', strtotime('-40 Days')). "') AND (`dt`<='" .date('Y-m-d H:i:s', strtotime('-30 Days')). "'))";
                $order_q = sqlquery($order_query);
                while ($order = sqlfetch($order_q)) {

                    // GET ORDER ITEMS
                    $item_query = "SELECT `options` FROM `" .$this->tables['order_items']. "` WHERE (`cart_id`=" .$order['id']. ")";
                    $item_q = sqlquery($item_query);
                    while ($item = sqlfetch($item_q)) {

                        unset($options);

                        // HAVE OPTIONS
                        if ($item['options']) {

                            // DECODE OPTIONS
                            $options = json_decode(stripslashes($item['options']), true);

                            // HAVE ARRAY
                            if (count($options) > 0) {
                                foreach ($options as $option_id => $option_value) {

                                    // HAVE VALUE(S)
                                    if (count($option_value) > 0) {

                                        // GET OPTION INFO
                                        $option_info = sqlfetch(sqlquery("SELECT `type` FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .$option_id. ")"));

                                        // IS FILE
                                        if ($option_info['type'] == 'file') {
                                            foreach ($option_value as $file) {
                                                @unlink(SITE_ROOT. 'extensions/' .$this->Extension. '/files/orders/' .$order['id']. '-' .$file);
                                                $out[] = 'Removed file (' .$file. ') for order: ' .$order['id'];
                                            }
                                        }

                                    }

                                }
                            }

                        }

                    }

                }

            }

            return implode("\n", $out). "\n";

        }


        #############################
        # REVIEWS
        #############################


        // CRITERIA
        public function review_criteria($what='') {
            if ($what) {
                $criteria = $this->getSetting('review_criteria-' .$what);
            } else {
                $criteria = $this->getSetting('review_criteria');
            }
            if (($criteria) && (!is_array($criteria))) {
                $criteria = json_decode($criteria, true);
            }
            if (!is_array($criteria)) $criteria = array();
            return $criteria;
        }


        // RECORD A REVIEW
        public function review_record($vars) {
            global $cms, $_uccms;

            // ITEM
            if ($vars['review']['what'] == 'item') {

                $id = (int)$vars['review']['item_id'];

                // HAVE ID
                if ($id) {

                    // GET ITEM
                    $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$id. ")";
                    $item_q = sqlquery($item_query);
                    $item = sqlfetch($item_q);

                    // ITEM FOUND
                    if ($item['id']) {

                        // DB COLUMNS
                        $columns = array(
                            'rating'        => $vars['calc']['rating'],
                            'reviews'       => $vars['calc']['reviews'],
                            'review_data'   => json_encode($vars['calc']['criteria'])
                        );

                        // RECORD INFO
                        $query = "UPDATE `" .$this->tables['items']. "` SET " .uccms_createSet($columns). " WHERE (`id`=" .$item['id']. ")";
                        sqlquery($query);

                    }

                    // EMAIL SETTINGS
                    $esettings = array(
                        'item_id'   => $item['id']
                    );

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'item_id'               => $item['id'],
                        'item_title'            => stripslashes($item['name']),
                        'item_url'              => WWW_ROOT . substr($this->itemURL($item['id'], $item['category_id'], $item), 1),
                        'from_name'             => $vars['review']['name'],
                        'content'               => $vars['review']['content'],
                        'view_url'              => WWW_ROOT . substr($this->itemURL($item['id'], $item['category_id'], $item). '#reviews', 1)
                    );

                    // IS REPLY
                    if ($vars['review']['reply_to_id']) {

                        // GET ORIGINAL REVIEW
                        $orig_query = "SELECT * FROM `uccms_ratings_reviews` WHERE (`id`=" .(int)$vars['review']['reply_to_id']. ")";
                        $orig_q = sqlquery($orig_query);
                        $orig = sqlfetch($orig_q);

                        // HAVE ACCOUNT ID
                        if ($orig['account_id']) {

                            // GET ACCOUNT
                            $acct = $_uccms['_account']->getAccount('', true, $orig['account_id']);

                            // HAVE EMAIL ADDRESS
                            if ($acct['email']) {

                                $esettings['template']  = 'ratings-reviews/reply-received.html';
                                $esettings['subject']   = ($cms->getSetting('site_name') ? $cms->getSetting('site_name'). ' - ' : ''). 'Reply to Review Received';
                                $esettings['to_email']  = stripslashes($acct['email']);

                                $evars['heading']   = 'Reply to Review Received';
                                $evars['to_name']   = ($acct['firstname'] ? ' ' .stripslashes($acct['firstname']) : '');

                            }

                        }

                    // REVIEW
                    } else {

                        $esettings['template'] = 'ratings-reviews/review-submitted.html';
                        $esettings['subject']   = ($cms->getSetting('site_name') ? $cms->getSetting('site_name'). ' - ' : ''). 'Review Submitted';

                        // WHO TO SEND TO
                        if ($item['email_lead']) {
                            $esettings['to_email'] = stripslashes($item['email_lead']);
                        } else {
                            $esettings['to_email'] = stripslashes($item['email']);
                        }

                        $evars['heading']       = 'Review Submitted';
                        $evars['manage_url']    = WWW_ROOT . '/account/item-directory/edit/?id=' .$item['id']. '&clear=true';

                    }

                    // HAVE EMAIL
                    if ($esettings['to_email']) {

                        // SEND EMAIL
                        $result = $_uccms['_account']->sendEmail($esettings, $evars);

                    }

                }

            }

        }


        #############################
        # MISC
        #############################

        // BUILD RRULE
        public function rrule_generate($params) {
            $out = '';

            // IS RECURRING
            if ($params['recurring']) {

                $parts = array();

                // FREQUENCY
                switch ($params['recurring_freq']) {
                    case 'day':
                        $parts['FREQ'] = 'DAILY';
                        break;
                    case 'week':
                        $parts['FREQ'] = 'WEEKLY';
                        break;
                    case 'month':
                        $parts['FREQ'] = 'MONTHLY';
                        break;
                    case 'year':
                        $parts['FREQ'] = 'YEARLY';
                        break;
                }

                // INTERVAL
                if ($params['recurring_interval'] > 0) {
                    $parts['INTERVAL'] = $params['recurring_interval'];
                }

                // COUNT
                if ($params['recurring_count'] > 0) {
                    $parts['COUNT'] = $params['recurring_count'];
                }

                // BY DAY
                if ($params['recurring_byday']) {
                    $parts['BYDAY'] = stripslashes($params['recurring_byday']);
                }

                // BY DAY OF MONTH
                if ($params['recurring_bymonthday']) {
                    $parts['BYMONTHDAY'] = stripslashes($params['recurring_bymonthday']);
                }

                // HAVE END DATE
                if (substr($params['end'], 0, 10) != '0000-00-00') {
                    $parts['UNTIL'] = date('c', strtotime($params['end']));
                }

                // HAVE PARTS
                if (count($parts) > 0) {
                    $tpa = array();
                    foreach ($parts as $name => $value) {
                        $tpa[] = $name. '=' .$value;
                    }
                    $out = implode(';', $tpa);
                    unset($parts);
                    unset($tpa);
                }

            }

            return $out;

        }


        public function personManifest_fieldMap($what='') {

            $out = [

                // BILLING
                'billing.firstname' => [
                    'title' => 'First Name',
                    'field' => ['order','billing_firstname'],
                ],
                'billing.lastname' => [
                    'title' => 'Last Name',
                    'field' => ['order','billing_lastname'],
                ],
                'billing.fullname' => [
                    'title' => 'Name',
                    'field' => ['order','billing_fullname'],
                ],

                // ATTRIBUTE
                'attribute.id' => [
                    'title' => '',
                    'field' => ['oitem','options'],
                ],

                // FORMATTED ATTRIBUTE
                'fattribute.id' => [
                    'title' => '',
                    'field' => ['oitem','options-formatted'],
                ],

                // PERSON
                'person.firstname' => [
                    'title' => 'First Name',
                    'field' => ['person','firstname'],
                ],
                'person.lastname' => [
                    'title' => 'Last Name',
                    'field' => ['person','lastname'],
                ],
                'person.dob' => [
                    'title' => 'DOB',
                    'field' => ['person','dob'],
                ],
                'person.phone' => [
                    'title' => 'Phone',
                    'field' => ['person','phone'],
                ],
                'person.email' => [
                    'title' => 'Email',
                    'field' => ['person','email'],
                ],
                'person.notes' => [
                    'title' => 'Notes',
                    'field' => ['person','notes'],
                ],

            ];

            return $out;

        }


        // MANIFEST OF PEOPLE ASSOCIATED WITH AN ITEM OR ORDER
        public function personManifest($vars, $options=[]) {

            $out = [];

            if (count($options['person_fields']) == 0) {
                $options['person_fields']  = [
                    'person.firstname',
                    'person.lastname',
                    'person.dob',
                    'person.phone',
                    'person.email',
                    'person.notes',
                ];
            }

            // BY ORDER
            if ($vars['order_id']) {

                // GET ORDER INFO
                $order = $this->orderInfo((int)$vars['order_id'], $options['order']);

                //echo print_r($order);
                //exit;

                // GET MANIFEST FOR ORDER
                $out = $this->personManifest_fromOrder($order, $options);

                if (!$out['original-order']) $out['original-order'] = $order['id'];

            // BY ITEM
            } else if ($vars['item_id']) {

                $wa = [];

                $wa[] = "oi.item_id=" .(int)$vars['item_id'];
                $wa[] = "o.status IN('charged','processing','shipped','complete')";

                // GET ITEM
                $item_query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`id`=" .$vars['item_id']. ")";
                $item_q = sqlquery($item_query);
                $item = sqlfetch($item_q);

                // IS BOOKING / RENTAL
                if ($item['type'] == 2) {

                    include_once(dirname(__FILE__). '/store-types/booking.php');
                    $_booking = new uccms_StoreType();

                    $bookings_from = ($vars['from'] ? strtotime($vars['from']) : '');
                    $bookings_to = ($vars['to'] ? strtotime($vars['to']) : '');

                    // GET BOOKINGS
                    $bookings = $_booking->getBookings($bookings_from, $bookings_to, [
                        'item_id'       => $item['id'],
                        'duration_id'   => (int)$vars['duration_id'],
                    ]);

                    /*
                    if ($_SERVER['REMOTE_ADDR'] == '24.28.79.50') {
                        echo print_r($bookings);
                        exit;
                    }
                    */

                    $oida = [];
                    foreach ((array)$bookings as $booking) {
                        $oida[$booking['order_id']] = $booking['order_id'];
                    }

                    if (count($oida) > 0) {
                        $wa[] = "o.id IN(" .implode(',', $oida). ")";
                    } else {
                        return [];
                    }

                // ALL OTHERS
                } else {

                    if ($vars['from'] || $vars['to']) {

                        $dt_from    = date('Y-m-d H:i:s', strtotime($vars['from']));
                        $dt_to      = date('Y-m-d H:i:s', strtotime($vars['to']));

                        if (($vars['from']) && ($vars['to'])) {
                            $wa[] = "(o.dt>='" .$dt_from. "') AND (o.dt<='" .$dt_to. "')";
                        } else if ($vars['from']) {
                            $wa[] = "o.dt>='" .$dt_from. "'";
                        } else if ($vars['to']) {
                            $wa[] = "o.dt<='" .$dt_to. "'";
                        }

                    }

                }

                // GET ORDERS
                $orders_query = "
                SELECT o.id
                FROM `" .$this->tables['orders']. "` AS `o`
                INNER JOIN `" .$this->tables['order_items']. "` AS `oi` ON oi.cart_id=o.id
                WHERE (" .implode(") AND (", $wa). ")
                GROUP BY o.id
                ORDER BY o.id ASC
                ";

                //echo $orders_query;
                //exit;

                $orders_q = sqlquery($orders_query);
                while ($order = sqlfetch($orders_q)) {

                    // GET ORDER'S MANIFEST
                    $omani = $this->personManifest([
                        'order_id' => $order['id']
                    ], $options);

                    unset($omani['original-order']);

                    //$out = array_merge_recursive($out, $omani);

                    $out['totals'] = array_merge_recursive((array)$out['totals'], (array)$omani['totals']);

                    foreach ((array)$omani['orders'] as $oid => $o) {
                        $out['orders'][$oid] = $o;
                    }

                }

            }

            // HAS ORDERS
            if (count($out['orders']) > 0) {
                foreach ($out['orders'] as $oid => $order) {
                    unset($out['orders'][$oid]);
                    $out['orders'][$order['id']] = $order;
                }
            }

            /*
            if ($order['id'] == 25) {
                echo print_r($out);
                exit;
            }
            */

            // IS BOOKING / RENTAL
            if ($item['type'] == 2) {

                // HAS PERSONS
                if (count($out['persons']) > 0) {
                    foreach ($out['persons'] as $pid => $person) {
                        unset($out['persons'][$pid]);
                        $out['persons'][$person['id']] = $person;
                        if (!in_array($person['id'], (array)$out['orders'][$person['order_id']]['persons'])) {
                            $out['orders'][$person['order_id']]['persons'][] = $person['id'];
                        }
                        $out['totals']['persons']['count']++;
                    }
                }
            }

            if (is_array($out['totals']['persons']['count'])) {
                $out['totals']['persons']['count'] = array_sum($out['totals']['persons']['count']);
            }
            //$out['totals']['persons']['count'] = count($out['persons']);

            return $out;

        }

        // BUILD MANIFEST FROM ORDER DETAILS
        public function personManifest_fromOrder($order=[], $options=[], $out=[]) {

            // HAVE ORDER
            if ($order['id']) {

                $aia = [];

                // STYLE: Makeup
                if ($options['style'] == 'makeup') {
                    if (count($order['items']) > 1) {
                        $oitem = array_shift($order['items']);
                        $order['makeup']['items'] = $order['items'];
                        unset($order['items']);
                        $order['items'][$oitem['id']] = $oitem;
                        unset($oitem);
                    }
                }

                $fieldmap = $this->personManifest_fieldMap();

                // ORDER ITEM ATTRIBUTE ARRAY
                $oiaa = [];

                // LOOP THROUGH ITEMS
                foreach ((array)$order['items'] as $oii => $oitem) {

                    if (is_string($oitem['options'])) {
                        $oitem['options'] = json_decode($oitem['options'], true);
                    }

                    $fa = [];

                    // IS BOOKING / RENTAL
                    if ($order['items'][$oitem['id']]['item']['type'] == 2) {

                        // LOOP THROUGH PERSONS
                        foreach ((array)$oitem['persons'] as $person) {

                            $out['persons'][$person['id']] = $person;

                            $out['totals']['persons']['count']++;

                            // PERSON FIELDS
                            foreach ((array)$options['person_fields'] as $field) {

                                $ff = (array)$fieldmap[explode('=', $field)[0]]['field'];
                                $parent_array = array_shift($ff);

                                // GET VALUE FROM FIELD WITH MAP
                                $val = $this->personManifest_ValueFromField($field, $$parent_array, $ff, $fieldmap);

                                $fa[$field] = $val;

                            }

                            $order['persons-mapped'][$person['id']] = $fa;
                            unset($fa);

                        }

                    // ALL OTHERS
                    } else {

                        $out['totals']['persons']['count']++;

                        // PERSON FIELDS
                        foreach ((array)$options['person_fields'] as $field) {

                            $ff = (array)$fieldmap[explode('=', $field)[0]]['field'];
                            $parent_array = array_shift($ff);

                            //print_r($$parent_array);
                            //exit;

                            // GET VALUE FROM FIELD WITH MAP
                            $fa[$field] = $this->personManifest_ValueFromField($field, $$parent_array, $ff, $fieldmap);

                        }

                        $order['persons-mapped'][] = $fa;

                    }


                    // HAS ATTRIBUTE VALUES
                    if (count($oitem['options']) > 0) {

                        // LOOP
                        foreach ($oitem['options'] as $attr_id => $attr_vals) {

                            // DON'T HAVE ATTRIBUTE INFO YET
                            if (!$aia[$attr_id]) {

                                // GET ATTRIBUTE INFO
                                $attribute_query = "SELECT * FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .(int)$attr_id. ")";
                                $attribute_q = sqlquery($attribute_query);
                                $aia[$attr_id] = sqlfetch($attribute_q);

                            }

                            // HAVE ATTRIBUTE INFO
                            if ($aia[$attr_id]['id']) {

                                $attr = $aia[$attr_id];

                                // IS PEOPLE
                                if ($attr['type'] == 'people') {

                                    // GET OPTIONS
                                    $attr_options = json_decode((string)$attr['options'], true);

                                    foreach ((array)$attr_options as $opt_i => $opt) {

                                        $title = $this->makeRewrite(stripslashes($opt['title']));
                                        $out['totals']['attributes'][$title]['title'] = stripslashes($opt['title']);
                                        $out['totals']['attributes'][$title]['markup'] = $opt['markup'];
                                        $out['totals']['attributes'][$title]['default'] = $opt['default'];
                                        $out['totals']['attributes'][$title]['count'] += $attr_vals[$opt_i];

                                        $order['totals']['attributes'][$title] = $out['totals']['attributes'][$title];

                                    }

                                }

                            }

                        }

                    }

                }

                $out['orders'][$order['id']] = $order;

                // LOOP THROUGH LINKED ORDERS
                foreach ((array)$order['linked-orders'] as $lorder) {

                    // GET MANIFEST FROM LINKED ORDER
                    $out = $this->personManifest_fromOrder($lorder, $options, $out);

                }

            } else {
                $out['error'] = ($order['error'] ? $order['error'] : 'Order not found.');
            }

            return $out;

        }

        // GET VALUE FROM MAPPED FIELD
        public function personManifest_ValueFromField($field, $parent_array=[], $ff=[], $fieldmap=[]) {

            $out = [];

            if (!$field) return;

            if (stristr($field, '=')) {
                list($field, $val) = explode('=', $field);
            }

            // TITLE
            $out['title'] = $fieldmap[$field]['title'];

            if (($field == 'attribute.id') || ($field == 'fattribute.id')) {
                if ($val) {

                    $attr_query = "SELECT * FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .(int)$val. ")";
                    $attr_q = sqlquery($attr_query);
                    $attr = sqlfetch($attr_q);

                    /*
                    if (is_string($attr['options'])) {
                        $attr['options'] = json_decode($attr['options'], true);
                    }
                    */

                    $out['title'] = stripslashes($attr['title']);
                }
            }

            // VALUE
            $out['value'] = '';
            if ($parent_array) {
                $out['value'] = $this->arrayPath($parent_array, $ff);
                if ($val) {
                    $out['value'] = $out['value'][$val];
                    if (is_array($out['value'])) {

                        if (isset($out['value']['options'])) {
                            $out['value'] = $out['value']['options'];
                        } else if (isset($out['value']['value'])) {
                            $out['value'] = $out['value']['value'];
                        } else {
                            $out['value'] = array_shift($out['value']);
                        }
                    }
                }
            }

            return $out;

        }

        // https://stackoverflow.com/questions/9628176/using-a-string-path-to-set-nested-array-data/36042293#36042293
        public function arrayPath(&$array, $path=array(), &$value=null) {
            $args = func_get_args();
            $ref = &$array;
            foreach ((array)$path as $key) {
                if (!is_array($ref)) {
                    $ref = array();
                }
                $ref = &$ref[$key];
            }
            $prev = $ref;
            if (array_key_exists(2, $args)) {
                $ref = $value;
            }
            return $prev;
        }



        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # GLOBAL SEARCH
        #############################


        public function globalSearch($q='', $params=array()) {

            $results = array();

            // GET MATCHING ITEMS
            $results_query = "SELECT `id`, `slug`, `title` FROM `" .$this->tables['items']. "` WHERE (`title` LIKE '%" .$q. "%') ORDER BY `updated_dt` DESC LIMIT " .(int)$params['limit'];
            $results_q = sqlquery($results_query);
            while ($row = sqlfetch($results_q)) {
                if ($row['slug']) {
                    $slug = stripslashes($row['slug']);
                } else {
                    $slug = $this->makeRewrite($row['title']);
                }
                $results[] = array(
                    'title' => stripslashes($row['title']),
                    'url'   => '/' .$this->storePath(). '/item/' .trim($slug). '-' .$row['id']. '/',
                    'type'  => 'ecommerce'
                );
            }

            return $results;

        }


        // MOVED TO GLOBAL.PHP (update all references to this)
        #############################
        # SITEMAP
        #############################


        public function getSitemap($page=array()) {
            global $bigtree;

            $out = array();

            // CATEGORIES
            $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => str_replace($bigtree['path'][0], $this->storePath(), $this->categoryURL($r['id'], $r))
                );
            }

            // ITEMS
            $query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
            $q = sqlquery($query);
            while ($r = sqlfetch($q)) {
                $out[] = array(
                    'link'  => substr(WWW_ROOT, 0, -1) . $this->itemURL($r['id'], 0, $r)
                );
            }

            return $out;

        }


    }

?>