<?php

class ShipStation {

	// ShipStation credentials
	private $ssApiKey;
	private $ssApiSecret;
	private $authorization;
    private $defaultAuthorization;

	// Shipstation endpoint & methods
	private $endpoint;
	private $methodsPaths;

    // Error Handling
    private $lastError;

    // Requests cap handling
    private $remainingRequests;
    private $resetTime;
    private $lastRequestTime;


    // INSTANTIATE SHIPSTATION CLASS
    public function __construct() {

    	// DEFINE ENDPOINT
    	$this->endpoint 		= 'https://ssapi.shipstation.com/';

    	// DEFINE DEFAULT CREDENTIALS
    	// Authorization Token = The String: "{SS API Key}:{SS API Secret}" encoded with base64
		$this->ssApiKey			= null;
		$this->ssApiSecret		= null;
		$this->authorization 	= 'Basic {Your Authorization Token Here}';

        // REQUESTS CAP HANDLING
        $this->remainingRequests    = 40; // Current SS per-minute limit
        $this->resetTime            = 0;
        $this->lastRequestTime      = null;

		// DEFINE METHODS PATHS
		$this->methodsPaths = array (

            // STORE RELATED METHODS
            'getStores'             => 'stores',

            // WAREHOUSE RELATED METHODS
            'getWarehouses'         => 'warehouses',

            // CARRIER RELATED METHODS
            'getCarriers'           => 'carriers',
            'getCarrier'            => 'carriers/getcarrier?carrierCode={code}',
            'carrierServices'       => 'carriers/listservices?carrierCode={code}',
            'carrierPackages'       => 'carriers/listpackages?carrierCode={code}',

            // SHIPMENT RELATED METHODS
            'getShipments'          => 'shipments',
            'getRates'              => 'shipments/getrates',
            'createLabel'           => 'shipments/createlabel',
            'voidLabel'             => 'shipments/voidlabel',

            // ORDER RELATED METHODS
            'getOrders'             => 'orders',
            'getOrder'              => 'orders/{id}',
	        'addTagToOrder'	        => 'orders/addtag',
            'addOrder'              => 'orders/createorder',
            'deleteOrder'           => 'orders/{id}',

		);

    }


    // SET SHIPSTATION API KEY
    public function setSsApiKey($ssApiKey) {
        $this->ssApiKey = $ssApiKey;
        if (!empty($this->ssApiSecret)) {
            $this->authorization = 'Basic '.base64_encode($this->ssApiKey.':'.$this->ssApiSecret);
        }

    }


    // SET SHIPSTATION API KEY
    public function setSsApiSecret($ssApiSecret) {
        $this->ssApiSecret = $ssApiSecret;
        if (!empty($this->ssApiKey)) {
            $this->authorization = 'Basic '.base64_encode($this->ssApiKey.':'.$this->ssApiSecret);
        }

    }


    // SETS THE AUTHORIZATION TOKEN TO USE DIRECTLY ALLOWING TO SWITCH BETWEEN MULTIPLE SHIPSTATION ACCOUNTS FASTER
    public function setAuthorization($authorization) {
        $this->authorization = $authorization;
    }


    // RESETS THE AUTHORIZATION TOKEN TO DEFAULT
    public function resetAuthorization() {
        $this->authorization =  $this->defaultAuthorization;
    }


    ############################################################
    # STORE RELATED METHODS
    ############################################################


    // GET LIST OF STORES AVAILABLE
    public function getStores() {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $response = Unirest::get(
            $this->endpoint.$this->methodsPaths['getStores'],
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    ############################################################
    # WAREHOUSE RELATED METHODS
    ############################################################


    // GET LIST OF WAREHOUSES AVAILABLE
    public function getWarehouses() {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $response = Unirest::get(
            $this->endpoint.$this->methodsPaths['getWarehouses'],
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    ############################################################
    # CARRIER
    ############################################################


    // GET LIST OF CARRIERS AVAILABLE
    public function getCarriers() {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $response = Unirest::get(
            $this->endpoint.$this->methodsPaths['getCarriers'],
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    // GET A SPECIFIC CARRIER'S INFORMATION
    public function getCarrier($carrierCode) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $methodPath = str_replace('{code}', $carrierCode, $this->methodsPaths['getCarrier']);
        $response = Unirest::get(
            $this->endpoint.$methodPath,
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    // GET SERVICES FOR A CARRIER
    public function carrierServices($carrierCode) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $methodPath = str_replace('{code}', $carrierCode, $this->methodsPaths['carrierServices']);
        $response = Unirest::get(
            $this->endpoint.$methodPath,
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    // GET PACKAGES FOR A CARRIER
    public function carrierPackages($carrierCode) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $methodPath = str_replace('{code}', $carrierCode, $this->methodsPaths['carrierPackages']);
        $response = Unirest::get(
            $this->endpoint.$methodPath,
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    ############################################################
    # SHIPMENT RELATED METHODS
    ############################################################


    // GET A LIST OF ALL SHIPMENTS ON SHIPSTATION MATCHING THE FILTER
    public function getShipments($filters) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        // The API can't handle empty or null values on filters... (¬¬)
        // Validation of types would be useful.
        foreach($filters as $key=>$value) {
            if (empty($value)) {
                unset($filters[$key]);
            }
        }

        // BUILD THE QUERY STRING AND GET THE SHIPMENTS
        $response = Unirest::get(
            $this->endpoint.$this->methodsPaths['getShipments'].'?'.http_build_query($filters),
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    // GET A LIST OF ALL SHIPMENTS ON SHIPSTATION MATCHING THE FILTER ON ALL PAGES
    public function getAllShipments($filters) {
        $allShipments = array();
        $searchResult = $this->getShipments($filters);
        $shipments = $searchResult->shipments;
        if (!empty($shipments)) {
            foreach ($shipments as $sh) {
                array_push($allShipments,$sh);
            }
            $currentPage = $searchResult->page;
            $totalPages = $searchResult->pages;
            if ($currentPage < $totalPages) {
                for ($i=2; $i<=$totalPages; $i++) {
                    $filters['page'] = $i;
                    $searchResult = $this->getShipments($filters);
                    $shipments = $searchResult->shipments;
                    foreach ($shipments as $sh) {
                        array_push($allShipments,$sh);
                    }
                }
            }

        }
        return $allShipments;
    }


    // GET RATES
    public function getRates($label) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        // The API can't handle empty or null values on filters... (¬¬)
        // Validation of types would be useful.
        foreach($label as $key => $value) {
            if (empty($value)) {
                unset($label[$key]);
            }
        }

        $response = Unirest::post(
            $this->endpoint.$this->methodsPaths['getRates'],
            array(
                'Authorization' => $this->authorization,
                'content-type' => 'application/json'
            ),
            json_encode($label)
        );

        return $this->processReply($response);

    }


    // CREATE NEW SHIPPING LABEL
    public function createLabel($label) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        // The API can't handle empty or null values on filters... (¬¬)
        // Validation of types would be useful.
        foreach($label as $key => $value) {
            if (empty($value)) {
                unset($label[$key]);
            }
        }

        $response = Unirest::post(
            $this->endpoint.$this->methodsPaths['createLabel'],
            array(
                'Authorization' => $this->authorization,
                'content-type' => 'application/json'
            ),
            json_encode($label)
        );

        return $this->processReply($response);

    }


    // VOID A SHIPPING LABEL
    public function voidLabel($shipmentID) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $response = Unirest::post(
            $this->endpoint.$this->methodsPaths['voidLabel'],
            array(
                'Authorization' => $this->authorization,
                'content-type' => 'application/json'
            ),
            json_encode(array(
                'shipmentID' => $shipmentID
            ))
        );

        return $this->processReply($response);

    }


    ############################################################
    # ORDER RELATED METHODS
    ############################################################


    // GET A LIST OF ALL ORDERS ON SHIPSTATION MATCHING THE FILTER
    public function getOrders($filters) {

        // Enforce API requests cap
        $this->enforceApiRateLimit();

        // The API can't handle empty or null values on filters... (¬¬)
        // Validation of types would be useful.
        foreach($filters as $key=>$value) {
            if (empty($value)) {
                unset($filters[$key]);
            }
        }

        // BUILD THE QUERY STRING AND GET THE ORDERS
        $response = Unirest::get(
            $this->endpoint.$this->methodsPaths['getOrders'].'?'.http_build_query($filters),
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    // GET A LIST OF ALL ORDERS ON SHIPSTATION MATCHING THE FILTER OF ALL THE PAGES
    public function getAllOrders($filters){
        $allOrders = array();
        $searchResult 	= $this->getOrders($filters);
        if(!empty($searchResult)){
            $orders = $searchResult->orders;
            foreach ($orders as $or) {
                array_push($allOrders,$or);
            }
            $currentPage = $searchResult->page;
            $totalPages = $searchResult->pages;
            if($currentPage < $totalPages){
                for($i=2;$i<=$totalPages;$i++){
                    $filters['page'] = $i;
                    $searchResult 	= $this->getOrders($filters);
                    $orders = $searchResult->orders;
                    foreach ($orders as $or) {
                        array_push($allOrders,$or);
                    }
                }
            }
        }
        return $allOrders;
    }


    // GET A SPECIFIC ORDER ON SHIPSTATION BY ITS ID
    public function getOrder($orderId) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $methodPath = str_replace('{id}', $orderId, $this->methodsPaths['getOrder']);
        $response = Unirest::get(
            $this->endpoint.$methodPath,
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    // ADD A TAG TO A SHIPSTATION ORDER
    public function addTagToOrder($orderId, $tagId) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $response = Unirest::post(
            $this->endpoint.$this->methodsPaths['addTagToOrder'],
            array(
                'Authorization' => $this->authorization,
                'Content-type'  => 'application/json'
            ),
            json_encode(array(
                'orderId'   => $orderId,
                'tagId'     => $tagId
            ))
        );

        return $this->processReply($response);

    }


    // ADD A NEW ORDER TO SHIPSTATION
    public function addOrder($order) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        // The API can't handle empty or null values on filters... (¬¬)
        // Validation of types would be useful.
        foreach($order as $key => $value) {
            if (empty($value)) {
                unset($order[$key]);
            }
        }

        $response = Unirest::post(
            $this->endpoint.$this->methodsPaths['addOrder'],
            array(
                'Authorization' => $this->authorization,
                'content-type' => 'application/json'
            ),
            json_encode($order)
        );

        return $this->processReply($response);

    }


    // DELETE AN ORDER ON SHIPSTATION BY ITS ID
    public function deleteOrder($orderId) {

        // ENFORCE API REQUESTS CAP
        $this->enforceApiRateLimit();

        $methodPath = str_replace('{id}', $orderId, $this->methodsPaths['deleteOrder']);
        $response = Unirest::delete(
            $this->endpoint.$methodPath,
            array(
                'Authorization' => $this->authorization
            )
        );

        return $this->processReply($response);

    }


    ############################################################


    // SET THE ERROR OBJECT FOR LAST FAILED REQUEST
    public function setLastError($response) {
        $error['code']      = $response->code;
        $error['headers']   = $response->headers;
        $error['message']   = $response->raw_body;
        $this->lastError    = $error;
    }


    // RETURN THE LAST RESPONSE FROM SERVER, REPORTED AS ERROR
    public function getLastError() {
        return $this->lastError;
    }


    // PROCESS REPLY FROM SERVER, INTENDED TO ADD FURTHER VALIDATION/HANDLING
    private function processReply($response) {

        $out = array();

        // HAVE RESPONSE
        if (is_object($response)) {

            $this->remainingRequests    = $response->headers['X-Rate-Limit-Remaining'];
            $this->resetTime            = $response->headers['X-Rate-Limit-Reset'];
            $this->lastRequestTime      = time();

            if ($response->code == 200) {
                $out['success'] = true;
                $out['response'] = $response->body;
            } else {
                $message = json_decode($response->raw_body, true);
                $out['error']['code']           = $response->code;
                $out['error']['message']        = $message['ExceptionMessage'];
                $out['error']['message_full']   = $message;
                $out['error']['headers']        = $response->headers;
            }

        // NO RESPONSE
        } else {
            $out['error']['message'] = 'No response from ShipStation.';
        }

        return $out;

    }


    // ENFORCE SHIPSTATION API RATE LIMIT
    private function enforceApiRateLimit() {
        if ($this->remainingRequests > 0) {
            return;
        } else {
            if (!empty($this->lastRequestTime)) {
                $elapsedTime = (time() - $this->lastRequestTime);
                if ($elapsedTime > $this->resetTime) {
                    return;
                } else {
                    $waitingTime = ($this->resetTime - $elapsedTime);
                    sleep($waitingTime);
                }
            } else {
                return;
            }
        }
    }


}


?>
