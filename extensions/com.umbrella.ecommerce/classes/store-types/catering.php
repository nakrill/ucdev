<?php

class uccms_StoreType {


    // RETURNS ARRAY OF MEAL TIMES
    public function mealTimes() {
        global $_uccms_ecomm;
        $out = $_uccms_ecomm->getSetting('catering_mealtimes');
        if (($out) && (!is_array($out))) {
            $out = json_decode($out, true);
        }
        if ((!$out) || ((is_array($out) && (count($out) == 0)))) {
            $out = array(
                1 => array(
                    'title' => 'Breakfast',
                    'time'  => '07:00:00'
                ),
                2 => array(
                    'title' => 'AM Snack',
                    'time'  => '9:30:00'
                ),
                3 => array(
                    'title' => 'Lunch',
                    'time'  => '12:00:00'
                ),
                4 => array(
                    'title' => 'PM Snack',
                    'time'  => '15:00:00'
                ),
                5 => array(
                    'title' => 'Cocktail Hour',
                    'time'  => '17:00:00'
                ),
                6 => array(
                    'title' => 'Dinner',
                    'time'  => '18:00:00'
                )
            );
        }
        return $out;
    }


    // SORT ORDER ITEMS BY DAY AND TIME
    public function sortOItemsByDayTime($citems) {
        global $_uccms_ecomm;

        // GET MEALTIMES
        $mealtimes = $this->mealTimes();

        // CATEGORY SORT ARRAY
        $csa = array();

        // GET CATEGORY SORT ORDER
        $cat_sort_query = "SELECT `id`, `sort` FROM `" .$_uccms_ecomm->tables['categories']. "`";
        $cat_sort_q = sqlquery($cat_sort_query);
        while ($cs = sqlfetch($cat_sort_q)) {
            $csa[$cs['id']] = (int)$cs['sort'];
        }

        // MOVE DAY AND MEALTIME TO MAIN ITEM ARRAY
        array_walk($citems, function(&$item) use ($_uccms_ecomm, $mealtimes, $csa) {

            // GET CATEGORY SORT ORDER

            $item['display_group']['day_time']      = 0;
            $item['display_group']['category']      = $item['category_id'];
            $item['display_group']['category_sort'] = (int)$csa[$item['category_id']];
            $item['display_group']['type']          = 0;
            $item['display_group']['id']            = $item['id'];

            if ($item['extra']) {

                $itemt = sqlfetch(sqlquery("SELECT `type` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item['item_id']. ")"));
                $item['display_group']['type'] = $itemt['type'];

                $extra = json_decode(stripslashes($item['extra']), true);
                if ($extra['date']) {
                    if ($extra['mealtime']) {
                        $mt = $mealtimes[$extra['mealtime']]['time'];
                    } else {
                        $mt = '00:00:00';
                    }
                    $item['display_group']['day_time'] = trim(strtotime($extra['date']. ' ' .$mt));
                }

            }

        });

        $sort = array();

        // BUILD SORTING ARRAYS
        foreach ($citems as $k => $v) {
            $sort['day_time'][$k]       = $v['display_group']['day_time'];
            $sort['category'][$k]       = $v['display_group']['category'];
            $sort['category_sort'][$k]  = $v['display_group']['category_sort'];
            $sort['type'][$k]           = $v['display_group']['type'];
            $sort['id'][$k]             = $v['display_group']['id'];
        }

        // SORT
        array_multisort($sort['day_time'], SORT_ASC, $sort['category_sort'], SORT_ASC, $sort['category'], SORT_ASC, $sort['type'], SORT_ASC, $sort['id'], SORT_ASC, $citems);

        //echo print_r($citems);

        return $citems;

    }


    // STATUS COLORS
    public function statusColors() {
        $out['quote'] = array(
        );
        $out['order'] = array(
        );
        return $out;
    }


}

?>