<?php

class uccms_StoreType {


    // STATUS COLORS
    public function statusColors() {
        $out['quote'] = array(
        );
        $out['order'] = array(
        );
        return $out;
    }


    // GET BOOKINGS
    public function getBookings($from='', $to='', $vars=array()) {
        global $_uccms_ecomm;

        $out = array();

        $vars['cart_temp_hold'] = (int)$_uccms_ecomm->getSetting('cart_temp_hold');

        $cth = (int)$vars['cart_temp_hold'];

        if (is_array($vars['item_id'])) {
            $wa[] = "bb.item_id IN(" .implode(',', $vars['item_id']). ")";
        } else if ($vars['item_id']) {
            $wa[] = "bb.item_id=" .(int)$vars['item_id'];
        }

        if (is_array($vars['duration_id'])) {
            $wa[] = "bb.duration_id IN(" .implode(',', $vars['duration_id']). ")";
        } else if ($vars['duration_id']) {
            $wa[] = "bb.duration_id=" .(int)$vars['duration_id'];
        }

        if (!$from) $from = time();
        if (!$to) $to = strtotime('+1 Month');

        $dt_from = date('Y-m-d H:i:s', $from);
        $dt_to = date('Y-m-d H:i:s', $to - 1);

        /*
        if ($from) {
            $wa[] = "'" .date('Y-m-d H:i:s', $from). "'>=bb.dt_from";
        }
        if ($to) {
            $wa[] = "'" .date('Y-m-d H:i:s', $to). "'<=bb.dt_to";
        }
        */

        $wa[] = "
        (bb.dt_from BETWEEN '" .$dt_from. "' AND '" .$dt_to. "')
        OR
        (bb.dt_to BETWEEN '" .$dt_from. "' AND '" .$dt_to. "')
        OR
        (('" .$dt_from. "'>=bb.dt_from) AND ('" .$dt_to. "'<=bb.dt_to))
        ";

        //SELECT bb.*, bi.*, bb.id AS `id`, oi.quantity FROM `uccms_ecommerce_booking_bookings` AS `bb` INNER JOIN `uccms_ecommerce_orders` AS `o` ON bb.order_id=o.id INNER JOIN `uccms_ecommerce_order_items` AS `oi` ON bb.oitem_id=oi.id INNER JOIN `uccms_ecommerce_booking_items` AS `bi` ON bb.item_id=bi.id WHERE (bb.item_id=1) AND ( (bb.dt_from BETWEEN '2017-06-20 14:00:00' AND '2017-06-20 15:59:59') OR (bb.dt_to BETWEEN '2017-06-20 14:00:00' AND '2017-06-20 15:59:59') OR (('2017-06-20 14:00:00'>=bb.dt_from) AND ('2017-06-20 15:59:59'<=bb.dt_to)) ) AND ((o.status='charged') OR (o.status='complete'))

        if (is_array($vars['status'])) {
            $sa = array();
            if ($cth > 0) {
                if (!in_array('cart', $vars['status'])) {
                    $vars['status'][] = 'cart';
                }
            }
            foreach ($vars['status'] as $status) {
                $sa[] = "o.status='" .$status. "'";
            }
            $wa[] = "(" .implode(") OR (", $sa). ")";
        } else if ($vars['status']) {
            $wa[] = "(o.status='" .$vars['status']. "')";
        } else {
            if ($cth > 0) {
                $wa[] = "(o.status IN ('cart','charged','processing','shipped','complete'))";
            } else {
                $wa[] = "(o.status IN ('charged','processing','shipped','complete'))";
            }
        }

        // HAVE A CART TEMP HOLD TIME
        if ($cth > 0) {

            // INCLUDE CART ITEMS WITHIN TIMEFRAME
            $wa[] = "((o.status = 'cart') AND (oi.dt>='" .date('Y-m-d H:i:s', strtotime('-' .$cth. ' Seconds')). "')) OR (o.status != 'cart')";

        }

        // ORDERS TO IGNORE (LIKE SELF)
        if (count((array)$vars['ignore_orders']) > 0) {
            $wa[] = "o.id NOT IN(" .implode(',', array_map('intval', $vars['ignore_orders'])). ")";
        }

        // GET BOOKINGS
        $bookings_query = "
        SELECT bb.*, bi.*, bb.id AS `id`, oi.quantity
        FROM `" .$_uccms_ecomm->tables['booking_bookings']. "` AS `bb`
        INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON bb.order_id=o.id
        INNER JOIN `" .$_uccms_ecomm->tables['order_items']. "` AS `oi` ON bb.oitem_id=oi.id
        INNER JOIN `" .$_uccms_ecomm->tables['booking_items']. "` AS `bi` ON bb.item_id=bi.id
        WHERE (" .implode(") AND (", $wa). ")
        ";

        // ORDER
        if ($vars['order_by']) {
            $bookings_query .= " ORDER BY " .sqlescape($vars['order_by']);
        }

        // LIMIT
        if ($vars['limit']) {
            $bookings_query .= " LIMIT " .(int)$vars['limit'];
        }

        //echo $bookings_query. '<br />';

        $bookings_q = sqlquery($bookings_query);
        while ($booking = sqlfetch($bookings_q)) {
            $out[$booking['id']] = $booking;
        }

        return $out;

    }


    // GET BOOKINGS FOR CALENDAR
    public function calendarBookings($id, $from='', $to='', $vars=array()) {
        global $_uccms_ecomm;

        $out = array();

        $id = (int)$id;

        if ($id) {

            // read global cart_temp_hold and pass into getBookings vars below

            // GET BOOKING ITEMS WITH CALENDAR
            $bi_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`calendar_id`=" .$id. ")";
            $bi_q = sqlquery($bi_query);
            while ($bi = sqlfetch($bi_q)) {

                $out = $out + $this->getBookings($from, $to, array(
                    'item_id' => $bi['id']
                ));

            }

        }

        return $out;

    }


    // GET AVAILABILITY
    public function availability($vars=array()) {
        global $_uccms_ecomm;

        $out = array();

        // ITEM ARRAY
        $ia = array();

        // BY CALENDAR
        if ($vars['calendar_id']) {

            // GET ALL ITEMS WITH CALENDAR
            $items_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`calendar_id`=" .(int)$vars['calendar_id']. ")";
            $items_q = sqlquery($items_query);
            while ($item = sqlfetch($items_q)) {
                $ia[$item['id']] = $item;
            }

        // BY ITEM
        } else if ($vars['item_id']) {

            $ia[$vars['item_id']]['id'] = (int)$vars['item_id'];

        }

        // HAVE ITEMS
        if (count($ia) > 0) {

            // BY DAY
            if (($vars['by'] == 'd') || (!$vars['by'])) {

                $from = strtotime(date('Y-m-d', $vars['from']));

                if (($vars['to']) && ($vars['to'] != '0000-00-00 00:00:00')) {
                    $to = strtotime(date('Y-m-d', $vars['to']));
                } else {
                    $to = $from;
                }

                $current = $from;

                $da = array();

                while ($current <= $to) {
                    $da[] = date('Y-m-d', $current);
                    $current = strtotime(date('Y-m-d', strtotime('+1 Day', $current)));
                }

                // HAVE DAYS
                if (count($da) > 0) {

                    // ARRAY OF DURATIONS
                    $dura = array();

                    // DAYS OF THE WEEK
                    $dow = $_uccms_ecomm->daysOfWeek();

                    // LOOP THROUGH ITEMS
                    foreach ($ia as $item) {

                        $vars['max_simultaneous_bookings'] = $item['max_simultaneous_bookings'];

                        // GET DURATIONS
                        $duration_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`item_id`=" .$item['id']. ") AND (`visible`=1) ORDER BY `sort` ASC";
                        $duration_q = sqlquery($duration_query);
                        while ($duration = sqlfetch($duration_q)) {
                            $dura[$item['id']][$duration['id']] = $duration;
                        }

                    }

                    // DAY INFO ARRAY
                    $dia = array();

                    // LOOP
                    foreach ($da as $date) {

                        //echo '<b>' .$date. '</b><br />';

                        $out[$date]['available'] = 0;
                        $out[$date]['bookings']  = array();

                        // BY CALENDAR
                        if ($vars['calendar_id']) {
                            $where_sql = "(`calendar_id`=" .(int)$vars['calendar_id']. ") AND ";

                        // BY ITEM
                        } else if ($vars['item_id']) {
                            $where_sql = "(`item_id`=" .(int)$vars['item_id']. ") AND ";
                        }

                        $day_num = date('N', strtotime($date));

                        $day = array();

                        // ALREADY HAVE DAY INFO
                        if ($dia[$day_num]['id']) {

                            $day = $dia[$day_num];

                        // DON'T HAVE DAY INFO
                        } else {

                            // GET DAY INFO
                            $day_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_days']. "` WHERE " .$where_sql. " (`day`=" .$day_num. ")";
                            $day_q = sqlquery($day_query);
                            $day = sqlfetch($day_q);

                            $dia[$day_num] = $day;

                        }

                        // DAY IS BOOKABLE
                        if ($day['bookable']) {

                            // SEE IF IS A BLACKED OUT DATE
                            $bd_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_calendar_blackouts']. "` WHERE " .$where_sql. " ('" .$date. " 00:00:00'>=`dt_from`) AND ('" .$date. " 00:00:00'<=`dt_to`)";
                            $bd_q = sqlquery($bd_query);
                            if (sqlrows($bd_q) > 0) {

                                $bd = sqlfetch($bd_q);

                                $out[$date]['reason_id'] = $bd['reason_id'];

                                if ($vars['item_id']) {

                                    $out[$date]['items'][$vars['item_id']]['available'] = 0;

                                    $out[$date]['items'][$vars['item_id']]['reason_id'] = $bd['reason_id'];

                                }

                            // NOT BLACKED OUT DATE
                            } else {

                                // LOOP THROUGH ITEMS
                                foreach ($ia as $item) {

                                    $out[$date]['items'][$item['id']]['available'] = 0;

                                    // DURATIONS FOR ITEM
                                    $durations = $dura[$item['id']];

                                    $start  = $day['start'];
                                    $end    = $day['end'];

                                    // HAVE DURATIONS
                                    if (count($durations) > 0) {

                                        // LOOP
                                        foreach ($durations as $duration) {

                                            // NO DAYS AVAILABLE
                                            if (!$duration['days']) {
                                                continue;
                                            }

                                            // NOT AVAILABLE ON THIS DAY
                                            if (!in_array($dow[$day['day']]['code'], explode(',', $duration['days']))) {
                                                continue;
                                            }

                                            $tdur = array();

                                            switch ($duration['unit']) {
                                                case 'd':
                                                    $what_string = 'Days';
                                                    break;
                                                case 'h':
                                                    $what_string = 'Hours';
                                                    break;
                                                default:
                                                    $what_string = 'Minutes';
                                                    break;
                                            }

                                            if ($duration['start'] == '00:00:00') {
                                                $start = $day['start'];
                                            } else {
                                                $start = $duration['start'];
                                            }

                                            if ($duration['end'] == '00:00:00') {
                                                $end = $day['end'];
                                            } else {
                                                $end = $duration['end'];
                                            }

                                            // ALL DAY
                                            if (($start == '00:00:00') && ($end == '00:00:00')) {

                                                // BY DAYS
                                                if ($duration['unit'] == 'd') {
                                                    $end = date('Y-m-d H:i:s', strtotime('+ ' .($duration['num'] - 1). ' ' .$what_string, strtotime($date)));
                                                }

                                                // GET AVAILABILITY
                                                $avail = $this->getAvailability(strtotime($date. ' 00:00:00'), strtotime($end), $vars);

                                                //echo print_r($avail). "\n\n";

                                                $out[$date]['available'] += $avail['available'];
                                                $out[$date]['items'][$item['id']]['available'] += $avail['available'];

                                                // HAVE BOOKINGS
                                                if (count($avail['bookings']) > 0) {

                                                    // LOOP
                                                    foreach ($avail['bookings'] as $booking) {
                                                        if (!$out[$date]['bookings'][$booking['id']]) {
                                                            if ($booking['duration_id']) {
                                                                $booking['duration_title'] = stripslashes($durations[$booking['duration_id']]['title']);
                                                            }
                                                            $out[$date]['bookings'][$booking['id']] = $booking;
                                                        }
                                                    }

                                                }

                                                // DURATION
                                                $tdur[] = array_merge(
                                                    array(
                                                        'id'        => $duration['id'],
                                                        'title'     => stripslashes($duration['title']),
                                                        'start'     => $date. ' 00:00:00',
                                                        'end'       => $end,
                                                        'markup'    => $duration['markup'],
                                                    ), $avail
                                                );

                                            // PARTIAL DAY
                                            } else {

                                                // TIMESTAMPS
                                                $ts_start = strtotime(date('Y-m-d ' .$start, strtotime($date)));
                                                $ts_end = strtotime(date('Y-m-d ' .$end, strtotime($date)));

                                                // BY DAYS
                                                if ($duration['unit'] == 'd') {
                                                    $ts_end += ((60 * 60 * 24) * (int)$duration['num']);
                                                }

                                                $i = 0;

                                                $curr = $ts_start;

                                                // LOOP THROUGH DURATIONS TILL END
                                                while ($curr < $ts_end) {

                                                    $until = strtotime('+' .$duration['num']. ' ' .$what_string, $curr);

                                                    if ($until > $ts_end) {
                                                        break;
                                                    }

                                                    // GET AVAILABILITY
                                                    $avail = $this->getAvailability($curr, $until, $vars);

                                                    // IS CALENDAR
                                                    if ($vars['calendar_id']) {

                                                        // HAVE MAX
                                                        if ($vars['max_simultaneous_bookings']) {

                                                            if ($avail['available'] > $vars['max_simultaneous_bookings']) {
                                                                $out[$date]['available'] = $vars['max_simultaneous_bookings'];
                                                            } else {
                                                                $out[$date]['available'] += $avail['available'];
                                                            }

                                                        // INCREMENT
                                                        } else {
                                                            $out[$date]['available'] += $avail['available'];
                                                        }

                                                    // NOT CALENDAR
                                                    } else {
                                                        $out[$date]['available'] += $avail['available'];
                                                    }


                                                    /*
                                                    if (($item['id'] == 1) && (date('Y-m-d', $curr) == '2017-06-20')) {

                                                    //echo print_r($avail). '<br />';
                                                    echo date('Y-m-d H:i:s', $curr). ' - ' .date('Y-m-d H:i:s', $until). '<br />';
                                                    echo $avail['available']. ' - ' .$out[$date]['available']. '<br /><br />';

                                                    }
                                                    */


                                                    $out[$date]['items'][$item['id']]['available'] += $avail['available'];

                                                    //echo $out[$date]['available']. '<br /><br />';

                                                    // HAVE BOOKINGS
                                                    if (count($avail['bookings']) > 0) {

                                                        // LOOP
                                                        foreach ($avail['bookings'] as $booking) {
                                                            if (!$out[$date]['bookings'][$booking['id']]) {
                                                                if ($booking['duration_id']) {
                                                                    $booking['duration_title'] = stripslashes($durations[$booking['duration_id']]['title']);
                                                                }
                                                                $out[$date]['bookings'][$booking['id']] = $booking;
                                                            }
                                                        }

                                                    }

                                                    // DURATION
                                                    $tdur[$i] = array_merge(
                                                        array(
                                                            'i'         => $i,
                                                            'id'        => $duration['id'],
                                                            'title'     => stripslashes($duration['title']),
                                                            'start'     => date('Y-m-d H:i:s', $curr),
                                                            'end'       => date('Y-m-d H:i:s', $until),
                                                            'markup'    => $duration['markup'],
                                                        ), $avail
                                                    );

                                                    $curr = $until;

                                                    $i++;

                                                }

                                                // DIFFERENT DAY
                                                if (date('Y-m-d', $ts_start) != date('Y-m-d', $until)) {
                                                    $tdur[$i-1]['end'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d ' .$end, $ts_end)));
                                                }

                                            }

                                            // ADD DURATION TO OUTPUT
                                            $out[$date]['items'][$item['id']]['durations'][$duration['id']] = $tdur;

                                        }

                                        unset($tdur);

                                    // NO DURATIONS
                                    } else {

                                        $out[$date]['items'][$item['id']]['durations'][0] = array(
                                            'start' => $start,
                                            'end'   => $end
                                        );

                                    }

                                }

                            }

                        // DAY IS NOT BOOKABLE
                        } else {

                            $out[$date]['bookings']     = array();
                            $out[$date]['available']    = 0;

                            if ($vars['item_id']) {
                                $out[$date]['items'][$vars['item_id']]['available'] = 0;
                            }

                        }

                    }

                }

            }

        }

        //echo print_r($out);
        //exit;

        return $out;

    }


    // GET ITEM AVAILABILITY
    public function getAvailability($from='', $to='', $vars) {
        global $_uccms_ecomm;

        $out = array();

        unset($bookings);

        // BY CALENDAR
        if ($vars['calendar_id']) {
            $bookings = $this->calendarBookings((int)$vars['calendar_id'], $from, $to, $vars);

        // BY ITEM
        } else if ($vars['item_id']) {
            $bookings = $this->getBookings($from, $to, $vars);
        }

        /*
        if (date('Y-m-d', $from) == '2017-06-20') {
            echo print_r($bookings);
        }
        */

        // HAVE BOOKINGS
        if (count($bookings) > 0) {

            //echo print_r($bookings). '<br />';
            //exit;

            // ITEM BOOKED ARRAY
            $iba = array();

            // ITEM LIMIT ARRAY
            $ila = array();

            // LOOP
            foreach ($bookings as $booking) {
                $iba[$booking['item_id']] += $booking['quantity'];
                $ila[$booking['item_id']] = $booking['max_simultaneous_bookings'];
            }

            // HAVE BOOKED ARRAY
            if (count($iba) > 0) {

                // LOOP
                foreach ($iba as $item_id => $count) {

                    // HAVE LIMIT
                    if ($ila[$item_id]) {
                        $avail = $ila[$item_id] - $count;
                        if ($avail <= 0) $avail = 0;

                    // NO LIMIT
                    } else {
                        $avail = 99999;
                    }

                    // CALENDAR SPECIFIED
                    if ($vars['calendar_id']) {

                        $out['available'] += $avail;

                        // MULTIPLE ITEMS
                        $out['items'][$item_id]['available'] = $avail;

                    // NO CALENDAR - SAME ITEM
                    } else if ($item_id == $vars['item_id']) {

                        // SINGLE ITEM
                        $out['available'] = $avail;

                    }

                }

            }

        // NO BOOKINGS
        } else {

            // CALENDAR AND MAX SPECIFIED
            if (($vars['calendar_id']) && ($vars['max_simultaneous_bookings'])) {

                $out['available'] = $vars['max_simultaneous_bookings'];

            // HAVE ITEM ID
            } else if ($vars['item_id']) {

                // GET BOOKING ITEM INFO
                $booking_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$vars['item_id']. ")";
                $booking_item_q = sqlquery($booking_item_query);
                $booking_item = sqlfetch($booking_item_q);

                // HAVE LIMIT
                if ($booking_item['max_simultaneous_bookings']) {
                    $out['available'] = $booking_item['max_simultaneous_bookings'];

                // NO LIMIT
                } else {
                    $out['available'] = 99999;
                }

            // NO ITEM ID
            } else {
                $out['available'] = 99999;
            }

        }

        // HAVE BOOKINGS ON THIS DAY
        $out['bookings'] = $bookings;

        return $out;

    }


    // DURATION TITLE
    public function durationTitle($duration=array()) {

        if ($duration['title']) {
            $out = stripslashes($duration['title']);
        } else {
            $out = number_format($duration['num'], 0);
            switch ($duration['unit']) {
                case 'm':
                    $out .= ' minute';
                    break;
                case 'h':
                    $out .= ' hour';
                    break;
                case 'd':
                    $out .= ' day';
                    break;
            }
            if ($duration['num'] != 1) {
                $out .= 's';
            }
        }
        return $out;
    }


    // BOOKING PERSONS
    public function bookingPersons($oitem_id) {
        global $_uccms_ecomm;

        $out = array();

        // GET PERSONS INFO FOR THIS CART ITEM
        $persons_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` WHERE (`oitem_id`=" .(int)$oitem_id. ") ORDER BY `sort` ASC, `id` ASC";
        $persons_q = sqlquery($persons_query);
        while ($person = sqlfetch($persons_q)) {
            $out[$person['id']] = $person;
        }

        return $out;

    }

}

?>