<?php

class global_uccms_Ecommerce extends uccms_Ecommerce {


    #############################
    # GENERAL: Sitemap
    #############################

    public function general_sitemap($page=array()) {
        global $bigtree;

        $out = array();

        // CATEGORIES
        $query = "SELECT * FROM `" .$this->tables['categories']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => str_replace($bigtree['path'][0], $this->storePath(), $this->categoryURL($r['id'], $r))
            );
        }

        // ITEMS
        $query = "SELECT * FROM `" .$this->tables['items']. "` WHERE (`active`=1) ORDER BY `slug` ASC";
        $q = sqlquery($query);
        while ($r = sqlfetch($q)) {
            $out[] = array(
                'link'  => substr(WWW_ROOT, 0, -1) . $this->itemURL($r['id'], 0, $r)
            );
        }

        return $out;

    }


    #############################
    # GENERAL: Search
    #############################

    public function general_search($q='', $params=array()) {

        $results = array();

        // GET MATCHING ITEMS
        $results_query = "SELECT `id`, `slug`, `title` FROM `" .$this->tables['items']. "` WHERE (`title` LIKE '%" .$q. "%') ORDER BY `updated_dt` DESC LIMIT " .(int)$params['limit'];
        $results_q = sqlquery($results_query);
        while ($row = sqlfetch($results_q)) {
            if ($row['slug']) {
                $slug = stripslashes($row['slug']);
            } else {
                $slug = $this->makeRewrite($row['title']);
            }
            $results[] = array(
                'title' => stripslashes($row['title']),
                'url'   => '/' .$this->storePath(). '/item/' .trim($slug). '-' .$row['id']. '/',
                'type'  => 'ecommerce'
            );
        }

        return $results;

    }


    #############################
    # GENERAL: Cron
    #############################

    // RUN THE MAIN CRON
    public function runCron() {
        global $_uccms;

        $out = array();

        // WITHIN 4:00 AM AND 6:00 AM
        if ((time() >= strtotime(date('n/j/Y 04:00:00'))) && (time() <= strtotime(date('n/j/Y 06:00:00')))) {

            #########################
            # SCHEDULED PAYMENT - EMAIL
            #########################

            // GET NOT-EMAILED SCHEDULED PAYMENTS FOR TODAY
            $sp_query = "SELECT * FROM `" .$this->tables['scheduled_payments']. "` WHERE (`dt_email`='" .date('Y-m-d'). "') AND (`emailed`=0)";
            $sp_q = sqlquery($sp_query);
            while ($sp = sqlfetch($sp_q)) {

                // HAVE PAYMENT PROFILE ID AND CHARGING TODAY - DON'T EMAIL
                if (($sp['payment_profile_id'] > 0) && ($sp['dt_due'] == date('Y-m-d'))) {
                    continue;
                }

                // GET ORDER INFO
                $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$sp['order_id']. ")";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                // ORDER FOUND
                if ($order['id']) {

                    // GET EXTRA INFO
                    $extra = $this->cartExtra($sp['order_id']);

                    if (!$_POST['send']['to_name']) $_POST['send']['to_name'] = trim(stripslashes($extra['contact_firstname']. ' ' .$extra['contact_lastname']));
                    if (!$_POST['send']['to_email']) $_POST['send']['to_email'] = stripslashes($extra['contact_email']);

                    // EMAIL SETTINGS
                    $esettings = array(
                        'template'      => 'scheduled_payment-due.html',
                        'subject'       => 'Payment Due',
                        'order_id'      => $sp['order_id'],
                        'from_name'     => stripslashes($this->getSetting('email_from_name')),
                        'from_email'    => stripslashes($this->getSetting('email_from_email')),
                        'to_name'       => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                        'to_email'      => stripslashes($order['billing_email']),
                        'sent_by'       => 0
                    );

                    if ($sp['dt_due'] == date('Y-m-d')) {
                        $sp_date = 'today';
                    } else {
                        $sp_date = ' by ' .date('n/j/Y', strtotime($sp['dt_due']));
                    }

                    if ($sp['title']) {
                        $sp_for = ' for "' .stripslashes($sp['title']). '"';
                    }

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'order_id'              => $sp['order_id'],
                        'order_hash'            => $order['hash'],
                        'firstname'             => stripslashes($order['billing_firstname']),
                        'message'               => '<b>A payment of $' .$sp['amount']. ' is due ' .$sp_date . $sp_for. '.</b>',
                        'order_link'            => WWW_ROOT . $this->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash'])
                    );

                    // SEND EMAIL
                    $eresult = $this->sendEmail($esettings, $evars);

                    // HAVE ERROR
                    if ($eresult['err']) {
                        $out[] = 'Failed to email for scheduled payment ID: ' .$sp['id'];

                    // NO ERROR
                    } else {

                        // UPDATE - MARK AS SENT
                        $update_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `emailed`=1 WHERE (`id`=" .$sp['id']. ")";
                        sqlquery($update_query);

                    }

                // ORDER NOT FOUND
                } else {

                    $out[] = 'Order (ID: ' .$sp['order_id']. ') not found for scheduled payment ID: ' .$sp['id']. '. Deleting scheduled payment..';

                    // DELETE SCHEDULED PAYMENT
                    $delete_query = "DELETE FROM `" .$this->tables['scheduled_payments']. "` WHERE (`id`=" .$sp['id']. ")";
                    sqlquery($delete_query);

                }

            }

            #########################
            # SCHEDULED PAYMENT - CHARGE
            #########################

            // GET NOT-CHARGED SCHEDULED PAYMENTS FOR TODAY
            $sp_query = "SELECT * FROM `" .$this->tables['scheduled_payments']. "` WHERE (`dt_due`='" .date('Y-m-d'). "') AND (`payment_profile_id`>0) AND (`charged`=0)";
            $sp_q = sqlquery($sp_query);
            while ($sp = sqlfetch($sp_q)) {

                $transaction_id = 0;

                // GET ORDER INFO
                $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$sp['order_id']. ")";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

                // ORDER FOUND
                if ($order['id']) {

                    // GET EXTRA INFO
                    $extra = $this->cartExtra($sp['order_id']);

                    // EMAIL SETTINGS
                    $esettings = array(
                        'order_id'      => $order['id'],
                        'from_name'     => stripslashes($this->getSetting('email_from_name')),
                        'from_email'    => stripslashes($this->getSetting('email_from_email')),
                        'to_name'       => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                        'to_email'      => stripslashes($order['billing_email']),
                        'sent_by'       => 0
                    );

                    if ($sp['title']) {
                        $payment_title = '"' .stripslashes($sp['title']). '"';
                    } else {
                        $payment_title = 'order #' .$order['id'];
                    }

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'order_id'              => $sp['order_id'],
                        'order_hash'            => $order['hash'],
                        'firstname'             => stripslashes($order['billing_firstname']),
                        'payment_title'         => $payment_title,
                        'amount'                => number_format($this->toFloat($sp['amount']), 2),
                        'order_link'            => WWW_ROOT . $this->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash'])
                    );

                    // GET PAYMENT PROFILE
                    $pp = $_uccms['_account']->pp_getProfile($sp['payment_profile_id'], $order['customer_id']);

                    // PAYMENT PROFILE FOUND
                    if ($pp['id']) {

                        $payment_method     = 'profile';
                        $payment_name       = stripslashes($pp['name']);
                        $payment_lastfour   = $pp['lastfour'];
                        $payment_expiration = $pp['expires'];

                        // PAYMENT - CHARGE
                        $payment_data = array(
                            'amount'    => $sp['amount'],
                            'customer' => array(
                                'id'            => $order['customer_id'],
                                'name'          => $payment_name,
                                'email'         => $order['billing_email'],
                                'phone'         => $order['billing_phone'],
                                'address'       => array(
                                    'street'    => $order['billing_address1'],
                                    'street2'   => $order['billing_address2'],
                                    'city'      => $order['billing_city'],
                                    'state'     => $order['billing_state'],
                                    'zip'       => $order['billing_zip'],
                                    'country'   => $order['billing_city']
                                ),
                                'description'   => ''
                            ),
                            'note' => 'Scheduled payment on order ID: ' .$order['id'],
                            'card' => array(
                                'id'            => $pp['id'],
                                'name'          => $payment_name,
                                'number'        => $payment_lastfour,
                                'expiration'    => $payment_expiration
                            )
                        );

                        // PROCESS PAYMENT
                        $payment_result = $_uccms['_account']->payment_charge($payment_data);

                        // TRANSACTION ID
                        $transaction_id = $payment_result['transaction_id'];

                        // TRANSACTION MESSAGE (ERROR)
                        $transaction_message = $payment_result['error'];

                        // LOG TRANSACTION
                        $this->logTransaction(array(
                            'cart_id'               => $order['id'],
                            'status'                => ($transaction_id ? 'charged' : 'failed'),
                            'amount'                => $sp['amount'],
                            'method'                => 'scheduled',
                            'payment_profile_id'    => $payment_result['profile_id'],
                            'name'                  => $payment_name,
                            'lastfour'              => $payment_lastfour,
                            'expiration'            => $payment_expiration,
                            'transaction_id'        => $transaction_id,
                            'message'               => $transaction_message,
                            //'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                        ));

                        // CHARGE SUCCESSFUL
                        if ($transaction_id) {

                            // CALCULATE BALANCE
                            $balance = $this->orderBalance($order['id'], $sp['amount']);

                            // ADD TO ORDER INFO
                            $order['transaction_id'] = $transaction_id;

                            $columns = array(
                                'status'    => 'charged'
                            );

                            if ($balance == 0.00) {
                                $columns['paid_in_full'] = 1;
                            }

                            // UPDATE ORDER
                            $cart_query = "UPDATE `" .$this->tables['orders']. "` SET " .$this->createSet($columns). " WHERE (`id`=" .$order['id']. ")";
                            sqlquery($cart_query);

                            // UPDATE SCHEDULED PAYMENT
                            $cart_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `charged`=1 WHERE (`id`=" .$sp['id']. ")";
                            sqlquery($cart_query);

                            $esettings['template'] = 'scheduled_payment-processed.html';
                            $esettings['subject']  = 'Scheduled Payment Processed';

                        // TRANSACTION FAILED
                        } else {

                            // UPDATE SCHEDULED PAYMENT
                            $cart_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `charged`=2 WHERE (`id`=" .$sp['id']. ")";
                            sqlquery($cart_query);

                            $esettings['template']  = 'scheduled_payment-failed.html';
                            $esettings['subject']   = 'Scheduled Payment Failed';
                            $esettings['error']     = $transaction_message;

                        }

                    // PAYMENT PROFILE NOT FOUND
                    } else {

                        // UPDATE SCHEDULED PAYMENT
                        $cart_query = "UPDATE `" .$this->tables['scheduled_payments']. "` SET `charged`=2 WHERE (`id`=" .$sp['id']. ")";
                        sqlquery($cart_query);

                        $esettings['template']  = 'scheduled_payment-failed.html';
                        $esettings['subject']   = 'Scheduled Payment Failed';
                        $esettings['message']   = 'Payment method not found.';

                    }

                    // SEND EMAIL
                    $eresult = $this->sendEmail($esettings, $evars);

                    // PAYMENT SUCCESSFUL
                    if ($transaction_id) {

                        // LOG MESSAGE
                        $this->logQuoteMessage(array(
                            'order_id'      => $order['id'],
                            'by'            => 0,
                            'message'       => 'Paid $' .number_format($this->toFloat($sp['amount']), 2). ' with credit card ending in ' .$payment_lastfour. '.',
                            'status'        => 'charged',
                            'email_log_id'  => $eresult['log_id']
                        ));

                    }

                    // COPY OF EMAIL TO ADMIN(S)
                    $admin_emails = $this->getSetting('email_order_copy');
                    if ($admin_emails) {
                        $emails = explode(',', stripslashes($admin_emails));
                        foreach ($emails as $email) {
                            $esettings['to_name']   = '';
                            $esettings['to_email']  = $email;
                            $this->sendEmail($esettings, $evars);
                        }
                    }

                    /*
                    // ORDER DATA FOR REVSOCIAL API
                    $rs_orderdata = array(
                        'billing' => array(
                            'firstname'     => stripslashes($order['billing_firstname']),
                            'lastname'      => stripslashes($order['billing_lastname']),
                            'company'       => stripslashes($order['billing_company']),
                            'address1'      => stripslashes($order['billing_address1']),
                            'address2'      => stripslashes($order['billing_address2']),
                            'city'          => stripslashes($order['billing_city']),
                            'state'         => stripslashes($order['billing_state']),
                            'zip'           => stripslashes($order['billing_zip']),
                            'country'       => stripslashes($order['billing_country']),
                            'phone'         => stripslashes($order['billing_phone']),
                            'email'         => stripslashes($order['billing_email'])
                        ),
                        'shipping' => array(
                            'firstname'     => stripslashes($order['shipping_firstname']),
                            'lastname'      => stripslashes($order['shipping_lastname']),
                            'company'       => stripslashes($order['shipping_company']),
                            'address1'      => stripslashes($order['shipping_address1']),
                            'address2'      => stripslashes($order['shipping_address2']),
                            'city'          => stripslashes($order['shipping_city']),
                            'state'         => stripslashes($order['shipping_state']),
                            'zip'           => stripslashes($order['shipping_zip']),
                            'country'       => stripslashes($order['shipping_country']),
                            'phone'         => stripslashes($order['shipping_phone']),
                            'email'         => stripslashes($order['shipping_email'])
                        ),
                        'order' => $order
                    );
                    $rs_orderdata['order']['total'] = $sp['amount'];
                    $rs_orderdata['items']          = $rs_items;

                    // RECORD ORDER WITH REVSOCIAL
                    $_uccms_ecomm->revsocial_recordOrder($order['id'], $order['customer_id'], $rs_orderdata);
                    */

                // ORDER NOT FOUND
                } else {

                    $out[] = 'Order (ID: ' .$sp['order_id']. ') not found for scheduled payment ID: ' .$sp['id']. '. Deleting scheduled payment..';

                    // DELETE SCHEDULED PAYMENT
                    $delete_query = "DELETE FROM `" .$this->tables['scheduled_payments']. "` WHERE (`id`=" .$sp['id']. ")";
                    sqlquery($delete_query);

                }

            }

            #########################
            # CLEAN UP OLD FILES
            #########################

            // GET NOT-PLACED ORDERS PLACED WITHIN 30 AND 40 DAYS
            $order_query = "SELECT `id` FROM `" .$this->tables['orders']. "` WHERE (`status`='cart') AND ((`dt`>='" .date('Y-m-d H:i:s', strtotime('-40 Days')). "') AND (`dt`<='" .date('Y-m-d H:i:s', strtotime('-30 Days')). "'))";
            $order_q = sqlquery($order_query);
            while ($order = sqlfetch($order_q)) {

                // GET ORDER ITEMS
                $item_query = "SELECT `options` FROM `" .$this->tables['order_items']. "` WHERE (`cart_id`=" .$order['id']. ")";
                $item_q = sqlquery($item_query);
                while ($item = sqlfetch($item_q)) {

                    unset($options);

                    // HAVE OPTIONS
                    if ($item['options']) {

                        // DECODE OPTIONS
                        $options = json_decode(stripslashes($item['options']), true);

                        // HAVE ARRAY
                        if (count((array)$options) > 0) {
                            foreach ($options as $option_id => $option_value) {

                                // HAVE VALUE(S)
                                if (count((array)$option_value) > 0) {

                                    // GET OPTION INFO
                                    $option_info = sqlfetch(sqlquery("SELECT `type` FROM `" .$this->tables['attributes']. "` WHERE (`id`=" .$option_id. ")"));

                                    // IS FILE
                                    if ($option_info['type'] == 'file') {
                                        foreach ($option_value as $file) {
                                            @unlink(SITE_ROOT. 'extensions/' .$this->Extension. '/files/orders/' .$order['id']. '-' .$file);
                                            $out[] = 'Removed file (' .$file. ') for order: ' .$order['id'];
                                        }
                                    }

                                }

                            }
                        }

                    }

                }

            }

        }

        return implode("\n", $out). "\n";

    }


    #############################
    # PAYMENT - Transactions
    #############################

    public function payment_transactions($vars=array()) {

        $out = array();

        $wa = array();

        // DATE
        if (count((array)$vars['date']) == 2) {
            $wa[] = "(`dt`>='" .date('Y-m-d', strtotime($vars['date']['from'])). " 00:00:00') AND (`dt`<='" .date('Y-m-d', strtotime($vars['date']['to'])). " 23:59:59')";
        } else if ($vars['date']['from']) {
            $wa[] = "`dt`>='" .date('Y-m-d', strtotime($vars['date']['from'])). " 00:00:00'";
        } else if ($vars['date']['to']) {
            $wa[] = "`dt`<='" .date('Y-m-d', strtotime($vars['date']['to'])). " 23:59:59'";
        }

        // KEYWORD
        if ($vars['q']) {
            $q = trim(sqlescape($vars['q']));
            $wa[] = "(`name` LIKE '%" .$q. "%') OR (`lastfour` LIKE '%" .$q. "%')";
        }

        // ORDERBY - Date
        if ($vars['query']['orderby']['field'] == 'dt') {
            $orderby_sql = " ORDER BY `dt` " .($vars['query']['orderby']['direction'] == "desc" ? "DESC" : "ASC"). " ";
        }

        // LIMIT
        if ($vars['query']['limit']['num'] > 0) {
            $limit_sql = " LIMIT " .(int)$vars['query']['limit']['num']. " ";
        }

        if (count($wa) > 0) {
            $where_sql = " WHERE (" .implode(" ) AND ( ", $wa). ")";
        } else {
            $where_sql = "";
        }

        $accounta = array();

        // GET TRANSACTION LOGS
        $trans_query = "SELECT * FROM `" .$this->tables['transaction_log']. "` " .$where_sql . $orderby_sql . $limit_sql;
        $trans_q = sqlquery($trans_query);
        while ($trans = sqlfetch($trans_q)) {

            // GET ORDER INFO
            $order_query = "SELECT * FROM `" .$this->tables['orders']. "` WHERE (`id`=" .$trans['cart_id']. ")";
            $order_q = sqlquery($order_query);
            $order = sqlfetch($order_q);

            // HAVE ACCOUNT ID
            if (($trans['account_id']) && (!$accounta[$trans['account_id']])) {

                // GET ACCOUNT INFO
                $account_query = "
                SELECT *
                FROM `uccms_accounts` AS `a`
                INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
                WHERE (a.id=" .$trans['account_id']. ")
                ";
                $account_q = sqlquery($account_query);
                $accounta[$trans['account_id']] = sqlfetch($account_q);

            }

            if ($accounta[$trans['account_id']]['id']) {
                $account = $accounta[$trans['account_id']];
            } else {
                $account = array();
            }

            $out[] = array(
                'number'        => $order['id'],
                'dt'            => $trans['dt'],
                'name'          => trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])),
                'method'        => $trans['method'],
                'lastfour'      => $trans['lastfour'],
                'description'   => 'E-Commerce',
                'amount'        => $trans['amount'],
                'success'       => ($trans['status'] == 'charged' ? true : false),
                'url'           => array(
                    'edit' => '/admin/' .$this->Extension. '*ecommerce/orders/edit/?id=' .$order['id']
                ),
            );

        }

        return $out;

    }


}

?>