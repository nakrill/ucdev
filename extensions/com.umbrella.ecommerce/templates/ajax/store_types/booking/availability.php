<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$out = array();

// FROM DATE SPECIFIED
if ($_REQUEST['from']) {
    $from = date('Y-m-d', strtotime($_REQUEST['from']));

// FROM DATE NOT SPECIFIED
} else {
    $from = date('Y-m-01'); // FIRST OF THIS MONTH
}

// TO DATE SPECIFIED
if ($_REQUEST['to']) {
    $to = date('Y-m-d', strtotime($_REQUEST['to']));

// TO DATE NOT SPECIFIED
} else {
    $to = date('Y-m-t', strtotime($from)); // LAST DAY OF FROM MONTH
}

$avail_settings = array(
    'from'      => strtotime('-7 Days', strtotime($from)),
    'to'        => strtotime('+7 Days', strtotime($to))
);

if ($_REQUEST['calendar_id']) {
    $avail_settings['calendar_id'] = (int)$_REQUEST['calendar_id'];
} else {
    $avail_settings['item_id'] = (int)$_REQUEST['item_id'];
}

// GET AVAILABILITY
$out = $_uccms_ecomm->stc->availability($avail_settings);

echo json_encode($out);

?>