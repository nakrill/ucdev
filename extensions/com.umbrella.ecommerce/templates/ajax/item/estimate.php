<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$out = array();

// HAVE ITEM ID
if ($_REQUEST['item']['item_id']) {

    $oitem['id'] = (int)$_REQUEST['item']['id'];

    include(dirname(__FILE__). '/../../routed/shop/data/item_cart.php');

    // need to take attributes into account

    $out['price'] = $item['other']['price'];

// NO ITEM ID
} else {
    $out['err'] = 'Item not specified.';
}

echo json_encode($out);

?>