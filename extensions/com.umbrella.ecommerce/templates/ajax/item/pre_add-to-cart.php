<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$out = array();

// INITIALIZE CART
$cart = $_uccms_ecomm->initCart();

// HAVE CART ID
if ($cart['id']) {

    // TRY ADDING TO CART
    $result = $_uccms_ecomm->cartAddItem($cart['id'], $_REQUEST, false, true);

    // HAVE ERRORS
    if ($result['error']) {
        $out['error'] = $_uccms['_site-message']->display();
    }

}

echo json_encode($out);

?>