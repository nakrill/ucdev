<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$out = array();

// INITIALIZE CART
$cart = $_uccms_ecomm->initCart();

// HAVE CART ID
if ($cart['id']) {

    // TRY ADDING TO CART
    $result = $_uccms_ecomm->cartAddItem($cart['id'], $_REQUEST, false, true);

    // NO ERRORS
    if (count((array)$result['error']) == 0) {

        // ADD TO CART
        $result = $_uccms_ecomm->cartAddItem($cart['id'], $_REQUEST);

    }

    // HAVE ERRORS
    if ($result['error']) {

        $out['error'] = $_uccms['_site-message']->display();

    } else {

        $out['success'] = true;

        $out['item'] = $result['item'];

        $citems = $_uccms_ecomm->cartItems($cart['id']);
        $out['num_items'] = count((array)$citems);

    }

}

echo json_encode($out);

?>