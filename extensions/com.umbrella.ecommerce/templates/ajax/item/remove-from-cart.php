<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$out = [];

// INITIALIZE CART
$cart = $_uccms_ecomm->initCart();

// HAVE CART ID
if ($cart['id']) {

    // REMOVE FROM CART
    $out = $_uccms_ecomm->cartRemoveItem((int)$_REQUEST['id'], $cart['id']);

}

echo json_encode($out);

?>