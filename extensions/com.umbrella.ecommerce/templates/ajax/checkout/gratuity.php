<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$out = array();

// INIT CART
$cart = $_uccms_ecomm->initCart(false, $_uccms_ecomm->cartID());

// HAVE CART ID
if ($cart['id']) {

    $out['gratuity'] = 0.00;

    // GRATUITY ENABLED
    if ($_uccms_ecomm->getSetting('gratuity_enabled')) {

        // GET CART ITEMS
        $citems = $_uccms_ecomm->cartItems($cart['id']);

        // HAVE ITEMS
        if (count($citems) > 0) {

            // LOOP THROUGH ITEMS
            foreach ($citems as $oitem) {

                // GET ITEM INFO
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

            }

        }

        // CALCULATE GRATUITY
        $out['gratuity'] = $_uccms_ecomm->orderGratuity($cart);

    }

// NO CART ID
} else {
    $out['err'] = 'Cart not found.';
}

echo json_encode($out);

?>