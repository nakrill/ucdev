<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

$out = array();

// CART ID
$cart_id = $_uccms_ecomm->cartID();

// HAVE CART ID
if ($cart_id) {

    // CART ITEMS
    $citems = array();

    // TAX BILLING ADDRESS
    if ($_uccms_ecomm->getSetting('tax_billing')) {

        // BILLING VARS
        if (($_REQUEST['billing_state']) || ($_REQUEST['billing_zip'])) {
            $vars['country']    = 'United States';
            $vars['state']      = $_REQUEST['billing_state'];
            $vars['zip']        = $_REQUEST['billing_zip'];
        }

    // TAX SHIPPING
    } else {

        // SHIPPING SAME AS BILLING
        if (($_REQUEST['shipping_same']) && ($_REQUEST['shipping_same'] != 'false')) {

            // BILLING VARS
            if (($_REQUEST['billing_state']) || ($_REQUEST['billing_zip'])) {
                $vars['country']    = 'United States';
                $vars['state']      = $_REQUEST['billing_state'];
                $vars['zip']        = $_REQUEST['billing_zip'];
            }

        // SHIPPING DIFFERENT
        } else {

            // SHIPPING VARS
            if (($_REQUEST['shipping_state']) || ($_REQUEST['shipping_zip'])) {
                $vars['country']    = 'United States';
                $vars['state']      = $_REQUEST['shipping_state'];
                $vars['zip']        = $_REQUEST['shipping_zip'];
            }

        }

    }

    if ($_REQUEST['quote']) $vars['quote'] = true;

    $out['input'] = $vars;

    // GET TAX
    $out['tax'] = $_uccms_ecomm->cartTax($cart_id, $citems, $vars);

// NO CART ID
} else {
    $out['err'] = 'Cart ID not specified.';
}

// FORMAT TOTAL
$out['tax']['total'] = number_format($out['tax']['total'], 2);

echo json_encode($out);

?>