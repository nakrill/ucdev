<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

// GET SETTING(S)
$ecomm['settings'] = $_uccms_ecomm->getSettings();

// INCLUDE FILE
$include_file = dirname(__FILE__). '/my-orders/default.php';

// HAVE COMMANDS
if (count($bigtree['commands']) > 1) {

    $base = array_shift($bigtree['commands']);

    // GET BIGTREE ROUTING
    list($include, $commands) = BigTree::route(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/account/' .$base. '/', $bigtree['commands']);

    // FILE TO INCLUDE
    if ($include) {
        $include_file = $include;
    }

}

// INCLUDE ROUTED FILE
include($include_file);

?>