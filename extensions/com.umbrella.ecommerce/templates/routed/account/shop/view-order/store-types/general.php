<style type="text/css">

    #myAccount #order .section {
        padding-bottom: 40px;
    }
    #myAccount #order .section h4 {
        margin: 0 0 15px 0;
    }

    #myAccount #order .section .col-1-2 {
        min-width: 350px;
    }

    #myAccount #order .section.details {
        padding-bottom: 10px;
    }
    #myAccount #order .section.billing_shipping {
        padding-bottom: 15px;
    }

    #myAccount #order table th {
        padding: 5px 0;
        text-align: left;
    }
    #myAccount #order table td {
        padding: 0 8px 8px 0;
    }

    #myAccount #order .details .status {
        margin-left: 0px;
        padding-left: 0px;
    }
    #myAccount #order .details .status .order_status {
        padding-top: 6px;
        font-size: 1.4em;
        opacity: .7;
    }

    #myAccount #order .fields.billing .phone, #myAccount #order .fields.billing .email, #myAccount #order .fields.shipping .phone, #myAccount #order .fields.shipping .email {
        padding-top: 6px;
    }

    #myAccount #cart {
        width: 100%;
        margin-bottom: 5px;
        border: 0px none;
    }

    #myAccount #cart th {
        padding: 0px;
        border-bottom: 1px solid #eee;
        background-color: transparent;
    }

    #myAccount #cart .thumb {
        width: 10%;
        text-align: center;
    }
    #myAccount #cart .title {
        width: 50%;
        text-align: left;
    }
    #myAccount #cart .price {
        width: 15%;
        text-align: left;
    }
    #myAccount #cart .quantity {
        width: 15%;
        text-align: left;
    }
    #myAccount #cart .total {
        width: 10%;
        text-align: right;
    }

    #myAccount #cart .item td {
        padding: 8px;
    }

    #myAccount #cart th.title {
        padding-left: 8px;
    }
    #myAccount #cart th.total {
        padding-right: 8px;
    }
    #myAccount #cart td.quantity {
        padding-left: 0px;
        padding-right: 0px;
    }
    #myAccount #cart td.price {
        padding-left: 0px;
        padding-right: 0px;
    }

    #myAccount #cart .item > td {
        border-bottom: 1px solid #eee;
    }

    #myAccount #cart .item .thumb {
        vertical-align: top;
    }
    #myAccount #cart .item .thumb img {
        height: 50px;
    }

    #myAccount #cart .item .options {
        list-style: none;
        margin: 0px;
        padding: 2px 0 0 10px;
    }
    #myAccount #cart .item .options li {
        font-size: .8em;
        line-height: 1.2em;
    }

    #myAccount #cart .item .actions {
        display: none;
        position: absolute;
        top: 4px;
        right: 8px;
        font-size: 1.4em;
    }

    #myAccount #cart .sub td {
        padding: 10px 0 0;
        background-color: #fff;
        text-align: right;
    }
    #myAccount #cart .sub td.title {
        padding-right: 0px;
    }

    #myAccount .transactions .transaction_dt {
        width: 150px;
        text-align: left;
    }
    #myAccount .transactions .transaction_status {
        width: 100px;
        text-align: left;
    }
    #myAccount .transactions .transaction_amount {
        width: 100px;
        text-align: left;
    }
    #myAccount .transactions .transaction_id {
        width: 200px;
        text-align: left;
    }
    #myAccount .transactions .transaction_message {
        width: 290px;
        text-align: left;
    }
    #myAccount .transactions .transaction_ip {
        width: 100px;
        text-align: left;
    }

</style>

<h3>Order Details</h3>

<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ID SPECIFIED
if ($id) {

    // GET ORDER
    if (BIGTREE_ADMIN_ROUTED) {
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ")";
    } else {
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ") AND (`customer_id`=" .$_uccms['_account']->userID(). ") AND (`status` NOT IN ('cart','deleted'))";
    }
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

    // ORDER FOUND
    if ($order['id']) {

        // IS PREPARING QUOTE
        if (($order['quote'] == 1) && ($order['status'] == 'preparing')) {
            ?>
            Quote / Order is being prepared - please check back later.
            <?php

        // ALL GOOD
        } else {

            // GET ORDER ITEMS
            $citems = $_uccms_ecomm->cartItems($order['id']);

            // GET BALANCE
            $balance = $_uccms_ecomm->orderBalance($order['id']);

            // CALCULATE PAID
            $paid = number_format($order['order_total'] - $balance, 2);

            ?>

            <div id="order">

                <div class="section details grid">

                    <div class="col-1-2">

                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td><strong>Order ID:</strong>&nbsp;</td>
                                <td><strong><?php echo $order['id']; ?></strong></td>
                            </tr>
                            <tr>
                                <td>Date:&nbsp;</td>
                                <td><?php echo date('n/j/Y', strtotime($order['dt'])); ?></td>
                            </tr>
                            <tr>
                                <td>Time:&nbsp;</td>
                                <td><?php echo date('g:i A', strtotime($order['dt'])); ?> EDT</td>
                            </tr>
                        </table>

                    </div>

                    <div class="col-1-2">

                        <div class="contain">

                            <fieldset class="status">
                                <label>Status</label>
                                <div class="order_status"><?php echo strtoupper($order['status']); ?></div>
                            </fieldset>

                        </div>

                    </div>

                </div>

                <div class="section billing_shipping grid">

                    <div class="col-1-2">
                        <h4>Billing</h4>
                        <div class="fields billing">
                            <div class="name">
                                <?php echo trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname'])); ?>
                            </div>
                            <?php if ($order['billing_company']) { ?>
                                <div class="company"><?php echo stripslashes($order['billing_company']);  ?></div>
                            <?php } ?>
                            <?php if ($order['billing_address1']) { ?>
                                <div class="address1"><?php echo stripslashes($order['billing_address1']);  ?></div>
                            <?php } ?>
                            <?php if ($order['billing_address2']) { ?>
                                <div class="address2"><?php echo stripslashes($order['billing_address2']);  ?></div>
                            <?php } ?>
                            <?php
                            $billing_csz = array();
                            if ($order['billing_city']) $billing_csz[] = stripslashes($order['billing_city']);
                            if ($order['billing_state']) $billing_csz[] = stripslashes($order['billing_state']);
                            if ($order['billing_zip']) $billing_csz[] = stripslashes($order['billing_zip']);
                            if (count($billing_csz) > 0) {
                                ?>
                                <div class="csz"><?php echo implode(' ', $billing_csz); ?></div>
                                <?php
                            }
                            ?>
                            <?php if ($order['billing_phone']) { ?>
                                <div class="phone"><?php echo stripslashes($order['billing_phone']);  ?></div>
                            <?php } ?>
                            <?php if ($order['billing_email']) { ?>
                                <div class="email"><?php echo stripslashes($order['billing_email']);  ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-1-2">
                        <h4>Shipping</h4>
                        <div class="fields shipping">
                            <div class="name">
                                <?php echo trim(stripslashes($order['shipping_firstname']. ' ' .$order['shipping_lastname'])); ?>
                            </div>
                            <?php if ($order['shipping_company']) { ?>
                                <div class="company"><?php echo stripslashes($order['shipping_company']);  ?></div>
                            <?php } ?>
                            <?php if ($order['shipping_address1']) { ?>
                                <div class="address1"><?php echo stripslashes($order['shipping_address1']);  ?></div>
                            <?php } ?>
                            <?php if ($order['shipping_address2']) { ?>
                                <div class="address2"><?php echo stripslashes($order['shipping_address2']);  ?></div>
                            <?php } ?>
                            <?php
                            $shipping_csz = array();
                            if ($order['shipping_city']) $shipping_csz[] = stripslashes($order['shipping_city']);
                            if ($order['shipping_state']) $shipping_csz[] = stripslashes($order['shipping_state']);
                            if ($order['shipping_zip']) $shipping_csz[] = stripslashes($order['shipping_zip']);
                            if (count($shipping_csz) > 0) {
                                ?>
                                <div class="csz"><?php echo implode(' ', $shipping_csz); ?></div>
                                <?php
                            }
                            ?>
                            <?php if ($order['shipping_phone']) { ?>
                                <div class="phone"><?php echo stripslashes($order['shipping_phone']);  ?></div>
                            <?php } ?>
                            <?php if ($order['shipping_email']) { ?>
                                <div class="email"><?php echo stripslashes($order['shipping_email']);  ?></div>
                            <?php } ?>
                        </div>
                    </div>

                </div>

                <div class="section cart">

                    <h4>Cart</h4>

                    <table id="cart" cellpadding="0" cellspacing="0">

                        <tr>
                            <th class="thumb"></th>
                            <th class="title">Item</th>
                            <th class="price">Price</th>
                            <th class="quantity">Quantity</th>
                            <th class="total">Total</th>
                        </tr>

                        <?php

                        // SUBTOTAL
                        $order['subtotal'] = 0;

                        // ATTRIBUTE ARRAY
                        $attra = array();

                        // AFTER TAX ITEM ARRAY
                        $atia = array();

                        // LOOP THROUGH ITEMS
                        foreach ($citems as $oitem) {

                            // AFTER TAX ITEM
                            if ($oitem['after_tax']) {

                                // ADD TO AFTER TAX ITEM ARRAY
                                $atia[] = $oitem;

                            // NORMAL ITEM
                            } else {

                                // GET ITEM INFO
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                // GROUP HEADINGS
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/store_types/' .$_uccms_ecomm->storeType(). '/cart-group-heading.php');

                                // SETTINGS FOR DISPLAYING ITEM IN CART
                                $item_cart_settings = array();

                                // DISPLAY ITEM IN CART
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                                unset($item, $oitem);

                            }

                        }

                        ?>

                        <tr class="sub subtotal">
                            <td colspan="5">
                                Subtotal: $<span class="num"><?php echo number_format($order['order_subtotal'], 2); ?></span>
                            </td>
                        </tr>

                        <tr class="sub tax">
                            <td colspan="5">
                                Tax: $<span class="num"><?php echo number_format($order['order_tax'], 2); ?></span>
                            </td>
                        </tr>

                        <tr class="sub shipping <?php if (count($atia) > 0) { echo 'with_after_tax'; } ?>">
                            <td colspan="5">
                                Shipping: $<span class="num"><?php echo number_format($order['order_shipping'], 2); ?></span>
                            </td>
                        </tr>

                        <?php if ($_uccms_ecomm->getSetting('gratuity_enabled')) { ?>
                            <tr class="sub gratuity">
                                <td colspan="5">
                                    (<?php echo ($order['override_gratuity'] ? $order['override_gratuity'] : $_uccms_ecomm->getSetting('gratuity_percent')); ?>%) Gratuity: $<span class="num"><?php echo $order['order_gratuity']; ?></span>
                                </td>
                            </tr>
                        <?php } ?>

                        <?php

                        // HAVE AFTER TAX ITEMS
                        if (count($atia) > 0) {

                            ?>
                            <tr class="sub">
                                <td colspan="5"></td>
                            </tr>
                            <?php

                            // LOOP
                            foreach ($atia as $oitem) {

                                // GET ITEM INFO
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                // GROUP HEADINGS
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/store_types/' .$_uccms_ecomm->storeType(). '/cart-group-heading.php');

                                // SETTINGS FOR DISPLAYING ITEM IN CART
                                $item_cart_settings = array(
                                    'after_tax' => true
                                );

                                // DISPLAY ITEM IN CART
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                                unset($item, $oitem);

                            }

                        }

                        ?>

                        <tr class="sub total">
                            <td colspan="5">
                                Total: $<span class="num"><?php echo number_format($order['order_total'], 2); ?></span>
                            </td>
                        </tr>

                    </table>

                    <div style="padding-top: 10px; text-align: right;">
                        <div style="padding-bottom: 10px; font-size: 1.2em;">
                            Paid: $<?=$paid?>
                        </div>
                        <div style="font-size: 1.4em;">
                            Balance: $<?=$balance?>
                        </div>
                    </div>

                </div>

                <?php if ($order['notes']) { ?>
                    <div class="section notes">
                        <h4>Notes</h4>
                        <div><?php echo nl2br(stripslashes($order['notes'])); ?></div>
                    </div>
                <?php } ?>

                <?php

                // GET TRANSACTION LOG
                $translog_query = "SELECT * FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`cart_id`='" .stripslashes($order['id']). "') ORDER BY `dt` DESC";
                $translog_q = sqlquery($translog_query);

                // HAVE TRANSACTIONS
                if (sqlrows($translog_q) > 0) {

                    ?>

                    <div class="section transactions">

                        <h4>Transactions</h4>

                        <table width="100%">

                            <tr>
                                <th>Time</th>
                                <th>Status</th>
                                <th>Amount</th>
                                <th>ID</th>
                                <th>Message</th>
                            </tr>

                            <?php

                            // LOOP
                            while ($transaction = sqlfetch($translog_q)) {

                                ?>

                                <tr>
                                    <td class="dt"><?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?></td>
                                    <td class="status"><?php echo ucwords($transaction['status']); ?></td>
                                    <td class="amount">$<?php echo number_format($transaction['amount'], 2); ?></td>
                                    <td class="id"><?php echo stripslashes($transaction['transaction_id']); ?></td>
                                    <td class="message"><?php echo stripslashes($transaction['message']); ?></td>
                                </tr>

                                <?php

                            }

                            ?>

                        </table>

                    </div>

                    <?php

                }

                ?>

            </div>

            <?php

        }

    // ORDER NOT FOUND
    } else {
        ?>
        Order not found.
        <?php
    }

// NO ORDER ID
} else {
    ?>
    Order ID not specified.
    <?php
}

?>