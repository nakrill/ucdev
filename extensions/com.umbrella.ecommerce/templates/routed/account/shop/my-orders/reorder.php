<?php

// CLEAN UP
$id = (int)$_REQUEST['id'];

// HAVE ID
if ($id) {

    // GET ITEM
    $item_query = "
    SELECT oi.*
    FROM `" .$_uccms_ecomm->tables['order_items']. "` AS `oi`
    INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON oi.cart_id=o.id
    INNER JOIN `" .$_uccms_ecomm->tables['items']. "` AS `i` ON oi.item_id=i.id
    WHERE (oi.id=" .$id. ") AND (o.customer_id=" .$_uccms['_account']->userID(). ") AND (i.active=1)
    ";
    $item_q = sqlquery($item_query);
    $item = sqlfetch($item_q);

    // ITEM FOUND
    if ($item['id']) {

        // REMOVE VALUES
        unset($item['id']);
        unset($item['dt']);

        // GET / INIT CART
        $cart = $_uccms_ecomm->initCart();

        // SET NEW VALUES
        $item['cart_id'] = $cart['id'];

        // INSERT INTO CART
        $insert_query = "INSERT INTO `" .$_uccms_ecomm->tables['order_items']. "` SET " .uccms_createSet($item). ", `dt`=NOW()";
        if (sqlquery($insert_query)) {
            $_uccms['_site-message']->set('success', 'Item added to cart.');
        } else {
            $_uccms['_site-message']->set('error', 'Failed adding item to cart.');
        }

    // ITEM NOT FOUND
    } else {
        $_uccms['_site-message']->set('error', 'Item not found.');
    }

// NO ID
} else {
    $_uccms['_site-message']->set('error', 'Item ID not specified.');
}

BigTree::redirect('../../');

?>