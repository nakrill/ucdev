<div class="orders_v2">

    <?php

    // LOOP
    foreach ($stoa as $order) {

        // GET EXTRA INFO
        $extra = $_uccms_ecomm->cartExtra($order['id']);

        // IS QUOTE
        if ($order['quote'] == 1) {
            if ($order['status'] == 'preparing') {
                $status_url = '';
            } else {
                $status_url = WWW_ROOT . $_uccms_ecomm->storePath(). '/quote/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']);
            }

        // IS ORDER
        } else {
            $status_url = WWW_ROOT . $_uccms_ecomm->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']);
        }

        ?>

        <div class="order">
            <div class="heading">
                <table>
                    <tr>
                        <td width="20%">
                            <div class="head">Request Placed</div>
                            <div class="info"><?php echo date('n/j/Y g:i A', strtotime($order['dt'])); ?></div>
                        </td>
                        <td width="20%">
                            <div class="head">Total</div>
                            <div class="info">$<?php echo number_format($order['order_total'], 2); ?></div>
                        </td>
                        <td width="20%">
                            <div class="head">Event</div>
                            <div class="info"><?php if ($extra['event_location1']) { echo stripslashes($extra['event_location1']); } else if ($extra['event_date'] != '0000-00-00') { echo date('n/j/Y', strtotime($extra['event_date'])); } ?></div>
                        </td>
                        <td width="40%" style="text-align: right;">
                            <div class="head">Order # <?php echo $order['id']; ?></div>
                            <div class="info larger"><a href="<?php echo $status_url; ?>" target="_blank">Order Details</a></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content">
                <table>
                    <tr>
                        <td class="items">

                            <h3 class="status"><?php if ($order['quote'] == 1) { echo 'Quote - '; } echo ucwords($order['status']); ?></h3>

                            <table>

                                <?php

                                // GET CART ITEMS
                                $citems = $_uccms_ecomm->cartItems($order['id']);

                                // LOOP THROUGH ITEMS
                                foreach ($citems as $oitem) {

                                    // GET ITEM INFO
                                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                    ?>

                                    <tr id="item-<?php echo $oitem['id']; ?>" class="item <?php if ($item['quote']) { echo 'quote'; } ?>">
                                        <td class="thumb">
                                            <img src="<?php echo $item['other']['image_url']; ?>" alt="<?php echo stripslashes($item['title']); ?>" />
                                        </td>
                                        <td class="info">
                                            <a href="<?php echo $item['other']['link']; ?>" class="title"><?php echo stripslashes($item['title']); ?></a>
                                            <?php if (count($item['other']['options']) > 0) { ?>
                                                <ul class="options">
                                                    <?php
                                                    foreach ($item['other']['options'] as $option_id => $values) {
                                                        if ($values[0]) {
                                                            ?>
                                                            <li>
                                                                <?php echo stripslashes($attra[$option_id]['title']);  ?>: <?php echo implode(', ', $values); ?>
                                                            </li>
                                                            <?php
                                                        }
                                                    } ?>
                                                </ul>
                                            <?php } ?>
                                            <div class="price">
                                                <?php echo $item['other']['price_formatted']; ?>
                                            </div>
                                            <div class="reorder">
                                                <a href="./my-orders/reorder/?id=<?php echo $oitem['id']; ?>" class="button">Buy Again</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <?php

                                }

                                ?>

                            </table>

                        </td>
                        <td class="buttons">
                            <?php if ($order['quote'] > 0) { ?>
                                <?php if ($order['status'] != 'preparing') { ?>
                                    <a href="<?php echo $status_url; ?>" target="_blank" class="button">View Quote</a>
                                <?php } ?>
                            <?php } ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <?php

    }

    ?>

</div>