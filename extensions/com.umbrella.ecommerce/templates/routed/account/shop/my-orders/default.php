<?php

$no_orders = true;
$latest_order = false;

// GET ORDERS
$order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .$_uccms['_account']->userID(). ") AND (`status` NOT IN ('cart','deleted')) ORDER BY `dt` DESC";
$order_q = sqlquery($order_query);

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/account/master/my-orders.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/account/custom/my-orders.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/account/master/my-orders.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/account/custom/my-orders.js"></script>

<h3>My Orders</h3>

<?php

// HAVE ORDERS
if (sqlrows($order_q) > 0 ) {

    $stoa = array(); // STORE TYPE ORDER ARRAY
    $oa = array(); // ORDER ARRAY

    $i = 1;

    // LOOP
    while ($order = sqlfetch($order_q)) {
        if ($i == 1) {
            $latest_order = $order;
        }
        switch ($_uccms_ecomm->storeType()) {
            case 'catering':
                if ($order['quote'] > 0) {
                    $stoa[] = $order;
                } else {
                    $oa[] = $order;
                }
                break;
            default:
                $oa[] = $order;
                break;
        }
        $i++;
    }

    // HAVE A LATEST ORDER
    if (is_array($latest_order)) {

        ?>

        <div class="section billing_shipping grid">

            <div class="col-1-2">
                <h4>Billing</h4>
                <div class="fields billing">
                    <div class="name">
                        <?php echo trim(stripslashes($latest_order['billing_firstname']. ' ' .$latest_order['billing_lastname'])); ?>
                    </div>
                    <?php if ($latest_order['billing_company']) { ?>
                        <div class="company"><?php echo stripslashes($latest_order['billing_company']);  ?></div>
                    <?php } ?>
                    <?php if ($latest_order['billing_address1']) { ?>
                        <div class="address1"><?php echo stripslashes($latest_order['billing_address1']);  ?></div>
                    <?php } ?>
                    <?php if ($latest_order['billing_address2']) { ?>
                        <div class="address2"><?php echo stripslashes($latest_order['billing_address2']);  ?></div>
                    <?php } ?>
                    <?php
                    $billing_csz = array();
                    if ($latest_order['billing_city']) $billing_csz[] = stripslashes($latest_order['billing_city']);
                    if ($latest_order['billing_state']) $billing_csz[] = stripslashes($latest_order['billing_state']);
                    if ($latest_order['billing_zip']) $billing_csz[] = stripslashes($latest_order['billing_zip']);
                    if (count($billing_csz) > 0) {
                        ?>
                        <div class="csz"><?php echo implode(' ', $billing_csz); ?></div>
                        <?php
                    }
                    ?>
                    <?php if ($latest_order['billing_phone']) { ?>
                        <div class="phone"><?php echo stripslashes($latest_order['billing_phone']);  ?></div>
                    <?php } ?>
                    <?php if ($latest_order['billing_email']) { ?>
                        <div class="email"><?php echo stripslashes($latest_order['billing_email']);  ?></div>
                    <?php } ?>
                </div>
            </div>

            <div class="col-1-2">
                <h4>Shipping</h4>
                <div class="fields shipping">
                    <div class="name">
                        <?php echo trim(stripslashes($latest_order['shipping_firstname']. ' ' .$latest_order['shipping_lastname'])); ?>
                    </div>
                    <?php if ($latest_order['shipping_company']) { ?>
                        <div class="company"><?php echo stripslashes($latest_order['shipping_company']);  ?></div>
                    <?php } ?>
                    <?php if ($latest_order['shipping_address1']) { ?>
                        <div class="address1"><?php echo stripslashes($latest_order['shipping_address1']);  ?></div>
                    <?php } ?>
                    <?php if ($latest_order['shipping_address2']) { ?>
                        <div class="address2"><?php echo stripslashes($latest_order['shipping_address2']);  ?></div>
                    <?php } ?>
                    <?php
                    $shipping_csz = array();
                    if ($latest_order['shipping_city']) $shipping_csz[] = stripslashes($latest_order['shipping_city']);
                    if ($latest_order['shipping_state']) $shipping_csz[] = stripslashes($latest_order['shipping_state']);
                    if ($latest_order['shipping_zip']) $shipping_csz[] = stripslashes($latest_order['shipping_zip']);
                    if (count($shipping_csz) > 0) {
                        ?>
                        <div class="csz"><?php echo implode(' ', $shipping_csz); ?></div>
                        <?php
                    }
                    ?>
                    <?php if ($latest_order['shipping_phone']) { ?>
                        <div class="phone"><?php echo stripslashes($latest_order['shipping_phone']);  ?></div>
                    <?php } ?>
                    <?php if ($latest_order['shipping_email']) { ?>
                        <div class="email"><?php echo stripslashes($latest_order['shipping_email']);  ?></div>
                    <?php } ?>
                </div>
            </div>

        </div>

        <?php

    }

    // HAVE STORE TYPE ORDERS
    if (count($stoa) > 0) {
        include(dirname(__FILE__). '/store-types/' .$_uccms_ecomm->storeType(). '.php');
    }

    // HAVE "NORMAL" ORDERS
    if (count($oa) > 0) {
        include(dirname(__FILE__). '/store-types/general.php');
    }

// NO ORDERS
} else {
    ?>
    It doesn't look like you've placed any orders yet.
    <?php
}

?>