<?php

if ($_uccms_ecomm->cartID()) {

    ?>

    <div class="cart_widget">
        <h3>Cart</h3>

        <?php

        // GET CART ITEMS
        $cartitema = $_uccms_ecomm->cartItems($_uccms_ecomm->cartID());

        // HAVE ITEMS IN CART
        if (count($cartitema) > 0) {

            ?>

            <a href="<?php echo WWW_ROOT . $bigtree['path'][0]; ?>/cart/"><?php echo count($cartitema); ?> items</a>

            <?php /*

            <ul>

                <?php

                // LOOP
                foreach ($cartitema as $cartitem) {

                    ?>
                    <li><a href="<?php echo $_uccms_ecomm->itemURL($cartitem['item_id'], $cartitem['category_id'], $cartitem); ?>"><?php echo stripslashes($cartitem['title']); ?></a></li>
                    <?php

                }

                ?>

            </ul>

            <?php
            */

        // NO ITEMS IN CART
        } else {
            ?>
            No items
            <?php
        }

        ?>

    </div>

    <?php

}

?>