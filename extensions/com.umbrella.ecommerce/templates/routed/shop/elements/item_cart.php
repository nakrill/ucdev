<?php

if ($item['id']) {

    $classa = array('item');
    if ($item['quote']) $classa[] = 'quote';
    if ($item['type'] == 3) $classa[] = 'package';
    if ($item_cart_settings['after_tax']) $classa[] = 'aftertax';
    $item_classes = implode(' ', $classa);

    $attr_options = array();

    // IN ADMIN
    if (defined('BIGTREE_ADMIN_ROUTED')) {
        $attr_options['file_download'] = true;
        $attr_options['order_id'] = $order['id'];
    }

    ?>

    <tr id="item-<?php echo $oitem['id']; ?>" class="<?php echo $item_classes; ?>">
        <td class="thumb">
            <img src="<?php echo $item['other']['image_url']; ?>" alt="<?php echo stripslashes($item['title']); ?>" />
        </td>
        <td class="title">
            <div class="item_title"><?php if ($item_cart_settings['edit_link']) { ?><a href="<?php echo $item_cart_settings['edit_link']; ?>"><?php } ?><?php echo stripslashes($item['title']); ?><?php if ($item_cart_settings['edit_link']) { ?></a><?php } ?></div>
            <?php if ($booking['id']) { ?>
                <div class="booking">
                    <?php if ($booking_duration['title']) { ?>
                        <span class="title"><?php echo stripslashes($booking_duration['title']); ?>:</span>
                    <?php } ?>
                    <span class="date">
                        <?php
                        $from_to = '';
                        if (($booking['dt_from']) && ($booking['dt_from'] != '0000-00-00 00:00:00')) {
                            list($booking_from_date, $booking_from_time) = explode(' ', $booking['dt_from']);
                            $from_to .= '
                            <span class="from">
                                <span class="date">' .date('n/j/Y', strtotime($booking_from_date)). '</span>
                                ';
                                if ($booking_from_time != '00:00:00') {
                                    $from_to .= '<span class="time">' .date('g:i A', strtotime($booking_from_time)). '</span>';
                                }
                                $from_to .='
                            </span>
                            ';
                            if (($booking['dt_to']) && ($booking['dt_to'] != '0000-00-00 00:00:00')) {
                                list($booking_to_date, $booking_to_time) = explode(' ', $booking['dt_to']);
                                $from_to .= ' to
                                <span class="to">';
                                    if ($booking_from_date != $booking_to_date) {
                                        $from_to .= '<span class="date">' .date('n/j/Y', strtotime($booking_to_date)). '</span>';
                                    }
                                    if ($booking_from_time != '00:00:00') {
                                        $from_to .= '
                                        <span class="time">' .date('g:i A', strtotime($booking_to_time) + 1). '</span>';
                                    }
                                    $from_to .='
                                </span>
                                ';
                            }
                        }
                        echo $from_to;
                        ?>
                    </span>
                    <?php if ($booking['markup'] != 0.00) { ?>
                        <span class="markup">(+ $<?php echo $_uccms_ecomm->toFloat($booking['markup']); ?>)</span>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ((count((array)$item['other']['options']) > 0) || (($item['type'] == 3) && (count((array)$item['package']['items']) > 0))) { ?>
                <ul class="options">
                    <?php

                    // IS PACKAGE AND HAVE ITEMS
                    if (($item['type'] == 3) && (count($item['package']['items']) > 0)) {

                        // LOOP THROUGH ITEMS
                        foreach ($item['package']['items'] as $pitem_id => $pitem) {

                            // GET SELECTED ATTRIBUTES / OPTIONS
                            $pitem_attrs = $_uccms_ecomm->item_attrAndOptionTitles($pitem, $pitem['other']['attribute_titles'], $attr_options);

                            ?>

                            <li><?php echo stripslashes($pitem['item']['title']); if ($pitem['quantity'] > 1) { echo ' (' .number_format($pitem['quantity'], 0). ')'; } ?>
                                <?php if (count($pitem_attrs) > 0) { ?>
                                    <ul>
                                        <?php foreach ($pitem_attrs as $attr) { ?>
                                            <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>

                            <?php

                        }

                    // OTHER TYPES
                    } else {
                        if (count((array)$item['other']['options']) > 0) {
                            foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra, $attr_options) as $attr) {
                                ?>
                                <li><?php echo $attr['title']; ?>: <?php echo $attr['options']; ?></li>
                                <?php
                            }
                        }
                    }

                    ?>
                </ul>
            <?php } ?>
            <?php
            if ($booking['id']) {

                $persons = array();

                // GET PERSONS INFO FOR THIS CART ITEM
                $persons_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` WHERE (`oitem_id`=" .$oitem['id']. ") ORDER BY `sort` ASC, `id` ASC";
                $persons_q = sqlquery($persons_query);

                // HAVE PERSONS
                if (sqlrows($persons_q) > 0) {
                    $pi = 1;

                    ?>
                    <div class="persons">
                        <?php
                        while ($person = sqlfetch($persons_q)) {
                            ?>
                            <div class="person clearfix">
                                <div class="label">
                                    Person <?php echo $pi; ?>:
                                </div>
                                <div class="input firstname">
                                    <?php echo stripslashes($person['firstname']); ?>
                                </div>
                                <div class="input lastname">
                                    <?php echo stripslashes($person['lastname']); ?>
                                </div>
                                <div class="input dob">
                                    <?php if (($person['dob']) && ($person['dob'] != '0000-00-00')) { echo date('n/j/Y', strtotime($person['dob'])); } ?>
                                </div>
                                <div class="input email">
                                    <?php echo stripslashes($person['email']); ?>
                                </div>
                                <div class="input notes">
                                    <?php echo stripslashes($person['notes']); ?>
                                </div>
                            </div>
                            <?php
                            $pi++;
                        }
                        ?>
                    </div>
                    <?php

                }

            }
            ?>
            <?php if ($oitem['backorder_num']) { ?>
                <div class="backordered">
                    Backordered: <?php echo number_format($oitem['backorder_num']); ?>
                </div>
            <?php } ?>
        </td>
        <td class="price">
            <?php if ($item_cart_settings['hide_price']) { ?>
                -
            <?php } else { ?>
                <?php echo $item['other']['price_formatted']; ?>
            <?php } ?>
        </td>
        <td class="quantity">
            <?php if ($item_cart_settings['change_quantity']) { ?>
                <input type="text" value="<?php echo number_format($oitem['quantity'], 0); ?>" />
                <br />
                <a href="<?php echo $item_cart_settings['update_link']; ?>" class="update">Update</a> | <a href="<?php echo $item_cart_settings['remove_link']; ?>" class="remove">Remove</a>
            <?php } else { ?>
                <?php echo number_format($oitem['quantity'], 0); ?>
            <?php } ?>
        </td>
        <td class="total" style="position: relative;">
            <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
                <div class="actions">
                    <a href="#" class="edit" title="Edit" data-id="<?=$oitem['id']?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href="#" class="delete" title="Delete" data-id="<?=$oitem['id']?>"><i class="fa fa-times"></i></a>
                </div>
            <?php } ?>
            <?php if ($item_cart_settings['hide_price']) { ?>
                -
            <?php } else { ?>
                <?php echo $item['other']['total_formatted']; ?>
            <?php } ?>
        </td>
    </tr>

    <?php

}

?>