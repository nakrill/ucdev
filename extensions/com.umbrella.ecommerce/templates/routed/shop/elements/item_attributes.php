<?php

$variants = [];
// INVENTORY CONTROL ENABLED
if ($_uccms_ecomm->getSetting('inventory_track')) {
    $variants = $_uccms_ecomm->item_getVariants($item['id']);
}

// PACKAGE
if ($item['type'] == 3) {

    // LOOP THROUGH PACKAGE ITEMS
    foreach ($_uccms_ecomm->package_getItems($item['id']) as $pitem) {

        unset($pitem_attrs);

        // NO VARIANT SPECIFIED (ALLOWING CUSTOMER TO CHOOSE)
        if (!$pitem['variant_id']) {

            // ATTRIBUTES ARRAY
            $attra = $_uccms_ecomm->itemAttributes($pitem['item_id']);

            // ATTRIBUTE USE COUNT ARRAY
            $auca = array();

            // LOOP
            foreach ($attra as $key => $attribute) {

                unset($field_values);

                // HAVE ALREADY SELECTED OPTIONS
                if (is_array($selattra)) {

                    // CHECKBOX, WIDTH/HEIGHT
                    if (($attribute['type'] == 'checkbox') || ($attribute['type'] == 'width_height')) {
                        $field_values = $selattra[$pitem['id']][$attribute['id']]; // ARRAY OF SELECTED OPTIONS

                    // ALL OTHER FIELDS
                    } else {
                        if (!$auca[$attribute['id']]) $auca[$attribute['id']] = 0;
                        $field_values = $selattra[$pitem['id']][$attribute['id']][$auca[$attribute['id']]]; // ONLY FIRST VALUE OF ARRAY EACH TIME
                    }

                // USE DEFAULTS
                } else {

                    // ATTRIBUTE HAS OPTIONS
                    if ($attribute['options']) {
                        $options = json_decode($attribute['options'], true);
                        foreach ($options as $option) {
                            if ($option['default']) {
                                $field_values = stripslashes($option['title']); // DEFAULT
                            }
                        }
                    }

                }

                $field_name = 'attribute[' .$pitem['id']. '][' .$attribute['id']. '][]';

                // ATTRIBUTE ELEMENT
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/attributes/base.php');

                $auca[$attribute['id']]++;

            }

        }

        unset($pitem);

    }

// OTHER TYPES
} else {

    // ATTRIBUTE USE COUNT ARRAY
    $auca = array();

    // LOOP
    foreach ($attra as $key => $attribute) {

        unset($field_values);

        // HAVE ALREADY SELECTED OPTIONS
        if (is_array($selattra)) {

            // MULTIPLE VALUES
            if (($attribute['type'] == 'checkbox') || ($attribute['type'] == 'width_height') || ($attribute['type'] == 'people')) {
                $field_values = $selattra[$attribute['id']]; // ARRAY OF SELECTED OPTIONS

            // ALL OTHER FIELDS
            } else {
                if (!$auca[$attribute['id']]) $auca[$attribute['id']] = 0;
                $field_values = $selattra[$attribute['id']][$auca[$attribute['id']]]; // ONLY FIRST VALUE OF ARRAY EACH TIME
            }

        // USE DEFAULTS
        } else {

            // ATTRIBUTE HAS OPTIONS
            if ($attribute['options']) {
                $options = json_decode($attribute['options'], true);
                foreach ($options as $option) {
                    if ($option['default']) {
                        $field_values = stripslashes($option['title']); // DEFAULT
                    }
                }
            }

        }

        $field_name = 'attribute[' .$attribute['id']. '][]';

        // ATTRIBUTE ELEMENT
        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/attributes/base.php');

        $auca[$attribute['id']]++;

    }

}

?>