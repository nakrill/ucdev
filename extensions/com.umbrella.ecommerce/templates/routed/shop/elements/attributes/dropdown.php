<div class="options">

    <select name="<?php echo $field_name; ?>">
        <option value="0">Select</option>
        <?php

        // LOOP
        foreach ($options as $option_id => $option) {

            // DEFAULT
            if (count($fva) == 0) {
                if ($option['default']) {
                    $fva[] = $option_id;
                }
            }

            ?>
            <option value="<?php echo $option_id; ?>" <?php if (in_array($option_id, $fva)) { ?>selected="selected"<?php } ?>><?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?><?php if ($option['markup']) { ?> (+$<?php echo number_format(stripslashes($option['markup']), 2); ?>)<?php } ?></option>
            <?php
        }
        ?>
    </select>

</div>