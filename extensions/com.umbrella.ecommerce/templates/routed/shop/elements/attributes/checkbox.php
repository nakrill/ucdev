<div class="options">

    <?php

    // HAVE OPTIONS
    if (count($options) > 0) {

        // LOOP THROUGH OPTIONS
        foreach ($options as $option_id => $option) {

            // DEFAULT
            if (count($fva) == 0) {
                if ($option['default']) {
                    $fva[] = $option_id;
                }
            }

            ?>
            <div class="option"><input type="checkbox" name="<?php echo $field_name; ?>" value="<?php echo $option_id; ?>" <?php if (in_array($option_id, $fva)) { ?>checked="checked"<?php } ?> />&nbsp;<?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?><?php if ($option['markup']) { ?> (+$<?php echo number_format(stripslashes($option['markup']), 2); ?>)<?php } ?></div>
            <?php
        }

    }

    ?>

</div>