<div class="options">
    <?php

    // FIELD VALUES
    $field = array(
        'id'        => 'attr-tp-' .$key,
        'key'       => $field_name,
        'value'     => $fva[0],
        'required'  => $attribute['required']
    );

    // INCLUDE FIELD
    include(BigTree::path('admin/form-field-types/draw/time.php'));

    ?>
</div>