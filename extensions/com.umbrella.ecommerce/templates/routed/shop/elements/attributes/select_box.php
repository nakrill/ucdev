<div class="options select_box clearfix">

    <?php

    // HAVE OPTIONS
    if (count($options) > 0) {

        // LOOP THROUGH OPTIONS
        foreach ($options as $option_id => $option) {

            // DEFAULT
            if (count($fva) == 0) {
                if ($option['default']) {
                    $fva[] = $option_id;
                }
            }

            // AVAILABILITY
            $oavail = true;
            if ($_uccms_ecomm->getSetting('inventory_track')) {
                $oavail = $option['inventory']['available'];
            }

            ?>
            <div class="option<?php echo ($oavail ? ' available' : ' unavailable') . (in_array($option_id, $fva) ? ' selected' : ''); ?>" data-value="<?php echo $option_id; ?>">
                <div class="border2">
                    <?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?>
                    <?php if ($option['markup']) { ?>
                        <div class="markup">+$<?php echo number_format(stripslashes($option['markup']), 2); ?></div>
                    <?php } ?>
                </div>
            </div>
            <?php
        }

        ?>

        <input type="hidden" name="<?php echo $field_name; ?>" value="<?php echo stripslashes($fva[0]); ?>" />

        <?php

    }

    ?>

</div>