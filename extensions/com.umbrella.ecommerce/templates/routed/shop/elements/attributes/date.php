<div class="options">
    <?php

    // SET DEFAULT DATE FORMAT IF NONE SPECIFIED
    if (!$bigtree["config"]["date_format"]) {
        $bigtree["config"]["date_format"] = 'm/d/Y';
    }

    // FIELD VALUES
    $field = array(
        'id'        => 'attr-tp-' .$key,
        'key'       => $field_name,
        'value'     => $fva[0],
        'required'  => $attribute['required'],
        'options'   => array(
            'default_today' => true
        )
    );

    // INCLUDE FIELD
    include(BigTree::path('admin/form-field-types/draw/date.php'));

    ?>

</div>