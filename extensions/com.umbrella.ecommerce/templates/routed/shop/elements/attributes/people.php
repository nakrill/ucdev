<style type="text/css">
    #ecommContainer .item_container .qty {
        display: none !important;
    }
</style>

<div class="options">

    <?php

    // HAVE OPTIONS
    if (count($options) > 0) {

        $option_max = ($attribute['limit'] > 1 ? (int)$attribute['limit'] : 1);

        // LOOP THROUGH OPTIONS
        foreach ($options as $option_id => $option) {

            $option_field_name = str_replace('[]', '[' .$option_id. ']', $field_name);

            $option_min = (int)$option['default'];

            $option_value = (int)$option['default'];
            if (isset($fva[$option_id])) {
                $option_value = (int)$fva[$option_id];
            }

            ?>
            <div class="option clearfix">
                <div class="input-group">
                    <label class="control-label col-sm-2" for="<?php echo $option_field_name; ?>">
                        <?php echo htmlentities(stripslashes($option['title']), ENT_QUOTES); ?>
                    </label>
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default btn-number" <?php if ($option_value == $option_min) { ?>disabled="disabled"<?php } ?> data-type="minus" data-field="<?php echo $option_field_name; ?>">
                            <i class="fa fa-minus"></i>
                        </button>
                    </span>
                    <input type="text" name="<?php echo $option_field_name; ?>" class="form-control input-number" min="<?php echo $option_min; ?>" max="<?php echo $option_max; ?>" value="<?php echo $option_value; ?>" />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default btn-number" <?php if ($option_value >= $option_max) { ?>disabled="disabled"<?php } ?> data-type="plus" data-field="<?php echo $option_field_name; ?>">
                            <i class="fa fa-plus"></i>
                        </button>
                    </span>
                </div>
                <div class="markup">
                    <?php if ($option['markup']) { ?>
                        <span class="sym">$</span><span class="num"><?php echo number_format(stripslashes($option['markup']), 2); ?></span><?php if ($attribute['description']) { ?><span class="desc"><?php echo $attribute['description']; ?></span><?php } ?>
                    <?php } else { ?>
                        <span class="free">free</span>
                    <?php } ?>
                </div>
            </div>
            <?php
        }

    }

    ?>

</div>