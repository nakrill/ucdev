<?php

// HAVE ATTRIBUTE ID
if ($attribute['id']) {

    // GET OPTIONS
    $options = json_decode(stripslashes($attribute['options']), true);

    // TRACKING INVENTORY
    if ($_uccms_ecomm->getSetting('inventory_track')) {
        foreach ((array)$options as $option_id => $option) {
            $options[$option_id]['inventory']['available'] = $variants[$_uccms_ecomm->cart_locationID()][md5($attribute['id']. ':' .$option_id)]['inventory_available'];
        }
    }

    // FIELD VALUES ARRAY
    $fva = array();

    // ALREADY SELECTED
    if (is_array($field_values)) {
        $fva = $field_values;
    } else {
        $fva[] = $field_values;
    }

    ?>

    <fieldset id="af-<?php echo $key; ?>" class="attribute <?php echo $attribute['type']; ?>">

        <?php

        // HAVE TITLE
        if ($attribute['title']) {
            ?>
            <label class="title"><?php echo stripslashes($attribute['title']); ?><?php if ($attribute['required']) { ?><span class="reqd">*</span><?php } ?></label>
            <?php
        }

        // NOT PEOPLE
        if ($attribute['type'] != 'people') {

            // HAVE DESCRIPTION
            if ($attribute['description']) {
                ?>
                <label class="description"><?php echo stripslashes($attribute['description']); ?></label>
                <?php
            }

        }

        // HAVE LIMIT
        if ($attribute['limit'] > 0) {
            ?>
            <label class="limit">Limit: <?php echo number_format($attribute['limit'], 0); ?></label>
            <?php
        }

        ?>

        <div class="options">
            <?php include(dirname(__FILE__). '/' .stripslashes($attribute['type']). '.php'); ?>
        </div>

    </fieldset>

    <?php

}

?>