<div class="options">
    <div class="file_upload">
        <input type="file" name="attribute-<?php echo $attribute['id']; ?>" />
        <?php if ($attribute['file_types']) { ?>
            <div class="allowed limit">Allowed: <?php echo str_replace(',', ', ', stripslashes($attribute['file_types'])); ?></div>
        <?php } ?>
    </div>
    <?php if ((count($fva) > 0) && ($fva[0])) { ?>
        <div class="files">
            <?php

            // LOOP
            foreach ($fva as $file) {

                // EXTENSION
                $ext = strtolower(str_replace('.', '', strrchr($file, '.')));

                ?>
                <input type="hidden" name="<?php echo $field_name; ?>" value="<?php echo $file; ?>" />
                <input type="hidden" name="attribute-<?php echo $attribute['id']; ?>-existing" value="<?php echo $file; ?>" />
                <input type="hidden" name="attribute-<?php echo $attribute['id']; ?>-remove" value="" />
                <div class="file file-<?php echo $attribute['id']; ?>">
                    <div class="file_name">
                        <a href="#" class="remove" data-id="<?php echo $attribute['id']; ?>">x</a> <?php echo $file; ?>
                    </div>
                </div>
                <?php
            }

            ?>
        </div>
    <?php } ?>
</div>