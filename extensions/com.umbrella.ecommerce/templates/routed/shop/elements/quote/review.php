<?php

// IS IN ADMIN
if (defined('BIGTREE_ADMIN_ROUTED')) {
    $extended_view = true;
} else {
    $admin = new BigTreeAdmin;
    if (($admin->Level) && ($bigtree['path'][0] == 'admin')) { // IS ADMIN
        $extended_view = true;
    } else {
        if (($order['quote'] != 1) && ($order['status'] != 'cart')) {
            $extended_view = true;
        } else {
            $exstatusa = array('presented','accepted','declined');
            if (in_array($order['status'], $exstatusa)) {
                $extended_view = true;
            }
        }
    }
}

// EXTENDED VIEW
if ($extended_view) {

    // GET BALANCE
    $balance = $_uccms_ecomm->orderBalance($order['id']);

    // SCHEDULED PAYMENTS ARRAY
    $spa = array();

    // SCHEDULED PAYMENT AMOUNT
    $scheduled_amount = 0.00;

    // HAVE ORDER ID
    if ($order['id']) {

        $order['discount'] = $order['order_discount'];

        // GET EXTRA INFO
        $extra_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_' .$_uccms_ecomm->storeType()]. "` WHERE (`order_id`=" .$order['id']. ")";
        $extra_q = sqlquery($extra_query);
        $extra = sqlfetch($extra_q);

        // GET SCHEDULED PAYMENTS
        $spayment_query = "SELECT * FROM `" .$_uccms_ecomm->tables['scheduled_payments']. "` WHERE (`order_id`=" .$order['id']. ") ORDER BY `dt_due` ASC";
        $spayment_q = sqlquery($spayment_query);
        while ($spayment = sqlfetch($spayment_q)) {

            // ADD TO ARRAY
            $spa[$spayment['id']] = $spayment;

            // IS TODAY OR OLDER
            if ($spayment['dt_due'] <= date('Y-m-d')) {
                $scheduled_amount += $spayment['amount'];
            }

        }

        $payment_profiles = array();

        // PAYMENT PROFILES ENABLED
        if ($_uccms['_account']->paymentProfilesEnabled()) {

            $pp_aid = 0;

            // IS LOGGED IN
            if ($_uccms['_account']->userID()) {
                $pp_aid = $_uccms['_account']->userID();
            } else {
                $admin = new BigTreeAdmin;
                if (($admin->Level) && ($bigtree['path'][0] == 'admin')) { // IS ADMIN
                    $pp_aid = $order['customer_id'];
                }
            }

            // IS LOGGED IN
            if ($pp_aid) {

                // GET PAYMENT PROFILES
                $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$pp_aid. ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
                $pp_q = sqlquery($pp_query);
                while ($pp = sqlfetch($pp_q)) {
                    $payment_profiles[$pp['id']] = $pp;
                }

            }

        }

    }

    // IN ADMIN
    if (defined('BIGTREE_ADMIN_ROUTED')) {

        ?>

        <style type="text/css">
            #ecommContainer #review .heading {
                padding-top: 0px;
            }
            #ecommContainer #review .heading .half {
                margin: 0px;
            }
            #ecommContainer #review .heading .half.left .quarter {
                float: left;
                margin-right: 0px;
            }
            #ecommContainer #review .heading .half.right .select {
                float: right;
                text-align: left;
            }
            #ecommContainer #review .details .left {
                margin-bottom: 0px;
            }
            #ecommContainer #review .details table {
                border: 0px none;
            }
            #ecommContainer #review .details td {
                padding: 4px 0;
                color: inherit;
            }
            #ecommContainer #review .details table tr:nth-child(2n) td {
                background-color: #fff;
            }
            #ecommContainer .cart {
                padding-bottom: 0px;
            }
        </style>

        <?php

    }

}

?>

<div id="review">

    <?php if ($order['quote'] != 2) { ?>

        <div class="heading contain" position="relative">

            <div class="half left">
                <div class="contain">

                    <div class="quarter left business_logo">
                        <?php
                        if ($_uccms_ecomm->getSetting('quote_logo')) {
                            $logo = $_uccms_ecomm->bigtreeFileURL($_uccms_ecomm->getSetting('quote_logo'));
                        } else if ($cms->getSetting('logo')) {
                            $logo = $cms->getSetting('logo');
                        }
                        if ($logo) {
                            ?><img src="<?=$logo?>" alt="<?=$cms->getSetting('site_name')?>" />
                        <?php } ?>
                    </div>

                    <div class="quarter left business_info">
                        <div class="store_name"><?php echo stripslashes($_uccms_ecomm->getSetting('store_name')); ?></div>
                        <div class="store_address"><?php echo nl2br(stripslashes($_uccms_ecomm->getSetting('store_address'))); ?></div>
                        <div class="store_phone"><?php echo stripslashes($_uccms_ecomm->getSetting('store_phone')); ?></div>
                    </div>

                </div>
            </div>

            <div class="half right contain">

                <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>

                    <table style="float: right; width: auto; margin: 0px; padding: 0px; border: 0px none;">
                        <tr>
                            <td style="padding: 0px;">
                                <fieldset>
                                    <select name="status">
                                        <option value="requested" <?php if ($order['status'] == 'reqested') { ?>selected="selected"<?php } ?>>Requested</option>
                                        <option value="preparing" <?php if ($order['status'] == 'preparing') { ?>selected="selected"<?php } ?>>Preparing</option>
                                        <option value="presented" <?php if ($order['status'] == 'presented') { ?>selected="selected"<?php } ?>>Presented</option>
                                        <option value="declined" <?php if ($order['status'] == 'declined') { ?>selected="selected"<?php } ?>>Declined</option>
                                        <option value="accepted" <?php if ($order['status'] == 'accepted') { ?>selected="selected"<?php } ?>>Accepted</option>
                                        <option value="placed">Convert to Order</option>
                                        <?php if ($order['status'] == 'deleted') { ?>
                                            <option value="deleted" selected="selected">Deleted</option>
                                        <?php } ?>
                                        <?php if ($order['status'] == 'cloned') { ?>
                                            <option value="cloned" selected="selected">Cloned</option>
                                        <?php } ?>
                                    </select>
                                </fieldset>
                            </td>
                            <?php if (($send['to_email']) && (($order['status'] != 'cart') && ($order['status'] != 'deleted'))) { ?>
                                <td style="padding: 0 0 0 15px;">
                                    <a href="#send" class="button green send_quote" style="display: block; margin-top: -7px;">Send</a>
                                </td>
                            <?php } ?>
                        </tr>
                    </table>

                <?php } else { ?>

                    <?php if ($extended_view) { ?>

                        <?php if (!defined('BIGTREE_ADMIN_ROUTED')) { ?>
                            <?php if (($order['status'] == 'accepted') || ($order['status'] == 'declined') || ($order['status'] == 'placed') || ($order['status'] == 'charged')) { ?>
                                <div class="status">
                                    <?php
                                    if (($order['status'] == 'placed') || ($order['status'] == 'charged')) {
                                        if ($order['quote'] == 2) {
                                            echo 'accepted';
                                        } else {
                                            echo 'receipt';
                                        }
                                    } else {
                                        echo $order['status'];
                                    }
                                    ?>
                                </div>
                            <?php } else if ($order['paid_in_full']) { ?>
                                <div class="status">
                                    Paid
                                </div>
                            <?php } ?>
                        <?php } ?>

                        <?php if (($order['status'] == 'presented') || ($order['status'] == 'declined')) { ?>

                            <div class="buttons">
                                <a href="./status/?id=<?=$order['id']?>&h=<?=stripslashes($order['hash'])?>&status=accepted" class="button accept green">Accept Quote</a> <a href="#" class="button decline red">Decline Quote</a>
                            </div>

                            <div class="decline_details" style="display: none;">
                                <form enctype="multipart/form-data" action="./status/" method="post">
                                <input type="hidden" name="id" value="<?=$order['id']?>" />
                                <input type="hidden" name="h" value="<?=stripslashes($order['hash'])?>" />
                                <input type="hidden" name="status" value="declined" />
                                <fieldset>
                                    <label>Reason / Adjustment Request:</label>
                                    <textarea name="message"></textarea>
                                </fieldset>
                                <div class="buttons">
                                    <input type="submit" value="Submit" />
                                </div>
                                </form>
                            </div>

                        <?php } ?>

                        <script type="text/javascript">
                            $(function() {
                                $('#review .buttons .decline').click(function(e) {
                                    e.preventDefault();
                                    $('#review .decline_details').toggle();
                                });
                            });
                        </script>

                    <?php } else { ?>
                        PROPOSAL
                    <?php } ?>

                <?php } ?>

            </div>

        </div>

    <?php } ?>

    <?php

    // GROUP HEADINGS
    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/modules/ecommerce/quotes/edit/store_types/' .$_uccms_ecomm->storeType(). '/review_head-details.php');

    ?>

    <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
        <div class="contain" style="margin: -5px 0 5px;">
            <a href="#" class="button_small cart_addItem">Add Item</a>
        </div>
    <?php } ?>

    <div class="cart section_padding">

        <div class="contain box">

            <table id="cart" cellpadding="0" cellspacing="0">

                <tr>
                    <th class="thumb"></th>
                    <th class="title">Item</th>
                    <th class="price">Price</th>
                    <th class="quantity">Quantity</th>
                    <th class="total">Total</th>
                </tr>

                <?php

                // HAVE ITEMS
                if (count((array)$citems) > 0) {

                    // STORE IS CATERING
                    if ($_uccms_ecomm->storeType() == 'catering') {

                        // SORT ITEMS
                        $citems = $_uccms_ecomm->stc->sortOItemsByDayTime($citems);

                    }

                    // HAVE TAX OVERRIDE
                    if ($order['override_tax']) {
                        $tax['total'] = number_format($order['override_tax'], 2);

                    // NORMAL TAX CALCULATION
                    } else {

                        // GET TAX
                        $tax = $_uccms_ecomm->cartTax($order['id'], $citems, $taxvals);

                    }

                    // HAVE SHIPPING OVERRIDE
                    if ($order['override_shipping']) {
                        $shipping['markup_type']    = 'amount';
                        $shipping['markup']         = number_format($order['override_shipping'], 2);

                    // GET SHIPPING
                    } else {

                        // GET SHIPPING SERVICES
                        $shipping_services = $_uccms_ecomm->cartShippingServices($order['id']);

                        // GET SHIPPING AMOUNT
                        $shipping = $_uccms_ecomm->cartShipping($shipping_services, $order['shipping_service']);

                        if (!$shipping['id']) {
                            $shipping['markup'] = 0.00;
                        }

                    }

                    // ATTRIBUTE ARRAY
                    $attra = array();

                    // CURRENT DISPLAY GROUPS
                    $cdga = array();

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // IS PRODUCT & AFTER TAX ITEM
                        if (($item['type'] == 0) && ($oitem['after_tax'])) {
                            $atia[] = $oitem;
                            continue;

                        // IS SERVICE
                        } else if ($item['type'] == 1) {
                            $service_fees += str_replace(',', '', $item['other']['total']);

                        // IS RENTAL
                        } else if ($item['type'] == 2) {
                            $rental_fees += str_replace(',', '', $item['other']['total']);
                        }

                        // GROUP HEADINGS
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/store_types/' .$_uccms_ecomm->storeType(). '/cart-group-heading.php');

                        // SETTINGS FOR DISPLAYING ITEM IN CART
                        $item_cart_settings = array(
                            'hide_price' => !$extended_view
                        );

                        // DISPLAY ITEM IN CART
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                        unset($item, $oitem);

                    }

                    // ADD VALUES TO ORDER ARRAY
                    $order['tax']       = $tax['total'];

                    // ORDER SHIPPING
                    if ($shipping['markup_type'] == 'percent') {
                        $order['shipping'] = number_format($order['subtotal'] * ($shipping['markup'] / 100), 2, '.', '');
                    } else {
                        $order['shipping']  = $shipping['markup'];
                    }

                    ?>

                    <?php if ($extended_view) { ?>

                        <tr class="sub right subtotal">
                            <td colspan="4" class="title">
                                Subtotal:
                            </td>
                            <td class="value">
                                $<?php echo number_format($order['subtotal'], 2); ?>
                            </td>
                        </tr>

                        <?php if ($order['discount']) { ?>
                            <tr class="sub right subtotal">
                                <td colspan="4" class="title">
                                    Discount:
                                </td>
                                <td class="value">
                                    $<?php echo number_format($order['discount'], 2); ?>
                                </td>
                            </tr>
                        <?php } ?>

                        <tr class="sub right tax">
                            <td colspan="4" class="title">
                                Tax:
                            </td>
                            <td class="value">
                                <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
                                    $<input type="text" name="order[override_tax]" value="<?=$order['override_tax']?>" placeholder="<?php echo number_format($order['tax'], 2); ?>" />
                                <?php } else { ?>
                                    $<?php echo number_format($order['tax'], 2); ?>
                                <?php } ?>
                            </td>
                        </tr>

                        <?php

                        // HAVE AFTER TAX ITEMS
                        if (count((array)$atia) > 0) {

                            ?>
                            <tr>
                                <td colspan="4" style="padding: 5px 0;"></td>
                            </tr>
                            <?php

                            // RESET CURRENT DISPLAY GROUPS
                            $cdga = '';

                            // LOOP
                            foreach ($atia as $oitem) {

                                // GET ITEM INFO
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                                // IS SERVICE
                                if ($item['type'] == 1) {
                                    $service_fees += str_replace(',', '', $item['other']['total']);

                                // IS RENTAL
                                } else if ($item['type'] == 2) {
                                    $rental_fees += str_replace(',', '', $item['other']['total']);
                                }

                                // GROUP HEADINGS
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/store_types/' .$_uccms_ecomm->storeType(). '/cart-group-heading.php');

                                // SETTINGS FOR DISPLAYING ITEM IN CART
                                $item_cart_settings = array(
                                    'hide_price'    => !$extended_view,
                                    'after_tax'     => true
                                );

                                // DISPLAY ITEM IN CART
                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                                unset($item, $oitem);

                            }

                        }

                        ?>

                        <tr class="sub right shipping">
                            <td colspan="4" class="title">
                                Shipping:
                            </td>
                            <td class="value">
                                <?php
                                if (defined('BIGTREE_ADMIN_ROUTED')) {
                                    $shipping_amount = '';
                                    if (($order['override_shipping']) && ($order['override_shipping'] != 0.00)) {
                                        $shipping_amount = number_format($order['override_shipping'], 2);
                                    } else if (($order['shipping']) && ($order['shipping'] != 0.00)) {
                                        $shipping_amount = number_format($order['shipping'], 2);
                                    }
                                    ?>
                                    $<input type="text" name="order[override_shipping]" value="<?php echo $shipping_amount; ?>" placeholder="<?php echo number_format($order['shipping'], 2); ?>" />
                                <?php } else { ?>
                                    $<?php
                                    if ($order['override_shipping']) {
                                        echo number_format((float)$order['override_shipping'], 2);
                                    } else {
                                        echo number_format($order['shipping'], 2);
                                    }
                                }
                                ?>
                            </td>
                        </tr>

                        <?php if ($_uccms_ecomm->getSetting('gratuity_enabled')) { ?>
                            <?php
                            $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);
                            ?>
                            <tr class="sub right gratuity">
                                <td colspan="4" class="title">
                                    <?php if (defined('BIGTREE_ADMIN_ROUTED')) { ?>
                                        <select name="order[gratuity_type]" class="custom_control" style="border-color: #bbb; border-right: 0px none;">
                                            <option value="percent" <?php if ($order['gratuity_type'] == 'percent') { ?>selected="selected"<?php } ?>>%</option>
                                            <option value="amount" <?php if ($order['gratuity_type'] == 'amount') { ?>selected="selected"<?php } ?>>$</option>
                                        </select><input type="text" name="order[override_gratuity]" value="<?=$order['override_gratuity']?>" placeholder="<?php echo $_uccms_ecomm->getSetting('gratuity_percent'); ?>" style="display: inline-block; width: 21px; height: 17px; border-color: #bbb; border-left: 0px none; padding-left: 5px; margin-left: -1px; text-align: right; padding-right: 5px;" />&nbsp;
                                    <?php } ?>
                                    Gratuity:
                                </td>
                                <td class="value">
                                    $<?=$order['gratuity']?>
                                </td>
                            </tr>
                        <?php } ?>

                        <?php

                        // MAIN SERVICE FEE
                        $service_fees += $_uccms_ecomm->orderServiceFee($order);

                        // HAVE FEE(S)
                        if ($service_fees) {

                            ?>
                            <tr class="sub right service_fees">
                                <td colspan="4" class="title">
                                    Service Fee:
                                </td>
                                <td class="value">
                                    $<?php echo number_format($service_fees, 2); ?>
                                </td>
                            </tr>
                        <?php } ?>

                        <?php if ($rental_fees) { ?>
                            <tr class="sub right rental_fees">
                                <td colspan="4" class="title">
                                    Rental Fees:
                                </td>
                                <td class="value">
                                    $<?php echo number_format($rental_fees, 2); ?>
                                </td>
                            </tr>
                        <?php } ?>

                    <?php } ?>

                    <?php if ($extended_view) { ?>

                        <tr class="sub right total">
                            <td colspan="4" class="title">
                                Total:
                            </td>
                            <td class="value">
                                $<?php echo number_format($_uccms_ecomm->orderTotal($order), 2); ?>
                            </td>
                        </tr>

                    <?php } ?>

                    <?php

                // NO ITEMS
                } else {
                    ?>
                    <tr class="item">
                        <td colspan="5" style="padding: 10px 0; background-color: #fff; text-align: center;">
                            No items.
                        </td>
                    </tr>
                    <?php
                }

                ?>

            </table>

        </div>

    </div>

    <?php if (($order['status'] != 'cart') && ($order['id']) && (($extended_view) && (!defined('BIGTREE_ADMIN_ROUTED')))) { ?>

        <?php

        // GET MESSAGES
        $message_query = "SELECT * FROM `" .$_uccms_ecomm->tables['quote_message_log']. "` WHERE (`order_id`=" .$order['id']. ") ORDER BY `dt` DESC";
        $message_q = sqlquery($message_query);

        // NUMBER OF MESSAGES
        $num_messages = sqlrows($message_q);

        // HAVE MESSAGES
        if ($num_messages > 0) {

            ?>

            <div class="log">
                <h4>Log</h4>
                <?php while ($message = sqlfetch($message_q)) { ?>
                    <div class="item">
                        <div class="info contain">
                            <div class="left">
                                <?=date('M j, Y g:i a', strtotime($message['dt']))?> - <?php echo ($message['by'] ? stripslashes($_uccms_ecomm->getSetting('store_name')) : 'Client') ?>
                            </div>
                            <div class="right">
                                <?=ucwords($message['status'])?>
                            </div>
                        </div>
                        <div class="message">
                            <?=stripslashes($message['message'])?>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <?php

        }

        ?>

    <?php } ?>

</div>

<?php if (($order['status'] == 'cart') && (!defined('BIGTREE_ADMIN_ROUTED'))) { ?>

    <div class="continue clearfix">

        <div class="edit">
            <a href="javascript:history.back(0);">&laquo; Edit</a>
        </div>

        <div class="submit">
            <input type="submit" name="" value="Submit" />
        </div>

    </div>

<?php } ?>

<?php if ((!defined('BIGTREE_ADMIN_ROUTED')) && (!in_array($order['status'], array('requested','preparing','presented','declined','cart','deleted'))) && ($extended_view)) { ?>

    <div id="review_right_col">
        <div class="close"><a href="#" title="Hide">x</a></div>
        <div class="padding">

            <?php

            // HAVE QUOTE PAYMENT ERROR
            if ($_SESSION['ecomm']['quote_pay']['err']) {
                ?>
                <div class="message error">
                    <?php
                    if (is_array($_SESSION['ecomm']['quote_pay']['err'])) {
                        echo '<ul>';
                        foreach ($_SESSION['ecomm']['quote_pay']['err'] as $err) {
                            echo '<li>' .$err. '</li>';
                        }
                        echo '</ul>';
                    } else {
                        echo $_SESSION['ecomm']['quote_pay']['err'];
                    }
                    ?>
                </div>
                <?php
                unset($_SESSION['ecomm']['quote_pay']['err']);
            }

            // THANK YOU
            if ($_REQUEST['thanks']) { ?>
                <div class="thanks">
                    Thank you for your payment!
                </div>
            <?php }

            ?>

            <div class="top_balance">
                Balance: $<?=$balance?><?php if ($balance > 0.00) { ?> <a href="#" class="button pay" data-amount="<?=$balance?>">Pay</a><?php } ?>
            </div>

            <div class="section payment expand">

                <h3>
                    Payment
                    <i class="fa fa-caret-down"></i>
                </h3>

                <div class="expand_content closed">

                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr class="item total">
                            <td class="title">Total:</td>
                            <td class="value">$<?php echo number_format($_uccms_ecomm->orderTotal($order), 2); ?></td>
                        </tr>
                        <tr class="item paid">
                            <td class="title">Paid:</td>
                            <td class="value">$<?php
                            $paid = number_format($_uccms_ecomm->orderTotal($order) - $balance, 2);
                            if ($paid > 0) {
                                echo $paid;
                            } else {
                                echo '0.00';
                            }
                            ?></td>
                        </tr>
                    </table>

                    <?php

                    // ORDER IS NOT FULLY PAID FOR
                    if ($balance > 0.00) {

                        // ADD PAYMENT INFO TO $vals
                        $vals['payment'] = $_SESSION['ecomm']['quote_pay']['fields']['payment'];

                        if (!$vals['payment']['amount']) {
                            if ($scheduled_amount) {
                                $vals['payment']['amount'] = $scheduled_amount;
                            } else {
                                $vals['payment']['amount'] = $balance;
                            }
                        }

                        // GET PAYMENT METHODS
                        $payment_methods = $_uccms_ecomm->getPaymentMethods(true);

                        ?>

                        <div class="payment_form item">

                            <form action="./process-payment/" method="post">
                            <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
                            <input type="hidden" name="order_h" value="<?php echo $order['hash']; ?>" />
                            <input type="hidden" name="do" value="process" />

                            <div class="amount">
                                Amount to pay: $<input type="text" name="amount" value="<?=number_format($vals['payment']['amount'], 2)?>" />
                            </div>

                            <?php include(dirname(__FILE__). '/../payment_options.php'); ?>

                            <div class="contain submit" style="<?php if ((!$vals['payment']['method']) && (count($payment_methods) != 1)) { ?>display: none;<?php } ?>">
                                <input type="submit" value="Submit" class="button green" />
                            </div>

                            </form>

                        </div>

                        <?php

                    }

                    ?>

                </div>

            </div>

            <?php

            // HAVE SCHEDULED PAYMENTS
            if (count($spa) > 0) {

                ?>

                <div class="section scheduled_payments expand">

                    <h3>
                        Payments Due
                        <i class="fa <?php if ($_REQUEST['thanks']) { ?>fa-caret-up<?php } else { ?>fa-caret-down<?php } ?>"></i>
                    </h3>

                    <div class="expand_content closed">

                        <div class="clearfix">
                            <?php foreach ($spa as $sp) { ?>
                                <div class="item">
                                    <div class="clearfix">
                                        <div class="date"><?php echo date('n/j/Y', strtotime($sp['dt_due'])); ?></div>
                                        <div class="amount">$<?php echo number_format($sp['amount'], 2); ?></div>
                                        <div class="pay"><a href="#" class="button" data-amount="<?php echo number_format($sp['amount'], 2); ?>">Pay</a></div>
                                    </div>
                                    <?php if ($sp['title']) { ?>
                                        <div class="title">
                                            <?php echo stripslashes($sp['title']); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>

                    </div>

                </div>

                <?php

            }

            // GET TRANSACTION LOG
            $translog_query = "SELECT * FROM `" .$_uccms_ecomm->tables['transaction_log']. "` WHERE (`cart_id`='" .$order['id']. "') ORDER BY `dt` DESC";
            $translog_q = sqlquery($translog_query);

            // HAVE TRANSACTIONS
            if (sqlrows($translog_q) > 0) {

                ?>

                <div class="section transactions expand">

                    <h3>
                        Payment Log
                        <i class="fa <?php if ($_REQUEST['thanks']) { ?>fa-caret-up<?php } else { ?>fa-caret-down<?php } ?>"></i>
                    </h3>

                    <div class="expand_content <?php if (!$_REQUEST['thanks']) { ?>closed<?php } ?>">

                        <table width="100%" cellpadding="0" cellspacing="0">
                            <?php while ($transaction = sqlfetch($translog_q)) { ?>
                                <tr class="item">
                                    <td class="title">
                                        <div class="date"><?php echo date('n/j/Y', strtotime($transaction['dt'])); ?></div>
                                        <div class="time"><?php echo date('g:i A T', strtotime($transaction['dt'])); ?></div>
                                    </td>
                                    <td class="amount">
                                        $<?php echo number_format($transaction['amount'], 2); ?>
                                    </td>
                                    <td class="status">
                                        <?php echo ucwords($transaction['status']); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>

                    </div>

                </div>

                <?php

            }

            ?>

        </div>
    </div>

    <div id="review_right_col-expand" style="display: none;">
        <a href="#" title="Payment Details"><img src="/extensions/com.umbrella.ecommerce/images/cash-register.png" /></a>
    </div>

<?php } ?>