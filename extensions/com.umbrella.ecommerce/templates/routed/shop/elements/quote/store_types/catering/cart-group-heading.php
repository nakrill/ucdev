<?php

// DAY / TIME IS DIFFERENT
if ($oitem['display_group']['day_time'] != $cdga['day_time']) {

    // LOOP DAY / TIME
    $loop_day = ($cdga['day_time'] ? date('m/d/Y', $cdga['day_time']) : '');
    $loop_time = ($cdga['day_time'] ? date('g:i a', $cdga['day_time']) : '');

    // ITEM DAY / TIME
    $item_day = date('m/d/Y', $oitem['display_group']['day_time']);
    $item_time = date('g:i a', $oitem['display_group']['day_time']);

    // DAY OR TIME IS DIFFERENT
    if (($item_day != $loop_day) || ($item_time != $loop_time)) {

        ?>

        <tr class="group_heading">
            <td colspan="5">
                <strong><?=$item_day?></strong> <?=$item_time?>
            </td>
        </tr>

        <?php

        if ($item_day != $loop_day) {
            $loop_time = '';
        }

    }

}

// HAVE CATEGORY AND IS DIFFERENT
if (($oitem['display_group']['category']) && ($oitem['display_group']['category'] != $cdga['category'])) {

    // DON'T HAVE CATEGORY INFO YET
    if (!$dgca[$oitem['display_group']['category']]) {

        // GET CATEGORY INFO
        $dgc_query = "SELECT * FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`id`=" .(int)$oitem['display_group']['category']. ")";
        $dgc_q = sqlquery($dgc_query);
        $dgca[$oitem['display_group']['category']] = sqlfetch($dgc_q);

    }

    ?>

    <tr class="group_heading2">
        <td colspan="5">
            <strong><?php echo stripslashes($dgca[$oitem['display_group']['category']]['title']); ?></strong>
        </td>
    </tr>

    <?php

}

// SET CURRENT DISPLAY GROUP
$cdga = $oitem['display_group'];

?>