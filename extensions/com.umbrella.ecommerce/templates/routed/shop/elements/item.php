<?php

// HAVE ITEM ID
if ($item['id']) {

    // LINK
    $link = $_uccms_ecomm->itemURL($item['id'], (int)$item['category_id'], $item);

    // GET IMAGE
    $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC LIMIT 1";
    $image = sqlfetch(sqlquery($image_query));

    // HAVE IMAGE
    if ($image['image']) {
        $image_url = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
    } else {
        $image_url = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
    }

    // ON SALE
    if ($item['price_sale'] != '0.00') {
        $sale = true;
        $price = $item['price_sale'];

    // NOT ON SALE
    } else {
        $sale = false;
        $price = $_uccms_ecomm->itemPrice($item);
    }

    ?>

    <div class="item2 col-md-3 <?php if ($sale) { ?>sale<?php } ?>" data-id="<?php echo $item['id']; ?>">
        <div class="contain">
            <div class="image">
                <a href="<?php echo $link; ?>"><img src="<?php echo $image_url; ?>" class="img-responsive" alt="<?php echo stripslashes($item['title']); ?>" /></a>
            </div>
            <div class="details">
                <?php if ($item['title']) { ?>
                    <div class="title"><a href="<?php echo $link; ?>"><?php echo stripslashes($item['title']); ?></a></div>
                <?php } ?>
                <?php if ((!$item['hide_price']) && (!$item['quote'])) { ?>
                    <div class="price">
                        $<?php echo number_format($price, 2); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php

}

?>