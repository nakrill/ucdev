<?php

// NOT IN ADMIN
if (!defined('BIGTREE_ADMIN_ROUTED')) {

    // REMOVE ONES NOT AVAILABLE FOR FRONTEND
    if (count($payment_methods) > 0) {
        foreach ($payment_methods as $pi => $pm) {
            if (!$pm['frontend']) unset($payment_methods[$pi]); // NOT AVAILABLE FOR FRONTEND
        }
    }

}

// NUMBER OF PAYMENT METHODS
$num_payment_methods = count($payment_methods);

// HAVE PAYMENT METHODS
if ($num_payment_methods > 0) {

        // PAYMENT METHODS
        foreach ($payment_methods as $payment) {

            ?>

            <div class="method">
                <?php if ($num_payment_methods > 1) { ?>
                    <div class="top">
                        <input type="radio" name="payment[method]" value="<?php echo $payment['id']; ?>" <?php if (($payment['id'] == $vals['payment']['method']) || ($num_payment_methods == 1)) { ?>checked="checked"<?php } ?> /> <?php echo stripslashes($payment['title']); ?>
                    </div>
                <?php } else { ?>
                    <input type="hidden" name="payment[method]" value="<?php echo $payment['id']; ?>" />
                <?php } ?>
                <div class="info" style="<?php if ($num_payment_methods > 1) { if ($payment['id'] != $vals['payment']['method']) { ?>display: none;<?php } } ?>">

                    <?php if ($payment['description']) { ?>
                        <div class="description"><?php echo nl2br(stripslashes($payment['description'])); ?></div>
                    <?php } ?>

                    <?php

                    // CARD PAYMENT
                    if (!$payment['hide_cc']) {

                        // LOGGED IN AND HAVE PAYMENT PROFILE(S)
                        if (($_uccms['_account']->userID()) && (count($payment_profiles) > 0)) { ?>
                            <div class="payment_profile">
                                <fieldset>
                                    <label>Your Cards</label>
                                    <select name="payment[profile_id]">
                                        <?php foreach ($payment_profiles as $pp) { ?>
                                            <option value="<?php echo $pp['id']; ?>" <?php if ($pp['id'] == $form_session['payment']['profile_id']) { ?>selected="selected"<?php } ?>><?php if ($pp['type']) { echo ucwords($pp['type']). ' - '; } echo $pp['lastfour']; ?></option>
                                        <?php } ?>
                                        <option value="">New Card</option>
                                    </select>
                                </fieldset>
                            </div>
                        <?php } ?>

                        <div id="card_new" style="margin-top: 10px; <?php if (($_uccms['_account']->userID()) && (count($payment_profiles) > 0)) { ?>display: none;<?php } ?>">

                            <fieldset>
                                <label>Name On Card *</label>
                                <input type="text" name="payment[<?php echo $payment['id']; ?>][name]" value="<?php echo $vals['payment'][$payment['id']]['name']; ?>" />
                            </fieldset>

                            <fieldset>
                                <label>Card Number *</label>
                                <input type="text" name="payment[<?php echo $payment['id']; ?>][number]" value="<?php echo $vals['payment'][$payment['id']]['number']; ?>" />
                            </fieldset>

                            <fieldset>
                                <label>Card Expiration *</label>
                                <input type="text" name="payment[<?php echo $payment['id']; ?>][expiration]" value="<?php echo $vals['payment'][$payment['id']]['expiration']; ?>" placeholder="mm/yy" maxlength="5" />
                            </fieldset>

                            <fieldset>
                                <label>Card Zip *</label>
                                <input type="text" name="payment[<?php echo $payment['id']; ?>][zip]" value="<?php echo $vals['payment'][$payment['id']]['zip']; ?>" />
                            </fieldset>

                            <fieldset>
                                <label>Card CVV *</label>
                                <input type="text" name="payment[<?php echo $payment['id']; ?>][cvv]" value="<?php echo $vals['payment'][$payment['id']]['cvv']; ?>" />
                            </fieldset>

                            <div class="credit_card_logos">
                                <img src="/extensions/<?php echo $_uccms_ecomm->Extension; ?>/images/credit-card-icons.png" class="credit-cards" />
                            </div>

                        </div>

                        <?php

                    }

                    ?>

                </div>
            </div>

            <?php

        }

    ?>

    <script type="text/javascript">
        $(document).ready(function() {

            // CARD SELECT
            $('.method select[name="payment[profile_id]"').change(function() {
                var $parent = $(this).closest('.method');
                if ($(this).val() != '') {
                    $parent.find('#card_new').hide();
                } else {
                    $parent.find('#card_new').show();
                }
            });

        });
    </script>

    <?php

// NO PAYMENT METHODS
} else {
    ?>
    No payment methods configured.
    <?php
}

?>