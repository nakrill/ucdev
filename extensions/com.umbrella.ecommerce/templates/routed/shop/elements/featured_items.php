<?php

// LIMIT
$featured_limit = ($ecomm['settings']['home_featured_num'] ? (int)$ecomm['settings']['home_featured_num'] : 3);

// GET FEATURED ITEMS
$featured_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`active`=1) AND (`featured`=1) AND (`deleted`=0) ORDER BY `id` ASC LIMIT " .$featured_limit;
$featured_q = sqlquery($featured_query);

// HAVE FEATURED ITEMS
if (sqlrows($featured_q) > 0) {

    // HEADING
    if (!$ecomm['settings']['home_featured_title']) $ecomm['settings']['home_featured_title'] = 'Featured Items';

    ?>

    <div class="featured">

        <h3><?php echo stripslashes($ecomm['settings']['home_featured_title']); ?></h3>

        <div class="item_container contain">

            <?php

            // LOOP
            while ($item = sqlfetch($featured_q)) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item.php');
            }

            ?>

        </div>

    </div>

    <?php

}

?>