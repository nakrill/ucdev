<?php

// GET BOOKING INFO
$booking_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
$booking_item_q = sqlquery($booking_item_query);
$booking_item = sqlfetch($booking_item_q);

?>

<link rel="stylesheet" href="/css/lib/daterangepicker.css">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script type="text/javascript" src="/js/lib/daterangepicker.js"></script>

<style type="text/css">

    #ecommContainer .item_container .booking .duration {
        display: none;
        margin-top: 10px;
    }

    #ecommContainer .item_container .booking {
        margin: 15px 0 0 0;
        padding: 0px;
    }

    #ecommContainer .item_container .booking .duration .options .option {
        margin-bottom: 8px;
        padding: 5px 10px;
        border: 1px solid #ddd;
        cursor: pointer;
    }
    #ecommContainer .item_container .booking .duration .options .option:last-child {
        margin-bottom: 0px;
    }
    #ecommContainer .item_container .booking .duration .options .option:hover {
        background-color: #f5f5f5;
    }
    #ecommContainer .item_container .booking .duration .options .option.selected {
        background-color: #f0f0f0;
    }
    #ecommContainer .item_container .booking .duration .options .option .title {
        float: left;
        margin-right: 15px;
    }
    #ecommContainer .item_container .booking .duration .options .option .available {
        float: right;
    }
    #ecommContainer .item_container .booking .duration .options .option .date_time {
        clear: both;
        float: left;
        margin-right: 15px;
    }
    #ecommContainer .item_container .booking .duration .options .option .markup {
        float: right;
    }
    #ecommContainer .item_container .booking .duration .options .option.unavailable {
        cursor: default;
        opacity: .5;
    }
    #ecommContainer .item_container .booking .duration .options .option.unavailable:hover {
        background-color: transparent;
    }

</style>

<?php

$batch_mode = 'days';
$batch_days = 0;

$from_to = '';
if (($booking['dt_from']) && ($booking['dt_from'] != '0000-00-00 00:00:00')) {
    $from_to .= date('Y-m-d', strtotime($booking['dt_from']));
    if (($booking['dt_to']) && ($booking['dt_to'] != '0000-00-00 00:00:00')) {
        $from_to .= ' to ' .date('Y-m-d', strtotime($booking['dt_to']));
    }
}

?>

<script type="text/javascript">

    $(document).ready(function() {

        booking_avail_data = {};
        booking_avail_days = {};

        // CALENDAR CONFIG
        var calendar_config = {
            inline: true,
            container: '#ecommContainer .item_container .booking .from_to .calendar_inline',
            alwaysOpen: true,
            extraClass: 'date-range-picker-custom',
            singleMonth: true,
            selectForward: true,
            startDate: '<?php echo date('Y-m-d'); ?>',
            <?php if ($batch_mode) { ?>
                batchMode: '<?php echo $batch_mode; ?>',
                batchDays: <?php echo (int)$batch_days; ?>,
            <?php } ?>
            beforeShowDay: function(t) {
                var valid = 1;
                var dt = moment(t).format('YYYY-MM-DD').toString();
                var avail = booking_avail_days[dt];
                if (typeof avail === 'undefined') avail = 0;
                if (avail == 0) {
                    valid = 0;
                    var _class = 'noselect';
                } else {
                    var _class = '';
                }
                var _tooltip = '';
                return [valid,_class,_tooltip];
            },
            showDateFilter: function(time, date) {
                var date_formatted = moment(time).format('YYYY-MM-DD');
                var avail = booking_avail_days[date_formatted.toString()];
                if (typeof avail === 'undefined') avail = '&nbsp;';
                return '<div style="padding:0 5px;">\
                            <span style="font-weight:bold">'+date+'</span>\
                            <div style="opacity:0.3;">' +avail+ '</div>\
                        </div>';
            },

            <?
            /*
            hoveringTooltip: function(days) {
                var D = ['One','Two', 'Three','Four','Five'];
                return D[days] ? D[days] : days;
            },
            */
            ?>

            setValue: function(s, s1, s2) {
                //$('#ecommContainer .item_container .booking .from_to input[name="booking[from]"]').val(s1);
                //$('#ecommContainer .item_container .booking .from_to input[name="booking[to]"]').val(s2);
            },

            beforeNextMonth: function(month, callback) {
                $('#ecommContainer .item_container .booking .duration').hide();
                $('#ecommContainer .item_container .booking .duration .options .dynamic').remove();
                $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                    item_id: <?php echo $item['id']; ?>,
                    from: month,
                    <?php if ($booking_item['calendar_id']) { ?>
                        calendar_id: <?php echo $booking_item['calendar_id']; ?>
                    <?php } ?>
                }, function(data) {
                    booking_avail_data = data;
                    $.each(data, function(date, avail) {
                        booking_avail_days[date] = avail.items[<?php echo $item['id']; ?>].available;
                    });
                    callback();
                }, 'json');
            },

            beforePrevMonth: function(month, callback) {
                $('#ecommContainer .item_container .booking .duration').hide();
                $('#ecommContainer .item_container .booking .duration .options .dynamic').remove();
                $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
                    item_id: <?php echo $item['id']; ?>,
                    from: month,
                    <?php if ($booking_item['calendar_id']) { ?>
                        calendar_id: <?php echo $booking_item['calendar_id']; ?>
                    <?php } ?>
                }, function(data) {
                    booking_avail_data = data;
                    $.each(data, function(date, avail) {
                        booking_avail_days[date] = avail.items[<?php echo $item['id']; ?>].available;
                    });
                    callback();
                }, 'json');
            }

        }

        // CALENDAR
        $.get('/*/com.umbrella.ecommerce/ajax/store_types/booking/availability/', {
            item_id: <?php echo $item['id']; ?>,
            from: '<?php echo (($booking['dt_from'] && $booking['dt_from'] != '0000-00-00 00:00:00') ? date('Y-m-01', strtotime($booking['dt_from'])) : date('Y-m-01')); ?>',
            <?php if ($booking_item['calendar_id']) { ?>
                calendar_id: <?php echo $booking_item['calendar_id']; ?>
            <?php } ?>
        }, function(data) {

            booking_avail_data = data;

            $.each(data, function(date, avail) {
                booking_avail_days[date] = avail.items[<?php echo $item['id']; ?>].available;
            });

            // CALENDAR
            $('#ecommContainer .item_container .booking .from_to .input').dateRangePicker(calendar_config)
            .bind('datepicker-first-date-selected', function(event, obj) {
                /*
                console.log(event);
                console.log(obj);
                console.log(booking_avail_data);
                */
            })
            .bind('datepicker-change', function(event, obj) {

                $('#ecommContainer .item_container .booking .duration').hide();
                $('#ecommContainer .item_container .booking .duration .options .dynamic').remove();

                var date = moment(obj.date1).format('YYYY-MM-DD').toString();

                var day = booking_avail_data[date]['items'][<?php echo $item['id']; ?>];

                var num_durations = 0;

                var selected_date = '<?php echo ($booking['dt_from'] ? date('Y-m-d', strtotime($booking['dt_from'])) : ''); ?>';
                var duration_id = <?php echo (int)$booking['duration_id']; ?>;
                var duration_i = <?php echo (int)$booking['duration_i']; ?>;

                $.each(day['durations'], function(dur_id, durations) {
                    var i = 0;
                    $.each(durations, function(index, duration) {
                        var clone = $('#ecommContainer .item_container .booking .duration .options .option.template').clone();
                        clone.removeClass('template');
                        clone.addClass('dynamic');
                        if (!duration.available) {
                            clone.addClass('unavailable');
                        } else {
                            if (date == selected_date) {
                                if ((duration.id == duration_id) && (i == duration_i)) {
                                    clone.addClass('selected');
                                }
                            }
                        }
                        clone.attr('data-duration_id', duration.id);
                        clone.attr('data-duration_i', i);
                        clone.attr('data-duration_from', duration.start);
                        clone.attr('data-duration_to', duration.end);
                        clone.find('.title').text(duration.title);

                        var start = duration.start.split(' ');
                        var end = duration.end.split(' ');

                        var tta = [];

                        tta.push('<span class="from">');
                        if (start[0] != end[0]) { // DIFFERENT DAYS
                            tta.push('<span class="date">' +moment(start[0], 'YYYY-MM-DD').format('M/D/YYYY').toString()+ '</span>');
                        }
                        if (start[1] != '00:00:00') { // NOT FULL DAY
                            tta.push('<span class="time">' +moment(start[1], 'HH:mm:ss').format('h:mm A').toString()+ '</span>');
                        }
                        tta.push('</span> to <span class="to">');
                        if (start[0] != end[0]) { // DIFFERENT DAYS
                            tta.push('<span class="date">' +moment(end[0], 'YYYY-MM-DD').format('M/D/YYYY').toString()+ '</span>');
                        }
                        if (end[1] != '00:00:00') { // NOT FULL DAY
                            tta.push('<span class="time">' +moment(end[1], 'HH:mm:ss').format('h:mm A').toString()+ '</span>');
                        }
                        tta.push('</span>');

                        clone.find('.date_time').html(tta.join(' '));

                        clone.find('.available').text(duration.available);

                        if ((duration.markup) && (duration.markup != '0.00')) {
                            clone.find('.markup .num').text(duration.markup);
                        } else {
                            clone.find('.markup').remove();
                        }
                        $('#ecommContainer .item_container .booking .duration .options').append(clone.show());
                        num_durations++;
                        i++;
                    });
                });

                if (num_durations > 0) {
                    $('#ecommContainer .item_container .booking .duration').show();
                    if (num_durations == 1) {
                        $('#ecommContainer .item_container .booking .duration .options .option.dynamic').click();
                    }
                }

            });

            <?php if (($booking['dt_from']) && ($booking['dt_from'] != '0000-00-00 00:00:00')) { ?>
                $('#ecommContainer .item_container .booking .from_to .date-picker-wrapper .month-wrapper table .day[data-date="<?php echo date('Y-m-d', strtotime($booking['dt_from'])); ?>"]').click();
            <?php } ?>

        }, 'json');

        // DURATION SELECT
        $('#ecommContainer .item_container .booking .duration .options').on('click', '.option', function(e) {
            if (!$(this).hasClass('unavailable')) {
                $('#ecommContainer .item_container .booking .from_to input[name="booking[from]"]').val($(this).attr('data-duration_from'));
                $('#ecommContainer .item_container .booking .from_to input[name="booking[to]"]').val($(this).attr('data-duration_to'));
                $('#ecommContainer .item_container .booking .from_to input[name="booking[duration_id]"]').val($(this).attr('data-duration_id'));
                $('#ecommContainer .item_container .booking .from_to input[name="booking[duration_i]"]').val($(this).attr('data-duration_i'));
                $('#ecommContainer .item_container .booking .duration .options .option').removeClass('selected');
                $(this).addClass('selected');
                $('#ecommContainer .item_container .add-to-cart').show();
            }
        });

    });

</script>

<div class="from_to">

    <input type="hidden" name="booking[from_to_input]" value="<?php echo $from_to; ?>" class="input" />

    <input type="hidden" name="booking[from]" value="<?php echo $booking['dt_from']; ?>" />
    <input type="hidden" name="booking[to]" value="<?php echo $booking['dt_to']; ?>" />

    <input type="hidden" name="booking[duration_id]" value="<?php echo $booking['duration_id']; ?>" />
    <input type="hidden" name="booking[duration_i]" value="<?php echo $booking['duration_i']; ?>" />

    <div class="calendar_inline"></div>

    <div class="duration">

        <div class="options select_box clearfix">

            <div class="option template" style="display: none;">
                <div class="inner clearfix">
                    <div class="title"></div>
                    <div class="available"></div>
                    <div class="date_time"></div>
                    <div class="markup"><span class="plus">+</span><span class="currency">$</span><span class="num"></span></div>
                </div>
            </div>

        </div>

    </div>

</div>