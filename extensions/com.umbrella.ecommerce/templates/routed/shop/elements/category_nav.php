<?php

if (!$ecomm['settings']['home_hide_categories']) {

    // GET MASTER CATEGORIES
    $cats = $_uccms_ecomm->subCategories(0, true, true);

    // HAVE MASTER CATEGORIES
    if (count($cats) > 0) {

        ?>

        <div class="category_nav">
            <h3>Categories</h3>
            <ul>
                <?php

                // LOOP
                foreach ($cats as $cat) {

                    ?>
                    <li class="<?php if ($cat['id'] == $category_master_parent_id) { ?>active<?php } ?>"><a href="<?php echo $_uccms_ecomm->categoryURL($cat['id'], $cat); ?>"><?php echo stripslashes($cat['title']); ?></a></li>
                    <?php

                }

                ?>
            </ul>
        </div>

        <?php

    }

}

?>