<?php

// LIMIT
$recent_limit = ($ecomm['settings']['home_recent_num'] ? (int)$ecomm['settings']['home_recent_num'] : 3);

// GET RECENT ITEMS
$recent_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`active`=1) AND (`not_recent`=0) AND (`deleted`=0) ORDER BY `id` ASC LIMIT " .$recent_limit;
$recent_q = sqlquery($recent_query);

// HAVE RECENT ITEMS
if (sqlrows($recent_q) > 0) {

    // HEADING
    if (!$ecomm['settings']['home_recent_title']) $ecomm['settings']['home_recent_title'] = 'Recent Items';

    ?>

    <div class="recent">

        <h3><?php echo stripslashes($ecomm['settings']['home_recent_title']); ?></h3>

        <div class="item_container contain">

            <?php

            // LOOP
            while ($item = sqlfetch($recent_q)) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item.php');
            }

            ?>

        </div>

    </div>

    <?php

}

?>