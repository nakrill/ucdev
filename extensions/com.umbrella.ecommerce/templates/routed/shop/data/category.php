<?php

// CLEAN UP
$category_id = (int)$category_id;

// HAVE CATEGORY ID
if ($category_id > 0) {

    // GET CATEGORY
    $category_query = "SELECT * FROM `" .$_uccms_ecomm->tables['categories']. "` WHERE (`id`=" .$category_id. ") AND (`active`=1)";
    $category_q = sqlquery($category_query);
    $category = sqlfetch($category_q);

}

// HAVE CATEGORY
if ($category['id']) {

    // TRACK CATEGORY VIEW
    $_SESSION['uccmsAcct_webTracking']['events']['category-view'] = trim(stripslashes($category['title']). ' (#' .$category['id']. ')');

    // CUSTOM EDIT LINK
    $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/categories/edit/?id=' .$category['id'];

    // PRICEGROUPS ENABLED
    if ($_uccms_ecomm->getSetting('pricegroups_enabled')) {

        $access = false;

        // ACCOUNT'S PRICE GROUPS
        $apg = $_uccms_ecomm->accountPriceGroups();

        // CATEGORY'S PRICE GROUPS
        $cpg = $_uccms_ecomm->categoryPriceGroups($category['id'], 'true');

        // HAVE PRICE GROUPS
        if (count($cpg) > 0) {

            // ACCOUNT HAS ACCESS TO ONE OR MORE OF CATEGORY'S PRICE GROUPS
            if (count(array_intersect(array_keys($cpg), $apg)) > 0) {
                $access = true;
            }

        // NO PRICE GROUPS
        } else {
            $access = true;
        }

        // NO ACCESS
        if ($access == false) {
            $_uccms['_site-message']->set('err', 'You do not have access to this category.');
            BigTree::redirect('../');
        }

    }

    // CATEGORY SLUG
    if ($category['slug']) {
        $slug = stripslashes($category['slug']);
    } else {
        $slug = $_uccms_ecomm->makeRewrite(stripslashes($category['title']));
    }

    // SLUGS DON'T MATCH - 301 TO CURRENT
    if ($bigtree['path'][1] != $slug. '-' .$category['id']) {

        // 301 REDIRECT TO CURRENT CATEGORY URL
        header('Location: ' .$_uccms_ecomm->categoryURL($category['id'], $category), true, 301);
        exit;

    }

    // META
    if ($category['meta_title']) {
        $bigtree['page']['title'] = stripslashes($category['meta_title']);
    } else {
        $bigtree['page']['title'] = stripslashes($category['title']);
    }
    if ($category['meta_description']) {
        $bigtree['page']['meta_description'] = stripslashes($category['meta_description']);
    } else if ($category['description']) {
        $bigtree['page']['description'] = stripslashes($category['description']);
    }
    if ($category['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($category['meta_keywords']);

    // CANONICAL URL
    $bigtree['page']['canonical'] = $_uccms_ecomm->categoryURL($category['id'], $category);

    // CATEGORY PARENT(S) ARRAY
    $catpa = $_uccms_ecomm->getCategoryParents($category['id']);

    // CATEGORY MASTER PARENT ID
    $category_master_parent_id = $catpa[count($catpa) - 1]['id'];

    // GET SUB-CATEGORIES
    $sub_categories = $_uccms_ecomm->subCategories($category['id'], true, true);

    // SET CATEGORY ID
    $_GET['category'] = $category['id'];

    // PAGE LIMIT
    $page_limit = $_uccms_ecomm->perPage();

    // OFFSET
    if ($_GET['page']) {
        $offset = ((int)$_GET['page'] - 1) * $page_limit;
    } else {
        $offset = 0;
    }

    // SORT
    if ($_REQUEST['sort']) {
        list($order_field, $order_direction) = explode('.', $_REQUEST['sort']);
    }

    // VARIABLES TO FIND BY
    $items_vars = array(
        'vars'              => $_GET,
        'order_field'       => $order_field,
        'order_direction'   => $order_direction,
        'limit'             => $page_limit,
        'offset'            => $offset
    );

    //echo print_r($prop_vars);

    // GET MATCHING ITEMS
    $items = $_uccms_ecomm->getItems($items_vars);

    //echo print_r($items);
    //exit;

    // HAVE RESULTS
    if ($items['num_results'] > 0) {

        // PAGING CLASS
        require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

        // INIT PAGING CLASS
        $_paging = new paging();

        // OPTIONAL - URL VARIABLES TO IGNORE
        $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

        // PREPARE PAGING
        $_paging->prepare($page_limit, 10, $_GET['page'], $_GET);

        // GENERATE PAGING
        $pages = $_paging->output($items['num_results'], $items['num_total']);

        // SET ITEMS
        $category_items = $items['results'];

        // NUMBER OF PAGED ITEMS
        $num_items = count($category_items);

        // TOTAL NUMBER OF ITEMS
        $total_category_items = $items['num_total'];

    }

// CATEGORY NOT FOUND
} else {
    header('HTTP/1.0 404 Not Found');
    $bigtree['page']['title'] = 'Category Not Found';
    $bigtree['page']['meta_description'] = '';
    $bigtree['page']['meta_keywords'] = '';
}

?>