<?php

// HAVE ORDER ID & HASH
if (($_REQUEST['order_id']) && ($_REQUEST['h'])) {

    // GET ORDER INFO
    $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .(int)$_REQUEST['order_id']. ") AND (`hash`='" .sqlescape($_REQUEST['h']). "')";
    $order_q = sqlquery($order_query);
    $order = sqlfetch($order_q);

}

// NO ORDER FOUND
if (!$order['id']) {

    // INITIALIZE CART
    $order = $_uccms_ecomm->initCart(false);

}

// HAVE ORDER ID
if ($order['id']) {

    // GET QUOTABLE ORDER ITEMS
    $citems = $_uccms_ecomm->cartItems($order['id'], true);

    // NUMBER OF ITEMS
    $num_items = count($citems);

    // HAVE QUOTABLE ITEMS
    if ($num_items > 0) {

        // REVIEWING
        if ($_REQUEST['do'] == 'review') {

            // SAVE FIELDS TO SESSION
            unset($_SESSION['ecomm']['quote_request']['fields']);
            $_SESSION['ecomm']['quote_request']['fields'] = $_POST;
            $vals = $_POST;

        // PROCESSING
        } else if ($_REQUEST['do'] == 'process') {


        // MAIN PAGE
        } else {

            // HAVE SESSION VALUES
            if (count($_SESSION['ecomm']['quote_request']['fields']) > 0) {
                $vals = $_SESSION['ecomm']['quote_request']['fields'];

            } else {

                // HAVE USER ID (IS LOGGED IN)
                if ($_uccms['_account']->userID()) {

                    // GET LATEST ORDER
                    $lorder_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .$_uccms['_account']->userID(). ") AND (`status`!='cart') ORDER BY `dt` DESC LIMIT 1";
                    $lorder_q = sqlquery($lorder_query);
                    $lorder = sqlfetch($lorder_q);

                    // LAST ORDER FOUND
                    if ($lorder['id']) {
                        $vals['billing']['firstname']   = stripslashes($lorder['billing_firstname']);
                        $vals['billing']['lastname']    = stripslashes($lorder['billing_lastname']);
                        $vals['billing']['company']     = stripslashes($lorder['billing_company']);
                        $vals['billing']['address1']    = stripslashes($lorder['billing_address1']);
                        $vals['billing']['address2']    = stripslashes($lorder['billing_address2']);
                        $vals['billing']['city']        = stripslashes($lorder['billing_city']);
                        $vals['billing']['state']       = stripslashes($lorder['billing_state']);
                        $vals['billing']['zip']         = stripslashes($lorder['billing_zip']);
                        $vals['billing']['phone']       = stripslashes($lorder['billing_phone']);
                        $vals['billing']['email']       = stripslashes($lorder['billing_email']);
                        $vals['order']['shipping_same'] = (int)$lorder['shipping_same'];
                        $vals['shipping']['firstname']  = stripslashes($lorder['shipping_firstname']);
                        $vals['shipping']['lastname']   = stripslashes($lorder['shipping_lastname']);
                        $vals['shipping']['company']    = stripslashes($lorder['shipping_company']);
                        $vals['shipping']['address1']   = stripslashes($lorder['shipping_address1']);
                        $vals['shipping']['address2']   = stripslashes($lorder['shipping_address2']);
                        $vals['shipping']['city']       = stripslashes($lorder['shipping_city']);
                        $vals['shipping']['state']      = stripslashes($lorder['shipping_state']);
                        $vals['shipping']['zip']        = stripslashes($lorder['shipping_zip']);
                        $vals['shipping']['phone']      = stripslashes($lorder['shipping_phone']);
                        $vals['shipping']['email']      = stripslashes($lorder['shipping_email']);
                    }

                }

                // TRY TO GET FROM CURRENT ORDER
                $vals['contact']['firstname']   = stripslashes($order['billing_firstname']);
                $vals['contact']['lastname']    = stripslashes($order['billing_lastname']);
                $vals['contact']['company']     = stripslashes($order['billing_company']);
                $vals['contact']['address1']    = stripslashes($order['billing_address1']);
                $vals['contact']['address2']    = stripslashes($order['billing_address2']);
                $vals['contact']['city']        = stripslashes($order['billing_city']);
                $vals['contact']['state']       = stripslashes($order['billing_state']);
                $vals['contact']['zip']         = stripslashes($order['billing_zip']);
                $vals['contact']['phone']       = stripslashes($order['billing_phone']);
                $vals['contact']['email']       = stripslashes($order['billing_email']);

            }

        }

    // NO QUOTABLE ITEMS
    } else {
        $_uccms['_site-message']->set('error', 'No quotable items found in your cart.');
    }

// NO ORDER FOUND
} else {
    $_uccms['_site-message']->set('error', 'Order not found.');
}

?>