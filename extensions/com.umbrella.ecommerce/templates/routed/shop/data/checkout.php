<?php

// INITIALIZE CART
$cart = $_uccms_ecomm->initCart();

// HAVE CART ID
if ($cart['id']) {

    // GET CART ITEMS
    $citems = $_uccms_ecomm->cartItems($cart['id']);

    // NUMBER OF ITEMS
    $num_items = count($citems);

    // HAVE ITEMS IN CART
    if ($num_items > 0) {

        $i = 0;

        // LOOP
        foreach ($citems as $item) {
            if ($item['quote']) {
                $i++;
            }
        }

        // ALL ITEMS ARE QUOTE ITEMS
        if ($i == $num_items) {

            // SEND TO QUOTE
            Bigtree::redirect('../quote/');
            exit;

        // HAVE ITEMS TO PAY FOR
        } else {

            $order = array();

            // HAVE SESSION VALUES
            if (count($_SESSION['ecomm']['checkout']['fields']) > 0) {
                $vals = $_SESSION['ecomm']['checkout']['fields'];

            // HAVE USER ID (IS LOGGED IN)
            } else if ($_uccms['_account']->userID()) {

                // GET LATEST ORDER
                $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`customer_id`=" .$_uccms['_account']->userID(). ") AND (`status`!='cart') ORDER BY `dt` DESC LIMIT 1";
                $order_q = sqlquery($order_query);
                $order = sqlfetch($order_q);

            } else {

                // USE THIS CART
                $order = $cart;

            }

            // HAVE ORDER INFO
            if ($order['id']) {
                $vals['billing']['firstname']   = stripslashes($order['billing_firstname']);
                $vals['billing']['lastname']    = stripslashes($order['billing_lastname']);
                $vals['billing']['company']     = stripslashes($order['billing_company']);
                $vals['billing']['address1']    = stripslashes($order['billing_address1']);
                $vals['billing']['address2']    = stripslashes($order['billing_address2']);
                $vals['billing']['city']        = stripslashes($order['billing_city']);
                $vals['billing']['state']       = stripslashes($order['billing_state']);
                $vals['billing']['zip']         = stripslashes($order['billing_zip']);
                $vals['billing']['phone']       = stripslashes($order['billing_phone']);
                $vals['billing']['email']       = stripslashes($order['billing_email']);
                $vals['order']['shipping_same'] = (int)$order['shipping_same'];
                $vals['shipping']['firstname']  = stripslashes($order['shipping_firstname']);
                $vals['shipping']['lastname']   = stripslashes($order['shipping_lastname']);
                $vals['shipping']['company']    = stripslashes($order['shipping_company']);
                $vals['shipping']['address1']   = stripslashes($order['shipping_address1']);
                $vals['shipping']['address2']   = stripslashes($order['shipping_address2']);
                $vals['shipping']['city']       = stripslashes($order['shipping_city']);
                $vals['shipping']['state']      = stripslashes($order['shipping_state']);
                $vals['shipping']['zip']        = stripslashes($order['shipping_zip']);
                $vals['shipping']['phone']      = stripslashes($order['shipping_phone']);
                $vals['shipping']['email']      = stripslashes($order['shipping_email']);
            }

            // HAVE BILLING STATE AND/OR ZIP
            if (($vals['billing']['state']) || ($vals['billing']['zip'])) {
                $taxvals['country'] = 'United States';
                $taxvals['state']   = $vals['billing']['state'];
                $taxvals['zip']     = $vals['billing']['zip'];
            }

            $taxvals['quote'] = false;

            // GET TAX
            $tax = $_uccms_ecomm->cartTax($cart['id'], $citems, $taxvals);

            // GET PAYMENT METHODS
            $payment_methods = $_uccms_ecomm->getPaymentMethods(true);

            // PAYMENT PROFILES
            $payment_profiles = array();

            // IS LOGGED IN
            if ($_uccms['_account']->userID()) {

                // PAYMENT PROFILES ENABLED
                if ($_uccms['_account']->paymentProfilesEnabled()) {

                    // GET PAYMENT PROFILES
                    $pp_query = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$_uccms['_account']->userID(). ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
                    $pp_q = sqlquery($pp_query);
                    while ($pp = sqlfetch($pp_q)) {
                        $payment_profiles[$pp['id']] = $pp;
                    }

                }

            }

        }

    // NO ITEMS
    } else {

        // SEND TO CART
        Bigtree::redirect('../cart/');
        exit;

    }

// NO CART
} else {

    // SEND TO CART
    Bigtree::redirect('../cart/');
    exit;

}

?>