<?php

$customer_id = 0;

// ACCOUNTS ARE ENABLED
if ($_uccms['_account']->isEnabled()) {

    // NOT LOGGED IN
    if (!$_uccms['_account']->loggedIn()) {

        $password = '';
        $account_send_email = false;
        $auto_login = false;

        // EMAIL AND PASSWORD SPECIFIED
        if (($account['email']) && ($account['password'])) {

            // TRY TO LOG IN
            $login = $_uccms['_account']->login($account['email'], $account['password'], false, false);

            // LOGIN SUCCESSFUL
            if ($login['success']) {
                $customer_id = $login['id'];
            }

        }

        // NO CUSTOMER ID YET
        if (!$customer_id) {

            // PASSWORD SPECIFIED
            if ($account['password']) {
                $password = trim($account['password']);
                $account_send_email = true;
                $auto_login = true;

            // NO PASSWORD SPECIFIED
            } else {

                // HAVE EMAIL ADDRESS
                if ($account['email']) {

                    // LOOK FOR EXISTING ACCOUNT
                    $acct_query = "SELECT `id` FROM `". $_uccms['_account']->config['db']['accounts'] ."` WHERE (`username`='" .sqlescape($account['email']). "')";
                    $acct_q = sqlquery($acct_query);
                    $acct = sqlfetch($acct_q);

                    // ACCOUNT FOUND
                    if ($acct['id']) {
                        $customer_id = $acct['id'];

                    // ACCOUNT NOT FOUND
                    } else {

                        // AUTOMATICALLY CREATING ACCOUNTS
                        if ($_uccms_ecomm->getSetting('accounts_auto_create')) {
                            $password = $_uccms['_account']->rand_string(10, true); // GENERATE PASSWORD
                            $account_send_email = $_uccms_ecomm->getSetting('accounts_auto_email');
                        }

                    }

                }

            }

            // HAVE FIELDS NEEDED
            if (($account['email']) && ($password)) {

                // ACCOUNT DETAILS
                $account_details = array(
                    'firstname' => $account['firstname'],
                    'lastname'  => $account['lastname']
                );

                // CREATE ACCOUNT
                $create = $_uccms['_account']->register($account['email'], $password, $account['email'], $account_details, $account_send_email);

                // SUCCESS
                if ($create['success'] == true) {

                    // NEW CUSTOMER (ACCOUNT) ID
                    $customer_id = $create['id'];

                    // LOGIN
                    if ($auto_login) {
                        $login = $_uccms['_account']->login($account['email'], $password, false, false);
                    }

                    $_uccms['_site-message']->set('success', 'Account created.');

                // FAILED
                } else {

                    // HAVE ERROR
                    if ($create['error']) {
                        $_uccms['_site-message']->set('error', $create['message']);
                    } else {
                        $_uccms['_site-message']->set('error', 'Failed to create account.');
                    }

                }

            }

        }

    }

}

// NO CUSTOMER ID
if (!$customer_id) {
    if ($order['customer_id']) { // HAVE FROM ORDER
        $customer_id = $order['customer_id'];
    } else if ($_uccms['_account']->userID()) { // IS LOGGED IN
        $customer_id = $_uccms['_account']->userID();
    } else {
        $customer_id = $_uccms_ecomm->customerID($vals['billing']['email']); // GENERATE
    }
}

?>