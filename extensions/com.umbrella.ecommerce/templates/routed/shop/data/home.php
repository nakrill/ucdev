<?php

// META
if ($_properties['settings']['home_meta_title']) $bigtree['page']['title'] = stripslashes($_properties['settings']['home_meta_title']);
if ($_properties['settings']['home_meta_description']) $bigtree['page']['meta_description'] = stripslashes($_properties['settings']['home_meta_description']);
if ($_properties['settings']['home_meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($_properties['settings']['home_meta_keywords']);

// PAGE LIMIT
$page_limit = $_uccms_ecomm->perPage();

// OFFSET
if ($_GET['page']) {
    $offset = ((int)$_GET['page'] - 1) * $page_limit;
} else {
    $offset = 0;
}

// SORT
if ($_REQUEST['sort']) {
    list($order_field, $order_direction) = explode('.', $_REQUEST['sort']);
}

// VARIABLES TO FIND BY
$items_vars = array(
    'vars'              => $_GET,
    'order_field'       => $order_field,
    'order_direction'   => $order_direction,
    'limit'             => $page_limit,
    'offset'            => $offset
);

if (count($_GET) > 1) {
    $items_vars['log'] = true;
}

//echo print_r($prop_vars);

// GET MATCHING ITEMS
$items = $_uccms_ecomm->getItems($items_vars);

//echo print_r($items);
//exit;

// HAVE RESULTS
if ($items['num_results'] > 0) {

    // PAGING CLASS
    require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

    // INIT PAGING CLASS
    $_paging = new paging();

    // OPTIONAL - URL VARIABLES TO IGNORE
    $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

    // PREPARE PAGING
    $_paging->prepare($page_limit, 10, $_GET['page'], $_GET);

    // GENERATE PAGING
    $pages = $_paging->output($items['num_results'], $items['num_total']);

}

// DISPLAY FORMAT
$display_style = 'grid';

?>