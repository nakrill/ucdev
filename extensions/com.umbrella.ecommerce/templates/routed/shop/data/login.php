<?php

// ACCOUNTS ARE ENABLED
if ($_uccms['_account']->isEnabled()) {

    // ALREADY LOGGED IN
    if ($_uccms['_account']->loggedIn()) {
        BigTree::redirect('../checkout/persons/');

    // NOT LOGGED IN
    } else {

        // CLEAN UP
        $username = trim($_POST['email']);
        $password = trim($_POST['password']);

        // HAVE USERNAME AND PASSWORD
        if (($username) && ($password)) {

            // LOGIN
            $login = $_uccms['_account']->login($username, $password, $_REQUEST['remember'], false);

            // LOGIN SUCCESSFUL
            if ($login['success'] == true) {

                $_uccms['_site-message']->set('success', 'You have been logged in.');

                BigTree::redirect('../checkout/persons/');

            // LOGIN FAILED
            } else {

                // HAVE ERROR
                if ($login['error']) {

                    $_uccms['_site-message']->set('error', $login['message']);

                    // TRIES REMAINING SPECIFIED
                    if ($login['tries_remaining']) {
                        $_uccms['_site-message']->set('error', 'You have <strong>' .$login['tries_remaining']. ' tries remaining</strong>.');
                    }

                    // TEMPORARY SUSPENSION
                    if ($login['error'] == 5) {
                        $_uccms['_site-message']->set('error', 'Please wait <strong>' .$login['minutes']. ' minutes</strong> to try again.');
                    }

                // GENERAL ERROR
                } else {
                    $_uccms['_site-message']->set('error', 'Failed to log in.');
                }

            }

        }

    }

// ACCOUNTS ARE NOT ENABLED
} else {
    BigTree::redirect('../checkout/persons/');
}

?>