<?php

// INITIALIZE CART
$cart = $_uccms_ecomm->initCart();

// HAVE CART MESSAGE
if ($_SESSION['ecomm']['cart']['message']) {
    $msg = $_SESSION['ecomm']['cart']['message'];
    unset($_SESSION['ecomm']['cart']['message']);
}

// HAVE CART ID
if ($cart['id']) {

    // IS ADDING ITEM
    if (($_REQUEST['do'] == 'add') && ($_REQUEST['item']['id'])) {
        $result = $_uccms_ecomm->cartAddItem($cart['id'], $_REQUEST);
        if ($result['success']) {
            $_uccms['_site-message']->set('success', $result['message']);
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-add'] = trim(stripslashes($result['item']['title']). ' (#' .$result['item']['id']. ')');
            }
        } else {
            $_uccms['_site-message']->set('error', $result['message']);
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-add-error'] = $result['message'];
            }
        }
        BigTree::redirect('./');
        exit;
    }

    // IS UPDATING QUANTITY
    if (($_REQUEST['do'] == 'quantity') && ($_REQUEST['id'])) {
        $result = $_uccms_ecomm->cartItemQuantity($_REQUEST['id'], $_REQUEST['quantity']);
        if ($result['success']) {
            $_uccms['_site-message']->set('success', $result['message']);
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-quantity-change'] = trim(stripslashes($result['item']['title']). ' (#' .$result['item']['id']. ') - ' .(int)$_REQUEST['quantity']);
            }
        } else {
            $_uccms['_site-message']->set('error', $result['message']);
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-quantity-error'] = $result['message'];
            }
        }
    }

    // IS REMOVING ITEM
    if (($_REQUEST['do'] == 'remove') && ($_REQUEST['id'])) {
        $result = $_uccms_ecomm->cartRemoveItem($_REQUEST['id']);
        if ($result['success']) {
            $_uccms['_site-message']->set('success', $result['message']);
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-remove'] = trim(stripslashes($result['item']['title']). ' (#' .$result['item']['id']. ')');
            }
        } else {
            $_uccms['_site-message']->set('error', $result['message']);
            if ($result['item']['id']) {
                $_SESSION['uccmsAcct_webTracking']['events']['cart-remove-error'] = $result['message'];
            }
        }
    }

    // GET CART ITEMS
    $citems = $_uccms_ecomm->cartItems($cart['id']);

    // NUMBER OF ITEMS
    $num_items = count($citems);

    // PURCHASING ENABLED / DISABLED
    $purchasing_enabled = $_uccms_ecomm->purchasingEnabled();

}

?>