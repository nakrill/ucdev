<?php

// HAVE ITEM ID
if ($oitem['item_id']) {

    // CUSTOM ITEM INFO
    if (file_exists(dirname(__FILE__). '/item_cart-custom.php')) {
        include(dirname(__FILE__). '/item_cart-custom.php');

    // NORMAL ITEM INFO
    } else {

        // GET ITEM
        $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$oitem['item_id']. ") AND (`deleted`=0)";
        $item_q = sqlquery($item_query);
        $item = sqlfetch($item_q);

        // ITEM FOUND
        if ($item['id']) {

            unset($booking, $booking_item, $booking_duration);

            // STORE TYPE IS BOOKING AND ITEM IS BOOKABLE
            if (($_uccms_ecomm->storeType() == 'booking') && ($item['booking_enabled'])) {

                // ITEM CAN BE BOOKED
                if ($item['booking_enabled']) {

                    // GET BOOKING ITEM INFO
                    $booking_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
                    $booking_item_q = sqlquery($booking_item_query);
                    $booking_item = sqlfetch($booking_item_q);

                    // GET BOOKING INFO
                    $booking_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$oitem['id']. ")";
                    $booking_q = sqlquery($booking_query);
                    $booking = sqlfetch($booking_q);

                    // HAVE BOOKING ID
                    if ($booking['duration_id']) {

                        // GET DURATION
                        $booking_duration_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_item_durations']. "` WHERE (`id`=" .$booking['duration_id']. ")";
                        $booking_duration_q = sqlquery($booking_duration_query);
                        $booking_duration = sqlfetch($booking_duration_q);

                    }

                }

            }

            // LINK
            $item['other']['link'] = $_uccms_ecomm->itemURL($item['id'], (int)$oitem['category_id'], $item);

            // EDIT LINK
            $item['other']['link_edit'] = $item['other']['link']. '?ciid=' .$oitem['id'];

            // GET IMAGE
            $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC LIMIT 1";
            $image = sqlfetch(sqlquery($image_query));

            // HAVE IMAGE
            if ($image['image']) {
                $item['other']['image_url'] = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
            } else {
                $item['other']['image_url'] = WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg';
            }

            // BASE ATTRIBUTE ARRAY
            if (!$attra) $attra = array();

            // HAVE PRICE OVERRIDE
            if (($oitem['override_price']) || ($oitem['override_price'] == '0.00')) {
                $item['other']['sale']  = false;
                $item['other']['price'] = $oitem['override_price'];

            // NORMAL PRICING
            } else {

                // ON SALE
                if ($item['price_sale'] != '0.00') {
                    $item['other']['sale']  = true;
                    $item['other']['price'] = $item['price_sale'];

                // NOT ON SALE
                } else {
                    $item['other']['sale']  = false;
                    if (defined('BIGTREE_ADMIN_ROUTED')) { // IS IN ADMIN
                        $item['other']['price'] = $_uccms_ecomm->itemPrice($item, $oitem['pricegroup_id']);
                    } else {
                        $item['other']['price'] = $_uccms_ecomm->itemPrice($item);
                    }
                }

            }

            // BOOKING DURATION MARKUP EXTRA
            if (($booking['markup']) && ($oitem['override_price'] != '0.00')) {
                $item['other']['price'] += $booking['markup'];
            }

            // GET ITEM ATTRIBUTES & PRICING
            $attr_info = $_uccms_ecomm->cartItemAttributes($oitem, $attra);

            // ADD ATTRIBUTES TO ATTRIBUTE ARRAY
            if (is_array($attr_info['attra'])) {
                foreach ($attr_info['attra'] as $attr_id => $attr) {
                    $attra[$attr_id] = $attr;
                }
            }

            // ITEM VALUES
            $item['other']['options']       = $attr_info['item_options'];
            $item['other']['attributes']    = $attra;
            $item['other']['price']         += $attr_info['item_markup'];

            // PACKAGE
            if ($item['type'] == 3) {

                // LOOP THROUGH PACKAGE ITEMS
                foreach ($_uccms_ecomm->package_getItems($item['id']) as $pitem) {

                    $piout = $pitem;

                    // GET ITEM
                    $pitem_query = "SELECT `id`, `title` FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$pitem['item_id']. ")";
                    $pitem_q = sqlquery($pitem_query);
                    $piout['item'] = sqlfetch($pitem_q);

                    // ATTRIBUTES ARRAY
                    $pattra = $_uccms_ecomm->itemAttributes($pitem['item_id']);

                    // VARIANT SPECIFIED
                    if ($pitem['variant_id']) {

                        // GET VARIANT INFO
                        $var_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_variants']. "` WHERE (`id`=" .$pitem['variant_id']. ")";
                        $var_q = sqlquery($var_query);
                        $var = sqlfetch($var_q);

                        // VARIANT ATTRS
                        $pitem['options'] = $_uccms_ecomm->item_attrsFromVariantCode($var['code']);

                    // NO VARIANT SPECIFIED (ALLOWING CUSTOMER TO CHOOSE)
                    } else {

                        $pitem['options'] = $item['other']['options'][$pitem['id']];

                    }

                    // GET ITEM ATTRIBUTES & PRICING
                    $attr_info = $_uccms_ecomm->cartItemAttributes($pitem, $pattra);

                    // ADD ATTRIBUTE MARKUP TO ITEM PRICE
                    $item['other']['price'] += $attr_info['item_markup'];

                    $piout['other']['options'] = $pitem['options'];

                    // ITEM HAS ATTRIBUTES
                    if (count($pattra) > 0) {
                        foreach ($pattra as $tpattr) {
                            if ($tpattr['options']) {
                                $tpattr['options'] = json_decode(stripslashes($tpattr['options']), true);
                            }
                            $piout['other']['attributes'][$tpattr['id']] = $tpattr;
                            $piout['other']['attribute_titles'][$tpattr['id']]['title'] = $tpattr['title'];
                        }
                        unset($tpattr);
                    }

                    $item['package']['items'][$pitem['id']] = $piout;

                }

                unset($pitem);
                unset($piout);

            }

            // QUOTE ITEM AND NOT IN ADMIN OR REVIEWING
            if (($item['quote']) && ((!defined('BIGTREE_ADMIN_ROUTED')) && (!$extended_view))) {

                $have_quote_items = true;

                // FORMAT
                $item['other']['price'] = '';
                $item['other']['total'] = '';
                $item['other']['price_formatted'] = '';
                $item['other']['total_formatted'] = 'Quote required';

            // NORMAL ITEM
            } else {

                // TOTAL
                $item['other']['total'] = $oitem['quantity'] * $item['other']['price'];

                // IS AFTER TAX ITEM
                if ($item['after_tax']) {

                    // ORDER AFTERTAX
                    $order['aftertax'] += $item['other']['total'];

                // IS NORMAL ITEM
                } else {

                    // ORDER SUBTOTAL
                    $order['subtotal'] += $item['other']['total'];

                }

                // FORMAT
                $item['other']['price'] = number_format($item['other']['price'], 2);
                $item['other']['total'] = number_format($item['other']['total'], 2);
                $item['other']['price_formatted'] = '$<span class="num">' .$item['other']['price']. '</span>';
                $item['other']['total_formatted'] = '$<span class="num">' .$item['other']['total']. '</span>';

            }

        }

    }

}

?>