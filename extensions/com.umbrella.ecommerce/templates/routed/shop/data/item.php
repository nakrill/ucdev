<?php

// https://developers.google.com/structured-data/rich-snippets/products?hl=en&rd=1
// https://developers.google.com/structured-data/testing-tool/

// CLEAN UP
$item_id = (int)$item_id;

// HAVE ITEM ID
if ($item_id > 0) {

    // GET ITEM
    $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item_id. ") AND (`active`=1) AND (`deleted`=0)";
    $item_q = sqlquery($item_query);
    $item = sqlfetch($item_q);

}

// HAVE ITEM
if ($item['id']) {

    // TRACK ITEM VIEW
    $_SESSION['uccmsAcct_webTracking']['events']['item-view'] = trim(stripslashes($item['title']). ' (#' .$item['id']. ')');

    // GET BOOKING ITEM INFO
    $booking_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
    $booking_item_q = sqlquery($booking_item_query);
    $booking_item = sqlfetch($booking_item_q);

    // CUSTOM EDIT LINK
    $bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/items/edit/?id=' .$item['id'];

    // ITEM SLUG
    if ($item['slug']) {
        $slug = stripslashes($item['slug']);
    } else {
        $slug = $_uccms_ecomm->makeRewrite(stripslashes($item['title']));
    }

    // SLUGS DON'T MATCH - 301 TO CURRENT
    if ($bigtree['path'][2] != $slug. '-' .$item['id']) {

        $catgory_id = 0;

        // IS IN CATEGORY
        if ($bigtree['path'][1] != 'item') {

            // GET LAST PATH PARTS
            $ppa = explode('-', $bigtree['path'][1]);

            // HAS NUMBER ON END - IS CATEGORY
            if (is_numeric($ppa[count($ppa) - 1])) {
                $category_id = $ppa[count($ppa) - 1];
            }
        }

        // 301 REDIRECT TO CURRENT ITEM URL
        header('Location: ' .$_uccms_ecomm->itemURL($item['id'], $category_id, $item), true, 301);
        exit;

    }

    // META
    if ($item['meta_title']) {
        $bigtree['page']['title'] = stripslashes($item['meta_title']);
    } else {
        $bigtree['page']['title'] = stripslashes($item['title']);
    }
    if ($item['meta_description']) {
        $bigtree['page']['meta_description'] = stripslashes($item['meta_description']);
    } else if ($item['description']) {
        $bigtree['page']['description'] = stripslashes($item['description']);
    }
    if ($item['meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($item['meta_keywords']);

    // CANONICAL URL
    $bigtree['page']['canonical'] = $_uccms_ecomm->itemURL($item['id'], 0, $item);

    // CATEGORY PARENT(S) ARRAY
    $catpa = array();

    // CATEGORY MASTER PARENT ID
    $category_master_parent_id = 0;

    // IS IN CATEGORY
    if ($bigtree['path'][1] != 'item') {

        // GET LAST PATH PARTS
        $ppa = explode('-', $bigtree['path'][1]);

        // HAS NUMBER ON END - IS CATEGORY
        if (is_numeric($ppa[count($ppa) - 1])) {
            $category_id = $ppa[count($ppa) - 1];
        }

        // CATEGORY PARENT(S) ARRAY
        $catpa = $_uccms_ecomm->getCategoryParents($category_id);

        // CATEGORY MASTER PARENT ID
        $category_master_parent_id = $catpa[count($catpa) - 1]['id'];

    }

    // IMAGE ARRAY
    $imagea = array();

    $ii = 0;

    // GET IMAGES
    $image_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_images']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC";
    $image_q = sqlquery($image_query);
    while ($image = sqlfetch($image_q)) {
        if ($image['image']) {
            $imagea[$ii] = $image;
            $imagea[$ii]['url'] = $_uccms_ecomm->imageBridgeOut($image['image'], 'items');
            $imagea[$ii]['url_thumb'] = BigTree::prefixFile($_uccms_ecomm->imageBridgeOut($image['image'], 'items'), 't_');
            $ii++;
        }
    }

    // VIDEO ARRAY
    $videoa = array();

    $ii = 0;

    // GET VIDEOS
    $video_query = "SELECT * FROM `" .$_uccms_ecomm->tables['item_videos']. "` WHERE (`item_id`=" .$item['id']. ") ORDER BY `sort` ASC";
    $video_q = sqlquery($video_query);
    while ($video = sqlfetch($video_q)) {
        if ($video['video']) {
            $videoa[$ii] = $video;
            $videoa[$ii]['info'] = videoInfo(stripslashes($video['video']));
            $videoa[$ii]['url'] = stripslashes($video['video']);
            $videoa[$ii]['url_thumb'] = $videoa[$ii]['info']['thumb'];
            $ii++;
        }
    }

    // ON SALE
    if ($item['price_sale'] != '0.00') {
        $sale = true;
        $price = $item['price_sale'];

    // NOT ON SALE
    } else {
        $sale = false;
        $price = $_uccms_ecomm->itemPrice($item);
    }

    // PURCHASING ENABLED / DISABLED
    $purchasing_enabled = $_uccms_ecomm->purchasingEnabled($item);

    // MINIMUM QUANTITY
    $qty_min = $_uccms_ecomm->itemQtyMin($item);

    // ATTRIBUTES ARRAY
    $attra = $_uccms_ecomm->itemAttributes($item['id']);

    // IS EDITING ITEM FROM THE CART
    if ($_REQUEST['ciid']) {

        // GET ATTRIBUTES & QUANTITY FROM CART ENTRY
        $ci_query = "SELECT * FROM `" .$_uccms_ecomm->tables['order_items']. "` WHERE (`id`=" .(int)sqlescape($_REQUEST['ciid']). ") AND (`cart_id`=" .(int)$_uccms_ecomm->cartID(). ")";
        $ci_q = sqlquery($ci_query);
        $ci = sqlfetch($ci_q);

        // HAVE CART ITEM
        if ($ci['id']) {

            // ATTRIBUTES
            $selattra = json_decode(stripslashes($ci['options']), true);

            // QUANTITY
            if ($ci['quantity']) {
                $_REQUEST['quantity'] = $ci['quantity'];
            }

            // GET BOOKING INFO
            $booking_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_bookings']. "` WHERE (`oitem_id`=" .$ci['id']. ")";
            $booking_q = sqlquery($booking_query);
            $booking = sqlfetch($booking_q);

        }

    // HAVE SELECTED ATTRIBUTES FROM URL
    } else if ($_REQUEST['options']) {
        parse_str(urldecode($_REQUEST['options']), $selattra);

    // HAVE SELECTED ATTRIBUTES FROM SESSION
    } else if ($_SESSION['ecomm']['item']['attributes']) {
        $selattra = $_SESSION['ecomm']['item']['attributes'];
        unset($_SESSION['ecomm']['item']['attributes']);
    }

// ITEM NOT FOUND
} else {
    header('HTTP/1.0 404 Not Found');
    $bigtree['page']['title'] = 'Item Not Found';
    $bigtree['page']['meta_description'] = '';
    $bigtree['page']['meta_keywords'] = '';
}

?>