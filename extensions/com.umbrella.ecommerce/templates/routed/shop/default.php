<?php

// MODULE CLASS
$_uccms_ecomm = new uccms_Ecommerce;

// GET SETTING(S)
$ecomm['settings'] = $_uccms_ecomm->getSettings();

// SECTIONS DIRECTORY
$sections_dir = dirname(__FILE__). '/sections';

// DATA DIRECTORY
$data_dir = dirname(__FILE__). '/data';

// NUMBER OF LEVELS IN URL PATH
$path_num = count($bigtree['path']);

// WIDGET
if ($bigtree['path'][1] == 'widget') {
    include($sections_dir. '/widgets/default.php');
}

// ICAL
if ($bigtree['path'][1] == 'ical') {

    // ADMIN ICAL
    if ($bigtree['path'][2] == 'admin') {
        include($sections_dir. '/ical/admin.php');

    // CALENDAR ICAL
    } else if ($bigtree['path'][2] == 'calendar') {
        include($sections_dir. '/ical/calendar.php');
    }

}

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/master.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/master.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/master.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/master.js"></script>

<section id="ecommContainer">
    <div class="container">

        <?php

        // STORE ENABLED
        if ($ecomm['settings']['storefront_enabled']) {

            // HOME
            if ($path_num == 1) {
                include($data_dir. '/home.php');
                include($sections_dir. '/home/default.php');

            // ONE LEVEL DEEP
            } else if ($path_num == 2) {

                // CART PAGE
                if ($bigtree['path'][1] == 'cart') {
                    include($data_dir. '/cart.php');
                    include($sections_dir. '/cart/default.php');

                // LOGIN PAGE
                } else if ($bigtree['path'][1] == 'login') {
                    include($data_dir. '/login.php');
                    include($sections_dir. '/login/default.php');

                // CHECKOUT PAGE
                } else if ($bigtree['path'][1] == 'checkout') {
                    include($data_dir. '/checkout.php');
                    include($sections_dir. '/checkout/default.php');

                // QUOTE SECTION
                } else if ($bigtree['path'][1] == 'quote') {
                    include($sections_dir. '/quote/default.php');

                } else {

                    // GET LAST PATH PARTS
                    $ppa = explode('-', $bigtree['path'][1]);

                    // HAS NUMBER ON END - IS CATEGORY
                    if (is_numeric($ppa[count($ppa) - 1])) {
                        $category_id = $ppa[count($ppa) - 1];
                        include($data_dir. '/category.php');
                        include($sections_dir. '/category/default.php');
                    }

                }

            // TWO LEVELS DEEP
            } else if ($path_num == 3) {

                // GET LAST PATH PARTS
                $ppa = explode('-', $bigtree['path'][2]);

                // HAS NUMBER ON END - IS ITEM
                if (is_numeric($ppa[count($ppa) - 1])) {
                    $item_id = $ppa[count($ppa) - 1];
                    include($data_dir. '/item.php');
                    include($sections_dir. '/item/default.php');

                // IS CART
                } else if ($bigtree['path'][1] == 'cart') {

                    // PROCESS
                    if ($bigtree['path'][2] == 'process') {
                        include($sections_dir. '/cart/process.php');
                    }

                // IS CHECKOUT
                } else if ($bigtree['path'][1] == 'checkout') {

                    // AGREE
                    if ($bigtree['path'][2] == 'agree') {
                        include($sections_dir. '/checkout/agree.php');

                    // AGREE - PROCESS
                    } else if ($bigtree['path'][2] == 'agree-process') {
                        include($sections_dir. '/checkout/agree-process.php');

                    // PERSONS
                    } else if ($bigtree['path'][2] == 'persons') {
                        include($sections_dir. '/checkout/persons.php');

                    // AGREE - PROCESS
                    } else if ($bigtree['path'][2] == 'persons-process') {
                        include($sections_dir. '/checkout/persons-process.php');

                    // PROCESS
                    } else if ($bigtree['path'][2] == 'process') {
                        include($sections_dir. '/checkout/process.php');

                    // THANKS
                    } else if ($bigtree['path'][2] == 'thanks') {
                        include($sections_dir. '/checkout/thanks.php');
                    }

                // IS ORDER
                } else if ($bigtree['path'][1] == 'order') {

                    // REVIEW
                    if ($bigtree['path'][2] == 'review') {
                        include($data_dir. '/quote_review.php');
                        include($sections_dir. '/order/review/default.php');
                    }

                // IS QUOTE
                } else if ($bigtree['path'][1] == 'quote') {

                    // REQUEST
                    if ($bigtree['path'][2] == 'request') {
                        include($data_dir. '/quote_request.php');
                        include($sections_dir. '/quote/request/default.php');

                    // REVIEW
                    } else if ($bigtree['path'][2] == 'review') {
                        include($data_dir. '/quote_review.php');
                        include($sections_dir. '/quote/review/default.php');
                    }

                }

            // FOUR LEVELS DEEP
            } else if ($path_num == 4) {

                // IS ORDER
                if ($bigtree['path'][1] == 'order') {

                    // REVIEW
                    if ($bigtree['path'][2] == 'review') {

                        // PROCESS PAYMENT
                        if ($bigtree['path'][3] == 'process-payment') {
                            include($sections_dir. '/order/review/process-payment.php');
                        }

                    }

                // IS QUOTE
                } else if ($bigtree['path'][1] == 'quote') {

                    // REQUEST
                    if ($bigtree['path'][2] == 'request') {

                        // REVIEW
                        if ($bigtree['path'][3] == 'review') {
                            include($data_dir. '/quote_request.php');
                            include($sections_dir. '/quote/request/review.php');

                        // PROCESS
                        } else if ($bigtree['path'][3] == 'process') {
                            include($data_dir. '/quote_request.php');
                            include($sections_dir. '/quote/request/process.php');

                        // THANKS
                        } else if ($bigtree['path'][3] == 'thanks') {
                            include($sections_dir. '/quote/request/thanks.php');
                        }

                    // REVIEW
                    } else if ($bigtree['path'][2] == 'review') {

                        // STATUS
                        if ($bigtree['path'][3] == 'status') {
                            include($sections_dir. '/quote/review/status.php');

                        // PROCESS PAYMENT
                        } else if ($bigtree['path'][3] == 'process-payment') {
                            include($sections_dir. '/quote/review/process-payment.php');
                        }

                    }

                }

            }

        // STORE DISABLED
        } else {

            // IS SUB PAGE
            if ($path_num > 1) {
                header('Location: ' .WWW_ROOT . $bigtree['path'][0]. '/', true, 302);
                exit;
            }

            ?>

            <div class="storefront_disabled">
                <?php
                if (!$ecomm['settings']['store_disabled_content']) $ecomm['settings']['store_disabled_content'] = 'Storefront disabled.';
                echo stripslashes($ecomm['settings']['store_disabled_content']);
                ?>
            </div>

        <?php } ?>

    </div>
</section>