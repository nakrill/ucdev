<?php

// META
if ($ecomm['settings']['home_meta_title']) $bigtree['page']['title'] = stripslashes($ecomm['settings']['home_meta_title']);
if ($ecomm['settings']['home_meta_description']) $bigtree['page']['meta_description'] = stripslashes($ecomm['settings']['home_meta_description']);
if ($ecomm['settings']['home_meta_keywords']) $bigtree['page']['meta_keywords'] = stripslashes($ecomm['settings']['home_meta_keywords']);

?>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/home.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/home.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/home.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/home.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="row">

    <div class="smallColumn col-md-3">

        <?php //include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/cart_widget.php'); ?>
        <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/category_nav.php'); ?>

    </div>

    <div class="largeColumn col-md-9">

    <?php

        // HOME PAGE
        if ((count($bigtree['path']) == 1) && (count($_GET) <= 1)) {

            // HAVE CONTENT
            if ($ecomm['settings']['home_content']) {
                ?>
                <div class="content">
                    <?php echo stripslashes($ecomm['settings']['home_content']); ?>
                </div>
                <?php
            }

            // DISPLAYING FEATURED
            if ((int)$ecomm['settings']['home_featured_num'] > 0) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/featured_items.php');
            }

            // DISPLAYING RECENT
            if ((int)$ecomm['settings']['home_recent_num'] > 0) {
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/recent_items.php');
            }

        // SEARCHING / FILTERING
        } else {

            ?>

            <div class="search-results">

                <?php

                // HAVE ITEMS
                if (count($items['results']) > 0) {

                    ?>

                    <div class="head row">

                        <div class="heading col-md-4">
                            <h3><?php echo number_format($items['num_total'], 0); ?> Item<?php if ($items['num_total'] != 1) { echo 's'; } ?></h3>
                        </div>

                        <div class="paging col-md-5 col-xs-6">

                            <?php

                            // HAVE PAGES
                            if ($pages) {
                                ?>

                                <div class="paging">
                                    <?php echo $pages; ?>
                                </div>
                                <?php

                            }

                            ?>

                        </div>

                    </div>

                    <div class="items">

                        <div class="item_container row">

                            <?php

                            // LOOP
                            foreach ($items['results'] as $item) {

                                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item.php');

                            }

                            ?>

                        </div>

                    </div>

                    <?php if ($pages) { ?>
                        <div class="paging bottom">
                            <?php echo $pages; ?>
                        </div>
                    <?php } ?>

                    <?php

                // NO LISTINGS
                } else {

                    ?>

                    <div class="head row">

                        <div class="heading col-md-3">
                            <h3>0 Items</h3>
                        </div>

                    </div>

                    <div class="no-results">

                        No results.

                    </div>

                    <?php

                }

                ?>

            </div>

            <?php

        }

        ?>

    </div>

</div>