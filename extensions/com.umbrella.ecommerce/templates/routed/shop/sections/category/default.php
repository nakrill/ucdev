<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/category.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/category.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/category.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/category.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="smallColumn">

    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/cart_widget.php'); ?>
    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/category_nav.php'); ?>

</div>

<div class="largeColumn withSmallColumn category_container">

    <?php

    // HAVE CATEGORY
    if ($category['id']) {

        // BREADCRUMB OPTIONS
        $bcoptions = array(
            'base'      => WWW_ROOT . $bigtree['path'][0]. '/',
            'rewrite'   => true
        );

        // BREADCRUMBS
        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/breadcrumbs.php');

        // HAVE INFO FOR CATEGORY HEAD
        if (($category['image']) || ($category['title']) || ($category['description'])) {

            ?>

            <div class="category_head contain">

                <?php

                // HAVE IMAGE
                if ($category['image']) {
                    ?>
                    <img src="<?php echo $_uccms_ecomm->imageBridgeOut($category['image'], 'categories'); ?>" alt="<?php echo stripslashes($category['title']); ?>" />
                    <?php
                }

                // HAVE TITLE
                if ($category['title']) {
                    ?>
                    <h1><?php echo stripslashes($category['title']); ?></h1>
                    <?php
                }

                // HAVE DESCRIPTION
                if ($category['description']) {
                    ?>
                    <div class="description">
                        <?php echo nl2br(stripslashes($category['description'])); ?>
                    </div>
                    <?php
                }

                ?>

            </div>

            <?php

        }

        // HAVE SUB-CATEGORIES
        if (count($sub_categories) > 0) {

            ?>

            <div class="sub_categories">
                <h3>Sub-Categories</h3>
                <div class="contain">
                    <?php foreach ($sub_categories as $sub) { ?>
                        <div class="sub">
                            <a href="<?php echo $_uccms_ecomm->categoryURL($sub['id'], $sub); ?>"><?php echo stripslashes($sub['title']); ?></a>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <?php

        }

        // HAVE ITEMS
        if ($num_items > 0) {

            ?>

            <div class="paging sort">
                <form>
                <table style="width: 100%; border: 0px;">
                    <tr>
                        <td width="50%" style="vertical-align: middle;">
                            Sort by: <select name="sort">
                                <option value="title" <?php if ($_REQUEST['sort'] == 'title') { ?>selected="selected"<?php } ?>>Title</option>
                                <option value="price" <?php if ($_REQUEST['sort'] == 'price') { ?>selected="selected"<?php } ?>>Price</option>
                            </select>
                        </td>
                        <td width="50%" style="text-align: right;">
                            <?php echo $pages; ?>
                        </td>
                    </tr>
                </table>
                </form>
            </div>

            <div class="items">

                <div class="item_container contain">

                    <?php

                    // LOOP
                    foreach ($category_items as $item) {
                        $item['category_id']    = $category['id'];
                        $item['category_slug']  = stripslashes($category['slug']);
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item.php');
                    }

                    ?>

                </div>

            </div>

            <div class="paging bottom">
                <?php echo $pages; ?>
            </div>

            <?php

        }

    // CATEGORY NOT FOUND
    } else {
        ?>
        Category not found.
        <?php
    }

    ?>

</div>