<?php

// IF SSL IS ENABLED - FORCE IT ON THIS PAGE
if ($_uccms_ecomm->getSetting('ssl_enabled')) {
    $cms->makeSecure();
}

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<h3>Checkout</h3>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/checkout.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/checkout.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/checkout.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/checkout.js"></script>

<div id="hv_shipping_service" style="display: none;"><?php echo $vals['order']['shipping_service']; ?></div>

<form action="./process/" method="post">
<input type="hidden" name="do" value="process" />

<div class="contain" position="relative">

    <div class="half left">

        <div class="billing_info section_padding">

            <h4>Billing</h4>

            <div class="contain box">

                <div style="padding-bottom: 10px;">
                    Fields marked with * required.
                </div>

                <fieldset class="billing_firstname">
                    <label>First Name *</label>
                    <input type="text" name="billing[firstname]" value="<?php echo $vals['billing']['firstname']; ?>" />
                </fieldset>

                <fieldset class="billing_lastname">
                    <label>Last Name *</label>
                    <input type="text" name="billing[lastname]" value="<?php echo $vals['billing']['lastname']; ?>" />
                </fieldset>

                <fieldset class="billing_company">
                    <label>Company Name</label>
                    <input type="text" name="billing[company]" value="<?php echo $vals['billing']['company']; ?>" />
                </fieldset>

                <fieldset class="billing_address1">
                    <label>Address *</label>
                    <input type="text" name="billing[address1]" value="<?php echo $vals['billing']['address1']; ?>" />
                </fieldset>

                <fieldset class="billing_address2">
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="billing[address2]" value="<?php echo $vals['billing']['address2']; ?>" />
                </fieldset>

                <fieldset class="billing_city">
                    <label>City *</label>
                    <input type="text" name="billing[city]" value="<?php echo $vals['billing']['city']; ?>" />
                </fieldset>

                <fieldset class="billing_state">
                    <label>State *</label>
                    <select name="billing[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $vals['billing']['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="billing_zip">
                    <label>Zip *</label>
                    <input type="text" name="billing[zip]" value="<?php echo $vals['billing']['zip']; ?>" />
                </fieldset>

                <fieldset class="billing_phone">
                    <label>Phone</label>
                    <input type="text" name="billing[phone]" value="<?php echo $vals['billing']['phone']; ?>" />
                </fieldset>

                <fieldset class="billing_email">
                    <label>Email *</label>
                    <input type="text" name="billing[email]" value="<?php echo $vals['billing']['email']; ?>" />
                </fieldset>

            </div>

        </div>

    </div>

    <div class="half left">

        <div class="shipping_info section_padding">

            <h4>Shipping</h4>

            <div class="contain box">

                <div class="shipping_same" style="padding-bottom: 10px;">
                    <input type="checkbox" name="order[shipping_same]" value="1" <?php if ($vals['order']['shipping_same']) { ?>checked="checked"<?php } ?> /> Same as billing
                </div>

                <div id="toggle_shipping_info" style="<?php if ($vals['order']['shipping_same']) { ?>display: none;<?php } ?>">

                    <fieldset class="shipping_firstname">
                        <label>First Name *</label>
                        <input type="text" name="shipping[firstname]" value="<?php echo $vals['shipping']['firstname']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_lastname">
                        <label>Last Name *</label>
                        <input type="text" name="shipping[lastname]" value="<?php echo $vals['shipping']['lastname']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_company">
                        <label>Company Name</label>
                        <input type="text" name="shipping[company]" value="<?php echo $vals['shipping']['company']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_address1">
                        <label>Address *</label>
                        <input type="text" name="shipping[address1]" value="<?php echo $vals['shipping']['address1']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_address2">
                        <label>Address (Apt, Bldg)</label>
                        <input type="text" name="shipping[address2]" value="<?php echo $vals['shipping']['address2']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_city">
                        <label>City *</label>
                        <input type="text" name="shipping[city]" value="<?php echo $vals['shipping']['city']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_state">
                        <label>State *</label>
                        <select name="shipping[state]">
                            <option value="">Select</option>
                            <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                <option value="<?php echo $state_code; ?>" <?php if ($state_code == $vals['shipping']['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset class="shipping_zip">
                        <label>Zip *</label>
                        <input type="text" name="shipping[zip]" value="<?php echo $vals['shipping']['zip']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_phone">
                        <label>Phone</label>
                        <input type="text" name="shipping[phone]" value="<?php echo $vals['shipping']['phone']; ?>" />
                    </fieldset>

                    <fieldset class="shipping_email">
                        <label>Email *</label>
                        <input type="text" name="shipping[email]" value="<?php echo $vals['shipping']['email']; ?>" />
                    </fieldset>

                </div>

            </div>

        </div>

    </div>

</div>

<?php

// ACCOUNTS ARE ENABLED
if ($_uccms['_account']->isEnabled()) {

    // NOT LOGGED IN
    if (!$_uccms['_account']->loggedIn()) {

        ?>

        <div class="create_account section_padding">

            <h4>Account</h4>

            <div class="contain box">

                <p>
                    <strong>Save time later.</strong> Specify a password and we'll automatically create an account for you for faster checkout and easy access to order history.
                </p>

                <fieldset class="account_password">
                    <label>Password</label>
                    <input type="password" name="account[password]" value="" />
                </fieldset>

            </div>

        </div>

        <?php

    }

}

?>

<div class="shipping_method section_padding">

    <h4>Shipping Method</h4>

    <div class="contain box">
        Loading..
    </div>

</div>

<div class="cart section_padding">

    <h4>Cart</h4>

    <div class="contain box">

        <table id="cart" cellpadding="0" cellspacing="0">

            <tr>
                <th class="thumb"></th>
                <th class="title">Item</th>
                <th class="price">Price</th>
                <th class="quantity">Quantity</th>
                <th class="total">Total</th>
            </tr>

            <?php

            // SUBTOTAL
            $order['subtotal'] = 0;

            // ATTRIBUTE ARRAY
            $attra = array();

            // AFTER TAX ITEM ARRAY
            $atia = array();

            // LOOP THROUGH ITEMS
            foreach ($citems as $oitem) {

                // AFTER TAX ITEM
                if ($oitem['after_tax']) {

                    // ADD TO AFTER TAX ITEM ARRAY
                    $atia[] = $oitem;

                // NORMAL ITEM
                } else {

                    // GET ITEM INFO
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                    // SETTINGS FOR DISPLAYING ITEM IN CART
                    $item_cart_settings = array();

                    // DISPLAY ITEM IN CART
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                    unset($item, $oitem);

                }

            }

            // ADD VALUES TO ORDER ARRAY
            $order['tax']       = $tax['total'];
            //$order['shipping']  = $shipping['markup'];

            $cart['order_subtotal'] = $order['subtotal'];

            // GET DISCOUNT
            $discount = $_uccms_ecomm->cartDiscount($cart, $citems);

            ?>

            <tr class="sub right subtotal <?php if (!$discount['discount']) { ?>bold<?php } ?>">
                <td colspan="5">
                    Subtotal: $<span class="num"><?php echo number_format($order['subtotal'], 2); ?></span>
                </td>
            </tr>

            <?php

            // HAVE DISCOUNT
            if ($discount['discount']) {
                $order['discount'] = $discount['discount'];
                $t_tax = $order['tax'];
                unset($order['tax']);
                ?>
                <tr class="sub right pre">
                    <td colspan="5">
                        After discount (<?php if ($discount['coupon']['code']) { ?>Coupon: <?php echo $discount['coupon']['code']; ?> - <?php } ?>$<span class="num"><?php echo number_format($discount['discount'], 2); ?></span>): $<?php echo $_uccms_ecomm->orderTotal($order); ?>
                    </td>
                </tr>
                <?php
                $order['tax'] = $t_tax;
                $order['discount'] = $discount['discount'];
            }

            ?>

            <tr class="sub right tax">
                <td colspan="5">
                    Tax: $<span class="num"><?php echo number_format($order['tax'], 2); ?></span>
                </td>
            </tr>

            <tr class="sub right shipping <?php if (count($atia) > 0) { echo 'with_after_tax'; } ?>">
                <td colspan="5">
                    Shipping: $<span class="num"><?php echo number_format($order['shipping'], 2); ?></span>
                </td>
            </tr>

            <?php if ($_uccms_ecomm->getSetting('gratuity_enabled')) { ?>
                <?php
                $order['gratuity'] = $_uccms_ecomm->orderGratuity($order);
                ?>
                <tr class="sub right gratuity">
                    <td colspan="5">
                        Gratuity: $<span class="num"><?php echo $order['gratuity']; ?></span>
                    </td>
                </tr>
            <?php } ?>

            <?php

            // HAVE AFTER TAX ITEMS
            if (count($atia) > 0) {

                ?>
                <tr>
                    <td colspan="5"></td>
                </tr>
                <?php

                // LOOP
                foreach ($atia as $oitem) {

                    // GET ITEM INFO
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                    // SETTINGS FOR DISPLAYING ITEM IN CART
                    $item_cart_settings = array(
                        'after_tax' => true
                    );

                    // DISPLAY ITEM IN CART
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                    unset($item, $oitem);

                }

            }

            ?>

            <tr class="sub right total">
                <td colspan="5">
                    Total: $<span class="num"><?php echo $_uccms_ecomm->orderTotal($order); ?></span>
                </td>
            </tr>

        </table>

        <?php if ($have_quote_items) { ?>
            <div class="have_quote_items">
                <?php echo stripslashes($_uccms_ecomm->getSetting('cart_requires_quote_message')); ?>
            </div>
        <?php } ?>

    </div>

</div>

<div class="contain" position="relative">

    <div class="half left">

        <div class="payment section_padding">

            <h4>Payment</h4>

            <div class="contain box">

                <?php include(dirname(__FILE__). '/../../elements/payment_options.php'); ?>

            </div>

        </div>

    </div>

    <div class="half left">

        <div class="notes section_padding">

            <?php

            // CHECKOUT / ORDER NOTES ENABLED
            if ($_uccms_ecomm->getSetting('checkout_notes')) {

                // GET NOTES DESCRIPTION
                $notes_description = $_uccms_ecomm->getSetting('checkout_notes_description');

                ?>

                <h4>Notes</h4>

                <div class="contain box">

                    <?php if ($notes_description) { ?>
                        <div class="description"><?php echo stripslashes($notes_description); ?></div>
                    <?php } ?>

                    <textarea name="order[notes]"><?php echo $vals['order']['notes']; ?></textarea>

                </div>

                <?php

            }

            ?>

        </div>

    </div>

</div>

<div class="submit section_padding">
    <input type="submit" name="" value="Place Order" />
</div>

</form>