<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/checkout.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/checkout.css" />

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

// DEFAULTS
if (!$ecomm['settings']['checkout_thanks_content']) $ecomm['settings']['checkout_thanks_title'] = 'Thank you for your order!';

// HAVE TITLE
if ($ecomm['settings']['checkout_thanks_title']) {
    ?>
    <h3><?php echo stripslashes($ecomm['settings']['checkout_thanks_title']); ?></h3>
    <?php
}
?>

<?php echo stripslashes($ecomm['settings']['checkout_thanks_content']); ?>

<?php if ($_SESSION['ecomm']['quote_request']) { ?>
    <div class="quote_request">
        <a href="../../quote/?order_id=<?php echo $_SESSION['ecomm']['order_id']; ?>&h=<?php echo $_SESSION['ecomm']['order_hash']; ?>">Click here</a> to continue to your quote request.
    </div>
<?php } ?>