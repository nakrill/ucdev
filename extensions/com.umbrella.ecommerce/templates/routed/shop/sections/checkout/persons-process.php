<?php

// STORE TYPE IS BOOKING
if ($_uccms_ecomm->storeType() == 'booking') {

    // HAVE PERSON ARRAY
    if (count($_POST['person']) > 0) {

        // LOOP THROUGH CART ITEMS
        foreach ($_POST['person'] as $oitem_id => $persons) {

            $oitem_id = (int)$oitem_id;

            // HAVE PERSONS FOR CART ITEM
            if (count($persons) > 0) {

                // LOOP THROUGH PERSONS
                foreach ($persons as $person_id => $person) {

                    $create = false;

                    // DB COLUMNS
                    $columns = array(
                        'firstname' => $person['firstname'],
                        'lastname'  => $person['lastname'],
                        'dob'       => ($person['dob'] ? date('Y-m-d', strtotime($person['dob'])) : '0000-00-00'),
                        'email'     => $person['email'],
                        'phone'     => $person['phone'],
                        'notes'     => $person['notes']
                    );

                    // IS NEW
                    if (substr($person_id, 0, 2) == 'n-') {
                        $create = true;

                    // IS EXISTING
                    } else {

                        $person_id = (int)$person_id;

                        // SEE IF WE ALREADY HAVE PERSON'S RECORD
                        $query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` WHERE (`id`=" .$person_id. ") AND (`oitem_id`=" .$oitem_id. ")";
                        $q = sqlquery($query);

                        // HAVE REDCORD
                        if (sqlrows($q) > 0) {

                            // UPDATE
                            $query = "UPDATE `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$person_id. ")";
                            sqlquery($query);

                        // NO RECORD
                        } else {
                            $create = true;
                        }

                    }

                    // IS CREATING
                    if ($create) {

                        $columns['oitem_id'] = $oitem_id;

                        // CREATE
                        $query = "INSERT INTO `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` SET " .$_uccms_ecomm->createSet($columns). "";
                        sqlquery($query);

                    }

                }

            // NO PERSONS FOR ORDER
            } else {

                // DELETE
                $query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['booking_bookings_persons']. "` WHERE (`oitem_id`=" .$oitem_id. ")";
                sqlquery($query);

            }

        }

        $_uccms['_site-message']->set('success', 'People updated.');

    }

}

BigTree::redirect('../');

?>