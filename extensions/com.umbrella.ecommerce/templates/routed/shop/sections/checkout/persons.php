<?php

// STORE TYPE IS BOOKING
if ($_uccms_ecomm->storeType() == 'booking') {

    // PERSON ITEMS ARARY
    $pia = array();

    // GET CART ITEMS
    $citems = $_uccms_ecomm->cartItems($_uccms_ecomm->cartID());

    // LOOP THROUGH ITEMS
    foreach ($citems as $oitem) {

        // GET ITEM INFO
        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

        // RENTAL / BOOKING AND BOOKING ENABLED
        if (($item['type'] == 2) && ($item['booking_enabled'])) {
            if ($booking['id']) {
                $pia[] = $oitem;
            }
        }

    }

    // NO ITEMS NEEDING PERSONS
    if (count($pia) == 0) {
        BigTree::redirect('../'); // SEND TO CHECKOUT
    }

    // DISPLAY ANY SITE MESSAGES
    echo $_uccms['_site-message']->display();

    ?>

    <style type="text/css">

        #ecommContainer .section.persons .item {
            margin-bottom: 20px;
        }
        #ecommContainer .section.persons .item .heading {
            padding: 0 0 5px 0;
            border-bottom: 1px solid #ccc;
        }
        #ecommContainer .section.persons .item .heading h4 {
            margin: 0 0 5px 0;
        }
        #ecommContainer .section.persons .item .people {

        }
        #ecommContainer .section.persons .item .people .person {
            padding: 7px 10px;
        }
        #ecommContainer .section.persons .item .people .person:nth-child(even) {
            background-color: #f5f5f5;
        }
        #ecommContainer .section.persons .item .people .person .label, #ecommContainer .section.persons .item .people .person .input {
            float: left;
        }
        #ecommContainer .section.persons .item .people .person .label {
            line-height: 23px;
        }
        #ecommContainer .section.persons .item .people .person .input {
            padding-left: 15px;
        }

    </style>

    <h3>People</h3>

    <form action="../persons-process/" method="post">

    <div class="section persons contain" position="relative">

        <?php

        // LOOP
        foreach ($pia as $oitem) {

            // GET ITEM INFO
            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

            ?>

            <div class="item">

                <div class="heading">
                    <h4><?php echo stripslashes($item['title']); ?></h4>
                    <?php if ($booking_duration['title']) { ?>
                        <span class="title"><?php echo stripslashes($booking_duration['title']); ?>:</span>
                    <?php } ?>
                    <span class="date">
                        <?php
                        $from_to = '';
                        if (($booking['dt_from']) && ($booking['dt_from'] != '0000-00-00 00:00:00')) {
                            list($booking_from_date, $booking_from_time) = explode(' ', $booking['dt_from']);
                            $from_to .= '
                            <span class="from">
                                <span class="date">' .date('n/j/Y', strtotime($booking_from_date)). '</span>
                                ';
                                if ($booking_from_time != '00:00:00') {
                                    $from_to .= '<span class="time">' .date('g:i A', strtotime($booking_from_time)). '</span>';
                                }
                                $from_to .='
                            </span>
                            ';
                            if (($booking['dt_to']) && ($booking['dt_to'] != '0000-00-00 00:00:00')) {
                                list($booking_to_date, $booking_to_time) = explode(' ', $booking['dt_to']);
                                $from_to .= ' to
                                <span class="to">';
                                    if ($booking_from_date != $booking_to_date) {
                                        $from_to .= '<span class="date">' .date('n/j/Y', strtotime($booking_to_date)). '</span>';
                                    }
                                    if ($booking_from_time != '00:00:00') {
                                        $from_to .= '
                                        <span class="time">' .date('g:i A', strtotime($booking_to_time) + 1). '</span>';
                                    }
                                    $from_to .='
                                </span>
                                ';
                            }
                        }
                        echo $from_to;
                        ?>
                    </span>
                </div>

                <div class="people">

                    <?php

                    // GET BOOKING PERSONS
                    $persons = $_uccms_ecomm->stc->bookingPersons($oitem['id']);

                    // QUANTITY
                    $qty = (int)$oitem['quantity'];

                    // LOOP THROUGH QUANTITY
                    for ($i=0; $i<=$qty - 1; $i++) {

                        $person = array_values($persons)[$i];
                        if (!is_array($person)) $person = array();

                        if (!$person['id']) {
                            $person['id'] = 'n-' .($i + 1);
                        }

                        $person['dob'] = '';
                        if (($person['dob']) && ($person['dob'] != '0000-00-00')) {
                            $person['dob'] = date('n/j/Y', strtotime($person['dob']));
                        }

                        ?>
                        <div class="person clearfix">
                            <div class="label">
                                Person <?php echo $i + 1; ?>
                            </div>
                            <div class="input firstname">
                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][firstname]" placeholder="First Name" value="<?php echo stripslashes($person['firstname']); ?>" />
                            </div>
                            <div class="input lastname">
                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][lastname]" placeholder="Last Name" value="<?php echo stripslashes($person['lastname']); ?>" />
                            </div>
                            <div class="input email">
                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][email]" placeholder="Email" value="<?php echo stripslashes($person['email']); ?>" />
                            </div>
                            <div class="input phone">
                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][phone]" placeholder="Phone" value="<?php echo stripslashes($person['phone']); ?>" />
                            </div>
                            <div class="input dob">
                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][dob]" placeholder="DOB (mm/dd/yyyy)" value="<?php echo $person['dob']; ?>" />
                            </div>
                            <div class="input notes">
                                <input type="text" name="person[<?php echo $oitem['id']; ?>][<?php echo $person['id']; ?>][notes]" placeholder="Notes" value="<?php echo stripslashes($person['notes']); ?>" />
                            </div>
                        </div>
                        <?php
                    }

                    ?>

                </div>

            </div>

            <?php

        }

        ?>

        <div class="buttons">
            <input type="submit" value="Continue" class="button" />
        </div>

    </div>

    </form>

    <?php

// NOT ENABLED - SEND TO CHECKOUT
} else {
    BigTree::redirect('../');
}

?>