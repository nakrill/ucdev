<?php

// IF SSL IS ENABLED - FORCE IT ON THIS PAGE
if ($_uccms_ecomm->getSetting('ssl_enabled')) {
    $cms->makeSecure();
}

// LEGAL / POLICY PAGE ENABLED
if ($_uccms_ecomm->getSetting('checkout_legal_enabled')) {

    // DISPLAY ANY SITE MESSAGES
    echo $_uccms['_site-message']->display();

    ?>

    <h3>Agree</h3>

    <form action="../agree-process/" method="post">

    <div class="contain" position="relative">

        <?php echo $_uccms_ecomm->getSetting('checkout_legal_content'); ?>

        <?php if ($_uccms_ecomm->getSetting('checkout_legal_required')) { ?>
            <div class="agree">
                <fieldset>
                    <input type="checkbox" name="agree" value="1" /> <label class="for_checkbox">I agree</label>
                </fieldset>
            </div>
        <?php } ?>

        <div class="buttons">
            <input type="submit" value="Continue" class="button" />
        </div>

    </div>

    </form>

    <?php

// NOT ENABLED
} else {
    BigTree::redirect('../../login/');
}

?>