<?php

// LEGAL / POLICY PAGE ENABLED
if ($_uccms_ecomm->getSetting('checkout_legal_enabled')) {

    // UPDATE CART
    $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `legal_agree`=" .(int)$_POST['agree']. " WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
    sqlquery($cart_query);

    // AGREE REQUIRED
    if ($_uccms_ecomm->getSetting('checkout_legal_required')) {

        // NOT AGREED
        if ($_POST['agree'] != 1) {

            $_uccms['_site-message']->set('error', 'You must agree to continue.');

            BigTree::redirect('../agree/');

        }

    }

}

BigTree::redirect('../../login/');

?>