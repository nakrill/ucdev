<?php

// APPLYING COUPON CODE
if ($_REQUEST['apply_coupon']) {

    // CLEAN UP
    $code = sqlescape(trim(strtolower($_REQUEST['coupon_code'])));

    // HAVE CODE
    if ($code) {

        // LOOK FOR MATCH
        $coupon_query = "SELECT * FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (LOWER(`code`)='" .$code. "')";
        $coupon_q = sqlquery($coupon_query);
        $coupon = sqlfetch($coupon_q);

        // COUPON FOUND
        if ($coupon['id']) {

            // ADD COUPON TO CART
            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `coupon_id`=" .$coupon['id']. " WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
            sqlquery($cart_query);

        // COUPON NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Coupon not found.');
        }

    // NO CODE
    } else {

        // UPDATE CART
        $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `coupon_id`=0 WHERE (`id`=" .$_uccms_ecomm->cartID(). ")";
        sqlquery($cart_query);

    }

    BigTree::redirect('../');

}

BigTree::redirect('../../checkout/agree/');

?>