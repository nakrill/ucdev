<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/cart.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/cart.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/cart.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/cart.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<h3>Cart</h3>

<?php if ($num_items > 0) { ?>

    <form action="./process/" method="get">

    <table id="cart" cellpadding="0" cellspacing="0">

        <tr>
            <th class="thumb"></th>
            <th class="title">Item</th>
            <th class="price">Price</th>
            <th class="quantity">Quantity</th>
            <th class="total">Total</th>
        </tr>

        <?php

        $quote_i = 0;

        // SUBTOTAL
        $subtotal = 0;

        // ATTRIBUTE ARRAY
        $attra = array();

        // AFTER TAX ITEM ARRAY
        $atia = array();

        // LOOP THROUGH ITEMS
        foreach ($citems as $oitem) {

            // IS QUOTE ITEM
            if ($oitem['quote']) {
                $quote_i++;
            }

            // AFTER TAX ITEM
            if ($oitem['after_tax']) {

                // ADD TO AFTER TAX ITEM ARRAY
                $atia[] = $oitem;

            // NORMAL ITEM
            } else {

                // GET ITEM INFO
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                // SETTINGS FOR DISPLAYING ITEM IN CART
                $item_cart_settings = array(
                    'edit_link'         => $item['other']['link_edit'],
                    'change_quantity'   => true,
                    'update_link'       => './?do=quantity&id=' .$oitem['id'],
                    'remove_link'       => './?do=remove&id=' .$oitem['id']
                );

                // DISPLAY ITEM IN CART
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                unset($item, $oitem);

            }

        }

        $cart['order_subtotal'] = $order['subtotal'];

        // GET DISCOUNT
        $discount = $_uccms_ecomm->cartDiscount($cart, $citems);

        ?>

        <tr class="sub right subtotal <?php if (!$discount['discount']) { ?>bold<?php } ?>">
            <td colspan="5">
                Subtotal: $<?php echo number_format($order['subtotal'], 2); ?>
            </td>
        </tr>

        <?php

        // HAVE DISCOUNT
        if ($discount['discount']) {
            $order['discount'] = $discount['discount'];
            ?>
            <tr class="sub right pre">
                <td colspan="5">
                    After discount (<?php if ($discount['coupon']['code']) { ?>Coupon: <?php echo $discount['coupon']['code']; ?> - <?php } ?>$<span class="num"><?php echo number_format($discount['discount'], 2); ?></span>): <strong>$<span class="num"><?php echo $_uccms_ecomm->orderTotal($order); ?></span></strong>
                </td>
            </tr>
            <?php
        }

        // HAVE AFTER TAX ITEMS
        if (count($atia) > 0) {

            ?>
            <tr class="after_tax">
                <td colspan="5">
                    <?php echo stripslashes($_uccms_ecomm->getSetting('cart_after_tax_title')); ?>&nbsp;
                </td>
            </tr>
            <?php

            // LOOP
            foreach ($atia as $oitem) {

                // GET ITEM INFO
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                // SETTINGS FOR DISPLAYING ITEM IN CART
                $item_cart_settings = array(
                    'edit_link'         => $item['other']['link_edit'],
                    'change_quantity'   => true,
                    'update_link'       => './?do=quantity&id=' .$oitem['id'],
                    'remove_link'       => './?do=remove&id=' .$oitem['id'],
                    'after_tax'         => true
                );

                // DISPLAY ITEM IN CART
                include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                unset($item, $oitem);

            }

        }

        ?>

    </table>

    <?php if (($have_quote_items) && (count($citems) != $quote_i)) { ?>
        <div class="have_quote_items">
            <?php echo stripslashes($_uccms_ecomm->getSetting('cart_requires_quote_message')); ?>
        </div>
    <?php } ?>

    <?php
    // GET COUPONS
    $coupons_query = "SELECT `id` FROM `" .$_uccms_ecomm->tables['coupons']. "` WHERE (`deleted_dt`='0000-00-00 00:00:00')";
    $coupons_q = sqlquery($coupons_query);
    if (sqlrows($coupons_q) > 0) { // HAVE COUPONS
        ?>
        <a name="coupon"></a>
        <div class="coupon_input <?php if ($discount['coupon']['error']) { ?>with-error<?php } ?>">
            Coupon Code: <input type="text" name="coupon_code" value="" /> <input type="submit" name="apply_coupon" value="Apply" />
            <?php if ($discount['coupon']['error']) { ?>
                <div class="error">
                    <?php echo $discount['coupon']['error']; ?>
                </div>
            <?php } ?>
        </div>
        <?php
    }
    ?>

    <div class="continue clearfix">

        <div class="shopping">
            <a href="/<?php echo $_uccms_ecomm->storePath(); ?>/">&laquo; Continue Shopping</a>
        </div>

        <?php if ($purchasing_enabled) { ?>
            <div class="checkout">
                <input type="submit" name="" value="<?php if (count($citems) == $quote_i) { ?>Request Quote<?php } else { ?>Checkout<?php } ?>" />
            </div>
        <?php } ?>

    </div>

    </form>

<?php } else { ?>

    <div class="cart_empty">
        Your shopping cart is empty.
    </div>

<?php } ?>