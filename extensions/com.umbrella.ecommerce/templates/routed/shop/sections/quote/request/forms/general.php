<script type="text/javascript">

    $(document).ready(function() {

        // SHIPPING SAME TOGGLE
        $('#shipping_same').change(function(e) {
            if ($(this).prop('checked')) {
                $('#toggle_shipping_same').hide();
            } else {
                $('#toggle_shipping_same').show();
            }
        });

    });

</script>

<div class="contain" position="relative">

    <div class="half left">

        <div class="billing_info section_padding">

            <h4>Billing Info</h4>

            <div class="contain box">

                <fieldset>
                    <label>First Name</label>
                    <input type="text" name="billing[firstname]" value="<?php echo stripslashes($vals['billing']['firstname']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Last Name</label>
                    <input type="text" name="billing[lastname]" value="<?php echo stripslashes($vals['billing']['lastname']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Company Name</label>
                    <input type="text" name="billing[company]" value="<?php echo stripslashes($vals['billing']['company']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Address</label>
                    <input type="text" name="billing[address1]" value="<?php echo stripslashes($vals['billing']['address1']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="billing[address2]" value="<?php echo stripslashes($vals['billing']['address2']); ?>" />
                </fieldset>

                <fieldset>
                    <label>City</label>
                    <input type="text" name="billing[city]" value="<?php echo stripslashes($vals['billing']['city']); ?>" />
                </fieldset>

                <fieldset>
                    <label>State</label>
                    <select name="billing[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $vals['billing']['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Zip</label>
                    <input type="text" name="billing[zip]" value="<?php echo stripslashes($vals['billing']['zip']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Phone</label>
                    <input type="text" name="billing[phone]" value="<?php echo stripslashes($vals['billing']['phone']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Email</label>
                    <input type="text" name="billing[email]" value="<?php echo stripslashes($vals['billing']['email']); ?>" />
                </fieldset>

            </div>

        </div>

    </div>

    <div class="half right">

        <div class="shipping_info section_padding">

            <h4>Shipping Info</h4>

            <div class="contain box">

                <fieldset>
                    <input id="shipping_same" type="checkbox" name="shipping[same]" value="1" <?php if ($vals['shipping']['same']) { ?>checked="checked"<?php } ?> /> Same as Billing
                </fieldset>

                <div id="toggle_shipping_same" class="contain" style="clear: right; <?php if ($vals['shipping']['same']) { ?>display: none;<?php } ?> padding-top: 10px;">

                    <fieldset>
                        <label>First Name</label>
                        <input type="text" name="shipping[firstname]" value="<?php echo stripslashes($vals['shipping']['firstname']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Last Name</label>
                        <input type="text" name="shipping[lastname]" value="<?php echo stripslashes($vals['shipping']['lastname']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Company Name</label>
                        <input type="text" name="shipping[company]" value="<?php echo stripslashes($vals['shipping']['company']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Address</label>
                        <input type="text" name="shipping[address1]" value="<?php echo stripslashes($vals['shipping']['address1']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Address (Apt, Bldg)</label>
                        <input type="text" name="shipping[address2]" value="<?php echo stripslashes($vals['shipping']['address2']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>City</label>
                        <input type="text" name="shipping[city]" value="<?php echo stripslashes($vals['shipping']['city']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>State</label>
                        <select name="shipping[state]">
                            <option value="">Select</option>
                            <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                                <option value="<?php echo $state_code; ?>" <?php if ($state_code == $vals['shipping']['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>Zip</label>
                        <input type="text" name="shipping[zip]" value="<?php echo stripslashes($vals['shipping']['zip']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Phone</label>
                        <input type="text" name="shipping[phone]" value="<?php echo stripslashes($vals['shipping']['phone']); ?>" />
                    </fieldset>

                    <fieldset>
                        <label>Email</label>
                        <input type="text" name="shipping[email]" value="<?php echo stripslashes($vals['shipping']['email']); ?>" />
                    </fieldset>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="contain" position="relative" style="margin-top: 15px;">

    <div class="half left">

        <div class="notes section_padding">

            <h4>Notes</h4>

            <fieldset class="order_notes">
                <textarea name="order[notes]" style="width: 100%;"><?php echo $vals['order']['notes']; ?></textarea>
            </fieldset>

        </div>

    </div>

    <div class="half right">

        <?php

        // ACCOUNTS ARE ENABLED
        if ($_uccms['_account']->isEnabled()) {

            // NOT LOGGED IN
            if (!$_uccms['_account']->loggedIn()) {

                ?>

                <div class="create_account section_padding">

                    <h4>Account</h4>

                    <div class="contain box">

                        <p>
                            <strong>Save time later.</strong> Specify a password and we'll automatically create an account for you for faster checkout and easy access to order history.
                        </p>

                        <fieldset class="account_password">
                            <label>Password</label>
                            <input type="password" name="account[password]" value="" />
                        </fieldset>

                    </div>

                </div>

                <?php

            }

        }

        ?>

    </div>

</div>

<div class="continue">
    <input type="submit" name="" value="Continue" />
</div>