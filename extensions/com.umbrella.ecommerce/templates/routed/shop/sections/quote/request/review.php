<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<h3>Review Quote Request</h3>

<?php if ($num_items > 0) { ?>

    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/quote_request.css" />
    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/quote_request.css" />
    <script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/quote_request.js"></script>
    <script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/quote_request.js"></script>

    <form action="../process/" method="post">
    <input type="hidden" name="do" value="process" />
    <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
    <input type="hidden" name="h" value="<?php echo stripslashes($order['hash']); ?>" />

        <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/review.php'); ?>

    </form>

<?php } ?>