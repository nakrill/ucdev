<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/quote_request.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/quote_request.css" />

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

// DEFAULTS
//if (!$ecomm['settings']['quote_request_thanks_title']) $ecomm['settings']['checkout_thanks_title'] = 'Thank you!';
if (!$ecomm['settings']['quote_request_thanks_content']) $ecomm['settings']['quote_request_thanks_title'] = 'Thank you for your quote request!';

// HAVE TITLE
if ($ecomm['settings']['quote_request_thanks_title']) {
    ?>
    <h3><?php echo stripslashes($ecomm['settings']['quote_request_thanks_title']); ?></h3>
    <?php
}
?>

<?php echo stripslashes($ecomm['settings']['quote_request_thanks_content']); ?>