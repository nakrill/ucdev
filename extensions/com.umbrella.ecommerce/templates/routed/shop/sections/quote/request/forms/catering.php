<div class="contain" position="relative">

    <div class="half left">

        <div class="event_info section_padding">

            <h4>Event Info</h4>

            <div class="contain box">

                <fieldset class="event_location">
                    <label>Location *</label>
                    <input type="text" name="event[location1]" value="<?php echo $vals['event']['location1']; ?>" />
                </fieldset>

                <fieldset class="event_location">
                    <label>Location (cont)</label>
                    <input type="text" name="event[location2]" value="<?php echo $vals['event']['location2']; ?>" />
                </fieldset>

                <fieldset class="event_address1">
                    <label>Address *</label>
                    <input type="text" name="event[address1]" value="<?php echo $vals['event']['address1']; ?>" />
                </fieldset>

                <fieldset class="event_address2">
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="event[address2]" value="<?php echo $vals['event']['address2']; ?>" />
                </fieldset>

                <fieldset class="event_city">
                    <label>City *</label>
                    <input type="text" name="event[city]" value="<?php echo $vals['event']['city']; ?>" />
                </fieldset>

                <fieldset class="event_state">
                    <label>State *</label>
                    <select name="event[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $vals['event']['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="event_zip">
                    <label>Zip *</label>
                    <input type="text" name="event[zip]" value="<?php echo $vals['event']['zip']; ?>" />
                </fieldset>

                <fieldset class="event_phone">
                    <label>Phone *</label>
                    <input type="text" name="event[phone]" value="<?php echo $vals['event']['phone']; ?>" />
                </fieldset>

                <fieldset class="event_date">
                    <label>Event Date *</label>
                    <div>
                        <?php

                        // SET DEFAULT DATE FORMAT IF NONE SPECIFIED
                        if (!$bigtree["config"]["date_format"]) {
                            $bigtree["config"]["date_format"] = 'm/d/Y';
                        }

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'event_date',
                            'key'       => 'event[date]',
                            'value'     => $vals['event']['date'],
                            'required'  => true,
                            'options'   => array(
                                'default_today' => true
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>
                </fieldset>

                <fieldset class="service_time">
                    <label>Service Time *</label>
                    <div>
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'service_time',
                            'key'       => 'service[time]',
                            'value'     => $vals['service']['time'],
                            'required'  => true
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/time.php'));

                        ?>
                    </div>
                </fieldset>

                <fieldset class="service_type">
                    <label>Service Type *</label>
                    <select name="service[type]">
                        <option value="catering_event" <?php if ($vals['service']['type'] == 'catering_event') { ?>selected="selected"<?php } ?>>Event Catering</option>
                        <option value="catering_wedding" <?php if ($vals['service']['type'] == 'catering_wedding') { ?>selected="selected"<?php } ?>>Wedding Catering</option>
                        <option value="chef" <?php if ($vals['service']['type'] == 'chef') { ?>selected="selected"<?php } ?>>Chef</option>
                        <option value="beverage" <?php if ($vals['service']['type'] == 'beverage') { ?>selected="selected"<?php } ?>>Beverage</option>
                        <option value="pickup" <?php if ($vals['service']['type'] == 'pickup') { ?>selected="selected"<?php } ?>>Pickup</option>
                        <option value="dropoff" <?php if ($vals['service']['type'] == 'dropoff') { ?>selected="selected"<?php } ?>>Dropoff</option>
                        <option value="in_house" <?php if ($vals['service']['type'] == 'in_house') { ?>selected="selected"<?php } ?>>In-House</option>
                        <option value="menu_tasting" <?php if ($vals['service']['type'] == 'menu_tasting') { ?>selected="selected"<?php } ?>>Menu Tasting</option>
                    </select>
                </fieldset>

                <fieldset class="event_num_people">
                    <label># People *</label>
                    <input type="text" name="event[num_people]" value="<?php echo $vals['event']['num_people']; ?>" />
                </fieldset>

                <fieldset class="event_notes">
                    <label>Special Requests / Notes</label>
                    <textarea name="event[notes]"><?php echo $vals['event']['notes']; ?></textarea>
                </fieldset>

            </div>

        </div>

    </div>

    <div class="half right">

        <div class="contact_info section_padding">

            <h4>Contact Info</h4>

            <div class="contain box">

                <fieldset class="contact_firstname">
                    <label>First Name *</label>
                    <input type="text" name="contact[firstname]" value="<?php echo $vals['contact']['firstname']; ?>" />
                </fieldset>

                <fieldset class="contact_lastname">
                    <label>Last Name *</label>
                    <input type="text" name="contact[lastname]" value="<?php echo $vals['contact']['lastname']; ?>" />
                </fieldset>

                <fieldset class="contact_company">
                    <label>Company</label>
                    <input type="text" name="contact[company]" value="<?php echo $vals['contact']['company']; ?>" />
                </fieldset>

                <fieldset class="contact_address1">
                    <label>Address *</label>
                    <input type="text" name="contact[address1]" value="<?php echo $vals['contact']['address1']; ?>" />
                </fieldset>

                <fieldset class="contact_address2">
                    <label>Address (Apt, Bldg)</label>
                    <input type="text" name="contact[address2]" value="<?php echo $vals['contact']['address2']; ?>" />
                </fieldset>

                <fieldset class="contact_city">
                    <label>City *</label>
                    <input type="text" name="contact[city]" value="<?php echo $vals['contact']['city']; ?>" />
                </fieldset>

                <fieldset class="contact_state">
                    <label>State *</label>
                    <select name="contact[state]">
                        <option value="">Select</option>
                        <?php foreach (BigTree::$StateList as $state_code => $state_name) { ?>
                            <option value="<?php echo $state_code; ?>" <?php if ($state_code == $vals['contact']['state']) { ?>selected="selected"<?php } ?>><?php echo $state_name; ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset class="contact_zip">
                    <label>Zip *</label>
                    <input type="text" name="contact[zip]" value="<?php echo $vals['contact']['zip']; ?>" />
                </fieldset>

                <fieldset class="contact_phone">
                    <label>Phone *</label>
                    <input type="text" name="contact[phone]" value="<?php echo $vals['contact']['phone']; ?>" />
                </fieldset>

                <fieldset class="contact_email">
                    <label>Email *</label>
                    <input type="text" name="contact[email]" value="<?php echo $vals['contact']['email']; ?>" />
                </fieldset>

            </div>

        </div>

        <?php

        // ACCOUNTS ARE ENABLED
        if ($_uccms['_account']->isEnabled()) {

            // NOT LOGGED IN
            if (!$_uccms['_account']->loggedIn()) {

                ?>

                <div class="create_account section_padding">

                    <h4>Account</h4>

                    <div class="contain box">

                        <p>
                            <strong>Save time later.</strong> Specify a password and we'll automatically create an account for you for faster checkout and easy access to order history.
                        </p>

                        <fieldset class="account_password">
                            <label>Password</label>
                            <input type="password" name="account[password]" value="" />
                        </fieldset>

                    </div>

                </div>

                <?php

            }

        }

        ?>

        <div class="continue">
            <input type="submit" name="" value="Continue" />
        </div>

    </div>

</div>

<script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
    $('.date_picker').datepicker({ dateFormat: 'mm/dd/yy', duration: 200, showAnim: 'slideDown' });
    $('.time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
</script>
