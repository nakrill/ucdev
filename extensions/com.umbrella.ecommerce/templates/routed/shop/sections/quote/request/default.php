<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<h3>Quote Request</h3>

<?php if ($num_items > 0) { ?>

    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/quote_request.css" />
    <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/quote_request.css" />
    <script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/quote_request.js"></script>
    <script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/quote_request.js"></script>

    <style type="text/css">
        #ecommContainer #cart .item .price, #ecommContainer #cart .item .total {
            display: none;
        }
    </style>

    <div class="cart section_padding">

        <div class="contain box">

            <table id="cart" cellpadding="0" cellspacing="0">

                <tr>
                    <th class="thumb"></th>
                    <th class="title">Item</th>
                    <th class="quantity">Quantity</th>
                </tr>

                <?php

                // ATTRIBUTE ARRAY
                $attra = array();

                // LOOP THROUGH ITEMS
                foreach ($citems as $oitem) {

                    // GET ITEM INFO
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                    // SETTINGS FOR DISPLAYING ITEM IN CART
                    $item_cart_settings = array();

                    // DISPLAY ITEM IN CART
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_cart.php');

                    unset($item, $oitem);

                }

                ?>

            </table>

        </div>

    </div>

    <form action="./review/" method="post">
    <input type="hidden" name="do" value="review" />
    <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>" />
    <input type="hidden" name="h" value="<?php echo stripslashes($order['hash']); ?>" />

        <?php include(dirname(__FILE__). '/forms/' .$_uccms_ecomm->storeType(). '.php'); ?>

    </form>

<?php } ?>