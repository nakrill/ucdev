<?php

// URL TO GO BACK TO
$back_url = '../review/?order_id=' .$_REQUEST['order_id']. '&h=' .$_REQUEST['h'];

// IS PROCESSING
if ($_POST['do'] == 'process') {

    // HAVE ITEMS
    if ($num_items > 0) {

        /*
        // REQUIRED FIELDS
        $reqfa = array(
            'firstname' => 'First Name',
            'lastname'  => 'Last Name',
            'address1'  => 'Address',
            'city'      => 'City',
            'state'     => 'State',
            'zip'       => 'Zip',
            'phone'     => 'Phone',
            'email'     => 'Email'
        );

        // CHECK REQUIRED
        foreach ($reqfa as $name => $title) {
            $what = 'general';
            if ($name == 'email') $what = 'email';
            if (!$_uccms_ecomm->checkRequired($_POST['billing'][$name], $what)) {
                $_SESSION['ecomm']['quote_request']['err'][] = 'Missing / Incorrect: Billing - ' .$title;
            }
        }

        // HAVE ERRORS
        if (count($_SESSION['ecomm']['quote_request']['err']) > 0) {
            BigTree::redirect($back_url);
            exit;
        }
        */

        // VALUES
        $vals = $_SESSION['ecomm']['quote_request']['fields'];

        // remove old account code

        // LOOP THROUGH ITEMS
        foreach ($citems as $oitem) {

            // GET ITEM INFO
            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

            // TEMP EMAIL VAR
            $tevar = '
            <tr>
                <td valign="top" style="padding: 8px;">
                    <img src="' .$item['other']['image_url']. '" alt="' .stripslashes($item['title']). '" height="50" />
                </td>
                <td width="80%" style="padding: 8px;">
                    <a href="' .$item['other']['link']. '">' .stripslashes($item['title']). '</a>';
                    if (count($item['other']['options']) > 0) {
                        $tevar .= '<ul style="margin: 0; padding: 0 0 0 5px; list-style: outside none none; font-size: 0.8em;">';
                            foreach ($item['other']['options'] as $option_id => $values) {
                                if ($values[0]) {
                                    $tevar .= '<li>' .stripslashes($attra[$option_id]['title']). ': ' .implode(', ', $values). '</li>';
                                }
                            }
                        $tevar .= '</ul>';
                    }
                $tevar .= '
                </td>
                <td width="20%" style="padding: 8px; text-align: right;">
                    ' .number_format($oitem['quantity'], 0). '
                </td>
            </tr>
            ';

            // IS AFTER TAX ITEM
            if ($oitem['after_tax']) {
                $evars_items_aftertax = $tevar;
            } else {
                $evars_items = $tevar;
            }

        }

        // DATABASE COLUMNS
        $columns = array(
            'quote'                 => 1,
            'status'                => 'requested',
            'ip'                    => ip2long($_SERVER['REMOTE_ADDR']),
        );

        // IS CATERING
        if ($_uccms_ecomm->storeType() == 'catering') {

            $columns['billing_firstname']   = $vals['contact']['firstname'];
            $columns['billing_lastname']    = $vals['contact']['lastname'];
            $columns['billing_company']     = $vals['contact']['company'];
            $columns['billing_address1']    = $vals['contact']['address1'];
            $columns['billing_address2']    = $vals['contact']['address2'];
            $columns['billing_city']        = $vals['contact']['city'];
            $columns['billing_state']       = $vals['contact']['state'];
            $columns['billing_zip']         = $vals['contact']['zip'];
            $columns['billing_country']     = '';
            $columns['billing_phone']       = $vals['contact']['phone'];
            $columns['billing_email']       = $vals['contact']['email'];
            $columns['shipping_same']       = 1;
            $columns['notes']               = $vals['event']['notes'];

        // IS GENERAL
        } else {

            $columns['billing_firstname']   = $vals['billing']['firstname'];
            $columns['billing_lastname']    = $vals['billing']['lastname'];
            $columns['billing_company']     = $vals['billing']['company'];
            $columns['billing_address1']    = $vals['billing']['address1'];
            $columns['billing_address2']    = $vals['billing']['address2'];
            $columns['billing_city']        = $vals['billing']['city'];
            $columns['billing_state']       = $vals['billing']['state'];
            $columns['billing_zip']         = $vals['billing']['zip'];
            $columns['billing_country']     = '';
            $columns['billing_phone']       = $vals['billing']['phone'];
            $columns['billing_email']       = $vals['billing']['email'];
            $columns['shipping_same']       = $vals['shipping']['same'];
            $columns['shipping_firstname']  = $vals['shipping']['firstname'];
            $columns['shipping_lastname']   = $vals['shipping']['lastname'];
            $columns['shipping_company']    = $vals['shipping']['company'];
            $columns['shipping_address1']   = $vals['shipping']['address1'];
            $columns['shipping_address2']   = $vals['shipping']['address2'];
            $columns['shipping_city']       = $vals['shipping']['city'];
            $columns['shipping_state']      = $vals['shipping']['state'];
            $columns['shipping_zip']        = $vals['shipping']['zip'];
            $columns['shipping_country']    = '';
            $columns['shipping_phone']      = $vals['shipping']['phone'];
            $columns['shipping_email']      = $vals['shipping']['email'];
            $columns['notes']               = $vals['order']['notes'];

        }

        // IS CATERING
        if ($_uccms_ecomm->storeType() == 'catering') {
            $account_info = [
                'email'     => $vals['contact']['email'],
                'firstname' => $vals['contact']['firstname'],
                'lastname'  => $vals['contact']['lastname'],
                'password'  => $_POST['account']['password'],
            ];
        // IS GENERAL
        } else {
            $account_info = [
                'email'     => $vals['billing']['email'],
                'firstname' => $vals['billing']['firstname'],
                'lastname'  => $vals['billing']['lastname'],
                'password'  => $_POST['account']['password'],
            ];
        }

        // SAVE CUSTOMER
        $customer = $_uccms_ecomm->orderSaveCustomer(array_merge($columns, [
            'account' => $account_info
        ]));
        $customer_id = $customer['id'];

        // ORDER HASH
        if ($order['hash']) {
            $order_hash = $order['hash'];
        } else {
            $order_hash = $_uccms_ecomm->generateOrderHash($customer_id);
        }

        $columns['hash'] = $order_hash;
        $columns['customer_id'] = $customer_id;

        // CREATE ORDER
        $cart_query = "INSERT INTO `" .$_uccms_ecomm->tables['orders']. "` SET `dt`=NOW(), " .$_uccms_ecomm->createSet($columns). "";

        // ORDER SUCCESSFUL
        if (sqlquery($cart_query)) {

            // ORDER ID
            $order_id = sqlid();

            // HAVE OLD ORDER ID
            if ($order['id']) {

                // MOVE QUOTE ITEMS TO NEW ORDER ID
                $item_query = "UPDATE `" .$_uccms_ecomm->tables['order_items']. "` SET `cart_id`=" .$order_id. " WHERE (`cart_id`=" .$order['id']. ") AND (`quote`=1)";
                sqlquery($item_query);

            }

            // STORE TYPE
            switch ($_uccms_ecomm->storeType()) {

                // GENERAL
                case 'general':

                    $details_table = $_uccms_ecomm->tables['quote_general'];
                    $details_columns = array(
                        'order_id'              => $order_id
                    );
                    break;

                // CATERING
                case 'catering':

                    $details_table = $_uccms_ecomm->tables['quote_catering'];
                    $details_columns = array(
                        'order_id'              => $order_id,
                        'event_location1'       => $vals['event']['location1'],
                        'event_location2'       => $vals['event']['location2'],
                        'event_address1'        => $vals['event']['address1'],
                        'event_address2'        => $vals['event']['address2'],
                        'event_city'            => $vals['event']['city'],
                        'event_state'           => $vals['event']['state'],
                        'event_zip'             => $vals['event']['zip'],
                        'event_phone'           => $vals['event']['phone'],
                        'event_date'            => date('Y-m-d', strtotime($vals['event']['date'])),
                        'event_num_people'      => number_format($vals['event']['num_people'], 0, '', ''),
                        'event_notes'           => $vals['event']['notes'],
                        'service_time'          => date('H:i:00', strtotime($vals['service']['time'])),
                        'service_type'          => $vals['service']['type'],
                        'contact_firstname'     => $vals['contact']['firstname'],
                        'contact_lastname'      => $vals['contact']['lastname'],
                        'contact_company'       => $vals['contact']['company'],
                        'contact_address1'      => $vals['contact']['address1'],
                        'contact_address2'      => $vals['contact']['address2'],
                        'contact_city'          => $vals['contact']['city'],
                        'contact_state'         => $vals['contact']['state'],
                        'contact_zip'           => $vals['contact']['zip'],
                        'contact_phone'         => $vals['contact']['phone'],
                        'contact_email'         => $vals['contact']['email']
                    );

                    break;

            }

            // HAVE DETAILS TABLE
            if ($details_table) {

                // DETAILS DB
                $details_query = "INSERT INTO `" .$details_table. "` SET " .$_uccms_ecomm->createSet($details_columns). "";
                sqlquery($details_query);

            }

            ##########################
            # EMAIL
            ##########################

            // EMAIL SETTINGS
            $esettings = array(
                'template'      => 'quote_request-store.html',
                'subject'       => 'Quote Request',
                'order_id'      => $order_id
            );

            // AFTER TAX ITEMS
            if ($evars_items_aftertax) {
                $evars_items_aftertax = '
                <tr>
                    <td colspan="5" align="right" style="text-align: right;">
                        ' .$evars_items_aftertax. '
                    </td>
                </tr>
                ';
            }

            // EMAIL VARIABLES
            $evars = array(
                'date'              => date('n/j/Y'),
                'time'              => date('g:i A T'),
                'order_id'          => $order_id,
                'items'             => $evars_items,
                'items_aftertax'    => $evars_items_aftertax,
            );

            // COPY OF ORDER TO ADMIN(S)
            if ($ecomm['settings']['email_order_copy']) {
                $emails = explode(',', stripslashes($ecomm['settings']['email_order_copy']));
                foreach ($emails as $email) {
                    $emailtoa[$email] = $email;
                }
            }

            // HAVE EMAILS TO SEND TO
            if (count($emailtoa) > 0) {

                // LOOP
                foreach ($emailtoa as $to_email => $to_name) {

                    // WHO TO SEND TO
                    $esettings['to_email'] = $to_email;

                    // SEND EMAIL
                    $result = $_uccms_ecomm->sendEmail($esettings, $evars);

                }

            }

            ##########################

            // CLEAR CART ID
            $_uccms_ecomm->clearCartID(true);

            // SEND TO THANK YOU PAGE
            BigTree::redirect('../thanks/');

        // QUERY FAILED
        } else {
            $_uccms['_site-message']->set('error', 'Failed to process quote request.');
            BigTree::redirect($back_url);
            exit;
        }

    // NO ITEMS
    } else {
        $_uccms['_site-message']->set('error', 'No quotable items found.');
        BigTree::redirect($back_url);
        exit;
    }

// NOT PROCESSING
} else {
    $_uccms['_site-message']->set('error', 'Quote request form not submitted.');
    BigTree::redirect($back_url);
    exit;
}

?>