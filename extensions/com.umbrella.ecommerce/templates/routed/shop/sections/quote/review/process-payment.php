<?php

// BACK URL
$back_url = '../?id=' .$_POST['order_id']. '&h=' .$_POST['order_h'];

// SAVE FIELDS TO SESSION
unset($_SESSION['ecomm']['quote_pay']['fields']);
$_SESSION['ecomm']['quote_pay']['fields'] = $_POST;

// IS PROCESSING
if ($_POST['do'] == 'process') {

    // FORMAT AMOUNT
    $amount = $_uccms_ecomm->toFloat($_POST['amount']);

    // HAVE AMOUNT
    if ($amount > 0.00) {

        // HAVE ORDER ID & HASH
        if (($_POST['order_id']) && ($_POST['order_h'])) {

            // GET ORDER INFO
            $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .(int)$_POST['order_id']. ") AND (`hash`='" .sqlescape($_POST['order_h']). "')";
            $order_q = sqlquery($order_query);
            $order = sqlfetch($order_q);

            // ORDER FOUND
            if ($order['id']) {

                // SET EXTENDED VIEW
                $extended_view = true;

                // GET ITEMS
                $citems = $_uccms_ecomm->cartItems($order['id']);

                // HAVE ITEMS
                if (count($citems) > 0) {

                    // CLEAN UP
                    $payment_method = sqlescape(trim($_POST['payment']['method']));

                    // HAVE PAYMENT METHOD
                    if ($payment_method) {

                        // GET PAYMENT METHOD INFO
                        $pmethod_query = "SELECT * FROM `" .$_uccms_ecomm->tables['payment_methods']. "` WHERE (`id`='" .$payment_method. "') AND (`active`=1) AND (`frontend`=1)";
                        $pmethod_q = sqlquery($pmethod_query);
                        $pmethod = sqlfetch($pmethod_q);

                        // NOT FOUND
                        if (!$pmethod['id']) {
                            $payment_method = '';
                        }

                    }

                    // NO PAYMENT METHOD
                    if (!$payment_method) {
                        $_uccms['_site-message']->set('error', 'Payment method not selected.');
                        BigTree::redirect($back_url);
                        exit;
                    }

                    // removed fields

                    // HAVE TAX OVERRIDE
                    if ($order['override_tax']) {
                        $tax['total'] = (float)$order['override_tax'];

                    // CALCULATE TAX
                    } else {
                        $tax = $_uccms_ecomm->cartTax($order['id'], $citems, array(
                            'country'   => 'United States',
                            'state'     => stripslashes($order['billing_state']),
                            'zip'       => stripslashes($order['billing_zip'])
                        ));
                    }

                    // HAVE SHIPPING OVERRIDE
                    if ($order['override_shipping']) {
                        $shipping['markup'] = (float)$order['override_shipping'];
                    } else {
                        $shipping['markup'] = 0.00;
                    }

                    // ORDER TOTAL
                    $order['total'] = 0;

                    // ORDER DATA FOR REVSOCIAL API
                    $rs_orderdata = array(
                        'billing'   => array(
                            'firstname'     => stripslashes($order['billing_firstname']),
                            'lastname'      => stripslashes($order['billing_lastname']),
                            'company'       => stripslashes($order['billing_company']),
                            'address1'      => stripslashes($order['billing_address1']),
                            'address2'      => stripslashes($order['billing_address2']),
                            'city'          => stripslashes($order['billing_city']),
                            'state'         => stripslashes($order['billing_state']),
                            'zip'           => stripslashes($order['billing_zip']),
                            'country'       => stripslashes($order['billing_country']),
                            'phone'         => stripslashes($order['billing_phone']),
                            'email'         => stripslashes($order['billing_email'])
                        ),
                        'shipping'  => array(
                            'firstname'     => stripslashes($order['shipping_firstname']),
                            'lastname'      => stripslashes($order['shipping_lastname']),
                            'company'       => stripslashes($order['shipping_company']),
                            'address1'      => stripslashes($order['shipping_address1']),
                            'address2'      => stripslashes($order['shipping_address2']),
                            'city'          => stripslashes($order['shipping_city']),
                            'state'         => stripslashes($order['shipping_state']),
                            'zip'           => stripslashes($order['shipping_zip']),
                            'country'       => stripslashes($order['shipping_country']),
                            'phone'         => stripslashes($order['shipping_phone']),
                            'email'         => stripslashes($order['shipping_email'])
                        )
                    );

                    // QUANTITY COUNT
                    $rs_items['num_products'] = count($citems);
                    $rs_items['num_quantity'] = 0;

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // ADD ITEM TOTAL TO ORDER TOTAL
                        $order['total'] += $item['other']['total'];

                        // TEMP EMAIL VAR
                        $tevar = '
                        <tr>
                            <td valign="top" style="padding: 8px;">
                                <img src="' .$item['other']['image_url']. '" alt="' .stripslashes($item['title']). '" height="50" />
                            </td>
                            <td width="50%" style="padding: 8px;">
                                <a href="' .$item['other']['link']. '">' .stripslashes($item['title']). '</a>';
                                if ($oitem['booking']['id']) {
                                    $tevar .= '<div class="booking">';
                                    if ($booking_duration['title']) {
                                        $tevar .= '<span class="title">' .stripslashes($booking_duration['title']). ':</span>';
                                    }
                                    $tevar .= '<span class="date" style="font-weight: bold;">';
                                    $from_to = '';
                                    if (($oitem['booking']['dt_from']) && ($oitem['booking']['dt_from'] != '0000-00-00 00:00:00')) {
                                        list($booking_from_date, $booking_from_time) = explode(' ', $oitem['booking']['dt_from']);
                                        $from_to .= '
                                        <span class="from">
                                            <span class="date">' .date('n/j/Y', strtotime($booking_from_date)). '</span>
                                            ';
                                            if ($booking_from_time != '00:00:00') {
                                                $from_to .= '<span class="time">' .date('g:i A', strtotime($booking_from_time)). '</span>';
                                            }
                                            $from_to .='
                                        </span>
                                        ';
                                        if (($oitem['booking']['dt_to']) && ($oitem['booking']['dt_to'] != '0000-00-00 00:00:00')) {
                                            list($booking_to_date, $booking_to_time) = explode(' ', $oitem['booking']['dt_to']);
                                            $from_to .= ' to
                                            <span class="to">';
                                                if ($booking_from_date != $booking_to_date) {
                                                    $from_to .= '<span class="date">' .date('n/j/Y', strtotime($booking_to_date)). '</span>';
                                                }
                                                if ($booking_from_time != '00:00:00') {
                                                    $from_to .= '
                                                    <span class="time">' .date('g:i A', strtotime($booking_to_time) + 1). '</span>';
                                                }
                                                $from_to .='
                                            </span>
                                            ';
                                        }
                                    }
                                    $tevar .= $from_to;
                                    $tevar .= '</span>';
                                    if ($oitem['booking']['markup'] != 0.00) {
                                        $tevar .= '<span class="markup">(+ $' .$_uccms_ecomm->toFloat($oitem['booking']['markup']). ')</span>';
                                    }
                                    $tevar .= '</div>';
                                }
                                if (($item['type'] == 3) && (count($item['package']['items']) > 0)) {
                                    foreach ($item['package']['items'] as $pitem_id => $pitem) {
                                        $pitem_attrs = $_uccms_ecomm->item_attrAndOptionTitles($pitem, $pitem['other']['attribute_titles'], $attr_options);
                                        $tevar .= '<li>' .stripslashes($pitem['item']['title']); if ($pitem['quantity'] > 1) { $tevar .= ' (' .number_format($pitem['quantity'], 0). ')'; }
                                        if (count($pitem_attrs) > 0) {
                                            $tevar .= '<ul>';
                                            foreach ($pitem_attrs as $attr) {
                                                $tevar .= '<li>' .$attr['title']. ': ' .$attr['options']. '</li>';
                                            }
                                            $tevar .= '</ul>';
                                        }
                                        $tevar .= '</li>';
                                    }
                                } else {
                                    if (count($item['other']['options']) > 0) {
                                        $tevar .= '<ul style="margin: 0; padding: 0 0 0 5px; list-style: outside none none; font-size: 0.8em;">';
                                            foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra) as $attr) {
                                                $tevar .= '<li>' .$attr['title']. ': ' .$attr['options']. '</li>';
                                            }
                                        $tevar .= '</ul>';
                                    }
                                }
                            $tevar .= '
                            </td>
                            <td style="padding: 8px;">
                                ' .$item['other']['price_formatted']. '
                            </td>
                            <td style="padding: 8px;">
                                ' .number_format($oitem['quantity'], 0). '
                            </td>
                            <td style="padding: 8px;">
                                ' .$item['other']['total_formatted']. '
                            </td>
                        </tr>
                        ';

                        // IS AFTER TAX ITEM
                        if ($oitem['after_tax']) {
                            $evars_items_aftertax .= $tevar;
                        } else {
                            $evars_items .= $tevar;
                        }

                        // ADD TO QUANTITY COUNT
                        $rs_items['num_quantity'] += $oitem['quantity'];

                        // PRODUCT OPTIONS (ATTRIBUTES)
                        $options = array();
                        if (count($item['other']['options']) > 0) {
                            foreach ($item['other']['options'] as $option_id => $values) {
                                if ($values[0]) {
                                    $options[stripslashes($attra[$option_id]['title'])] = implode(', ', $values);
                                }
                            }
                        }

                        // ADD TO PRODUCTS ARRAY
                        $rs_items['products'][] = array(
                            'sku'       => stripslashes($item['sku']),
                            'name'      => stripslashes($item['title']),
                            'options'   => $options,
                            'quantity'  => $oitem['quantity'],
                            'total'     => $item['other']['total']
                        );

                    }

                    // ORDER TAX
                    $order['tax']   = $tax['total'];
                    $order['total'] += $order['tax'];

                    // ORDER SHIPPING
                    $order['shipping']  = $shipping['markup'];
                    $order['total']     += $order['shipping'];

                    // ORDER GRATUITY
                    $order['gratuity']  = $_uccms_ecomm->orderGratuity($order);
                    $order['total']     += $order['gratuity'];

                    // removed fields

                    // ORDER TOTAL
                    $order_total = $_uccms_ecomm->orderTotal($order);

                    // DATABASE COLUMNS
                    $columns = array(
                        'order_subtotal'        => number_format($order['subtotal'], 2),
                        'order_discount'        => '',
                        'order_discount_id'     => '',
                        'order_tax'             => number_format($order['tax'], 2),
                        'order_aftertax'        => number_format($order['aftertax'], 2),
                        'order_shipping'        => number_format($order['shipping'], 2),
                        'order_gratuity'        => $order['gratuity'],
                        'order_total'           => $order_total
                    );

                    // CUSTOMER ID
                    if (!$order['customer_id']) {
                        $columns['customer_id'] = $_uccms_ecomm->customerID($order['billing_email']);
                    }

                    // UPDATE CART
                    $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `dt`=NOW(), " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order['id']. ")";

                    // UPDATE SUCCESSFUL
                    if (sqlquery($cart_query)) {

                        // CALCULATE BALANCE
                        $balance = $_uccms_ecomm->orderBalance($order['id'], $amount);

                        // PAYING BY CHECK/CASH
                        if ($payment_method == 'check_cash') {

                            $columns = array(
                                'status'    => 'placed'
                            );
                            if ($order['quote'] == 1) {
                                $columns['quote'] = 2;
                            }
                            if ($balance == 0.00) {
                                $columns['paid_in_full'] = 1;
                            }

                            // UPDATE STATUS
                            $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order['id']. ")";
                            sqlquery($cart_query);

                            // LOG TRANSACTION
                            $_uccms_ecomm->logTransaction(array(
                                'cart_id'   => $order['id'],
                                'status'    => 'captured',
                                'amount'    => $amount,
                                'method'    => $_POST['payment']['method'],
                                'ip'        => ip2long($_SERVER['REMOTE_ADDR'])
                            ));

                            $thanks = true;

                        // PAYING OTHER WAY
                        } else {

                            $payment_missing = false;

                            // NO CREDIT CARD FIELDS
                            if ($pmethod['hide_cc']) {

                            // PAYMENT PROFILE SPECIFIED
                            } else if ($_POST['payment']['profile_id']) {

                                // GET PAYMENT PROFILE
                                $pp = $_uccms['_account']->pp_getProfile((int)$_POST['payment']['profile_id'], $_uccms['_account']->userID());

                                // PAYMENT PROFILE FOUND
                                if ($pp['id']) {

                                    $payment_method     = 'profile';
                                    $payment_name       = stripslashes($pp['name']);
                                    $payment_lastfour   = $pp['lastfour'];
                                    $payment_expiration = $pp['expires'];

                                // PAYMENT PROFILE NOT FOUND
                                } else {
                                    $payment_missing = true;
                                    $_uccms['_site-message']->set('error', 'Payment method not found.');
                                }

                            // USE CREDIT CARD INFO
                            } else {

                                // REQUIRE PAYMENT FIELDS
                                $reqfa = array(
                                    'name'          => 'Name On Card',
                                    'number'        => 'Card Number',
                                    'expiration'    => 'Card Expiration',
                                    'zip'           => 'Card Zip',
                                    'cvv'           => 'Card CVV'
                                );

                                // CHECK REQUIRED
                                foreach ($reqfa as $name => $title) {
                                    if (!$_uccms_ecomm->checkRequired($_POST['payment'][$payment_method][$name])) {
                                        $payment_missing = true;
                                        $_uccms['_site-message']->set('error', 'Missing / Incorrect: Payment - ' .$title);
                                    }
                                }

                                $payment_name = $_POST['payment'][$payment_method]['name'];

                                // PAYMENT LAST FOUR
                                $payment_lastfour = substr(preg_replace("/[^0-9]/", '', $_POST['payment'][$payment_method]['number']), -4);

                                // PAYMENT EXPIRATION DATE FORMATTING
                                if ($_POST['payment'][$payment_method]['expiration']) {
                                    $payment_expiration = date('Y-m-d', strtotime(str_replace('/', '/01/', $_POST['payment'][$payment_method]['expiration'])));
                                } else {
                                    $payment_expiration = '0000-00-00';
                                }

                            }

                            // NO MISSING FIELDS
                            if (!$payment_missing) {

                                // PAYMENT - CHARGE
                                $payment_data = array(
                                    'method'    => $payment_method,
                                    'amount'    => $amount,
                                    'customer' => array(
                                        'id'            => ($order['customer_id'] ? $order['customer_id'] : $_uccms['_account']->userID()),
                                        'name'          => $payment_name,
                                        'email'         => $order['billing_email'],
                                        'phone'         => $order['billing_phone'],
                                        'address'       => array(
                                            'street'    => $order['billing_address1'],
                                            'street2'   => $order['billing_address2'],
                                            'city'      => $order['billing_city'],
                                            'state'     => $order['billing_state'],
                                            'zip'       => $order['billing_zip'],
                                            'country'   => $order['billing_city']
                                        ),
                                        'description'   => ''
                                    ),
                                    'note'  => 'Payment on order ID: ' .$order['id']
                                );

                                // HAVE PAYMENT PROFILE
                                if ($pp['id']) {
                                    $payment_data['card'] = array(
                                        'id'            => $pp['id'],
                                        'name'          => $payment_name,
                                        'number'        => $payment_lastfour,
                                        'expiration'    => $payment_expiration
                                    );

                                // USE CARD
                                } else {
                                    $payment_data['card'] = array(
                                        'name'          => $payment_name,
                                        'number'        => $_POST['payment'][$payment_method]['number'],
                                        'expiration'    => $payment_expiration,
                                        'zip'           => $_POST['payment'][$payment_method]['zip'],
                                        'cvv'           => $_POST['payment'][$payment_method]['cvv']
                                    );
                                }

                                // PROCESS PAYMENT
                                $payment_result = $_uccms['_account']->payment_charge($payment_data);

                                // TRANSACTION ID
                                $transaction_id = $payment_result['transaction_id'];

                                // TRANSACTION MESSAGE (ERROR)
                                $transaction_message = $payment_result['error'];

                            // PAYMENT INFO MISSING
                            } else {
                                BigTree::redirect('../');
                                exit;
                            }

                            // LOG TRANSACTION
                            $_uccms_ecomm->logTransaction(array(
                                'cart_id'               => $order['id'],
                                'status'                => ($transaction_id ? 'charged' : 'failed'),
                                'amount'                => $amount,
                                'method'                => $_POST['payment']['method'],
                                'payment_profile_id'    => $payment_result['profile_id'],
                                'name'                  => $_POST['payment'][$_POST['payment']['method']]['name'],
                                'lastfour'              => $payment_lastfour,
                                'expiration'            => $payment_expiration,
                                'transaction_id'        => $transaction_id,
                                'message'               => $transaction_message,
                                'ip'                    => ip2long($_SERVER['REMOTE_ADDR'])
                            ));

                            // CHARGE SUCCESSFUL
                            if ($transaction_id) {

                                // ADD TO ORDER INFO
                                $order['transaction_id'] = $transaction_id;

                                $columns = array(
                                    'status'    => 'charged'
                                );
                                if ($order['quote'] == 1) {
                                    $columns['quote'] = 2;
                                }
                                if ($balance == 0.00) {
                                    $columns['paid_in_full'] = 1;
                                }

                                // UPDATE ORDER
                                $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). " WHERE (`id`=" .$order['id']. ")";
                                sqlquery($cart_query);

                                $thanks = true;

                            // CHARGE FAILED
                            } else {

                                // IS NOT A QUOTE
                                if ($order['quote'] != 1) {

                                    // UPDATE ORDER
                                    $cart_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET `status`='failed' WHERE (`id`=" .$order['id']. ")";
                                    sqlquery($cart_query);

                                }

                                // SEND BACK TO CHECKOUT
                                $_uccms['_site-message']->set('error', 'Credit Card Error (' .$gateway->Message. ')');
                                BigTree::redirect($back_url);
                                exit;

                            }

                        }

                        ##########################
                        # EMAIL
                        ##########################

                        // IS QUOTE
                        if ($order['quote'] == 1) {

                            // EMAIL SETTINGS
                            $esettings = array(
                                'template'      => 'quote_payment-general.html', // catering (different variables?)
                                'subject'       => 'Quote Payment',
                                'order_id'      => $order['id']
                            );

                            $view_link = '<a href="' .WWW_ROOT . $_uccms_ecomm->storePath(). '/quote/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']). '" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">View Online</a>';

                        // IS ORDER
                        } else {

                            // EMAIL SETTINGS
                            $esettings = array(
                                'template'      => 'order_payment-general.html', // catering (different variables?)
                                'subject'       => 'Order Payment',
                                'order_id'      => $order['id']
                            );

                            $view_link = '<a href="' .WWW_ROOT . $_uccms_ecomm->storePath(). '/order/review/?id=' .$order['id']. '&h=' .stripslashes($order['hash']). '" style="display: inline-block; padding: 5px 14px; background-color: #59a8e9; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 3px;">View Online</a>';

                        }

                        // AFTER TAX ITEMS
                        if ($evars_items_aftertax) {
                            $evars_items_aftertax = '
                            <tr>
                                <td colspan="5" align="right" style="text-align: right;">
                                    ' .$evars_items_aftertax. '
                                </td>
                            </tr>
                            ';
                        }

                        // BILLING ADDRESS
                        $billing_address = stripslashes($order['billing_address1']);
                        if ($order['billing_address2']) {
                            $billing_address .= '<br />' .stripslashes($order['billing_address2']);
                        }

                        // SHIPPING ADDRESS
                        $shipping_address = stripslashes($order['shipping_address1']);
                        if ($order['shipping_address2']) {
                            $shipping_address .= '<br />' .stripslashes($order['shipping_address2']);
                        }

                        // NOTES
                        if ($order['notes']) {
                            $order_notes = '<div style="margin: 10px 0 10px 0;"><strong style="color: #666;">Notes:</strong> ' .stripslashes($order['notes']). '</div>';
                        }

                        // EMAIL VARIABLES
                        $evars = array(
                            'date'                  => date('n/j/Y'),
                            'time'                  => date('g:i A T'),
                            'order_id'              => $order['id'],
                            'billing_firstname'     => stripslashes($order['billing_firstname']),
                            'billing_lastname'      => stripslashes($order['billing_lastname']),
                            'billing_address'       => $billing_address,
                            'billing_city'          => stripslashes($order['billing_city']),
                            'billing_state'         => stripslashes($order['billing_state']),
                            'billing_zip'           => stripslashes($order['billing_zip']),
                            'shipping_firstname'    => stripslashes($order['shipping_firstname']),
                            'shipping_lastname'     => stripslashes($order['shipping_lastname']),
                            'shipping_address'      => stripslashes($shipping_address),
                            'shipping_city'         => stripslashes($order['shipping_city']),
                            'shipping_state'        => stripslashes($order['shipping_state']),
                            'shipping_zip'          => stripslashes($order['shipping_zip']),
                            'order_notes'           => $order_notes,
                            'items'                 => $evars_items,
                            'subtotal'              => '$' .number_format($order['subtotal'], 2),
                            'tax'                   => '$' .number_format($order['tax'], 2),
                            'items_aftertax'        => $evars_items_aftertax,
                            'shipping'              => '$' .number_format($order['shipping'], 2),
                            'total'                 => '$' .number_format($order['total'], 2),
                            'view_link'             => $view_link,
                            'amount_paid'           => number_format($amount, 2),
                            'balance'               => number_format($balance, 2)
                        );

                        // GRATUITY ENABLED
                        if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
                            $evars['gratuity'] = '
                            <tr>
                                <td colspan="5" align="right" style="padding-right: 10px; text-align: right;">
                                    Gratuity: $' .$order['gratuity']. '
                                </td>
                            </tr>
                            ';
                        }

                        // WHO TO SEND EMAILS TO
                        $emailtoa = array();
                        $emailtoa[$order['billing_email']] = $order['billing_email'];

                        // COPY OF ORDER TO ADMIN(S)
                        if ($ecomm['settings']['email_order_copy']) {
                            $emails = explode(',', stripslashes($ecomm['settings']['email_order_copy']));
                            foreach ($emails as $email) {
                                $emailtoa[$email] = $email;
                            }
                        }

                        // HAVE EMAILS TO SEND TO
                        if (count($emailtoa) > 0) {

                            $i = 1;

                            // LOOP
                            foreach ($emailtoa as $to_email => $to_name) {

                                // WHO TO SEND TO
                                $esettings['to_email'] = $to_email;

                                // SEND EMAIL
                                $eresult = $_uccms_ecomm->sendEmail($esettings, $evars);

                                // LOG FIRST
                                if ($i == 1) {

                                    if ($payment_method == 'check_cash') {
                                        $pm = 'Check / Cash';
                                    } else {
                                        $pm = 'credit card ending in ' .$payment_lastfour;
                                    }

                                    // LOG MESSAGE
                                    $_uccms_ecomm->logQuoteMessage(array(
                                        'order_id'      => $order['id'],
                                        'by'            => 0,
                                        'message'       => 'Paid $' .$amount. ' with ' .$pm. '.',
                                        'status'        => 'charged',
                                        'email_log_id'  => $eresult['log_id']
                                    ));

                                }

                                $i++;

                            }

                        }

                        // HAVE GLOBAL FREE SHIPPING
                        if ($order['shipping'] == 0.00) {

                            // ADDITIONAL ORDER INFO
                            $order['shipping_method'] = 'Free';

                        // NO GLOBAL FREE SHIPPING
                        } else {

                            // ADDITIONAL ORDER INFO
                            $order['shipping_method'] = stripslashes($shipping['method']). ' - ' .stripslashes($shipping['title']);

                        }

                        // ORDER DATA FOR REVSOCIAL API
                        $rs_orderdata['order']          = $order;
                        $rs_orderdata['order']['total'] = number_format($_uccms_ecomm->toFloat($order_total), 2);
                        $rs_orderdata['order']['paid']  = number_format($_uccms_ecomm->toFloat($amount), 2);
                        $rs_orderdata['items']          = $rs_items;

                        // RECORD ORDER WITH REVSOCIAL
                        $_uccms_ecomm->revsocial_recordOrder($order['id'], $order['customer_id'], $rs_orderdata);

                        // CLEAR CART ID
                        $_uccms_ecomm->clearCartID(true);

                        unset($_SESSION['ecomm']);

                    // UPDATE FAILED
                    } else {
                        $_uccms['_site-message']->set('error', 'Failed to update order. You were not charged.');
                    }

                // NO ITEMS IN ORDER
                } else {
                    $_uccms['_site-message']->set('error', 'You don\'t have any items in your order.');
                }

            // ORDER NOT FOUND
            } else {
                $_uccms['_site-message']->set('error', 'Order not found.');
            }

        // NO AMOUNT SPECIFIED
        } else {
            $_uccms['_site-message']->set('error', 'Payment amount not specified.');
        }

    // MISSING ORDER ID OR ORDER HASH
    } else {
        $_uccms['_site-message']->set('error', 'Required information missing.');
    }

// NOT PROCESSING
} else {
    $_uccms['_site-message']->set('error', 'Order payment form not submitted.');
}

if ($thanks) {
    $back_url .= '&thanks=true';
}

BigTree::redirect($back_url);
exit;

?>