<?php

// URL TO GO BACK TO
$back_url = '../?id=' .$_REQUEST['id']. '&h=' .$_REQUEST['h'];

// CLEAN UP
$id     = (int)$_REQUEST['id'];
$hash   = sqlescape($_REQUEST['h']);
$status = sqlescape($_REQUEST['status']);

// HAVE REQUIRED FIELDS
if (($id) && ($hash) && ($status)) {

    // STATUS ARRAY
    $stata = array(
        'accepted'  => 'accepted',
        'declined'  => 'declined'
    );

    // HAVE MATCHING STATUS
    if ($stata[$status]) {

        // GET ORDER INFO
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$id. ") AND (`hash`='" .$hash. "')";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // ORDER FOUND
        if ($order['id']) {

            // GET EXTRA INFO
            $extra = $_uccms_ecomm->cartExtra($order['id']);

            // DB COLUMNS
            $columns = array(
                'status'    => $stata[$status]
            );

            // APPROVED COLUMNS
            if ($status == 'accepted') {

                // RECORD IN REVSOCIL
                $revsocial_record = true;

                // GET ORDER ITEMS
                $citems = $_uccms_ecomm->cartItems($order['id']);

                // HAVE ITEMS
                if (count($citems) > 0) {

                    // INVENTORY CONTROL ENABLED
                    if ($_uccms_ecomm->getSetting('inventory_track')) {

                        // LOOP
                        foreach ($citems as $oitem) {

                            $ivars['item']          = $oitem;
                            $ivars['item']['id']    = $oitem['item_id'];

                            // ITEM OPTIONS
                            if ($oitem['options']) {
                                $ivars['attribute'] = json_decode(stripslashes($oitem['options']), true);
                            }

                            // SEE IF STILL GOOD IN CART
                            $check = $_uccms_ecomm->cartAddItem($id, $ivars, false, true);

                            unset($ivars);

                        }

                        // HAVE ERRORS
                        if ($_uccms['_site-message']->count('error') > 0) {
                            BigTree::redirect($back_url);
                            exit;
                        }

                    }

                    // ATTRIBUTE ARRAY
                    $attra = array();

                    // EXTENDED VIEW
                    $extended_view = true;

                    // LOOP THROUGH ITEMS
                    foreach ($citems as $oitem) {

                        // GET ITEM INFO
                        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                        // ADD ITEM TOTAL TO ORDER TOTAL
                        //$order['order_total'] += $item['other']['total'];

                        // TEMP EMAIL VAR
                        $tevar = '
                        <tr>
                            <td valign="top" style="padding: 8px;">
                                <img src="' .$item['other']['image_url']. '" alt="' .stripslashes($item['title']). '" height="50" />
                            </td>
                            <td width="50%" style="padding: 8px;">
                                <a href="' .$item['other']['link']. '">' .stripslashes($item['title']). '</a>';
                                if (count($item['other']['options']) > 0) {
                                    $tevar .= '<ul style="margin: 0; padding: 0 0 0 5px; list-style: outside none none; font-size: 0.8em;">';
                                        foreach ($item['other']['options'] as $option_id => $values) {
                                            if ($values[0]) {
                                                $tevar .= '<li>' .stripslashes($attra[$option_id]['title']). ': ' .implode(', ', $values). '</li>';
                                            }
                                        }
                                    $tevar .= '</ul>';
                                }
                            $tevar .= '
                            </td>
                            <td style="padding: 8px;">
                                ' .$item['other']['price_formatted']. '
                            </td>
                            <td style="padding: 8px;">
                                ' .number_format($oitem['quantity'], 0). '
                            </td>
                            <td style="padding: 8px;">
                                ' .$item['other']['total_formatted']. '
                            </td>
                        </tr>
                        ';

                        // IS AFTER TAX ITEM
                        if ($oitem['after_tax']) {
                            $evars_items_aftertax .= $tevar;
                        } else {
                            $evars_items .= $tevar;
                        }

                        // ADD TO QUANTITY COUNT
                        $rs_items['num_quantity'] += $oitem['quantity'];

                        // PRODUCT OPTIONS (ATTRIBUTES)
                        $options = array();
                        if (count($item['other']['options']) > 0) {
                            foreach ($_uccms_ecomm->item_attrAndOptionTitles($item, $attra) as $attr) {
                                $options[$attr['title']] = $attr['options'];
                            }
                        }

                        // ADD TO PRODUCTS ARRAY
                        $rs_items['products'][] = array(
                            'sku'       => stripslashes($item['sku']),
                            'name'      => stripslashes($item['title']),
                            'options'   => $options,
                            'quantity'  => $oitem['quantity'],
                            'total'     => $item['other']['total']
                        );

                        $subtotal += $item['other']['total'];

                    }

                    // HAVE TAX OVERRIDE
                    if ($order['override_tax']) {
                        $tax['total'] = number_format($order['override_tax'], 2);

                    // NORMAL TAX CALCULATION
                    } else {

                        // HAVE BILLING STATE AND/OR ZIP
                        if (($vals['contact']['state']) || ($vals['contact']['zip'])) {
                            $taxvals['country'] = 'United States';
                            $taxvals['state']   = $vals['contact']['state'];
                            $taxvals['zip']     = $vals['contact']['zip'];
                        }

                        //$taxvals['quote'] = false;

                        // GET TAX
                        $tax = $_uccms_ecomm->cartTax($order['id'], $citems, $taxvals);

                    }

                    // HAVE SHIPPING OVERRIDE
                    if ($order['override_shipping']) {
                        $shipping['markup'] = number_format($order['override_shipping'], 2);
                    } else {
                        $shipping['markup'] = 0.00;
                    }

                    // ADD VALUES TO ORDER ARRAY
                    $order['order_tax']       = $tax['total'];
                    $order['order_shipping']  = $shipping['markup'];

                }

                // CALCULATE GRATUITY
                if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
                    $order['order_gratuity'] = $_uccms_ecomm->orderGratuity($order);
                }

                // GET DISCOUNT
                $discount = $_uccms_ecomm->cartDiscount($order, $citems);

                // HAVE DISCOUNT
                if ($discount['discount']) {

                    $order['discount'] = $discount['discount'];

                    $evars_discount = '
                        <tr>
                            <td colspan="5" align="right" style="padding-right: 10px; text-align: right;">
                                After discount ('; if ($discount['coupon']['code']) { $evars_discount .= 'Coupon: ' .$discount['coupon']['code']. ' - '; } $evars_discount .= '$' .number_format($discount['discount'], 2). '): $' .$_uccms_ecomm->orderTotal($order). '
                            </td>
                        </tr>
                    ';

                }

                // CALCULATE TOTAL
                $order['order_total'] = $_uccms_ecomm->orderTotal($order);

                // DB COLUMNS
                $columns['quote']           = 2;
                $columns['status']          = 'placed';
                $columns['ip']              = ip2long($_SERVER['REMOTE_ADDR']);
                $columns['order_subtotal']  = $_uccms_ecomm->dbNumDecimal($subtotal);
                $columns['order_tax']       = $_uccms_ecomm->dbNumDecimal($order['order_tax']);
                $columns['order_aftertax']  = $_uccms_ecomm->dbNumDecimal($order['order_aftertax']);
                $columns['order_shipping']  = $_uccms_ecomm->dbNumDecimal($order['order_shipping']);
                $columns['order_gratuity']  = $order['order_gratuity'];
                $columns['order_total']     = $order['order_total'];

                // REVSOCIAL WEB TRACKING
                $_SESSION['uccmsAcct_webTracking']['events']['quote-accepted'] = $order['id'];

            }

            // UPDATE ORDER
            $update_query = "UPDATE `" .$_uccms_ecomm->tables['orders']. "` SET " .$_uccms_ecomm->createSet($columns). ", `dt`=NOW() WHERE (`id`=" .$order['id']. ")";

            // ORDER UPDATED
            if (sqlquery($update_query)) {

                // NO ORDER EXTRA YET
                if (!$oextra['id']) {

                    // CATERING
                    if ($_uccms_ecomm->storeType() == 'catering') {

                        // GET MEALTIMES
                        $mta = $_uccms_ecomm->stc->mealTimes();

                        $event_date     = '';
                        $event_time     = '';
                        $event_people   = 0;

                        // LOOP THROUGH ITEMS
                        foreach ($citems as $oitem) {

                            // GET ITEM INFO
                            include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/data/item_cart.php');

                            // HAS EXTRA
                            if ($oitem['extra']) {

                                // GET EXTRA
                                $oiextra = json_decode(stripslashes($oitem['extra']), true);

                                if ($oiextra['date']) {

                                    $event_date     = $oiextra['date'];
                                    $event_time     = $mta[$oiextra['mealtime']]['time'];
                                    $event_people   = $oitem['quantity'];

                                    break;

                                }

                            }

                        }

                    }

                    $oextra_table = '';

                    // STORE TYPE
                    switch ($_uccms_ecomm->storeType()) {

                        // GENERAL
                        case 'general':

                            $oextra_table = $_uccms_ecomm->tables['quote_general'];
                            $oextra_columns = array(
                                'order_id'              => $order['id']
                            );
                            break;

                        // CATERING
                        case 'catering':

                            $oextra_table = $_uccms_ecomm->tables['quote_catering'];
                            $oextra_columns = array(
                                'order_id'              => $order['id'],
                                'event_location1'       => '',
                                'event_location2'       => '',
                                'event_address1'        => '',
                                'event_address2'        => '',
                                'event_city'            => '',
                                'event_state'           => '',
                                'event_zip'             => '',
                                'event_phone'           => '',
                                'event_date'            => date('Y-m-d', strtotime($event_date)),
                                'event_num_people'      => $event_people,
                                'event_notes'           => stripslashes($order['notes']),
                                'service_time'          => date('H:i:00', strtotime($event_time)),
                                'service_type'          => '',
                                'contact_firstname'     => stripslashes($order['billing_firstname']),
                                'contact_lastname'      => stripslashes($order['billing_lastname']),
                                'contact_company'       => '',
                                'contact_address1'      => '',
                                'contact_address2'      => '',
                                'contact_city'          => '',
                                'contact_state'         => '',
                                'contact_zip'           => '',
                                'contact_phone'         => '',
                                'contact_email'         => ''
                            );

                            break;

                    }

                    // HAVE EXTRA TABLE
                    if ($oextra_table) {

                        // CREATE EXTRA RECORD
                        $oextra_query = "INSERT INTO `" .$oextra_table. "` SET " .$_uccms_ecomm->createSet($oextra_columns). "";
                        sqlquery($oextra_query);

                    }

                }

                // ACCEPTED
                if ($status == 'accepted') {

                    // UPDATE ORDER ITEMS
                    $update_query = "UPDATE `" .$_uccms_ecomm->tables['order_items']. "` SET (`quote`=0) WHERE (`cart_id`=" .$order['id']. ")";
                    sqlquery($update_query);

                    // INVENTORY CONTROL ENABLED
                    if ($_uccms_ecomm->getSetting('inventory_track')) {

                        // HAVE ITEMS IN CART
                        if (count($citems) > 0) {

                            // LOOP
                            foreach ($citems as $oitem) {

                                // ITEM OPTIONS
                                if ($oitem['options']) {
                                    $attrs = json_decode(stripslashes($oitem['options']), true);
                                } else {
                                    $attrs = array();
                                }

                                $vals = array(
                                    'available' => ($oitem['quantity'] * -1)
                                );

                                // ADJUST INVENTORY
                                $inventory_result = $_uccms_ecomm->item_changeInventory($oitem['item_id'], $_uccms_ecomm->cart_locationID(), $attrs, $vals, array(
                                    'log'   => array(
                                        'source'    => 'quote',
                                        'source_id' => $order['id']
                                    )
                                ));

                                unset($ivars);

                            }

                        }

                    }

                }

                // RECALCULATE ORDER
                $_uccms_ecomm->orderRecalculate($order['id']);

                // MESSAGE
                $message = ($_REQUEST['message'] ? '<p>' .nl2br($_REQUEST['message']). '</p>' : '');

                // LOG MESSAGE
                $_uccms_ecomm->logQuoteMessage(array(
                    'order_id'  => $order['id'],
                    'by'        => 0,
                    'message'   => $message,
                    'status'    => $stata[$status]
                ));

                // GET BALANCE
                $balance = $_uccms_ecomm->orderBalance($order['id']);

                // CALCULATE PAID
                $paid = number_format($order['order_total'] - $balance, 2);

                ##########################
                # ADMIN EMAIL
                ##########################

                // EMAIL SETTINGS
                $esettings = array(
                    'template'      => 'quote_review-' .$stata[$status]. '-store.html',
                    'subject'       => 'Quote ' .ucwords($stata[$status]),
                    'order_id'      => $order['id']
                );

                // CATERING
                if ($_uccms_ecomm->storeType() == 'catering') {
                    $name = trim(stripslashes($oextra['contact_firstname']. ' ' .$oextra['contact_lastname'] . ($oextra['contact_company'] ? ' (' .$oextra['contact_company']. ')' : '')));
                } else {
                    $name = trim($order['billing_firstname']. ' ' .$order['billing_lastname']);
                }

                // EMAIL VARIABLES
                $evars = array(
                    'date'          => date('n/j/Y'),
                    'time'          => date('g:i A T'),
                    'order_id'      => $order['id'],
                    'status'        => $stata[$status],
                    'name'          => $name,
                    'message'       => $message,
                    'admin_url'     => ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/quotes/edit/?id=' .$order['id']
                );

                // COPY OF ORDER TO ADMIN(S)
                if ($ecomm['settings']['email_order_copy']) {
                    $emails = explode(',', stripslashes($ecomm['settings']['email_order_copy']));
                    foreach ($emails as $email) {

                        // WHO TO SEND TO
                        $esettings['to_email'] = trim($email);

                        // SEND EMAIL
                        $result = $_uccms_ecomm->sendEmail($esettings, $evars);

                    }
                }

                ##########################
                # CUSTOMER EMAIL
                ##########################

                if ($status == 'accepted') {

                    // EMAIL SETTINGS
                    $esettings = array(
                        'template'      => 'order_placed-customer.html',
                        'subject'       => 'Order Placed',
                        'order_id'      => $order['id']
                    );

                    // AFTER TAX ITEMS
                    if ($evars_items_aftertax) {
                        $evars_items_aftertax = '
                        <tr>
                            <td colspan="5" align="right" style="text-align: right;">
                                ' .$evars_items_aftertax. '
                            </td>
                        </tr>
                        ';
                    }

                    // BILLING ADDRESS
                    $billing_address = $order['billing_address1'];
                    if ($order['billing_address2']) {
                        $billing_address .= '<br />' .$order['billing_address2'];
                    }

                    // SHIPPING ADDRESS
                    $shipping_address = $order['shipping_address1'];
                    if ($order['shipping_address2']) {
                        $shipping_address .= '<br />' .$order['shipping_address2'];
                    }

                    // NOTES
                    if ($order['order_notes']) {
                        $order_notes = '<div style="margin: 10px 0 10px 0;"><strong style="color: #666;">Notes:</strong> ' .$order['order_notes']. '</div>';
                    }

                    // EMAIL VARIABLES
                    $evars = array(
                        'date'                  => date('n/j/Y'),
                        'time'                  => date('g:i A T'),
                        'order_id'              => $order['id'],
                        'billing_firstname'     => $order['billing_firstname'],
                        'billing_lastname'      => $order['billing_lastname'],
                        'name'                  => trim($order['billing_firstname']. ' ' .$order['billing_lastname']),
                        'billing_address'       => $billing_address,
                        'billing_city'          => $order['billing_city'],
                        'billing_state'         => $order['billing_state'],
                        'billing_zip'           => $order['billing_zip'],
                        'shipping_firstname'    => $order['shipping_firstname'],
                        'shipping_lastname'     => $order['shipping_lastname'],
                        'shipping_address'      => $shipping_address,
                        'shipping_city'         => $order['shipping_city'],
                        'shipping_state'        => $order['shipping_state'],
                        'shipping_zip'          => $order['shipping_zip'],
                        'order_notes'           => $order_notes,
                        'items'                 => $evars_items,
                        'subtotal'              => '$' .number_format($subtotal, 2),
                        'discount_element'      => $evars_discount,
                        'tax'                   => '$' .number_format($order['order_tax'], 2),
                        'items_aftertax'        => $evars_items_aftertax,
                        'shipping'              => '$' .number_format($order['order_shipping'], 2),
                        'total'                 => '$' .number_format($order['order_total'], 2),
                        'payment'               => '$' .number_format($paid, 2),
                        'balance'               => '$' .$_uccms_ecomm->orderBalance($order['id']),
                        'view_url'              => '#',
                        //'store_contact_phone'   => $store_contact_phone
                    );

                    // HAVE ACCOUNT ID
                    if ($_uccms['_account']->userID()) {
                        $evars['view_url'] = WWW_ROOT . $_uccms_ecomm->storePath(). '/order/review/?id=' .$order['id']. '&h=' .$order_hash;
                    }

                    // GRATUITY ENABLED
                    if ($_uccms_ecomm->getSetting('gratuity_enabled')) {
                        $evars['gratuity'] = '
                        <tr>
                            <td colspan="5" align="right" style="padding-right: 10px; text-align: right;">
                                Gratuity: $' .number_format($order['order_gratuity'], 2). '
                            </td>
                        </tr>
                        ';
                    } else {
                        $evars['gratuity'] = '';
                    }

                    // WHO TO SEND EMAIL TO
                    $esettings['to_email'] = $order['billing_email'];

                    // SEND EMAIL
                    $result = $_uccms_ecomm->sendEmail($esettings, $evars);

                }

                // RECORD IN REVSOCIAL
                if ($revsocial_record) {

                    // QUANTITY COUNT
                    $rs_items['num_products'] = count($citems);
                    $rs_items['num_quantity'] = $rs_items['num_quantity'];

                    // ORDER DATA FOR REVSOCIAL API
                    $rs_orderdata = array(
                        'billing'           => array(
                            'firstname' => stripslashes($order['billing_firstname']),
                            'lastname'  => stripslashes($order['billing_lastname']),
                            'address1'  => stripslashes($order['billing_address1']),
                            'address2'  => stripslashes($order['billing_address2']),
                            'city'      => stripslashes($order['billing_city']),
                            'state'     => stripslashes($order['billing_state']),
                            'zip'       => stripslashes($order['billing_zip']),
                            'phone'     => stripslashes($order['billing_phone']),
                            'email'     => stripslashes($order['billing_email'])
                        ),
                        'shipping'      => array(
                            'firstname' => stripslashes($order['shipping_firstname']),
                            'lastname'  => stripslashes($order['shipping_lastname']),
                            'address1'  => stripslashes($order['shipping_address1']),
                            'address2'  => stripslashes($order['shipping_address2']),
                            'city'      => stripslashes($order['shipping_city']),
                            'state'     => stripslashes($order['shipping_state']),
                            'zip'       => stripslashes($order['shipping_zip']),
                            'phone'     => stripslashes($order['shipping_phone']),
                            'email'     => stripslashes($order['shipping_email'])
                        )
                    );

                    // ORDER DATA FOR REVSOCIAL API
                    $rs_orderdata['order']              = $order;
                    $rs_orderdata['order']['subtotal']  = number_format($_uccms_ecomm->toFloat($subtotal), 2);
                    $rs_orderdata['order']['total']     = number_format($_uccms_ecomm->toFloat($order['order_total']), 2);
                    $rs_orderdata['order']['paid']      = $paid;
                    $rs_orderdata['items']              = $rs_items;

                    // RECORD ORDER WITH REVSOCIAL
                    $_uccms_ecomm->revsocial_recordOrder($order['id'], $order['customer_id'], $rs_orderdata);

                }

            // FAILED TO UPDATE ORDER
            } else {
                $_uccms['_site-message']->set('error', 'Failed to update order.');
            }

        // ORDER NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Quote not found.');
        }

    // STATUS NOT FOUND
    } else {
        $_uccms['_site-message']->set('error', 'Specified status not an option.');
    }

// MISSING REQUIRED FIELDS
} else {
    $_uccms['_site-message']->set('error', 'Missing required fields.');
}

BigTree::redirect($back_url);

?>