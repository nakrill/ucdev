<?php

// IS REVIEWING QUOTE
if ($_REQUEST['quote_id']) {

    // SEND TO REVIEW SECTION
    BigTree::redirect('./review/?quote_id=' .$_REQUEST['quote_id']. '&h=' .$_REQUEST['h']);
    exit;

// IS REQUESTING QUOTE
} else {

    // SEND TO REVIEW SECTION
    BigTree::redirect('./request/?order_id=' .$_REQUEST['order_id']. '&h=' .$_REQUEST['h']);
    exit;

}

?>