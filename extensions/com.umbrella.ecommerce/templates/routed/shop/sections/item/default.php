<link rel="stylesheet" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/lib/swipebox.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.1/js/jquery.swipebox.min.js"></script>

<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/item.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/item.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/item.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/item.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="smallColumn col-md-3">

    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/cart_widget.php'); ?>
    <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/category_nav.php'); ?>

</div>

<div class="largeColumn col-md-9 item_container" itemscope itemtype="http://schema.org/Product">

    <meta itemprop="url" content="<?php echo $_uccms_ecomm->itemURL($item['id'], 0, $item); ?>" />
    <meta itemprop="sku" content="<?php echo stripslashes($item['sku']); ?>" />

    <?php

    // HAVE ITEM
    if ($item['id']) {

        // BREADCRUMB OPTIONS
        $bcoptions = array(
            'base'      => WWW_ROOT . $bigtree['path'][0]. '/',
            'rewrite'   => true,
            'link_last' => true
        );

        // BREADCRUMBS
        include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/breadcrumbs.php');

        ?>

        <form id="form_item" action="../../cart/" method="post" enctype="multipart/form-data">
        <?php if ($purchasing_enabled) { ?>
            <input type="hidden" name="do" value="add" />
        <?php } ?>
        <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />
        <input type="hidden" name="item[category_id]" value="<?php echo $category_id; ?>" />
        <input type="hidden" name="ciid" value="<?php echo $ci['id']; ?>" />

        <div class="row" style="position: relative;">

            <div class="images_container col-md-6">
                <div class="contain">

                    <div class="main">

                        <?php

                        // HAVE IMAGES
                        if (count($imagea) > 0) {
                            foreach ($imagea as $i => $image) {
                                ?>
                                <a id="item-image-<?php echo $image['id']; ?>" href="<?php echo $image['url']; ?>" target="blank" class="swipebox <?php if ($i == 0) { ?>active<?php } ?>" rel="gallery-1"><img src="<?php echo $image['url']; ?>" alt="<?php if ($image['caption']) { echo stripslashes($image['caption']); } else { echo stripslashes($item['title']); } ?>" itemprop="image" /></a>
                                <?php
                            }
                        // NO IMAGES
                        } else {
                            ?>
                            <img src="<?php echo WWW_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/images/item_no-image.jpg'; ?>" alt="<?php echo stripslashes($item['title']); ?>" />
                            <?php
                        }

                        ?>

                    </div>

                    <?php

                    // HAVE MORE THAN ONE IMAGE OR HAVE VIDEOS
                    if ((count($imagea) > 1) || (count($videoa) > 0)) {
                        $ii = 0;
                        ?>
                        <div class="thumbs row">
                            <?php

                            // LOOP THROUGH IMAGES
                            foreach ($imagea as $image) {
                                if ($image['image']) {
                                    if ($image['caption']) {
                                        $caption = stripslashes($image['caption']);
                                    } else {
                                        $caption = stripslashes($item['title']);
                                    }
                                    ?>
                                    <div class="thumb image <?php if ($ii == 0) { ?>active<?php } ?> col-sm-4">
                                        <a href="#" title="<?php echo $caption; ?>" data-id="<?php echo $image['id']; ?>"><img src="<?php echo $image['url_thumb']; ?>" alt="<?php echo $caption; ?>" /></a>
                                    </div>
                                    <?php
                                    $ii++;
                                }
                            }

                            // LOOP THROUGH VIDEOS
                            foreach ($videoa as $video) {
                                if (($video['video']) && ($video['info']['thumb']))  {
                                    if ($video['caption']) {
                                        $caption = stripslashes($video['caption']);
                                    } else {
                                        $caption = stripslashes($item['title']);
                                    }
                                    ?>
                                    <div class="thumb video <?php if ($ii == 0) { ?>active<?php } ?>">
                                        <a href="<?php echo $video['video']; ?>" title="<?php echo $caption; ?>" target="_blank" class="swipebox-video" rel="gallery-1"><img src="<?php echo $video['url_thumb']; ?>" alt="<?php echo $caption; ?>" /></a>
                                    </div>
                                    <?php
                                    $ii++;
                                }
                            }

                            ?>
                        </div>
                        <?php
                    }

                    ?>

                </div>
            </div>

            <div class="item_info col-md-6">
                <div class="contain">

                    <?php

                    // HAVE TITLE
                    if ($item['title']) {
                        ?>
                        <h1 class="item-title" itemprop="name"><?php echo stripslashes($item['title']); ?></h1>
                        <?php
                    }

                    // <h2 itemprop="brand">Brand Name</h2>

                    ?>

                    <div class="price <?php if ($price == 0.00) { echo 'free'; } else if ($sale) { echo 'sale'; } ?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <meta itemprop="priceCurrency" content="USD" />

                        <?php if ($purchasing_enabled) { ?>
                            <link itemprop="availability" href="http://schema.org/InStock" />
                        <?php } else { ?>
                            <link itemprop="availability" href="http://schema.org/OutOfStock" />
                        <?php } ?>

                        <?php if ($item['quote']) { ?>
                            Add to cart for quote.
                        <?php } else if ($item['hide_price']) { ?>
                            Add to cart to see price.
                        <?php } else { ?>
                            <span class="value">
                                <?php
                                if ($price == 0.00) {
                                    echo 'Free';
                                } else {
                                    echo '$<span itemprop="price">' .number_format($price, 2). '</span>';
                                }
                                ?>
                            </span>
                        <?php } ?>

                    </div>

                    <?php

                    // HAVE SHORT DESCRIPTION
                    if ($item['description_short']) {
                        ?>
                        <div class="description_short">
                            <?php echo stripslashes($item['description_short']); ?>
                        </div>
                        <?php
                    }

                    ?>

                    <?php

                    // STORE TYPE IS BOOKING AND ITEM IS BOOKABLE
                    if (($_uccms_ecomm->storeType() == 'booking') && ($item['booking_enabled'])) {
                        ?>
                        <div class="booking">
                            <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/store_types/booking/item_booking.php'); ?>
                        </div>
                        <?php
                    }

                    // IS PACKAGE OR HAVE ATTRIBUTES
                    if (($item['type'] == 3) || (count($attra) > 0)) {
                        ?>
                        <div class="attributes">
                            <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/item_attributes.php'); ?>
                        </div>
                        <?php
                    }

                    ?>

                    <?php if ($purchasing_enabled) { ?>
                        <div class="qty">
                            <fieldset>
                                <label>Quantity: </label>
                                <input type="text" name="item[quantity]" value="<?php if ($_REQUEST['quantity']) { echo (int)$_REQUEST['quantity']; } else { echo 1; } ?>" />
                                <?php if ($qty_min > 0) { ?>
                                    <label><small>Minimum: <?php echo number_format($qty_min, 0); ?></small></label>
                                <?php } ?>
                            </fieldset>
                        </div>
                    <?php } ?>

                    <?php if ($purchasing_enabled) { ?>
                        <div class="add-to-cart">
                            <button type="submit" name="" class="btn btn-primary">
                                <?php if ($ci['id']) { ?>Update Cart<?php } else { ?>Add To Cart<?php } ?> <i class="fa fa-shopping-cart"></i>
                            </button>
                        </div>
                    <?php } ?>

                </div>
            </div>

        </div>

        </form>

        <?php

        // HAVE DESCRIPTION
        if ($item['description']) {
            ?>
            <div class="description">
                <h3>Description</h3>
                <div itemprop="description">
                    <?php echo stripslashes($item['description']); ?>
                </div>
            </div>
            <?php
        }

        /*
        // HAVE ATTRIBUTES
        if (count($attra) > 0) {
            ?>
            <script src="<?=STATIC_ROOT;?>extensions/<?php echo $_uccms_ecomm->Extension; ?>/js/lib/jquery-ui-timepicker-addon.js"></script>
            <script type="text/javascript">
                $('.date_picker').datepicker({ dateFormat: 'mm/dd/yy', duration: 200, showAnim: 'slideDown' });
                $('.time_picker').timepicker({ duration: 200, showAnim: "slideDown", ampm: true, hourGrid: 6, minuteGrid: 10, timeFormat: "hh:mm tt" });
            </script>
            <?php
        }
        */

    // ITEM NOT FOUND
    } else {
        ?>
        Item not found.
        <?php
    }

    ?>

</div>