<?php

// SIMPLE SECURITY
if ($_REQUEST['key'] == md5($bigtree['config']['db']['name'])) {

    // BLANK LAYOUT
    $bigtree['layout'] = 'blank';

    // EVENTS ARRAY
    $ea = array();

    // CUSTOM FILE
    if (file_exists(dirname(__FILE__). '/store-types/custom.php')) {
        include(dirname(__FILE__). '/store-types/custom.php');
    } else {
        include(dirname(__FILE__). '/store-types/' .$_uccms_ecomm->storeType(). '.php');
    }

    /*
    $event = array(
        'id'            => 'ecomm-order-1',
        'title'         => 'Test',
        'description'   => 'This is the description.',
        'start'         => strtotime('2016-06-25 08:00:00'),
        'end'           => strtotime('2016-06-25 14:00:00'),
        'location'      => '',
        'url'           => ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$eorder['order_id'],
        'dt_updated'    => ''
    );

    $ea[] = $_uccms_ecomm->iCal_createEvent($event);
    */

    // OUTPUT iCal CODE
    echo iCal_output($ea, false);

    die();

}

?>