<?php

$start  = '2017-01-01 00:00:00';
$end    = date('Y-m-d H:i:s', strtotime('+2 Years'));

$booking_params = array();

// BY CALENDAR
if ($_REQUEST['calendar_id']) {

    // GET BOOKINGS
    $bookings = $_uccms_ecomm->stc->calendarBookings((int)$_REQUEST['calendar_id'], strtotime($start), strtotime($end), $booking_params);

// OTHER
} else {

    // BY ITEM
    if ($_REQUEST['item_id']) {
        $booking_params['item_id'] = (int)$_REQUEST['item_id'];
    }

    // GET BOOKINGS
    $bookings = $_uccms_ecomm->stc->getBookings(strtotime($start), strtotime($end), $booking_params);

}

// HAVE BOOKINGS
if (count($bookings) > 0) {

    $ia = array();

    // LOOP
    foreach ($bookings as $booking) {

        // GET ORDER INFO
        $order_query = "SELECT * FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$booking['order_id']. ")";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // NOT DELETED
        if ($order['status'] != 'deleted') {

            // DON'T HAVE ITEM INFO YET
            if (!$ia[$booking['item_id']]['id']) {

                // GET ITEM INFO
                $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$booking['item_id']. ")";
                $item_q = sqlquery($item_query);
                $item = sqlfetch($item_q);

            }

            $title = trim(stripslashes($order['billing_firstname']. ' ' .$order['billing_lastname']));

            $description = '';

            if ($item['title']) {
                $description .= stripslashes($item['title']). "\\n";
            }

            $description .= ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$order['id'];

            // EVENT INFO ARRAY
            $event = array(
                'id'            => 'ecomm-order-' .$order['id']. '-booking-' .$booking['id'],
                'title'         => $title,
                'description'   => $description,
                'start'         => strtotime($booking['dt_from']),
                'end'           => strtotime($booking['dt_to'])+1,
                //'location'      => implode(', ', $loca),
                'url'           => ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$order['id'],
                //'dt_updated'    => ''
            );

            // ADD TO EVENTS ARRAY
            $ea[] = iCal_createEvent($event);

        }

    }

}

?>