<?php

// GET ORDERS
$eorder_query = "
SELECT qc.*
FROM `" .$_uccms_ecomm->tables['quote_catering']. "` AS `qc`
INNER JOIN `" .$_uccms_ecomm->tables['orders']. "` AS `o` ON qc.order_id=o.id
WHERE (o.status IN ('accepted','placed','charged'))
";
$eorder_q = sqlquery($eorder_query);

// HAVE ORDERS
if (count(sqlrows($eorder_q)) > 0) {

    // GET MEALTIMES
    $mta = $_uccms_ecomm->stc->mealTimes();

    // LOOP
    while ($eorder = sqlfetch($eorder_q)) {

        // GET ORDER INFO
        $order_query = "SELECT `status`, `quote` FROM `" .$_uccms_ecomm->tables['orders']. "` WHERE (`id`=" .$eorder['order_id']. ")";
        $order_q = sqlquery($order_query);
        $order = sqlfetch($order_q);

        // NOT DELETED
        if ($order['status'] != 'deleted') {

            // DEFAULT TITLE
            if ($eorder['location1']) {
                $title = stripslashes($eorder['location1']);
            } else if ($eorder['contact_company']) {
                $title = stripslashes($eorder['contact_company']);
            } else {
                $title = trim(stripslashes($eorder['contact_firstname']. ' ' .$eorder['contact_lastname']));
            }
            if (!$title) $title = 'Event';

            $loca = array();
            if ($eorder['event_address1']) {
                $loca[] = stripslashes($eorder['event_address1']);
            }
            if ($eorder['event_address2']) {
                $loca[] = stripslashes($eorder['event_address2']);
            }
            if ($eorder['event_city']) {
                $loca[] = stripslashes($eorder['event_city']);
            }
            if ($eorder['event_state']) {
                $loca[] = stripslashes($eorder['event_state']);
            }
            if ($eorder['event_zip']) {
                $loca[] = stripslashes($eorder['event_zip']);
            }

            $oc = 0;

            // GET ORDER ITEMS
            $citems = $_uccms_ecomm->cartItems($eorder['order_id']);

            // HAVE ITEMS
            if (count($citems) > 0) {

                // ITEM DATE AND TIME ARRAY FOR ORDER
                $idta = array();

                // LOOP
                foreach ($citems as $item) {

                    // HAS EXTRA
                    if ($item['extra']) {

                        // GET EXTRA
                        $extra = json_decode(stripslashes($item['extra']), true);

                        // HAVE DATE
                        if ($extra['date']) {

                            // ITEM DATE AND TIME
                            $dt = trim($extra['date']. ' ' .$mta[$extra['mealtime']]['time']);

                            // NOT IN ORDER ITEM DATE AND TIME ARRAY YET
                            if (!$idta[$dt]) {

                                // HAVE MEALTIME
                                if ($mta[$extra['mealtime']]['title']) {
                                    $item_title = $mta[$extra['mealtime']]['title']. ' - ' .$title;
                                }

                                // EVENT INFO ARRAY
                                $event = array(
                                    'id'            => 'ecomm-order-' .$eorder['order_id']. '-item-' .$item['id'],
                                    'title'         => $item_title,
                                    'description'   => 'Details at: ' .ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$eorder['order_id'],
                                    'start'         => strtotime($extra['date']. ' ' .$mta[$extra['mealtime']]['time']),
                                    'end'           => strtotime($extra['date']. ' ' .$mta[$extra['mealtime']]['time']),
                                    'location'      => implode(', ', $loca),
                                    'url'           => ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$eorder['order_id'],
                                    //'dt_updated'    => ''
                                );

                                // ADD TO EVENTS ARRAY
                                $ea[] = iCal_createEvent($event);

                                $idta[$dt] = $dt;

                                $oc++;

                            }

                        }

                    }

                }

            }

            // NO ITEMS - USE MAIN EVENT INFO
            if ($oc == 0) {

                // EVENT INFO ARRAY
                $event = array(
                    'id'            => 'ecomm-order-' .$eorder['order_id'],
                    'title'         => $title,
                    'description'   => 'Details at: ' .ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$eorder['order_id'],
                    'start'         => strtotime($eorder['event_date']. ' ' .$eorder['service_time']),
                    'end'           => strtotime($eorder['event_date']. ' ' .$eorder['service_time']),
                    'location'      => implode(', ', $loca),
                    'url'           => ADMIN_ROOT . $_uccms_ecomm->Extension. '*ecommerce/orders/edit/?id=' .$eorder['order_id'],
                    //'dt_updated'    => ''
                );

                // ADD TO EVENTS ARRAY
                $ea[] = iCal_createEvent($event);

            }

        }

    }

}

?>