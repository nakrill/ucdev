<?php

// http://davidjbradshaw.github.io/iframe-resizer/

header('Content-Type: application/javascript');

// IFRAME VARIABLES
$iframe = array(
    'id'    => ($_REQUEST['iframe_id'] ? $_REQUEST['iframe_id'] : 'widget-' .rand(1111, 9999). '-iframe'),
    'width' => ($_REQUEST['iframe_width'] ? $_REQUEST['iframe_width'] : '100%'),
    'url'   => STATIC_ROOT . $_uccms_ecomm->storePath(). '/widget/'
);

$url_dirs = array();

if ($_REQUEST['store_type']) {
    $url_dirs[] = 'store_type';
    $url_dirs[] = preg_replace('/[^0-9a-zA-Z_-]/', '', $_REQUEST['store_type']);
}

if ($_REQUEST['what']) {
    $url_dirs[] = preg_replace('/[^0-9a-zA-Z_-]/', '', $_REQUEST['what']);
}

// HAVE URL DIRS
if (count($url_dirs) > 0) {
    $iframe['url'] .= implode('/', $url_dirs). '/';
}

$widget_iframe_url = $iframe['url']. '?';

// HAVE OTHER VARIABLES
if (count($_REQUEST['vars']) > 0) {
    $widget_iframe_url .= http_build_query($_REQUEST['vars']). '&';
}

// CONTAINER
$container_id = ($_REQUEST['container_id'] ? $_REQUEST['container_id'] : 'uccms-widget-container');

?>

document.write('<script type="text/javascript" src="<?php echo STATIC_ROOT; ?>js/lib/iframeResizer.min.js"></script>');

// LOAD JQUERY
function widget_loadjQuery(oCallback) {
    if (typeof jQuery == 'undefined') {
        var oScript = document.createElement('script');
        oScript.type = 'text/javascript';
        oScript.src = '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js';
        oScript.onload = oCallback;
        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete') {
                oCallback();
            }
        }
        document.getElementsByTagName('head')[0].appendChild(oScript);
    } else {
        oCallback();
    }
}

// DO - LOAD JQUERY
widget_loadjQuery(function() {

    jQuery(document).ready(function($) {

        $('#<?php echo $container_id; ?>').html('<iframe id="<?php echo $iframe['id']; ?>" src="<?php echo $widget_iframe_url; ?>referrer_host=' +document.location.hostname+ '" width="<?php echo $iframe['width']; ?>" scrolling="no" style="border: 0px none;"></iframe>');

        var isOldIE = (navigator.userAgent.indexOf('MSIE') !== -1);

        $('#<?php echo $iframe['id']; ?>').iFrameResize({
            log: false,
            heightCalculationMethod: isOldIE ? 'max' : 'lowestElement'
        });

    });

});
