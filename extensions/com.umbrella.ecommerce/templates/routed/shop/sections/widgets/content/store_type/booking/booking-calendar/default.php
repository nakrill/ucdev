<?php

// STORE TYPE IS BOOKING
if ($_uccms_ecomm->storeType() == 'booking') {

    // MULTIPLE ITEM ID'S
    if ($_REQUEST['item_ids']) {

        // CONVERT AND CLEAN
        $item_ids = array_map(function($val) {
            return (int)trim($val);
        }, explode(',', $_REQUEST['item_ids']));

        // HAVE ITEM ID'S
        if (count($item_ids) > 0) {

            // HAVE MORE THAN ONE
            if (count($item_ids) > 1) {

                $items = array();

                // LOOP
                foreach ($item_ids as $item_id) {

                    // GET ITEM
                    $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item_id. ") AND (`active`=1) AND (`deleted`=0)";
                    $item_q = sqlquery($item_query);
                    $item = sqlfetch($item_q);

                    // ITEM IS BOOKABLE
                    if ($item['booking_enabled']) {
                        $items[$item['id']] = $item;
                    }

                }

                // HAVE BOOKABLE ITEMS
                if (count($items) > 0) {

                    ?>

                    <style type="text/css">

                        #select_item {
                            width: 100%;
                        }

                    </style>

                    <script type="text/javascript">

                        $(document).ready(function() {

                            // UPDATE ON ITEM ID CHANGE
                            $('#select_item').change(function(e) {
                                var url = window.location.href.replace(new RegExp("[&;]?item_id=[^&;]+", "gi"), '');
                                url += '&item_id=' +$(this).val();
                                window.location.href = url;
                            });

                        });

                    </script>

                    <select id="select_item" name="item_id" class="form-control">
                        <option value="">Select your event</option>
                        <?php foreach ($items as $item) { ?>
                            <option value="<?php echo $item['id']; ?>" <?php if ($item['id'] == $_REQUEST['item_id']) { ?>selected="selected"<?php } ?>><?php echo stripslashes($item['title']); ?></option>
                        <?php } ?>
                    </select>

                    <?php

                }

                // HAVE ONE
            } else {

                // NO ITEM ID SET YET
                if (!$_REQUEST['item_id']) {
                    $_REQUEST['item_id'] = $item_ids[0];
                }

            }

        }

    }

    // HAVE ITEM ID
    if ($_REQUEST['item_id']) {

        $item_id = (int)$_REQUEST['item_id'];

        // GET ITEM
        $item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['items']. "` WHERE (`id`=" .$item_id. ") AND (`active`=1) AND (`deleted`=0)";
        $item_q = sqlquery($item_query);
        $item = sqlfetch($item_q);

        // HAVE ITEM
        if ($item['id']) {

            // GET BOOKING ITEM INFO
            $booking_item_query = "SELECT * FROM `" .$_uccms_ecomm->tables['booking_items']. "` WHERE (`id`=" .$item['id']. ")";
            $booking_item_q = sqlquery($booking_item_query);
            $booking_item = sqlfetch($booking_item_q);

            // MINIMUM QUANTITY
            $qty_min = $_uccms_ecomm->itemQtyMin($item);

            // ATTRIBUTES ARRAY
            $attra = $_uccms_ecomm->itemAttributes($item['id']);

            // OPTIONS
            if ($_REQUEST['options']) {
                parse_str(urldecode($_REQUEST['options']), $selattra);
            }

            // ITEM IS BOOKABLE
            if ($item['booking_enabled']) {

                ?>

                <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/item.css?v=1.0.0" />
                <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/item.css?v=1.0.0" />

                <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/widgets/store_type/booking/booking-calendar.css?v=1.0.0" />
                <script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/widgets/store_type/booking/booking-calendar.js?v=1.0.0"></script>

                <div id="ecommContainer">

                    <div class="item_container">

                        <form id="form_item" action="<?php echo WWW_ROOT . $_uccms_ecomm->storePath(); ?>/cart/" method="post" enctype="multipart/form-data" target="_parent">
                            <input type="hidden" name="do" value="add" />
                            <input type="hidden" name="item[id]" value="<?php echo $item['id']; ?>" />
                            <input type="hidden" name="referrer_host" value="<?php echo urlencode($_REQUEST['referrer_host']); ?>" />

                            <input type="hidden" name="item[quantity]" value="1" />

                            <div class="booking">

                                <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/store_types/booking/item_booking.php'); ?>

                            </div>

                            <div class="order_info">

                                <fieldset>
                                    <label>First Name</label>
                                    <input type="text" name="order[billing_firstname]" value="<?php echo $_REQUEST['billing_firstname']; ?>" class="form-control" />
                                </fieldset>

                                <fieldset>
                                    <label>Last Name</label>
                                    <input type="text" name="order[billing_lastname]" value="<?php echo $_REQUEST['billing_lastname']; ?>" class="form-control" />
                                </fieldset>

                                <fieldset>
                                    <label>Email</label>
                                    <input type="text" name="order[billing_email]" value="<?php echo $_REQUEST['billing_email']; ?>" class="form-control" />
                                </fieldset>

                            </div>

                            <div class="buttons">
                                <input type="submit" value="Book Now" class="submit button btn btn-primary" />
                            </div>

                        </form>

                    </div>

                </div>

                <?php

                // BOOKING NOT ENABLED FOR ITEM
            } else {
                echo this_widgetError('Item cannot be booked.');
            }

            // ITEM NOT FOUND
        } else {
            echo this_widgetError('Item not found.');
        }

        // ITEM ID NOT SPECIFIED
    } else {

        // MULTIPLE ITEM ID'S NOT SPECIFIED
        if (!$_REQUEST['item_ids']) {
            echo this_widgetError('Item ID not specified.');
        }

    }

// STORE TYPE IS NOT BOOKING
} else {
    echo this_widgetError('Booking not enabled.');
}

?>