<?php

// JAVASCRIPT BASE
if ($bigtree['path'][2] == 'embed.js') {
    include(dirname(__FILE__). '/embed.js.php');
    die();
}

$bigtree['layout'] = 'blank';

?>
<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">

        <title>Widget</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">

        <link rel="stylesheet" type="text/css" href="/css/master-min.css?v=1.0.0" />

        <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/master.css?v=1.0.0" />
        <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/master.css?v=1.0.0" />

        <style type="text/css">

            body {
                margin: 0px;
                padding: 0px;
            }

            .site-messages {
                margin: 10px;
            }

            .widget_error {
                padding: 15px;
                text-align: center;
            }

        </style>

        <?php if ($_REQUEST['external_css']) { ?>
            <link rel="stylesheet" type="text/css" href="<?php echo str_replace(array('>','<'), '', $_REQUEST['external_css']); ?>" />
        <?php } ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    </head>

    <body>

        <?php

        // INCLUDE FILE
        $include_file = '';

        // HAVE COMMANDS
        if (count($bigtree['commands']) > 1) {

            $base = array_shift($bigtree['commands']);

            // GET BIGTREE ROUTING
            list($include, $commands) = BigTree::route(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/sections/widgets/content/', $bigtree['commands']);

            // FILE TO INCLUDE
            if ($include) {
                $include_file = $include;
            }

        }

        // HAVE FILE TO INCLUDE
        if ($include_file) {

            // INCLUDE ROUTED FILE
            include($include_file);

        // WIDGET NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Widget not found.');
        }

        ?>

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>

        <script>
            var iFrameResizer = {}
        </script>
        <script type="text/javascript" src="/js/lib/iframeResizer.contentWindow.min.js" defer></script>

    </body>

</html>

<?php

die();

// WIDGET ERROR
function this_widgetError($content='', $class='') {
    $out = '<div class="site-messages container"><div class="site-message type-error ' .$class. '"><i class="fa fa-frown-o"></i><ul><li>' .$content. '</li></ul></div></div>';
    return $out;
}

?>