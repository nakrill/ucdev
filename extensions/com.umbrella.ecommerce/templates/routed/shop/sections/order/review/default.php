<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<h3>Order Review</h3>

<?php

// ORDER FOUND
if ($order['id']) {

    // HAVE ITEMS
    if ($num_items > 0) {

        ?>

        <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/quote_request.css" />
        <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/quote_request.css" />
        <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/quote_review.css" />
        <link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/quote_review.css" />
        <script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/quote_review.js"></script>
        <script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/quote_review.js"></script>

        <?php include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/templates/routed/shop/elements/quote/review.php'); ?>

        <?php

    // NO ITEMS
    } else {
        ?>
        Order has no items.
        <?php
    }

// ORDER NOT FOUND
} else {
    ?>
    Order not found.
    <?php
}

?>