<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/master/login.css" />
<link rel="stylesheet" type="text/css" href="/extensions/<?=$_uccms_ecomm->Extension;?>/css/custom/login.css" />
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/master/login.js"></script>
<script src="/extensions/<?=$_uccms_ecomm->Extension;?>/js/custom/login.js"></script>

<?php

// DISPLAY ANY SITE MESSAGES
echo $_uccms['_site-message']->display();

?>

<div class="grid">

    <div class="login col-1-2">

        <h3>Returning Customer</h3>

        <form action="" method="post">

        <fieldset>
            <input type="email" name="email" value="<?php echo $_REQUEST['email']; ?>" placeholder="Email" />
        </fieldset>

        <fieldset style="margin-bottom: 0px;">
            <input type="password" name="password" placeholder="Password" />
            <div><small><a href="/account/forgot-password/">Forgot password?</a></small></div>
        </fieldset>

        <fieldset style="margin-top: -10px;">
            <input type="checkbox" name="remember" value="1" <?php if ($_REQUEST['remember']) { ?>checked="checked"<?php } ?> /> Remember me
        </fieldset>

        <fieldset>
            <input type="submit" value="Sign In to Checkout" />
        </fieldset>

        </form>

    </div>

    <div class="register col-1-2">

        <h3>New Customer</h3>

        <div style="padding-bottom: 1em;">
            You will have the option to create an account on the checkout page.
        </div>

        <div>
            <a href="../checkout/persons/" class="button">Continue</a>
        </div>

    </div>

</div>