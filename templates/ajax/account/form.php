<div <?php if ($_REQUEST['el_id']) { ?>id="<?php echo $_REQUEST['el_id']; ?>"<?php } ?> class="<?php if ($_REQUEST['el_class']) { echo $_REQUEST['el_class']; } ?>" style="display: block;">
    <h2 class="head"><?php if ($_REQUEST['el_class']) { echo $_REQUEST['heading']; } else { echo 'Login'; } ?></h2>
    <?php

    $login_vars = $_REQUEST;

    $login_vars['ajax'] = true;

    if ($_REQUEST['goto']) {
        $after = 'window.location.href="' .$_REQUEST['goto']. '"';
    } else {
        $after = 'location.reload();';
    }

    $login_vars['login_callback'] = "
    if (data.success) {
        " .$after. "
    } else {
        $('#uccms_login-register-' +data.el_id+ ' .col-login .message').html(data.message).addClass('type-error').show();
    }
    ";

    include(SERVER_ROOT. 'templates/elements/login-register.php');

    ?>

</div>