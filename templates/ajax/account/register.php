<?php

// CUSTOM FILE
$custom_file = dirname(__file__). '/register_custom.php';

// CUSTOM FILE EXISTS
if (file_exists($custom_file)) {
    include($custom_file);

// NO CUSTOM FILE
} else {

    $out = array();

    // REGISTRATIONS ARE ENABLED
    if ($_uccms['_account']->registrationsEnabled()) {

        // PAGE META
        $bigtree['page']['title'] = 'Account Register';

        // LOGGED IN
        if ($_uccms['_account']->loggedIn()) {
            $out['message'] = 'You are already logged in.';

        // NOT LOGGED IN
        } else {

            // FORM SUBMITTED
            if ($_POST['email']) {

                // ACCOUNT DETAILS
                $details = array(
                    'firstname' => $_POST['firstname'],
                    'lastname'  => $_POST['lastname']
                );

                // CREATE ACCOUNT
                $create = $_uccms['_account']->register($_POST['email'], $_POST['password'], $_POST['email'], $details);

                // SUCCESS
                if ($create['success'] == true) {

                    $out['success'] = true;

                    $out['message'] = 'Account created! <a href="../">My account.</a>';

                    // LOGIN
                    $login = $_uccms['_account']->login($_POST['email'], $_POST['password'], false, false);

                    $out['login'] = $login;

                    // HAVE E-COMMERCE
                    if (is_dir(SERVER_ROOT. 'extensions/com.umbrella.ecommerce/')) {

                        // ADD PRICEGROUP
                        $pg_query = "INSERT INTO `uccms_ecommerce_customers` SET `id`=" .$create['id']. ", `pricegroups`=1";
                        sqlquery($pg_query);

                    }

                // FAILED
                } else {

                    // HAVE ERROR
                    if ($create['error']) {
                        $out['message'] = $create['message'];
                    } else {
                        $out['message'] = 'Failed to create account.';
                    }

                }

            }

        }

    // REGISTRATIONS NOT ENABLED
    } else {
        $out['message'] = 'Registrations are not enabled.';
    }

    echo json_encode($out);

}

?>