<?php

// CUSTOM FILE
$custom_file = dirname(__file__). '/login_custom.php';

// CUSTOM FILE EXISTS
if (file_exists($custom_file)) {
    include($custom_file);

// NO CUSTOM FILE
} else {

    $out = array();

    // CLEAN UP
    $username = trim($_REQUEST['email']);
    $password = trim($_REQUEST['password']);

    // HAVE USERNAME AND PASSWORD
    if (($username) && ($password)) {

        // LOGIN
        $login = $_uccms['_account']->login($username, $password, $_REQUEST['remember'], false);

        // LOGIN SUCCESSFUL
        if ($login['success'] == true) {

            $out['success'] = true;
            $out['message'] = 'You have been logged in. <a href="../">My account.</a>';

        // LOGIN FAILED
        } else {

            // HAVE ERROR
            if ($login['error']) {

                $out['message'] = $login['message'];

                // TRIES REMAINING SPECIFIED
                if ($login['tries_remaining']) {
                    $out['message'] = 'You have <strong>' .$login['tries_remaining']. ' tries remaining</strong>.';
                }

                // TEMPORARY SUSPENSION
                if ($login['error'] == 5) {
                    $out['message'] = 'Please wait <strong>' .$login['minutes']. ' minutes</strong> to try again.';
                }

            // GENERAL ERROR
            } else {
                $out['message'] = 'Failed to log in.';
            }

        }

    // MISSING USERNAME / PASSWORD
    } else {
        $out['message'] = 'Email and password fields are required.';
    }

    echo json_encode($out);

}

?>