<?php

// CUSTOM FILE
$custom_file = dirname(__file__). '/paged-results_custom.php';

// CUSTOM FILE EXISTS
if (file_exists($custom_file)) {
    include($custom_file);

// NO CUSTOM FILE
} else {

    // RATINGS / REVIEWS
    include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
    if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

    // LIMIT
    if (!$_GET['limit']) $_GET['limit'] = 10;

    // CALCULATE OFFSET
    if ($_GET['page']) {
        $offset = ((int)$_GET['page'] - 1) * $_GET['limit'];
    } else {
        $offset = 0;
    }

    // ONLY ACTIVE
    $_GET['status'] = 1;

    // GET REVIEWS
    $rr = $_uccms_ratings_reviews->get(array(
        'vars'              => $_GET,
        'order_field'       => $_GET['order_field'],
        'order_direction'   => $_GET['order_direction'],
        'limit'             => $_GET['limit'],
        'offset'            => $offset
    ));

    // HAVE RESULTS
    if ($rr['num_results'] > 0) {

        // PAGING CLASS
        require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

        // INIT PAGING CLASS
        $_paging = new paging();

        // OPTIONAL - URL VARIABLES TO IGNORE
        $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

        // PREPARE PAGING
        $_paging->prepare($_GET['limit'], 10, $_GET['page'], $_GET);

        // GENERATE PAGING
        $pages = $_paging->output($rr['num_results'], $rr['num_total']);

    }

    // HAVE REVIEWS
    if (count((array)$rr['reviews']) > 0) {

        $criteria = array();

        // IS EXTENSION
        if ($_uccms_ratings_reviews->extensions[$_GET['source']]) {

            // NEW EXTENSION OBJECT
            $tobject = new $_uccms_ratings_reviews->extensions[$_GET['source']]['class'];

            // HAVE CRITERIA
            if (method_exists($tobject, 'review_criteria')) {
                $criteria = $tobject->review_criteria($_GET['what']);
            }

            unset($tobject);

        }

        // CAN REPLY
        if ($_uccms_ratings_reviews->getReviewAccessSession($_GET, 'reply')) {

            // IS LOGGED IN
            if ($_uccms['_account']->loggedIn()) {

                // GET ACCOUNT INFO
                $reply_account = $_uccms['_account']->getAccount('', true);

                // NAME FOR REPLY
                $reply_name = trim(stripslashes($reply_account['firstname']. ' ' .$reply_account['lastname']{0}));

            }

        }

        ?>

        <div class="paging-top">
            <?php echo $pages; ?>
        </div>

        <div class="items">

            <?php

            // LOOP
            foreach ($rr['reviews'] as $rr_id => $review) {

                unset($replies);

                ?>

                <div class="item review-<?php echo $rr_id; ?>">

                    <div class="head clearfix">
                        <div class="name">
                            <?php echo stripslashes($review['name']); ?>
                        </div>
                        <div class="date">
                            <?php if ($review['dt_created'] != '0000-00-00 00:00:00') { echo date('n/j/Y', strtotime($review['dt_created'])); } ?>
                        </div>
                    </div>

                    <div class="content">
                        <span class="quote start">&ldquo;</span><?php echo stripslashes($review['content']); ?><span class="quote end">&rdquo;</span>
                    </div>

                    <?php

                    // HAVE REVIEW CRITERIA AND HAVE MASTER CRITERIA
                    if ((count((array)$review['data']['criteria']) > 0) && (count((array)$criteria) > 0)) {
                        ?>
                        <div class="criteria">
                            <?php
                            foreach ($review['data']['criteria'] as $teria_id => $teria_rating) {
                                if ($criteria[$teria_id]['title']) {
                                    ?>
                                    <div class="item">
                                        <?php
                                        $star_vars = array(
                                            'el_id'     => 'rr-' .$rr_id. '-criteria-' .$teria_id,
                                            'value'     => $teria_rating,
                                            'readonly'  => true
                                        );
                                        include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                                        ?><label><?php echo $criteria[$teria_id]['title']; ?></label>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php
                    }

                    ?>

                    <div class="stars">
                        <?php
                        $star_vars = array(
                            'el_id'     => 'stars-' .$rr_id,
                            'value'     => $review['rating'],
                            'readonly'  => true
                        );
                        include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                        if ((count((array)$review['data']['criteria']) > 0) && (count((array)$criteria) > 0)) {
                            ?><label>Overall</label><?php
                        }
                        ?>
                    </div>

                    <?php

                    // HAVE A REPLY
                    $replies = $_uccms_ratings_reviews->get(array(
                        'vars' => array(
                            'reply_to_id'   => $rr_id
                        )
                    ));

                    // HAVE REPLIES
                    if (count((array)$replies['reviews']) > 0) {
                        foreach ($replies['reviews'] as $reply) {
                            ?>
                            <div class="reply">
                                <div class="head clearfix">
                                    <div class="from">
                                        Reply from <span class="name"><?php echo stripslashes($reply['name']); ?></span>
                                    </div>
                                    <div class="date">
                                        <?php if ($reply['dt_created'] != '0000-00-00 00:00:00') { echo date('n/j/Y', strtotime($reply['dt_created'])); } ?>
                                    </div>
                                </div>
                                <div class="content">
                                    <?php echo stripslashes($reply['content']); ?>
                                </div>
                            </div>
                            <?php
                        }
                    }

                    ?>

                    <?php if ($_uccms_ratings_reviews->getReviewAccessSession($_GET, 'reply')) { ?>
                        <div class="add-reply">
                            <a href="#" class="add">Reply</a>
                            <div class="toggle">
                                <form action="" method="post">
                                <input type="hidden" name="source" value="<?php echo $_GET['source']; ?>" />
                                <input type="hidden" name="what" value="<?php echo $_GET['what']; ?>" />
                                <input type="hidden" name="item_id" value="<?php echo $_GET['item_id']; ?>" />
                                <input type="hidden" name="reply_to_id" value="<?php echo $review['id']; ?>" />
                                <div class="item review_content">
                                    <textarea name="content" placeholder="Write your reply"></textarea>
                                </div>
                                <div class="item name">
                                    <input type="text" name="name" value="<?php echo $reply_name; ?>" placeholder="Name" />
                                </div>
                                <div class="buttons">
                                    <input type="submit" value="Submit" class="button" />
                                </div>
                                </form>
                                <div class="message"></div>
                            </div>
                        </div>
                    <?php } ?>

                </div>

                <?php

            }

            ?>

        </div>

        <div class="paging-bottom">
            <?php echo $pages; ?>
        </div>

        <?php

    } else {
        ?>
        <div class="none">No reviews yet.</div>
        <?php
    }

}

?>