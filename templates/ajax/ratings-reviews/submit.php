<?php

// CUSTOM FILE
$custom_file = dirname(__file__). '/submit_custom.php';

// CUSTOM FILE EXISTS
if (file_exists($custom_file)) {
    include($custom_file);

// NO CUSTOM FILE
} else {

    // RATINGS / REVIEWS
    include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
    if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

    $out = array();

    $can_submit = false;

    // ACCOUNTS ARE ENABLED
    if ($_uccms['_account']->isEnabled()) {

        // LOGGED IN
        if ($_uccms['_account']->loggedIn()) {

            $can_submit = true;

            // GET ACCOUNT INFO
            $review_account = $_uccms['_account']->getAccount('', true);

            // NAME FOR REVIEW
            $_POST['name'] = trim(stripslashes($review_account['firstname']. ' ' .$review_account['lastname']{0}));

        // NOT LOGGED IN
        } else {
            $out['login_required'] = true;
            $out['error'] = 'You must be logged in to submit your review.';
        }

    // ACCOUNTS NOT ENABLED
    } else {
        $can_submit = true;
    }

    // CAN SUBMIT
    if ($can_submit) {

        // MISSING
        if ((!$_POST['source']) || (!$_POST['what']) || (!$_POST['item_id'])) {
            $out['error'] = 'Missing required fields.';
        }

        // CLEAN UP
        $name       = trim($_POST['name']);
        $content    = trim(strip_tags($_POST['content']));
        $rating     = (int)$_POST['rating'];

        $data = '';

        // NAME - MISSING
        if (!$name) {
            $out['error'] = 'Please enter your name.';
        }

        // CONTENT - MISSING
        if (!$content) {
            $out['error'] = 'Please enter your review.';
        }

        // IS EXTENSION
        if ($_uccms_ratings_reviews->extensions[$_POST['source']]) {

            // NEW EXTENSION OBJECT
            $tobject = new $_uccms_ratings_reviews->extensions[$_POST['source']]['class'];

            // HAVE CRITERIA
            if (method_exists($tobject, 'review_criteria')) {
                $criteria = $tobject->review_criteria($_POST['what']);
                if (count((array)$criteria) > 0) {
                    foreach ($criteria as $teria_id => $teria) {
                        if ($teria['required']) {
                            if (!$_POST['criteria'][$teria_id]) {
                                $out['error'] = 'Please select your rating for ' .$teria['title']. '.';
                            }
                        }
                        $data['criteria'][$teria_id] = $_POST['criteria'][$teria_id];
                    }
                }
            }

            unset($tobject);

        }

        // RATING - MISSING
        if (!$rating) {
            if (count($criteria) > 0) {
                $out['error'] = 'Please select your overall rating.';
            } else {
                $out['error'] = 'Please select your rating.';
            }
        }

        // CAPTCHA
        if ($cms->getSetting('recaptcha_secret')) {
            $recaptcha_response = json_decode(BigTree::cURL('https://www.google.com/recaptcha/api/siteverify?secret=' .$cms->getSetting('recaptcha_secret'). '&response=' .urlencode($_POST['g-recaptcha-response']). '&remoteip=' .$_SERVER['REMOTE_ADDR']));
            if (!$recaptcha_response->success) {
                $out['error'] = 'Please verify you are human.';
            }
        }

        // NO ERROR
        if (!$out['error']) {

            // ADD
            $id = $_uccms_ratings_reviews->create(array(
                'status'        => 1,
                'source'        => $_POST['source'],
                'what'          => $_POST['what'],
                'item_id'       => (int)$_POST['item_id'],
                'title'         => $_POST['title'],
                'content'       => $content,
                'data'          => $data,
                'rating'        => $rating,
                'account_id'    => $_uccms['_account']->userID(),
                'name'          => $name,
                'ip'            => $_SERVER['REMOTE_ADDR']
            ));

            // HAVE NEW ID
            if ($id) {
                $out['success'] = 'true';
            } else {
                $out['error'] = 'Failed to record review.';
            }

        }

    // CAN'T SUBMIT
    } else {
        if (!$out['error']) $out['error'] = 'You don\'t have access to submit a review.';
    }

    echo json_encode($out);

}

?>