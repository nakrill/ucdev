<?php

// RATINGS / REVIEWS
include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

$out = array();

// CAN REPLY
if ($_SESSION['ratings_reviews']['can_reply']) {

    // CLEAN UP
    $reply_to_id = (int)$_POST['reply_to_id'];

    // GET REVIEW
    $review_query = "SELECT * FROM `" .$_uccms_ratings_reviews->tables['ratings_reviews']. "` WHERE (`id`=" .$reply_to_id. ")";
    $review_q = sqlquery($review_query);
    $review = sqlfetch($review_q);

    // REVIEW FOUND
    if ($review['id']) {

        // MISSING
        if (!$reply_to_id) {
            $out['error'] = 'Missing required fields.';
        }

        // CLEAN UP
        $name       = trim($_POST['name']);
        $content    = trim(strip_tags($_POST['content']));

        /*
        // NAME - MISSING
        if (!$name) {
            $out['error'] = 'Please enter your name.';
        }
        */

        // CONTENT - MISSING
        if (!$content) {
            $out['error'] = 'Please enter your reply.';
        }

        // NO ERROR
        if (!$out['error']) {

            // ADD
            $id = $_uccms_ratings_reviews->create(array(
                'reply_to_id'   => $review['id'],
                'status'        => 1,
                'source'        => stripslashes($review['source']),
                'what'          => stripslashes($review['what']),
                'item_id'       => $review['item_id'],
                'title'         => $_POST['title'],
                'content'       => $content,
                'data'          => '',
                'rating'        => 0,
                'account_id'    => $_uccms['_account']->userID(),
                'name'          => $name,
                'ip'            => $_SERVER['REMOTE_ADDR']
            ));

            // HAVE NEW ID
            if ($id) {
                $out['success'] = 'true';
            } else {
                $out['error'] = 'Failed to record reply.';
            }

        }

    // REVIEW NOT FOUND
    } else {
        $out['error'] = 'Review not found.';
    }

// CANNOT REPLY
} else {
    $out['error'] = 'You cannot reply to this review.';
}

echo json_encode($out);

?>