<?php

// RATINGS / REVIEWS
include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

// IS PAGED
if ($_GET['source']) {
    $reviews_vars = $_GET;
}

// LIMIT
if (!$reviews_vars['limit']) $reviews_vars['limit'] = 15;

// CALCULATE OFFSET
if ($reviews_vars['page']) {
    $offset = ((int)$reviews_vars['page'] - 1) * $reviews_vars['limit'];
} else {
    $offset = 0;
}

// GET REVIEWS
$rr = $_uccms_ratings_reviews->get(array(
    'vars'              => $reviews_vars,
    'order_field'       => $reviews_vars['order_field'],
    'order_direction'   => $reviews_vars['order_direction'],
    'limit'             => $reviews_vars['limit'],
    'offset'            => $offset
));

// HAVE RESULTS
if ($rr['num_results'] > 0) {

    // PAGING CLASS
    require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

    // INIT PAGING CLASS
    $_paging = new paging();

    // OPTIONAL - URL VARIABLES TO IGNORE
    $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

    // PREPARE PAGING
    $_paging->prepare($reviews_vars['limit'], 10, $reviews_vars['page'], $reviews_vars);

    // GENERATE PAGING
    $pages = $_paging->output($rr['num_results'], $rr['num_total']);

}

// HAVE REVIEWS
if (count($rr['reviews']) > 0) {

    ?>

    <link rel="stylesheet" type="text/css" href="/css/lib/ratings-reviews/review-admin.css" />
    <script src="/js/lib/ratings-reviews/review-admin.js"></script>

    <script type="text/javascript">

        // RATINGS & REVIEWS
        $(document).ready(function() {

            // PAGING CLICK
            $('.uccms_ratings-reviews .results').on('click', '.paging a', function(e) {
                e.preventDefault();
                var parent = $(this).closest('.uccms_ratings-reviews');
                $.get('/ajax/ratings-reviews/paged-results-admin/' +$(this).attr('href'), function(data) {
                    $(parent).find('.results').html(data);
                });
            });

        });

    </script>

    <div class="pages top">
        <?php echo $pages; ?>
    </div>

    <div class="reviews items clearfix">
        <?php

        // LOOP
        foreach ($rr['reviews'] as $review) {

            // DISPLAY
            include(SERVER_ROOT. 'templates/elements/ratings-reviews/review-admin.php');

        }

        ?>
    </div>

    <div class="pages bottom">
        <?php echo $pages; ?>
    </div>

    <?php

} else {
    ?>
    <div class="no_reviews">
        <?php if ($rr['num_total'] > 0) { ?>
            No reviews on this page.
        <?php } else { ?>
            No reviews.
        <?php } ?>
    </div>
    <?php
}

?>