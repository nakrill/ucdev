<?php

$current_url = BigTree::currentURL();

// META
if (!$bigtree["page"]["meta_keywords"]) $bigtree["page"]["meta_keywords"] = $cms->getSetting('meta_keywords');
if (!$bigtree["page"]["meta_description"]) $bigtree["page"]["meta_description"] = $cms->getSetting('meta_description');

?>

<!DOCTYPE html>
<html lang="en">

    <!--
    Site By: Umbrella Consultants
    http://www.umbrellaconsultants.com
    -->

    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes" />

        <title><?php if ($bigtree["page"]["title"]) { echo $bigtree["page"]["title"]. ' | '; } echo $cms->getSetting('site_name'); ?></title>

        <meta name="keywords" content="<?=$bigtree["page"]["meta_keywords"]?>" />
        <meta name="description" content="<?=$bigtree["page"]["meta_description"]?>" />

        <?php if ($cms->getSetting('favicon')) { ?>
            <link rel="icon" href="<?=$cms->getSetting('favicon')?>" type="image/x-icon" />
            <link rel="shortcut icon" href="<?=$cms->getSetting('favicon')?>" type="image/x-icon" />
        <?php } ?>

        <?php if ($bigtree['page']['canonical']) { ?>
            <link rel="canonical" href="<?php echo $bigtree['page']['canonical']; ?>" />
        <?php } ?>

        <?php
        if (count((array)$bigtree['page']['meta']) > 0) {
            foreach ($bigtree['page']['meta'] as $meta) {
                echo $meta. "\n";
            }
        }
        ?>

        <!-- G+ AND FACEBOOK META TAGS -->
        <meta property="og:title" content="<?=$bigtree["page"]["title"]?>" />
        <meta property="og:url" content="<?=$current_url?>" />
        <meta property="og:type" content="website">
        <meta property="og:image" content="<?php echo ($bigtree['page']['share_image'] ? $bigtree['page']['share_image'] : $cms->getSetting('facebook_image')); ?>" />
        <meta property="og:description" content="<?=$page["meta_description"]?>" />
        <meta property="og:site_name" content="<?=$cms->getSetting('site_name')?>" />

        <!-- TWITTER CARD -->
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@" />
        <meta name="twitter:creator" content="@" />
        <meta name="twitter:url" content="<?=$current_url?>" />
        <meta name="twitter:title" content="<?=$bigtree["page"]["title"]?>" />
        <meta name="twitter:description" content="<?=$page["meta_description"]?>" />
        <meta name="twitter:image" content="<?php echo ($bigtree['page']['share_image'] ? $bigtree['page']['share_image'] : $cms->getSetting('twitter_image')); ?>" />

        <meta name="google-site-verification" content="EK1EFR2UuHtDDZGF_zDjLFFbAGLKdyEHw0U0i9jApck" />

        <?php

        // EXTERNAL CSS
        $excss = trim(implode("\n", array(
            strip_tags($cms->getSetting('fonts_external')), // FONTS
            strip_tags($cms->getSetting('css_external')), // CSS
        )));

        // HAVE EXTERNAL CSS
        if ($excss) {
            foreach (explode("\n", $excss) as $excss_url) {
                $excss_url = trim($excss_url);
                if ($excss_url) {
                    ?>
                    <link rel="stylesheet" href="<?php echo $excss_url; ?>" type="text/css" />
                    <?php
                }
            }
        }

        ?>
        <link rel="stylesheet" href="/css/site.css" type="text/css" media="all" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="/js/site.js" async></script>
        <?php

        // EXTERNAL JS ARRAY
        $exjsa = explode("\n", trim(strip_tags($cms->getSetting('js_external'))));

        // HAVE EXTERNAL JS
        if (count((array)$exjsa) > 0) {
            foreach ($exjsa as $exjs_url) {
                $exjs_url = trim($exjs_url);
                if ($exjs_url) {
                    ?>
                    <script src="<?php echo trim($exjs_url); ?>"></script>
                    <?php
                    if (strpos($exjs_url, 'jquery.fitvids') !== false) {
                        ?>
                        <script type="text/javascript">$(document).ready(function(){ $('body').fitVids(); });</script>
                        <?php
                    }
                }
            }
        }

        ?>

        <?php
        $ga_tracking = $cms->getSetting('bigtree-internal-google-analytics-api');
        if ($ga_tracking['profileID']) {
            ?>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', '<?php echo $ga_tracking['profileID']; ?>', 'auto');
              ga('send', 'pageview');

            </script>
            <?php
        }
        ?>

    </head>

    <body class="<?php if ($bigtree['path'][0]) { echo $bigtree['path'][0]; } else { echo 'home'; } ?>">

        <?php echo revsocial_webTrackingCode(); ?>

        <nav id="topnav" class="navbar navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <?php if ($cms->getSetting('logo')) { ?>
                            <img src="<?=$cms->getSetting('logo')?>" alt="<?=$cms->getSetting('site_name')?>" id="logo" />
                        <?php } else if ($cms->getSetting('site_name')) { ?>
                            <?=$cms->getSetting('site_name')?>
                        <?php } ?>
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="<?php if (!$bigtree['path'][0]) { echo 'active'; } ?>"><a href="/">Home</a></li>
                        <?php

                        // TOP NAVIGATION
                        $top_nav = $cms->getNavByParent(0, 2);

                        foreach ($top_nav as $navItem) {

                            ?>
                            <li class="<? if ($navItem['route'] == $bigtree['path'][0]) { echo 'active'; } ?>">
                                <a href="<?=$navItem["link"]?>" <?=targetBlank($navItem["link"])?>><?=$navItem["title"]?></a>
                            </li>
                            <?php

                        }

                        ?>
                        <li style="padding-left: 15px;"><a href="/contact/" class="btn btn-primary">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <section id="content">

