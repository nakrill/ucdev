<?

include "_header.php";

$currentPage = BigTree::currentURL();

// Get all the categories, authors, and months with posts in them.
$categories = $_uccms_blog->getCategories();
$authors = $_uccms_blog->getAuthors();
$archives = $_uccms_blog->getMonths();

?>

<link rel="stylesheet" type="text/css" href="<?=WWW_ROOT?>extensions/<?=$_uccms_blog->Extension;?>/css/master/master.css" />
<link rel="stylesheet" type="text/css" href="<?=WWW_ROOT?>extensions/<?=$_uccms_blog->Extension;?>/css/custom/master.css" />
<script src="<?=WWW_ROOT?>extensions/<?=$_uccms_blog->Extension;?>/js/master/master.js"></script>
<script src="<?=WWW_ROOT?>extensions/<?=$_uccms_blog->Extension;?>/js/custom/master.js"></script>

<div id="blogContainer">
    <div class="site-width block_center contain">

        <? //include "_breadcrumb.php" ?>

        <aside class="sidebar">
            <nav class="subnav blognav">
                <div class="nav_options">

                    <?

                    // CATEGORIES
                    if (count($categories)) {
                        ?>
                        <h4>Categories</h4>
                        <ul>
                            <?
                            $i = 0;
                            foreach ($categories as $category) {
                                $i++;
                                $link = $blog_link."category/".$category["route"]."/";
                                ?>
                                <li<? if ($i == count($categories)) { ?> class="last"<? } ?>><a href="<?=$link?>"<? if (strpos($currentPage,$link) !== false) { ?> class="active"<? } ?>><?=$category["title"]?></a></li>
                                <?
                                }
                            ?>
                        </ul>

                        <?

                    }

                    // AUTHORS
                    if (count($authors)) {
                        ?>
                        <h4>Authors</h4>
                        <ul>
                            <?
                            $i = 0;
                            foreach ($authors as $author) {
                                $i++;
                                $link = $blog_link."author/".$author["route"]."/";
                                ?>
                                <li<? if ($i == count($authors)) { ?> class="last"<? } ?>><a href="<?=$link?>"<? if (strpos($currentPage,$link) !== false) { ?> class="active"<? } ?>><?=$author["name"]?></a></li>
                                <?
                            }
                            ?>
                        </ul>
                        <?
                        }

                        // ARCHIVES
                        if (count($archives)) {
                            ?>
                            <h4>Archives</h4>
                            <ul>
                                <?
                                $i = 0;
                                foreach ($archives as $archive) {
                                    $i++;
                                    $month = date("F Y", strtotime($archive));
                                    $link = $blog_link."month/".date("Y-m",strtotime($archive))."/";
                                    ?>
                                    <li<? if ($i == count($archives)) { ?> class="last"<? } ?>><a href="<?=$link?>"<? if (strpos($currentPage,$link) !== false) { ?> class="active"<? } ?>><?=$month?></a></li>
                                    <?
                                }
                                ?>
                            </ul>
                            <?
                        }

                    ?>

                    <h4>Search</h4>
                    <form class="search" method="post" action="<?=$blog_link?>search/">
                        <input type="text" name="query" class="query" value="" placeholder="Keyword" />
                        <input type="submit" value="Go" class="submit" />
                    </form>

                </div>
            </nav>
        </aside>

        <div class="main content contain">
            <?=$bigtree["content"]?>
        </div>

    </div>
</div>

<? include "_footer.php" ?>