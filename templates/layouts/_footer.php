
            </div>
        </section>

        <footer id="footer" class="noprint">
            <div class="container">
                <div class="background">


                    <?php

                    // GET FOOTER COLUMNS
                    $footer_columns = $cms->getSetting('footer_columns');

                    // HAVE FOOTER COLUMNS
                    if (count($footer_columns) > 0) {

                        ?>

                        <div class="columns row">

                            <?php

                            $fi = 1;

                            // LOOP
                            foreach ($footer_columns as $footer_column) {
                                ?>
                                <div class="column col-md-4">
                                    <h4><?php if ($footer_column['icon']) { ?><i class="icon-<?php echo $footer_column['icon']; ?>"></i> <?php } ?><?php echo $footer_column['title']; ?></h4>
                                    <div class="content">
                                        <?php echo $footer_column['content']; ?>
                                        <?php
                                        if ($footer_column['map_code']) {
                                            echo htmlspecialchars_decode($footer_column['map_code']);
                                        }
                                        if ($footer_column['form_code']) {
                                            echo htmlspecialchars_decode($footer_column['form_code']);
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                $fi++;
                            }
                            ?>

                        </div>

                        <?php

                    }

                    ?>

                </div>
            </div>
        </footer>

        <section class="copyright">
            <div class="container">
                <div class="background clearfix">
                    <div class="copyright col-md-6">&copy; <?=date("Y")?> <?=$cms->getSetting('site_name')?></div>
                    <div class="site_by col-md-6"><a href="http://www.umbrellaconsultants.com/cms/" target="_blank">Website Design & Development</a> by <a href="http://www.umbrellaconsultants.com/" target="_blank">Umbrella Consultants</a></div>
                </div>
            </div>
        </section>

    </body>
</html>