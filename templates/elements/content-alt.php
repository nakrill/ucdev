<?php

if (count($content_alt['rows']) > 0) {

    ?>

    <section class="alt-content">

        <?php foreach ($content_alt['rows'] as $ri => $alt) { ?>

            <div class="alt-row row row-<?php echo $ri; ?> image-<?php echo $alt['image_position']; ?>">

                <?php

                // TWO COLUMNS
                for ($i=1; $i<=2; $i++) {

                    if ((($i == 1) && ($alt['image_position'] == 'left')) || (($i == 2) && ($alt['image_position'] == 'right'))) {
                        ?>
                        <div class="image col-md-6">
                            <img src="<?php echo BigTree::prefixFile($alt['image'], 'm_'); ?>" alt="" />
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="content col-md-6">
                            <?php if ($alt['heading']) { ?>
                                <h3><?php echo $alt['heading']; ?></h3>
                            <?php } ?>
                            <?php if ($alt['content']) { ?>
                                <?php echo $alt['content']; ?>
                            <?php } ?>
                            <?php if ($alt['button_url']) { ?>
                                <a href="<?php echo $alt['button_url']; ?>" class="btn btn-primary"><?php echo $alt['button_text']; ?></a>
                            <?php } ?>
                        </div>
                        <?php
                    }

                }

                ?>
            </div>

        <?php } ?>

    </section>

    <?php

}

?>