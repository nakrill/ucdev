<?php if (($captcha_el) && ($cms->getSetting('recaptcha_key'))) { ?>
    <script type="text/javascript">

        // ADD THIS CALLBACK TO ARRAY
        if (!$.isArray(recaptchaCallbacks)) var recaptchaCallbacks = [];
        recaptchaCallbacks.push(function() {
            grecaptcha.render($('<?php echo $captcha_el; ?>')[0], {
                'sitekey': '<?php echo $cms->getSetting('recaptcha_key'); ?>',
                'size': '<?php echo ($captcha_size ? $captcha_size : 'normal'); ?>'
            });
        });

        // CALLBACK FOR reCAPTCHA
        var recaptchaCallback = function() {
            if ($.isArray(recaptchaCallbacks)) {
                while (recaptchaCallbacks.length){
                    recaptchaCallbacks.shift().call();
                }
            }
        };

        // GET reCAPTCHA SCRIPT
        $.getScript('https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit');

    </script>
    <div class="captcha">
        <div class="recaptcha"></div>
    </div>
<?php } ?>