<?php

// HAVE CAROUSEL ELEMENTS
if (count((array)$carousel['slides']) > 0) {

    if (!$carousel['id']) $carousel['id'] = rand(11111,99999);

    ?>

    <script type="text/javascript">
        $(function() {

            // PRELOAD IMAGES
            $('#carousel-<?php echo $carousel['id']; ?> .item .bg').each(function(i, v) {
                var url = $(v).data('preload');
                if (url && url != 'undefined') {
                    pic = new Image();
                    pic.src = url;
                }
            });

        });
    </script>

    <div id="carousel-<?php echo $carousel['id']; ?>" class="carousel fade-carousel height-60 slide <?php echo $carousel['class']; ?>" <?php if ($carousel['interval'] > 0) { ?>data-ride="carousel" data-interval="<?php echo $carousel['interval']; ?>"<?php } ?>>
        <div class="carousel-inner">

            <?php foreach ($carousel['slides'] as $i => $slide) { ?>

                <div class="item <?php if ($i == 0) { echo 'active'; } ?>">
                    <div class="bg" data-preload="<?php echo $slide['image']; ?>" style="background-image: url('<?php echo $slide['image']; ?>');"></div>
                    <div class="hero">
                        <hgroup>
                            <?php if ($slide['heading1']) { ?><h1><?php echo $slide['heading1']; ?></h1><?php } ?>
                            <?php if ($slide['heading2']) { ?><h3><?php echo $slide['heading2']; ?></h3><?php } ?>
                        </hgroup>
                        <?php if ($slide['content']) { ?>
                            <div class="content"><?php echo $slide['content']; ?></div>
                        <?php } ?>
                        <?php if ($slide['button_url']) { ?>
                            <a href="<?php echo $slide['button_url']; ?>" class="btn btn-primary btn-lg"><?php echo $slide['button_text']; ?></a>
                        <?php } ?>
                    </div>
                </div>

            <?php } ?>

        </div>

        <?php if (count($carousel['slides']) > 1) { ?>

            <ol class="carousel-indicators">
                <?php foreach ($carousel['slides'] as $i => $slide) { ?>
                    <li data-target="#carousel-<?php echo $carousel['id']; ?>" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) { echo 'active'; } ?>"></li>
                <?php } ?>
            </ol>

            <a class="left carousel-control" href="#carousel-<?php echo $carousel['id']; ?>" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Previous</span></a>
            <a class="right carousel-control" href="#carousel-<?php echo $carousel['id']; ?>" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span></a>

        <?php } ?>

    </div>

    <?php

}

?>