<?php
if (!$star_vars['el_id']) $star_vars['el_id'] = 'stars-' .rand(1111,9999);
if (!$star_vars['el_name']) $star_vars['el_name'] = 'rating';
if (!$star_vars['theme']) $star_vars['theme'] = 'fontawesome-stars-o';
?>
<link rel="stylesheet" href="/site/css/lib/ratings-reviews/<?php echo ($star_vars['theme'] ? $star_vars['theme'] : 'fontawesome-stars'); ?>.css" />
<select id="<?php echo $star_vars['el_id']; ?>" name="<?php echo $star_vars['el_name']; ?>" class="rating_stars">
    <?php if ($star_vars['allow_empty']) { ?>
        <option value=""></option>
    <?php } ?>
    <option value="1" <?php if ($star_vars['value'] == 1) { ?>selected="selected"<?php } ?>>1</option>
    <option value="2" <?php if ($star_vars['value'] == 2) { ?>selected="selected"<?php } ?>>2</option>
    <option value="3" <?php if ($star_vars['value'] == 3) { ?>selected="selected"<?php } ?>>3</option>
    <option value="4" <?php if ($star_vars['value'] == 4) { ?>selected="selected"<?php } ?>>4</option>
    <option value="5" <?php if ($star_vars['value'] == 5) { ?>selected="selected"<?php } ?>>5</option>
</select>
<script type="text/javascript">
    $(function() {
        $('#<?php echo $star_vars['el_id']; ?>').barrating({
            theme: '<?php echo $star_vars['theme']; ?>',
            initialRating: <?php echo ($star_vars['value'] ? $star_vars['value'] : 0); ?>,
            <?php if ($star_vars['allow_empty']) { ?>
                allowEmpty: true,
            <?php } ?>
            readonly: <?php echo ($star_vars['readonly'] ? 'true' : 'false'); ?>
        });
    });
</script>