<?php

unset($replies);

if (!is_array($review['data'])) {
    if ($review['data']) {
        $review['data'] = json_decode($review['data'], true);
    } else {
        $review['data'] = array();
    }
}

// NO NAME, BUT HAVE ACCOUNT ID
if ((!$review['name']) && ($review['account_id'])) {
    $name = sqlfetch(sqlquery("SELECT `firstname`, `lastname` FROM `uccms_accounts_details` WHERE (`id`=" .$review['account_id']. ")"));
    $review['name'] = trim(stripslashes($name['firstname']. ' ' .strtoupper($name['lastname']{0})));
}

// DON'T HAVE CRITERIA
if (!is_array($review_admin['criteria'])) {

    $review_admin['criteria'] = array();

    // IS EXTENSION
    if ($_uccms_ratings_reviews->extensions[$review['source']]) {

        // NEW EXTENSION OBJECT
        $tobject = new $_uccms_ratings_reviews->extensions[$review['source']]['class'];

        // HAVE CRITERIA
        if (method_exists($tobject, 'review_criteria')) {
            $review_admin['criteria'] = $tobject->review_criteria($review['what']);
        }

        unset($tobject);

    }

}

?>

<div class="item review-<?php echo $review['id']; ?>">

    <div class="head clearfix">
        <div class="name">
            <?php echo stripslashes($review['name']); ?>
        </div>
        <div class="date">
            <?php if ($review['dt_created'] != '0000-00-00 00:00:00') { echo date('n/j/Y g:i A', strtotime($review['dt_created'])); } ?>
        </div>
    </div>

    <div class="content">
        <?php echo stripslashes($review['content']); ?>
    </div>

    <?php

    // HAVE REVIEW CRITERIA AND HAVE MASTER CRITERIA
    if ((count($review['data']['criteria']) > 0) && (count($review_admin['criteria']) > 0)) {
        ?>
        <div class="criteria">
            <?php
            foreach ($review['data']['criteria'] as $teria_id => $teria_rating) {
                if ($review_admin['criteria'][$teria_id]['title']) {
                    ?>
                    <div class="item clearfix">
                        <?php
                        $star_vars = array(
                            'el_id'     => 'rr-' .$review['id']. '-criteria-' .$teria_id,
                            'value'     => $teria_rating,
                            'readonly'  => true
                        );
                        include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                        ?><label><?php echo $review_admin['criteria'][$teria_id]['title']; ?></label>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    }

    ?>

    <div class="stars clearfix">
        <?php
        $star_vars = array(
            'el_id'     => 'stars-' .$review['id'],
            'value'     => $review['rating'],
            'readonly'  => true
        );
        include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
        if ((count($review['data']['criteria']) > 0) && (count($review_admin['criteria']) > 0)) {
            ?><label>Overall</label><?php
        }
        ?>
    </div>

    <?php

    // NOT HIDING REPLIES
    if (!$review_admin['hide_replies']) {

        // HAVE A REPLY
        $replies = $_uccms_ratings_reviews->get(array(
            'vars' => array(
                'reply_to_id'   => $review['id']
            )
        ));

        // HAVE REPLIES
        if (count($replies['reviews']) > 0) {

            // LOOP
            foreach ($replies['reviews'] as $reply) {
                ?>
                <div class="reply">
                    <div class="head clearfix">
                        <div class="from">
                            Reply from <span class="name"><?php echo stripslashes($reply['name']); ?></span>
                        </div>
                        <div class="date">
                            <?php if ($reply['dt_created'] != '0000-00-00 00:00:00') { echo date('n/j/Y g:i A', strtotime($reply['dt_created'])); } ?>
                        </div>
                    </div>
                    <div class="content">
                        <?php echo stripslashes($reply['content']); ?>
                    </div>
                </div>
                <?php
            }

        }

    }

    // HAS REPLY PERMISSION
    if ($_uccms_ratings_reviews->getReviewAccessSession($review, 'reply')) {

        // GET ACCOUNT INFO
        if (!$review_admin['reply_account']) {
            $review_admin['reply_account'] = $_uccms['_account']->getAccount('', true);
            $review_admin['reply_name'] = trim(stripslashes($review_admin['reply_account']['firstname']. ' ' .$review_admin['reply_account']['lastname']{0}));
        }

        ?>

        <div class="add-reply">
            <a href="#" class="add">Reply</a>
            <div class="toggle">
                <form action="/ajax/ratings-reviews/reply/" method="post">
                <input type="hidden" name="source" value="<?php echo $review['source']; ?>" />
                <input type="hidden" name="what" value="<?php echo $review['what']; ?>" />
                <input type="hidden" name="item_id" value="<?php echo $review['item_id']; ?>" />
                <input type="hidden" name="reply_to_id" value="<?php echo $review['id']; ?>" />
                <div class="item review_content">
                    <textarea name="content" placeholder="Write your reply"></textarea>
                </div>
                <div class="item name">
                    <input type="text" name="name" value="<?php echo $review_admin['reply_name']; ?>" placeholder="Name" />
                </div>
                <div class="buttons">
                    <input type="submit" value="Submit" class="button" />
                </div>
                </form>
                <div class="message"></div>
            </div>
        </div>
    <?php } ?>

</div>