<?php

// CUSTOM FILE
$custom_file = dirname(__file__). '/reviews_custom.php';

// CUSTOM FILE EXISTS
if (file_exists($custom_file)) {
    include($custom_file);

// NO CUSTOM FILE
} else {

    // ELEMENT ID
    $el_id = rand(1111,9999);

    if (!isset($reviews_vars['login_lightwindow'])) {
        $reviews_vars['login_lightwindow'] = true;
    }

    ?>

    <script type="text/javascript">

        // RATINGS & REVIEWS
        $(document).ready(function() {

            $('#ratings-reviews-<?php echo $el_id; ?> .add').click(function(e) {
                e.preventDefault();
                $('html, body').animate({
                    scrollTop: $('#ratings-reviews-<?php echo $el_id; ?> .add-review').offset().top
                }, 1500);
            });

            // GET CONTENT
            $.get('/ajax/ratings-reviews/paged-results/', <?php echo ($reviews_vars ? json_encode($reviews_vars) : ''); ?>, function(data) {
                $('#ratings-reviews-<?php echo $el_id; ?> .results').html(data);
            });

            // PAGING CLICK
            $('#ratings-reviews-<?php echo $el_id; ?>').on('click', '.paging a', function(e) {
                e.preventDefault();
                $.get('/ajax/ratings-reviews/paged-results/' +$(this).attr('href'), function(data) {
                    $('#ratings-reviews-<?php echo $el_id; ?> .results').html(data);
                });
            });

            // REPLY TOGGLE
            $('#ratings-reviews-<?php echo $el_id; ?> .results').on('click', '.item .add-reply a.add', function(e) {
                e.preventDefault();
                $(this).next('.toggle').toggle();
            });

            // SUBMIT - REPLY
            $('#ratings-reviews-<?php echo $el_id; ?> .results').on('submit', '.item .add-reply form', function(e) {
                var _this = $(this);
                var parent = $(this).closest('.toggle');
                $(parent).find('.message').hide().html('').removeClass('error success');
                if ($(this).find('[name="content"]').val() == '') {
                    $(parent).find('.message').html('Please enter your reply.').addClass('error').show();
                    return false;
                }
                $.post('/ajax/ratings-reviews/reply/', $(this).serialize(), function(data) {
                    if (data['success']) {
                        _this.fadeOut(400, function() {
                            $(parent).find('.message').html('Reply submitted!').addClass('success').show();
                        });
                    } else {
                        if (data['error']) {
                            $(parent).find('.message').html(data['error']).addClass('error').show();
                        } else {
                            $(parent).find('.message').html('Failed to record reply.').addClass('error').show();
                        }
                    }
                }, 'json');
                return false;
            });

            // SUBMIT
            $('#ratings-reviews-<?php echo $el_id; ?> .add-review .form form').submit(function(e) {
                var _this = $(this);
                var parent = $(this).closest('.form');
                $(parent).find('.message').hide().html('').removeClass('error success');
                if ($(this).find('[name="name"]').val() == '') {
                    $(parent).find('.message').html('Please enter your name.').addClass('error').show();
                    return false;
                }
                if ($(this).find('[name="content"]').val() == '') {
                    $(parent).find('.message').html('Please enter your review.').addClass('error').show();
                    return false;
                }
                $.post('/ajax/ratings-reviews/submit/', $(this).serialize(), function(data) {
                    if (data['success']) {
                        _this.fadeOut(400, function() {
                            $(parent).find('.message').html('Thank you for submitting your review!').addClass('success').show();
                        });
                    } else {
                        if (data['error']) {
                            $(parent).find('.message').html(data['error']).addClass('error').show();
                            <?php if ($reviews_vars['login_lightwindow']) { ?>
                                if (data['login_required']) {
                                    $.featherlight($('#rr-login-lightbox-<?php echo $el_id; ?>'));
                                }
                            <?php } ?>
                        } else {
                            $(parent).find('.message').html('Failed to record review.').addClass('error').show();
                        }
                    }
                }, 'json');
                return false;
            });

        });

    </script>

    <a name="reviews"></a>

    <div id="ratings-reviews-<?php echo $el_id; ?>" class="uccms_ratings-reviews">

        <a href="#rr-<?php echo $el_id; ?>-add" class="add top button"><i class="fa fa-plus" aria-hidden="true"></i>Leave a review</a>

        <div class="results">
            <div class="loading">Loading..</div>
        </div>

        <div class="add-review">

            <h4>Your Review</h4>

            <?php

            $can_submit = false;

            // ACCOUNTS ARE ENABLED
            if ($_uccms['_account']->isEnabled()) {

                // LOGGED IN
                if ($_uccms['_account']->loggedIn()) {
                    $can_submit = true;

                // NOT LOGGED IN
                } else {

                    $login_vars = array(
                        'register'  => true
                    );

                    $login_vars['register_callback'] = "
                    if (data.success) {
                        var current_flw = $.featherlight.current();
                        current_flw.close();
                        $('#ratings-reviews-" .$el_id. " .form-review form').submit();
                    } else {
                        $('#uccms_login-register-' +data.el_id+ ' .col-register .message').html(data.message).addClass('type-error').show();
                    }
                    ";

                    // DO LOGIN IN LIGHTWINDOW
                    if ($reviews_vars['login_lightwindow']) {

                        $can_submit = true;

                        $login_vars['ajax'] = true;
                        $login_vars['login_callback'] = "
                        if (data.success) {
                            var current_flw = $.featherlight.current();
                            current_flw.close();
                            $('#ratings-reviews-" .$el_id. " .form-review form').submit();
                        } else {
                            $('#uccms_login-register-' +data.el_id+ ' .col-login .message').html(data.message).addClass('type-error').show();
                        }
                        ";

                        ?>

                        <div id="rr-login-lightbox-<?php echo $el_id; ?>" class="lightbox_content">
                            <h2 class="head">You must be logged in to submit a review.</h2>
                            <?php include(SERVER_ROOT. 'templates/elements/login-register.php'); ?>
                        </div>

                        <?php

                    // DO ON PAGE
                    } else {
                        include(SERVER_ROOT. 'templates/elements/login-register.php');
                    }

                }

            // ACCOUNTS ARE NOT ENABLED
            } else {
                $can_submit = true;
            }

            // CAN SUBMIT
            if ($can_submit) {

                // IS LOGGED IN
                if ($_uccms['_account']->loggedIn()) {

                    // GET ACCOUNT INFO
                    $review_account = $_uccms['_account']->getAccount('', true);

                    // NAME FOR REVIEW
                    $review_name = trim(stripslashes($review_account['firstname']. ' ' .$review_account['lastname']{0}));

                }


                ?>

                <a name="review-form"></a>

                <div class="form form-review">

                    <form action="" method="post">
                    <input type="hidden" name="source" value="<?php echo $reviews_vars['source']; ?>" />
                    <input type="hidden" name="what" value="<?php echo $reviews_vars['what']; ?>" />
                    <input type="hidden" name="item_id" value="<?php echo $reviews_vars['item_id']; ?>" />

                    <?php

                    // IS EXTENSION
                    if ($_uccms_ratings_reviews->extensions[$reviews_vars['source']]) {

                        // NEW EXTENSION OBJECT
                        $tobject = new $_uccms_ratings_reviews->extensions[$reviews_vars['source']]['class'];

                        // HAVE CRITERIA
                        if (method_exists($tobject, 'review_criteria')) {
                            $criteria = $tobject->review_criteria($reviews_vars['what']);
                            if (count($criteria) > 0) {
                                ?>
                                <div class="criteria">
                                    <?php foreach ($criteria as $teria_id => $teria) { ?>
                                        <div class="item">
                                            <?php
                                            $star_vars = array(
                                                'el_id'     => $el_id. '-criteria-' .$teria_id,
                                                'el_name'   => 'criteria[' .$teria_id. ']',
                                                'value'     => 5
                                            );
                                            include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                                            ?><label><?php echo $teria['title']; ?></label>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php
                            }
                        }

                        unset($tobject);

                    }

                    ?>

                    <div class="item stars">
                        <?php
                        $star_vars = array(
                            'value' => 5
                        );
                        include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                        if (count($criteria) > 0) {
                            ?><label>Overall</label><?php
                        }
                        ?>
                    </div>

                    <div class="item review_content">
                        <textarea name="content" placeholder="Write your review"></textarea>
                    </div>

                    <div class="item name">
                        <?php if ($review_name) { ?>
                            <?php echo $review_name; ?>
                            <input type="hidden" name="name" value="<?php echo $review_name; ?>" />
                        <?php } else { ?>
                            <input type="text" name="name" value="" placeholder="Name" />
                        <?php } ?>
                    </div>

                    <?php
                    $captcha_el = '#ratings-reviews-' .$el_id. ' .add-review .form .captcha .recaptcha';
                    include(SERVER_ROOT. 'templates/elements/captcha.php');
                    ?>

                    <div class="buttons">
                        <input type="submit" value="Submit" class="button" />
                    </div>

                    </form>

                    <div class="message site-message"></div>

                </div>

                <?php
            }

            ?>

        </div>

    </div>

    <?php

}

?>