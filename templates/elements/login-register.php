<?php

// CUSTOM FILE
$custom_file = dirname(__file__). '/login-register_custom.php';

// CUSTOM FILE EXISTS
if (file_exists($custom_file)) {
    include($custom_file);

// NO CUSTOM FILE
} else {

    // NOT AJAX - REDIRECT
    if (!$login_vars['ajax']) {
        if ($login_vars['redirect']) {
            $_uccms['_account']->setRedirect($login_vars['redirect']);
        } else {
            $_uccms['_account']->setRedirect($_SERVER['REQUEST_URI']);
        }
    }

    // REGISTRATIONS NOT ENABLED
    if (!$_uccms['_account']->registrationsEnabled()) {
        $login_vars['register'] = false;
    }

    $el_id_login_register = rand(1111, 9999);

    ?>

    <div id="uccms_login-register-<?php echo $el_id_login_register; ?>" class="uccms_login-register clearfix">

        <div class="col-login <?php if (!$login_vars['register']) { ?>no-register<?php } ?>">
            <div class="padding">

                <h5>Log In</h5>

                <form action="/account/login/" method="post">

                <fieldset>
                    <input type="email" name="email" value="" placeholder="Email" class="fw" />
                </fieldset>

                <fieldset>
                    <input type="password" name="password" placeholder="Password" class="fw" />
                    <label class="small"><a href="/account/forgot-password/" target="_blank">Forgot password?</a></label>
                </fieldset>

                <fieldset>
                    <input type="checkbox" name="remember" value="1" checked="checked" /> Remember me
                </fieldset>

                <fieldset>
                    <input type="submit" value="Log In" class="button fw" />
                </fieldset>

                </form>

            </div>
            <div class="message site-message"></div>
        </div>

        <?php if ($login_vars['register']) { ?>

            <div class="col-register">
                <div class="padding">

                    <h5>Create Account</h5>

                    <form action="/account/register/" method="post">

                    <fieldset>
                        <input type="text" name="firstname" value="" placeholder="First Name" class="fw" />
                    </fieldset>

                    <fieldset>
                        <input type="text" name="lastname" value="" placeholder="Last Name" class="fw" />
                    </fieldset>

                    <fieldset>
                        <input type="email" name="email" value="" placeholder="Email" class="fw" />
                    </fieldset>

                    <fieldset>
                        <input type="password" name="password" placeholder="Password" class="fw" />
                    </fieldset>

                    <fieldset>
                        <input type="submit" value="Create Account" class="button fw" />
                    </fieldset>

                    </form>

                </div>
            </div>

        <?php } ?>

    </div>

    <?php

    // IS AJAX
    if ($login_vars['ajax']) {
        ?>
        <script type="text/javascript">

            $(document).ready(function() {

                // LOGIN SUBMIT
                $('#uccms_login-register-<?php echo $el_id_login_register; ?> .col-login form').submit(function(e) {
                    e.preventDefault();
                    $('#uccms_login-register-<?php echo $el_id_login_register; ?> .col-login .message').hide().html('').removeClass('type-error').removeClass('type-success');
                    $.post('/ajax/account/login/', $(this).serialize(), function(data) {
                        data.el_id = <?php echo $el_id_login_register; ?>;
                        <?php echo $login_vars['login_callback']; ?>
                    }, 'json');
                });

                // REGISTER SUBMIT
                $('#uccms_login-register-<?php echo $el_id_login_register; ?> .col-register form').submit(function(e) {
                    e.preventDefault();
                    $('#uccms_login-register-<?php echo $el_id_login_register; ?> .col-register .message').hide().html('').removeClass('type-error').removeClass('type-success');
                    $.post('/ajax/account/register/', $(this).serialize(), function(data) {
                        data.el_id = <?php echo $el_id_login_register; ?>;
                        <?php echo $login_vars['register_callback']; ?>
                    }, 'json');
                });

            });

        </script>
        <?php
    }

}

?>