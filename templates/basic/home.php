<style type="text/css">

    #p_home section.top-columns .columns .column {
        padding: 0px;
    }
    #p_home section.top-columns .columns .column .background {
        padding: 50px 25px;
        background-color: #2E3841;
        text-align: center;
    }
    #p_home section.top-columns .columns .column:nth-child(even) .background {
        background-color: #2A333B;
    }
    #p_home section.top-columns .columns .column .image {

    }
    #p_home section.top-columns .columns .column h3 {
        margin-top: 30px;
        color: #fff;
    }
    #p_home section.top-columns .columns .column .text {
        margin-top: 15px;
        padding: 0 35px;
        line-height: 1.8em;
        color: rgba(255, 255, 255, .5);
    }

    #p_home section.projects {
        padding-top: 45px;
        padding-bottom: 30px;
        background-color: #f5f5f5;
    }
    #p_home section.projects h2 {
        margin: 0 0 20px;
        text-align: center;
    }

    #p_home .bottom-cta {
        padding: 30px 0;
        background-color: rgba(0, 0, 0, .3);
        color: #fff;
    }
    #p_home .bottom-cta .left {
        text-align: right;
    }

</style>

<script type="text/javascript">

    $(function() {

        equalHeight('#p_home section.top-columns .columns .column .background');

    });

</script>

<div id="p_home">

    <?php

    // CAROUSEL CONFIG
    $carousel = array(
        'slides'    => $bigtree['resources']['hero'],
        'interval'  => 4000
    );

    // INCLUDE ELEMENT
    include(SERVER_ROOT. 'templates/elements/carousel.php');

    ?>

    <?php if (count($bigtree['resources']['columns']) > 0) { ?>
        <section class="top-columns">
            <div class="columns clearfix">
                <?php foreach ($bigtree['resources']['columns'] as $column) { ?>
                    <div class="column col-md-3">
                        <div class="background">
                            <?php if ($column['image']) { ?>
                                <img src="<?php echo $column['image']; ?>" alt="" />
                            <?php } ?>
                            <h3><?php echo $column['heading']; ?></h3>
                            <div class="text">
                                <?php echo $column['text']; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </section>
    <?php } ?>

    <section id="main-content">
        <div class="container">

            <div class="content">
                <?php echo $bigtree['resources']['content']; ?>
            </div>

            <?php

            // ALTERNATING CONTENT CONFIG
            $content_alt = array(
                'rows'    => $bigtree['resources']['content_alt']
            );

            // INCLUDE CAROUSEL ELEMENT
            include(SERVER_ROOT. 'templates/elements/content-alt.php');

            ?>

        </div>
    </section>

    <?php

    // GET SUB PAGES (PROJECTS)
    $projects_query = "SELECT * FROM `bigtree_pages` WHERE (`template`='project') ORDER BY `created_at` DESC LIMIT 6";
    $projects_q = sqlquery($projects_query);

    // HAVE PROJECTS
    if (sqlrows($projects_q) > 0) {

        ?>

        <section class="projects">
            <div class="container">
                <h2>Latest Projects</h2>
                <div class="projects row">
                    <?php

                    // LOOP THROUGH SUB PAGES (PROJECTS)
                    while ($project = sqlfetch($projects_q)) {

                        // PAGE RESOURCES
                        $resources = json_decode($project['resources'], true);

                        //echo print_r($resources);

                        ?>
                        <div class="project col-md-4 col-sm-6 col-xs-12">
                            <div class="hovereffect">
                                <div class="background" style="background-image: url('<?php echo BigTree::prefixFile(str_replace('{staticroot}', '/', $resources['image']), 's_'); ?>');"></div>
                                <div class="overlay">
                                    <h2><a href="/<?php echo $project['path']; ?>/"><?php echo $project['title']; ?></a></h2>
                                    <p class="icon-links">
                                        <a href="#">
                                            <span class="fa fa-facebook"></span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php

                    }

                    ?>
                </div>
            </div>
        </section>

        <?php

    }

    // BOTTOM CTA
    if (($bigtree['resources']['bottom_cta-text']) || ($bigtree['resources']['bottom_cta-button-url'])) {
        ?>
        <section class="bottom-cta">
            <div class="container">
                <div class="left col-md-6">
                    <h4><?php echo $bigtree['resources']['bottom_cta-text']; ?></h4>
                </div>
                <div class="right col-md-6">
                    <a href="<?php echo $bigtree['resources']['bottom_cta-button-url']; ?>" class="btn btn-primary"><?php echo $bigtree['resources']['bottom_cta-button-text']; ?></a>
                </div>
            </div>
        </section>
        <?php
    }

    ?>

</div>