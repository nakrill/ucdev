<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__header.php');
} else {
    ?>
    <div class="site-width block_center">
        <div class="page clearfix">
    <?php
}
?>

<article class="row">
    <div class="mobile-full tablet-4 tablet-push-1 desktop-6 desktop-push-3">
        <h1>Page Not Found</h1>
        <hr />
        <p class="emphasized">404 Error</p>
    </div>
</article>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__footer.php');
} else {
    ?>
        </div>
    </div>
    <?php
}
?>