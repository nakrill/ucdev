<style type="text/css">

    #p_seasonal-items .seasons .season {
        margin: 15px 0;
    }
    #p_seasonal-items .seasons .season h2 {
        margin-left: 15px;
    }
    #p_seasonal-items .seasons .season .background {
        padding: 15px;
        background-color: #fff;
    }
    #p_seasonal-items .seasons .season .items {
        margin: 15px -15px;
        padding-top: 15px;
        border-top: 1px solid #f0f0f0;
    }
    #p_seasonal-items .seasons .season .items .item {
        position: relative;
        float: left;
        width: 25%;
        height: 120px;
        vertical-align: middle;
    }
    #p_seasonal-items .seasons .season .items .item img {
        position:absolute;
        top:0;
        bottom:0;
        margin:auto;
        max-width: 100%;
        height: auto;
        padding: 15px;
        vertical-align: middle;
    }

    @media (max-width: 991px) {

        #p_seasonal-items .seasons .season .items .item {
            width: 50%;
        }

    }

    @media (max-width: 575px) {

        #p_seasonal-items .seasons .season .items .item {
            float: none;
            width: 100%;
            margin: 15px 0 0;
        }

    }

</style>

<div id="p_seasonal-items" class="clearfix">

    <?php
    if ($bigtree['path'][0] == 'account') {
        include(SERVER_ROOT. 'templates/routed/account/__header.php');
    }
    ?>

    <?php if ($bigtree['resources']['heading']) { ?>
        <h1><?php echo $bigtree['resources']['heading']; ?></h1>
    <?php } ?>

    <?php echo $bigtree['resources']['content']; ?>

    <?php

    // HAVE SEASONS
    if (count($bigtree['resources']['seasons']) > 0) {

        ?>

        <div class="seasons">

            <?php

            // LOOP
            foreach ($bigtree['resources']['seasons'] as $season_id => $season) {

                ?>
                <div class="season season-<?php echo $season_id; ?> clearfix">

                    <?php if ($season['image']) { ?>
                        <img src="<?php echo $season['image']; ?>" alt="" />
                    <?php } ?>

                    <?php if ($season['title']) { ?>
                        <h2><?php echo $season['title']; ?></h2>
                    <?php } ?>

                    <div class="background">

                        <?php if ($season['description']) { ?>
                            <div class="description"><?php echo $season['description']; ?></div>
                        <?php } ?>

                        <?php if (count($season['items']) > 0) { ?>
                            <div class="items clearfix">
                                <?php foreach ($season['items'] as $sitem) { ?>
                                    <div class="item">
                                        <?php if ($sitem['url']) { ?>
                                            <a href="<?php echo $sitem['url']; ?>">
                                        <?php } ?>
                                        <?php if ($sitem['image']) { ?>
                                            <img src="<?php echo $sitem['image']; ?>" alt="<?php echo $sitem['title']; ?>" title="<?php echo $sitem['title']; ?>" />
                                        <?php } else { ?>
                                            <span class="title"><?php echo $sitem['title']; ?></span>
                                        <?php } ?>
                                        <?php if ($sitem['url']) { ?>
                                            </a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>

                    </div>

                </div>
                <?php

            }

            ?>

        </div>

        <?php

    }

    ?>

    <?php echo $bigtree['resources']['content_bottom']; ?>

    <?php
    if ($bigtree['path'][0] == 'account') {
        include(SERVER_ROOT. 'templates/routed/account/__footer.php');
    }
    ?>

</div>