<style type="text/css">

    .page.leftcol .col-large {
        float: right;
        width: 75%;
    }
    .page.leftcol .col-large > .padding {
        padding-left: 30px;
    }
    .page.leftcol .col-small {
        float: left;
        width: 25%;
    }

    @media screen and (max-width: 860px) {

        .page.leftcol .col-large {
            float: none;
            width: 100%;
            margin-bottom: 15px;
        }
        .page.leftcol .col-large > .padding {
            padding-left: 0px;
        }
        .page.leftcol .col-small {
            float: none;
            width: 100%;
        }

    }

</style>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__header.php');
} else {
    ?>
    <div class="site-width block_center">
        <div class="page leftcol clearfix">
    <?php
}
?>

<div class="col-large">
    <div class="padding">

        <?php if ($bigtree['resources']['heading']) { ?>
            <h1><?php echo $bigtree['resources']['heading']; ?></h1>
        <?php } ?>

        <?php echo $bigtree['resources']['content']; ?>

    </div>
</div>

<div class="col-small">
    <div class="padding">

        <?php if ($bigtree['resources']['smallcol_heading']) { ?>
            <h1><?php echo $bigtree['resources']['smallcol_heading']; ?></h1>
        <?php } ?>

        <?php echo $bigtree['resources']['smallcol_content']; ?>

    </div>
</div>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__footer.php');
} else {
    ?>
        </div>
    </div>
    <?php
}
?>