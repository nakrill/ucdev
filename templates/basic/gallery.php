<?php

// GALLERY ITEM CLASS
$gallery_item_class = 'col-md-4';

// INIT GALLERY
$_uccms_gallery = new gallery($bigtree['resources']['gallery_id']);

// CUSTOM EDIT LINK
$bigtree['bar_edit_link'] = ADMIN_ROOT . $_uccms_gallery->Extension. '*gallery/edit/' .$bigtree['resources']['gallery_id']. '/';

?>

<link rel="stylesheet" type="text/css" href="<?=WWW_ROOT?>extensions/<?=$_uccms_gallery->Extension;?>/css/master/master.css" />
<link rel="stylesheet" type="text/css" href="<?=WWW_ROOT?>extensions/<?=$_uccms_gallery->Extension;?>/css/custom/master.css" />

<section class="galleryContainer">
    <div class="container">

        <?php if ($bigtree['resources']['heading']) { ?>
            <h1><?php echo $bigtree['resources']['heading']; ?></h1>
        <?php } ?>

        <?php echo $bigtree['resources']['content_top']; ?>

        <?php

        // GROUPS
        $gallery_groups = $_uccms_gallery->getGroups(true);

        // MEDIA
        $gallery_media = $_uccms_gallery->getMedia($gallery_groups, true);

        // GROUP FILTER
        include(SERVER_ROOT. 'extensions/' .$_uccms_gallery->Extension. '/templates/elements/group_filter.php');

        // GALLERY
        include(SERVER_ROOT. 'extensions/' .$_uccms_gallery->Extension. '/templates/elements/gallery.php');

        ?>

        <?php echo $bigtree['resources']['content_bottom']; ?>

    </div>
</section>