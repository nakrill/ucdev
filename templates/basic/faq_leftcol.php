<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__header.php');
} else {
    ?>
    <section id="main-content">
        <div class="container">
    <?php
}
?>

<style type="text/css">

    #faqContainer {
        margin-top: 15px;
    }

    #faqContainer .col_left .contain h2 {
        margin: 7px 0 15px;
    }
    #faqContainer .col_left .contain h2 a {
        color: rgba(0, 0, 0, .5);
    }
    #faqContainer .col_left .contain ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #faqContainer .col_left .contain ul li {
        display: block;
        border-top: 1px solid rgba(0, 0, 0, .1);
    }
    #faqContainer .col_left .contain ul li:last-child {
        border-bottom: 1px solid rgba(0, 0, 0, .1);
    }
    #faqContainer .col_left .contain ul li a {
        display: block;
        padding: 10px;
    }
    #faqContainer .col_left .contain ul li.current a {
        color: <?php echo $bigtree['config']['css']['vars']['link_hover']; ?>;
    }

    #faqContainer .col_right h1 {
        margin-top: 0px;
    }

</style>

<div id="faqContainer">
    <div class="row">

        <div class="col_left col-md-3">
            <div class="contain">

                <?php

                // GET CURRENT FAQ PAGE
                $cfp_query = "SELECT `id`, `parent`, `nav_title`, `path` FROM `bigtree_pages` WHERE (`path`='" .sqlescape(implode('/', $bigtree['path'])). "') AND (`template`='faq_leftcol') LIMIT 1";
                $cfp_q = sqlquery($cfp_query);
                $faq_page = sqlfetch($cfp_q);

                // CURRENT FAN PAGE ID
                $cfp_id = $faq_page['id'];

                // HAVE PARENT
                if ($faq_page['parent'] > 0) {

                    // GET PARENT FAQ PAGE
                    $pfp_query = "SELECT `id`, `parent`, `nav_title`, `path` FROM `bigtree_pages` WHERE (`id`=" .$faq_page['parent']. ")";
                    $pfp_q = sqlquery($pfp_query);
                    $faq_page = sqlfetch($pfp_q);

                }

                // HAVE FAQ PAGE ID
                if ($faq_page['id']) {

                    ?>
                    <h2><a href="/<?php echo $faq_page['path']; ?>/"><?php echo stripslashes($faq_page['nav_title']); ?></a></h2>
                    <?php

                    // LEFT NAVIGATION
                    $left_nav = $cms->getNavByParent($faq_page['id'], 1);

                    // HAVE LEFT NAVIGATION
                    if (count($left_nav) > 0) {
                        ?>
                        <ul>
                            <?php foreach ($left_nav as $nav) { ?>
                                <li class="<?php if ($nav['id'] == $cfp_id) { echo 'current'; } ?>"><a href="<?php echo $nav['link']; ?>"><?php echo $nav['title']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <?php
                    }

                }

                ?>

            </div>
        </div>

        <div class="col_right col-md-9">
            <div class="contain">

                <?php if ($bigtree['resources']['heading']) { ?>
                    <h1><?php echo $bigtree['resources']['heading']; ?></h1>
                <?php } ?>

                <?php echo $bigtree['resources']['content']; ?>

            </div>
        </div>

    </div>
</div>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__footer.php');
} else {
    ?>
        </div>
    </section>
    <?php
}
?>