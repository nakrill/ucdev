<style type="text/css">

    .s_contact .container {
        margin-top: 20px;
        margin-bottom: 40px;
    }

    #rsf_form-vz8 .rsf_container {
        padding: 0px;
    }
    #rsf_form-vz8 .field input[type="text"], #rsf_form-vz8 .field textarea {
        width: 100%;
    }
    #rsf_form-vz8 .field textarea {
        height: 80px;
    }

    @media screen and (max-width: 860px) {


    }

</style>

<div class="s_contact">

    <?php

    // CAROUSEL CONFIG
    $carousel = array(
        'slides'    => $bigtree['resources']['hero'],
        'interval'  => 4000
    );

    // INCLUDE ELEMENT
    include(SERVER_ROOT. 'templates/elements/carousel.php');

    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="txt_cntr"><?=$bigtree['resources']['left_heading']?></h1>
                <?php if ($bigtree['resources']['gmaps_code']) { ?>
                    <figure class="img_inner">
                        <?=htmlspecialchars_decode($bigtree['resources']['gmaps_code'])?>
                    </figure>
                <?php } ?>
                <div itemscope itemtype="http://schema.org/LocalBusiness">
                    <span itemprop="name" style="display: none;"><?=$cms->getSetting('site_name')?></span>
                    <div class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <?php if ($bigtree['resources']['address_street']) { ?>
                            <span itemprop="streetAddress"><?=$bigtree['resources']['address_street']?></span>
                            <br />
                        <?php } ?>
                        <?php if ($bigtree['resources']['address_city']) { ?>
                            <span itemprop="addressLocality"><?=$bigtree['resources']['address_city']?></span>,
                        <?php } ?>
                        <span itemprop="addressRegion"><?=$bigtree['resources']['address_state']?></span> <span itemprop="postalCode"><?=$bigtree['resources']['address_zip']?></span>
                        <span itemprop="addressCountry" style="display: none;">US</span>
                    </div>
                    <?php if ($bigtree['resources']['phone']) { ?>
                        <div>Phone: <span itemprop="telephone"><?=$bigtree['resources']['phone']?></span></div>
                    <?php } ?>
                    <?php if ($bigtree['resources']['email']) { ?>
                        <div style="display: none;">E-mail: <a itemprop="email" class="link4" href="mailto:<?=$bigtree['resources']['email']?>"><?=$bigtree['resources']['email']?></a></div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-6">
                <h1 class="txt_cntr"><?=$bigtree['resources']['right_heading']?></h1>
                <?=htmlspecialchars_decode($bigtree['resources']['revsocial_form'])?>
            </div>
        </div>
    </div>

</div>