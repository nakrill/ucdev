<?php

// RATINGS / REVIEWS
include_once(SERVER_ROOT. 'uccms/includes/classes/ratings-reviews.php');
if (!$_uccms_ratings_reviews) $_uccms_ratings_reviews = new uccms_RatingsReviews;

// LIMIT
if (!$_GET['limit']) $_GET['limit'] = 24;

// CALCULATE OFFSET
if ($_GET['page']) {
    $offset = ((int)$_GET['page'] - 1) * $_GET['limit'];
} else {
    $offset = 0;
}

$_GET['status'] = 1; // ONLY ACTIVE
//$_GET['what']   = 'business'; // ONLY BUSINESSES

// GET REVIEWS
$rr = $_uccms_ratings_reviews->get(array(
    'vars'              => $_GET,
    //'order_field'       => $_GET['order_field'],
    'order_direction'   => 'desc', //$_GET['order_direction'],
    'limit'             => 16, //$_GET['limit'],
    'offset'            => $offset
));


// HAVE RESULTS
if ($rr['num_results'] > 0) {

    // PAGING CLASS
    require_once(SERVER_ROOT. '/uccms/includes/classes/paging.php');

    // INIT PAGING CLASS
    $_paging = new paging();

    // OPTIONAL - URL VARIABLES TO IGNORE
    $_paging->ignore = '__utma,__utmb,__utmc,__utmz,bigtree_htaccess_url';

    // PREPARE PAGING
    $_paging->prepare($_GET['limit'], 10, $_GET['page'], $_GET);

    // GENERATE PAGING
    $pages = $_paging->output($rr['num_results'], $rr['num_total']);

}

?>

<style type="text/css">

    .review-search form {

    }
    .review-search form button {
        width: 144px;
    }
    .review-search form button:hover {
        background: #F03D12;
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#F03D12), to(#F97D27));
        background: -webkit-linear-gradient(0% 0%, 0% 100%, from(#F03D12), to(#F97D27));
        background: -moz-linear-gradient(center top, #F03D12, #F97D27);
        border: 1px solid #F03D12;
    }
    .review-search i {
        margin-top: 21px;
    }
    .review-search form label {
        font-weight: 700;
        font-size: 12px;
        color: #f26739;
        text-transform: uppercase;
    }

    .review-list {
        margin-left: 0px;
        padding: 60px 0 40px 0;
    }
    .review-list .title {
        padding-bottom: 40px;
    }
    .review-list .title h3 {

    }
    .review-list .title h3, .review-list .title a {
        display: inline;
    }
    .review-list .title a {
        font-family: "Roboto", sans-serif;
        font-weight: 700;
        font-size: 11px;
        color: #8f95a3;
        text-transform: uppercase;
        background-color: #d2d5da;
        border: 1px solid #d2d5da;
        padding: 2px 10px;
        border-radius: 4px;
    }

    .review-list .reviews {
        margin: 15px -15px;
    }
    .review-list .reviews > .review {
        float: left;
        width: 30%;
        margin: 0 15px 15px;
        padding: 0;
    }
    .review-list .reviews > .review > .contain {
        margin: 0;
        padding: 20px;
        background-color: rgba(0, 0, 0, .7);
        border-radius: 5px;
        color: rgba(255, 255, 255, 1);
    }
    .review-list .reviews > .review .rating_stars {
        display: inline-block;
    }
    .review-list .reviews > .review .rating_stars .stars {
        float: right;
    }
    .review-list .reviews > .review .rating_stars .nums {
        float: left;
        margin: 3px 0 0 6px;
        font-weight: bold;
    }
    .review-list .reviews > .review .rating_stars .nums .rating {
        color: #333;
    }
    .review-list .reviews > .review .rating_stars .nums .split {
        padding: 0 3px;
        color: #c4c7cc;
    }
    .review-list .reviews > .review .rating_stars .br-widget a.br-selected:after {
        color: #fff;
    }
    .review-list .reviews > .review .rating_stars .nums .review-list .reviews {
        color: #c4c7cc;
    }
    .review-list .reviews > .review .rating_stars .none {
        opacity: .5;
    }

    .review-list .reviews > .review .date {
        padding: 0;
        line-height: 20px;
    }
    .review-list .reviews > .review .rating {
        padding: 0;
    }
    .review-list .reviews > .review p {
        margin: 15px 0 0;
        padding: 0;
    }
    .review-list .reviews > .review .name {
        margin: 15px 0 0;
        font-size: 1.2em;
        opacity: .7;
    }

    @media (max-width: 991.98px) {

        .review-list .reviews > .review {
            width: 46%;
        }

    }

    @media (max-width: 767.98px) {

        .review-list .reviews > .review {
            width: 44%;
        }

    }

    @media (max-width: 575.98px) {

        .review-list .reviews {
            height: auto !important;
            margin: 0px;
        }

        .review-list .reviews > .review {
            position: relative !important;
            top: 0 !important;
            left: 0 !important;
            float: none;
            width: 100%;
            margin: 0 0 15px;
        }

    }

</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

        // REVIEWS - MASONRY
        $('.review-list .reviews').isotope({
            itemSelector: '.review',
            layoutMode: 'masonry'
        })

    });

</script>

<? /*
<div class="review-search">
    <form>
    <div class="form-group">
        <label>Find</label>
        <input id="find" type="text" name="query" placeholder="Start typing something..." value="<?php echo $_REQUEST['query']; ?>" class="form-control" />
    </div>
    <br />
    <button type="submit" class="btn btn-primary">Search</button>
    </form>
</div>
*/ ?>

<div class="site-width block_center">
    <div class="page clearfix">


        <div class="review-list">

            <? /*
            <div class="container title">
                <h3>
                Reviews
                <?php if ($_REQUEST['query']) { ?> for <span style="color:#f26739"><?php echo $_REQUEST['query']; ?></span><?php } ?>
                </h3>
            </div>
            */ ?>

            <div class="container">

                <?php echo $_uccms['_site-message']->display(); ?>

                <?php if ($bigtree['resources']['heading']) { ?>
                    <h1><?php echo $bigtree['resources']['heading']; ?></h1>
                <?php } ?>

                <?php echo $bigtree['resources']['content']; ?>

                <?php

                // HAVE REVIEWS
                if ($rr['num_results'] > 0) {

                    ?>

                    <div class="reviews clearfix">

                        <?php

                        // LOOP
                        foreach ($rr['reviews'] as $rr_id => $review) {

                            ?>

                            <script type="application/ld+json">
                                {
                                    "@context": "http://schema.org/",
                                    "@type": "Review",
                                    "itemReviewed": {
                                        "@type": "Thing",
                                        "name": "Business"
                                    },
                                    "author": {
                                        "@type": "Person",
                                        "name": "<?php echo stripslashes($review['name']); ?>"
                                    },
                                    "reviewRating": {
                                        "@type": "Rating",
                                        "ratingValue": "<?php echo $review['rating']; ?>",
                                        "bestRating": "5"
                                    }
                                }
                            </script>

                            <div class="review clearfix">
                                <div class="contain">

                                    <div class="clearfix">

                                        <div class="date col-1-2 col-md-6">
                                            <?php if ($review['dt_created'] != '0000-00-00 00:00:00') { echo date('n/j/Y', strtotime($review['dt_created'])); } ?>
                                        </div>

                                        <div class="rating rating_stars col-1-2 col-md-6">
                                            <div class="stars">
                                                <?php
                                                $star_vars = array(
                                                    'el_id'     => 'stars-' .$rr_id,
                                                    'value'     => $review['rating'],
                                                    'readonly'  => true
                                                );
                                                include(SERVER_ROOT. 'templates/elements/ratings-reviews/stars.php');
                                                if ((count((array)$review['data']['criteria']) > 0) && (count((array)$criteria) > 0)) {
                                                    ?><label>Overall</label><?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                    </div>

                                    <?php if ($review['title']) { ?>
                                        <h4><?php echo stripslashes($title); ?></h4>
                                    <?php } ?>

                                    <?php if ($review['content']) { ?>
                                        <p>
                                            <?php echo stripslashes($review['content']); ?>
                                        </p>
                                    <?php } ?>

                                    <div class="name">
                                        <?php echo stripslashes($review['name']); ?>
                                    </div>

                                </div>
                            </div>

                            <?php

                        }

                        ?>

                    </div>

                    <?php

                // NO REVIEWS
                } else {
                    ?>
                    <div class="container">
                        <?php echo $bigtree['resources']['no_results_content']; ?>
                    </div>
                    <?php
                }

                ?>

            </div>

        </div>

    </div>
</div>