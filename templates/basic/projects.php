<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.0/isotope.pkgd.min.js"></script>

<style type="text/css">

    .p_projects h1 {
        margin-top: 0px;
    }
    .p_projects .project_categories {
        margin: 15px 0;
        padding: 0 0 10px 0;
        border-bottom: 1px solid #999;
    }
    .p_projects .project_categories a {
        display: inline-block;
        margin-right: 10px;
        font-size: 1.1em;
    }
    .p_projects .project_categories .button {
        padding: 5px 10px;
        letter-spacing: 0px;
    }
    .p_projects .project_categories a.active {
        color: #555;
    }

</style>

<? /*
<script type="text/javascript">

    $(window).load(function() {
        $('.p_projects .projects').isotope({
            itemSelector: '.project',
            layoutMode: 'masonry'
        });
    });

    $(document).ready(function() {

        $('.p_projects .project_categories a').click(function(e) {
            var href = $(this).attr('href');
            if (href == '#') {
                e.preventDefault();
                var category = $(this).attr('data-category');
                if (category == 0) {
                    $('.p_projects .projects').isotope({
                        filter: '*'
                    });
                } else {
                    $('.p_projects .projects').isotope({
                        filter: '.project.category-' +category
                    });
                }
                $('.p_projects .project_categories a').removeClass('active');
                $(this).addClass('active');
            }
        });

    });

</script>
*/ ?>

<?php if ($bigtree['resources']['hero_image']) { ?>
    <div id="main-hero" style="background-image: url('<?php echo $bigtree['resources']['hero_image']; ?>');">
        <div class="content">
            <?php if ($bigtree['resources']['heading']) { ?>
                <h1><?php echo $bigtree['resources']['heading']; ?></h1>
            <?php } ?>
            <?php if ($bigtree['resources']['hero_button_url']) { ?>
                <a href="<?php echo $bigtree['resources']['hero_button_url']; ?>" class="btn btn-primary"><?php echo $bigtree['resources']['hero_button_text']; ?></a>
            <?php } ?>
        </div>
    </div>
<?php } else { ?>
    <div style="height: 100px;"></div>
<?php } ?>

<?php

if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__header.php');
} else {
    ?>
    <section id="main-content">
        <div class="container">
    <?php
}
?>

<div class="p_projects">

    <?php if ($bigtree['resources']['heading']) { ?>
        <h1><?php echo $bigtree['resources']['heading']; ?></h1>
    <?php } ?>

    <?php if ($bigtree['resources']['content']) { ?>
        <div class="content">
            <?php echo $bigtree['resources']['content']; ?>
        </div>
    <?php } ?>

    <?php

    // GET SUB PAGES (PROJECTS)
    $pages_query = "SELECT * FROM `bigtree_pages` WHERE (`template`='project') ORDER BY `nav_title` ASC";
    $pages_q = sqlquery($pages_query);

    // HAVE SUB-PAGES
    if (count($pages_q) > 0) {

        /*
        $categories = array();

        // GET CATEGORIES
        $categories_query = "SELECT * FROM `custom_project_categories` ORDER BY `title` ASC";
        $categories_q = sqlquery($categories_query);
        while ($category = sqlfetch($categories_q)) {
            $categories[$category['id']] = $category;
        }

        ?>

        <div class="project_categories">
            <a href="#" data-category="0" class="active">All</a>
            <?php foreach ($categories as $category) { ?>
                <a href="#" data-category="<?php echo $category['id']; ?>" class="button"><?php echo stripslashes($category['title']); ?></a>
            <?php } ?>
        </div>
        */ ?>

        <section class="projects row clearfix">
            <?php

            // LOOP THROUGH SUB PAGES (PROJECTS)
            while ($page = sqlfetch($pages_q)) {

                // PAGE RESOURCES
                $resources = json_decode($page['resources'], true);

                //echo print_r($resources);

                ?>
                <div class="project col-md-4 col-sm-6 col-xs-12">
                    <div class="hovereffect">
                        <div class="background" style="background-image: url('<?php echo BigTree::prefixFile(str_replace('{staticroot}', '/', $resources['image']), 's_'); ?>');"></div>
                        <div class="overlay">
                            <h2><a href="/<?php echo $page['path']; ?>/"><?php echo $page['title']; ?></a></h2>
                            <p class="icon-links">
                                <a href="#">
                                    <span class="fa fa-facebook"></span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>

                <? /*
                <div class="project<?php if (count($resources['categories']) > 0) { foreach ($resources['categories'] as $cat) { echo ' category-' .$cat; } } ?> col-md-4">
                    <a href="/<?php echo $page['path']; ?>/">
                        <?php if ($resources['image']) { ?>
                            <img src="" alt="<?php echo $page['title']; ?>" />
                        <?php } ?>
                        <span class="title">
                            <?php echo $page['title']; ?>
                        </span>
                    </a>
                </div>
                <?php
                */

            }

            ?>
        </section>

        <?php
    }

    ?>

</div>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__footer.php');
} else {
    ?>
        </div>
    </section>
    <?php
}
?>