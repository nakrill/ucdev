<?php

// CAROUSEL CONFIG
$carousel = array(
    'slides'    => $bigtree['resources']['hero'],
    'interval'  => 4000
);

// INCLUDE ELEMENT
include(SERVER_ROOT. 'templates/elements/carousel.php');

?>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__header.php');
} else {
    ?>
    <section id="main-content">
        <div class="container">
    <?php
}
?>

<?php if ($bigtree['resources']['heading']) { ?>
    <h1><?php echo $bigtree['resources']['heading']; ?></h1>
<?php } ?>

<?php echo $bigtree['resources']['content']; ?>

<?php

// ALTERNATING CONTENT CONFIG
$content_alt = array(
    'rows'    => $bigtree['resources']['content_alt']
);

// INCLUDE ELEMENT
include(SERVER_ROOT. 'templates/elements/content-alt.php');

// HAVE GALLERY CODE
if ($bigtree['resources']['dam_gallery_code']) {
    echo htmlspecialchars_decode($bigtree['resources']['dam_gallery_code']);
}

?>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__footer.php');
} else {
    ?>
        </div>
    </section>
    <?php
}
?>