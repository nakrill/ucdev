<div class="site-width block_center">
    <div class="page">

        <div class="clearfix">

            <h1><?=$page_header?></h1>

            <?=$page_content?>

            <? include "../templates/layouts/_callouts.php" ?>

        </div>

        <?php

        if ($gallery_id) {

            // GALLERY ITEM CLASS
            $gallery_item_class = 'col-1-4';

            // INIT GALLERY
            $_uccms_gallery = new gallery($gallery_id);

            ?>

            <link rel="stylesheet" type="text/css" href="<?=WWW_ROOT?>extensions/<?=$_uccms_gallery->Extension;?>/css/master/master.css" />
            <link rel="stylesheet" type="text/css" href="<?=WWW_ROOT?>extensions/<?=$_uccms_gallery->Extension;?>/css/custom/master.css" />

            <?php

            // GROUPS
            $gallery_groups = $_uccms_gallery->getGroups(true);

            // MEDIA
            $gallery_media = $_uccms_gallery->getMedia($gallery_groups, true);

            // GROUP FILTER
            include(SERVER_ROOT. 'extensions/' .$_uccms_gallery->Extension. '/templates/elements/group_filter.php');

            // GALLERY
            include(SERVER_ROOT. 'extensions/' .$_uccms_gallery->Extension. '/templates/elements/gallery.php');

        }

        ?>

    </div>
</div>