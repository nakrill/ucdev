<style type="text/css">

    #p_contact .items .item {
        height: 385px;
    }
    #p_contact .items .item h2 {
        margin: 0px;
    }
    #p_contact .items .item .map {
        margin-bottom: 12px;
    }
    #p_contact .items .item .map iframe {
        width: 100%;
        height: 250px;
    }
    #p_contact .items .item .phone {
        margin-top: 8px;
    }
    #p_contact .items .item .email {
        display: none;
        margin-top: 8px;
    }
    #p_contact .items .item .url {
        display: none;
        margin-top: 8px;
    }
    #p_contact .items .item .address {
        margin-top: 8px;
    }
    #p_contact .items .item .lat_long {
        display: none;
        margin-top: 8px;
    }

    @media screen and (max-width: 860px) {

       #p_contact .items .col-1-2 {
            float: none;
            width: 100%;
            height: auto;
            padding: 0 0 20px 0;
        }
        .s_contact .col-1-2:last-child {
            padding-bottom: 0px;
        }

    }

</style>

<div id="p_contact">
    <div class="site-width block_center">
        <div class="page">

            <?php if ($bigtree['resources']['heading']) { ?>
                <h1><?php echo $bigtree['resources']['heading']; ?></h1>
            <?php } ?>

            <?php if ($bigtree['resources']['content']) { ?>
                <div class="content"><?php echo $bigtree['resources']['content']; ?></div>
            <?php } ?>

            <?php if (count($bigtree['resources']['locations']) > 0) { ?>

                <div class="items grid">

                    <?php foreach ($bigtree['resources']['locations'] as $location) { ?>

                        <div class="item col-1-2" itemscope itemtype="http://schema.org/LocalBusiness">

                            <?php if ($location['map_url']) { ?>
                                <link href="<?php echo $location['map_url']; ?>" itemprop="hasMap" />
                            <?php } ?>
                            <?php if ($location['social_facebook']) { ?>
                                <link href="<?php echo $location['social_facebook']; ?>" itemprop="sameAs" />
                            <?php } ?>
                            <?php if ($location['social_yelp']) { ?>
                                <link href="<?php echo $location['social_yelp']; ?>" itemprop="sameAs" />
                            <?php } ?>
                            <?php if ($location['social_googleplus']) { ?>
                                <link href="<?php echo $location['social_googleplus']; ?>" itemprop="sameAs" />
                            <?php } ?>
                            <?php if ($location['social_bingplaces']) { ?>
                                <link href="<?php echo $location['social_bingplaces']; ?>" itemprop="sameAs" />
                            <?php } ?>

                            <?php if ($location['map_embed']) { ?>
                                <div class="map">
                                    <?php echo htmlspecialchars_decode($location['map_embed']); ?>
                                </div>
                            <?php } ?>

                            <?php if ($location['name']) { ?>
                                <h2 itemprop="name"><?php echo $location['name']; ?></h2>
                            <?php } ?>

                            <?php if ($location['phone']) { ?>
                                <div class="phone"><a href="tel:+1<?php echo preg_replace('/\D/', '', $location['phone']); ?>" itemprop="telephone"><?php echo $location['phone']; ?></a></div>
                            <?php } ?>

                            <?php if ($location['email']) { ?>
                                <div class="email"><a href="mailto:<?php echo $location['email']; ?>" itemprop="email"><?php echo $location['email']; ?></a></div>
                            <?php } ?>

                            <?php if ($location['url']) { ?>
                                <div class="url"><a href="<?php echo $location['url']; ?>" target="_blank" itemprop="url"><?php echo $location['url']; ?></a></div>
                            <?php } ?>

                            <div class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <?php if ($location['address1']) { ?>
                                    <span itemprop="streetAddress">
                                        <?php echo $location['address1']; ?>
                                        <?php if ($location['address2']) { ?>
                                            <br />
                                            <?php echo $location['address2']; ?>
                                        <?php } ?>
                                    </span>
                                    <br />
                                <?php } ?>
                                <?php if ($location['city']) { ?>
                                    <span itemprop="addressLocality"><?php echo $location['city']; ?></span>,
                                <?php } ?>
                                <span itemprop="addressRegion"><?php echo $location['state']; ?></span> <span itemprop="postalCode"><?php echo $location['zip']; ?></span>
                            </div>

                            <?php if (($location['latitude']) || ($location['longitude'])) { ?>
                                <div class="lat_lon" itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
                                    <?php if ($location['latitude']) { ?>
                                        <meta itemprop="latitude" content="<?php echo $location['latitude']; ?>" />
                                    <?php } ?>
                                    <?php if ($location['longitude']) { ?>
                                        <meta itemprop="longitude" content="<?php echo $location['longitude']; ?>" />
                                    <?php } ?>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } ?>

                </div>

            <?php } ?>

        </div>
    </div>
</div>