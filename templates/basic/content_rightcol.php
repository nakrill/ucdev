<style type="text/css">

    #main-content .carousel {
        margin-top: 30px;
    }

    #main-content .col-small .block {
        margin: 30px 0;
    }
    #main-content .col-small .block > .contain {
        padding: 15px;
        background-color: rgba(0, 0, 0, .1);
        text-align: center;
        border-radius: 5px;
    }

</style>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__header.php');
} else {
    ?>
    <section id="main-content">
        <div class="container">
    <?php
}
?>

<div class="row">

    <div class="col-large col-md-8">

        <?php

        // CAROUSEL CONFIG
        $carousel = array(
            'slides'    => $bigtree['resources']['hero'],
            'interval'  => 4000
        );

        // INCLUDE ELEMENT
        include(SERVER_ROOT. 'templates/elements/carousel.php');

        ?>

        <?php if ($bigtree['resources']['heading']) { ?>
            <h1><?php echo $bigtree['resources']['heading']; ?></h1>
        <?php } ?>

        <?php echo $bigtree['resources']['content']; ?>

    </div>

    <div class="col-small col-md-4">

        <?php

        // GET CONTENT BLOCKS
        $blocks = $cms->getSetting('right-column_shared');

        // HAVE BLOCKS
        if (count($blocks) > 0) {

            // LOOP
            foreach ($blocks as $block) {

                // DON'T SHOW ON THIS PAGE
                if (is_array($block['dont_show_on']) && (in_array($bigtree['page']['id'], $block['dont_show_on']))) {
                    continue;
                }

                ?>
                <div class="block">
                    <div class="contain">
                        <?php if ($block['heading_image']) { ?>
                            <img src="<?php echo $block['heading_image']; ?>" alt="" class="heading-image" />
                        <?php } ?>
                        <?php if ($block['heading_text']) { ?>
                            <h3><?php echo $block['heading_text']; ?></h3>
                        <?php } ?>
                        <?php if ($block['content']) { ?>
                            <div class="content"><?php echo $block['content']; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <?php

            }

        }

        ?>

    </div>

</div>

<?php

// ALTERNATING CONTENT CONFIG
$content_alt = array(
    'rows'    => $bigtree['resources']['content_alt']
);

// INCLUDE ELEMENT
include(SERVER_ROOT. 'templates/elements/content-alt.php');

?>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__footer.php');
} else {
    ?>
        </div>
    </section>
    <?php
}
?>