<?php

if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__header.php');
} else {
    ?>
    <div class="site-width block_center">
        <div class="page clearfix">
    <?php
}

// GET CURRENT FAQ PAGE
$cfp_query = "SELECT * FROM `bigtree_pages` WHERE (`path`='" .sqlescape(implode('/', $bigtree['path'])). "') AND (`template`='faq_expand_newpage') LIMIT 1";
$cfp_q = sqlquery($cfp_query);
$faq_page = sqlfetch($cfp_q);

// HAVE PARENT
if ($faq_page['parent'] > 0) {

    // GET PARENT FAQ PAGE
    $pfp_query = "SELECT * FROM `bigtree_pages` WHERE (`id`=" .$faq_page['parent']. ") AND (`template`='faq_expand_newpage')";
    $pfp_q = sqlquery($pfp_query);
    $parent_page = sqlfetch($pfp_q);

}

// HAVE PAGE ID
if ($faq_page['id']) {

    $faq_page = $cms->getPage($faq_page['id']);

    // NO PARENT FOUND - USE THIS PAGE
    if (!$parent_page['id']) {
        $parent_page = $faq_page;
    }

    $parent_page = $cms->getPage($parent_page['id']);

}


?>

<style type="text/css">

    #faqContainer {
        margin-top: 15px;
    }

    #faqContainer .questions .qa {
        margin-bottom: 10px;
        padding: 0px;
        background-color: <?php echo $bigtree['config']['css']['vars']['siteThird']; ?>;
    }
    #faqContainer .questions .qa:last-child {
        margin-bottom: none;
    }
    #faqContainer .questions .qa h3 {
        margin: 0;
    }
    #faqContainer .questions .qa h3 a {
        display: block;
        padding: 10px;
        line-height: 1em;
    }
    #faqContainer .questions .qa h3 a i {
        float: right;
        margin-top: 1px;
    }
    #faqContainer .questions .qa .answer {
        padding: 0 10px;
    }

</style>

<div id="faqContainer">
    <div class="grid">

        <div class="col_main">
            <div class="padding">

                <?php if ($parent_page['id']) { ?>
                    <h1><?php echo $parent_page['resources']['heading']; ?></h1>
                    <div>
                        <?php echo $parent_page['resources']['content']; ?>
                    </div>
                <?php } ?>


                <div class="questions">

                    <?php

                    // HAVE PARENT PAGE ID
                    if ($parent_page['id']) {

                        // GET QUESTIONS & ANSWERS
                        $qas = $cms->getNavByParent($parent_page['id'], 1);

                        // HAVE QUESTIONS & ANSWERS
                        if (count($qas) > 0) {
                            foreach ($qas as $qa) {
                                $page = $cms->getPage($qa['id']);
                                if ($page['id'] == $faq_page['id']) {
                                    ?>
                                    <a name="answer"></a>
                                    <?php
                                }
                                ?>
                                <div class="qa">
                                    <h3 class="clearfix"><a href="<?php echo $qa['link']; ?>#answer"><?php echo $page['resources']['heading']; ?><i class="fa fa-caret-<?php if ($page['id'] == $faq_page['id']) { echo 'up'; } else { echo 'down'; } ?>" aria-hidden="true"></i></a></h3>
                                    <?php if ($page['id'] == $faq_page['id']) { ?>
                                        <div class="answer">
                                            <?php echo $page['resources']['content']; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php
                            }
                        }

                    }

                    ?>

                </div>

            </div>
        </div>

    </div>
</div>

<?php
if ($bigtree['path'][0] == 'account') {
    include(SERVER_ROOT. 'templates/routed/account/__footer.php');
} else {
    ?>
        </div>
    </div>
    <?php
}
?>