<?php

// IMAGES
$images = array();

// HAVE MAIN IMAGE
if ($bigtree['resources']['image']) {
    $images[] = array(
        'url'       => $bigtree['resources']['image'],
        'caption'   => ''
    );
}

// HAVE ADDITIONAL IMAGES
if (count($bigtree['resources']['images'] > 0)) {
    foreach ($bigtree['resources']['images'] as $image) {
        $images[] = array(
            'url'       => $image['image'],
            'caption'   => $image['caption']
        );
    }
}

?>

<style type="text/css">

    .p_project .breadcrumbs {
        padding: 20px 0;
        border-bottom: 1px solid #eee;
        font-size: 1.1em;
    }

    #projectCarousel {
        margin-bottom: 190px;
    }
    #projectCarousel .carousel-caption {
        padding-bottom: 0px;
    }
    #projectCarousel .carousel-indicators {
        left: 0px;
        bottom: -160px;
        width: 100%;
        margin: 0px;
    }
    #projectCarousel .carousel-indicators li {
        float: none;
        width: 180px;
        height: 110px;
        margin: 15px;
        border: 2px solid transparent;
        border-radius: 0;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        opacity: .7;
        transition: all 0.3s ease-in-out;
    }
    #projectCarousel .carousel-indicators li.active, #projectCarousel .carousel-indicators li:hover {
        opacity: 1;
    }

    .p_project .overview {
        margin: 30px 0;
    }
    .p_project .overview h3 {
        margin: 0 0 15px;
        padding: 0 0 10px;
        border-bottom: 1px solid #eee;
    }
    .p_project .overview .info .dl-horizontal dt {
        text-align: left;
    }
    .p_project .overview .info .dl-horizontal dd {
        margin: 15px 0;
    }

</style>

<?php if ($bigtree['resources']['hero_image']) { ?>
    <div id="main-hero" style="background-image: url('<?php echo $bigtree['resources']['hero_image']; ?>');">
        <div class="content">
            <?php if ($bigtree['resources']['title']) { ?>
                <h1><?php echo $bigtree['resources']['title']; ?></h1>
            <?php } ?>
            <?php if ($bigtree['resources']['hero_button_url']) { ?>
                <a href="<?php echo $bigtree['resources']['hero_button_url']; ?>" class="btn btn-primary"><?php echo $bigtree['resources']['hero_button_text']; ?></a>
            <?php } ?>
        </div>
    </div>
<?php } else { ?>
    <div style="height: 100px;"></div>
<?php } ?>

<div class="p_project">

    <div class="breadcrumbs clearfix">
        <div class="container">
            <a href="../" class="back">Projects</a> <i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo $bigtree['resources']['title']; ?>
        </div>
    </div>

    <?php
    if ($bigtree['path'][0] == 'account') {
        include(SERVER_ROOT. 'templates/routed/account/__header.php');
    } else {
        ?>
        <section id="main-content">
            <div class="container">
        <?php
    }
    ?>

    <div class="project">

        <?php if (count($images) > 0) { ?>

            <div id="projectCarousel" class="images carousel slide">

                <div class="carousel-inner">
                    <?php foreach ($images as $i => $image) { ?>
                        <div class="item <?php if ($i == 0) { echo 'active'; } ?>">
                            <img src="<?php echo BigTree::prefixFile($image['url'], 'l_'); ?>" alt="" />
                            <?php if ($image['caption']) { ?>
                                <div class="carousel-caption">
                                    <h4><?php echo $image['caption']; ?></h4>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <a class="left carousel-control" href="#projectCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#projectCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
                <ol class="carousel-indicators">
                    <?php foreach ($images as $i => $image) { ?>
                        <li data-target="#projectCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) { echo 'active'; } ?>" style="background-image: url('<?php echo BigTree::prefixFile($image['url'], 's_'); ?>');"></li>
                    <?php } ?>
                </ol>
            </div>

        <?php } ?>

        <div class="overview row">

            <div class="info col-md-6">

                <h3>Information</h3>

                <dl class="dl-horizontal">

                    <?php if ($bigtree['resources']['location']) { ?>
                        <dt>Location:</dt>
                        <dd><?php echo $bigtree['resources']['location']; ?></dd>
                    <?php } ?>

                    <?php if ($bigtree['resources']['customer']) { ?>
                        <dt>Customer:</dt>
                        <dd><?php echo $bigtree['resources']['customer']; ?></dd>
                    <?php } ?>

                    <?php if ($bigtree['resources']['product']) { ?>
                        <dt>Product:</dt>
                        <dd><?php echo $bigtree['resources']['product']; ?></dd>
                    <?php } ?>

                </dl>

            </div>

            <div class="desc col-md-6">

                <?php if ($bigtree['resources']['description']) { ?>

                    <h3>Description</h3>

                    <div class="description">
                        <?php echo $bigtree['resources']['description']; ?>
                    </div>

                <?php } ?>

            </div>

        </div>

    </div>

    <?php
    if ($bigtree['path'][0] == 'account') {
        include(SERVER_ROOT. 'templates/routed/account/__footer.php');
    } else {
        ?>
            </div>
        </section>
        <?php
    }
    ?>

    <?php

    /*
    // HAVE PROJECT CATEGORIES
    if (count($bigtree['resources']['categories'])) {

        $allcats = array();
        $cats = array();

        // GET ALL CATEGORIES
        $categories_query = "SELECT * FROM `custom_project_categories` ORDER BY `title` ASC";
        $categories_q = sqlquery($categories_query);
        while ($category = sqlfetch($categories_q)) {
            $allcats[$category['id']] = $category;
        }

        // LOOP
        foreach ($bigtree['resources']['categories'] as $cat) {
            if ($allcats[$cat]['title']) {
                $cats[] = stripslashes($allcats[$cat]['title']);
            }
        }

        if (count($categories) > 0) {
            ?>
            <div class="category box1" style="padding: 9px 12px 7px;">
                <span class="label">Categor<?php if (count($categories) > 1) { echo 'ies'; } else { echo 'y'; } ?>:</span> <?php echo implode(', ', $cats); ?>
            </div>
            <?php
        }

    }
    */

    ?>

</div>