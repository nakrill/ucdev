<?php

// LOGGED IN
if ($_uccms['_account']->loggedIn()) {

    // LOGOUT
    $logout = $_uccms['_account']->logout(false);

    if ($_uccms['_account']->loggedIn()) {
        $_uccms['_site-message']->set('error', 'Failed to log you out.');
    } else {
        $_uccms['_site-message']->set('success', 'You have been logged out.');
    }

    BigTree::redirect('../login/');

}

?>