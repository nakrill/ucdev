<?php

// CLEAN UP
$token = trim($_REQUEST['t']);

// HAVE TOKEN
if ($token) {

    // CHECK VERIFICATION TOKEN
    $check = $_uccms['_account']->checkToken('verify', $token);

    // MATCH FOUND
    if ($check) {

        // REMOVE VERIFICATION CODE
        $_uccms['_account']->updateAccount($check, array(
            'verify' => ''
        ));

        $_uccms['_site-message']->set('success', 'Your email address has been verified.');

    }

}

BigTree::redirect('../');

?>