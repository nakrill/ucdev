<?php

// IS LOGGED IN
if ($_uccms['_account']->loggedIn()) {

    // RESEND VERIFICATION EMAIL
    $resend = $_uccms['_account']->resendVerification();

    // SENT
    if ($resend['success'] == true) {
        if ($resend['message']) {
            $_uccms['_site-message']->set('info', $resend['message']);
        } else {
            $_uccms['_site-message']->set('success', 'A verification email has been sent to your email address.');
        }

    // FAILED
    } else {
        if ($resend['message']) {
            $_uccms['_site-message']->set('error', $resend['message']);
        } else {
            $_uccms['_site-message']->set('error', 'Failed to send verification email.');
        }
    }

// NOT LOGGED IN
} else {
    $_uccms['_site-message']->set('error', 'Please log in first.');
    $_uccms['_account']->setRedirect('/account/verify/resend/');
    BigTree::redirect('../../');
}

?>

<div class="site-width block_center">
    <div class="page">

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>

        <a href="javascript:history.back();">&laquo; Back</a>

    </div>
</div>