<?php

// PAYMENT PROFILES ENABLED
if ($_uccms['_account']->paymentProfilesEnabled()) {

    // SAVE TO SESSION
    $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment'] = $_POST['payment'];

    // ID SPECIFIED
    if ($_POST['id']) {

        // GET PAYMENT PROFILE
        $prof = $_uccms['_account']->pp_getProfile((int)$_POST['id'], $_uccms['_account']->userID());

        // PROFILE NOT FOUND
        if (!$prof['id']) {
            $_uccms['_site-message']->set('error', 'Payment method not found or not yours.');
            BigTree::redirect('../');
        }

        // CARD INFO
        $data = array(
            'card' => array(
                'name'          => $_POST['payment']['name'],
                'number'        => $_POST['payment']['number'],
                'expiration'    => $_POST['payment']['expiration'],
                'zip'           => $_POST['payment']['zip']
            ),
            'default' => (int)$_POST['payment']['default']
        );

        // UPDATE PROFILE
        $result = $_uccms['_account']->pp_updateProfile($prof['id'], $data);

        // PROFILE UPDATED
        if ($result['id']) {
            unset($_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']);
            $_uccms['_site-message']->set('success', 'Payment method updated.');

        // FAILED
        } else {
            if ($result['error']) {
                $_uccms['_site-message']->set('error', $result['error']);
            } else {
                $_uccms['_site-message']->set('error', 'Failed updating payment method. Please try again.');
            }
            BigTree::redirect('../edit/?id=' .$prof['id']);
        }

    // NEW PROFILE
    } else {

        // GET ACCOUNT INFO
        $account = $_uccms['_account']->getAccount(null, true, $_uccms['_account']->userID());

        // PROFILE DATA
        $data = array(
            'customer' => array(
                'description'   => '',
                'email'         => $account['email']
            ),
            'card' => array(
                'name'          => $_POST['payment']['name'],
                'number'        => $_POST['payment']['number'],
                'expiration'    => $_POST['payment']['expiration'],
                'zip'           => $_POST['payment']['zip'],
                'cvv'           => $_POST['payment']['cvv']
            ),
            'default' => (int)$_POST['payment']['default']
        );

        // CREATE PROFILE
        $result = $_uccms['_account']->pp_createProfile($data);

        // PROFILE CREATED
        if ($result['id']) {
            unset($_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']);
            $_uccms['_site-message']->set('success', 'Payment method added.');

        // FAILED
        } else {
            if ($result['error']) {
                $_uccms['_site-message']->set('error', $result['error']);
            } else {
                $_uccms['_site-message']->set('error', 'Failed adding payment method. Please try again.');
            }
            BigTree::redirect('../edit/');
        }

    }

// PAYMENT PROFILES NOT ENABLED
} else {
    $_uccms['_site-message']->set('error', 'Payment profiles not enabled.');
}

BigTree::redirect('../');

?>