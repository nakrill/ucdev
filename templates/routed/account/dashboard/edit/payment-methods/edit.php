<?php

// PAYMENT PROFILES ENABLED
if ($_uccms['_account']->paymentProfilesEnabled()) {

    // MAKE PAGE LOAD THROUGH HTTPS
    $cms->makeSecure();

    // ID SPECIFIED
    if ($_REQUEST['id']) {

        // GET PAYMENT PROFILE
        $prof = $_uccms['_account']->pp_getProfile((int)$_REQUEST['id'], $_uccms['_account']->userID());

        // PROFILE NOT FOUND
        if (!$prof['id']) {
            $_uccms['_site-message']->set('error', 'Payment method not found or not yours.');
            BigTree::redirect('../');
        }

    }

    // PAYMENT GATEWAY
    $gateway = new BigTreePaymentGateway();

    ?>

    <style type="text/css">

        #edit_profile .note {
            margin-bottom: 10px;
        }
        #edit_profile input[name="payment[number]"], #edit_profile input[name="payment[name]"] {
            width: 250px;
        }
        #edit_profile input[name="payment[expiration]"] {
            width: 80px;
        }
        #edit_profile input[name="payment[zip]"] {
            width: 80px;
        }
        #edit_profile input[name="payment[cvv]"] {
            width: 40px;
        }
        #edit_profile .link_delete {
            display: block;
            margin: 20px 0;
        }

    </style>

    <div id="edit_profile">

        <h3><?php if ($prof['id']) { ?>Edit<?php } else { ?>Add<?php } ?> Payment Method</h3>

        <form action="../process/" method="post">
        <input type="hidden" name="id" value="<?php echo $prof['id']; ?>" />

        <div class="note">
            * All fields required.
        </div>

        <div class="row">
            <div class="col-md-4">

                <fieldset class="form-group">
                    <label>Card Number</label>
                    <?php if (($prof['id']) && ($gateway->Service != 'basys')) { ?>
                        <?php if ($prof['type']) { echo ucwords($prof['type']). ' - '; } echo $prof['lastfour']; ?>
                    <?php } else { ?>
                        <input type="text" name="payment[number]" value="<?php echo $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['number']; ?>" class="form-control" />
                    <?php } ?>
                </fieldset>

                <fieldset class="form-group">
                    <label>Name On Card</label>
                    <input type="text" name="payment[name]" value="<?php if ($_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['name']) { echo $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['name']; } else { echo stripslashes($prof['name']); } ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Card Expiration</label>
                    <input type="text" name="payment[expiration]" value="<?php if ($_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['expires']) { echo $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['expires']; } else if ($prof['expires']) { echo date('m/Y', strtotime($prof['expires'])); } ?>" placeholder="mm/yyyy" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Card Zip</label>
                    <input type="text" name="payment[zip]" value="<?php if ($_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['zip']) { echo $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['zip']; } else { echo $prof['zip']; } ?>" class="form-control" />
                </fieldset>

                <fieldset class="form-group">
                    <label>Card CVV</label>
                    <input type="text" name="payment[cvv]" value="<?php echo $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']['cvv']; ?>" class="form-control" />
                </fieldset>

                <fieldset class="checkbox">
                    <label>
                        <input type="checkbox" name="payment[default]" value="1" <?php if ($prof['default']) { ?>checked="checked"<?php } ?> />Default
                    </label>
                </fieldset>

                <fieldset>
                    <input type="submit" value="Save" class="btn btn-primary" />
                </fieldset>

            </div>
        </div>

        </form>

        <?php if ($prof['id']) { ?>
            <a href="../delete/?id=<?php echo $prof['id']; ?>" class="link_delete" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');">Delete Payment Method</a>
        <?php } ?>

    </div>

    <?php

// PAYMENT PROFILES NOT ENABLED
} else {
    $_uccms['_site-message']->set('error', 'Payment profiles not enabled.');
    BigTree::redirect('../');
}

?>