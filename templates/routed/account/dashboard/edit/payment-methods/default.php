<?php

// PAYMENT PROFILES ENABLED
if ($_uccms['_account']->paymentProfilesEnabled()) {

    unset($_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment']);

    $allprofiles = array();

    // GET PAYMENT PROFILES
    $prof_sql = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$_uccms['_account']->userID(). ") AND (`active`=1) AND (`dt_deleted`='0000-00-00 00:00:00') ORDER BY `default` DESC, `dt_updated` DESC";
    $prof_query = sqlquery($prof_sql);
    while ($prof = sqlfetch($prof_query)) {
        $allprofiles[$prof['id']] = $prof;
    }

    ?>

    <style type="text/css">

        #payment_profiles .message-center {
            text-align: center;
        }
        #payment_profiles h3 {
            float: left;
        }
        #payment_profiles .link_add {
            float: right;
            display: block;
            margin-top: 15px;
        }
        #payment_profiles .profile {
            margin-bottom: 25px;
        }
        #payment_profiles .profile h4 {
            margin: 0 0 5px;
            font-size: 1.3em;
            letter-spacing: 0px;
        }
        #payment_profiles .profile h4 .default {
            display: inline-block;
            padding: 2px 0 0 8px;
            vertical-align: top;
            font-size: .7em;
            font-weight: normal;
        }
        #payment_profiles .profile .info {
            padding: 20px 20px 5px 20px;
            background-color: <?php echo $bigtree["config"]["css"]["vars"]['siteThird']; ?>;
        }
        #payment_profiles .profile .info .link_edit {
            float: right;
        }
        #payment_profiles .profile .info .item {
            margin-bottom: 15px;
        }
        #payment_profiles .profile .info .item:last-child {
            margin-bottom: 0px;
        }
        #payment_profiles .profile .info .item h5 {
            margin: 0 0 4px 0;
            font-size: 1.1em;
        }
        #payment_profiles .profile .view-payments {
            padding: 0 20px 20px;
            background-color: <?php echo $bigtree["config"]["css"]["vars"]['siteThird']; ?>;
        }

        #transactions {
            margin-top: 30px;
        }
        #transactions .head {
            margin-bottom: 15px;
        }
        #transactions .head h3 {
            float: left;
            margin: 0px;
        }
        #transactions .head select {
            float: right;
        }
        #transactions th {
            padding: 0 4px;
            text-align: left;
        }
        #transactions tr:nth-child(even) {
            background-color: #f5f5f5;
        }
        #transactions td {
            padding: 6px;
        }
        #transactions .title {
            width: 100px;
            text-align: left;
        }
        #transactions .dt {
            width: 180px;
            text-align: left;
        }
        #transactions .status {
            width: 140px;
            text-align: left;
        }
        #transactions .amount {
            width: 100px;
            text-align: left;
        }
        #transactions .id {
            width: 200px;
            text-align: left;
        }
        #transactions .message {
            width: 290px;
            text-align: left;
        }
        #transactions .ip {
            width: 100px;
            text-align: left;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function() {

            // PROFILE - VIEW PAYMENTS LINK
            $('#payment_profiles .profile .link_view-payments').click(function(e) {
                var id = $(this).attr('data-id');
                if ($('#transactions .head select option[value="' +id+ '"]').length > 0) {
                    $('#transactions .head select').val(id).change();
                } else {
                    e.preventDefault();
                    alert('No payments for this method found.');
                }
            });

            // TRANSACTIONS - FILTER BY PROFILE
            $('#transactions .head select').change(function() {
                var val = $(this).val();
                if (val) {
                    $('#transactions tr.profile').hide();
                    $('#transactions tr.profile-' +val).show();
                } else {
                    $('#transactions tr.profile').show();
                }
            });

        });

    </script>

    <div id="payment_profiles">

        <div class="clearfix">
            <h3>Payment Methods</h3>
            <a href="./edit/" class="link_add">+ Add Payment Method</a>
        </div>

        <?php

        // HAVE PAYMENT PROFILES
        if (count($allprofiles) > 0) {

            // LOOP
            foreach ($allprofiles as $prof) {

                ?>
                <div class="profile">
                    <h4><?php if ($prof['type']) { echo ucwords($prof['type']). ' - '; } echo $prof['lastfour']; ?><?php if ($prof['default']) { ?><span class="default">(default)</span><?php } ?></h4>
                    <div class="info row">
                        <div class="col-md-6">
                            <div class="item">
                                <h5>Cardholder Name</h5>
                                <?php echo stripslashes($prof['name']); ?>
                            </div>
                            <div class="item">
                                <h5>Card Number</h5>
                                ****<?php echo stripslashes($prof['lastfour']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="link_edit"><a href="./edit/?id=<?php echo $prof['id']; ?>">Edit</a></div>
                            <div class="item">
                                <h5>Expires</h5>
                                <?php echo date('m/Y', strtotime($prof['expires'])); ?>
                            </div>
                            <div class="item">
                                <h5>Last Updated</h5>
                                <?php echo date('n/j/Y g:i A', strtotime($prof['dt_updated'])); ?>
                            </div>
                        </div>
                    </div>
                    <div class="view-payments contain">
                        <a href="#payments" class="link_view-payments" data-id="<?php echo $prof['id']; ?>">Payments</a>
                    </div>
                </div>
                <?php

            }

        // NO PAYMENT PROFILES
        } else {
            ?>
            <div class="message-center">
                It doesn't look like you have any payment methods set up yet.
            </div>
            <?php
        }

        ?>

    </div>

    <?php

    // GET TRANSACTIONS
    $translog_query = "SELECT * FROM `" .$_uccms['_account']->config['db']['accounts_transaction_log']. "` WHERE (`account_id`='" .$_uccms['_account']->userID(). "') AND (`payment_profile_id`>0) ORDER BY `dt` DESC";
    $translog_q = sqlquery($translog_query);

    // HAVE TRANSACTIONS
    if (sqlrows($translog_q) > 0) {

        $transa = array();
        $tranplan = array();

        // LOOP
        while ($transaction = sqlfetch($translog_q)) {
            $transa[] = $transaction;
            $tranplan[$transaction['payment_profile_id']] = $transaction['payment_profile_id'];
        }

        ?>

        <a name="payments"></a>

        <div id="transactions" class="section">

            <div class="head contain">
                <h3>Payments</h3>
                <select name="payment_profile_id">
                    <option value="">All Methods</option>
                    <?php
                    foreach ($allprofiles as $prof) {
                        if (in_array($prof['id'], $tranplan)) {
                            ?>
                            <option value="<?php echo $prof['id']; ?>"><?php if ($prof['type']) { echo ucwords($prof['type']). ' - '; } echo stripslashes($prof['lastfour']); ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Time</th>
                        <th>Status</th>
                        <th>Amount</th>
                        <? /*<th>ID</th>*/ ?>
                        <th>Message</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    // LOOP
                    foreach ($transa as $transaction) {

                        ?>

                        <tr class="profile profile-<?php echo $transaction['payment_profile_id']; ?>">
                            <td class="title"><?php if ($allprofiles[$transaction['payment_profile_id']]['type']) { echo ucwords($allprofiles[$transaction['payment_profile_id']]['type']). ' - '; } echo stripslashes($allprofiles[$transaction['payment_profile_id']]['lastfour']); ?></td>
                            <td class="dt"><?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?></td>
                            <td class="status"><?php echo ucwords($transaction['action']); if ($transaction['success']) { echo ' - Success'; } else { echo ' - Failed'; } ?></td>
                            <td class="amount">$<?php echo number_format($transaction['amount'], 2); ?></td>
                            <? /*<td class="id"><?php echo stripslashes($transaction['transaction_id']); ?></td>*/ ?>
                            <td class="message"><?php echo stripslashes($transaction['response_message']); ?></td>
                        </tr>

                        <?php

                    }

                    ?>
                </tbody>
            </table>

        </div>

        <?php

    }

    ?>

<?php } else { ?>

    <div class="message-center">
        Payment profiles not enabled.
    </div>

<?php } ?>