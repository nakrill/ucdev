<?php

// PAYMENT PROFILES ENABLED
if ($_uccms['_account']->paymentProfilesEnabled()) {

    // ID SPECIFIED
    if ($_REQUEST['id']) {

        // GET PAYMENT PROFILE
        $prof = $_uccms['_account']->pp_getProfile((int)$_REQUEST['id'], $_uccms['_account']->userID());

        // PROFILE FOUND
        if ($prof['id']) {

            // DELETE PROFILE
            $result = $_uccms['_account']->pp_deleteProfile($prof['id']);

            // PROFILE CREATED
            if ($result['success']) {
                $_uccms['_site-message']->set('success', 'Payment method deleted.');

            // FAILED
            } else {
                if ($result['error']) {
                    $_uccms['_site-message']->set('error', $result['error']);
                } else {
                    $_uccms['_site-message']->set('error', 'Failed to delete payment method. Please try again.');
                }
                BigTree::redirect('../edit/?id=' .$prof['id']);
            }

        // PROFILE NOT FOUND
        } else {
            $_uccms['_site-message']->set('error', 'Payment method not found or not yours.');
        }

    // PROFILE ID NOT SPECIFIED
    } else {
        $_uccms['_site-message']->set('error', 'Payment method not specified.');
    }

// PAYMENT PROFILES NOT ENABLED
} else {
    $_uccms['_site-message']->set('error', 'Payment profiles not enabled.');
}

BigTree::redirect('../');

?>