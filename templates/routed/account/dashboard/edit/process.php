<?php

// MUST BE LOGGED IN
$_uccms['_account']->forceAuth();

// FORM SUBMITTED
if (is_array($_POST)) {

    // CLEAN UP
    $email = trim(sqlescape($_POST['email']));

    // EMAIL IS VALID
    if ($_uccms['_account']->validEmail($email)) {

        // SEE WHO'S USING EMAIL ADDRESS
        $query = "SELECT `id` FROM `" .$_uccms['_account']->config['db']['accounts']. "` WHERE (`username`='" .$email. "') OR (`email`='" .$email. "') LIMIT 1";
        $q = sqlquery($query);
        $account = sqlfetch($q);

        // MATCH FOUND AND IS NOT OWNED BY USER
        if (($account['id']) && ($account['id'] != $_uccms['_account']->userID())) {
            $_uccms['_site-message']->set('error', 'Email address already used by another account.');
            BigTree::redirect('../');
        }

        // ACCOUNT FIELDS
        $account_fields = array(
            'username'  => $email,
            'email'     => $email
        );

        // UPDATE ACCOUNT
        if ($_uccms['_account']->updateAccount($_uccms['_account']->userID(), $account_fields)) {

            // UPDATE ACCOUNT DETAILS
            $account_details = array(
                'firstname' => $_POST['details']['firstname'],
                'lastname'  => $_POST['details']['lastname']
            );

            // UPDATE DETAILS
            $query = "UPDATE `" .$_uccms['_account']->config['db']['accounts_details']. "` SET " .uccms_createSet($account_details). " WHERE (`id`=" .$_uccms['_account']->userID(). ")";
            sqlquery($query);

            $_uccms['_site-message']->set('success', 'Account updated.');

        // ACCOUNT NOT UPDATED
        } else {
            $_uccms['_site-message']->set('error', 'Failed to update account.');
        }

        // PASSWORD SPECIFIED
        if ($_POST['password']) {

            // CHANGE PASSWORD
            $change = $_uccms['_account']->changePassword($_POST['password']);

            // PASSWORD CHANGED
            if ($change['success'] == true) {
                $_uccms['_site-message']->set('success', 'Password changed.');

            // PASSWORD CHANGE FAILED
            } else {
                if ($change['error']) {
                    $_uccms['_site-message']->set('error', $change['error']);
                } else {
                    $_uccms['_site-message']->set('error', 'Failed to change password.');
                }
            }

        }

    // INVALID EMAIL
    } else {
        $_uccms['_site-message']->set('error', 'Invalid email address.');
    }

}

BigTree::redirect('../');

?>