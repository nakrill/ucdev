<?php

// GET ACCOUNT INFO
$account = $_uccms['_account']->getAccount('', true);

?>

<form action="./process/" method="post">

<div class="row">
    <div class="col-md-4">

        <fieldset class="form-group">
            <label>First Name:</label>
            <input type="text" name="details[firstname]" value="<?php echo stripslashes($account['firstname']); ?>" class="form-control" />
        </fieldset>

        <fieldset class="form-group">
            <label>Last Name:</label>
            <input type="text" name="details[lastname]" value="<?php echo stripslashes($account['lastname']); ?>" class="form-control" />
        </fieldset>

        <fieldset class="form-group">
            <label>Email:</label>
            <input type="email" name="email" value="<?php echo stripslashes($account['email']); ?>" class="form-control" />
        </fieldset>

        <fieldset class="form-group">
            <label>Password:</label>
            <input type="password" name="password" class="form-control" />
            <small>Only enter if changing.</small>
        </fieldset>

        <fieldset>
            <input type="submit" value="Save" class="btn btn-primary" />
        </fieldset>

    </div>
</div>

</form>