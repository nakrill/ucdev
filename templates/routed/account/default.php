<?php

include(dirname(__FILE__). '/__header.php');

// HAVE ROUTE
if ($include_route) {

    // INCLUDE FILE
    include($include_route);

// NO INCLUDE ROUTE
} else {
    ?>
    Page not found.
    <?php
}

include(dirname(__FILE__). '/__footer.php');

?>