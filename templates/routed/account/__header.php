<?php

// ACCOUNTS ARE NOT ENABLED
if (!$_uccms['_account']->isEnabled()) {

    // LOG USER OUT
    $_uccms['_account']->logout();

    // MESSAGE
    $_uccms['_site-message']->set('error', 'Accounts are not enabled.');

    // SEND TO HOME PAGE
    BigTree::redirect('/');

    exit;

}

// NOT LOGGED IN
if (!$_uccms['_account']->loggedIn()) {
    $_uccms['_account']->setRedirect($_SERVER['REQUEST_URI']);
    BigTree::redirect('/' .$bigtree['path'][0]. '/login/');
}

$have_current_page = false;

// GET ACCOUNT INFO
$account = $_uccms['_account']->getAccount('', true);

// NO FIRSTNAME
if (!$account['firstname']) {
    $account['firstname'] = 'Me';
}

// NAVIGATION ARRAY
$nava[0] = array(
    'path'  => '/' .$bigtree['path'][0]. '/',
    'title' => 'Dashboard'
);

// GET ACCOUNT PAGE ID
$acctpage_query = "SELECT `id` FROM `bigtree_pages` WHERE (`route`='account') LIMIT 1";
$acctpage_q = sqlquery($acctpage_query);
$acctpage = sqlfetch($acctpage_q);

// ACCOUNT PAGE FOUND
if ($acctpage['id']) {

    // GET SUB PAGES
    $subpages = $cms->getNavByParent($acctpage['id']);

    // HAVE SUB PAGES
    if (count($subpages) > 0) {

        $i = count($nava);

        // LOOP THROUGH SUB PAGES
        foreach ($subpages as $subpage) {

            // ADD TO NAVIGATION
            $nava[$i] = array(
                'path'  => $subpage['link'],
                'title' => $subpage['title']
            );

            // IS CURRENT ROUTE
            if ($subpage['route'] == $bigtree['path'][1]) {
                $nava[$i]['current'] = true;
                $have_current_page = true;
            }

            // GET SUB PAGES
            $subpages2 = $cms->getNavByParent($subpage['id']);

            // HAVE SUB PAGES
            if (count($subpages2) > 0) {

                $j = 0;

                // LOOP THROUGH SUB PAGES
                foreach ($subpages2 as $subpage2) {

                    // ADD TO NAVIGATION
                    $nava[$i]['sub'][$j] = array(
                        'path'  => $subpage2['link'],
                        'title' => $subpage2['title']
                    );

                    // IS CURRENT ROUTE
                    if ($subpage2['route'] == $bigtree['path'][2]) {
                        $nava[$i]['sub'][$j]['current'] = true;
                    }

                    $j++;

                }

            }

            $i++;

        }

    }

}

// INCLUDE ROUTED FILE
$include_route = '';

// HAVE COMMANDS
if (count($bigtree['commands']) > 0) {

    $btcommands = $bigtree['commands'];
    $base = array_shift($btcommands);

    // GET BIGTREE ROUTING
    list($include, $commands) = BigTree::route(SERVER_ROOT. 'templates/routed/account/dashboard/' .$base. '/', $btcommands);

    // HAVE LOCAL FILE TO INCLUDE
    if ($include) {

        // SET NEW COMMANDS
        $bigtree['commands'] = $commands;

        // SET INCLUDE FILE
        $include_route = $include;

    }

    unset($btcommands);

}

// EXTENSIONS DIRECTORY
$extensions_dir = SERVER_ROOT. 'extensions';

// GET EXTENSIONS
$extensions = array_diff(scandir($extensions_dir), array('..', '.'));

// HAVE EXTENSIONS
if (count($extensions) > 0) {
    foreach ($extensions as $extension) {
        $extension_dir = $extensions_dir. '/' .$extension. '/templates/routed/account';
        if (is_dir($extension_dir)) {
            @include($extension_dir. '/settings.php');
        }
    }
}

// HAVE ROUTING ARRAY
if (count((array)$routing) > 0) {

    // SORT ROUTING ARRAY
    usort($routing, function($a, $b) {
        return $a['sort'] - $b['sort'];
    });

    unset($cur_route);
    $i = count($nava);

    // LOOP THROUGH ROUTING
    foreach ($routing as $route) {

        // ADD TO NAVIGATION
        if ($route['title']) {
            $nava[$i] = array(
                'path'  => '/' .$bigtree['path'][0]. '/' .$route['route']. '/',
                'title' => $route['title']
            );
        }

        // IS CURRENT ROUTE
        if ($route['route'] == $bigtree['commands'][0]) {
            $cur_route = $route;
            $nava[$i]['current'] = true;
            $have_current_page = true;
        }

        $i++;

    }

    // IS IN SUBDIRECTORY & HAVE ROUTING
    if ((count($bigtree['commands']) > 0) && ($cur_route['extension'])) {

        // GET BIGTREE ROUTING
        list($include, $commands) = BigTree::route(SERVER_ROOT. 'extensions/' .$cur_route['extension']. '/templates/routed/account/' .$bigtree['commands'][0]. '/', $bigtree['commands']);

        // HAVE FILE TO INCLUDE
        if ($include) {

            // SET NEW COMMANDS
            $bigtree['commands'] = $commands;

            // SET INCLUDE FILE
            $include_route = $include;

        }

    }

}

// NO INCLUDE FILE SPECIFIED
if (!$include_route) {

    // USE DASHBOARD
    $include_route = dirname(__FILE__). '/dashboard/default.php';

}

// NO CURRENT PAGE
if (!$have_current_page) {
    $nava[0]['current'] = true; // SET ACCOUNT DASHBOARD TO CURRENT
}

// IS NOT VERIFIED YET
if (($account['verify']) && (strtotime($account['created']) < strtotime('-2 Weeks'))) {
    $_uccms['_site-message']->set('warning', 'Your email address still needs verified. <a href="./verify/resend/">Click here</a> to resend the verification email.');
}

?>

<style type="text/css">

    .myMenu {
        margin: 0 -1em 0 0;
        padding: 0;
    }
    .myMenu li {
        display: inline-block;
        list-style: none;
    }
    .myMenu li a:link {
        display: block;
        margin: 0;
        padding: 0.5em 1em;
        border-left: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
        text-decoration: none;
    }
    .myMenu li a:first-child {
        border-left: 0px;
    }
    .myMenu li ul {
        position: absolute;
        visibility: hidden;
        margin: 0;
        padding: 0;
        border-top: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
    }
    .myMenu li ul li {
        display: inline;
        float: none;
    }
    .myMenu li ul li a:link {
        background-color: <?php echo $bigtree['config']['css']['vars']['sitePrimary']; ?>;
        width: auto;
    }
    .myMenu li ul li a:hover {
        background-color: <?php echo $bigtree['config']['css']['vars']['sitePrimary']; ?>;
    }

    .account_nav {
        margin-bottom: 20px;
        padding: 0px;
        background-color: <?php echo $bigtree['config']['css']['vars']['siteThird']; ?>;
        border-top: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
        border-bottom: 1px solid <?php echo $bigtree['config']['css']['vars']['sitePrimary']; ?>;
    }
    .account_nav ul {
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    .account_nav li {
        display: block;
        float: left;
        border-right: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
    }
    .account_nav li:first-child {
        border-left: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
    }
    .account_nav li a {
        display: inline-block;
        padding: 10px 20px;
        background-color: <?php echo $bigtree['config']['css']['vars']['sitePrimary']; ?>;
    }
    .account_nav li a:hover {
        background-color: <?php echo $bigtree['config']['css']['vars']['siteThird']; ?>;
    }
    .account_nav li.current a {
        margin-bottom: -1px;
        border-bottom: 1px solid <?php echo $bigtree['config']['css']['vars']['siteBG']; ?>;
        background-color: <?php echo $bigtree['config']['css']['vars']['siteBG']; ?>;
        color: <?php echo $bigtree['config']['css']['vars']['link_hover']; ?>;
    }

    .account_nav li ul {
        position: absolute;
        visibility: hidden;
        margin: 1px 0 0 -1px;
        padding: 0 0 1px;
        border: 1px solid <?php echo $bigtree['config']['css']['vars']['siteSecondary']; ?>;
        border-top: 0px none;
    }
    .account_nav li ul li {
        display: block;
        float: none;
        border: 0px none;
    }
    .account_nav li ul li:first-child {
        border: 0px none;
    }
    .account_nav li ul li a:link, .account_nav li ul li a:visited {
        width: 100%;
        background-color: #fff;
        color: <?php echo $bigtree['config']['css']['vars']['link_color']; ?>;
    }
    .account_nav li ul li a:hover, .account_nav li ul li.current a {
        color: <?php echo $bigtree['config']['css']['vars']['link_hover']; ?>;
    }

    #myAccount {
        margin-bottom: 45px;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.myMenu > li').hover(
            function() {
                $(this).find('ul').css('visibility', 'visible');
            },
            function() {
                $(this).find('ul').css('visibility', 'hidden');
            }
        );
        $('.account_nav ul > li').hover(
            function() {
                $(this).find('ul').css('visibility', 'visible');
            },
            function() {
                $(this).find('ul').css('visibility', 'hidden');
            }
        );
    });
</script>

<section id="myAccount">
    <div class="container">

        <div class="contain clearfix noprint">
            <h2 style="float: left; margin-bottom: 10px;">My Account</h2>
            <div style="float: right; margin-top: 20px;">
                <ul class="myMenu">
                    <li><a href="javascript:void(0);"><?php echo trim(stripslashes($account['firstname'])); ?></a>
                        <ul>
                            <li><a href="/<?php echo $bigtree['path'][0]; ?>/edit/">Edit Account</a></li>
                            <?php if ($_uccms['_account']->paymentProfilesEnabled()) { ?>
                                <li><a href="/<?php echo $bigtree['path'][0]; ?>/edit/payment-methods/">Payment Methods</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li><a href="/<?php echo $bigtree['path'][0]; ?>/logout/">Log Out</a></li>
                </ul>
            </div>
        </div>

        <?php if (count($nava) > 0) { ?>
            <div class="account_nav noprint">
                <ul>
                    <?php foreach ($nava as $nav) { ?>
                        <li class="<?php if ($nav['current']) { echo 'current'; } ?>">
                            <a href="<?php echo $nav['path']; ?>"><?php echo $nav['title']; ?></a>
                            <?php if (count((array)$nav['sub']) > 0) { ?>
                                <ul>
                                    <?php foreach ($nav['sub'] as $sub) { ?>
                                        <li class="<?php if ($sub['current']) { echo 'current'; } ?>"><a href="<?php echo $sub['path']; ?>"><?php echo $sub['title']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
                <div style="clear: left; overflow: hidden; height: 0px;"></div>
            </div>
        <?php } ?>

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>