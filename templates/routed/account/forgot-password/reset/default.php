<?php

$token = trim($_REQUEST['t']);

// HAVE TOKEN
if ($token) {

    // CHECK TOKEN
    $id = $_uccms['_account']->checkToken('reset_token', $token);

    // NO USER ID
    if (!$id) {
        $_uccms['_site-message']->set('error', 'Invalid reset password URL.');
        BigTree::redirect('../');
    }

    // PASSWORD(S) SUBMITTED
    if ($_POST['password']) {

        // PASSWORDS MATCH
        if ($_POST['password'] == $_POST['password_re']) {

            // CHANGE PASSWORD
            $change = $_uccms['_account']->changePassword($_POST['password'], '', $token);

            // CHANGE SUCCESSFUL
            if ($change['success'] == true) {

                $_uccms['_site-message']->set('success', 'Password changed. You may now <a href="../../login/">log in</a> with it.');

            // CHANGE FAILED
            } else {
                if ($change['message']) {
                    $_uccms['_site-message']->set('error', $change['message']);
                } else {
                    $_uccms['_site-message']->set('error', 'Failed to change password.');
                }
            }

        // PASSWORDS DO NOT MATCH
        } else {
            $_uccms['_site-message']->set('error', 'Passwords do not match.');
        }

    }

// NO TOKEN
} else {
    $_uccms['_site-message']->set('error', 'Reset password link invalid.');
    BigTree::redirect('../');
}

?>

<style type="text/css">

    #password-reset h1 {
        margin-bottom: 30px;
        text-align: center;
    }
    #password-reset .login {
        display: block;
        margin-top: 10px;
        text-align: center;
    }

</style>

<div id="password-reset" class="modal-dialog">
    <div class="modal-body">

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>

        <h1>Reset Password</h1>

        <form id="resetForm" action="" method="post" novalidate="novalidate">
            <input type="hidden" name="t" value="<?php echo $token; ?>" />
            <div class="form-group">
                <input type="password" class="form-control" name="password" value="" required="" title="Please enter your password" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password_re" value="" required="" title="Please repeat your password" placeholder="Repeat password">
            </div>
            <button type="submit" class="btn btn-success btn-block">Change Password</button>
        </form>

    </div>
</div>