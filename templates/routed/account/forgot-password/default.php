<?php

// PAGE META
$bigtree['page']['title'] = 'Forgot Password';

// EMAIL SUBMITTED
if (count((array)$_POST) > 0) {

    // CLEAN UP
    $email = trim($_POST['email']);

    // TRACK
    $_SESSION['uccmsAcct_webTracking']['events']['forgot-password'] = $email;

    // PROCESS
    $forgot = $_uccms['_account']->forgotPassword($email);

    // SUCCESS
    if ($forgot['success']) {
        $_uccms['_site-message']->set('success', 'Password reset email sent. Please use the link in it to reset your password.');
    } else {
        if ($forgot['message']) {
            $_uccms['_site-message']->set('error', $forgot['message']);
        } else {
            $_uccms['_site-message']->set('error', 'Failed to reset password.');
        }
    }

}

?>

<style type="text/css">

    #forgot-password h1 {
        margin-bottom: 30px;
        text-align: center;
    }
    #forgot-password .login {
        display: block;
        margin-top: 10px;
        text-align: center;
    }

</style>

<div id="forgot-password" class="modal-dialog">
    <div class="modal-body">

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>

        <h1>Forgot Password</h1>

        <form id="forgotForm" action="" method="post" novalidate="novalidate">
            <div class="form-group">
                <input type="text" class="form-control" name="email" value="" required="" title="Please enter your email address" placeholder="example@gmail.com">
            </div>
            <button type="submit" class="btn btn-success btn-block">Reset Password</button>
            <a href="../login/" class="login small">Back to login</a>
        </form>

    </div>
</div>