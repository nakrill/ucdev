<?php

// RETURN URL SPECIFIED
if ($_REQUEST['return']) {
    $_uccms['_account']->setRedirect($_REQUEST['return']);
}

// CLEAN UP
$username = trim($_REQUEST['email']);
$password = trim($_REQUEST['password']);

// STILL HAVE USERNAME AND PASSWORD
if (($username) || ($password)) {

    // LOGIN
    $login = $_uccms['_account']->login($username, $password, $_REQUEST['remember']);

    // LOGIN SUCCESSFUL
    if ($login['success'] == true) {

        $_uccms['_site-message']->set('success', 'You have been logged in. <a href="../">My account.</a>');

    // LOGIN FAILED
    } else {

        // HAVE ERROR
        if ($login['error']) {

            $_uccms['_site-message']->set('error', $login['message']);

            // TRIES REMAINING SPECIFIED
            if ($login['tries_remaining']) {
                $_uccms['_site-message']->set('error', 'You have <strong>' .$login['tries_remaining']. ' tries remaining</strong>.');
            }

            // TEMPORARY SUSPENSION
            if ($login['error'] == 5) {
                $_uccms['_site-message']->set('error', 'Please wait <strong>' .$login['minutes']. ' minutes</strong> to try again.');
            }

        // GENERAL ERROR
        } else {
            $_uccms['_site-message']->set('error', 'Failed to log in.');
        }

    }

}

?>

<style type="text/css">

    #login h1 {
        margin-bottom: 30px;
        text-align: center;
    }
    #login .forgot {
        display: block;
        margin-top: 10px;
        text-align: center;
    }

</style>

<div class="page account-login">

    <div id="login" class="modal-dialog">
        <div class="modal-body">

            <?php

            // DISPLAY ANY SITE MESSAGES
            echo $_uccms['_site-message']->display();

            ?>

            <h1>Login</h1>
            <div class="row">
                <div class="col-xs-<?php echo ($_uccms['_account']->registrationsEnabled() ? 6 : 12); ?>">
                    <div class="well">
                        <form id="loginForm" action="" method="post" novalidate="novalidate">
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email" value="<?php echo $_REQUEST['email']; ?>" required="" title="Please enter your email address" placeholder="example@gmail.com">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password">
                                <span class="help-block"></span>
                            </div>
                            <div id="loginErrorMsg" class="alert alert-error hide">Wrong username or password</div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" id="remember" value="1" <?php if ($_REQUEST['remember']) { ?>checked="checked"<?php } ?>> Remember me
                                </label>
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Login</button>
                            <a href="../forgot-password/" class="forgot small">Forgot password?</a>
                        </form>
                    </div>
                </div>
                <?php if ($_uccms['_account']->registrationsEnabled()) { ?>
                    <div class="col-xs-6">
                        <p class="lead">Register now for <span class="text-success">FREE</span></p>
                        <ul class="list-unstyled" style="line-height: 2">
                            <li><span class="fa fa-check text-success"></span> See all your orders</li>
                            <li><span class="fa fa-check text-success"></span> Fast re-order</li>
                            <li><span class="fa fa-check text-success"></span> Save your favorites</li>
                            <li><span class="fa fa-check text-success"></span> Fast checkout</li>
                        </ul>
                        <p><a href="../register/" class="btn btn-info btn-block">Yes please, register now!</a></p>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>

</div>