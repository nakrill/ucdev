<?php

// REGISTRATIONS ARE ENABLED
if ($_uccms['_account']->registrationsEnabled()) {

    // PAGE META
    $bigtree['page']['title'] = 'Account Register';

    // LOGGED IN
    if ($_uccms['_account']->loggedIn()) {
        $_uccms['_site-message']->set('error', 'You are already logged in.');
        BigTree::redirect('../');

    // NOT LOGGED IN
    } else {

        // FORM SUBMITTED
        if (count((array)$_POST) > 0) {

            // ACCOUNT DETAILS
            $details = array(
                'firstname' => $_POST['firstname'],
                'lastname'  => $_POST['lastname']
            );

            // CREATE ACCOUNT
            $create = $_uccms['_account']->register($_POST['email'], $_POST['password'], $_POST['email'], $details);

            // SUCCESS
            if ($create['success'] == true) {

                // LOGIN
                $login = $_uccms['_account']->login($_POST['email'], $_POST['password'], false, false);

                // HAVE E-COMMERCE
                if (is_dir(SERVER_ROOT. 'extensions/com.umbrella.ecommerce/')) {

                    // ADD PRICEGROUP
                    $pg_query = "INSERT INTO `uccms_ecommerce_customers` SET `id`=" .$create['id']. ", `pricegroups`=1";
                    sqlquery($pg_query);

                }

                // NEEDS TO VERIFY EMAIL ADDRESS
                if ($create['verify'] == true) {
                    BigTree::redirect('./created/?verify=true');
                } else {
                    BigTree::redirect('./created/');
                }

            // FAILED
            } else {

                // HAVE ERROR
                if ($create['error']) {
                    $_uccms['_site-message']->set('error', $create['message']);
                } else {
                    $_uccms['_site-message']->set('error', 'Failed to create account.');
                }

            }

        }

    }

// REGISTRATIONS NOT ENABLED
} else {
    $_uccms['_site-message']->set('error', 'Registrations are not enabled.');
    BigTree::redirect('/');
}

?>

<style type="text/css">

    #register h1 {
        margin-bottom: 30px;
        text-align: center;
    }
    #register .login {
        display: block;
        margin-top: 10px;
        text-align: center;
    }

</style>

<div id="register" class="modal-dialog">
    <div class="modal-body">

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>

        <h1>Register</h1>
        <div class="row">
            <div class="col-xs-6">
                <div class="well">
                    <form id="registerForm" action="" method="post" novalidate="novalidate">
                        <div class="form-group">
                            <input type="text" class="form-control" name="firstname" value="<?php echo $_REQUEST['firstname']; ?>" required="" title="Please enter your first name" placeholder="First name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="lastname" value="<?php echo $_REQUEST['lastname']; ?>" required="" title="Please enter your last name" placeholder="Last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" value="<?php echo $_REQUEST['email']; ?>" required="" title="Please enter your email address" placeholder="example@gmail.com">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" value="" required="" title="Please enter your password" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-success btn-block">Create Account</button>
                        <a href="../login/" class="login small">Back to login</a>
                    </form>
                </div>
            </div>
            <div class="col-xs-6">
                <p class="lead">Register now for <span class="text-success">FREE</span></p>
                <ul class="list-unstyled" style="line-height: 2">
                    <li><span class="fa fa-check text-success"></span> See all your orders</li>
                    <li><span class="fa fa-check text-success"></span> Fast re-order</li>
                    <li><span class="fa fa-check text-success"></span> Save your favorites</li>
                    <li><span class="fa fa-check text-success"></span> Fast checkout</li>
                </ul>
            </div>
        </div>

    </div>
</div>