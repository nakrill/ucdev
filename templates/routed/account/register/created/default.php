<?php

// THEY'RE COMING FROM SOMEWHERE - SEND THEM BACK
if ($_SESSION[$_uccms['_account']->config['sessions']['names']['redirect']]) {
    $continue_url = $_SESSION[$_uccms['_account']->config['sessions']['names']['redirect']];
    unset($_SESSION[$_uccms['account']->config['sessions']['names']['redirect']]);
} else {
    $continue_url = '/account/';
}

?>

<style type="text/css">

    #account-registered {
        text-align: center;
    }

</style>

<div id="account-registered" class="modal-dialog">
    <div class="modal-body">

        <?php

        // DISPLAY ANY SITE MESSAGES
        echo $_uccms['_site-message']->display();

        ?>

        <h1>Account created!</h1>

        <p>Your account has been created!</p>

        <?php if ($_REQUEST['verify']) { ?>
            <p>Please check your email to verify your account.</p>
        <?php } ?>

        <a href="<?php echo $continue_url; ?>" class="btn btn-primary" style="text-align: center;">Continue</a>

    </div>
</div>