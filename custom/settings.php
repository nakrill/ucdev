<?
	$bigtree["config"]["css"]["prefix"] = false; // Flag for BigTree CSS3 parsing - automatic vendor prefixing for standard CSS3
	$bigtree["config"]["css"]["minify"] = false; // Flag for CSS minification

	// Array containing all CSS files to minify; key = name of compiled file
	// example: $bigtree["config"]["css"]["site"] compiles all CSS files into "site.css"
	$bigtree["config"]["css"]["files"]["site"] = array(
		//'master.css',
        'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css',
        //'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css',
        'custom.css'
	);
    $bigtree["config"]["css"]["files"]["master-min"] = array(
        'master.css'
    );

    /*
	$bigtree["config"]["css"]["files"]["site-ie8"] = array(
		"ie.css",
		"ie8.css"
	);

	$bigtree["config"]["css"]["files"]["site-ie9"] = array(
		"ie.css"
	);
    */

	// Array containing variables to be replaced in compiled CSS files
	// example: "variable_name" => "Variable Value" will replace all instances of $variable_name with 'Variable Value'
	$bigtree["config"]["css"]["vars"] = array(
        "iconSprite"    => 'url(../images/icons.png) no-repeat',
        'black'         => '#000',
		'darkGrey'      => '#777',
		'grey'          => '#999',
		'lightGrey'     => '#eee',
		'lighterGrey'   => '#ddd',
        'white'         => '#fff',
        'link_color'    => '#3f8ae3',
        'link_hover'    => '#333',
        'siteBG'        => '#fff',
        'sitePrimary'   => '#eee',
        'siteSecondary' => '#999',
        'siteThird'     => '#f9f9f9'
	);

	// Flag for JS minification
	$bigtree["config"]["js"]["minify"] = false;

	// Array containing all JS files to minify; key = name of compiled file
	// example: $bigtree["config"]["js"]["site"] compiles all JS files into "site.js"
	$bigtree["config"]["js"]["files"]["site"] = array(
        'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
		'master.js',
        'custom.js'
	);

    /*
	$bigtree["config"]["js"]["files"]["site-ie8"] = array(
		"ie/matchMedia.ie8.js",
		"ie/html5.js"
	);

	$bigtree["config"]["js"]["files"]["site-ie9"] = array(
		"ie/matchMedia.ie9.js",
		"ie/html5.js"
	);
    */

	// Array containing variables to be replaced in compiled JS files
	// example: "variable_name" => "Variable Value" will replace all instances of $variable_name with 'Variable Value'
	$bigtree["config"]["js"]["vars"] = array();

	// Admin Settings
	$bigtree["config"]["html_editor"] = array("name" => "TinyMCE 4","src" => "tinymce4/tinymce.js"); // WYSIWYG editor to use
	$bigtree["config"]["password_depth"] = 8; // The amount of work for the password hashing.  Higher is more secure but more costly on your CPU.

	$bigtree["config"]["admin_css"] = array(
        'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css',
        'https://use.fontawesome.com/releases/v5.1.1/css/all.css',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css',
        'uccms.css',
        'dashboard.css'
    ); // Additional CSS Files For the Admin to load, relative to /custom/admin/css/

	$bigtree["config"]["admin_js"] = array(
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/js/bootstrap.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js',
        'uccms.js',
        'dashboard.js',
    ); // Additional JavaScript Files For the Admin to load, relative to /custom/admin/js/

	$bigtree["config"]["ignore_admin_updates"] = true; // Set to true to disable pinging bigtreecms.org for version updates

	// Default Image Quality Presets
	$bigtree["config"]["image_quality"] = 90; // 0-100, size increases dramatically after 90
	$bigtree["config"]["retina_image_quality"] = 25; // 0-100, size increases dramatically after 90
	$bigtree["config"]["image_force_jpeg"] = false; // Set to true to make images uploaded as PNG save as JPG
	// Placeholder image defaults - add your own key to the "placeholder" array to create more placeholder image templates.
	$bigtree["config"]["placeholder"]["default"] = array(
		"background_color" => "CCCCCC",
		"text_color" => "666666",
		"image" => false,
		"text" => false
	);

	// Custom Output Filter Function
	$bigtree["config"]["output_filter"] = false;

	// Encryption key for encrypted settings
	$bigtree["config"]["settings_key"] = "5512f19dec8a93.88684406";

	// Base classes for BigTree.  If you want to extend / overwrite core features of the CMS, change these to your new class names
	// Set BIGTREE_CUSTOM_BASE_CLASS_PATH to the directory path (relative to /site/) of the file that will extend BigTreeCMS
	// Set BIGTREE_CUSTOM_ADMIN_CLASS_PATH to the directory path (relative to /site/) of the file that will extend BigTreeAdmin
	define("BIGTREE_CUSTOM_BASE_CLASS",false);
	define("BIGTREE_CUSTOM_ADMIN_CLASS",false);
	define("BIGTREE_CUSTOM_BASE_CLASS_PATH",false);
	define("BIGTREE_CUSTOM_ADMIN_CLASS_PATH",false);

    // Show all database tables in admin dropdowns
    $bigtree["config"]["show_all_tables_in_dropdowns"] = true;

    // UCCMS ADDED CODE
    include_once(dirname(__FILE__). '/../uccms/includes/include.php');

?>