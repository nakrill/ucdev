<?php

@include_once(__DIR__. '/custom.php');

function targetBlank($url) {
	return BigTree::isExternalLink($url) ? ' target="_blank"' : "";
}

?>