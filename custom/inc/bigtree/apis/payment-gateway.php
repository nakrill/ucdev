<?php
    /*
        Class: BigTreePaymentGateway
            Controls eCommerce payment systems.
            Wrapper overtop PayPal Payments Pro, Authorize.Net, PayPal Payflow Gateway, LinkPoint API, Stripe, InstaPay
    */

    class BigTreePaymentGateway {

        var $AVS = "";
        var $CountryCodes = array("ALAND ISLANDS" => "AX", "ALBANIA" => "AL", "ALGERIA" => "DZ", "AMERICAN SAMOA" => "AS", "ANDORRA" => "AD", "ANGUILLA" => "AI", "ANTARCTICA" => "AQ","ANTIGUA AND BARBUDA" => "AG", "ARGENTINA" => "AR", "ARMENIA" => "AM", "ARUBA" => "AW", "AUSTRALIA" => "AU", "AUSTRIA" => "AT", "AZERBAIJAN" => "AZ", "BAHAMAS" => "BS", "BAHRAIN" => "BH", "BANGLADESH" => "BD", "BARBADOS" => "BB", "BELGIUM" => "BE", "BELIZE" => "BZ", "BENIN" => "BJ", "BERMUDA" => "BM", "BHUTAN" => "BT", "BOSNIA-HERZEGOVINA" => "BA", "BOTSWANA" => "BW", "BOUVET ISLAND" => "BV", "BRAZIL" => "BR", "BRITISH INDIAN OCEAN TERRITORY" => "IO", "BRUNEI DARUSSALAM" => "BN", "BULGARIA" => "BG", "BURKINA FASO" => "BF", "CANADA" => "CA", "CAPE VERDE" => "CV", "CAYMAN ISLANDS" => "KY", "CENTRAL AFRICAN REPUBLIC" => "CF", "CHILE" => "CL", "CHINA" => "CN", "CHRISTMAS ISLAND" => "CX", "COCOS (KEELING) ISLANDS" => "CC", "COLOMBIA" => "CO", "COOK ISLANDS" => "CK", "COSTA RICA" => "CR", "CYPRUS" => "CY", "CZECH REPUBLIC" => "CZ", "DENMARK" => "DK", "DJIBOUTI" => "DJ", "DOMINICA" => "DM", "DOMINICAN REPUBLIC" => "DO", "ECUADOR" => "EC", "EGYPT" => "EG", "EL SALVADOR" => "SV", "ESTONIA" => "EE", "FALKLAND ISLANDS (MALVINAS)" => "FK", "FAROE ISLANDS" => "FO", "FIJI" => "FJ", "FINLAND" => "FI", "FRANCE" => "FR", "FRENCH GUIANA" => "GF", "FRENCH POLYNESIA" => "PF", "FRENCH SOUTHERN TERRITORIES" => "TF", "GABON" => "GA", "GAMBIA" => "GM", "GEORGIA" => "GE", "GERMANY" => "DE", "GHANA" => "GH", "GIBRALTAR" => "GI", "GREECE" => "GR", "GREENLAND" => "GL", "GRENADA" => "GD", "GUADELOUPE" => "GP", "GUAM" => "GU", "GUERNSEY" => "CG", "GUYANA" => "GY", "HEARD ISLAND AND MCDONALD ISLANDS" => "HM", "HOLY SEE (VATICAN CITY STATE)" => "VA", "HONDURAS" => "HN", "HONG KONG" => "HK", "HUNGARY" => "HU", "ICELAND" => "IS", "INDIA" => "IN", "INDONESIA" => "ID", "IRELAND" => "IE", "ISLE OF MAN" => "IM", "ISRAEL" => "IL", "ITALY" => "IT", "JAMAICA" => "JM", "JAPAN" => "JP", "JERSEY" => "JE", "JORDAN" => "JO", "KAZAKHSTAN" => "KZ", "KIRIBATI" => "KI", "KOREA, REPUBLIC OF" => "KR", "KUWAIT" => "KW", "KYRGYZSTAN" => "KG", "LATVIA" => "LV", "LESOTHO" => "LS", "LIECHTENSTEIN" => "LI", "LITHUANIA" => "LT", "LUXEMBOURG" => "LU", "MACAO" => "MO", "MACEDONIA" => "MK", "MADAGASCAR" => "MG", "MALAWI" => "MW", "MALAYSIA" => "MY", "MALTA" => "MT", "MARSHALL ISLANDS" => "MH", "MARTINIQUE" => "MQ", "MAURITANIA" => "MR", "MAURITIUS" => "MU", "MAYOTTE" => "YT", "MEXICO" => "MX", "MICRONESIA, FEDERATED STATES OF" => "FM", "MOLDOVA, REPUBLIC OF" => "MD", "MONACO" => "MC", "MONGOLIA" => "MN", "MONTENEGRO" => "ME", "MONTSERRAT" => "MS", "MOROCCO" => "MA", "MOZAMBIQUE" => "MZ", "NAMIBIA" => "NA", "NAURU" => "NR", "NEPAL" => "NP", "NETHERLANDS" => "NL", "NETHERLANDS ANTILLES" => "AN", "NEW CALEDONIA" => "NC", "NEW ZEALAND" => "NZ", "NICARAGUA" => "NI", "NIGER" => "NE", "NIUE" => "NU", "NORFOLK ISLAND" => "NF", "NORTHERN MARIANA ISLANDS" => "MP", "NORWAY" => "NO", "OMAN" => "OM", "PALAU" => "PW", "PALESTINE" => "PS", "PANAMA" => "PA", "PARAGUAY" => "PY", "PERU" => "PE", "PHILIPPINES" => "PH", "PITCAIRN" => "PN", "POLAND" => "PL", "PORTUGAL" => "PT", "PUERTO RICO" => "PR", "QATAR" => "QA", "REUNION" => "RE", "ROMANIA" => "RO", "RUSSIAN FEDERATION" => "RU", "RWANDA" => "RW", "SAINT HELENA" => "SH", "SAINT KITTS AND NEVIS" => "KN", "SAINT LUCIA" => "LC", "SAINT PIERRE AND MIQUELON" => "PM", "SAINT VINCENT AND THE GRENADINES" => "VC", "SAMOA" => "WS", "SAN MARINO" => "SM", "SAO TOME AND PRINCIPE" => "ST", "SAUDI ARABIA" => "SA", "SENEGAL" => "SN", "SERBIA" => "RS", "SEYCHELLES" => "SC", "SINGAPORE" => "SG", "SLOVAKIA" => "SK", "SLOVENIA" => "SI", "SOLOMON ISLANDS" => "SB", "SOUTH AFRICA" => "ZA", "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS" => "GS", "SPAIN" => "ES", "SURINAME" => "SR", "SVALBARD AND JAN MAYEN" => "SJ", "SWAZILAND" => "SZ", "SWEDEN" => "SE", "SWITZERLAND" => "CH", "TAIWAN, PROVINCE OF CHINA" => "TW", "TANZANIA, UNITED REPUBLIC OF" => "TZ", "THAILAND" => "TH", "TIMOR-LESTE" => "TL", "TOGO" => "TG", "TOKELAU" => "TK", "TONGA" => "TO", "TRINIDAD AND TOBAGO" => "TT", "TUNISIA" => "TN", "TURKEY" => "TR", "TURKMENISTAN" => "™", "TURKS AND CAICOS ISLANDS " => "TC", "TUVALU" => "TV","UGANDA" => "UG", "UKRAINE" => "UA", "UNITED ARAB EMIRATES" => "AE", "UNITED KINGDOM" => "GB", "UNITED STATES" => "US", "UNITED STATES MINOR OUTLYING ISLANDS" => "UM", "URUGUAY" => "UY", "UZBEKISTAN" => "UZ", "VANUATU" => "VU", "VENEZUELA" => "VE", "VIET NAM" => "VN", "VIRGIN ISLANDS, BRITISH" => "VG", "VIRGIN ISLANDS, U.S." => "VI", "WALLIS AND FUTUNA" => "WF", "WESTERN SAHARA" => "EH", "ZAMBIA" => "ZM");
        var $CVV = "";
        var $DefaultParameters = array();
        var $Environment = "";
        var $Errors = array();
        var $Headers = array();
        var $Last4CC = "";
        var $Message = "";
        var $PostURL = "";
        var $Service = "";
        var $Transaction = false;

        // Authorize.net Specific Properties
        var $APILogin = "";
        var $TransactionKey = "";
        var $Unresponsive = false;

        // LinkPoint Specific Properties
        var $Certificate = "";
        var $Store = "";

        // PayPal & Payflow Specific Properties
        var $Partner = "";
        var $Password = "";
        var $PayPalPeriods = array("day" => "Day", "week" => "Week", "month" => "Month", "year" => "Year");
        var $PayPalTransaction = "";
        var $Profile = "";
        var $Signature = "";
        var $Username = "";
        var $Vendor = "";

        // InstaPay Specific Properties
        var $IPAccountID = "";
        var $IPMerchantPin = "";

        // Basys Specific Properties
        var $BasysAccountID = "";
        var $BasysPassword = "";


        /*
            Constructor:
                Sets up the currently configured service.

            Parameters:
                gateway_override - Optionally specify the gateway you want to use (defaults to the admin default)
        */

        function __construct($gateway_override = false) {
            $s = BigTreeAdmin::getSetting("bigtree-internal-payment-gateway");

            // Setting doesn't exist? Create it.
            if ($s === false) {
                sqlquery("INSERT INTO bigtree_settings (`id`,`system`,`encrypted`) VALUES ('bigtree-internal-payment-gateway','on','on')");
                $s = array("service" => "", "settings" => array());
                BigTreeAdmin::updateSettingValue("bigtree-internal-payment-gateway",$s);
            }

            // If for some reason the setting doesn't exist, make one.
            $this->Service = isset($s["value"]["service"]) ? $s["value"]["service"] : "";
            $this->Settings = isset($s["value"]["settings"]) ? $s["value"]["settings"] : array();

            // If you specifically request a certain service, use it instead of the default
            if ($gateway_override) {
                $this->Service = $gateway_override;
            }

            if ($this->Service == "authorize.net") {
                $this->setupAuthorize();
            } elseif ($this->Service == "paypal") {
                $this->setupPayPal();
            } elseif ($this->Service == "paypal-rest") {
                $this->setupPayPalREST();
            } elseif ($this->Service == "payflow") {
                $this->setupPayflow();
            } elseif ($this->Service == "linkpoint") {
                $this->setupLinkPoint();
            } elseif ($this->Service == "stripe") {
                $this->setupStripe();
            } elseif ($this->Service == "instapay") {
                $this->setupInstaPay();
            } elseif ($this->Service == "basys") {
                $this->setupBasys();
            } elseif ($this->Service == "waived") {
                $this->setupWaived();
            } elseif ($this->Service == "donation") {
                $this->setupDonation();
            } elseif ($this->Service == "trade") {
                $this->setupTrade();
            }
        }

        /*
            Function: authorize
                Authorizes a credit card and returns the transaction ID for later capture.

            Parameters:
                amount - The amount to charge (includes the tax).
                tax - The amount of tax to charge (for accounting purposes, must also be included in total amount).
                card_name - Name as it appears on the credit card.
                card_number - Credit card number.
                card_expiration - 4 or 6 digit expiration date (MMYYYY or MMYY).
                cvv - Credit card security code.
                address - An address array with keys "street", "street2", "city", "state", "zip", "country"
                description - Description of what is being charged.
                email - Email address of the purchaser.
                phone - Phone number of the purchaser.
                customer - Customer ID of the purchaser.

            Returns:
                Transaction ID if successful, otherwise returns false.
                $this->Message will contain an error message if not successful.
        */

        function authorize($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description="",$email="",$phone="",$customer="",$profile='') {
            // Clean up the amount and tax.
            $amount = $this->formatCurrency($amount);
            $tax = $this->formatCurrency($tax);

            // Make card number only have numeric digits
            $card_number = preg_replace('/\D/', '', $card_number);

            if ($this->Service == "authorize.net") {
                return $this->authorizeAuthorize($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "paypal") {
                return $this->authorizePayPal($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "paypal-rest") {
                return $this->authorizePayPalREST($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "payflow") {
                return $this->authorizePayflow($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "linkpoint") {
                return $this->authorizeLinkPoint($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "stripe") {
                return $this->authorizeStripe($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile);
            } elseif ($this->Service == "instapay") {
                return $this->authorizeInstaPay($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "basys") {
                return $this->authorizeBasys($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile);
            } elseif ($this->Service == "waived") {
                return $this->authorizeWaived($amount,$tax,$description,$email,$phone,$customer);
            } elseif ($this->Service == "donation") {
                return $this->authorizeDonation($amount,$tax,$description,$email,$phone,$customer);
            } elseif ($this->Service == "trade") {
                return $this->authorizeTrade($amount,$tax,$description,$email,$phone,$customer);
            } else {
                throw new Exception("Invalid Payment Gateway");
            }
        }

        /*
            Function: authorizeAuthorize
                Authorize.net interface for <authorize>
        */

        protected function authorizeAuthorize($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer) {
            return $this->chargeAuthorize($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,"AUTH_ONLY");
        }

        /*
            Function: authorizeLinkPoint
                First Data / LinkPoint interface for <authorize>
        */

        protected function authorizeLinkPoint($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer) {
            return $this->chargeLinkPoint($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,"PREAUTH");
        }

        /*
            Function: authorizePayPal
                PayPal Payments Pro interface for <authorize>
        */

        protected function authorizePayPal($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer) {
            return $this->chargePayPal($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,"Authorization");
        }

        /*
            Function: authorizePayPalREST
                PayPal REST API interface for <authorize>
        */

        protected function authorizePayPalREST($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer) {
            return $this->chargePayPalREST($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,"authorize");
        }

        /*
            Function: authorizePayflow
                PayPal Payflow Gateway interface for <authorize>
        */

        protected function authorizePayflow($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer) {
            return $this->chargePayflow($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,"A");
        }

        /*
            Function: authorizeStripe
                Stripe interface for <authorize>
        */

        protected function authorizeStripe($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile) {
            return $this->chargeStripe($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile,"authorize");
        }

        /*
            Function: authorizeInstaPay
                InstaPay interface for <authorize>
        */

        protected function authorizeInstaPay($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer) {
            return $this->chargeInstapay($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,"authonly");
        }

        /*
            Function: authorizeBasys
                Basys interface for <authorize>
        */

        protected function authorizeBasys($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile) {
            return $this->chargeBasys($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile,"A");
        }

        /*
            Function: authorizeWaived
                Waived interface for <authorize>
        */

        protected function authorizeWaived($amount,$tax,$description,$email,$phone,$customer) {
            return $this->chargeWaived($amount,$tax,$description,$email,$phone,$customer);
        }

        /*
            Function: authorizeDonation
                Donation interface for <authorize>
        */

        protected function authorizeDonation($amount,$tax,$description,$email,$phone,$customer) {
            return $this->chargeDonation($amount,$tax,$description,$email,$phone,$customer);
        }

        /*
            Function: authorizeTrade
                Trade interface for <authorize>
        */

        protected function authorizeTrade($amount,$tax,$description,$email,$phone,$customer) {
            return $this->chargeTrade($amount,$tax,$description,$email,$phone,$customer);
        }

        /*
            Function: capture
                Captures a previously authorized transaction.

            Parameters:
                transaction - The transaction ID to capture funds for.
                amount - The amount to charge (must be equal to or lower than authorization amount).

            Returns:
                Transaction ID if successful, otherwise returns false.
                $this->Message will contain an error message if not successful.
        */

        function capture($transaction,$amount = 0) {
            // Clean up the amount.
            $amount = $this->formatCurrency($amount);

            if ($this->Service == "authorize.net") {
                return $this->captureAuthorize($transaction,$amount);
            } elseif ($this->Service == "paypal") {
                return $this->capturePayPal($transaction,$amount);
            } elseif ($this->Service == "paypal-rest") {
                return $this->capturePayPalREST($transaction,$amount);
            } elseif ($this->Service == "payflow") {
                return $this->capturePayflow($transaction,$amount);
            } elseif ($this->Service == "linkpoint") {
                return $this->captureLinkPoint($transaction,$amount);
            } elseif ($this->Service == "stripe") {
                return $this->captureStripe($transaction,$amount);
            } elseif ($this->Service == "instapay") {
                return $this->captureInstaPay($transaction,$amount);
            } elseif ($this->Service == "basys") {
                return $this->captureBasys($transaction,$amount);
            } elseif ($this->Service == "waived") {
                return $this->captureWaived($transaction,$amount);
            } elseif ($this->Service == "donation") {
                return $this->captureDonation($transaction,$amount);
            } elseif ($this->Service == "trade") {
                return $this->captureTrade($transaction,$amount);
            } else {
                throw new Exception("Invalid Payment Gateway");
            }
        }

        /*
            Function: captureAuthorize
                Authorize.Net interface for <capture>
        */

        protected function captureAuthorize($transaction,$amount) {
            $params = array();

            $params["x_type"] = "PRIOR_AUTH_CAPTURE";
            $params["x_trans_id"] = $transaction;
            if ($amount) {
                $params["x_amount"] = $amount;
            }

            $response = $this->sendAuthorize($params);

            // Setup response messages.
            $this->Transaction = $response["transaction"];
            $this->Message = $response["message"];

            if ($response["status"] == "approved") {
                return $response["transaction"];
            } else {
                return false;
            }
        }

        /*
            Function: captureLinkPoint
                First Data / LinkPoint interface for <capture>
        */

        protected function captureLinkPoint($transaction,$amount) {
            $params = array(
                "orderoptions" => array(
                    "ordertype" => "POSTAUTH"
                ),
                "transactiondetails" => array(
                    "ip" => $_SERVER["REMOTE_ADDR"],
                    "oid" => $transaction
                )
            );

            if ($amount) {
                $params["payment"]["chargetotal"] = $amount;
            }

            $response = $this->sendLinkPoint($params);

            // Setup response messages.
            $this->Transaction = strval($response->r_ordernum);
            $this->Message = strval($response->r_error);

            if (strval($response->r_message) == "ACCEPTED") {
                return $this->Transaction;
            } else {
                return false;
            }
        }

        /*
            Function: capturePayPal
                PayPal Payments Pro interface for <capture>
        */

        protected function capturePayPal($transaction,$amount) {
            $params = array();

            $params["METHOD"] = "DoCapture";
            $params["COMPLETETYPE"] = "Complete";
            $params["AUTHORIZATIONID"] = $transaction;
            $params["AMT"] = $amount;

            $response = $this->sendPayPal($params);

            // Setup response messages.
            $this->Transaction = $response["TRANSACTIONID"];
            $this->Message = urldecode($response["L_LONGMESSAGE0"]);

            if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                return $response["TRANSACTIONID"];
            } else {
                return false;
            }
        }

        /*
            Function: capturePayPalREST
                PayPal REST API interface for <capture>
        */

        protected function capturePayPalREST($transaction,$amount) {
            $data = json_encode(array(
                "amount" => array(
                    "currency" => "USD",
                    "total" => $amount
                )
            ));

            $response = $this->sendPayPalREST("payments/authorization/$transaction/capture",$data);
            if ($response->state == "completed") {
                return $response->id;
            } else {
                $this->Message = $response->message;
                return false;
            }
        }

        /*
            Function: capturePayflow
                PayPal Payflow Gateway interface for <capture>
        */

        protected function capturePayflow($transaction,$amount) {
            $params = array();

            $params["TRXTYPE"] = "D";
            $params["ORIGID"] = $transaction;
            if ($amount) {
                $params["AMT"] = $amount;
            }

            $response = $this->sendPayflow($params);

            // Setup response messages.
            $this->Transaction = $response["PNREF"];
            $this->Message = urldecode($response["RESPMSG"]);

            if ($response["RESULT"] == "0") {
                return $response["PNREF"];
            } else {
                return false;
            }
        }

        /*
            Function: captureStripe
                Stripe interface for <capture>
        */

        protected function captureStripe($transaction,$amount) {
            $params = array();

            $params["endpoint"] = "charges/" .$transaction. "/capture";

            // Note: Stripe seems to currently ignore "amount" value for now and captures full amount (7/2/2015)
            if (($amount) && ($amount != 0.00)) {
                $params["amount"] = $this->getMoneyAsCents($amount);
            }

            // Process Transaction
            $response = $this->sendStripe($params);

            if ($response["data"][0]["id"]) {
                $id = $response["data"][0]["id"];
            } else {
                $id = $response["id"];
            }

            // Setup response messages.
            $this->Transaction = $id;
            $this->Message = $response["error"]["message"];

            return $id;
        }

        /*
            Function: captureInstaPay
                InstaPay interface for <capture>
        */

        protected function captureInstaPay($transaction,$amount) {
            $params = array();

            // Get transaction parts
            $parts = $this->parseIPTransactionID($transaction);

            $params["action"] = "ns_quicksale_cc";
            $params["amount"] = $amount;
            $params["historykeyid"] = $parts["historyid"];

            // Process Transaction
            $response = $this->sendInstaPay($params);

            // Setup response messages.
            $this->Transaction = $response["transaction_full"];
            $this->Message = $response["message"];

            if ($response["status"] == "Accepted") {
                return $response["transaction_full"];
            } else {
                return false;
            }
        }

        /*
            Function: captureBasys
                Basys interface for <capture>
        */

        protected function captureBasys($transaction,$amount) {

            $params = array(
                'Transaction'   => array_merge($this->DefaultParameters, array(
                    'Submit_Time'       => date('c'),
                    'Tran_Type'         => 'M',
                    'TransactionID'     => $transaction,
                    'Tran_Amt'          => '0.00',
                    'Tran_Tax'          => '0.00',
                    'TaxExempt'         => 'false'
                ))
            );

            // Process Transaction
            $response = $this->sendBasys('Submit', array($params));

            // Setup response messages.
            $this->Transaction = $response->SubmitResult->Proc_ID;
            $this->Message = $response->SubmitResult->Proc_Mess;

            if ($response->SubmitResult->Proc_Resp == "Approved") {
                return $response->SubmitResult->Proc_ID;
            } else {
                return false;
            }


        }

        /*
            Function: captureWaived
                Waived interface for <capture>
        */

        protected function captureWaived($transaction,$amount) {
            return 1;
        }

        /*
            Function: captureDonation
                Donation interface for <capture>
        */

        protected function captureDonation($transaction,$amount) {
            return 1;
        }

        /*
            Function: captureTrade
                Trade interface for <capture>
        */

        protected function captureTrade($transaction,$amount) {
            return 1;
        }

        /*
            Function: cardType
                Returns the type of credit card based on the number.

            Parameters:
                card_number - The credit card number.

            Returns:
                The name of the card issuer.
        */

        function cardType($card_number) {
            $cards = array(
                "visa" => "(4\d{12}(?:\d{3})?)",
                "amex" => "(3[47]\d{13})",
                "jcb" => "(35[2-8][89]\d\d\d{10})",
                "maestro" => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)",
                "solo" => "((?:6334|6767)\d{12}(?:\d\d)?\d?)",
                "mastercard" => "(5[1-5]\d{14})",
                "switch" => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)",
                "discover" => '(^6(?:011|5[0-9]{2})[0-9]{12}$)'
            );
            $names = array("visa","amex","jcb","maestro","solo","mastercard","switch","discover");
            $matches = array();
            $pattern = "#^(?:".implode("|", $cards).")$#";
            $result = preg_match($pattern, str_replace(" ", "", $card_number), $matches);

            if ($result > 0) {
                return $names[count($matches) - 2];
            }
            return false;
        }

        /*
            Function: charge
                Charges a credit card (Authorization & Capture).

            Parameters:
                amount - The amount to charge (includes the tax).
                tax - The amount of tax to charge (for accounting purposes, must also be included in total amount).
                card_name - Name as it appears on the credit card.
                card_number - Credit card number.
                card_expiration - 4 or 6 digit expiration date (MMYYYY or MMYY).
                cvv - Credit card security code.
                address - An address array with keys "street", "street2", "city", "state", "zip", "country"
                description - Description of what is being charged.
                email - Email address of the purchaser.
                phone - Phone number of the purchaser.
                customer - Customer ID of the purchaser.

            Returns:
                Transaction ID if successful, otherwise returns false.
                $this->Message will contain an error message if not successful.
        */

        function charge($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description="",$email="",$phone="",$customer="",$profile='') {
            // Clean up the amount and tax.
            $amount = $this->formatCurrency($amount);
            $tax = $this->formatCurrency($tax);

            // Make card number only have numeric digits
            $card_number = preg_replace('/\D/', '', $card_number);

            if ($this->Service == "authorize.net") {
                return $this->chargeAuthorize($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "paypal") {
                return $this->chargePayPal($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "paypal-rest") {
                return $this->chargePayPalREST($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "payflow") {
                return $this->chargePayflow($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "linkpoint") {
                return $this->chargeLinkPoint($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "stripe") {
                return $this->chargeStripe($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile);
            } elseif ($this->Service == "instapay") {
                return $this->chargeInstaPay($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer);
            } elseif ($this->Service == "basys") {
                return $this->chargeBasys($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile);
            } elseif ($this->Service == "waived") {
                return $this->chargeWaived($amount,$tax,$description,$email,$phone,$customer);
            } elseif ($this->Service == "donation") {
                return $this->chargeDonation($amount,$tax,$description,$email,$phone,$customer);
            } elseif ($this->Service == "trade") {
                return $this->chargeTrade($amount,$tax,$description,$email,$phone,$customer);
            } else {
                throw new Exception("Invalid Payment Gateway");
            }
        }

        /*
            Function: chargeAuthorize
                Authorize.net interface for <charge>
        */

        protected function chargeAuthorize($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$action = "AUTH_CAPTURE") {
            $params = array();

            // Split the card name into first name and last name.
            $first_name = substr($card_name,0,strpos($card_name," "));
            $last_name = trim(substr($card_name,strlen($first_name)));

            $params["x_type"] = $action;

            $params["x_first_name"] = $first_name;
            $params["x_last_name"] = $last_name;
            $params["x_address"] = trim($address["street"]." ".$address["street2"]);
            $params["x_city"] = $address["city"];
            $params["x_state"] = $address["state"];
            $params["x_zip"] = $address["zip"];
            $params["x_country"] = $address["country"];

            $params["x_phone"] = $phone;
            $params["x_email"] = $email;
            $params["x_cust_id"] = $customer;
            $params["x_customer_ip"] = $_SERVER["REMOTE_ADDR"];

            $params["x_card_num"] = $card_number;
            $params["x_exp_date"] = $card_expiration;
            $params["x_card_code"] = $cvv;

            $params["x_amount"] = $amount;
            $params["x_tax"] = $tax;

            $params["x_description"] = $description;

            $response = $this->sendAuthorize($params);

            // Setup response messages.
            $this->Transaction = $response["transaction"];
            $this->Message = $response["message"];
            $this->Last4CC = $response["cc_last_4"];
            // Get a common AVS response.
            if ($response["avs"] == "A") {
                $this->AVS = "Address";
            } elseif ($response["avs"] == "W" || $response["avs"] == "Z") {
                $this->AVS = "Zip";
            } elseif ($response["avs"] == "X" || $response["avs"] == "Y") {
                $this->AVS = "Both";
            } else {
                $this->AVS = false;
            }
            // Get a common CVV response, either it passed or it didn't.
            if ($response["cvv"] == "2" || $response["cvv"] == "8" || $response["cvv"] == "A" || $response["cvv"] == "B") {
                $this->CVV = true;
            } else {
                $this->CVV = false;
            }

            if ($response["status"] == "approved") {
                return $response["transaction"];
            } else {
                return false;
            }
        }

        /*
            Function: chargeLinkPoint
                First Data / LinkPoint interface for <charge>
        */

        protected function chargeLinkPoint($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$action = "SALE") {
            $card_month = substr($card_expiration,0,2);
            $card_year = substr($card_expiration,-2,2);

            $params = array(
                "orderoptions" => array(
                    "ordertype" => $action
                ),
                "creditcard" => array(
                    "cardnumber" => $card_number,
                    "cardexpmonth" => $card_month,
                    "cardexpyear" => $card_year,
                    "cvmvalue" => $cvv,
                    "cvmindicator" => "provided"
                ),
                "transactiondetails" => array(
                    "ip" => $_SERVER["REMOTE_ADDR"]
                ),
                "billing" => array(
                    "name" => $card_name,
                    "address1" => $address["street"],
                    "address2" => $address["street2"],
                    "city" => $address["city"],
                    "state" => $address["state"],
                    "zip" => $address["zip"],
                    "phone" => $phone,
                    "email" => $email,
                    "userid" => $customer
                ),
                "payment" => array(
                    "tax" => $tax,
                    "chargetotal" => $amount
                ),
                "notes" => array(
                    "comments" => $description
                )
            );

            $response = $this->sendLinkPoint($params);

            // Setup response messages.
            $this->Transaction = strval($response->r_ordernum);
            $this->Message = strval($response->r_error);
            $this->Last4CC = substr(trim($card_number),-4,4);

            // Get a common AVS response.
            $a = substr(strval($response->r_avs),0,2);
            if ($a == "YN") {
                $this->AVS = "Address";
            } elseif ($a == "NY") {
                $this->AVS = "Zip";
            } elseif ($a == "YY") {
                $this->AVS = "Both";
            } else {
                $this->AVS = false;
            }

            // CVV match.
            if (substr(strval($response->r_avs),-1,1) == "M") {
                $this->CVV = true;
            } else {
                $this->CVV = false;
            }

            if (strval($response->r_message) == "APPROVED") {
                return $this->Transaction;
            } else {
                return false;
            }
        }

        /*
            Function: chargePayPal
                PayPal Payments Pro interface for <charge>
        */

        protected function chargePayPal($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$action = "Sale") {
            $params = array();

            // Split the card name into first name and last name.
            $first_name = substr($card_name,0,strpos($card_name," "));
            $last_name = trim(substr($card_name,strlen($first_name)));

            $params["METHOD"] = "DoDirectPayment";
            $params["PAYMENTACTION"] = $action;

            $params["AMT"] = $amount;
            $params["CREDITCARDTYPE"] = $this->cardType($card_number);
            $params["ACCT"] = $card_number;
            $params["EXPDATE"] = $card_expiration;
            $params["CVV2"] = $cvv;

            $params["IPADDRESS"] = $_SERVER["REMOTE_ADDR"];

            $params["FIRSTNAME"] = $first_name;
            $params["LASTNAME"] = $last_name;
            $params["STREET"] = trim($address["street"]." ".$address["street2"]);
            $params["CITY"] = $address["city"];
            $params["STATE"] = $address["state"];
            $params["ZIP"] = $address["zip"];
            $params["COUNTRYCODE"] = $address["country"];

            $params["EMAIL"] = $email;
            $params["PHONE"] = $phone;
            $params["NOTE"] = $description;

            $response = $this->sendPayPal($params);

            // Setup response messages.
            $this->Transaction = $response["TRANSACTIONID"];
            $this->Message = urldecode($response["L_LONGMESSAGE0"]);
            $this->Last4CC = substr(trim($card_number),-4,4);

            // Get a common AVS response.
            $a = $response["AVSCODE"];
            if ($a == "A" || $a == "B") {
                $this->AVS = "Address";
            } elseif ($a == "W" || $a == "Z" || $a == "P") {
                $this->AVS = "Zip";
            } elseif ($a == "D" || $a == "F" || $a == "M" || $a == "Y" || $a == "X") {
                $this->AVS = "Both";
            } else {
                $this->AVS = false;
            }

            // Get a common CVV response, either it passed or it didn't.
            if ($response["CVV2MATCH"] == "M") {
                $this->CVV = true;
            } else {
                $this->CVV = false;
            }

            if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                return $response["TRANSACTIONID"];
            } else {
                return false;
            }
        }

        /*
            Function: chargePayPalREST
                PayPal REST API interface for <charge>
        */

        protected function chargePayPalREST($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$action = "sale") {
            // Split the card name into first name and last name.
            $first_name = substr($card_name,0,strpos($card_name," "));
            $last_name = trim(substr($card_name,strlen($first_name)));

            // Separate card expiration out
            $card_expiration_month = substr($card_expiration,0,2);
            $card_expiration_year = substr($card_expiration,2);
            if (strlen($card_expiration) == 4) {
                $card_expiration_year = "20".$card_expiration_year;
            }

            // See if we can get a country code
            $country = isset($this->CountryCodes[strtoupper($address["country"])]) ? $this->CountryCodes[strtoupper($address["country"])] : $address["country"];

            // Split out tax and subtotals if present
            if ($tax) {
                $transaction_details = array(
                    "subtotal" => $this->formatCurrency(floatval($amount) - floatval($tax)),
                    "tax" => $tax,
                    "shipping" => 0
                );
            } else {
                $transaction_details = false;
            }

            // Setup credit card payment array
            $cc_array = array(
                "number" => $card_number,
                "type" => $this->cardType($card_number),
                "expire_month" => $card_expiration_month,
                "expire_year" => $card_expiration_year,
                "cvv2" => $cvv,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "billing_address" => array(
                    "line1" => $address["street"],
                    "line2" => $address["street2"],
                    "city" => $address["city"],
                    "state" => $address["state"],
                    "postal_code" => $address["zip"],
                    "country_code" => $country,
                    "phone" => $phone
                )
            );

            // Remove blank values, REST API doesn't like them
            $cc_array["billing_address"] = array_filter($cc_array["billing_address"]);
            $cc_array = array_filter($cc_array);

            // Full transaction array
            $data = array(
                "intent" => $action,
                "payer" => array(
                    "payment_method" => "credit_card",
                    "funding_instruments" => array(array("credit_card" => $cc_array))
                ),
                "transactions" => array(array(
                    "amount" => array(
                        "total" => $amount,
                        "currency" => "USD"
                    )
                ))
            );

            if ($transaction_details) {
                $data["transactions"][0]["amount"]["details"] = $transaction_details;
            }
            if ($email) {
                $data["payer"]["payer_info"]["email"] = $email;
            }
            if ($description) {
                $data["transactions"][0]["description"] = $description;
            }

            $response = $this->sendPayPalREST("payments/payment",json_encode($data));

            if ($response->state == "approved") {
                $this->Last4CC = substr(trim($card_number),-4,4);

                if ($action == "authorize") {
                    $this->Transaction = $response->transactions[0]->related_resources[0]->authorization->id;
                } else {
                    $this->Transaction = $response->transactions[0]->related_resources[0]->sale->id;
                }

                return $this->Transaction;
            } else {
                $this->Message = $response->message;
                $this->Errors = $response->details;

                return false;
            }
        }

        /*
            Function: chargePayflow
                PayPal Payflow Gateway interface for <charge>
        */

        protected function chargePayflow($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$action = "S") {
            $params = array();

            // Split the card name into first name and last name.
            $first_name = substr($card_name,0,strpos($card_name," "));
            $last_name = trim(substr($card_name,strlen($first_name)));

            $params["TRXTYPE"] = $action;
            $params["TENDER"] = "C"; // Credit card

            $params["AMT"] = $amount;
            $params["CREDITCARDTYPE"] = $this->cardType($card_number);
            $params["ACCT"] = $card_number;
            $params["EXPDATE"] = substr($card_expiration,0,4);
            $params["CVV2"] = $cvv;

            $params["IPADDRESS"] = $_SERVER["REMOTE_ADDR"];

            $params["FIRSTNAME"] = $first_name;
            $params["LASTNAME"] = $last_name;
            $params["STREET"] = trim($address["street"]." ".$address["street2"]);
            $params["CITY"] = $address["city"];
            $params["STATE"] = $address["state"];
            $params["ZIP"] = $address["zip"];
            $params["BILLTOCOUNTRY"] = $address["country"];

            $params["EMAIL"] = $email;
            $params["PHONE"] = $phone;
            $params["COMMENT1"] = $description;

            $response = $this->sendPayflow($params);

            // Setup response messages.
            $this->Transaction = $response["PNREF"];
            $this->Message = urldecode($response["RESPMSG"]);
            $this->Last4CC = substr(trim($card_number),-4,4);

            // Get a common AVS response.
            if ($response["AVSADDR"] == "Y" && $response["AVSZIP"] == "Y") {
                $this->AVS = "Both";
            } elseif ($response["AVSADDR"] == "Y") {
                $this->AVS = "Address";
            } elseif ($response["AVSZIP"] == "Y") {
                $this->AVS = "Zip";
            } else {
                $this->AVS = false;
            }

            // Get a common CVV response, either it passed or it didn't.
            if ($response["CVV2MATCH"] == "Y") {
                $this->CVV = true;
            } else {
                $this->CVV = false;
            }

            if ($response["RESULT"] == "0") {
                return $response["PNREF"];
            } else {
                return false;
            }
        }

        /*
            Function: chargeStripe
                Stripe interface for <charge>
        */

        protected function chargeStripe($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile,$action='') {
            $params = array();

            if ($action == "authorize") {
                $params["capture"] = "false";
            }

            $params["endpoint"] = "charges";

            // Stripe requires cents value
            $params["amount"]   = $this->getMoneyAsCents($amount);
            $params["currency"] = "usd";

            // CHARGE PROFILE
            if ((is_array($profile)) && (($profile['customer_id']) && ($profile['profile_id']))) {
                $params['customer'] = $profile['customer_id'];
                $params['source'] = $profile['profile_id'];

            // CHARGE CARD
            } else {
                $params["source"] = array(
                    'object'    => 'card',
                    'number'    => preg_replace("/[^0-9]/", "", $card_number),
                    'exp_month' => preg_replace("/\D/", "", substr($card_expiration,0,2)),
                    'exp_year'  => preg_replace("/\D/", "", substr($card_expiration,2)),
                    'cvc'       => $cvv,
                    'name'      => $card_name,
                    'address_line1'     => $address['street'],
                    'address_line2'     => $address['street2'],
                    'address_city'      => $address['city'],
                    'address_zip'       => $address['zip'],
                    'address_state'     => $address['state'],
                    'address_country'   => $address['country']
                );
            }

            $params["description"] = $description;

            $params["metadata"] = array(
                "email" => $email
            );

            // Process transaction
            $response = $this->sendStripe($params);

            // Setup response messages.
            $this->Transaction = $response["id"];
            $this->Message = $response["error"]["message"];
            $this->Last4CC = $response["source"]["last4"];

            // Get a common CVV response, either it passed or it didn't.
            if ($response["source"]["cvc_check"] == "pass") {
                $this->CVV = true;
            } else {
                $this->CVV = false;
            }

            if (($response["status"] == "succeeded") || ($response["status"] == "paid")) {
                return $response["id"];
            } else {
                return false;
            }
        }

        /*
            Function: chargeInstaPay
                InstaPay interface for <charge>
        */

        protected function chargeInstaPay($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$action = "authcapture") {
            $params = array();

            // Separate card expiration out
            $card_expiration_month = preg_replace('/\D/', '', substr($card_expiration,0,2));
            $card_expiration_year = preg_replace('/\D/', '', substr($card_expiration,2));
            if (strlen($card_expiration_year) == 2) {
                $card_expiration_year = "20".$card_expiration_year;
            }

            if ($action == "authonly") {
                $params["authonly"] = 1;
            }

            $params["action"] = "ns_quicksale_cc";

            $params["amount"] = $amount;
            $params["ccname"] = $card_name;
            $params["ccnum"] = $card_number;
            $params["expmon"] = $card_expiration_month;
            $params["expyear"] = $card_expiration_year;
            $params["cvv2"] = $cvv;
            $params["salestax"] = $tax;

            // Address info is array
            if (is_array($address)) {
                $params["ci_billaddr1"] = trim($address["street"]." ".$address["street2"]);
                $params["ci_billcity"] = $address["city"];
                $params["ci_billstate"] = $address["state"];
                $params["ci_billzip"] = $address["zip"];
                $params["ci_billcountry"] = $address["country"];
            } else {
                $params["ci_billaddr1"] = $address;
            }

            $params["ci_email"] = $email;
            $params["ci_phone"] = $phone;
            $params["ci_memo"] = $description;

            //$params["usepost"] = 1;

            // Process transaction
            $response = $this->sendInstaPay($params);

            // Setup response messages.
            $this->Transaction = $response["transaction_full"];
            $this->Message = $response["message"];
            $this->Last4CC = $response["cc_last_4"];

            // Get a common AVS response.
            if ($response["avs"] == "A") {
                $this->AVS = "Address";
            } elseif ($response["avs"] == "W" || $response["avs"] == "Z") {
                $this->AVS = "Zip";
            } elseif ($response["avs"] == "X" || $response["avs"] == "Y") {
                $this->AVS = "Both";
            } else {
                $this->AVS = false;
            }

            // Get a common CVV response, either it passed or it didn't.
            if ($response["cvv"] == "2" || $response["cvv"] == "8" || $response["cvv"] == "A" || $response["cvv"] == "B") {
                $this->CVV = true;
            } else {
                $this->CVV = false;
            }

            if ($response["status"] == "Accepted") {
                return $response["transaction_full"];
            } else {
                return false;
            }
        }

        /*
            Function: chargeBasys
                Basys interface for <charge>
        */

        protected function chargeBasys($amount,$tax,$card_name,$card_number,$card_expiration,$cvv,$address,$description,$email,$phone,$customer,$profile,$action = "S") {

            // Separate card expiration out
            $card_expiration_month = preg_replace('/\D/', '', substr($card_expiration,0,2));
            $card_expiration_year = preg_replace('/\D/', '', substr($card_expiration,2));
            if (strlen($card_expiration_year) == 2) {
                $card_expiration_year = "20".$card_expiration_year;
            }

            // CHARGE PROFILE
            if ((is_array($profile)) && (($profile['customer_id']) && ($profile['profile_id']))) {

                //echo print_r($profile);

                $params = array(
                    'Transaction'   => array_merge($this->DefaultParameters, array(
                        'Submit_Time'   => date('c'),
                        'Tran_Type'     => $action,
                        'Tran_Amt'      => $this->formatCurrency($amount),
                        'Tran_Tax'      => $tax,
                        'TaxExempt'     => '',
                        'Customer'      => $customer,
                        'Contact_Phone' => $phone,
                        'Contact_Email' => $email,
                        'Tran_Note'     => $description
                    )),
                    'RolodexCardID'     => $profile['profile_id']
                );

                // Process transaction
                $response = $this->sendBasys('SubmitWithRolodexCardID', array($params));

                $resp = $response->SubmitWithRolodexCardIDResult;

            // CHARGE CARD
            } else {

                $params = array(
                    'Transaction'   => array_merge($this->DefaultParameters, array(
                        'Submit_Time'   => date('c'),
                        'Tran_Type'     => $action,
                        'Tran_Amt'      => $this->formatCurrency($amount),
                        'Tran_Tax'      => $tax,
                        'TaxExempt'     => '',
                        'Card_Num'      => $card_number,
                        'Card_Name'     => $card_name,
                        'Card_Exp'      => date('m/y', strtotime($card_expiration_month. '/01/' .$card_expiration_year)),
                        'CVV_Num'       => $cvv,
                        'Customer'      => $customer,
                        'Contact_Phone' => $phone,
                        'Contact_Email' => $email,
                        'Tran_Note'     => $description
                    ))
                );

                if (is_array($address)) {
                    $params['Transaction']["AVS_Street"] = trim($address["street"]." ".$address["street2"]);
                    $params['Transaction']["AVS_Zip"] = $address["zip"];
                }

                // Process transaction
                $response = $this->sendBasys('Submit', array($params));

                $resp = $response->SubmitResult;

            }

            //echo print_r($resp);
            //exit;

            $this->Transaction  = $resp->Proc_ID;
            $this->Message      = $this->basys_message($resp);

            $this->Last4CC = substr($card_number, -4);

            // Card Type: $response->Card_Type

            /*
            // Get a common AVS response.
            if ($response->AVS_Code == "A") {
                $this->AVS = "Address";
            } elseif ($response->AVS_Code == "W" || $response->AVS_Code == "Z") {
                $this->AVS = "Zip";
            } elseif ($response->AVS_Code == "X" || $response->AVS_Code == "Y") {
                $this->AVS = "Both";
            } else {
                $this->AVS = false;
            }

            // Get a common CVV response, either it passed or it didn't.
            if ($response->CVV_Code == "2" || $response->CVV_Code == "8" || $response->CVV_Code == "A" || $response->CVV_Code == "B") {
                $this->CVV = true;
            } else {
                $this->CVV = false;
            }
            */

            //echo print_r($this);
            //echo 'id: ' .$response->SubmitResult->Proc_ID;
            //exit;

            if ($resp->Proc_Resp == 'Approved') {
                return $resp->Proc_ID;
            } else {
                return false;
            }

        }

        /*
            Function: chargeWaived
                Waived interface for <charge>
        */

        protected function chargeWaived($amount,$tax,$description,$email,$phone,$customer) {
            return BigTree::randomString(12);
        }

        /*
            Function: chargeDonation
                Donation interface for <charge>
        */

        protected function chargeDonation($amount,$tax,$description,$email,$phone,$customer) {
            return BigTree::randomString(12);
        }

        /*
            Function: chargeTrade
                Trade interface for <charge>
        */

        protected function chargeTrade($amount,$tax,$description,$email,$phone,$customer) {
            return BigTree::randomString(12);
        }


        /*
            Function: createRecurringPayment
                Creates a recurring payment profile.

            Parameters:
                description - A description of the subscription.
                amount - The amount to charge (includes the tax).
                start_date - The date to begin charging the user (YYYY-MM-DD, blank for immediately).
                period - The unit of how often to charge the user (options: day, week, month, year)
                frequency - The number of units that make up each billing period. (i.e. for bi-weekly the frequency is "2" and period is "week", quarterly would be frequency "3" and period "month")
                card_name - Name as it appears on the credit card.
                card_number - Credit card number.
                card_expiration - 4 or 6 digit expiration date (MMYYYY or MMYY).
                cvv - Credit card security code.
                address - An address array with keys "street", "street2", "city", "state", "zip", "country"
                email - Email address of the purchaser.
                trial_amount - The amount to charge during the (optional) trial period.
                trial_period - The unit of how often to charge the user during the (optional) trial period (options: day, week, month, year)
                trial_frequency - The number of units that make up each billing segment of the (optional) trial period.
                trial_length - The number of billing cycles the (optional) trial period should last.

            Returns:
                Subscriber ID if successful, otherwise returns false.
                $this->Message will contain an error message if not successful.
        */

        function createRecurringPayment($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount = false,$trial_period = false,$trial_frequency = false,$trial_length = false) {
            // Clean up the amount and trial amount.
            $amount = $this->formatCurrency($amount);
            $trial_amount = $this->formatCurrency($trial_amount);

            // Make card number only have numeric digits
            $card_number = preg_replace('/\D/', '', $card_number);

            // If a start date wasn't given, do it now.
            if (!$start_date) {
                $start_date = date("Y-m-d H:i:s");
            }

            if ($this->Service == "authorize.net") {
                return $this->createRecurringPaymentAuthorize($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "paypal") {
                return $this->createRecurringPaymentPayPal($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "paypal-rest") {
                return $this->createRecurringPaymentPayPalREST($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "payflow") {
                return $this->createRecurringPaymentPayflow($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "linkpoint") {
                return $this->createRecurringPaymentLinkPoint($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "linkpoint") {
                return $this->createRecurringPaymentStripe($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "instapay") {
                return $this->createRecurringPaymentInstaPay($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "basys") {
                return $this->createRecurringPaymentBasys($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "waived") {
                return $this->createRecurringPaymentWaived($description,$amount,$start_date,$period,$frequency,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "donation") {
                return $this->createRecurringPaymentDonation($description,$amount,$start_date,$period,$frequency,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            } elseif ($this->Service == "trade") {
                return $this->createRecurringPaymentTrade($description,$amount,$start_date,$period,$frequency,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length);
            }
            return false;
        }

        /*
            Function: createRecurringPaymentAuthorize
                Authorize.Net interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentAuthorize($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentLinkPoint
                First Data / LinkPoint interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentLinkPoint($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentPayflow
                Payflow Gateway interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentPayflow($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentPayPal
                PayPal Payments Pro interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentPayPal($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            $params = array();

            $sd = strtotime($start_date);
            // Parameters specific to creating a recurring profile
            $params["METHOD"] = "CreateRecurringPaymentsProfile";
            $params["PROFILESTARTDATE"] = gmdate("Y-m-d",$sd)."T".gmdate("H:i:s",$sd)."ZL";
            $params["BILLINGPERIOD"] = $this->PayPalPeriods[$period];
            $params["BILLINGFREQUENCY"] = $frequency;
            if ($trial_amount) {
                $params["TRIALAMT"] = $trial_amount;
                $params["TRIALBILLINGPERIOD"] = $this->PayPalPeriods[$trial_period];
                $params["TRIALBILLINGFREQUENCY"] = $trial_frequency;
                $params["TRIALTOTALBILLINGCYCLES"] = $trial_length;
            }
            $params["DESC"] = $description;

            // Split the card name into first name and last name.
            $first_name = substr($card_name,0,strpos($card_name," "));
            $last_name = trim(substr($card_name,strlen($first_name)));

            $params["AMT"] = $amount;
            $params["CREDITCARDTYPE"] = $this->cardType($card_number);
            $params["ACCT"] = $card_number;
            $params["EXPDATE"] = $card_expiration;
            $params["CVV2"] = $cvv;


            $params["FIRSTNAME"] = $first_name;
            $params["LASTNAME"] = $last_name;
            $params["STREET"] = trim($address["street"]." ".$address["street2"]);
            $params["CITY"] = $address["city"];
            $params["STATE"] = $address["state"];
            $params["ZIP"] = $address["zip"];
            if (strlen($address["country"]) == 2) {
                $params["COUNTRYCODE"] = $address["country"];
            } else {
                $params["COUNTRYCODE"] = $this->CountryCodes[strtoupper($address["country"])];
            }

            $params["EMAIL"] = $email;

            $response = $this->sendPayPal($params);

            // Setup response messages.
            $this->Profile = $response["PROFILEID"];
            $this->Message = urldecode($response["L_LONGMESSAGE0"]);

            if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                return $response["PROFILEID"];
            } else {
                return false;
            }
        }

        /*
            Function: createRecurringPaymentPayPalREST
                PayPal REST API interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentPayPalREST($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentStripe
                Stripe interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentStripe($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentInstaPay
                InstaPay interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentInstaPay($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentBasys
                Basys interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentBasys($description,$amount,$start_date,$period,$frequency,$card_name,$card_number,$card_expiration,$cvv,$address,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentWaived
                Waived interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentWaived($description,$amount,$start_date,$period,$frequency,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentDonation
                Donation interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentDonation($description,$amount,$start_date,$period,$frequency,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: createRecurringPaymentTrade
                Trade interface for <createRecurringPayment>
        */

        protected function createRecurringPaymentTrade($description,$amount,$start_date,$period,$frequency,$email,$trial_amount,$trial_period,$trial_frequency,$trial_length) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: formatCurrency
                Formats a currency amount for the payment gateways (they're picky).

            Parameters:
                amount - Currency amount

            Returns:
                A string
        */

        function formatCurrency($amount) {
            return number_format(round(floatval(str_replace(array('$',','),"",$amount)),2),2,".","");
        }

        /*
            Function: paypalExpressCheckoutDetails
                Returns checkout details for an Express Checkout transaciton.
                For: PayPal Payments Pro and Payflow Gateway ONLY.

            Parameters:
                token - The Express Checkout token returned by PayPal.

            Returns:
                An array of buyer information.
        */

        function paypalExpressCheckoutDetails($token) {
            $params = array();
            $params["TOKEN"] = $token;

            // Payflow
            if ($this->Service == "payflow") {
                $params["TRXTYPE"] = "S";
                $params["ACTION"] = "G";
                $params["TENDER"] = "P";

                $response = $this->sendPayflow($params);
                $this->Message = $response["RESPMSG"];

                if ($response["RESULT"] == "0") {
                    return $this->urldecode($response);
                } else {
                    return false;
                }
            // PayPal Payments Pro
            } elseif ($this->Service == "paypal") {
                $params["METHOD"] = "GetExpressCheckoutDetails";

                $response = $this->sendPayPal($params);
                $this->Message = urldecode($response["L_LONGMESSAGE0"]);

                if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                    return $this->urldecode($response);
                } else {
                    return false;
                }
            // PayPal REST API
            } elseif ($this->Service == "paypal-rest") {
                $token = $_SESSION["bigtree"]["paypal-rest-payment-id"];
                $response = $this->sendPayPalREST("payments/payment/$token");
                if ($response->id) {
                    return $response;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        /*
            Function: paypalExpressCheckoutProcess
                Processes an Express Checkout transaction.
                For: PayPal REST API, PayPal Payments Pro and Payflow Gateway ONLY.

            Parameters:
                token - The Express Checkout token returned by PayPal.
                payer_id - The Payer ID returned by PayPal.
                amount - The amount to charge.

            Returns:
                An array of buyer information.
        */

        function paypalExpressCheckoutProcess($token,$payer_id,$amount = false) {
            // Clean up the amount.
            $amount = $this->formatCurrency($amount);

            $params = array();
            $params["TOKEN"] = $token;
            $params["PAYERID"] = $payer_id;
            $params["PAYMENTREQUEST_0_AMT"] = $amount;
            $params["AMT"] = $amount;

            // Payflow
            if ($this->Service == "payflow") {
                $params["TRXTYPE"] = "S";
                $params["ACTION"] = "D";
                $params["TENDER"] = "P";

                $response = $this->sendPayflow($params);
                $this->Transaction = $response["PNREF"];
                $this->PayPalTransaction = $response["PPREF"];
                $this->Message = $response["RESPMSG"];

                if ($response["RESULT"] == "0") {
                    return $response["PNREF"];
                } else {
                    return false;
                }
            // PayPal Payments Pro
            } elseif ($this->Service == "paypal") {
                $params["METHOD"] = "DoExpressCheckoutPayment";
                $params["PAYMENTACTION"] = "Sale";

                $response = $this->sendPayPal($params);
                $this->Message = urldecode($response["L_LONGMESSAGE0"]);
                $this->Transaction = $response["TRANSACTIONID"];

                if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                    return $response["TRANSACTIONID"];
                } else {
                    return false;
                }
            } elseif ($this->Service == "paypal-rest") {
                $token = $_SESSION["bigtree"]["paypal-rest-payment-id"];
                $data = array("payer_id" => $payer_id);
                if ($amount) {
                    $data["transactions"] = array(array("amount" => array("total" => $amount,"currency" => "USD")));
                }
                $response = $this->sendPayPalREST("payments/payment/$token/execute",json_encode($data));
                if ($response->state == "approved") {
                    $this->Transaction = $response->transactions[0]->related_resources[0]->sale->id;
                    return $this->Transaction;
                } else {
                    $this->Errors = $response->details;
                    $this->Message = $response->message;
                    return false;
                }
            } else {
                return false;
            }
        }

        /*
            Function: paypalExpressCheckoutRedirect
                Sets up a PayPal Express Checkout session and redirects the user to PayPal.
                For: PayPal REST API, PayPal Payments Pro and Payflow Gateway ONLY.

            Parameters:
                amount - The amount to charge the user.
                success_url - The URL to return to on successful PayPal login.
                cancel_rul - The URL to return to if the user cancels payment.

            Returns:
                false in the event of a failure, otherwise redirects and dies.
        */

        function paypalExpressCheckoutRedirect($amount,$success_url,$cancel_url) {
            // Clean up the amount.
            $amount = $this->formatCurrency($amount);
            $params = array();

            $params["PAYMENTREQUEST_0_AMT"] = $amount;
            $params["AMT"] = $amount;
            $params["RETURNURL"] = $success_url;
            $params["CANCELURL"] = $cancel_url;

            // Payflow
            if ($this->Service == "payflow") {
                $params["TRXTYPE"] = "S";
                $params["ACTION"] = "S";
                $params["TENDER"] = "P";

                $response = $this->sendPayflow($params);
                $this->Message = $response["RESPMSG"];

                if ($response["RESULT"] == "0") {
                    BigTree::redirect("https://www".($this->Environment == "test" ? ".sandbox" : "").".paypal.com/webscr?cmd=_express-checkout&token=".urldecode($response["TOKEN"])."&AMT=$amount&CURRENCYCODE=USD&RETURNURL=$success_url&CANCELURL=$cancel_url");
                } else {
                    return false;
                }
            // PayPal Payments Pro
            } elseif ($this->Service == "paypal") {
                $params["METHOD"] = "SetExpressCheckout";
                $params["PAYMENTACTION"] = "Sale";

                $response = $this->sendPayPal($params);
                $this->Message = urldecode($response["L_LONGMESSAGE0"]);

                if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                    BigTree::redirect("https://www".($this->Environment == "test" ? ".sandbox" : "").".paypal.com/webscr?cmd=_express-checkout&token=".urldecode($response["TOKEN"])."&AMT=$amount&CURRENCYCODE=USD&RETURNURL=$success_url&CANCELURL=$cancel_url");
                } else {
                    return false;
                }
            // PayPal REST API
            } elseif ($this->Service == "paypal-rest") {
                $data = json_encode(array(
                    "intent" => "sale",
                    "redirect_urls" => array(
                        "return_url" => $success_url,
                        "cancel_url" => $cancel_url
                    ),
                    "payer" => array("payment_method" => "paypal"),
                    "transactions" => array(array(
                        "amount" => array(
                            "total" => $amount,
                            "currency" => "USD"
                        )
                    ))
                ));
                $response = $this->sendPayPalREST("payments/payment",$data);
                if ($response->state == "created") {
                    $_SESSION["bigtree"]["paypal-rest-payment-id"] = $response->id;
                    foreach ($response->links as $link) {
                        if ($link->rel == "approval_url") {
                            BigTree::redirect($link->href);
                        }
                    }
                } else {
                    $this->Errors = $response->details;
                    $this->Message = $response->message;
                    return false;
                }
            } else {
                return false;
            }
        }

        /*
            Function: paypalRESTTokenRequest
                Fetches a new authorization token from PayPal's OAuth servers.
        */

        function paypalRESTTokenRequest() {
            if ($this->Settings["paypal-rest-environment"] == "test") {
                $url = "api.sandbox.paypal.com";
            } else {
                $url = "api.paypal.com";
            }

            $r = json_decode(BigTree::cURL("https://$url/v1/oauth2/token","grant_type=client_credentials",array(CURLOPT_POST => true, CURLOPT_USERPWD => $this->Settings["paypal-rest-client-id"].":".$this->Settings["paypal-rest-client-secret"])));
            if ($r->error) {
                $this->Errors[] = $r->error;
                return false;
            }

            $this->Settings["paypal-rest-expiration"] = date("Y-m-d H:i:s",strtotime("+".$r->expires_in." seconds"));
            $this->Settings["paypal-rest-token"] = $r->access_token;
            $this->saveSettings();
            return true;
        }

        /*
            Function: paypalRESTVaultAuthorize
                Authorizes a payment using a token from the PayPal REST API vault.
                Payment should be captured using standard "capture" method

            Parameters:
                id - The card ID returned when the card was stored.
                user_id - The user ID related to this card (pass in false if no user id was stored with this card).
                amount - The amount to charge (includes the tax).
                tax - The amount of tax to charge (for accounting purposes, must also be included in total amount).
                description - Description of what is being charged.
                email - Email address of the purchaser.
        */

        function paypalRESTVaultAuthorize($id,$user_id,$amount,$tax = 0,$description = "",$email = "") {
            return $this->paypalRESTVaultCharge($id,$user_id,$amount,$tax,$description,$email,"authorize");
        }

        /*
            Function: paypalRESTVaultCharge
                Charges a credit card using a token from the PayPal REST API vault.

            Parameters:
                id - The card ID returned when the card was stored.
                user_id - The user ID related to this card (pass in false if no user id was stored with this card).
                amount - The amount to charge (includes the tax).
                tax - The amount of tax to charge (for accounting purposes, must also be included in total amount).
                description - Description of what is being charged.
                email - Email address of the purchaser.
                action - "sale" or "authorize" (defaults to sale)
        */

        function paypalRESTVaultCharge($id,$user_id,$amount,$tax = 0,$description = "",$email = "",$action = "sale") {
            $amount = $this->formatCurrency($amount);
            $tax = $this->formatCurrency($tax);

            // Split out tax and subtotals if present
            if ($tax) {
                $transaction_details = array(
                    "subtotal" => $this->formatCurrency(floatval($amount) - floatval($tax)),
                    "tax" => $tax,
                    "shipping" => 0
                );
            } else {
                $transaction_details = false;
            }

            $data = array(
                "intent" => $action,
                "payer" => array(
                    "payment_method" => "credit_card",
                    "funding_instruments" => array(array("credit_card_token" => array("credit_card_id" => $id))),
                ),
                "transactions" => array(array(
                    "amount" => array(
                        "total" => $amount,
                        "currency" => "USD"
                    )
                ))
            );

            if ($transaction_details) {
                $data["transactions"][0]["amount"]["details"] = $transaction_details;
            }
            if ($user_id) {
                $data["payer"]["funding_instruments"][0]["credit_card_token"]["payer_id"] = $user_id;
            }
            if ($email) {
                $data["payer"]["payer_info"]["email"] = $email;
            }
            if ($description) {
                $data["transactions"][0]["description"] = $description;
            }

            $response = $this->sendPayPalREST("payments/payment",json_encode($data));

            if ($response->state == "approved") {
                if ($action == "authorize") {
                    $this->Transaction = $response->transactions[0]->related_resources[0]->authorization->id;
                } else {
                    $this->Transaction = $response->transactions[0]->related_resources[0]->sale->id;
                }

                return $this->Transaction;
            } else {
                $this->Message = $response->message;
                $this->Errors = $response->details;

                return false;
            }
        }

        /*
            Function: paypalRESTVaultDelete
                Deletes a credit card stored in the PayPal REST API vault.

            Parameters:
                id - The card ID returned when the card was stored.
        */

        function paypalRESTVaultDelete($id) {
            $this->sendPayPalREST("vault/credit-card/$id","","DELETE");
        }

        /*
            Function: paypalRESTVaultLookup
                Looks up a credit card stored in the PayPal REST API vault.

            Parameters:
                id - The card ID returned when the card was stored.

            Returns:
                Credit card information (only the last 4 digits of the credit card number are visible)
        */

        function paypalRESTVaultLookup($id) {
            $response = $this->sendPayPalREST("vault/credit-card/$id");
            if ($response->state != "ok") {
                $this->Message = $response->message;
                return false;
            }

            $card = new stdClass;
            $card->Address = new stdClass;
            $card->Address->Street = $response->billing_address->line1;
            isset($response->billing_address->line2) ? $card->Address->Street2 = $response->billing_address->line2 : false;
            $card->Address->City = $response->billing_address->city;
            $card->Address->State = $response->billing_address->state;
            $card->Address->Zip = $response->billing_address->postal_code;
            $card->Address->Country = $response->billing_address->country_code;
            $card->ExpirationDate = $response->expire_month."/".$response->expire_year;
            $card->ID = $response->id;
            $card->Name = $response->first_name." ".$response->last_name;
            $card->Number = $response->number;
            $card->Type = $response->type == "amex" ? "American Express" : ucwords($response->type);
            $card->UserID = $response->payer_id;
            $card->ValidUntil = date("Y-m-d H:i:s",strtotime($response->valid_until));

            return $card;
        }

        /*
            Function: paypalRESTVaultStore
                Stores a credit card in the PayPal REST API vault.

            Parameters:
                name - Name on the credit card
                number - Credit card number
                expiration_date - Expiration date (MMYY or MMYYYY format)
                cvv - Credit card security code.
                address - An address array with keys "street", "street2", "city", "state", "zip", "country"
                user_id - A unique ID to associate with this storage (for example, the user ID of this person on your site)

            Returns:
                A card ID to be used for later recall.
        */

        function paypalRESTVaultStore($name,$number,$expiration_date,$cvv,$address,$user_id) {
            // Split the card name into first name and last name.
            $first_name = substr($name,0,strpos($name," "));
            $last_name = trim(substr($name,strlen($first_name)));

            // Make card number only have numeric digits
            $number = preg_replace('/\D/', '', $number);

            // Separate card expiration out
            $card_expiration_month = substr($expiration_date,0,2);
            $card_expiration_year = substr($expiration_date,2);
            if (strlen($expiration_date) == 4) {
                $card_expiration_year = "20".$card_expiration_year;
            }

            // See if we can get a country code
            $country = isset($this->CountryCodes[strtoupper($address["country"])]) ? $this->CountryCodes[strtoupper($address["country"])] : $address["country"];

            $data = array(
                "payer_id" => $user_id,
                "number" => $number,
                "type" => $this->cardType($number),
                "expire_month" => $card_expiration_month,
                "expire_year" => $card_expiration_year,
                "cvv2" => $cvv,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "billing_address" => array(
                    "line1" => $address["street"],
                    "line2" => $address["street2"],
                    "city" => $address["city"],
                    "state" => $address["state"],
                    "postal_code" => $address["zip"],
                    "country_code" => $country
                )
            );

            // Remove blank values, REST API doesn't like them
            $data["billing_address"] = array_filter($data["billing_address"]);
            $data = array_filter($data);

            $response = $this->sendPayPalREST("vault/credit-card",json_encode($data));
            if ($response->state == "ok") {
                return $response->id;
            }

            $this->Errors = $response->details;
            $this->Message = $response->message;
            return false;
        }

        /*
            Function: refund
                Refunds a settled transaction.

            Parameters:
                transaction - The transaction ID to capture funds for.
                card_number - The last four digits of the credit card number used for the transaction (required for some processors).
                amount - The amount to refund (required for some processors).

            Returns:
                Transaction ID if successful, otherwise returns false.
                $this->Message will contain an error message if not successful.
        */

        function refund($transaction,$card_number = "",$amount = 0) {
            // Clean up the amount.
            $amount = $this->formatCurrency($amount);

            // Make card number only have numeric digits
            $card_number = preg_replace('/\D/', '', $card_number);

            if ($this->Service == "authorize.net") {
                return $this->refundAuthorize($transaction,$card_number,$amount);
            } elseif ($this->Service == "paypal") {
                return $this->refundPayPal($transaction,$card_number,$amount);
            } elseif ($this->Service == "paypal-rest") {
                return $this->refundPayPalREST($transaction,$card_number,$amount);
            } elseif ($this->Service == "payflow") {
                return $this->refundPayflow($transaction,$card_number,$amount);
            } elseif ($this->Service == "linkpoint") {
                return $this->refundLinkPoint($transaction,$card_number,$amount);
            } elseif ($this->Service == "stripe") {
                return $this->refundStripe($transaction,$card_number,$amount);
            } elseif ($this->Service == "instapay") {
                return $this->refundInstaPay($transaction,$card_number,$amount);
            } elseif ($this->Service == "basys") {
                return $this->refundBasys($transaction,$card_number,$amount);
            } elseif ($this->Service == "waived") {
                return $this->refundWaived($transaction,$amount);
            } elseif ($this->Service == "donation") {
                return $this->refundDonation($transaction,$amount);
            } elseif ($this->Service == "trade") {
                return $this->refundTrade($transaction,$amount);
            } else {
                throw new Exception("Invalid Payment Gateway");
            }
        }

        /*
            Function: refundAuthorize
                Authorize.Net interface for <refund>
        */

        protected function refundAuthorize($transaction,$card_number,$amount) {
            $params = array();

            $params["x_type"] = "CREDIT";
            $params["x_trans_id"] = $transaction;
            $params["x_card_num"] = $card_number;
            if ($amount) {
                $params["x_amount"] = $amount;
            }

            $response = $this->sendAuthorize($params);

            // Setup response messages.
            $this->Transaction = $response["transaction"];
            $this->Message = $response["message"];

            if ($response["status"] == "approved") {
                return $response["transaction"];
            } else {
                return false;
            }
        }

        /*
            Function: refundLinkPoint
                First Data / LinkPoint interface for <refund>
        */

        protected function refundLinkPoint($transaction,$card_number,$amount) {
            $params = array(
                "orderoptions" => array(
                    "ordertype" => "CREDIT"
                ),
                "creditcard" => array(
                    "cardnumber" => $card_number
                ),
                "transactiondetails" => array(
                    "ip" => $_SERVER["REMOTE_ADDR"],
                    "oid" => $transaction
                )
            );

            if ($amount) {
                $params["payment"]["chargetotal"] = $amount;
            }

            $response = $this->sendLinkPoint($params);

            // Setup response messages.
            $this->Transaction = strval($response->r_ordernum);
            $this->Message = strval($response->r_error);

            if (strval($response->r_message) == "ACCEPTED") {
                return $this->Transaction;
            } else {
                return false;
            }
        }

        /*
            Function: refundPayPal
                PayPal Payments Pro interface for <refund>
        */

        protected function refundPayPal($transaction,$card_number,$amount) {
            $params = array();

            $params["METHOD"] = "RefundTransaction";
            $params["TRANSACTIONID"] = $transaction;

            if ($amount) {
                $params["REFUNDTYPE"] = "Partial";
                $params["AMT"] = $amount;
            } else {
                $params["REFUNDTYPE"] = "Full";
            }

            $response = $this->sendPayPal($params);

            // Setup response messages.
            $this->Transaction = $response["REFUNDTRANSACTIONID"];
            $this->Message = urldecode($response["L_LONGMESSAGE0"]);

            if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                return $response["REFUNDTRANSACTIONID"];
            } else {
                return false;
            }
        }

        /*
            Function: refundPayPalREST
                PayPal REST API interface for <refund>

                Note: Currently the PayPal REST API requires an amount to refund if the transaction was first authorized and later captured.
                This does not apply to transactions that were simply charged through the charge() method.
        */

        protected function refundPayPalREST($transaction,$card_number,$amount) {
            if ($amount) {
                $data = json_encode(array(
                    "amount" => array(
                        "total" => $amount,
                        "currency" => "USD"
                    )
                ));
            } else {
                $data = "{}";
            }
            // First try as if it was a sale.
            $response = $this->sendPayPalREST("payments/sale/$transaction/refund",$data);
            if ($response->state == "completed") {
                return $response->id;
            // Try as if it were auth/capture
            } elseif ($amount) {
                $response = $this->sendPayPalREST("payments/capture/$transaction/refund",$data);
                if ($response->state == "completed") {
                    return $response->id;
                }
            }

            $this->Message = $response->message;
            return false;
        }

        /*
            Function: refundPayflow
                PayPal Payflow Gateway interface for <refund>
        */

        protected function refundPayflow($transaction,$card_number,$amount) {
            $params = array();

            $params["TRXTYPE"] = "C";
            $params["ORIGID"] = $transaction;

            if ($amount) {
                $params["AMT"] = $amount;
            }

            $response = $this->sendPayflow($params);

            // Setup response messages.
            $this->Transaction = $response["PNREF"];
            $this->Message = urldecode($response["RESPMSG"]);

            if ($response["RESULT"] == "0") {
                return $response["PNREF"];
            } else {
                return false;
            }
        }

        /*
            Function: refundStripe
                Stripe interface for <refund>
        */

        protected function refundStripe($transaction,$card_number,$amount) {
            $params = array();

            $params["endpoint"] = "charges/" .$transaction. "/refunds";

            // Note: Stripe currently seems to want an "amount" specified for refunds, otherwise it will not process (7/2/2015)
            if (($amount) && ($amount != 0.00)) {
                $params["amount"] = $this->getMoneyAsCents($amount);
            }

            // Process Transaction
            $response = $this->sendStripe($params);

            if ($response["data"][0]["id"]) {
                $id = $response["data"][0]["id"];
            } else {
                $id = $response["id"];
            }

            // Setup response messages.
            $this->Transaction = $id;
            $this->Message = $response["error"]["message"];

            return $id;
        }

        /*
            Function: refundInstaPay
                InstaPay interface for <refund>
        */

        protected function refundInstaPay($transaction,$card_number,$amount) {
            $params = array();

            // Get transaction parts
            $parts = $this->parseIPTransactionID($transaction);

            $params["action"] = "ns_credit";
            $params["amount"] = $amount;
            $params["historykeyid"] = $parts["historyid"];

            // Process Transaction
            $response = $this->sendInstaPay($params);

            // Setup response messages.
            $this->Transaction = $response["transaction_full"];
            $this->Message = $response["message"];

            if ($response["status"] == "Accepted") {
                return $response["transaction_full"];
            } else {
                return false;
            }
        }

        /*
            Function: refundBasys
                Basys interface for <refund>
        */

        protected function refundBasys($transaction,$card_number,$amount) {

            $params = array(
                'Transaction'   => array_merge($this->DefaultParameters, array(
                    'Submit_Time'   => date('c'),
                    'Tran_Type'     => 'R',
                    'Proc_ID'       => $transaction,
                    'Tran_Amt'      => $this->formatCurrency($amount),
                    'Tran_Tax'      => '0.00',
                    'TaxExempt'     => 'false'
                ))
            );

            // Process Transaction
            $response = $this->sendBasys('Submit', array($params));

            // Setup response messages.
            $this->Transaction = $response->SubmitResult->Proc_ID;
            $this->Message = $response->SubmitResult->Proc_Mess;

            if ($response->SubmitResult->Proc_Resp == "Approved") {
                return $response->SubmitResult->Proc_ID;
            } else {
                return false;
            }

        }

        /*
            Function: refundWaived
                Waived interface for <refund>
        */

        protected function refundWaived($transaction,$amount) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: refundDonation
                Donation interface for <refund>
        */

        protected function refundDonation($transaction,$amount) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: refundTrade
                Trade interface for <refund>
        */

        protected function refundTrade($transaction,$amount) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: saveSettings
                Saves changed service and settings.
        */

        function saveSettings() {
            $admin = new BigTreeAdmin;
            $admin->updateSettingValue("bigtree-internal-payment-gateway",array("service" => $this->Service,"settings" => $this->Settings));
        }

        /*
            Function: sendAuthorize
                Sends a command to Authorize.Net.
        */

        protected function sendAuthorize($params) {
            $count = 0;
            $possibilities = array("","approved","declined","error");
            $this->Unresponsive = false;

            // Get the default parameters
            $params = array_merge($this->DefaultParameters,$params);

            // Authorize wants a GET instead of a POST, so we have to convert it away from an array.
            $fields = array();
            foreach ($params as $key => $val) {
                $fields[] = $key."=".str_replace("&","%26",$val);
            }

            // Send it off to the server, try 3 times.
            while ($count < 3) {
                $response = BigTree::cURL($this->PostURL,implode("&",$fields));
                if ($response) {
                    $r = explode("|",$response);
                    return array(
                        "status" => $possibilities[$r[0]],
                        "message" => $r[3],
                        "authorization" => $r[4],
                        "avs" => $r[5],
                        "cvv" => $r[39],
                        "transaction" => $r[6],
                        "cc_last_4" => substr($r[50],-4,4)
                    );
                }

                $count++;
            }

            $this->Unresponsive = true;
            return false;
        }

        /*
            Function: sendLinkPoint
                Sends a command to First Data / LinkPoint.
        */

        protected function sendLinkPoint($params) {
            $count = 0;
            $this->Unresponsive = false;

            $params["merchantinfo"]["configfile"] = $this->Store;
            $xml = "<order>";
            foreach ($params as $container => $data) {
                $xml .= "<$container>";
                foreach ($data as $key => $val) {
                    if (is_array($val)) {
                        $xml .= "<$key>";
                        foreach ($val as $k => $v) {
                            $xml .= "<$k>".htmlspecialchars($v)."</$k>";
                        }
                        $xml .= "</$key>";
                    } else {
                        $xml .= "<$key>".htmlspecialchars($val)."</$key>";
                    }
                }
                $xml .= "</$container>";
            }
            $xml .= "</order>";

            // Send it off to the server, try 3 times.
            while ($count < 3) {
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$this->PostURL);
                curl_setopt($ch,CURLOPT_POST, 1);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $xml);
                curl_setopt($ch,CURLOPT_SSLCERT, $this->Certificate);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                if ($response) {
                    return simplexml_load_string("<lpresonsecontainer>".$response."</lpresonsecontainer>");
                }
                $count++;
            }

            $this->Unresponsive = true;
            return false;
        }

        /*
            Function: sendPayPal
                Sends a command to PayPal Payments Pro.
        */

        protected function sendPayPal($params) {
            $count = 0;
            $this->Unresponsive = false;

            // Get the default parameters
            $params = array_merge($this->DefaultParameters,$params);

            // Authorize wants a GET instead of a POST, so we have to convert it away from an array.
            $fields = array();
            foreach ($params as $key => $val) {
                $fields[] = $key."=".str_replace("&","%26",$val);
            }

            // Send it off to the server, try 3 times.
            while ($count < 3) {
                $response = BigTree::cURL($this->PostURL,implode("&",$fields));

                if ($response) {
                    $response_array = array();
                    $response_parts = explode("&",$response);
                    foreach ($response_parts as $part) {
                        list($key,$val) = explode("=",$part);
                        $response_array[$key] = $val;
                    }

                    return $response_array;
                }

                $count++;
            }

            $this->Unresponsive = true;
            return false;
        }

        /*
            Function: sendPayPalREST
                Sends a command to PayPal REST API.
        */

        protected function sendPayPalREST($endpoint,$data = "",$method = false) {
            if ($method) {
                return json_decode(BigTree::cURL($this->PostURL.$endpoint,$data,array(CURLOPT_HTTPHEADER => $this->Headers,CURLOPT_CUSTOMREQUEST => $method)));
            } else {
                return json_decode(BigTree::cURL($this->PostURL.$endpoint,$data,array(CURLOPT_HTTPHEADER => $this->Headers)));
            }
        }

        /*
            Function: sendPayflow
                Sends a command to PayPal Payflow Gateway.
        */

        protected function sendPayflow($params) {
            $count = 0;
            $this->Unresponsive = false;

            // We build a random hash to submit as the transaction ID so that Payflow knows we're trying a repeat transaction, and spoof Mozilla.
            $extras = array(
                CURLOPT_HTTPHEADER => array("X-VPS-Request-ID: ".uniqid("",true)),
                CURLOPT_USERAGENT => "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)"
            );

            // Get the default parameters
            $params = array_merge($this->DefaultParameters,$params);

            // Authorize wants a GET instead of a POST, so we have to convert it away from an array.
            $fields = array();
            foreach ($params as $key => $val) {
                $fields[] = $key."=".str_replace("&","%26",$val);
            }

            // Send it off to the server, try 3 times.
            while ($count < 3) {
                $response = BigTree::cURL($this->PostURL,implode("&",$fields),$extras);

                if ($response) {
                    $response = strstr($response, 'RESULT');
                    $response_array = array();
                    $response_parts = explode("&",$response);
                    foreach ($response_parts as $part) {
                        list($key,$val) = explode("=",$part);
                        $response_array[$key] = $val;
                    }

                    return $response_array;
                }

                $count++;
            }

            $this->Unresponsive = true;
            return false;
        }

        /*
            Function: sendStripe
                Sends a command to Stripe
        */

        protected function sendStripe($params, $method=false) {
            $count = 0;
            $this->Unresponsive = false;

            // Get the default parameters
            $params = array_merge($this->DefaultParameters, $params);

            // Url to post to
            $post_url = $this->PostURL. '/' .$params['endpoint'];
            unset($params['endpoint']);

            // Options
            $options = array(
                CURLOPT_USERPWD     => $this->APILogin,
                CURLOPT_HTTPHEADER  => array('Content-Type: application/x-www-form-urlencoded')
            );
            if ($method) $options[CURLOPT_CUSTOMREQUEST] = $method;

            // Send it off to the server, try 3 times.
            while ($count < 3) {
                $response = BigTree::cURL($post_url, http_build_query($params), $options);

                if ($response) {
                    return json_decode($response, true);
                }

                $count++;
            }

            $this->Unresponsive = true;
            return false;
        }

        /*
            Function: sendInstaPay
                Sends a command to InstaPay
        */

        protected function sendInstaPay($params) {
            $count = 0;
            $this->Unresponsive = false;

            // Get the default parameters
            $params = array_merge($this->DefaultParameters,$params);

            // Send it off to the server, try 3 times.
            while ($count < 3) {
                $response = BigTree::cURL($this->PostURL, $params);
                if (($response) && ($response["message"] != "1000410001:Invalid merchant")) {

                    // Parse response
                    $rvals = $this->parseIPResult($response);

                    if ($rvals["Status"] == "Declined") {
                        $message = $rvals["Declined"][1]. ':' .$rvals["Declined"][2];
                    }

                    return array(
                        "status"            => $rvals["Status"],
                        "message"           => $message,
                        "historyid"         => $rvals["historyid"],
                        "orderid"           => $rvals["orderid"],
                        "cc_last_4"         => substr($rvals["ACCOUNTNUMBER"], -4, 4),
                        "type"              => $rvals["PAYTYPE"],
                        "reason_code"       => $rvals["rcode"],
                        "recurid"           => $rvals["recurid"],
                        "result"            => $rvals["result"],
                        "transaction"       => $rvals["transid"],
                        "transaction_full"  => $rvals['historyid']. '|' .$rvals['orderid']. '|' .$rvals["transid"]
                    );

                }

                $count++;
            }

            $this->Unresponsive = true;
            return false;
        }

        /*
            Function: sendBasys
                Sends a command to Basys
        */

        protected function sendBasys($action, $params) {
            $count = 0;
            $this->Unresponsive = false;

            // Soap client
            $client = new SoapClient($this->PostURL. '?WSDL', array('trace' => 1));

            // Send it off to the server, try 3 times.
            //while ($count < 3) {

                try {

                    //echo print_r($params);
                    //exit;

                    // Get response
                    $response = $client->__soapCall($action, $params);

                    //echo print_r($client->__getLastRequest());
                    //echo print_r($response);
                    //exit;

                    return $response;

                } catch (Exception $ex) {

                    @error_log($client->__getLastRequest());

                    //echo print_r($client->__getLastRequest());
                    //echo 'SOAP Error: ' .$ex->getMessage(). ' (Line ' .$ex->getLine(). ' in ' .$ex->getFile(). ')';
                    //exit;

                }

            //}

            $this->Unresponsive = true;
            return false;

        }

        /*
            Function: setupAuthorize
                Prepares an environment for Authorize.Net payments.
        */

        protected function setupAuthorize() {
            $this->APILogin = $this->Settings["authorize-api-login"];
            $this->TransactionKey = $this->Settings["authorize-transaction-key"];
            $this->Environment = $this->Settings["authorize-environment"];

            if ($this->Environment == "test") {
                $this->PostURL = "https://test.authorize.net/gateway/transact.dll";
            } else {
                $this->PostURL = "https://secure.authorize.net/gateway/transact.dll";
            }

            $this->DefaultParameters = array(
                "x_delim_data" => "TRUE",
                "x_delim_char" => "|",
                "x_relay_response" => "FALSE",
                "x_url" => "FALSE",
                "x_version" => "3.1",
                "x_method" => "CC",
                "x_login" => $this->APILogin,
                "x_tran_key" => $this->TransactionKey
            );
        }

        /*
            Function: setupLinkPoint
                Prepares an environment for First Data / LinkPoint.
        */

        protected function setupLinkPoint() {
            $this->Store = $this->Settings["linkpoint-store"];
            $this->Environment = $this->Settings["linkpoint-environment"];
            $this->Certificate = SERVER_ROOT."custom/certificates/".$this->Settings["linkpoint-certificate"];

            if ($this->Environment == "test") {
                $this->PostURL = "https://staging.linkpt.net:1129";
            } else {
                $this->PostURL = "https://secure.linkpt.net:1129";
                $this->DefaultParameters["orderoptions"] = array(
                    "result" => "live"
                );
            }
        }

        /*
            Function: setupPayPal
                Prepares an environment for PayPal Payments Pro payments.
        */

        protected function setupPayPal() {
            $this->Username = $this->Settings["paypal-username"];
            $this->Password = $this->Settings["paypal-password"];
            $this->Signature = $this->Settings["paypal-signature"];
            $this->Environment = $this->Settings["paypal-environment"];

            if ($this->Environment == "test") {
                $this->PostURL = "https://api-3t.sandbox.paypal.com/nvp";
            } else {
                $this->PostURL = "https://api-3t.paypal.com/nvp";
            }

            $this->DefaultParameters = array(
                "VERSION" => "54.0",
                "USER" => $this->Username,
                "PWD" => $this->Password,
                "SIGNATURE" => $this->Signature
            );
        }

        /*
            Function: setupPayPalREST
                Prepares an environment for the PayPal REST API.
        */

        protected function setupPayPalREST() {
            // Check on the token expiration, get a new one if needed in the next minute
            if (strtotime($this->Settings["paypal-rest-expiration"]) < time() + 60) {
                $this->paypalRESTTokenRequest();
            }
            // Setup default cURL headers
            $this->Headers = array(
                "Content-type: application/json",
                "Authorization: Bearer ".$this->Settings["paypal-rest-token"]
            );

            if ($this->Settings["paypal-rest-environment"] == "test") {
                $this->PostURL = "https://api.sandbox.paypal.com/v1/";
            } else {
                $this->PostURL = "https://api.paypal.com/v1/";
            }
        }

        /*
            Function: setupPayflow
                Prepares an environment for PayPal Payflow Gateway payments.
        */

        protected function setupPayflow() {
            $this->Username = $this->Settings["payflow-username"];
            $this->Password = $this->Settings["payflow-password"];
            $this->Vendor = $this->Settings["payflow-vendor"];
            $this->Environment = $this->Settings["payflow-environment"];
            $this->Partner = $this->Settings["payflow-partner"];

            if ($this->Environment == "test") {
                $this->PostURL = "https://pilot-payflowpro.paypal.com";
            } else {
                $this->PostURL = 'https://payflowpro.paypal.com';
            }

            $this->DefaultParameters = array(
                "USER" => $this->Username,
                "VENDOR" => $this->Vendor,
                "PARTNER" => $this->Partner,
                "PWD" => $this->Password
            );
        }

        /*
            Function: setupStripe
                Prepares an environment for Stripe payments.
        */

        protected function setupStripe() {
            $this->APILogin = $this->Settings["stripe-api-key"];

            $this->PostURL = "https://api.stripe.com/v1";

            $this->DefaultParameters = array(
            );
        }

        /*
            Function: setupInstaPay
                Prepares an environment for InstaPay payments.
        */

        protected function setupInstaPay() {
            $this->IPMerchantPin = $this->Settings["instapay-merchant-pin"];
            $this->Environment = $this->Settings["instapay-environment"];

            if ($this->Environment == "test") {
                $this->IPAccountID = 'TEST0';
            } else {
                $this->IPAccountID = $this->Settings["instapay-account-id"];
            }

            $this->PostURL = "https://trans.instapaygateway.com/cgi-bin/process.cgi";

            $this->DefaultParameters = array(
                "acctid"                => $this->IPAccountID,
                "merchantpin"           => $this->IPMerchantPin,
                "ci_ipaddress"          => $_SERVER['REMOTE_ADDR']
            );
        }

        /*
            Function: setupBasys
                Prepares an environment for Basys payments.
        */

        protected function setupBasys() {
            $this->BasysAccountID = $this->Settings["basys-account-id"];
            $this->BasysPassword = $this->Settings["basys-password"];

            $this->PostURL = "https://connect16.basyspro.net/connect.asmx";

            $this->DefaultParameters = array(
                'Syn_Act'   => $this->BasysAccountID,
                'Syn_Pwd'   => $this->BasysPassword,
                'IP'        => $_SERVER['REMOTE_ADDR']
            );
        }

        /*
            Function: setupWaived
                Prepares an environment for waived payments.
        */

        protected function setupWaived() {

        }

        /*
            Function: setupDonation
                Prepares an environment for donation payments.
        */

        protected function setupDonation() {

        }

        /*
            Function: setupTrade
                Prepares an environment for trade payments.
        */

        protected function setupTrade() {

        }


        /*
            Function: urldecode
                urldecodes a whole array.
        */

        protected function urldecode($array) {
            foreach ($array as &$item) {
                $item = urldecode($item);
            }
            return $array;
        }

        /*
            Function: void
                Voids an authorization.

            Parameters:
                authorization - The transaction/authorization ID to void.

            Returns:
                Transaction ID if successful, otherwise returns false.
                $this->Message will contain an error message if not successful.
        */

        function void($authorization, $amount=0) {
            if ($this->Service == "authorize.net") {
                return $this->voidAuthorize($authorization);
            } elseif ($this->Service == "paypal") {
                return $this->voidPayPal($authorization);
            } elseif ($this->Service == "paypal-rest") {
                return $this->voidPayPalREST($authorization);
            } elseif ($this->Service == "payflow") {
                return $this->voidPayflow($authorization);
            } elseif ($this->Service == "linkpoint") {
                return $this->voidLinkPoint($authorization);
            } elseif ($this->Service == "stripe") {
                return $this->voidStripe($authorization, $amount);
            } elseif ($this->Service == "instapay") {
                return $this->voidInstaPay($authorization);
            } elseif ($this->Service == "basys") {
                return $this->voidBasys($authorization);
            } elseif ($this->Service == "waived") {
                return $this->voidWaived($authorization);
            } elseif ($this->Service == "donation") {
                return $this->voidDonation($authorization);
            } elseif ($this->Service == "trade") {
                return $this->voidTrade($authorization);
            } else {
                throw new Exception("Invalid Payment Gateway");
            }
        }

        /*
            Function: voidAuthorize
                Authorize.Net interface for <void>
        */

        protected function voidAuthorize($authorization) {
            $params = array();

            $params["x_type"] = "VOID";
            $params["x_trans_id"] = $authorization;

            $response = $this->sendAuthorize($params);

            // Setup response messages.
            $this->Transaction = $response["transaction"];
            $this->Message = $response["message"];

            if ($response["status"] == "approved") {
                return $response["transaction"];
            } else {
                return false;
            }
        }

        /*
            Function: voidLinkPoint
                First Data / LinkPoint interface for <void>
        */

        protected function voidLinkPoint($authorization) {
            $params = array(
                "orderoptions" => array(
                    "ordertype" => "VOID"
                ),
                "transactiondetails" => array(
                    "ip" => $_SERVER["REMOTE_ADDR"],
                    "oid" => $authorization
                )
            );

            $response = $this->sendLinkPoint($params);

            // Setup response messages.
            $this->Transaction = strval($response->r_ordernum);
            $this->Message = strval($response->r_error);

            if (strval($response->r_message) == "ACCEPTED") {
                return $this->Transaction;
            } else {
                return false;
            }
        }

        /*
            Function: voidPayPal
                PayPal Payments Pro interface for <void>
        */

        protected function voidPayPal($authorization) {
            $params = array();

            $params["METHOD"] = "DoVoid";
            $params["AUTHORIZATIONID"] = $authorization;

            $response = $this->sendPayPal($params);

            // Setup response messages.
            $this->Transaction = $response["AUTHORIZATIONID"];
            $this->Message = urldecode($response["L_LONGMESSAGE0"]);

            if ($response["ACK"] == "Success" || $response["ACK"] == "SuccessWithWarning") {
                return $response["AUTHORIZATIONID"];
            } else {
                return false;
            }
        }

        /*
            Function: voidPayPalREST
                PayPal REST API interface for <void>
        */

        protected function voidPayPalREST($authorization) {
            $response = $this->sendPayPalREST("payments/authorization/$authorization/void","{}");
            if ($response->state == "voided") {
                return $response->id;
            } else {
                $this->Message = $response->message;
                return false;
            }
        }

        /*
            Function: voidPayflow
                PayPal Payflow Gateway interface for <void>
        */

        protected function voidPayflow($authorization) {
            $params = array();

            $params["TRXTYPE"] = "V";
            $params["ORIGID"] = $authorization;

            $response = $this->sendPayflow($params);

            // Setup response messages.
            $this->Transaction = $response["PNREF"];
            $this->Message = urldecode($response["RESPMSG"]);

            if ($response["RESULT"] == "0") {
                return $response["PNREF"];
            } else {
                return false;
            }
        }

        /*
            Function: voidStripe
                Stripe interface for <void>
        */

        protected function voidStripe($authorization, $amount) {
            return $this->refundStripe($authorization, '', $amount);
        }

        /*
            Function: voidInstaPay
                InstaPay interface for <void>
        */

        protected function voidInstaPay($authorization) {
            $params = array();

            // Get transaction parts
            $parts = $this->parseIPTransactionID($authorization);

            $params["action"] = "ns_void";
            //$params["amount"] = $amount;
            $params["historykeyid"] = $parts["historyid"];
            $params["orderkeyid"] = $parts["orderid"];

            // Process Transaction
            $response = $this->sendInstaPay($params);

            // Setup response messages.
            $this->Transaction = $response["transaction_full"];
            $this->Message = $response["message"];

            if ($response["status"] == "Accepted") {
                return $response["transaction_full"];
            } else {
                return false;
            }
        }

        /*
            Function: voidBasys
                Basys interface for <void>
        */

        protected function voidBasys($authorization) {

            $params = array(
                'Transaction'   => array_merge($this->DefaultParameters, array(
                    'Submit_Time'   => date('c'),
                    'Tran_Type'     => 'V',
                    'Proc_ID'       => $transaction,
                    'Tran_Amt'      => '0.00',
                    'Tran_Tax'      => '0.00',
                    'TaxExempt'     => 'false'
                ))
            );

            // Process Transaction
            $response = $this->sendBasys('Submit', array($params));

            // Setup response messages.
            $this->Transaction = $response->SubmitResult->Proc_ID;
            $this->Message = $response->SubmitResult->Proc_Mess;

            if ($response->SubmitResult->Proc_Resp == "Approved") {
                return $response->SubmitResult->Proc_ID;
            } else {
                return false;
            }

        }

        /*
            Function: voidWaived
                Waived interface for <void>
        */

        protected function voidWaived($authorization) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: voidDonation
                Donation interface for <void>
        */

        protected function voidDonation($authorization) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /*
            Function: voidTrade
                Trade interface for <void>
        */

        protected function voidTrade($authorization) {
            throw new Exception("This method is not currently supported for the selected payment gateway.");
        }

        /* Function getMoneyAsCents
                Convert dollar amount to cents
        */

        function getMoneyAsCents($value) {
            $value = preg_replace("/\,/i", "" ,$value); // strip out commas
            $value = preg_replace("/([^0-9\.\-])/i", "", $value); // strip out all but numbers, dash, and dot
            // make sure we are dealing with a proper number now, no +.4393 or 3...304 or 76.5895,94
            if (!is_numeric($value)) {
                return 0.00;
            }
            // convert to a float explicitly
            $value = (float)$value;
            return round($value, 2) * 100;
        }

        /*
            Function: parseIPResult
                Parse the response from InstaPay
        */

        protected function parseIPResult($result) {
            $retval = "";
            if ($result) {
                $result = trim(str_replace("<html><body><plaintext>", "", $result));
                $lines = explode("\n", $result);
                foreach ($lines as $line) {
                    $items = explode("=", $line);
                    $heading = trim($items[0]);
                    $vals = explode(":", $items[1]);
                    if (count($vals) == 1) {
                        $retval[$heading] = trim($vals[0]);
                    } else {
                        array_walk($vals, "trim");
                        $retval[$heading] = $vals;
                    }
                }
            }
            return $retval;
        }

        /*
            Function: parseIPTransactionID
                Parses an InstaPay Transaction ID
        */

        protected function parseIPTransactionID($transaction) {
            $parts = explode("|", $transaction);
            $out["historyid"] = $parts[0];
            $out["orderid"] = $parts[1];
            $out["transactionid"] = $parts[2];
            return $out;
        }


        /*
            Function: createCustomer
                Create a customer profile.
        */

        function createCustomer($data) {

            // PAYMENT PROFILES FOR SERVICE ENABLED
            if ($this->Settings[$this->Service. '-payment-profiles']) {

                if ($this->Service == 'stripe') {
                    return $this->stripe_createCustomer($data);
                } else if ($this->Service == 'basys') {
                    return $this->basys_createCustomer($data);
                }

            }

        }


        /*
            Function: stripe_createCustomer
                Stripe interface for creating a customer (which payment profiles are attached to).
        */

        protected function stripe_createCustomer($data) {

            $out = array();

            // PARAMS TO SEND TO STRIPE
            $params = array(
                'endpoint'      => 'customers',
                //'name'          => $data['customer']['name'],
                'email'         => $data['customer']['email'],
                'description'   => $data['customer']['description']
            );

            if (is_array($data['customer']['metadata'])) {
                $params['metadata'] = $data['customer']['metadata'];
            }

            // PROCESS
            $response = $this->sendStripe($params);

            // SUCCESSFUL
            if ($response['id']) {
                $out['id'] = $response['id'];
            } else {
                $out['error'] = $response['error']['message'];
            }

            return $out;

        }

        /*
            Function: basys_createCustomer
                Basys interface for creating a customer (which payment profiles are attached to).
        */

        protected function basys_createCustomer($data) {

            $out = array();

            $name = explode(' ', $data['customer']['name'], 2);

            $params = array(
                'Account'   => $this->DefaultParameters['Syn_Act'],
                'Password'  => $this->DefaultParameters['Syn_Pwd'],
                'Record' => array(
                    'ID'        => 0,
                    'Reference' => $data['customer']['account_id'],
                    'Customer'  => $data['customer']['account_id'],
                    'Contact'   => array(
                        'FirstName' => $name[0],
                        'LastName'  => $name[1],
                        'Name'      => $data['customer']['name'],
                        'Title'     => '',
                        'Phone'     => preg_replace('/\D/', '', $data['customer']['phone']),
                        'Email'     => $data['customer']['email']
                    ),
                    'Business'  => array(
                        'Name'  => '-'
                    ),
                    'TaxExempt' => ''
                )
            );

            // Send to Basys
            $response = $this->sendBasys('SaveRolodex', array($params));

            //echo print_r($response);
            //exit;

            // HAVE ERROR
            if ($response->SaveRolodexResult->Error) {
                $out['error'] = $response->SaveRolodexResult->Message;

            // NO ERROR
            } else {

                // GET CUSTOMER
                $response = $this->getCustomer($data);

                //echo print_r($response);
                //exit;

                // HAVE ERROR
                if ($response->GetRolodexRecordResult->Error) {
                    $out['error'] = $response->GetRolodexRecordResult->Message;

                // NO ERROR
                } else {

                    // HAVE ERROR
                    if ($response->GetRolodexRecordResult->Error) {
                        $out['error'] = $response->GetRolodexRecordResult->Message;

                    // NO ERROR
                    } else {
                        $out['id'] = $response->GetRolodexRecordResult->ID;
                    }

                }

            }

            return $out;

        }



        /*
            Function: getCustomer
                Get a customer profile.
        */

        function getCustomer($data) {

            // PAYMENT PROFILES FOR SERVICE ENABLED
            if ($this->Settings[$this->Service. '-payment-profiles']) {

                if ($this->Service == 'stripe') {
                    //return $this->stripe_getCustomer($data['customer']['id']);
                } else if ($this->Service == 'basys') {
                    return $this->basys_getCustomer($data['customer']['account_id']);
                }

            }

        }

        /*
            Function: basys_getCustomer
                Basys interface for getting a customer (including their cards)
        */

        function basys_getCustomer($id) {

            // GET CUSTOMER PROFILE & CARDS
            $params = array(
                'Account'   => $this->DefaultParameters['Syn_Act'],
                'Password'  => $this->DefaultParameters['Syn_Pwd'],
                'Reference' => $id
            );

            // Send to Basys
            $response = $this->sendBasys('GetRolodexRecord', array($params));

            //echo print_r($response);
            //exit;

            return $response;

        }


        /*
            Function: updateCustomer
                Update a customer profile.
        */

        function updateCustomer($data) {

            // PAYMENT PROFILES FOR SERVICE ENABLED
            if ($this->Settings[$this->Service. '-payment-profiles']) {

                if ($this->Service == 'stripe') {
                    return $this->stripe_updateCustomer($data);
                } else if ($this->Service == 'basys') {
                    return $this->basys_updateCustomer($data);
                }

            }

        }


        /*
            Function: stripe_updateCustomer
                Stripe interface for updating a customer (which payment profiles are attached to).
        */

        protected function stripe_updateCustomer($data) {

            $out = array();

            // PARAMS TO SEND TO STRIPE
            $params = array(
                'endpoint'      => 'customers/' .$data['customer']['id'],
                'email'         => $data['customer']['email'],
                'description'   => $data['customer']['description']
            );

            if (is_array($data['customer']['metadata'])) {
                $params['metadata'] = $data['customer']['metadata'];
            }

            // PROCESS
            $response = $this->sendStripe($params);

            // SUCCESSFUL
            if ($response['id']) {
                $out['id'] = $response['id'];
            } else {
                $out['error'] = $response['error']['message'];
            }

            return $out;

        }

        /*
            Function: basys_updateCustomer
                Basys interface for updating a customer (which payment profiles are attached to).
        */

        protected function basys_updateCustomer($data) {

            $out = array();

            $out['error'] = 'Customer updating is not supported with Basys.';

            /*
            $name = explode(' ', $data['customer']['name'], 2);

            $params = array(
                'Account'   => $this->DefaultParameters['Syn_Act'],
                'Password'  => $this->DefaultParameters['Syn_Pwd'],
                'Record' => array(
                    'ID'        => $data['customer']['id'],
                    'Reference' => $data['customer']['account_id'],
                    'Customer'  => $data['customer']['account_id'],
                    'Contact'   => array(
                        'FirstName' => $name[0],
                        'LastName'  => $name[1],
                        'Name'      => $data['customer']['name'],
                        'Title'     => '',
                        'Phone'     => $data['customer']['phone'],
                        'Email'     => $data['customer']['email']
                    ),
                    'Business'  => array(
                        'Name'  => '-'
                    ),
                    'TaxExempt' => ''
                )
            );

            // Send to Basys
            $response = $this->sendBasys('SaveRolodex', array($params));

            echo print_r($response);
            exit;

            // HAVE ERROR
            if ($response->SaveRolodexResult->Error) {
                $out['error'] = $response->SaveRolodexResult->Message;

            // NO ERROR
            } else {
                $out['id'] = $data['customer']['id'];
            }
            */

            return $out;

        }


        /*
            Function: deleteCustomer
                Delete a customer.
        */

        function deleteCustomer($data) {

            // PAYMENT PROFILES FOR SERVICE ENABLED
            if ($this->Settings[$this->Service. '-payment-profiles']) {

                if ($this->Service == 'stripe') {
                    return $this->stripe_deleteCustomer($data);
                } else if ($this->Service == 'basys') {
                    return $this->basys_deleteCustomer($data);
                }

            }

        }

        /*
            Function: stripe_deleteCustomer
                Stripe interface for deleting a customer.
        */

        protected function stripe_deleteCustomer($data) {

            $out = array();

            // HAVE CUSTOMER ID
            if ($data['customer']['id']) {

                // PARAMS TO SEND TO STRIPE
                $params = array(
                    'endpoint' => 'customers/' .$data['customer']['id']
                );

                // PROCESS
                $response = $this->sendStripe($params, 'DELETE');

                // SUCCESSFUL
                if ($response['deleted']) {
                    $out['success'] = true;
                } else {
                    $out['error'] = $response['error']['message'];
                }

            // MISSING CUSTOMER ID / PROFILE ID
            } else {
                $out['error'] = 'Missing customer ID.';
            }

            return $out;

        }

        /*
            Function: basys_deleteCustomer
                Basys interface for deleting a customer.
        */

        protected function basys_deleteCustomer($data) {

            $out = array();

            $out['error'] = 'Basys does not support deleting a customer record.';

        }



        /*
            Function: createProfile
                Create a payment profile.
        */

        function createProfile($data) {

            // PAYMENT PROFILES FOR SERVICE ENABLED
            if ($this->Settings[$this->Service. '-payment-profiles']) {

                if ($this->Service == 'stripe') {
                    return $this->stripe_createProfile($data);
                } else if ($this->Service == 'basys') {
                    return $this->basys_createProfile($data);
                }

            }

        }


        /*
            Function: stripe_createProfile
                Stripe interface for creating a payment profile.
        */

        protected function stripe_createProfile($data) {

            $out = array();

            // HAVE CUSTOMER ID
            if ($data['customer']['id']) {

                // PARAMS TO SEND TO STRIPE
                $params = array(
                    'endpoint'          => 'customers/' .$data['customer']['id']. '/sources',
                    'source'            => array(
                        'object'        => 'card',
                        'name'          => ($data['customer']['name'] ? $data['customer']['name'] : $data['card']['name']),
                        'exp_month'     => date('m', strtotime($data['card']['expiration'])),
                        'exp_year'      => date('Y', strtotime($data['card']['expiration'])),
                        'number'        => $data['card']['number'],
                        'address_zip'   => $data['card']['zip'],
                        'cvc'           => $data['card']['cvv']
                    )
                );

                if (is_array($data['card']['metadata'])) {
                    $params['metadata'] = $data['card']['metadata'];
                }

                // PROCESS
                $response = $this->sendStripe($params);

                // SUCCESSFUL
                if ($response['id']) {
                    $out = $response;
                } else {
                    $out['error'] = $response['error']['message'];
                }

            // NO CUSTOMER ID
            } else {
                $out['error'] = 'Missing customer ID.';
            }

            return $out;

        }

        /*
            Function: basys_createProfile
                Basys interface for creating a payment profile.
        */

        protected function basys_createProfile($data) {

            $out = array();

            // HAVE CUSTOMER ID
            if ($data['customer']['id']) {

                $card_lastfour  = substr($data['card']['number'], -4);
                $card_exp       = date('m/y', strtotime($data['card']['expiration']));

                $params = array(
                    'Account'   => $this->DefaultParameters['Syn_Act'],
                    'Password'  => $this->DefaultParameters['Syn_Pwd'],
                    'RolodexID' => $data['customer']['id'],
                    'RolodexCreditCard' => array(
                        'Holder'        => ($data['customer']['name'] ? $data['customer']['name'] : $data['card']['name']),
                        'Number'        => $data['card']['number'],
                        'ExpDate'       => $card_exp,
                        'CVV'           => $data['card']['cvv'],
                        'Address'       => $data['customer']['address']['street'],
                        'City'          => $data['customer']['address']['city'],
                        'State'         => $data['customer']['address']['state'],
                        'ZIP'           => $data['card']['zip'],
                        'CardID'        => 0,
                        'Swiped'        => false,
                        'CVVType'       => 'Normal',
                        'NotPresent'    => false,
                        'Main'          => false
                    )
                );

                //echo print_r($params);

                // Send to Basys
                $response = $this->sendBasys('SaveRolodexCard', array($params));

                //echo print_r($response);
                //exit;

                // HAVE ERROR
                if ($response->SaveRolodexCardResult->Error) {
                    $out['error'] = $response->SaveRolodexCardResult->Message;

                // NO ERROR
                } else {

                    // GET CUSTOMER
                    $response = $this->getCustomer($data);

                    //echo print_r($response);
                    //exit;

                    // HAVE ERROR
                    if ($response->GetRolodexRecordResult->Error) {
                        $out['error'] = $response->GetRolodexRecordResult->Message;

                    // NO ERROR
                    } else {

                        // HAVE CREDIT CARD(S)
                        if (count($response->GetRolodexRecordResult->CreditCards) > 0) {

                            $cards = array();

                            // HAVE ONE
                            if (is_object($response->GetRolodexRecordResult->CreditCards->RolodexCreditCard)) {
                                $cards[] = $response->GetRolodexRecordResult->CreditCards->RolodexCreditCard;

                            // HAVE MULTIPLE
                            } else if (is_array($response->GetRolodexRecordResult->CreditCards->RolodexCreditCard)) {
                                $cards = $response->GetRolodexRecordResult->CreditCards->RolodexCreditCard;

                            }

                            //echo print_r($cards);

                            // HAVE CARDS
                            if (count($cards) > 0) {

                                // LOOP
                                foreach ($cards as $card) {

                                    //echo print_r($card);
                                    //echo substr($card->Number, -4). ' - ' .$card_lastfour. "\n";
                                    //echo $card->ExpDate. ' - ' .$card_exp;

                                    // MATCH - IS CARD
                                    if ((substr($card->Number, -4) == $card_lastfour) && ($card->ExpDate == $card_exp)) {

                                        $out['id']      = $card->CardID;
                                        $out['brand']   = strtolower($this->cardType($data['card']['number']));

                                    }

                                }

                            // NO CARDS
                            } else {
                                $out['error'] = 'No cards found.';
                            }

                        }

                    }

                }

            // NO CUSTOMER ID
            } else {
                $out['error'] = 'Missing customer ID.';
            }

            //echo print_r($out);
            //exit;

            return $out;

        }

        /*
            Function: updateProfile
                Update a payment profile.
        */

        function updateProfile($data) {

            // PAYMENT PROFILES FOR SERVICE ENABLED
            if ($this->Settings[$this->Service. '-payment-profiles']) {

                if ($this->Service == 'stripe') {
                    return $this->stripe_updateProfile($data);
                } else if ($this->Service == 'basys') {
                    return $this->basys_updateProfile($data);
                }

            }

        }


        /*
            Function: stripe_updateProfile
                Stripe interface for updating a payment profile.
        */

        protected function stripe_updateProfile($data) {

            $out = array();

            // HAVE CUSTOMER ID & PROFILE ID
            if (($data['customer']['id']) && ($data['profile']['id'])) {

                // PARAMS TO SEND TO STRIPE
                $params = array(
                    'endpoint'      => 'customers/' .$data['customer']['id']. '/sources/' .$data['profile']['id'],
                    'name'          => ($data['customer']['name'] ? $data['customer']['name'] : $data['card']['name']),
                    'exp_month'     => date('m', strtotime($data['card']['expiration'])),
                    'exp_year'      => date('Y', strtotime($data['card']['expiration'])),
                    'address_zip'   => $data['card']['zip']
                );

                if (is_array($data['card']['metadata'])) {
                    $params['metadata'] = $data['card']['metadata'];
                }

                // PROCESS
                $response = $this->sendStripe($params);

                // SUCCESSFUL
                if ($response['id']) {
                    $out['id'] = $response['id'];
                } else {
                    $out['error'] = $response['error']['message'];
                }

            // MISSING CUSTOMER ID / PROFILE ID
            } else {
                $out['error'] = 'Missing customer ID / profile ID.';
            }

            return $out;

        }

        /*
            Function: basys_updateProfile
                Basys interface for updating a payment profile.
        */

        protected function basys_updateProfile($data) {

            $out = array();

            // HAVE CUSTOMER ID & PROFILE ID
            if (($data['customer']['id']) && ($data['profile']['id'])) {

                $params = array(
                    'Account'   => $this->DefaultParameters['Syn_Act'],
                    'Password'  => $this->DefaultParameters['Syn_Pwd'],
                    'RolodexID' => $data['customer']['id'],
                    'RolodexCreditCard' => array(
                        'Holder'        => ($data['customer']['name'] ? $data['customer']['name'] : $data['card']['name']),
                        'Number'        => $data['card']['number'],
                        'ExpDate'       => date('m/y', strtotime($data['card']['expiration'])),
                        'CVV'           => $data['card']['cvv'],
                        'Address'       => $data['customer']['address']['street'],
                        'City'          => $data['customer']['address']['city'],
                        'State'         => $data['customer']['address']['state'],
                        'ZIP'           => $data['card']['zip'],
                        'CardID'        => $data['profile']['id'],
                        'Swiped'        => false,
                        'CVVType'       => 'Normal',
                        'NotPresent'    => false,
                        'Main'          => false
                    )
                );

                // Send to Basys
                $response = $this->sendBasys('SaveRolodexCard', array($params));

                //echo print_r($response);
                //exit;

                // HAVE ERROR
                if ($response->SaveRolodexCardResult->Error) {
                    $out['error'] = $response->SaveRolodexCardResult->Message;

                // NO ERROR
                } else {
                    $out['id'] = $data['customer']['id'];
                }

            // MISSING CUSTOMER ID / PROFILE ID
            } else {
                $out['error'] = 'Missing customer ID / profile ID.';
            }

            return $out;

        }


        /*
            Function: deleteProfile
                Delete a payment profile.
        */

        function deleteProfile($data) {

            // PAYMENT PROFILES FOR SERVICE ENABLED
            if ($this->Settings[$this->Service. '-payment-profiles']) {

                if ($this->Service == 'stripe') {
                    return $this->stripe_deleteProfile($data);
                } else if ($this->Service == 'basys') {
                    return $this->basys_deleteProfile($data);
                }

            }

        }


        /*
            Function: stripe_deleteProfile
                Stripe interface for deleting a payment profile.
        */

        protected function stripe_deleteProfile($data) {

            $out = array();

            // HAVE CUSTOMER ID & PROFILE ID
            if (($data['customer']['id']) && ($data['profile']['id'])) {

                // PARAMS TO SEND TO STRIPE
                $params = array(
                    'endpoint' => 'customers/' .$data['customer']['id']. '/sources/' .$data['profile']['id']
                );

                // PROCESS
                $response = $this->sendStripe($params, 'DELETE');

                // SUCCESSFUL
                if ($response['deleted']) {
                    $out['success'] = true;
                } else {
                    $out['error'] = $response['error']['message'];
                }

            // MISSING CUSTOMER ID / PROFILE ID
            } else {
                $out['error'] = 'Missing customer ID / profile ID.';
            }

            return $out;

        }

        /*
            Function: basys_deleteProfile
                Basys interface for deleting a payment profile.
        */

        protected function basys_deleteProfile($data) {

            $out = array();

            // HAVE CUSTOMER ID & PROFILE ID
            if (($data['customer']['id']) && ($data['profile']['id'])) {

                $params = array(
                    'Account'       => $this->DefaultParameters['Syn_Act'],
                    'Password'      => $this->DefaultParameters['Syn_Pwd'],
                    'RolodexCardID' => $data['profile']['id']
                );

                // Process Transaction
                $response = $this->sendBasys('DeleteRolodexCardByID', array($params));

                //echo print_r($response);
                //exit;

                // HAVE ERROR
                if ($response->DeleteRolodexCardByIDResult->Error) {
                    $out['error'] = $response->DeleteRolodexCardByIDResult->Message;

                // NO ERROR
                } else {
                    $out['success'] = true;
                }

            // MISSING CUSTOMER ID / PROFILE ID
            } else {
                $out['error'] = 'Missing customer ID / profile ID.';
            }

            return $out;

        }


        // HANDLE BASYS MESSAGE(S)
        public function basys_message($response=array()) {

            $out = '';

            if (($response->Proc_Resp == 'Declined') || ($response->Proc_Resp == 'Error')) {
                $out .= $response->Proc_Resp. ': ';
            }

            if ($response->Proc_Mess) {
                $out .= $response->Proc_Mess;
            }

            // CARD TYPE
            $card_type = strtolower($response->Card_Type);

            // VISA
            if ($card_type == 'visa') {

                switch ($response->AVS_Code) {

                    case 'A':
                        $avs = 'Address matches, ZIP code does not';
                        break;

                    /*
                    case 'B':
                        $avs = 'Address matches, ZIP not verified';
                        break;
                    */

                    case 'C':
                        $avs = 'Address and ZIP code not verified due to incompatible formats';
                        break;

                    /*
                    case 'D':
                        $avs = 'Address and ZIP code match (International)';
                        break;

                    case 'F':
                        $avs = 'Address and ZIP code match (U.K.)';
                        break;

                    case 'G':
                        $avs = 'Address not verified for International transaction (International only)';
                        break;

                    case 'I':
                        $avs = 'Address not verified (International)';
                        break;

                    case 'M':
                        $avs = 'Address and ZIP code match (International)';
                        break;
                    */

                    case 'N':
                        $avs = 'Neither the ZIP nor the address matches';
                        break;

                    /*
                    case 'P':
                        $avs = 'ZIP matches, address not verified';
                        break;

                    case 'R':
                        $avs = 'Issuer\'s authorization system is unavailable; try again later';
                        break;

                    case 'S':
                        $avs = 'AVS not supported at this time';
                        break;

                    case 'U':
                        $avs = 'Unable to perform address verification because either address information is unavailable or Issuer does not support AVS';
                        break;

                    case 'Y':
                        $avs = 'Address & 5-digit or 9-digit ZIP match';
                        break;

                    case 'Z':
                        $avs = 'Either 5-digit or 9-digit ZIP matches; address does not or is not included in request';
                        break;
                    */

                }


                /*


                */

            // MASTER CARD
            } else if ($card_type == 'master card') {

                switch ($response->AVS_Code) {

                    case 'A':
                        $avs = 'Address matches, ZIP code does not';
                        break;

                    case 'N':
                        $avs = 'Neither the ZIP nor the address matches';
                        break;

                    /*
                    case 'R':
                        $avs = 'Retry; system unable to process';
                        break;

                    case 'S':
                        $avs = 'AVS not supported at this time';
                        break;

                    case 'U':
                        $avs = 'No data from issuer/Authorization system';
                        break;
                    */

                    case 'W':
                        $avs = 'ZIP code matches, address does not';
                        break;

                    /*
                    case 'X':
                        $avs = 'Exact match on ZIP code and address';
                        break;

                    case 'Y':
                        $avs = 'Exact match on ZIP code and address';
                        break;
                    */

                    case 'Z':
                        $avs = 'ZIP code matches, address does not';
                        break;

                }

            // AMEX
            } else if ($card_type == 'american express') {

                switch ($response->AVS_Code) {

                    case 'A':
                        $avs = 'Address matches, ZIP code does not';
                        break;

                    case 'N':
                        $avs = 'Neither the ZIP nor the address matches';
                        break;

                    /*
                    case 'R':
                        $avs = 'System unavailable; retry';
                        break;

                    case 'S':
                        $avs = 'AVS not supported at this time';
                        break;

                    case 'U':
                        $avs = 'The necessary information is not available; account number is neither U.S. nor Canadian';
                        break;

                    case 'Y':
                        $avs = 'Address and ZIP code are both correct';
                        break;
                    */

                    case 'Z':
                        $avs = 'ZIP code matches, address does not';
                        break;

                }

            // DISCOVER
            } else if ($card_type == 'discover') {

                switch ($response->AVS_Code) {

                    /*
                    case 'A':
                        $avs = 'Address and ZIP code match';
                        break;
                    */

                    case 'G':
                        $avs = 'Neither the ZIP nor the address matches';
                        break;

                    /*
                    case 'S':
                        $avs = 'AVS not supported at this time';
                        break;
                    */

                    case 'T':
                        $avs = 'ZIP code matches, address does not';
                        break;

                    /*
                    case 'U':
                        $avs = 'Retry, system unable to process';
                        break;

                    case 'W':
                        $avs = 'No data from issuer/authorization';
                        break;

                    case 'X':
                        $avs = 'Address and ZIP code match';
                        break;
                    */

                    case 'Y':
                        $avs = 'Address matches, ZIP code does not';
                        break;

                    case 'Z':
                        $avs = 'ZIP code matches, address does not';
                        break;

                }

            }

            if ($avs) {
                $out .= ' (' .$avs. ')';
            }

            // VISA / MASTER CARD / DISCOVER
            if (($card_type == 'visa') || ($card_type == 'master card') || ($card_type == 'discover')) {

                switch ($response->CVV_Code) {

                    case 'M':
                        $cvv = 'Match';
                        break;

                    case 'N':
                        $cvv = 'No Match';
                        break;

                    case 'P':
                        $cvv = 'Not Processed';
                        break;

                    case 'S':
                        $cvv = 'CID should be on the Card, but the Merchant has indicated that it is not present';
                        break;

                    case 'U':
                        $cvv = 'Issuer unable to Process request';
                        break;

                }

            // AMEX
            } else if ($card_type == 'american express') {

                switch ($response->CVV_Code) {

                    case 'Y':
                        $cvv = 'Match';
                        break;

                    case 'N':
                        $cvv = 'No Match';
                        break;

                    case 'U':
                        $cvv = 'Issuer unable to Process request';
                        break;

                }

            }

            return $out;

        }



    }
