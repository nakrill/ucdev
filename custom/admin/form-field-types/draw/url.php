<?php

$id_rand = rand(11111,99999);

?>

<style type="text/css">

    #field_link_browser-<?php echo $id_rand; ?> {
        position: relative;
    }
    #field_link_browser-<?php echo $id_rand; ?> .expand-link {
        position: absolute;
        top: 7px;
        right: 10px;
        font-size: 1.6em;
    }
    #field_link_browser-<?php echo $id_rand; ?> .expand-content {
        display: none;
        padding: 15px;
        background-color: #f5f5f5;
    }
    #field_link_browser-<?php echo $id_rand; ?> .expand-content .search input[type="text"] {
        width: 845px;
    }
    .bigtree_dialog_window #field_link_browser-<?php echo $id_rand; ?> .expand-content .search input[type="text"] {
        width: 650px;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results {
        height: 116px;
        overflow-y: scroll;
        margin-top: 5px;
        border: 1px solid #ddd;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .loading, #field_link_browser-<?php echo $id_rand; ?> .results .no-results {
        padding: 10px;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items {
        display: none;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item {
        position: relative;
        display: block;
        overflow: hidden;
        padding: 8px 10px;
        background-color: #fff;
        border-bottom: 1px solid #f1f1f1;
        cursor: pointer;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item:nth-child(even) {
        background: #f9f9f9;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item:hover {
        background-color: #eaf2fa;
        color: #151515;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item .title {
        width: calc(100% - 68px);
        font-size: .92em;
        color: #333;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item .type {
        position: absolute;
        top: 0px;
        right: 0px;
        height: 100%;
        padding: 0 10px;
        background-color: #fff;
        float: right;
        font-size: .8em;
        line-height: 28px;
        text-transform: uppercase;
        color: #666;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item:nth-child(even) .type {
        background: #f9f9f9;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item:hover .type {
        background-color: #eaf2fa;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item.active {
        background-color: #eaf2fa;
        font-weight: bold;
    }
    #field_link_browser-<?php echo $id_rand; ?> .results .items .item.active .type {
        background-color: transparent;
    }

</style>

<script type="text/javascript">

    // DONE TYPING
    function lb_doSearch(options, callback) {

        $('#field_link_browser-<?php echo $id_rand; ?> .results .items').hide();
        $('#field_link_browser-<?php echo $id_rand; ?> .results .loading').show();

        $.get('/admin/ajax/wysiwyg/link_browser/get/', {
            q: $('#field_link_browser-<?php echo $id_rand; ?> .search input[type="text"]').val()
        }, function(data) {
            $('#field_link_browser-<?php echo $id_rand; ?> .results .loading').hide();
            $('#field_link_browser-<?php echo $id_rand; ?> .results .items').html(data).show();
            var curval = $('#field_link_browser-<?php echo $id_rand; ?> .text_input input[type="text"]').val();
            $('#field_link_browser-<?php echo $id_rand; ?> .results .items .item').each(function(i, v) {
                if ($(this).attr('href') == curval) {
                    $(this).addClass('active');
                }
            });
            if (typeof callback == 'function') {
                callback(data);
            }
        }, 'html');

    }

    $(document).ready(function() {

        var lb_typingTimer;

        $('#field_link_browser-<?php echo $id_rand; ?> .expand-link').click(function(e) {
            e.preventDefault();
            var $content = $('#field_link_browser-<?php echo $id_rand; ?> .expand-content');
            if ($content.is(':visible')) {
                $content.slideUp(500);
            } else {
                $content.slideDown(500);
                lb_doSearch();
            }
        });

        // ON KEYUP, START THE COUNTDOWN
        $('#field_link_browser-<?php echo $id_rand; ?> .search input[type="text"]').on('keyup', function () {
            clearTimeout(lb_typingTimer);
            lb_typingTimer = setTimeout(lb_doSearch, 600);
            $('#field_link_browser-<?php echo $id_rand; ?> .results .items').hide();
            $('#field_link_browser-<?php echo $id_rand; ?> .results .loading').show();
        });

        // ON KEYDOWN, CLEAR THE COUNTDOWN
        $('#field_link_browser-<?php echo $id_rand; ?> .search input[type="text"]').on('keydown', function () {
            clearTimeout(lb_typingTimer);
        });

        // ITEM CLICK
        $('#field_link_browser-<?php echo $id_rand; ?> .results .items').on('click', '.item', function(e) {
            e.preventDefault();
            $('#field_link_browser-<?php echo $id_rand; ?> .results .items .item').removeClass('active');
            $(this).addClass('active');
            var url = $(this).attr('href');
            $('#field_link_browser-<?php echo $id_rand; ?> .text_input input[type="text"]').val(url);
        });

    });

</script>

<div id="field_link_browser-<?php echo $id_rand; ?>">

    <?php

    /*

    When drawing a field type you are provided with the $field array with the following keys:
        "title" — The title given by the developer to draw as the label (drawn automatically)
        "subtitle" — The subtitle given by the developer to draw as the smaller part of the label (drawn automatically)
        "key" — The value you should use for the "name" attribute of your form field
        "value" — The existing value for this form field
        "id" — A unique ID you can assign to your form field for use in JavaScript
        "tabindex" — The current tab index you can use for the "tabindex" attribute of your form field
        "options" — An array of options provided by the developer
        "required" — A boolean value of whether this form field is required or not
    */

    include BigTree::path("admin/form-field-types/draw/text.php");

    ?>

    <a href="#" title="Select from existing pages" class="expand-link"><i class="fa fa-list" aria-hidden="true"></i></a>

    <div class="expand-content" style="display: none;">

        <div class="search">
            <input type="text" name="" value="" placeholder="Search" />
        </div>

        <div class="results">
            <div class="loading">Getting results..</div>
            <div class="items"></div>
        </div>

    </div>

</div>