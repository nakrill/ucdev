<?php

$width = isset($bigtree["html_editor_width"]) ? $bigtree["html_editor_width"] : false;
$height = isset($bigtree["html_editor_height"]) ? $bigtree["html_editor_height"] : false;
$content_css = $cms->getSetting("tinymce-content-css");
$html_editor = isset($bigtree["config"]["html_editor"]) ? $bigtree["config"]["html_editor"]["name"] : "TinyMCE 4";

// GET EXTERNAL FONTS
$ex_fonts = explode("\n", trim(strip_tags($cms->getSetting('fonts_external'))));

// HAVE EXTERNAL FONTS
if (count($ex_fonts) > 0) {
    foreach ($ex_fonts as $ex_font) {
        $ex_font = str_replace('http://', '//', str_replace('https://', '//', trim($ex_font)));
        list($ex_font_baseurl, $ex_font_sizes) = explode(':', $ex_font, 2);
        echo '<link type="text/css" rel="stylesheet" href="' .$ex_font. '">';
        if ($content_css) {
            $content_css .= ',' .$ex_font_baseurl;
        } else {
            $content_css .= $ex_font_baseurl;
        }
    }
}

$content_css = str_replace("\n", "", $content_css);

// DEFAULT TINYMCE FONT ARRAY
$mce_default_fonts = array(
    array(
        'name'  => 'Arial',
        'code'  => 'arial,sans-serif'
    ),
    array(
        'name'  => 'Book Antiqua',
        'code'  => 'book antiqua,serif'
    ),
    array(
        'name'  => 'Courier New',
        'code'  => 'courier new,serif'
    ),
    array(
        'name'  => 'Georgia',
        'code'  => 'georgia,serif'
    ),
    array(
        'name'  => 'Helvetica',
        'code'  => 'helvetica,sans-serif'
    ),
    array(
        'name'  => 'Tahoma',
        'code'  => 'tahoma,sans-serif'
    ),
    array(
        'name'  => 'Terminal',
        'code'  => 'terminal,serif'
    ),
    array(
        'name'  => 'Times New Roman',
        'code'  => 'times new roman,times,serif'
    ),
    array(
        'name'  => 'Trebuchet MS',
        'code'  => 'trebuchet ms,serif'
    ),
    array(
        'name'  => 'Verdana',
        'code'  => 'verdana,serif'
    )
);

?>
<script>
    $(document).ready(function() {
        <?php

        // FULL EDITOR
        if (count((array)$bigtree['html_fields'])) {

            // TINYMCE FONT LIST
            $mce_fonts = array();

            // HAVE DEFAULT FONTS
            if (count($mce_default_fonts) > 0) {
                foreach ($mce_default_fonts as $mce_font) {
                    $mce_fonts[] = "'" .$mce_font['name']. "=" .$mce_font['code']. ";'";
                }
            }

            // CUSTOM TINYMCE FONTS
            $mce_custom_fonts = explode("\n", trim(strip_tags($cms->getSetting('tinymce_fonts'))));

            // HAVE CUSTOM TINYMCE FONTS
            if (count($mce_custom_fonts) > 0) {
                foreach ($mce_custom_fonts as $mce_font) {
                    $mce_font = trim($mce_font);
                    if ($mce_font) {
                        $mce_fonts[] = "'" .$mce_font. ";'";
                    }
                }
            }

            // HAVE FONTS
            if (count($mce_fonts) > 0) {
                $mce_fonts = "font_formats: " .implode(' + ', $mce_fonts). ",";
            }

            ?>
            tinyMCE.init({
                <?php if ($content_css) { echo 'content_css: "' .$content_css. '",' . "\n"; } ?>
                theme: "modern",
                mode: "exact",
                elements: "<?php echo implode(',', $bigtree['html_fields'])?>",
                <?php if ($bigtree['path'][0] == 'admin') { ?>
                    file_browser_callback: BigTreeFileManager.tinyMCEOpen,
                <?php } ?>
                menubar: false,
                branding: false,
                plugins: "code,anchor,image,link,table,visualblocks,lists,hr,paste",
                toolbar: "undo redo | styleselect | fontselect | bold italic underline | bullist numlist outdent indent | hr anchor link unlink image table | paste<?php if ($bigtree['path'][0] == 'admin') { ?> code<?php } ?>",
                paste_remove_spans: true,
                paste_remove_styles: true,
                paste_strip_class_attributes: true,
                paste_auto_cleanup_on_paste: true,
                relative_urls: false,
                remove_script_host: true,
                gecko_spellcheck: true,
                <?php echo $mce_fonts; ?>
                extended_valid_elements : "*[*]"
                <?php if ($width) { ?>,width: "<?php echo $width; ?>"<?php } ?>
                <?php if ($height) { ?>,height: "<?php echo $height; ?>"<?php } ?>
            });
            <?php

        }

        // SIMPLE EDITOR
        if (count((array)$bigtree["simple_html_fields"])) {

            ?>
            tinyMCE.init({
                <?php if ($content_css) { echo 'content_css: "' .$content_css. '",' . "\n"; } ?>
                theme: "modern",
                mode: "exact",
                elements: "<?php echo implode(',', $bigtree['simple_html_fields']); ?>",
                <?php if ($bigtree['path'][0] == 'admin') { ?>
                    file_browser_callback: BigTreeFileManager.tinyMCEOpen,
                <?php } ?>
                menubar: false,
                branding: false,
                plugins: "link,code,visualblocks,lists",
                toolbar: "bold italic underline alignleft aligncenter alignright bullist numlist link unlink paste<?php if ($bigtree['path'][0] == 'admin') { ?> code<?php } ?>",
                paste_remove_spans: true,
                paste_remove_styles: true,
                paste_strip_class_attributes: true,
                paste_auto_cleanup_on_paste: true,
                gecko_spellcheck: true,
                relative_urls: false,
                remove_script_host: true,
                extended_valid_elements : "*[*]"
                <?php if ($width) { ?>,width: "<?php echo $width; ?>"<?php } ?>
                <?php if ($height) { ?>,height: "<?php echo $height; ?>"<?php } ?>
            });
            <?

        }

        ?>
    });
</script>