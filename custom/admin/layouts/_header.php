<?php

// EXTENSIONS DIRECTORY
$extensions_dir = SERVER_ROOT. 'extensions';

// GET EXTENSIONS
$extensions = array_diff(scandir($extensions_dir), array('..', '.'));

/*
$nav = isset($bigtree["nav_override"]) ? $bigtree["nav_override"] : array(
    array("link" => "dashboard", "title" => "Dashboard", "access" => 0, "children" => array(
        array("link" => "", "title" => "Overview", "access" => 0),
        array("link" => "pending-changes", "title" => "Pending Changes", "access" => 0),
        array("link" => "messages", "title" => "Message Center", "access" => 0),
        array("link" => "vitals-statistics", "title" => "Vitals &amp; Statistics", "access" => 1)
    )),
    array("link" => "pages", "title" => "Pages", "access" => 0),
    array("link" => "modules", "title" => "Modules", "access" => 0),
    array("link" => "users", "title" => "Users", "access" => 1),
    array("link" => "settings", "title" => "Settings", "access" => 1),
    array("link" => "developer", "title" => "Developer", "access" => 2, "children" => array(
        array("link" => "", "title" => "Create", "access" => 2, "group" => true, "children" => array(
            array("link" => "developer/templates", "title" => "Templates", "access" => 2),
            array("link" => "developer/modules", "title" => "Modules", "access" => 2),
            array("link" => "developer/callouts", "title" => "Callouts", "access" => 2),
            array("link" => "developer/field-types", "title" => "Field Types", "access" => 2),
            array("link" => "developer/feeds", "title" => "Feeds", "access" => 2),
            array("link" => "developer/settings", "title" => "Settings", "access" => 2),
            array("link" => "developer/extensions", "title" => "Extensions &amp; Packages", "access" => 2),
        )),
        array("link" => "", "title" => "Configure", "access" => 2, "group" => true, "children" => array(
            array("link" => "developer/cloud-storage", "title" => "Cloud Storage", "access" => 2),
            array("link" => "developer/payment-gateway", "title" => "Payment Gateway", "access" => 2),
            array("link" => "dashboard/vitals-statistics/analytics/configure/", "title" => "Analytics", "access" => 1),
            array("link" => "developer/geocoding", "title" => "Geocoding", "access" => 2),
            array("link" => "developer/email", "title" => "Email Delivery", "access" => 2),
            array("link" => "developer/services", "title" => "Service APIs", "access" => 2),
            array("link" => "developer/media", "title" => "Media", "access" => 2),
            array("link" => "developer/security", "title" => "Security", "access" => 2)
        ))
    ))
);
*/

$nav = [
    ["link" => "dashboard", "title" => "Dashboard", "access" => 0],
    ["link" => "pages", "title" => "Pages", "access" => 0],
];
//if ($_uccms['_account']->isEnabled()) {
if ($cms->getSetting('uccms_accounts')[0]['enabled'] == 'on') {
    array_push($nav, ["link" => "accounts", "title" => "Accounts", "access" => 1]);
}

// LOOP THROUGH EXTENSIONS
foreach ((array)$extensions as $extension) {
    @include($extensions_dir. '/' .$extension. '/modules/dashboard/nav.php');
}

array_push($nav,
    ["link" => "reports", "title" => "Reports", "access" => 1, "children" => [
        ["link" => "seo", "title" => "SEO", "access" => 1],
    ]],
    ["link" => "settings", "title" => "Settings", "access" => 1, "children" => [
        ["link" => "settings", "title" => "General", "access" => 1],
        ["link" => "../users", "title" => "Users", "access" => 1],
    ]],
    ["link" => "developer", "title" => "Developer", "access" => 2, "children" => [
        ["link" => "", "title" => "Create", "access" => 2, "group" => true, "children" => [
            ["link" => "developer/templates", "title" => "Templates", "access" => 2],
            ["link" => "developer/modules", "title" => "Modules", "access" => 2],
            ["link" => "developer/callouts", "title" => "Callouts", "access" => 2],
            ["link" => "developer/field-types", "title" => "Field Types", "access" => 2],
            ["link" => "developer/feeds", "title" => "Feeds", "access" => 2],
            ["link" => "developer/settings", "title" => "Settings", "access" => 2],
            ["link" => "developer/extensions", "title" => "Extensions &amp; Packages", "access" => 2],
        ]],
        ["link" => "", "title" => "Configure", "access" => 2, "group" => true, "children" => [
            ["link" => "developer/cloud-storage", "title" => "Cloud Storage", "access" => 2],
            ["link" => "developer/payment-gateway", "title" => "Payment Gateway", "access" => 2],
            ["link" => "dashboard/vitals-statistics/analytics/configure/", "title" => "Analytics", "access" => 1],
            ["link" => "developer/geocoding", "title" => "Geocoding", "access" => 2],
            ["link" => "developer/email", "title" => "Email Delivery", "access" => 2],
            ["link" => "developer/services", "title" => "Service APIs", "access" => 2],
            ["link" => "developer/media", "title" => "Media", "access" => 2],
            ["link" => "developer/security", "title" => "Security", "access" => 2]
        ]]
    ]]
);

$unread_messages = $admin->getUnreadMessageCount();
$site = $cms->getPage(0,false);

// Show an alert for being on the development site of a live site, in maintenance mode, or in developer mode
$environment_alert = false;
if (!empty($bigtree["config"]["maintenance_url"])) {
    $environment_alert = '<span><strong>Maintenance Mode</strong> &middot; Entire Site Restricted To Developers</span>';
} elseif (!empty($bigtree["config"]["developer_mode"])) {
    $environment_alert = '<span><strong>Developer Mode</strong> &middot; Admin Area Restricted To Developers</span>';
} elseif ($bigtree["config"]["environment"] == "dev" && $bigtree["config"]["environment_live_url"]) {
    $environment_alert = '<span><strong>Development Site</strong> &middot; Changes Will Not Affect Live Site!</span><a href="'.$bigtree["config"]["environment_live_url"].'">Go Live</a>';
}

if ($bigtree['path'][1] == 'pages') $bigtree['admin_title'] = 'Page: ' .$bigtree['admin_title'];

?>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="ie ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><? if (isset($bigtree["admin_title"])) { ?><?=BigTree::safeEncode($bigtree["admin_title"])?> | <? } ?><?=$cms->getSetting('site_name')?> Admin</title>
        <link rel="stylesheet" href="<?=ADMIN_ROOT?>css/main.css" type="text/css" media="screen" />
        <?
            // Configuration based CSS
            if (isset($bigtree["config"]["admin_css"]) && is_array($bigtree["config"]["admin_css"])) {
                foreach ($bigtree["config"]["admin_css"] as $style) {
                    if (!strstr($style, '//')) $style = ADMIN_ROOT. 'css/' .$style; // UCCMS - Files from URL
                    ?>
                    <link rel="stylesheet" href="<?=$style?>" type="text/css" media="screen" />
                    <?
                }
            }

            // Runtime based CSS
            if (isset($bigtree["css"]) && is_array($bigtree["css"])) {
                $bigtree["css"] = array_unique($bigtree["css"]);
                foreach ($bigtree["css"] as $style) {
                    $css_path = explode("/",$style);

                    // This is an extension piece acknowledging it could be used outside the extension root
                    if ($css_path[0] == "*") {
                        $include_path = ADMIN_ROOT.$style;
                    // This is an extension inside its routed directory loading its own styles
                    } elseif (defined("EXTENSION_ROOT")) {
                        $include_path = ADMIN_ROOT."*/".$bigtree["module"]["extension"]."/css/".$style;
                    // This is just a regular old include
                    } else {
                        $include_path = ADMIN_ROOT."css/".$style;
                    }
                    ?>
                    <link rel="stylesheet" href="<?=$include_path?>" type="text/css" media="screen" />
                    <?
                }
            }
        ?>
        <style type="text/css">

            /************ START: For Ivan ************/

            a.btn:visited {
                color: #fff;
            }

            #content > .navbar {
                position: fixed;
                width: calc(100% - 250px);
                z-index: 10;
                transition: all 0.3s;
            }
            #content.active > .navbar {
                width: 100%;
                margin: 0 -5px;
            }
            #content > .padding {
                padding-top: 60px;
            }

            #page {
                z-index: auto;
            }

            .table > .summary nav.view_paging a:visited {
                color: #999;
            }
            .table > .summary nav.view_paging a.active {
                color: #00C2B8;
            }

            .tooltip .arrow {
                background: transparent;
            }


            /************ END: For Ivan ************/

        </style>
        <script>
            var CSRFTokenField = "<?=$admin->CSRFTokenField?>";
            var CSRFToken = "<?=$admin->CSRFToken?>";
        </script>
        <script src="<?=ADMIN_ROOT?>js/lib.js"></script>
        <script src="<?=ADMIN_ROOT?>js/main.js"></script>
        <script>BigTree.dateFormat = "<?=BigTree::phpDateTojQuery($bigtree["config"]["date_format"])?>";</script>
        <script src="<?=ADMIN_ROOT?>js/<?=isset($bigtree["config"]["html_editor"]) ? $bigtree["config"]["html_editor"]["src"] : "tinymce3/tiny_mce.js"?>"></script>
        <?

        // Configuration based JS
        if (isset($bigtree["config"]["admin_js"]) && is_array($bigtree["config"]["admin_js"])) {
            foreach ($bigtree["config"]["admin_js"] as $script) {
                if (!strstr($script, '//')) $script = ADMIN_ROOT. 'js/' .$script; // UCCMS - Files from URL
                ?>
                <script src="<?=$script?>"></script>
                <?
            }
        }

        // Runtime based JS
        if (isset($bigtree["js"]) && is_array($bigtree["js"])) {
            $bigtree["js"] = array_unique($bigtree["js"]);
            foreach ($bigtree["js"] as $script) {

                $js_path = explode("/",$script);

                // This is an extension piece acknowledging it could be used outside the extension root
                if ($js_path[0] == "*") {
                    $include_path = ADMIN_ROOT.$script;
                // This is an extension inside its routed directory loading its own scripts
                } elseif (defined("EXTENSION_ROOT")) {
                    $include_path = ADMIN_ROOT."*/".$bigtree["module"]["extension"]."/js/".$script;
                // This is just a regular old include
                } else {
                    $include_path = ADMIN_ROOT."js/".$script;
                }
            }
            ?>
            <script src="<?=$include_path?>"></script>
            <?
        }
        ?>
        <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body class="bigtree">
        <script>
            if ($.browser.name == "msie" && $.browser.versionNumber > 11) {
                $("body").addClass("browser_msedge browser_msedge_" + $.browser.versionNumber);
            } else {
                $("body").addClass("browser_" + $.browser.name).addClass("browser_" + $.browser.name + "_" + $.browser.versionNumber);
            }
        </script>
        <? /*
        <header class="main">
            <section>
                <a href="<? if ($bigtree["config"]["force_secure_login"]) { echo str_replace("http://","https://",ADMIN_ROOT); } else { echo ADMIN_ROOT; } ?>login/logout/?true<? $admin->drawCSRFTokenGET() ?>" class="logout"><span></span>Logout</a>
                <div></div>
                <p class="messages"><a href="<?=ADMIN_ROOT?>dashboard/messages/"><?=$unread_messages?> Unread Messages</a></p>
                <div></div>
                <p class="welcome"><span class="gravatar"><img src="<?=BigTree::gravatar($admin->User, 28)?>" alt="" /></span>Welcome Back <a href="<?=ADMIN_ROOT?>users/profile/"><?=$admin->Name?></a></p>
                <strong><?=$site["nav_title"]?></strong>
                <a href="<?=WWW_ROOT?>" target="_blank" class="view_site">View Site</a>
            </section>
        </header>
        <nav class="main">
            <section>
                <ul>
                    <?
                        $x = -1;
                        foreach ($nav as $item) {
                            if ($admin->Level >= $item["access"] && (!$admin->HidePages || $item["link"] != "pages")) {
                                $x++;
                                // Need to check custom nav states better
                                $link_pieces = explode("/",$item["link"]);
                                $path_pieces = array_slice($bigtree["path"],1,count($link_pieces));
                    ?>
                    <li>
                        <a href="<?=ADMIN_ROOT?><?=$item["link"]?>/"<? if ($link_pieces == $path_pieces || ($item["link"] == "modules" && isset($bigtree["module"]))) { $bigtree["active_nav_item"] = $x; ?> class="active"<? } ?>><span class="<?=$cms->urlify($item["title"])?>"></span><?=$item["title"]?></a>
                        <? if (isset($item["children"]) && count($item["children"])) { ?>
                        <ul>
                            <?
                                foreach ($item["children"] as $child) {
                                    if ($admin->Level >= $child["access"]) {
                                        if (isset($child["group"]) && count($child["children"])) {
                            ?>
                            <li class="grouper"><?=$child["title"]?></li>
                            <?
                                            foreach ($child["children"] as $c) {
                            ?>
                            <li><a href="<?=ADMIN_ROOT?><?=$c["link"]?>/"><?=$c["title"]?></a></li>
                            <?
                                            }
                                        } elseif (!isset($child["group"])) {
                            ?>
                            <li><a href="<?=ADMIN_ROOT?><?=$item["link"]?>/<?=$child["link"]?>/"><?=$child["title"]?></a></li>
                            <?
                                        }
                                    }
                                }
                            ?>
                        </ul>
                        <? } ?>
                    </li>
                    <?
                            }
                        }
                    ?>
                </ul>
                <form method="get" action="<?=ADMIN_ROOT?>search/">
                    <input type="submit" class="qs_image" alt="Search" />
                    <input type="search" name="query" autocomplete="off" placeholder="Quick Search" class="qs_query" />
                    <div id="quick_search_results" style="display: none;"></div>
                </form>
            </section>
        </nav>
        */ ?>

        <? /*
        <aside id="growl" class=""><article><a class="close" href="#"></a><span class="icon_growl_success"></span><section><h3>Error</h3><p>Inventory not enabled.</p></section></article></aside>
        */ ?>

        <div id="uccmsDashboard" class="body">
            <div class="wrapper">

                <? if ($environment_alert) { ?>

                <div class="environment_alert">
                    <?=$environment_alert?>
                </div>
                <? } ?>

                <aside id="growl"></aside>

                <nav id="sidebar">

                    <div class="sidebar-header">
                        <h3 title="<?php echo $cms->getSetting('site_name'); ?>"><span><?php echo $cms->getSetting('site_name'); ?></span></h3>
                    </div>

                    <ul class="list-unstyled components">

                        <?
                        $x = -1;
                        foreach ($nav as $item) {
                            if ($admin->Level >= $item["access"] && (!$admin->HidePages || $item["link"] != "pages")) {
                                $x++;

                                $link_pieces = explode("/",$item["link"]);
                                $path_pieces = array_slice($bigtree["path"],1,count($link_pieces));

                                ?>
                                <li<? if ($link_pieces == $path_pieces || ($item["link"] == "modules" && isset($bigtree["module"]))) { $bigtree["active_nav_item"] = $x; ?> class="active"<? } ?>>
                                    <? if (isset($item["children"]) && count($item["children"])) { ?>
                                        <a href="#sidebarNav-<?php echo $x; ?>" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle<?php if ($x != $bigtree['active_nav_item']) { echo ' collapsed'; } ?>"><span class="<?=$cms->urlify($item["title"])?>"></span><?=$item["title"]?></a>
                                        <ul id="sidebarNav-<?php echo $x; ?>" class="collapse list-unstyled <?php if ($x == $bigtree['active_nav_item']) { echo 'show'; } ?>" data-parent="#sidebar ul.components">
                                            <?
                                            foreach ($item["children"] as $child) {
                                                if ($admin->Level >= $child["access"]) {
                                                    if (isset($child["group"]) && count($child["children"])) {
                                                        ?>
                                                        <li class="grouper"><?=$child["title"]?></li>
                                                        <?
                                                        foreach ($child["children"] as $c) {
                                                            ?>
                                                            <li><a href="<?=ADMIN_ROOT?><?=$c["link"]?>/"><?=$c["title"]?></a></li>
                                                            <?
                                                        }
                                                    } elseif (!isset($child["group"])) {
                                                        $li_active = false;
                                                        if ($x == $bigtree['active_nav_item']) {
                                                            if ($child['link'] == '.') {
                                                                if (implode('/', $bigtree['path']) == 'admin/' .$item['link']) {
                                                                    $li_active = true;
                                                                }
                                                            } else {
                                                                if (stristr(implode('/', $bigtree['path']), 'admin/' .$item['link'] . '/' .$child['link'])) {
                                                                    $li_active = true;
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <li<?php if ($li_active) { ?> class="active"<?php } ?>><a href="<?=ADMIN_ROOT?><?=$item["link"]?>/<?=$child["link"]?>/"><?=$child["title"]?></a></li>
                                                        <?
                                                    }
                                                }
                                            }
                                            ?>
                                        </ul>
                                    <?php } else { ?>
                                        <a href="<?php echo ADMIN_ROOT.$item["link"]; ?>/"><span class="<?=$cms->urlify($item["title"])?>"></span><?=$item["title"]?></a>
                                    <?php } ?>
                                </li>
                                <?
                            }
                        }
                        ?>

                    </ul>

                    <? /*
                    <ul class="list-unstyled CTAs">
                        <li>
                            <a href="#" class="download">New Order</a>
                        </li>
                    </ul>
                    */ ?>

                </nav>

                <div id="content">

                    <nav class="navbar navbar-expand-lg <? /*navbar-light*/ ?> <? /*bg-light*/ ?>">
                        <div class="container-fluid">

                            <a id="sidebarCollapse" href="#">
                                <i class="fas fa-bars"></i>
                            </a>

                            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fas fa-bars"></i>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="nav navbar-nav ml-auto">
                                    <li class="view-site nav-item">
                                        <a href="/" target="_blank" class="nav-link">View site</a>
                                    </li>
                                    <li class="messages nav-item">
                                        <a class="nav-link" href="<?php echo ADMIN_ROOT; ?>dashboard/messages/" title="<?php echo (int)$unread_messages; ?> unread messages">
                                            <?php if ($unread_messages) { ?>
                                                <span class="num-unread"><?php echo $unread_messages; ?></span>
                                            <?php } ?>
                                            <i class="far fa-envelope"></i>
                                        </a>
                                    </li>
                                    <li class="account nav-item dropdown">
                                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="gravatar"><img src="<?=BigTree::gravatar($admin->User, 28)?>" alt="" /></span><?php echo $admin->Name; ?> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo ADMIN_ROOT; ?>users/profile/" class="nav-link">Account</a></li>
                                            <li><a href="<? if ($bigtree["config"]["force_secure_login"]) { echo str_replace("http://","https://",ADMIN_ROOT); } else { echo ADMIN_ROOT; } ?>login/logout/?true<? $admin->drawCSRFTokenGET() ?>" class="nav-link logout"><span></span>Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <div class="padding">
