                    </div>

                </div>

            </div>
        </div>
        <footer class="main">
            <section>
                <article class="bigtree">
                    <a href="http://www.umbrellaconsultants.com/" target="_blank" class="logo" style="background-image: none;"><img src="<?=ADMIN_ROOT?>images/uccms_logo_large-m.png" alt="Umbrella CMS" /></a>
                </article>
                <article class="fastspot">
                    <p>Version <?=BIGTREE_VERSION?></p>
                    <p>&copy; <?=date("Y")?> Umbrella Consultants</p>
                </article>
            </section>
        </footer>

        <?
            if (isset($_SESSION["bigtree_admin"]["growl"])) {
        ?>
        <script>BigTree.growl("<?=BigTree::safeEncode($_SESSION["bigtree_admin"]["growl"]["title"])?>","<?=BigTree::safeEncode($_SESSION["bigtree_admin"]["growl"]["message"])?>",5000,"<?=htmlspecialchars($_SESSION["bigtree_admin"]["growl"]["type"])?>");</script>
        <?
                unset($_SESSION["bigtree_admin"]["growl"]);
            }
        ?>

        <?php

        // HAS ECOMM EXTENSION
        if (class_exists('uccms_Ecommerce')) {

            if (!$_uccms_ecomm) $_uccms_ecomm = new uccms_Ecommerce();

            // HAS ACCESS
            if ($_uccms_ecomm->adminModulePermission()) {

                // ADMIN FOOTER CART ENABLED
                if ($_uccms_ecomm->getSetting('admin_footer_cart_enabled')) {
                    include(SERVER_ROOT. 'extensions/' .$_uccms_ecomm->Extension. '/modules/admin/footer-cart/element.php');
                }

            }

        }

        ?>

        <?php echo revsocial_webTrackingCode(); ?>

     </body>
</html>