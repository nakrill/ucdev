<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

?>

<h1>
    <span class="page_icon users"></span>
    <a class="first" href="/admin/accounts/">Frontend Accounts</a>
    <span class="divider">›</span>
    <a class="last" href="/admin/accounts/">View Accounts</a>
</h1>

<nav id="sub_nav">
    <a class="active" href="/admin/accounts/"><span class="icon_small icon_small_list"></span>View Accounts</a>
    <a href="/admin/accounts/edit/"><span class="icon_small icon_small_add"></span>Add Account</a>
    <menu style="display: none;">
        <span class="icon"></span>
        <div></div>
    </menu>
</nav>

<style type="text/css">

    .table .users_created {
        text-align: left;
        width: 240px;
    }

</style>

<script>
    // Placing this here inline because we want the menu rendering changed on page render if it's too large.
    var width = 0;
    var menu = $("#sub_nav menu div");
    var extras = $("<div>");
    $("#sub_nav > a").each(function() {
        var iwidth = $(this).width() + 29;
        if (width + iwidth > 910) {
            extras.append($(this));
        }
        width += iwidth;
    });
    if (width > 910) {
        $("#sub_nav menu").show().find("div").prepend(extras.html());
    }
</script>

<div class="table">
    <summary>
        <input type="search" autocomplete="off" class="form_search" placeholder="Search" id="query" name="query">
        <span class="form_search_icon"></span>
        <nav id="view_paging" class="view_paging"></nav>
    </summary>
    <header>
        <span class="view_column users_name"><a name="name" href="ASC" class="sort_column">Name <em></em></a></span>
        <span class="view_column users_email"><a name="email" href="ASC" class="sort_column desc">Email <em></em></a></span>
        <span class="view_column users_created"><a name="created" href="DESC" class="sort_column">Created <em>▼</em></a></span>
        <span style="width: 80px;" class="view_action">Actions</span>
    </header>
    <ul id="results" class="items">
        <? include(SERVER_ROOT. 'custom/admin/ajax/accounts/get-page.php'); ?>
    </ul>
</div>

<script>

    BigTree.localSortColumn = "created";
    BigTree.localSortDirection = "DESC";
    BigTree.localSearchTimer = false;
    BigTree.localSearch = function() {
        $("#results").load("<?=ADMIN_ROOT?>ajax/accounts/get-page/?sort=" + escape(BigTree.localSortColumn) + "&sort_direction=" + escape(BigTree.localSortDirection) + "&page=1&query=" + escape($("#query").val()));
    }

    $("#query").keyup(function() {
        if (BigTree.localSearchTimer) {
            clearTimeout(BigTree.localSearchTimer);
        }
        BigTree.localSearchTimer = setTimeout("BigTree.localSearch()",400);
    });

    $(".table").on("click",".icon_delete",function() {
        if ($(this).hasClass("disabled_icon")) {
            return false;
        }
        BigTreeDialog({
            title: "Delete Account",
            content: '<p class="confirm">Are you sure you want to delete this account?</p>',
            icon: "delete",
            alternateSaveText: "OK",
            callback: $.proxy(function() {
                $.ajax("<?=ADMIN_ROOT?>ajax/accounts/delete/", { type: "POST", data: { id: $(this).attr("href").substr(1) } });
            },this)
        });
        return false;
    }).on("click",".sort_column",function() {
        BigTree.localSortDirection = BigTree.cleanHref($(this).attr("href"));
        BigTree.localSortColumn = $(this).attr("name");
        if ($(this).hasClass("asc") || $(this).hasClass("desc")) {
            $(this).toggleClass("asc").toggleClass("desc");
            if (BigTree.localSortDirection == "DESC") {
                $(this).attr("href","ASC");
                BigTree.localSortDirection = "ASC";
                   $(this).find("em").html("&#9650;");
            } else {
                $(this).attr("href","DESC");
                BigTree.localSortDirection = "DESC";
                   $(this).find("em").html("&#9660;");
            }
        } else {
            if (BigTree.localSortDirection == "ASC") {
                var dchar = "&#9650;";
            } else {
                var dchar = "&#9660;";
            }
            $(this).parents("header").find(".sort_column").removeClass("asc").removeClass("desc").find("em").html("");
            $(this).addClass(BigTree.localSortDirection.toLowerCase()).find("em").html(dchar);
        }
        $("#results").load("<?=ADMIN_ROOT?>ajax/accounts/get-page/?sort=" + escape(BigTree.localSortColumn) + "&sort_direction=" + escape(BigTree.localSortDirection) + "&page=1&query=" + escape($("#query").val()));
        return false;
    }).on("click","#view_paging a",function() {
        if ($(this).hasClass("active") || $(this).hasClass("disabled")) {
            return false;
        }
        $("#results").load("<?=ADMIN_ROOT?>ajax/accounts/get-page/?sort=" + escape(BigTree.localSortColumn) + "&sort_direction=" + escape(BigTree.localSortDirection) + "&page=" + BigTree.cleanHref($(this).attr("href")) + "&query=" + escape($("#query").val()));
        return false;
    });
</script>