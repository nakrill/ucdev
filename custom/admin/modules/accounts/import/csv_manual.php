<?php

echo 'un-exit';
exit;

//echo 'Memory Limit: ' .ini_get('memory_limit'). "\n";
/*
    - If limit is too low, edit /site/php.ini and set memory_limit = 512M
    - Running from command line: php -d memory_limit=512M manual.php
*/

// CONTENT TO FIND
$content_replace_find = array(
);

// CONTENT TO REPLACE
$content_replace_replace = array(
);

##########################################################

// BEING RUN FROM COMMAND LINE OR NOT
$cli = (php_sapi_name() == 'cli' ? true : false);

// BEING RUN FROM COMMAND LINE
if ($cli) {

    // REQUIRE BIGTREE
    $server_root = str_replace('custom/admin/modules/accounts/import/csv_manual.php', '', strtr(__FILE__, "\\", "/"));
    require($server_root. 'custom/environment.php');
    require($server_root. 'custom/settings.php');
    require($server_root. 'core/bootstrap.php');

}

// READ FILE
if (($handle = fopen(dirname(__FILE__). '/data.csv', "r")) !== FALSE) {

    $heading = array();
    $failed = array();

    $num_added = 0;
    $num_updated = 0;
    $num_failed = 0;

    $i = 0;

    // LOOP
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

        // FIRST RECORD (HEADING)
        if ($i == 0) {

            $data[] = 'Import Note';

            $heading = $data;

            $i++;

            continue;

        }

        $i++;

        // EMAIL ADDRESS
        $email = trim($data[2]);

        // IS VALID
        if ($_uccms['_account']->validEmail($email)) {

            // ACCOUNT INFO
            $account = array(
                'username'  => $email,
                'email'     => $email
            );

            // NO PASSWORD SPECIFIED
            if (!$data[3]) {

                // GENERATE PASSWORD
                $data[3] = $_uccms['_account']->rand_string(10, true);

            }

            // ENCRYPT
            $randomSalt = $_uccms['_account']->rand_string(20);
            $saltedPass = hash('sha256', trim($data[3]) . $_uccms['_account']->config['keys']['salt'] . $randomSalt);

            // ACCOUNT DETAILS
            $account_details = array(
                'firstname'         => $data[1],
                'lastname'          => $data[0],
                'password'          => $saltedPass,
                'password_salt'     => $randomSalt
            );

            /*
            echo print_r($account);
            echo '<br />';
            echo print_r($account_details);
            */

            // GET ACCOUNT ID IF ACCOUNT EXISTS
            $account_id = $_uccms['_account']->userExists(array('username' => $email, 'email' => $email));

            // ACCOUNT EXISTS
            if ($account_id) {

                // UPDATE MAIN
                $_uccms['_account']->updateAccount($account_id, $account);

                // UPDATE DETAILS
                $_uccms['_account']->updateAccount($account_id, $account_details, true);

                $num_updated++;

            // ACCOUNT DOES NOT EXIST
            } else {

                // REGISTER
                $result = $_uccms['_account']->register($account['username'], $data[3], $account['email'], $account_details, false);

                // SUCCESS
                if ($result['success']) {

                    $num_added++;

                // FAILED
                } else {

                    $data[] = $result['message'];

                    $failed[] = $data;

                }

            }

        // EMAIL IS NOT VALID
        } else {

            $data[] = 'Valid email address required.';

            $failed[] = $data;

        }

        //echo ($cli ? "\n" : '<br />');

    }

    fclose($handle);

    // HAVE FAILED
    if (count($failed) > 0) {

        $ca = array();
        $ca[] = $heading;
        foreach ($failed as $fail) {
            $ca[] = $fail;
        }

        $failed_file = 'missing_emails-' .time(). '.csv';

        // SAVE
        file_put_contents(dirname(__FILE__). '/' .$failed_file, array_to_csv($ca));

        echo count($failed). ' failed. Saved to ' .$failed_file;
        echo ($cli ? "\n" : '<br />');

    }

// FAILED TO READ FILE
} else {
    echo 'Failed to open data.csv';
}

echo ($cli ? "\n" : '<br />');
echo '<b>All done.</b> Added: ' .$num_added. ' Updated: ' .$num_updated;

?>