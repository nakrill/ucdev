<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// ACCOUNT CLASS - UPDATE CONFIG
$_uccms['_account']->setConfig(array(
    'admin' => array(
        'from' => true
    ))
);

//echo print_r($_POST);
//exit;

// CLEAN UP
$id = (int)$_REQUEST['id'];

// SUBMITTED
if (is_array($_POST)) {

    // SAVE VALUES TO SESSION
    unset($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post']);
    $_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post'] = $_POST;

    // REQUIRED FIELDS
    $reqfa[] = 'email';

    // NEW ACCOUNT
    if (!$id) {
        $reqfa[] = 'password';
    }

    // CHECK REQUIRED FIELDS
    foreach ($reqfa as $req) {
        if (!$_POST[$req]) {
            $admin->growl('Frontend Accounts', 'One or more required fields missing.');
            BigTree::redirect(ADMIN_ROOT. 'accounts/edit/?id=' .$id);
        }
    }

    // ACCOUNT DB COLUMNS
    $dbc_account = array(
        'username'  => $_POST['email'],
        'email'     => $_POST['email']
    );

    // DETAILS DB COLUMNS
    $dbc_details = array(
        'firstname' => $_POST['details']['firstname'],
        'lastname'  => $_POST['details']['lastname'],
        'credit'    => (float)$_POST['details']['credit']
    );

    // MATTD: Add onto $dbc_details array from optional process_extra.php file

    // HAVE ID
    if ($id) {

        // GET ACCOUNT INFO
        $query = "
        SELECT *
        FROM `". $_uccms['_account']->config['db']['accounts'] ."` AS `a`
        INNER JOIN `". $_uccms['_account']->config['db']['accounts_details'] ."` AS `ad` ON a.id=ad.id
        WHERE (a.id=" .$id. ")
        ";
        $q = sqlquery($query);
        $account = sqlfetch($q);

        // ACCOUNT FOUND
        if ($account['id']) {

            // ACCOUNT CLASS - UPDATE CONFIG
            $_uccms['_account']->setConfig(array(
                'admin' => array(
                    'acct_id' => $account['id']
                ))
            );

            $dbc_account['status'] = (int)$_POST['status'];

            // WAS DELETED BUT NOT ANYMORE
            if (($_POST['current_status'] == 9) && ($_POST['status'] != 9)) {
                $dbc_account['deleted'] = '0000-00-00 00:00:00';
            }

            // EMAIL ADDRESS VERIFIED
            if ($_POST['verified']) {
                $dbc_account['verify'] = '';
            } else {
                $dbc_account['verify'] = $_uccms['_account']->rand_string(20, true);
            }

            // UPDATE ACCOUNT
            if ($_uccms['_account']->updateAccount($account['id'], $dbc_account)) {

                // UPDATE ACCOUNT DETAILS
                if ($_uccms['_account']->updateAccount($account['id'], $dbc_details, true)) {

                    // ACCOUNT JUST DELETED
                    if (($_POST['current_status'] != 9) && ($_POST['status'] == 9)) {

                        // DELETE ACCOUNT
                        if ($_uccms['_account']->delete($account['id'])) {

                            // CLEAR POST SESSION
                            unset($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post']);

                            $admin->growl('Frontend Accounts', 'Account deleted.');

                            $deleted = true;

                        // ACCOUNT NOT DELETED
                        } else {
                            $admin->growl('Frontend Accounts', 'Failed to delete account.');
                        }

                    }

                    // PASSWORD SUBMITTED
                    if ($_POST['password']) {

                        // CHANGE PASSWORD
                        $change = $_uccms['_account']->changePassword($_POST['password']);

                        // PASSWORD CHANGED
                        if ($change['success'] == true) {

                            // CLEAR POST SESSION
                            unset($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post']);

                            $admin->growl('Frontend Accounts', 'Account updated.');

                        // PASSWORD NOT CHANGED
                        } else {
                            if ($change['message']) {
                                $admin->growl('Frontend Accounts', $change['message']);
                            } else {
                                $admin->growl('Frontend Accounts', 'Failed to change password.');
                            }
                        }

                    // NO PASSWORD SUBMITTED & NOT JUST DELETED
                    } else if (!$deleted) {

                        // CLEAR POST SESSION
                        unset($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post']);

                        $admin->growl('Frontend Accounts', 'Account updated.');

                    }

                // DETAILS FAILED
                } else {
                    $admin->growl('Frontend Accounts', 'Failed to update account details.');
                }

            // FAILED
            } else {
                $admin->growl('Frontend Accounts', 'Failed to update account.');
            }

        // ACCOUNT NOT FOUND
        } else {
            $admin->growl('Frontend Accounts', 'Account not found.');
        }

    // NO ID
    } else {

        // REGISTER ACCOUNT
        $register = $_uccms['_account']->register($_POST['email'], $_POST['password'], $_POST['email'], $dbc_details, $_POST['send_welcome_email'], true);

        // ACCOUNT CREATED
        if ($register['success'] == true) {

            // CLEAR POST SESSION
            unset($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post']);

            // USE NEW ACCOUNT ID
            $id = $register['id'];

            $admin->growl('Frontend Accounts', 'Account created.');

        // ACCOUNT NOT CREATED
        } else {
            if ($register['message']) {
                $admin->growl('Frontend Accounts', $register['message']);
            } else {
                $admin->growl('Frontend Accounts', 'Failed to create account.');
            }
        }

    }

    // FROM SOMEWHERE
    if ($_POST['from']) {

        // SEND BACK
        BigTree::redirect(ADMIN_ROOT . str_replace('id=new', 'id=' .$id, str_replace(ADMIN_ROOT, '', $_POST['from'])));

    }

}

BigTree::redirect(ADMIN_ROOT. 'accounts/edit/?id=' .$id);

?>