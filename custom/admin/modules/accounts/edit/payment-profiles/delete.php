<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// PAYMENT PROFILES ENABLED
if ($_uccms['_account']->paymentProfilesEnabled()) {

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // ACCOUNT ID SPECIFIED
    if ($_REQUEST['account_id']) {

        // GET ACCOUNT
        $account = $_uccms['_account']->getAccount(null, true, (int)$_REQUEST['account_id']);

        // ACCOUNT FOUND
        if ($account['id']) {

            // SAVE TO SESSION
            $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment'] = $_REQUEST['payment'];

            // PAYMENT PROFILE ID SPECIFIED
            if ($_REQUEST['id']) {

                // GET PAYMENT PROFILE
                $prof = $_uccms['_account']->pp_getProfile((int)$_REQUEST['id'], $account['id']);

                // PROFILE NOT FOUND
                if (!$prof['id']) {
                    $admin->growl('Frontend Accounts', 'Payment method not found.');
                    BigTree::redirect(ADMIN_ROOT. 'accounts/edit/?id=' .$account['id']);
                }

                // DELETE PROFILE
                $result = $_uccms['_account']->pp_deleteProfile($prof['id']);

                // PROFILE CREATED
                if ($result['success']) {
                    $admin->growl('Frontend Accounts', 'Payment method deleted.');

                // FAILED
                } else {
                    if ($result['error']) {
                        $admin->growl('Payment Methods', $result['error']);
                    } else {
                        $admin->growl('Payment Methods', 'Failed to delete payment method. Please try again.');
                    }
                }

            // PROFILE ID NOT SPECIFIED
            } else {
                $admin->growl('Frontend Accounts', 'Payment method not specified.');
            }

            BigTree::redirect(ADMIN_ROOT. 'accounts/edit/?id=' .$account['id']);

        // ACCOUNT NOT FOUND
        } else {
            $admin->growl('Frontend Accounts', 'Account not found.');
        }

    // ACCOUNT ID NOT SPECIFIED
    } else {
        $admin->growl('Frontend Accounts', 'Account not specified.');
    }

// PAYMENT PROFILES NOT ENABLED
} else {
    $admin->growl('Frontend Accounts', 'Payment profiles not enabled.');
}

BigTree::redirect(ADMIN_ROOT. 'accounts/');

?>