<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// PAYMENT PROFILES ENABLED
if ($_uccms['_account']->paymentProfilesEnabled()) {

    // CLEAN UP
    $id = (int)$_POST['id'];

    // ACCOUNT ID SPECIFIED
    if ($_POST['account_id']) {

        // GET ACCOUNT
        $account = $_uccms['_account']->getAccount(null, true, (int)$_POST['account_id']);

        // ACCOUNT FOUND
        if ($account['id']) {

            // SAVE TO SESSION
            $_SESSION['_ACCOUNT']['EDIT']['payment_profiles']['payment'] = $_POST['payment'];

            // PAYMENT PROFILE ID SPECIFIED
            if ($_POST['id']) {

                // GET PAYMENT PROFILE
                $prof = $_uccms['_account']->pp_getProfile((int)$_POST['id'], $account['id']);

                // PROFILE NOT FOUND
                if (!$prof['id']) {
                    $admin->growl('Frontend Accounts', 'Payment method not found.');
                    BigTree::redirect(ADMIN_ROOT. 'accounts/edit/?id=' .$account['id']);
                }

                // CARD INFO
                $data = array(
                    'card' => array(
                        'name'          => $_POST['payment']['name'],
                        'expiration'    => $_POST['payment']['expiration'],
                        'zip'           => $_POST['payment']['zip']
                    ),
                    'default' => (int)$_POST['payment']['default']
                );

                // UPDATE PROFILE
                $result = $_uccms['_account']->pp_updateProfile($prof['id'], $data);

                // PROFILE UPDATED
                if ($result['id']) {
                    unset($_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']);
                    $admin->growl('Payment Methods', 'Payment method updated.');

                // FAILED
                } else {
                    if ($result['error']) {
                        $admin->growl('Payment Methods', $result['error']);
                    } else {
                        $admin->growl('Payment Methods', 'Failed updating payment method. Please try again.');
                    }
                    BigTree::redirect(ADMIN_ROOT. 'accounts/edit/payment-profiles/edit/?account_id=' .$account['id']. '&id=' .$prof['id']);
                }

            // PAYMENT PROFILE ID NOT SPECIFIED
            } else {

                // PROFILE DATA
                $data = array(
                    'customer' => array(
                        'description'   => '',
                        'email'         => $account['email']
                    ),
                    'card' => array(
                        'name'          => $_POST['payment']['name'],
                        'number'        => $_POST['payment']['number'],
                        'expiration'    => $_POST['payment']['expiration'],
                        'zip'           => $_POST['payment']['zip'],
                        'cvv'           => $_POST['payment']['cvv']
                    ),
                    'default' => (int)$_POST['payment']['default']
                );

                // CREATE PROFILE
                $result = $_uccms['_account']->pp_createProfile($data, $account['id']);

                // PROFILE CREATED
                if ($result['id']) {
                    unset($_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']);
                    $admin->growl('Payment Methods', 'Payment method added.');

                // FAILED
                } else {
                    if ($result['error']) {
                        $admin->growl('Payment Methods', $result['error']);
                    } else {
                        $admin->growl('Payment Methods', 'Failed adding payment method. Please try again.');
                    }
                    BigTree::redirect(ADMIN_ROOT. 'accounts/edit/payment-profiles/edit/?account_id=' .$account['id']);
                }

            }

            BigTree::redirect(ADMIN_ROOT. 'accounts/edit/?id=' .$account['id']);

        // ACCOUNT NOT FOUND
        } else {
            $admin->growl('Frontend Accounts', 'Account not found.');
        }

    // ACCOUNT ID NOT SPECIFIED
    } else {
        $admin->growl('Frontend Accounts', 'Account not specified.');
    }

// PAYMENT PROFILES NOT ENABLED
} else {
    $admin->growl('Frontend Accounts', 'Payment profiles not enabled.');
}

BigTree::redirect(ADMIN_ROOT. 'accounts/');

?>