<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// PAYMENT PROFILES ENABLED
if ($_uccms['_account']->paymentProfilesEnabled()) {

    // CLEAN UP
    $id = (int)$_REQUEST['id'];

    // ACCOUNT ID SPECIFIED
    if ($_REQUEST['account_id']) {

        // GET ACCOUNT
        $account = $_uccms['_account']->getAccount(null, true, (int)$_REQUEST['account_id']);

        // ACCOUNT FOUND
        if ($account['id']) {

            // PAYMENT PROFILE ID SPECIFIED
            if ($_REQUEST['id']) {

                // GET PAYMENT PROFILE
                $prof = $_uccms['_account']->pp_getProfile((int)$_REQUEST['id'], (int)$_REQUEST['account_id']);

                // PROFILE NOT FOUND
                if (!$prof['id']) {
                    $admin->growl('Frontend Accounts', 'Payment method not found.');
                    BigTree::redirect('../?id=' .$_REQUEST['account_id']);
                }

                $do_title = 'Edit';

            // NO PAYMENT PROFILE ID
            } else {

                $do_title = 'Add';

            }

        // ACCOUNT NOT FOUND
        } else {
            $admin->growl('Frontend Accounts', 'Account not found.');
            BigTree::redirect(ADMIN_ROOT. 'accounts/');
        }

    // ACCOUNT ID NOT SPECIFIED
    } else {
        $admin->growl('Frontend Accounts', 'Account not specified.');
        BigTree::redirect(ADMIN_ROOT. 'accounts/');
    }

// PAYMENT PROFILES NOT ENABLED
} else {
    $admin->growl('Frontend Accounts', 'Payment profiles not enabled.');
    BigTree::redirect(ADMIN_ROOT. 'accounts/');
}

?>

<h1>
    <span class="page_icon users"></span>
    <a class="first" href="/admin/accounts/">Frontend Accounts</a>
    <span class="divider">›</span>
    <a class="last" href="../?id=<?php echo $_REQUEST['id']; ?>"><?php echo trim(stripslashes($account['firstname']. ' ' .$account['lastname'])); ?></a>
    <span class="divider">›</span>
    <a class="last" href=""><?php echo $do_title; ?> Payment Method</a>
</h1>

<nav id="sub_nav">
    <a href="/admin/accounts/"><span class="icon_small icon_small_list"></span>View Accounts</a>
    <a class="active" href="/admin/accounts/edit/"><span class="icon_small icon_small_add"></span>Add Account</a>
    <menu style="display: none;">
        <span class="icon"></span>
        <div></div>
    </menu>
</nav>

<div class="container">

    <form method="post" action="../process/" class="module">
    <input type="hidden" name="account_id" value="<?php echo $_REQUEST['account_id']; ?>" />
    <input type="hidden" name="id" value="<?php echo $prof['id']; ?>" />

    <header>
        <h2><?php echo $do_title; ?> Payment Method</h2>
    </header>

    <section>

        <?php if ($_REQUEST['error']) { ?>
            <p class="error_message"><?php echo urldecode($_REQUEST['error']); ?></p>
        <?php } ?>

        <div style="padding-bottom: 20px;">
            * All fields required
        </div>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Card Number</label>
                    <?php if ($prof['id']) { ?>
                        <?php if ($prof['type']) { echo ucwords($prof['type']). ' - '; } echo $prof['lastfour']; ?>
                    <?php } else { ?>
                        <input type="text" name="payment[number]" value="<?php echo $_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['number']; ?>" />
                    <?php } ?>
                </fieldset>

                <fieldset>
                    <label>Name On Card</label>
                    <input type="text" name="payment[name]" value="<?php if ($_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['name']) { echo $_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['name']; } else { echo stripslashes($prof['name']); } ?>" />
                </fieldset>

                <fieldset>
                    <label>Card Expiration</label>
                    <input type="text" name="payment[expiration]" value="<?php if ($_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['expires']) { echo $_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['expires']; } else if ($prof['expires']) { echo date('m/Y', strtotime($prof['expires'])); } ?>" placeholder="mm/yyyy" />
                </fieldset>

                <fieldset>
                    <label>Card Zip</label>
                    <input type="text" name="payment[zip]" value="<?php if ($_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['zip']) { echo $_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['zip']; } else { echo $prof['zip']; } ?>" />
                </fieldset>

                <fieldset>
                    <label>Card CVV</label>
                    <input type="text" name="payment[cvv]" value="<?php echo $_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']['cvv']; ?>" />
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <input type="checkbox" name="payment[default]" value="1" <?php if ($prof['default']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Default</label>
                </fieldset>

                <fieldset>
                    <input type="checkbox" name="payment[active]" value="1" <?php if ($prof['active']) { ?>checked="checked"<?php } ?> />
                    <label class="for_checkbox">Active</label>
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input type="submit" value="<?php echo $do_title; ?>" class="blue" />
    </footer>

    </form>

</div>