<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// CLEAN UP
$id = (int)$_REQUEST['id'];

// ACCOUNT ID SPECIFIED
if ($id) {

    // GET ACCOUNT INFO
    $query = "
    SELECT *
    FROM `". $_uccms['_account']->config['db']['accounts'] ."` AS `a`
    INNER JOIN `". $_uccms['_account']->config['db']['accounts_details'] ."` AS `ad` ON a.id=ad.id
    WHERE (a.id=" .$id. ")
    ";
    $q = sqlquery($query);
    $account = sqlfetch($q);

    // ACCOUNT FOUND
    if ($account['id']) {

        $do_title = 'Edit';

    // ACCOUNT NOT FOUND
    } else {
        $admin->growl('Frontend Accounts', 'Account not found.');
        BigTree::redirect(ADMIN_ROOT. 'accounts/');
    }

// NO ACCOUNT ID SPECIFIED
} else {

    $do_title = 'Add';

}

unset($_SESSION['ADMIN']['ACCOUNTS']['payment_profiles']['payment']);

// HAVE POSTED INFO
if (is_array($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post'])) {
    foreach ($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post'] as $name => $value) {
        $account[$name] = $value;
    }
}
unset($_SESSION['UCCMS']['ADMIN']['ACCOUNTS']['post']);

?>

<h1>
    <span class="page_icon users"></span>
    <a class="first" href="/admin/accounts/">Frontend Accounts</a>
    <span class="divider">›</span>
    <a class="last" href=""><?php echo $do_title; ?> Account</a>
</h1>

<nav id="sub_nav">
    <a href="/admin/accounts/"><span class="icon_small icon_small_list"></span>View Accounts</a>
    <a class="active" href="/admin/accounts/edit/"><span class="icon_small icon_small_add"></span>Add Account</a>
    <menu style="display: none;">
        <span class="icon"></span>
        <div></div>
    </menu>
</nav>

<div class="container">

    <form method="post" action="./process/" class="module">
    <input type="hidden" name="id" value="<?php echo $account['id']; ?>" />
    <?php if ($_REQUEST['from']) { ?>
        <input type="hidden" name="from" value="<?php echo $_REQUEST['from']; ?>" />
    <?php } ?>

    <section>

        <p style="display: none;" class="error_message">Errors found! Please fix the highlighted fields before submitting.</p>

        <div class="contain">

            <div class="left">

                <fieldset>
                    <label class="required">Email </label>
                    <input type="text" name="email" value="<?php echo stripslashes($account['email']); ?>" autocomplete="off" class="required email" />
                </fieldset>

                <fieldset>
                    <label class="required">Password <?php if ($account['id']) { ?><small>Only enter if changing</small><?php } ?></label>
                    <input type="password" name="password" value="" class="custom_control" />
                </fieldset>

                <?php if ($account['id']) { ?>

                    <input type="hidden" name="current_status" value="<?php echo $account['status']; ?>" />

                    <fieldset>
                        <label>Status </label>
                        <select name="status">
                            <option value="0" <?php if ($account['status'] == 0) { ?>selected="selected"<?php } ?>>Inactive</option>
                            <option value="1" <?php if ($account['status'] == 1) { ?>selected="selected"<?php } ?>>Active</option>
                            <option value="9" <?php if ($account['status'] == 9) { ?>selected="selected"<?php } ?>>Deleted</option>
                        </select>
                    </fieldset>

                    <fieldset>
                        <input type="checkbox" name="verified" value="1" <?php if (!$account['verify']) { ?>checked="checked"<?php } ?> /><label class="for_checkbox">Email address verified</label>
                    </fieldset>

                <?php } else { ?>

                    <fieldset>
                        <input type="checkbox" name="send_welcome_email" value="1" checked="checked" /><label class="for_checkbox">Send welcome email</label>
                    </fieldset>

                <?php } ?>

            </div>

            <div class="right">

                <fieldset>
                    <label>First Name</label>
                    <input type="text" name="details[firstname]" value="<?php echo stripslashes($account['firstname']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Last Name</label>
                    <input type="text" name="details[lastname]" value="<?php echo stripslashes($account['lastname']); ?>" />
                </fieldset>

                <fieldset>
                    <label>Credit</label>
                    $<input type="text" name="details[credit]" value="<?php echo number_format((int)$account['credit'], 0); ?>" style="display: inline-block; width: 60px;" />
                </fieldset>

            </div>

        </div>

    </section>

    <footer>
        <input type="submit" value="<?php echo $do_title; ?>" class="blue" />
    </footer>

    </form>

</div>

<?php

// HAVE ACCOUNT
if ($account['id']) {

    // CUSTOM FIELDS
    if (file_exists(dirname(__FILE__). '/custom/fields.php')) {
        include(dirname(__FILE__). '/custom/fields.php');
    }

}

// HAVE ACCOUNT & PAYMENT PROFILES ENABLED
if (($account['id']) && ($_uccms['_account']->paymentProfilesEnabled())) {

    $allprofiles = array();
    $allprofiles_active = array();

    // GET PAYMENT PROFILES
    $prof_sql = "SELECT * FROM `". $_uccms['_account']->config['db']['accounts_payment_profiles'] ."` WHERE (`account_id`=" .$account['id']. ") ORDER BY `default` DESC, `dt_updated` DESC";
    $prof_query = sqlquery($prof_sql);
    while ($prof = sqlfetch($prof_query)) {
        $allprofiles[$prof['id']] = $prof;
        if ($prof['dt_deleted'] == '0000-00-00 00:00:00') {
            $allprofiles_active[$prof['id']] = $prof;
        }
    }

    ?>

    <style type="text/css">

        #payment-profiles .item_title {
            width: 150px;
            text-align: left;
        }
        #payment-profiles .item_name {
            width: 300px;
            text-align: left;
        }
        #payment-profiles .item_status {
            width: 100px;
            text-align: left;
        }
        #payment-profiles .item_default {
            width: 100px;
            text-align: left;
        }
        #payment-profiles .item_expires {
            width: 190px;
            text-align: left;
        }
        #payment-profiles .item_edit {
            width: 50px;
            text-align: left;
        }
        #payment-profiles .item_delete {
            width: 50px;
            text-align: left;
        }

    </style>

    <div id="payment-profiles" class="table">
        <summary>
            <h2>Payment Methods</h2>
            <a class="add_resource add" href="./payment-profiles/edit/?account_id=<?php echo $account['id']; ?>"><span></span>Add Payment Method</a>
        </summary>
        <?php if (count($allprofiles_active) > 0) { ?>
            <header style="clear: both;">
                <span class="item_title">Card</span>
                <span class="item_name">Name</span>
                <span class="item_status">Status</span>
                <span class="item_default">Default</span>
                <span class="item_expires">Expires</span>
                <span class="item_edit">Edit</span>
                <span class="item_delete">Delete</span>
            </header>
            <ul class="items">
                <?php foreach ($allprofiles_active as $prof) { ?>
                    <li class="item contain">
                        <section class="item_title">
                            <a href="./payment-profiles/edit/?account_id=<?php echo $prof['account_id']; ?>&id=<?php echo $prof['id']; ?>"><?php if ($prof['type']) { echo ucwords($prof['type']). ' - '; } echo $prof['lastfour']; ?></a>
                        </section>
                        <section class="item_name">
                            <?php echo stripslashes($prof['name']); ?>
                        </section>
                        <section class="item_status status_<?php if ($prof['active'] == 1) { ?>published<?php } else { ?>pending<?php } ?>">
                            <?php if ($prof['active'] == 1) { ?>Active<?php } else { ?>Inactive<?php } ?>
                        </section>
                        <section class="item_default default_<?php if ($prof['default'] == 1) { ?>yes<?php } else { ?>no<?php } ?>">
                            <?php if ($prof['default'] == 1) { ?>Yes<?php } ?>
                        </section>
                        <section class="item_expires">
                            <?php echo date('m/Y', strtotime($prof['expires'])); ?>
                        </section>
                        <section class="item_edit">
                            <a class="icon_edit" title="Edit Category" href="./payment-profiles/edit/?account_id=<?php echo $prof['account_id']; ?>&id=<?php echo $prof['id']; ?>"></a>
                        </section>
                        <section class="item_delete">
                            <a href="./payment-profiles/delete/?account_id=<?php echo $prof['account_id']; ?>&id=<?php echo $prof['id']; ?>" class="icon_delete" title="Delete Item" onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this this?');"></a>
                        </section>
                    </li>
                <?php } ?>
            </ul>
        <?php } else { ?>
            <div style="padding: 10px; text-align: center;">Account does not have any payment methods set up yet.</div>
        <?php } ?>
    </div>

    <?php

    // GET TRANSACTIONS
    $translog_query = "SELECT * FROM `" .$_uccms['_account']->config['db']['accounts_transaction_log']. "` WHERE (`account_id`='" .$account['id']. "') ORDER BY `dt` DESC";
    $translog_q = sqlquery($translog_query);

    // HAVE TRANSACTIONS
    if (sqlrows($translog_q) > 0) {

        ?>

        <a name="transactions"></a>

        <style type="text/css">

            #transactions .transaction_dt {
                width: 150px;
                text-align: left;
            }
            #transactions .transaction_amount {
                width: 100px;
                text-align: left;
            }
            #transactions .transaction_method {
                width: 175px;
                text-align: left;
            }
            #transactions .transaction_status {
                width: 120px;
                text-align: left;
            }
            #transactions .transaction_note {
                width: 370px;
                text-align: left;
            }
            #transactions .transaction_edit {
                width: 50px;
            }
            #transactions .transaction_delete {
                width: 50px;
            }

            #transactions .transactions ul .item {
                height: auto;
            }
            #transactions .transactions ul .item .edit {
                padding: 10px 20px 0;
                border-top: 1px dashed #ddd;
            }

        </style>

        <div id="transactions" class="container">

            <header>
                <h2>Transactions</h2>
            </header>

            <div class="transactions table" style="margin: 0px; border: 0px none;">

                <header style="clear: both;">
                    <span class="transaction_dt">Date / Time</span>
                    <span class="transaction_amount">Amount</span>
                    <span class="transaction_method">Method</span>
                    <span class="transaction_status">Status</span>
                    <span class="transaction_note">Note</span>
                    <? /*
                    <span class="transaction_edit">Edit</span>
                    <span class="transaction_delete">Delete</span>
                    */ ?>
                </header>

                <ul class="items">

                    <?php

                    // LOOP
                    while ($transaction = sqlfetch($translog_q)) {

                        ?>
                        <li class="item contain">
                            <section class="transaction_dt">
                                <?php echo date('n/j/Y g:i A T', strtotime($transaction['dt'])); ?>
                            </section>
                            <section class="transaction_amount">
                                $<?php echo number_format($transaction['amount'], 2); ?>
                            </section>
                            <section class="transaction_method">
                                <?php if ($allprofiles[$transaction['payment_profile_id']]['type']) { echo ucwords($allprofiles[$transaction['payment_profile_id']]['type']). ' - '; } echo stripslashes($allprofiles[$transaction['payment_profile_id']]['lastfour']); ?>
                            </section>
                            <section class="transaction_status">
                                <?php echo ucwords($transaction['action']); if ($transaction['success']) { echo ' - Success'; } else { echo ' - Failed'; } ?>
                            </section>
                            <section class="transaction_note">
                                <?php
                                if ($transaction['response_message']) {
                                    echo stripslashes($transaction['response_message']);
                                } else {
                                    echo stripslashes($transaction['transaction_id']);
                                }
                                ?>
                            </section>
                            <? /*
                            <section class="transaction_edit">
                                <a href="#" title="Edit" class="icon_edit"></a>
                            </section>
                            <section class="transaction_delete">
                                <a onclick="return confirmPrompt(this.href, 'Are you sure you want to delete this?');" title="Delete" class="icon_delete" href="./processes/delete_transaction/?order_id=<?php echo $order['id']; ?>&id=<?php echo $transaction['id']; ?>"></a>
                            </section>
                            */ ?>
                        </li>
                        <?php

                    }

                    ?>

                </ul>

            </div>

        </div>

    <?php } ?>

<?php } ?>