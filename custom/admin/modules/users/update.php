<?php

// REVSOCIAL API KEY
$api_key = $cms->getSetting('rs_api-key');

// HAVE API KEY
if ($api_key) {

    $id = (int)$_POST['id'];

    $rs_aid = 0;

    // NEW REVSOCIAL ACCOUNT
    if ($_POST['revsocial_account_id'] == 'new') {

        // SPLIT UP NAME
        $name = _this_nameSplit($_POST['name']);

        // REST CLIENT
        require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

        // INIT REST API CLASS
        $_api = new RestClient(array(
            'base_url' => 'https://api.revsocial.com/v1'
        ));

        // MAKE API CALL
        $result = $_api->post('account/create', array(
            'api_key'   => $api_key,
            'data'      => array(
                'email'     => $_POST['email'],
                'password'  => $_POST['password'],
                'firstname' => $name[0],
                'lastname'  => $name[1]
            )
        ));

        // API RESPONSE
        $resp = $result->decode_response();

        //echo print_r($resp);
        //exit;

        if (($resp->success) && ($resp->account_id)) {
            $rs_aid = $resp->account_id;
        } else {
            if ($resp->err) {
                $error = $resp->err;
            } else {
                $error = 'Failed to create RevSocial user.';
            }
            $admin->growl('RevSocial Create User', $error, 'error');
            //BigTree::redirect(ADMIN_ROOT. 'users/edit/' .$id. '/');
        }

    // USE SPECIFIED ID
    } else {
        $rs_aid = (int)$_POST['revsocial_account_id'];
    }

    // UPDATE REVSOCIAL ACCOUNT ID
    $sql = "UPDATE `bigtree_users` SET `revsocial_account_id`=" .$rs_aid. " WHERE (`id`=" .$id. ")";
    sqlquery($sql);

}

// NORMAL PROCESSING
require(SERVER_ROOT. '/core/admin/modules/users/update.php');


// SPLIT A NAME
function _this_nameSplit($name) {
    $name = trim($name);
    $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
    $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
    return array($first_name, $last_name);
}


?>