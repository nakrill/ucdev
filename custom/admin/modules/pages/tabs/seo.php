<p class="error_message" style="display: none;">Errors found! Please fix the highlighted fields before submitting.</p>
<fieldset class="form-group">
    <label>URL Route <small>(leave blank to auto generate)</small></label>
    <input type="text" name="route" value="<?=$bigtree["current_page"]["route"]?>" tabindex="2" class="form-control" />
</fieldset>
<div class="row">
    <fieldset class="form-group col-md-6">
        <label>Meta Keywords</label>
        <textarea name="meta_keywords" class="form-control"><?=$bigtree["current_page"]["meta_keywords"]?></textarea>
    </fieldset>
    <fieldset class="form-group col-md-6">
        <label>Meta Description</label>
        <textarea name="meta_description" class="form-control"><?=$bigtree["current_page"]["meta_description"]?></textarea>
    </fieldset>
</div>
<fieldset class="form-group">
    <input type="checkbox" name="seo_invisible"<? if ($bigtree["current_page"]["seo_invisible"]) { ?> checked="checked"<? } ?> /> <label class="for_checkbox">Hide From Search Engines</label>
</fieldset>