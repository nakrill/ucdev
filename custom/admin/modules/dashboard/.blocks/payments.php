<?php

if (!defined('WWW_ROOT')) exit;

###########################################################



###########################################################

?>

<style type="text/css">

    #uccmsDashboard .card.payments .payments .payment [class*="dcol-"] {
        padding-left: 0px;
    }

</style>

<script type="text/javascript">

    $(function() {

        // GET PAYMENT ITEMS
        $.get('<?php echo ADMIN_ROOT; ?>ajax/modules/dashboard/blocks/payments/', {
            limit: 10
        }, function(data) {
            if (Object.keys(data).length > 0) {
                $('#uccmsDashboard .card.payments .content .no-results').hide();
                $.each(data, function(i, v) {

                    var $el = $('#uccmsDashboard .card.payments .payments .payment.template').clone();

                    if (v.number) $el.find('.order-number[class*="dcol-"]').text(v.number);
                    if (v.dt) $el.find('.date[class*="dcol-"]').text(v.dt);
                    if (v.name) $el.find('.name[class*="dcol-"]').text(v.name);
                    if (v.method) $el.find('.payment-method[class*="dcol-"]').text(v.method);
                    if (v.description) $el.find('.description[class*="dcol-"]').text(v.description);
                    if (v.amount) $el.find('.amount[class*="dcol-"]').addClass(v.success ? 'text-color-green' : 'text-color-red').text(v.amount);

                    $el.attr('data-id', v.number);
                    if ((typeof v.url == 'object') && (v.url.edit)) {
                        $el.attr('href', v.url.edit);
                        $el.attr('data-link', v.url.edit);
                    }

                    $el.insertBefore('#uccmsDashboard .card.payments .payments .payment.template').removeClass('template').show();

                });
                equalHeight('#uccmsDashboard .drow .dblock .contain');
            } else {
                $('#uccmsDashboard .card.payments .content .payments').hide();
                $('#uccmsDashboard .card.payments .view-all').hide();
                $('#uccmsDashboard .card.payments .content .no-results').show();
            }
        }, 'json');

    });

</script>


<div class="payments dblock col-md-12">
    <div class="card">
        <div class="card-header">
            <a href="/admin/dashboard/drilldown/payments/" class="h5">Recent Payments</a>
            <a href="/admin/dashboard/drilldown/payments/" class="btn btn-info small float-sm-right">View all</a>
        </div>
        <div class="card-body">
            <div class="content">

                <div class="payments ditems alt-bg-odd">

                    <a href="#" class="payment drow ditem template">
                        <span class="dcol-1 order-number light-text">&nbsp;</span>
                        <span class="dcol-2 date">&nbsp;</span>
                        <span class="dcol-3 name bolder-text">&nbsp;</span>
                        <span class="dcol-2 payment-method light-text">&nbsp;</span>
                        <span class="dcol-3 description">&nbsp;</span>
                        <span class="dcol-1 amount bold-text align-right">&nbsp;</span>
                    </a>

                </div>

                <div class="no-results" style="display: none;">
                    It doesn't look like you've received any payments yet.
                </div>

            </div>
        </div>
    </div>
</div>
