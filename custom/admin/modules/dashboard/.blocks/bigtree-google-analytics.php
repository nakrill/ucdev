<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// GET GOOGLE ANALYTICS TRAFFIC
if (file_exists(SERVER_ROOT."cache/analytics.json")) {
    $ga_cache = json_decode(file_get_contents(SERVER_ROOT."cache/analytics.json"),true);
} else {
    $ga_cache = false;
}

###########################################################

if ($ga_cache && count($ga_cache["two_week"])) {

    ?>

    <style type="text/css">

        #uccmsDashboard .dblock.google-analytics .table {
            background-color: #fff;
            border-radius: 3px;
            box-shadow: 0 0 6px rgba(0, 0, 0, .1);
        }
        #uccmsDashboard .dblock.google-analytics .table > summary {
            margin: 0px;
            padding: 0px;
            background-color: #fff;
            border: 0px none;
        }
        #uccmsDashboard .dblock.google-analytics .table > summary h2 span {
            margin-top: -6px;
        }
        #uccmsDashboard .dblock.google-analytics .table > summary .more {
            margin-top: 0px;
        }
        #uccmsDashboard .dblock.google-analytics .table > section {
            overflow: visible;
            margin: 15px 0 0;
            border-radius: 3px;
        }

    </style>

    <div class="dblock google-analytics dcol-12">

        <?php

        $visits = $ga_cache["two_week"];

        $min = min((is_array($visits)) ? $visits : array($visits));
        $max = max((is_array($visits)) ? $visits : array($visits)) - $min;

        if ($max == 0) {
            $max = 1;
        }

        $bar_height = 70;

        ?>

        <div class="table">
            <summary>
                <h2 class="full">
                    <span class="analytics"></span>
                    Recent Traffic <small>Visits In The Past Two Weeks</small>
                    <a href="<?=ADMIN_ROOT?>dashboard/vitals-statistics/analytics/" class="more">View Analytics</a>
                </h2>
            </summary>
            <section>
                <? if ($visits) { ?>
                    <div class="graph">
                        <?
                        $x = 0;
                        foreach ($visits as $date => $count) {
                            $height = round($bar_height * ($count - $min) / $max) + 12;
                            $x++;
                            if (!$count) {
                                $count = 0;
                            }
                            ?>
                            <section class="bar<? if ($x == 14) { ?> last<? } elseif ($x == 1) { ?> first<? } ?>" style="height: <?=$height?>px; margin-top: <?=(82-$height)?>px;">
                                <?=$count?>
                            </section>
                            <?
                        }
                        $x = 0;
                        foreach ($visits as $date => $count) {
                            $x++;
                            ?>
                            <section class="date<? if ($x == 14) { ?> last<? } elseif ($x == 1) { ?> first<? } ?>"><?=date("n/j/y",strtotime($date))?></section>
                            <?
                        }
                        ?>
                    </div>
                <? } else { ?>
                    <p>No recent traffic</p>
                    <?
                }
                ?>
            </section>
        </div>

    </div>

    <?

}

?>