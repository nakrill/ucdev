<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// NO FORM BUILDER EXTENSION
if (!class_exists('uccms_FormBuilder')) {
    return;
}

if (!$_uccms_formbuilder) $_uccms_formbuilder = new uccms_FormBuilder();

// NO ACCESS
if (!$_uccms_formbuilder->adminModulePermission()) {
    return;
}

// GET TODAY'S ENTRIES
$today_query = "SELECT * FROM `" .$_uccms_formbuilder->tables['entries']. "` WHERE (`created_at`>='" .date('Y-m-d'). " 00:00:00')";
$today_q = sqlquery($today_query);

$num_today = sqlrows($today_q);

// GET LATEST ENTRIES
$entries_query = "SELECT * FROM `" .$_uccms_formbuilder->tables['entries']. "` ORDER BY `id` DESC LIMIT 5";
$entries_q = sqlquery($entries_query);

// FUNCTION: Recursively Implode
function this_recursiveImplode(array $array, $glue = ',', $include_keys=false, $trim_all=true) {
    $glued_string = '';
    array_walk_recursive($array, function($value, $key) use ($glue, $include_keys, &$glued_string) {
        $include_keys and $glued_string .= $key.$glue;
        $glued_string .= $value.$glue;
    });
    strlen($glue) > 0 and $glued_string = substr($glued_string, 0, -strlen($glue));
    $trim_all and $glued_string = preg_replace("/(\s)/ixsm", '', $glued_string);
    return (string) $glued_string;
}

###########################################################

?>

<style type="text/css">

    #uccmsDashboard .dblock.FormBuilder_leads .stats {
        text-align: center;
        font-size: 2em;
    }

    #uccmsDashboard .dblock.FormBuilder_leads .entries {
        margin-top: 15px;
    }
    #uccmsDashboard .dblock.FormBuilder_leads .entries .entry [class*="dcol-"] {
        padding-left: 0px;
    }
    #uccmsDashboard .dblock.FormBuilder_leads .entries .entry .sep {
        display: inline-block;
        padding: 0 5px;
        opacity: .7;
    }

</style>

<div class="dblock FormBuilder_leads col-md-6 no-equal-height">
    <div class="contain">

        <div class="view-all top float-right">
            <a href="/admin/<?php echo $_uccms_formbuilder->Extension; ?>*form-builder/" class="btn btn-sm">View all</a>
        </div>

        <h2><a href="/admin/<?php echo $_uccms_formbuilder->Extension; ?>*form-builder/">Forms</a></h2>
        <h3>Leads</h3>

        <div class="content">

            <div class="stats row">

                <div class="col-md-4">
                    <div class="stat">

                        <a href="/admin/<?php echo $_uccms_formbuilder->Extension; ?>*form-builder/">
                            <span class="num"><?php echo number_format($num_today, 0); ?></span>
                            <span class="title">Today</span>
                        </a>

                    </div>
                </div>

            </div>

            <?php

            // HAVE RESULTS
            if (sqlrows($entries_q) > 0) {

                ?>

                <div class="table-responsive-sm entries">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 20%;">Date</th>
                                <th scope="col" style="width: 30%;">Form</th>
                                <th scope="col" style="width: 50%;">Data</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                            $accounta = array();

                            // ARRAY OF FORMS
                            $forma = array();

                            // LOOP
                            while ($entry = sqlfetch($entries_q)) {

                                $today = false;

                                // NO FORM INFO
                                if (!$forma[$entry['form']]) {

                                    // GET FORM INFO
                                    $form_query = "SELECT * FROM `" .$_uccms_formbuilder->tables['forms']. "` WHERE (`id`=" .$entry['form']. ")";
                                    $form_q = sqlquery($form_query);
                                    $forma[$entry['form']] = sqlfetch($form_q);

                                }

                                // CLEAN UP DATA
                                $entry_data = json_decode(str_replace("\n", '<br />', stripslashes(str_replace('\"', '&quot;', $entry['data']))), true);
                                $entry_data = this_recursiveImplode($entry_data, '<span class="sep">&middot;</span>', false, false);

                                if (date('Y-m-d', strtotime($entry['created_at'])) == date('Y-m-d')) {
                                    $today = true;
                                    $date_title = date('n/j/Y', strtotime($entry['created_at']));
                                    $date_display = date('g:i a', strtotime($entry['created_at']));
                                } else {
                                    $date_title = date('g:i a', strtotime($entry['created_at']));
                                    $date_display = date('n/j/Y', strtotime($entry['created_at']));
                                }

                                ?>

                                <tr class="entry">
                                    <td><?php echo $date_display; ?></td>
                                    <td><?php echo substrChar(stripslashes($forma[$entry['form']]['title']), 32, '..'); ?></td>
                                    <td class="overflow-ellipsis"><span><?php echo substrChar($entry_data, 500, '..'); ?></span></td>
                                </tr>

                                <? /*
                                <a href="/admin/<?php echo $_uccms_formbuilder->Extension; ?>*form-builder/view-entry/<?php echo $entry['id']; ?>/" class="entry row" data-link="/admin/<?php echo $_uccms_formbuilder->Extension; ?>*form-builder/view-entry/<?php echo $entry['id']; ?>/">
                                    <span class="dcol-2 id light-text">
                                        <?php echo $entry['id']; ?>
                                    </span>
                                    <span class="col-md-3 date bold<?php echo ($today ? 'er' : ''); ?>-text" title="<?php echo $date_title; ?>">
                                        <?php echo $date_display; ?>
                                    </span>
                                    <span class="col-md-4 form-name light-text" title="<?php echo stripslashes($forma[$entry['form']]['title']); ?>">
                                        <?php echo substrChar(stripslashes($forma[$entry['form']]['title']), 12, '..'); ?>
                                    </span>
                                    <span class="col-md-5 data bold<?php echo ($today ? 'er' : ''); ?>-text" title="<?php echo substrWord($entry_data, 100, '..'); ?>">
                                        <?php echo substrChar($entry_data, 30, '..'); ?>
                                    </span>
                                </a>
                                */ ?>

                                <?php

                            }

                            ?>

                        </tbody>
                    </table>
                </div>

                <? /*
                <div class="view-all bottom">
                    <a href="/admin/<?php echo $_uccms_formbuilder->Extension; ?>*form-builder/" class="btn btn-sm">View all</a>
                </div>
                */ ?>

                <?php

            // NO RESULTS
            } else {
                ?>
                <div class="no-results">
                    No form submissions yet.
                </div>
                <?php
            }

            ?>

        </div>

    </div>
</div>