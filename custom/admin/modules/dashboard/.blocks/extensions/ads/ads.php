<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// NO FORM BUILDER EXTENSION
if (!class_exists('uccms_Ads')) {
    return;
}

if (!$_uccms_ads) $_uccms_ads = new uccms_Ads();

// NO ACCESS
if (!$_uccms_ads->adminModulePermission()) {
    return;
}

// GET ACTIVE ADS
$active_query = "SELECT `id` FROM `" .$_uccms_ads->tables['ads']. "` WHERE (`status`=1)";
$active_q = sqlquery($active_query);

$num_active = sqlrows($active_q);

// GET LATEST ADS
$ads_query = "SELECT * FROM `" .$_uccms_ads->tables['ads']. "` ORDER BY `dt_created` DESC LIMIT 5";
$ads_q = sqlquery($ads_query);

###########################################################

?>

<style type="text/css">

    #uccmsDashboard .dblock.Ads_ads .ads .ad [class*="dcol-"] {
        padding-left: 0px;
    }

</style>

<div class="dblock Ads_ads col-md-6 no-equal-height">
    <div class="contain">

        <h2><a href="/admin/<?php echo $_uccms_formbuilder->Extension; ?>*form-builder/">Ads</a></h2>

        <div class="view-all top float-right">
            <a href="/admin/<?php echo $_uccms_ads->Extension; ?>*ads/" class="btn btn-sm">View all</a>
        </div>

        <div class="content">

            <div class="row">

                <div class="col-md-3">
                    <div class="big-circle-num">
                        <a href="/admin/<?php echo $_uccms_ads->Extension; ?>*ads/ads/?status=active">
                            <span class="num background-color-<?php echo ($num_active > 0 ? 'green' : 'red'); ?>"><?php echo number_format($num_active, 0); ?></span>
                            <span class="title">Active</span>
                        </a>
                    </div>
                </div>

                <div class="col-md-9" style="margin-top: -45px;">

                    <?php

                    // HAVE RESULTS
                    if (sqlrows($ads_q) > 0) {

                        ?>
                        <div class="ads ditems alt-bg-odd">
                            <?php

                            $accounta = array();

                            // ARRAY OF FORMS
                            $forma = array();

                            // LOOP
                            while ($ad = sqlfetch($ads_q)) {

                                if (date('Y-m-d', strtotime($ad['dt_created'])) == date('Y-m-d')) {
                                    $date_title = date('n/j/Y', strtotime($ad['dt_created']));
                                    $date_display = date('g:i a', strtotime($ad['dt_created']));
                                } else {
                                    $date_title = date('g:i a', strtotime($ad['dt_created']));
                                    $date_display = date('n/j/Y', strtotime($ad['dt_created']));
                                }

                                ?>

                                <a class="ad drow ditem" href="/admin/<?php echo $_uccms_ads->Extension; ?>*ads/ad/edit/?id=<?php echo $ad['id']; ?>/" data-link="/admin/<?php echo $_uccms_ads->Extension; ?>*ads/ad/edit/?id=<?php echo $ad['id']; ?>/">
                                    <span class="dcol-1 id light-text">
                                        <?php echo $ad['id']; ?>
                                    </span>
                                    <span class="dcol-4 date" title="<?php echo $date_title; ?>">
                                        <?php echo $date_display; ?>
                                    </span>
                                    <span class="dcol-7 title bolder-text">
                                        <?php echo substrWord(stripslashes($ad['title']), 40, '..'); ?>
                                    </span>
                                </a>

                                <?php

                            }

                            ?>

                        </div>

                        <div class="view-all bottom">
                            <a href="/admin/<?php echo $_uccms_ads->Extension; ?>*ads/" class="btn small">View all</a>
                        </div>

                        <?php

                    // NO RESULTS
                    } else {
                        ?>
                        <div class="no-results">
                            No ads yet.
                        </div>
                        <?php
                    }

                    ?>

                </div>
            </div>

        </div>

    </div>
</div>