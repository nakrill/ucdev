<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// NO MEMBERSHIP EXTENSION
if (!class_exists('uccms_Membership')) {
    return;
}

if (!$_uccms_membership) $_uccms_membership = new uccms_Membership();

// NO ACCESS
if (!$_uccms_membership->adminModulePermission()) {
    return;
}

// GET MEMBERS
$members_query = "SELECT * FROM `" .$_uccms_membership->tables['account_plans']. "` WHERE (`status`=1) AND (`dt_expires`>'" .date('Y-m-d 23:59:59'). "') GROUP BY `account_id` ORDER BY `dt_expires` ASC LIMIT 5";
$members_q = sqlquery($members_query);

###########################################################

?>

<style type="text/css">

    #uccmsDashboard .dblock.Membership_upcoming-payments .members .member [class*="dcol-"] {
        padding-left: 0px;
    }
    #uccmsDashboard .dblock.Membership_upcoming-payments .members .member .order-number {

    }
    #uccmsDashboard .dblock.Membership_upcoming-payments .members .member .member-name {

    }

</style>

<div class="dblock Membership_upcoming-payments dcol-6">
    <div class="contain">
        <h2><a href="/admin/<?php echo $_uccms_membership->Extension; ?>*membership/">Membership</a></h2>
        <h3>Upcoming Payments</h3>
        <div class="content">
            <?php

            // HAVE RESULTS
            if (sqlrows($members_q) > 0) {

                ?>
                <div class="members ditems alt-bg-odd">
                    <?php

                    $accounta = array();

                    // PLANS ARRAY
                    $plana = array();

                    // GET PLANS
                    $plans_query = "SELECT * FROM `" .$_uccms_membership->tables['plans']. "` WHERE (`active`=1)";
                    $plans_q = sqlquery($plans_query);
                    while ($plan = sqlfetch($plans_q)) {
                        $plana[$plan['id']] = $plan;
                    }

                    // LOOP
                    while ($member = sqlfetch($members_q)) {

                        if (!$accounta[$member['account_id']]) {

                            // ACCOUNT INFO
                            $account_query = "
                            SELECT *
                            FROM `" .$_uccms_membership->tables['accounts']. "` AS `a`
                            INNER JOIN `" .$_uccms_membership->tables['account_details']. "` AS `ad` ON a.id=ad.id
                            WHERE (a.id=" .$member['account_id']. ")
                            ";
                            $account_q = sqlquery($account_query);
                            $accounta[$member['account_id']] = sqlfetch($account_q);

                        }

                        $account = $accounta[$member['account_id']];
                        $plan = $plana[$member['plan_id']];

                        if ($member['price_override'] > 0.00) {
                            $price = $member['price_override'];
                        } else {
                            if ($member['term'] == 'y') {
                                $price = $plan['price_yr'];
                            } else {
                                $price = $plan['price_mo'];
                            }
                        }

                        ?>

                        <div class="member drow ditem">
                            <div class="dcol-1 order-number light-text">
                                <?php echo $member['id']; ?>
                            </div>
                            <div class="dcol-4 member-name bolder-text">
                                <a href="/admin/<?php echo $_uccms_membership->Extension; ?>*membership/members/edit/?id=<?php echo $account['id']; ?>"><?php echo trim(stripslashes($account['firstname']. ' ' .$account['lastname'])); ?></a>
                            </div>
                            <div class="dcol-3 plan-title light-text">
                                <?php echo stripslashes($plan['title']); ?>
                            </div>
                            <div class="dcol-2 payment-date">
                                <span class="little-bg-box background-color-green"><?php echo date('n/j/Y', strtotime($member['dt_expires'])); ?></span>
                            </div>
                            <div class="dcol-2 payment-amount bold-text align-right">
                                $<?php echo number_format($price, 2); ?>
                            </div>
                        </div>

                        <?php

                    }

                    ?>

                </div>

                <?php

            // NO RESULTS
            } else {
                ?>
                <div class="no-results">
                    No upcoming membership payments found.
                </div>
                <?php
            }

            ?>

        </div>
    </div>
</div>