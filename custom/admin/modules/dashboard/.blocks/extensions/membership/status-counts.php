<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// NO MEMBERSHIP EXTENSION
if (!class_exists('uccms_Membership')) {
    return;
}

if (!$_uccms_membership) $_uccms_membership = new uccms_Membership();

// NO ACCESS
if (!$_uccms_membership->adminModulePermission()) {
    return;
}

// GET SUBMITTED
$submitted_query = "SELECT * FROM `" .$_uccms_membership->tables['account_plans']. "` WHERE (`status`=2) GROUP BY `account_id`";
$submitted_q = sqlquery($submitted_query);
$num_submitted = sqlrows($submitted_q);

// GET ACTIVE MEMBERS
$active_query = "SELECT * FROM `" .$_uccms_membership->tables['account_plans']. "` WHERE (`status`=1) GROUP BY `account_id`";
$active_q = sqlquery($active_query);
$num_active = sqlrows($active_q);

// GET EXPIRED
$expired_query = "SELECT * FROM `" .$_uccms_membership->tables['account_plans']. "` WHERE (`status`=7) GROUP BY `account_id`";
$expired_q = sqlquery($expired_query);
$num_expired = sqlrows($expired_q);

/*
// GET INACTIVE
$inactive_query = "SELECT * FROM `" .$_uccms_membership->tables['account_plans']. "` WHERE (`status`=0) GROUP BY `account_id`";
$inactive_q = sqlquery($inactive_query);
$num_inactive = sqlrows($inactive_q);
*/

// GET YTD REVENUE
$ytd_query = "SELECT SUM(`amount`) AS `total` FROM `" .$_uccms_membership->tables['transaction_log']. "` WHERE (`dt`>='" .date('Y'). "-01-01 00:00:00') AND (`status`='charged')";
$ytd_q = sqlquery($ytd_query);
$ytd = sqlfetch($ytd_q);

###########################################################

?>

<style type="text/css">

    #uccmsDashboard .dblock.Membership_status-counts .members .member [class*="dcol-"] {
        padding-left: 0px;
    }
    #uccmsDashboard .dblock.Membership_status-counts .members .member .order-number {

    }
    #uccmsDashboard .dblock.Membership_status-counts .members .member .member-name {

    }

</style>

<div class="dblock Membership_status-counts dcol-6">
    <div class="contain">
        <div class="top float-right">
            <div class="stat horizontal large bold light-text">
                <span class="title">YTD:</span>
                <span class="value">$<?php echo number_format($ytd['total'], 2); ?></span>
            </div>
        </div>
        <h2><a href="/admin/<?php echo $_uccms_membership->Extension; ?>*membership/">Membership</a></h2>
        <h3>Statuses</h3>
        <div class="content">

            <?php

            // HAVE RESULTS
            if (($num_submitted > 0) || ($num_active > 0) || ($num_expired > 0) || ($num_inactive > 0)) {

                ?>

                <div class="statuses drow num-boxes">
                    <div class="status submitted box border-color-lightblue dcol-4">
                        <span class="title">Submitted</span>
                        <span class="num"><?php echo number_format($num_submitted, 0); ?></span>
                        <a href="/admin/<?php echo $_uccms_membership->Extension; ?>*membership/members/?status=submitted" class="view-all">View</a>
                    </div>
                    <div class="status active box border-color-green dcol-4">
                        <span class="title">Active</span>
                        <span class="num"><?php echo number_format($num_active, 0); ?></span>
                        <a href="/admin/<?php echo $_uccms_membership->Extension; ?>*membership/members/?status=active" class="view-all">View</a>
                    </div>
                    <div class="status expired box border-color-red dcol-4">
                        <span class="title">Expired</span>
                        <span class="num"><?php echo number_format($num_expired, 0); ?></span>
                        <a href="/admin/<?php echo $_uccms_membership->Extension; ?>*membership/members/?status=expired" class="view-all">View</a>
                    </div>
                </div>

                <div class="view-all align-center">
                    <a href="/admin/<?php echo $_uccms_membership->Extension; ?>*membership/members/" class="btn small">View all</a>
                </div>

                <?php

            // NO RESULTS
            } else {
                ?>
                <div class="no-results">
                    No members found.
                </div>
                <?php
            }

            ?>

        </div>
    </div>
</div>