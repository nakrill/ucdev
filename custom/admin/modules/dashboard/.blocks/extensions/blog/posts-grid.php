<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// NO blog EXTENSION
if (!class_exists('uccms_Blog')) {
    @include(dirname(__FILE__). '/upsell.php');
    return;
}

if (!$_uccms_blog) $_uccms_blog = new uccms_Blog();

// NO ACCESS
if (!$_uccms_blog->adminModulePermission()) {
    return;
}

// LIMIT
$limit = ($block_config['limit'] ? (int)$block_config['limit'] : 3);

/*
// GET LATEST POSTS
$posts_query = "SELECT * FROM `" .$_uccms_blog->tables['posts']. "` WHERE (`status`=1) ORDER BY `dt_published` DESC LIMIT " .$limit;
$posts_q = sqlquery($posts_query);

$num_posts = sqlrows($posts_q);
*/

// GET LATEST POSTS
$pda = $_uccms_blog->getPosts(array(
    'limit' => $limit
));

$num_posts = count((array)$pda['posts']);

###########################################################

?>

<style type="text/css">

    #uccmsDashboard .dblock.Blog_posts-grid .posts .post h4 {
        height: 49px;
        overflow: hidden;
    }

</style>

<div class="dblock Blog_posts-grid cards nobg col-lg-12">
    <div class="contain">
        <?php if ($num_posts > 0) { ?>
            <div class="view-all top float-right">
                <a href="/admin/<?php echo $_uccms_blog->Extension; ?>*blog/posts/" class="btn btn-sm">View all</a>
            </div>
        <?php } ?>
        <h2><a href="/admin/<?php echo $_uccms_blog->Extension; ?>*blog/">Blog</a></h2>
        <h3>Latest Posts</h3>
        <div class="content">

            <?php

            // HAVE RESULTS
            if ($num_posts > 0) {

                ?>

                <div class="posts row">

                    <?php

                    // LOOP
                    foreach ($pda['posts'] as $post) {

                        ?>

                        <div class="post-col col-lg-<?php echo ($block_config['col-size'] ? $block_config['col-size'] : 3); ?>">
                            <div class="post dblock card" data-link="/admin/<?php echo $_uccms_blog->Extension; ?>*blog/posts/edit/?id=<?php echo $post['id']; ?>">
                                <?php if ($post['images'][0]['image']) { ?>
                                    <img class="card-img-top" src="<?php echo $_uccms_blog->imageBridgeOut($post['images'][0]['image'], 'posts'); ?>" alt="">
                                <?php } ?>
                                <div class="card-body with-bottom-block">
                                    <h5 class="card-title"><?php echo stripslashes($post['title']); ?></h5>
                                    <p class="card-text"><?php echo substrWord(strip_tags(stripslashes($post['content'])), 100, '..'); ?></p>
                                    <div class="bottom-block-links row">
                                        <a class="link edit col-md-6" href="#"><?php echo $_uccms_blog->statuses[$post['status']]; ?></a>
                                        <a class="link view col-md-6" href="<?php echo $_uccms_blog->postURL($post['id']); ?>">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php

                    }

                    ?>

                </div>

                <?php

            // NO RESULTS
            } else {
                ?>
                <div class="no-results">
                    No posts found.
                </div>
                <?php
            }

            ?>

        </div>
    </div>
</div>