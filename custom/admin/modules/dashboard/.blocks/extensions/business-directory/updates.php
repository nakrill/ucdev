<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// NO BUSINESS DIRECTORY EXTENSION
if (!class_exists('uccms_BusinessDirectory')) {
    return;
}

if (!$_uccms_businessdir) $_uccms_businessdir = new uccms_BusinessDirectory();

// NO ACCESS
if (!$_uccms_businessdir->adminModulePermission()) {
    return;
}

// BUSINESSES ARRAY
$businessa = array();

// LAST CREATED
$lcreated_query = "SELECT *, `created_dt` AS `sort_by`, 'created' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`created_dt`!='0000-00-00 00:00:00') AND (`pending`=0) ORDER BY `created_dt` DESC LIMIT 10";
$lcreated_q = sqlquery($lcreated_query);
while ($lcreated = sqlfetch($lcreated_q)) {
    $businessa[] = $lcreated;
}

// LAST SUBMITTED
$lsubmitted_query = "SELECT *, `submitted_dt` AS `sort_by`, 'submitted' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`submitted_dt`!='0000-00-00 00:00:00') ORDER BY `submitted_dt` DESC LIMIT 10";
$lsubmitted_q = sqlquery($lsubmitted_query);
while ($lsubmitted = sqlfetch($lsubmitted_q)) {
    $businessa[] = $lsubmitted;
}

// LAST UPDATED
$lupdated_query = "SELECT *, `updated_dt` AS `sort_by`, 'updated' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`updated_dt`!='0000-00-00 00:00:00') ORDER BY `updated_dt` DESC LIMIT 10";
$lupdated_q = sqlquery($lupdated_query);
while ($lupdated = sqlfetch($lupdated_q)) {
    $businessa[] = $lupdated;
}

/*
// LAST DELETED
$ldeleted_query = "SELECT *, `deleted_dt` AS `sort_by`, 'deleted' AS `item_what` FROM `" .$_uccms_businessdir->tables['businesses']. "` WHERE (`deleted_dt`!='0000-00-00 00:00:00') ORDER BY `deleted_dt` DESC LIMIT 10";
$ldeleted_q = sqlquery($ldeleted_query);
while ($ldeleted = sqlfetch($ldeleted_q)) {
    $businessa[] = $ldeleted;
}
*/

$num_businesses = count($businessa);

###########################################################

?>

<style type="text/css">

    #uccmsDashboard .dblock.Business-Directory_Updates .businesses .business [class*="dcol-"] {
        padding-left: 0px;
    }
    #uccmsDashboard .dblock.Business-Directory_Updates .businesses .business .order-number {

    }
    #uccmsDashboard .dblock.Business-Directory_Updates .businesses .business .business-name {

    }

</style>

<div class="dblock Business-Directory_Updates col-md-12">
    <div class="card">
        <div class="card-header">
            <a href="../<?php echo $extension; ?>*business-directory/" class="h5">Business Directory Updates</a>
            <?php if ($num_businesses > 0) { ?>
                <div class="view-all top float-right">
                    <a href="/admin/<?php echo $_uccms_businessdir->Extension; ?>*business-directory/businesses/" class="btn small">View all</a>
                </div>
            <?php } ?>
        </div>
        <div class="card-body">

            <?php

            // HAVE BUSINESSES
            if ($num_businesses > 0) {

                ?>

                <div class="businesses ditems alt-bg-odd">

                    <?php

                    // SORT BY DATE/TIME ASCENDING
                    usort($businessa, function($a, $b) {
                        return strtotime($b['sort_by']) - strtotime($a['sort_by']);
                    });

                    // STATUSES
                    $statusa = array(
                        'created'   => array(
                            'title'     => 'Created',
                            'icon'      => 'fa-plus-circle',
                            'color'     => 'lightblue',
                        ),
                        'submitted'     => array(
                            'title'     => 'Submitted',
                            'icon'      => 'check-circle',
                            'color'     => 'blue',
                        ),
                        'updated' => array(
                            'title'     => 'Updated',
                            'icon'      => 'fa-floppy-o',
                            'color'     => 'green',
                        ),
                        'deleted'   => array(
                            'title'     => 'Deleted',
                            'icon'      => 'fa-times-circle',
                            'color'     => 'red'
                        )
                    );

                    $i = 1;

                    // LOOP
                    foreach ($businessa as $business) {
                        $actiona = array(
                            $business['id']
                        );
                        ?>
                        <div class="ditem drow ditem">
                            <?php /*if ($statusa[$business['item_what']]['icon']) { ?>
                                <div class="icon" style="<?php if ($statusa[$business['item_what']]['color']) { echo 'background-color: ' .$statusa[$business['item_what']]['color']; } ?>"><i class="fa fa-fw <?php echo $statusa[$business['item_what']]['icon']; ?>"></i></div>
                            <?php }*/ ?>
                            <div class="dcol-5 business-name bolder-text">
                                <a href="/admin/<?php echo $_uccms_businessdir->Extension; ?>*business-directory/businesses/edit/?id=<?php echo stripslashes($business['id']); ?>"><?php echo substrWord(stripslashes($business['name']), 50, '..'); ?></a>
                            </div>
                            <div class="dcol-4 dt">
                                <?php echo date('j M g:i A', strtotime($business['sort_by'])); ?>
                            </div>
                            <div class="dcol-3 status align-right">
                                <span class="little-bg-box background-color-<?php echo $statusa[$business['item_what']]['color']; ?>">
                                    <?php echo $statusa[$business['item_what']]['title']; //if ($business['pending']) { echo ' <span style="color: #ff8a80;">(pending)</span>'; } ?>
                                </span>
                            </div>
                        </div>
                        <?php
                        if ($i == 5) {
                            break;
                        } else {
                            $i++;
                        }
                    }

                    unset($businessa);

                    ?>

                </div>

                <?php

            // NO RESULTS
            } else {
                ?>
                <div class="no-results">
                    No business listing updates.
                </div>
                <?php
            }

            ?>

        </div>
    </div>
</div>