<?php

//echo print_r($bigtree["nav_tree"]);

$bigtree['nav_tree']['dashboard']['children']['drilldown'] = array(
    'title' => 'Drilldown',
    'link' => 'dashboard/drilldown',
    'icon' => 'page',
    'nav_icon' => 'pages',
    'children' => array(
        'payments' => array(
            'title' => 'Payments',
            'link' => 'dashboard/drilldown/payments',
            'nav_icon' => 'list',
        )
    )
);

//echo print_r($bigtree["nav_tree"]);
//exit;

$bigtree['page_override'] = array(
    'navigation' => array(
        array(
            'title' => 'Payments',
            'link'  => 'dashboard/drilldown/payments',
            'hidden' => false
        ),
    ),
);


$perPage = 15;


?>

<style type="text/css">


</style>

<script type="text/javascript">

    var drilldownData = {};

    $(function() {

        // INITIAL PAGE
        this_drilldownChangePage();

        // SEARCH / FILTER CHANGE
        $('#form_search-filter input, #form_search-filter select').change(function() {
            this_getDrilldownData({}, $('#form_search-filter').serialize(), function() {
                this_drilldownChangePage();
            });
        });

        // PAGE CLICK
        $('#uccmsDashboard .drilldown').on('click', '#view_paging a', function(e) {
            if ($(this).hasClass('active') || $(this).hasClass('disabled')) {
                return false;
            }
            this_drilldownChangePage(BigTree.cleanHref($(this).attr('href')));
        });

    });


    // PAGE CHANGE
    function this_drilldownChangePage(page, stopLoop) {

        if ((!page) || (typeof page == 'undefined')) page = 1;
        page = parseInt(page);

        var num_results = Object.keys(drilldownData).length;

        if ((num_results == 0) && (!stopLoop)) {
            this_getDrilldownData({}, {}, function() {
                this_drilldownChangePage(page, true);
            });
            return false;
        }

        $('#uccmsDashboard .drilldown .results.ditems .result:not(.template)').remove();

        if (num_results > 0) {

            $('#uccmsDashboard .drilldown .results .no-results').hide();

            var offset = parseInt((page - 1) * <?php echo $perPage; ?>);

            $.each(drilldownData, function(i, v) {

                if ((i >= offset) && (i <= (offset + <?php echo $perPage; ?>))) {

                    var $el = $('#uccmsDashboard .drilldown .results.ditems .result.template').clone();

                    if (v.number) $el.find('.order-number[class*="dcol-"]').text(v.number);
                    if (v.dt) $el.find('.date[class*="dcol-"]').text(v.dt);
                    if (v.name) $el.find('.name[class*="dcol-"]').text(v.name);
                    if (v.method) $el.find('.payment-method[class*="dcol-"]').text(v.method);
                    if (v.description) $el.find('.description[class*="dcol-"]').text(v.description);
                    if (v.amount) $el.find('.amount[class*="dcol-"]').addClass(v.success ? 'text-color-green' : 'text-color-red').text(v.amount);

                    $el.attr('data-id', v.number);
                    if ((typeof v.url == 'object') && (v.url.edit)) {
                        $el.attr('href', v.url.edit);
                        $el.attr('data-link', v.url.edit);
                    }

                    $el.insertBefore('#uccmsDashboard .drilldown .results.ditems .result.template').removeClass('template').show();

                }

            });

            BigTree.setPageCount('#view_paging', Math.ceil(num_results / <?php echo $perPage; ?>), page);

        } else {
            $('#uccmsDashboard .drilldown .results .result').hide();
            $('#uccmsDashboard .drilldown .results .no-results').show();
        }

    }

    // GET DRILLDOWN DATA
    function this_getDrilldownData(configIn, paramsIn, callback) {

        var config = {};
        if (typeof configIn != 'object') configIn = {};
        $.extend(config, configIn);

        params = {
            limit: 1000
        };
        if (typeof paramsIn == 'string') {
            paramsIn = JSON.parse('{"' +decodeURI(paramsIn).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"')+ '"}');
        }
        if (typeof paramsIn != 'object') paramsIn = {};
        $.extend(params, paramsIn);

        // GET PAYMENT ITEMS
        $.get('<?php echo ADMIN_ROOT; ?>ajax/modules/dashboard/blocks/payments/', params, function(data) {
            drilldownData = data;
            if (typeof callback == 'function') {
                callback();
            }
        }, 'json');

    }

</script>

<form id="form_search-filter" action="" method="get">

<h3 class="uccms_toggle" data-what="search-filter"><span>Filter</span><span class="icon_small icon_small_caret_down"></span></h3>
<div class="search-filter uccms_toggle-search-filter" style="display: none;">

    <div class="drow">

        <div class="dcol-6">

            <fieldset>
                <label>Timeframe</label>

                <div class="dates contain">
                    <div class="from contain">
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'date_from',
                            'key'       => 'date_from',
                            'value'     => ($_REQUEST['date_from'] ? date('n/j/Y', strtotime($_REQUEST['date_from'])) : ''),
                            'required'  => false,
                            'options'   => array(
                                'default_now'   => false
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>
                    <div class="div">-</div>
                    <div class="to contain">
                        <?php

                        // FIELD VALUES
                        $field = array(
                            'id'        => 'date_to',
                            'key'       => 'date_to',
                            'value'     => ($_REQUEST['date_to'] ? date('n/j/Y', strtotime($_REQUEST['date_to'])) : ''),
                            'required'  => false,
                            'options'   => array(
                                'default_now'   => false
                            )
                        );

                        // INCLUDE FIELD
                        include(BigTree::path('admin/form-field-types/draw/date.php'));

                        ?>
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="dcol-6">

            <fieldset>
                <label>Extension</label>
                <select name="module_id">
                    <option value="">All</option>
                    <?php foreach ($_extglobals->extensions(array('must-have-methods' => array('payment_transactions'))) as $extension) { ?>
                        <option value="<?php echo $extension['id']; ?>"><?php echo stripslashes($extension['name']); ?></option>
                    <?php } ?>
                </select>
            </fieldset>

        </div>

    </div>

</div>

<div class="table">
    <summary>
        <input name="q" value="<?php echo $_REQUEST['q']; ?>" type="search" placeholder="Search" class="form_search" />
        <nav id="view_paging" class="view_paging"></nav>
    </summary>
    <? /*
    <header>
        <span class="view_column" style="width: 114px;">Date Submitted</span>
        <span class="view_column" style="width: 166px;">First Name</span>
        <span class="view_column" style="width: 166px;">Last Name</span>
        <span class="view_column" style="width: 166px;">Email Address</span>
        <span class="view_column" style="width: 166px;">Message</span>
        <span class="view_actions" style="width: 80px;">Actions</span>
    </header>
    */ ?>

    <div class="results-container">

        <div class="results ditems alt-bg-even contain">

            <a href="#" class="result drow ditem template">
                <span class="dcol-1 order-number light-text">&nbsp;</span>
                <span class="dcol-2 date">&nbsp;</span>
                <span class="dcol-3 name bolder-text">&nbsp;</span>
                <span class="dcol-2 payment-method light-text">&nbsp;</span>
                <span class="dcol-3 description">&nbsp;</span>
                <span class="dcol-1 amount bold-text align-right">&nbsp;</span>
            </a>

        </div>

        <div class="no-results" style="display: none;">
            No payments recently received.
        </div>

    </div>

</div>

</form>