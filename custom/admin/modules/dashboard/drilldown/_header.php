<?php


###########################################################

?>

<style type="text/css">

    #uccmsDashboard .drilldown {
        margin: 0 15px;
    }
    #uccmsDashboard .drilldown .search-filter {
        margin: 15px 0;
    }
    #uccmsDashboard .drilldown .search-filter input[type="text"] {
        width: 95%;
    }
    #uccmsDashboard .drilldown .search-filter .dates > div {
        display: inline-block;
        line-height: 30px;
        vertical-align: text-bottom;
    }
    #uccmsDashboard .drilldown .search-filter .dates input {
        width: 100px !important;
    }
    #uccmsDashboard .drilldown .paging-container {
        margin-top: 10px;
    }
    #uccmsDashboard .drilldown .results-container {
        background-color: #fff;
    }
    #uccmsDashboard .drilldown .results.ditems .result {
        display: block;
    }
    #uccmsDashboard .drilldown .results.ditems .result.template {
        display: none;
    }
    #uccmsDashboard .drilldown .results.ditems .result span[class*="dcol-"] {
        display: block;
        line-height: 1.4em;
        color: #666;
    }

</style>

<script type="text/javascript">

    $(function() {


    });

</script>

<div class="drilldown">