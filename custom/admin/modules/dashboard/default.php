<?php

$here = dirname(__FILE__);

$drows = [
    [
        'blocks' => [
            // GOOGLE ANALYTICS
            [
                'id' => 'bigtree-google-analytics',
                'include' => $here. '/.blocks/bigtree-google-analytics.php'
            ]
        ]
    ],
];

// PAYMENT GATEWAY
$gateway = new BigTreePaymentGateway();

// HAVE SERVICE
if ($gateway->Service) {
    array_push($drows,
        [
            'blocks' => [
                // PAYMENTS
                [
                    'id' => 'payments',
                    'include' => $here. '/.blocks/payments.php'
                ]
            ]
        ]
    );
}

array_push($drows,
    [
        'blocks' => [
            // MEMBERSHIP - STATUS COUNTS
            [
                'id' => 'Membership_status-counts',
                'include' => $here. '/.blocks/extensions/membership/status-counts.php'
            ],
            // BUSINESS DIRECTORY: Updates
            [
                'id' => 'BusinessDirectory_updates',
                'include' => $here. '/.blocks/extensions/business-directory/updates.php'
            ]
        ]
    ],
    [
        'blocks' => [
            // FORM BUILDER: Leads
            [
                'id' => 'FormBuilder_leads',
                'include' => $here. '/.blocks/extensions/form-builder/leads.php'
            ],
            // ADS: Ads
            [
                'id' => 'Ads_ads',
                'include' => $here. '/.blocks/extensions/ads/ads.php'
            ]
        ]
    ],
    [
        'blocks' => [
            // FORM BUILDER: Leads
            [
                'id' => 'Blog_posts-grid',
                'config' => [
                    'limit'     => 4,
                    'col-size'  => 3
                ],
                'include' => $here. '/.blocks/extensions/blog/posts-grid.php'
            ]
        ]
    ]
);


?>

<style type="text/css">

    #uccmsDashboard .dblock .view-all.top {
        margin: 0px;
    }

</style>

<script type="text/javascript">

    $(function() {

        equalHeight('#uccmsDashboard .drow .dblock:not(.no-equal-height) .contain');

        // BLOCK CLICK
        $('#uccmsDashboard').on('click', '.dblock[data-link]', function(e) {
            e.preventDefault();
            var link = '';
            if ($(e.target).closest('a').length) {
                var tlink = $(e.target).closest('a').attr('href');
                if ((tlink) && (tlink != '#')) {
                    link = tlink;
                }
            }
            if (!link) {
                var link = $(this).attr('data-link');
                if (!link) link = $(this).attr('href');
            }
            if ((link) && (link != '#')) {
                window.location = link;
            }
        });

        // BLOCK ITEM CLICK
        $('#uccmsDashboard').on('click', '.dblock .ditem[data-link], .dblock a.ditem', function(e) {
            e.preventDefault();
            var link = $(this).attr('data-link');
            if (!link) link = $(this).attr('href');
            if ((link) && (link != '#')) {
                window.location = link;
            }
        });

    });

</script>

<?php

// LOOP THROUGH ROWS
foreach ((array)$drows as $drow) {
    ?>
    <div class="drow row" data-id="<?php echo $drow['id']; ?>">
        <?php
        foreach ((array)$drow['blocks'] as $dblock) {
            $block_config = $dblock['config'];
            @include($dblock['include']);
        }
        ?>
    </div>
    <?php
}

/*
// EXTENSIONS DIRECTORY
$extensions_dir = SERVER_ROOT. 'extensions';

// GET EXTENSIONS
$extensions = array_diff(scandir($extensions_dir), array('..', '.'));

// LOOP THROUGH EXTENSIONS
foreach ((array)$extensions as $extension) {

    // EXTENSION DIRECTORY
    $extension_dir = $extensions_dir. '/' .$extension;

    // DASHBOARD BLOCK FILE
    $block_file = $extension_dir. '/modules/dashboard/block.php';

    // INCLUDE
    @include($block_file);

}
*/

?>