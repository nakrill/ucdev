<form action="" method="get">

<div class="container">

    <header>
        <h2>Business Lookup</h2>
    </header>

    <section>

        <div class="contain">

            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top;">
                        <fieldset>
                            <label>Business Phone</label>
                            <input type="text" value="<?=$_REQUEST['business']['phone']?>" name="business[phone]" style="width: 378px;">
                        </fieldset>
                    </td>
                    <td style="padding: 0 10px; white-space: nowrap;">
                        - or -
                    </td>
                    <td width="50%" style="vertical-align: top;">
                        <fieldset>
                            <label>Business Name</label>
                            <input type="text" value="<?=$_REQUEST['business']['name']?>" name="business[name]" style="width: 378px;">
                        </fieldset>

                        <fieldset>
                            <label>Zip</label>
                            <input type="text" value="<?=$_REQUEST['business']['zip']?>" name="business[zip]" style="width: 40px;">
                        </fieldset>
                    </td>
                </tr>
            </table>

        </div>

    </section>

    <footer>
        <input type="submit" value="Find" class="blue">
    </footer>

</div>

</form>

<?php

if (is_array($_REQUEST['business'])) {

    //echo print_r($_REQUEST['business']);

    // HAVE REQUIRED INFO
    if (($_REQUEST['business']['phone']) || (($_REQUEST['business']['name']) && ($_REQUEST['business']['zip']))) {

        // BUSINESS INFO
        $business_info = array(
            'Phone'         => $_REQUEST['business']['phone'], // char: 10
            'BusinessName'  => $_REQUEST['business']['name'], // max char: 100
            'Zip'           => $_REQUEST['business']['zip'] // char: 5
        );

        // GET BUSINESS
        $data = $_localeze->getBusiness($business_info);

        // HAVE ERROR
        if ($data['Response']['Error'] > 0) {

            echo 'Error: ' .$data['Response']['Message'];

        // NO ERROR
        } else {

            // HAVE MATCHES
            if (count($data) > 0) {

                ?>

                <div class="table" id="results">
                    <summary>
                        <h2>Results</h2>
                    </summary>

                    <ul class="items">

                        <?php

                        // LOOP
                        foreach ($data as $business_info) {

                            // HAVE PID
                            if ($business_info['PID']) {

                                // PARSE BUSINESS INFO
                                $business = $_blisting->localeze_parseBusinessInfo($business_info, $_blisting->getOurBusinessData());

                                ?>

                                <li class="item contain" style="height: auto; line-height: 1em;">

                                    <table width="100%" style="margin-bottom: 0px; border: 0px none;">
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <div style="width: 110px; padding-right: 25px; overflow: hidden; text-align: center;">
                                                    <?php if ($business['logo_url']) { ?>
                                                        <img src="//<?=$business['logo_url']?>" alt="" style="max-width: 110px; height: auto;" />
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td style="width: 50%; vertical-align: top;">
                                                <div style="padding-bottom: 8px; font-size: 1.2em; font-weight: bold;">
                                                    <?=$business['name']?>
                                                </div>
                                                <div style="padding-bottom: 4px;">
                                                    <?=$business['address']?>
                                                </div>
                                                <div>
                                                    <?=$business['city']?>, <?=$business['state']?> <?=$business['zip']?>
                                                </div>
                                            </td>
                                            <td style="width: 50%; vertical-align: top;">
                                                <div style="padding-bottom: 8px; font-size: 1.2em;">
                                                    <?=$_blisting->prettyPhone($business['phone']['main'])?>
                                                </div>
                                                <?php if ($business['phone']['tollfree']) { ?>
                                                    <div style="padding-bottom: 4px;">
                                                        <?=$_blisting->prettyPhone($business['phone']['tollfree'])?>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($business['url']) { ?>
                                                    <div>
                                                        <a href=""><?=$business['url']?></a>
                                                    </div>
                                                <?php } ?>
                                            </td>
                                            <td style="vertical-align: top; white-space: nowrap;">
                                                <a href="./set-pid/?pid=<?=$business_info['PID']?>" class="button" style="height: auto;" onclick="return confirmPrompt(this.href, 'Are you sure you want to claim this?');">Claim Business</a>
                                            </td>
                                        </tr>
                                    </table>

                                </li>

                                <?php

                            }

                        }

                        ?>

                        <li class="item contain" style="height: auto; padding: 20px 0; text-align: center; line-height: 1em;">
                            <div style="padding-bottom: 10px; font-size: 1.3em; opacity: .6;">
                                Can't find your business?
                            </div>
                            <a href="./set-pid/?pid=n" class="button" style="height: auto;" onclick="return confirmPrompt(this.href, 'Are you sure you want to create a new listing?');">Create New Listing</a>
                        </li>

                    </ul>

                </div>

                <?php

            // NO MATCHES
            } else {
                ?>
                <b>No matches found.</b>
                <br />
                <br />
                <a href="./set-pid/?pid=n" class="button" style="height: auto;" onclick="return confirmPrompt(this.href, 'Are you sure you want to create a new listing?');">Create New Listing</a>
                <?php
            }

        }

        ?>

        <? /*
        <ul>
            <li>List of results.</li>
            <li>Select button on available ones.</li>
            <li>Figure out how we "own" it when processing.</li>
            <li>Save PID when processing.</li>
        </ul>
        */ ?>



        <?php

    // MISSING REQUIRED INFO
    } else {
        ?>
        Please specify a phone number, or business name and zip code.
        <?php
    }

}

?>