<style type="text/css">
    span.required {
        color: red;
    }
    .select {
        display: inline-block;
        float: none;
    }
    #categories .category.contain {
        border-bottom: 1px solid #fff;
    }
    #categories .subs_selected .item {
        padding-bottom: 5px;
    }
    #categories .subs_selected .item:last-child {
        padding-bottom: 0px;
    }
</style>

<script type="text/javascript">

    $(function() {

        // CATEGORY SELECT
        $('#categories select.category').change(function() {
            this_getSubCategories($(this).attr('data-what'), $(this).find('option:selected').text());
        });

        // CATEGORY - ADD ATTRIBUTE
        $('#categories .sub_select').on('click', '.add', function(e) {
            e.preventDefault();
            var what = $(this).attr('data-what');
            var id = $(this).prev('select').val();
            var selected = $(this).prev('select').find('option:selected');
            var name = selected.text();
            var group_id = selected.closest('optgroup').attr('data-id');
            var group_name = selected.closest('optgroup').attr('label');
            $('#categories .category.' +what+ '  .subs_selected').append('<div class="item"><input type="checkbox" name="business[attributes][' +what+ '][' +group_id+ '##' +group_name+ '][' +id+ ']" value="' +name+ '" checked="checked" /><label class="for_checkbox">' +group_name+ ' / ' +name.replace(/\-/g, '').trim()+ '</label></div>');
            BigTreeCustomControls('#categories .category.' +what+ '  .subs_selected');
            $('#categories .category.' +what+ '  .subs_selected').show();
        });

        // HOURS - CLOSED CLICK
        $('#hours .day input.closed').click(function() {
            $(this).closest('.day').find('select').prop('disabled', $(this).prop('checked'));
        });

    });

    // GET SUB-CATEGORIES
    function this_getSubCategories(what, val) {
        $.get('/admin/ajax/modules/settings/business/category/?what=' +what+ '&val=' +val, function(data) {
            $('#categories .category.' +what+ ' .sub_select').html(data).show();
        }, 'html');
    }

</script>

<?php

// GET CATEGORIES
$cata = $_blisting->getCategories('', false);
asort($cata);

// ISN'T A NEW LISTING
if ($pid != 'n') {

    // GET BUSINESS INFO
    $bida = $_localeze->getBusiness(array(
        'PID'   => $pid
    ));

    //echo print_r($bida);
    //exit;

    // GET BUSINESS FIELDS
    $business = $_blisting->localeze_parseBusinessInfo($bida[0], $_blisting->getOurBusinessData());

    //echo print_r($business);
    //exit;

    // CHECK VALIDATION
    $validation = $_localeze->dataValidationStatus($bida);

    // ALL GOOD
    if ($validation['status'] == 'Passed') {

        ?>

        <div style="padding-bottom: 15px; color: green;">
            Data validation passed.
        </div>

        <?php

    // VALIDATION ERRORS
    } else {

        ?>

        <div style="padding-bottom: 15px; color: red;">
            Data validation has issues.
            <ul>
                <?php foreach ($validation['err'] as $err) { ?>
                    <li style="color: orange;"><?=$err?></li>
                <?php } ?>
            </ul>

        </div>

        <?php

    }

// IS NEW LISTING
} else {

}

// HAVE BUSINESS INFO FROM SUBMITTED SESSION
if (is_array($_SESSION['UCCMS']['ADMIN']['BUSINESS_LISTING']['post']['business'])) {
    $business = $_blisting->parseOurData($_SESSION['UCCMS']['ADMIN']['BUSINESS_LISTING']['post']['business']);
}

//echo print_r($business['categories']);
//exit;

// MISSING ARRAYS
if (!is_array($business['hours'])) $business['hours'] = array();
if (!is_array($business['languages'])) $business['languages'] = array();

?>

<form action="./process/" method="post">

<div class="container">

    <header>
        <h2>Business Details</h2>
        <? /*<a class="button" href="">Link</a>*/ ?>
    </header>

    <section>

        <div class="contain">

            <div class="left last">

                <fieldset>
                    <label>Business Name <span class="required">*</span></label>
                    <input type="text" value="<?=$business['name']?>" name="business[name]" />
                </fieldset>

                <fieldset>
                    <label>Address <span class="required">*</span></label>
                    <input type="text" value="<?=$business['address']?>" name="business[address]" />
                </fieldset>

                <fieldset>
                    <label>City <span class="required">*</span></label>
                    <input type="text" value="<?=$business['city']?>" name="business[city]" />
                </fieldset>

                <fieldset>
                    <label>State <span class="required">*</span></label>
                    <select name="business[state]">
                        <option value="">Select</option>
                        <?php foreach ($_blisting->usStates() as $init => $name) { ?>
                            <option value="<?=$init?>" <?php if ($init == $business['state']) { ?>selected="selected"<?php } ?>><?=$name?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Zip</label>
                    <input type="text" value="<?=$business['zip']?>" name="business[zip]" style="display: inline; width: 40px;" /><span class="required">*</span> - <input type="text" value="<?=$business['zip_4']?>" name="business[zip_4]" style="display: inline; width: 30px;" />
                </fieldset>

                <? /*
                <fieldset>
                    <label>Department</label>
                    <input type="text" value="<?=$business['department']?>" name="business[department]" />
                </fieldset>
                */ ?>

                <fieldset>
                    <label>Email</label>
                    <input type="text" value="<?=$business['email']?>" name="business[email]" />
                </fieldset>

                <fieldset>
                    <label>Phone <span class="required">*</span></label>
                    <input type="text" value="<?=$business['phone']['main']?>" name="business[phone][main]" />
                </fieldset>

                <fieldset>
                    <label>Fax</label>
                    <input type="text" value="<?=$business['fax']?>" name="business[fax]" />
                </fieldset>

                <fieldset>
                    <label>Toll-Free Number</label>
                    <input type="text" value="<?=$business['phone']['tollfree']?>" name="business[phone][tollfree]" />
                </fieldset>

                <fieldset>
                    <label>Alternate Number</label>
                    <input type="text" value="<?=$business['phone']['alt']?>" name="business[phone][alt]" />
                </fieldset>

                <fieldset>
                    <label>Mobile</label>
                    <input type="text" value="<?=$business['phone']['mobile']?>" name="business[phone][mobile]" />
                </fieldset>

                <fieldset>
                    <label>Website URL</label>
                    <input type="text" value="<?=$business['url']?>" name="business[url]" placeholder="www.domain.com" />
                </fieldset>

                <fieldset>
                    <label>Logo URL</label>
                    <input type="text" value="<?=$business['logo_url']?>" name="business[logo_url]" placeholder="www.domain.com/logo.jpg" />
                </fieldset>

            </div>

            <div class="right last">

                <fieldset>
                    <label>Tagline</label>
                    <input type="text" value="<?=$business['tagline']?>" name="business[tagline]" />
                </fieldset>

                <fieldset>
                    <label>Keywords</label>
                    <textarea name="business[keywords]" style="height: 60px;"><?=$business['keywords']?></textarea>
                </fieldset>

                <fieldset>
                    <label>Year Opened</label>
                    <select name="business[year_opened]">
                        <?php for ($i=date('Y'); $i>=1700; $i--) { ?>
                            <option value="<?=$i?>" <?php if ($i == $business['year_opened']) { ?>selected="selected"<?php } ?>><?=$i?></option>
                        <?php } ?>
                    </select>
                </fieldset>

                <fieldset>
                    <label>Languages Spoken</label>
                    <?php foreach ($_localeze->localeze_languages() as $lid => $name) { ?>
                        <input type="checkbox" name="business[languages][<?=$lid?>]" value="1" <?php if (in_array($lid, $business['languages'])) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                    <?php } ?>
                </fieldset>

                <fieldset>
                    <label>Payment Methods Accepted</label>
                    <?php foreach ($_localeze->localeze_paymentMethods() as $pmid => $name) { ?>
                        <input type="checkbox" name="business[payment_methods][<?=$pmid?>]" value="1" <?php if ($business['payment_methods'][$pmid]) { ?>checked="checked"<?php } ?> /> <label class="for_checkbox"><?=$name?></label>
                    <?php } ?>
                </fieldset>

                <fieldset>
                    <label>LinkedIn URL</label>
                    <input type="text" value="<?=$business['social']['linkedin']?>" name="business[social][linkedin]" placeholder="www.linkedin.com/company/companyname" />
                </fieldset>

                <fieldset>
                    <label>Yelp</label>
                    <input type="text" value="<?=$business['social']['yelp']?>" name="business[social][yelp]" placeholder="www.yelp.com/biz/name" />
                </fieldset>

                <fieldset>
                    <label>Foursquare URL</label>
                    <input type="text" value="<?=$business['social']['foursquare']?>" name="business[social][foursquare]" placeholder="www.foursquare.com/v/name/value" />
                </fieldset>

                <fieldset>
                    <label>Facebook URL</label>
                    <input type="text" value="<?=$business['social']['facebook']?>" name="business[social][facebook]" placeholder="www.facebook.com/pagename" />
                </fieldset>

                <fieldset>
                    <label>Twitter URL</label>
                    <input type="text" value="<?=$business['social']['twitter']?>" name="business[social][twitter]" placeholder="www.twitter.com/username" />
                </fieldset>

                <fieldset>
                    <label>Google+</label>
                    <input type="text" value="<?=$business['social']['googleplus']?>" name="business[social][googleplus]" placeholder="plus.google.com/+name" />
                </fieldset>

            </div>

        </div>

    </section>

</div>

<div id="categories" class="container">

    <header>
        <h2>Categories</h2>
    </header>

    <section>

        <?php

        $catha = array(
            0 => array(
                'name'  => 'primary',
                'title' => 'Primary Category'
            ),
            1 => array(
                'name'  => 'alt1',
                'title' => 'Alternate Category 1'
            ),
            2 => array(
                'name'  => 'alt2',
                'title' => 'Alternate Category 2'
            )
        );

        foreach ($catha as $cath_id => $cath) {

            // HAVE KEYWORDS
            if ((is_array($business['categories'][$cath['name']]['Attributes'])) && (count($business['categories'][$cath['name']]['Attributes']) > 0)) {
                $have_keywords = true;
            } else {
                $have_keywords = false;
            }

            ?>

            <div class="category <?=$cath['name']?> contain" style="padding: 15px; background-color: #f5f5f5; border-radius: 2px;">

                <h3><?=$cath['title']?><?php if ($cath_id == 0) { ?> <span class="required">*</span><?php } ?></h3>

                <?php
                //echo strtoupper($business['categories'][$cath['name']]['Name']);
                ?>

                <select name="business[categories][<?=$cath['name']?>]" class="custom_control category" data-what="<?=$cath['name']?>">
                    <option value=""><?php if ($cath['name'] == 'primary') { echo 'Required'; } else { echo 'Select'; } ?></option>
                    <?php foreach ($cata as $cat_id => $cat_title) { ?>
                        <option value="<?=$cat_id?>##<?=$cat_title?>" <?php if (strtoupper($cat_title) == strtoupper($business['categories'][$cath['name']]['Name'])) { echo 'selected="selected"'; } ?>><?=ucwords(strtolower($cat_title))?></option>
                    <?php } ?>
                </select>

                <div class="sub_select" style="<?php if (!$business['categories'][$cath['name']]['Name']) { ?>display: none;<?php } ?> padding-top: 15px;">
                    <?php if ($business['categories'][$cath['name']]['Name']) { ?>
                        Loading..
                        <script type="text/javascript">
                            $(function() {
                                this_getSubCategories('<?=$cath['name']?>', '<?=$business['categories'][$cath['name']]['Name']?>');
                            });
                        </script>
                    <?php } ?>
                </div>

                <div class="subs_selected" style="<?php if (!$have_keywords) { ?>display: none;<?php } ?> padding-top: 15px;">

                    <?php
                    if ($have_keywords) {
                        unset($keywords);
                        if ($business['categories'][$cath['name']]['Attributes']['Attribute'][0]['ID']) {
                            $keywords = $business['categories'][$cath['name']]['Attributes']['Attribute'];
                        } else {
                            $keywords[] = $business['categories'][$cath['name']]['Attributes']['Attribute'];
                        }
                        foreach ($keywords as $keyword) {
                            ?>
                            <div class="item"><input type="checkbox" name="business[attributes][<?=$cath['name']?>][<?=$keyword['Group']['ID']?>##<?=$keyword['Group']['Name']?>][<?=$keyword['ID']?>]" value="<?=$keyword['Name']?>" checked="checked" /><label class="for_checkbox"><?=$keyword['Group']['Name']?> / <?=$keyword['Name']?></label></div>
                            <?php
                        }
                        unset($keywords);
                    }
                    ?>

                </div>

            </div>

            <?php

        }

        ?>

    </section>

</div>

<div id="hours" class="container">

    <header>
        <h2>Hours Of Operation</h2>
    </header>

    <section>

        <div class="contain">

            <table style="margin: 0px; border: 0px;">
                <?php

                // LOOP THROUGH DAYS OF WEEK
                foreach ($_localeze->localeze_daysOfWeek() as $day_code => $day_name) {

                    $closed = false;

                    // GET DAY FROM BUSINESS HOURS
                    foreach ($business['hours'] as $hours) {
                        if ($hours['day_start'] == $day_code) {
                            $this_day = $hours;
                            if (($hours['closed']) || (($hours['time_start'] == '00:00') && ($hours['time_end'] == '00:00'))) {
                                $closed = true;
                            }
                            break;
                        }
                    }

                    ?>
                    <tr class="day">
                        <td style="font-weight: bold;"><?=$day_name?></td>
                        <td>
                            <select name="business[hours][<?=$day_code?>][start]" <?php if ($closed) { ?>disabled="disabled"<?php } ?>class="custom_control">
                                <?php
                                for ($i=1; $i<=24; $i++) {
                                    for ($j=0; $j<=45; $j+=15) {
                                        if ($closed) {
                                            $selected = false;
                                        } else {
                                            if (date('H:i', strtotime($i. ':' .$j)) == $this_day['time_start']) {
                                                $selected = true;
                                            } else {
                                                $selected = false;
                                            }
                                        }
                                        ?>
                                        <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            &nbsp;-&nbsp;
                            <select name="business[hours][<?=$day_code?>][end]" <?php if ($closed) { ?>disabled="disabled"<?php } ?> class="custom_control">
                                <?php
                                for ($i=1; $i<=24; $i++) {
                                    for ($j=0; $j<=45; $j+=15) {
                                        if ($closed) {
                                            $selected = false;
                                        } else {
                                            if (date('H:i', strtotime($i. ':' .$j)) == $this_day['time_end']) {
                                                $selected = true;
                                            } else {
                                                $selected = false;
                                            }
                                        }
                                        ?>
                                        <option value="<?=$i. ':' .$j?>" <?php if ($selected) { ?>selected="selected"<?php } ?>><?=date('g:i a', strtotime($i. ':' .$j))?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td><input type="checkbox" name="business[hours][<?=$day_code?>][closed]" value="1" <?php if ($closed) { ?>checked="checked"<?php } ?>class="closed" /> Closed</td>
                    </tr>
                <?php } ?>
            </table>

        </div>

    </section>

</div>

<input type="submit" value="Save" class="blue">

</form>