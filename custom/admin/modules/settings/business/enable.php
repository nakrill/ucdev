<?php

// IS DEVELOPER
if ($admin->Level == 2) {

    // BUSINESS LISTING CLASS'S
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/base.php');
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/admin.php');

    // INIT LOCALEZE CLASS
    $_blisting = new BusinessListing_Admin();

    // ADD / UPDATE SETTING
    $_blisting->updateSetting('enabled', (int)$_POST['enabled'], array(
        'name' => 'Business Listing Enabled'
    ));

    $admin->growl('Business Listing', ($_POST['enabled'] ? 'Enabled' : 'Disabled'));

    // LOCALEZE PID
    $pid = $_blisting->getSetting('localeze-pid');

    // IF WE HAVE A LOCALEZE PID
    if ($pid) {

        // LOCALEZE CLASS
        require_once(SERVER_ROOT. 'uccms/includes/classes/localeze.php');

        // INIT LOCALEZE CLASS
        $_localeze = new Localeze();

        // STATUS INFO
        $status_info = array(
            'status'    => (int)$_POST['enabled'], // 0 = disabled, 1 = active
            'renew'     => (int)$_POST['enabled'] // 0 = no renew, 1 = auto-renew
        );

        // GET BUSINESS INFO
        $bida = $_localeze->getBusiness(array(
            'PID'   => $pid
        ));

        // BUSINESS FOUND
        if ($bida['Records']['Record']['PID'] == $pid) {

            // BUSINESS INFO
            $business_info = array(
                'Phone'         => '(252) 441-1111', // char: 10
                'BusinessName'  => 'Acme 1', // max char: 100
                //'Department'    => '', // optional, max char: 100
                'Address'       => '123 Main St', // max char: 100
                'City'          => 'Kill Devil Hills', // max char: 28
                'State'         => 'NC', // char: 2
                'Zip'           => '27948', // char: 5
                'Categories'    => array(
                    array(
                        'Type'  => 'Primary', // Alt1, Alt2
                        'Name'  => 'Restaurants' // See EID 2935 for Category Names
                    )
                )
            );

            // UPDATE SUBSCRIPTION
            $upsub = $_localeze->updateSubscription($status_info, $business_info);

            // SUCCESSFUL
            if ($upsub['ErrorCode'] == 0) {

                // record status and date in db

               $admin->growl('Localeze', 'Subscription status updated.');

            // FAILED
            } else {
                $admin->growl('Localeze Error', 'Failed to update subscription status. (' .$upsub['Message']. ')');
            }

        }

    }

}

BigTree::redirect(ADMIN_ROOT. 'settings/business/');

?>