<?php

//echo print_r($_POST);
//exit;

// SUBMITTED
if (is_array($_POST)) {

    // SAVE VALUES TO SESSION
    unset($_SESSION['UCCMS']['ADMIN']['BUSINESS_LISTING']['post']);
    $_SESSION['UCCMS']['ADMIN']['BUSINESS_LISTING']['post'] = $_POST;

    // check session
    //BigTree::redirect(ADMIN_ROOT. 'settings/business/');
    //exit;

    // BUSINESS LISTING CLASS'S
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/base.php');
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/admin.php');

    // INIT LOCALEZE CLASS
    $_blisting = new BusinessListing_Admin();

    // ADD / UPDATE SETTING
    $_blisting->updateSetting('business-data', $_POST['business'], array(
        'name' => 'Business Listing - Data'
    ));

    // LOCALEZE PID
    $pid = $_blisting->getSetting('localeze-pid');

    // IF WE HAVE A LOCALEZE PID
    if ($pid) {

        // LOCALEZE CLASS
        require_once(SERVER_ROOT. 'uccms/includes/classes/localeze.php');

        // INIT LOCALEZE CLASS
        $_localeze = new Localeze();

        // BUSINESS INFO
        $business_info = array(
            'Phone'             => $_POST['business']['phone']['main'],
            'BusinessName'      => $_POST['business']['name'],
            //'Department'        => '', // max char: 100
            'Address'           => $_POST['business']['address'],
            'City'              => $_POST['business']['city'],
            'State'             => $_POST['business']['state'],
            'Zip'               => $_POST['business']['zip'],
            'Plus4'             => $_POST['business']['zip_4'],
            'Fax'               => $_POST['business']['fax'],
            'TollFreeNumber'    => $_POST['business']['phone']['tollfree'],
            'AltNumber'         => $_POST['business']['phone']['alt'],
            'MobileNumber'      => $_POST['business']['phone']['mobile'],
            'Categories'        => $_blisting->formatPostCategories($_POST),
            'UnstructuredTerms' => $_POST['business']['keywords'], // keywords (csv)
            'CreditCards'       => array_keys((array)$_POST['business']['payment_methods']), // array of id's
            'URL'               => $_POST['business']['url'], // max char: 2048
            'eMail'             => $_POST['business']['email'], // max char: 320
            'YearOpenned'       => $_POST['business']['year_opened'], // number (char 4)
            'HoursOfOperation'  => $_blisting->formatPostHours($_POST), // array or open 24/7: '00000000'
            'LanguagesSpoken'   => array_keys((array)$_POST['business']['languages']), // array
            'TagLine'           => $_POST['business']['tagline'], // max char: 150
            'LogoImage'         => $_POST['business']['logo_url'], // max char: 128
            //'SharedKey'         => '', // max char: 50 (our internal client id)
            'Social'            => array(
                'Facebook'          => $_POST['business']['social']['facebook'], // max char: 128
                'LinkedIn'          => $_POST['business']['social']['linkedin'], // max char: 128
                'Twitter'           => $_POST['business']['social']['twitter'], // max char: 128
                'GooglePlus'        => $_POST['business']['social']['googleplus'], // max char: 128
                'Yelp'              => $_POST['business']['social']['yelp'], // max char: 128
                'Foursquare'        => $_POST['business']['social']['foursquare'] // max char: 128
            )
        );

        //echo print_r($business_info);
        //exit;

        //$_localeze->debug = false;

        // ADD / UPDATE BUSINESS
        $addup = $_localeze->addUpdateBusiness($business_info);

        //echo print_r($addup);
        //exit;

        // HAVE PID
        if ($addup['PID']) {

            // ADD / UPDATE SETTING
            $_blisting->updateSetting('localeze-pid', $addup['PID'], array(
                'name' => 'Business Listing - Localeze PID'
            ));

        }

        //echo print_r($addup);
        //exit;

        // NO ERROR
        if ($addup['Error'] == 0) {

            /*

            // CHECK VALIDATION
            $validation = $_localeze->dataValidationStatus($addup);

            // ALL GOOD
            if ($validation['status'] == 'Passed') {

                $admin->growl('Business Listing', 'Updated and validated!');

            // VALIDATION ERRORS
            } else {

                $admin->growl('Business Listing', 'Updated but with validation errors.');

                // LOOP THROUGH ERRORS
                foreach ($validation['err'] as $err) {
                    $_SESSION['UCCMS']['ADMIN']['BUSINESS-LISTING']['validation_err'][] = $err;
                    //echo $err['status'] . $err['field'] . $err['message'];
                }

            }

            */

            unset($_SESSION['UCCMS']['ADMIN']['BUSINESS_LISTING']['post']);

            $admin->growl('Business Listing', 'Updated!');

            // STATUS INFO
            $status_info = array(
                'status'    => 1, // 0 = disabled, 1 = active
                'renew'     => 1 // 0 = no renew, 1 = auto-renew
            );

            // BUSINESS INFO
            $business_info = array(
                'Phone'         => $_POST['business']['phone']['main'], // char: 10
                'BusinessName'  => $_POST['business']['name'], // max char: 100
                //'Department'    => '', // optional, max char: 100
                'Address'       => $_POST['business']['address'], // max char: 100
                'City'          => $_POST['business']['city'], // max char: 28
                'State'         => $_POST['business']['state'], // char: 2
                'Zip'           => $_POST['business']['zip'], // char: 5
                'Categories'    => $categories
            );

            // UPDATE SUBSCRIPTION
            $upsub = $_localeze->updateSubscription($status_info, $business_info);

            // SUCCESSFUL
            if ($upsub['ErrorCode'] == 0) {

                // record status and date in db

                // IS DEVELOPER
                if ($admin->Level == 2) {
                    $admin->growl('Business Listing', 'Info and subscription updated.');
                }

            // FAILED
            } else {

                // IS DEVELOPER
                if ($admin->Level == 2) {
                    $admin->growl('Business Listing', 'Info updated but failed to update subscription status. (' .$upsub['Message']. ')');
                }

            }

        // HAVE ERROR
        } else {
            $admin->growl('Business Listing', 'Error: ' .$addup['ErrorMessage']);
        }

    // NO PID
    } else {
        $admin->growl('Business Listing', 'No PID');
    }

}

BigTree::redirect(ADMIN_ROOT. 'settings/business/');

?>