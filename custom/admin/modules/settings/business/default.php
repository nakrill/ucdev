<?php

// LOCALEZE CLASS
require_once(SERVER_ROOT. 'uccms/includes/classes/localeze.php');

// INIT LOCALEZE CLASS
$_localeze = new Localeze();

// LOCALEZE IS ENABLED
if ($_localeze->isEnabled()) {

    $bigtree["breadcrumb"] = array(
        array(
            'link'  => 'settings',
            'title' => 'Settings'
        ),
        array(
            'link'  => 'settings/business',
            'title' => 'Business Listing'
        )
    );

    // BUSINESS LISTING CLASS'S
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/base.php');
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/admin.php');

    // INIT LOCALEZE CLASS
    $_blisting = new BusinessListing_Admin();

    // IS DEVELOPER
    if ($admin->Level == 2) {

        ?>

        <form action="./enable/" method="post">

        <div class="container expand">

            <header>
                <h2>Admin Only</h2>
            </header>

            <section>

                <fieldset>
                    <input type="checkbox" name="enabled" value="1" <?php if ($_blisting->isEnabled() == 1) { echo 'checked="checked"'; } ?> />
                    <label class="checkbox">Business Listing Enabled</label>
                </fieldset>

            </section>

            <footer>
                <input type="submit" value="Save" class="blue">
            </footer>

        </div>

        </form>

        <?php

    }

    // IS ENABLED
    if ($_blisting->isEnabled() == 1) {

        // LOCALEZE PID
        $pid = $_blisting->getSetting('localeze-pid');

        // HAVE PID
        if ($pid) {

            // SHOW EDIT FORM
            include(dirname(__FILE__). '/pages/edit.php');

        // NO PID
        } else {

            include(dirname(__FILE__). '/pages/lookup.php');

        }

    // NOT ENABLED
    } else {

        ?>

        Business listing not enabled for this site.

        <?php

    }

// LOCALEZE NOT ENABLED
} else {

    // IS DEVELOPER
    if ($admin->Level == 2) {
        $admin->growl('Error', 'Localeze not enabled.');
    }

    BigTree::redirect(ADMIN_ROOT);

}

?>