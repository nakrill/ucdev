<?php

// IS DEVELOPER
if ($admin->Level == 2) {

    // CLEAN UP
    if ($_REQUEST['pid'] == 'n') {
        $pid = 'n';
    } else {
        $pid = (int)$_REQUEST['pid'];
    }

    // BUSINESS LISTING CLASS'S
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/base.php');
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/admin.php');

    // INIT LOCALEZE CLASS
    $_blisting = new BusinessListing_Admin();

    // ADD / UPDATE SETTING
    $_blisting->updateSetting('localeze-pid', $pid, array(
        'name' => 'Business Listing - Localeze PID'
    ));

    // mark listing as ours in Localeze

    $admin->growl('Business Listing', ($pid ? 'Business Linked' : 'Business Unlinked'));

}

BigTree::redirect(ADMIN_ROOT. 'settings/business/');

?>