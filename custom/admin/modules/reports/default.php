<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// REPORTS CLASS
require_once(SERVER_ROOT. 'uccms/includes/classes/reports.php');

// INIT CLASS
$_reports = new Reports();




// REVSOCIAL API KEY
$api_key = $cms->getSetting('rs_api-key');

// HAVE API KEY
if ($api_key) {

    // REST CLIENT
    require_once(SERVER_ROOT. '/uccms/includes/classes/restclient.php');

    // INIT REST API CLASS
    $_api = new RestClient(array(
        'base_url' => 'https://api.revsocial.com/v1'
    ));

    // MAKE API CALL
    $result = $_api->get('reports/all', array(
        'api_key'   => $api_key,
        'data'      => array(
            //'active'    => true,
            //'tracking'  => true
        )
    ));

    // API RESPONSE
    $resp = $result->decode_response();

    // HAVE REPORTS
    if (count($resp->reports) > 0) {

        // CLEAN UP
        $report_id = (int)$_REQUEST['id'];

        // REPORT
        $report = $resp->reports->$report_id;

        // REPORT SPECIFIED BUT NOT FOUND
        if (($report_id) && (!$report)) {
            $admin->growl('Error', 'Report not found.');
            BigTree::redirect(ADMIN_ROOT. 'reports/');
        }

        ?>

        <style type="text/css">

            #reports .report {
                margin-top: 15px;
                padding: 15px;
                background-color: #f5f5f5;
            }

        </style>

        <script type="text/javascript">

            $(document).ready(function() {

                $('#reports .head select[name="report_id"]').change(function() {
                    var val = $(this).val();
                    if (val) {
                        location.href = './?id=' +val;
                    }
                });

            });

        </script>

        <div id="reports">

        <h1>
            <span class="page_icon home"></span>
            <a href="/admin/" class=" first">Dashboard</a>
            <span class="divider">›</span>
            <a href="/admin/reports/" class=" last">Reports</a>
        </h1>

            <div class="head">

                <fieldset>
                    <select name="report_id">
                        <option value="">Select a Report</option>
                        <?php foreach ($resp->reports as $rep) { ?>
                            <option value="<?php echo $rep->id; ?>" <?php if ($rep->id == $report->id) { ?>selected="selected"<?php } ?>><?php echo stripslashes($rep->title); ?></option>
                        <?php } ?>
                    </select>
                </fieldset>

            </div>

            <?php if ($report->id) { ?>

                <div class="report">
                    <?php echo print_r($report); ?>
                </div>

            <?php } ?>

        </div>

        <?php

    // NO REPORTS
    } else {

        ?>
        <div class="no-reports">
            It doesn't look like you have any reports set up. <a href="http://www.revsocial.com/dashboard/reports/" target="_blank">Click here</a> to create one!
        </div>
        <?php

    }

}


/*
$recipients = array(
    2
);

$result = $admin->createMessage('Order Placed - #123', 'View details: <a href="#">View Order</a>', $recipients);

echo print_r($result);
*/

?>