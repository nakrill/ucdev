<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// HAVE REVSOCIAL API
if ($cms->getSetting('rs_api-key')) {

    // KEY PARTS
    $kpa = explode('-', $cms->getSetting('rs_api-key'));

    // BUSINESS ID
    $business_id = $kpa[0];

    ?>

    <style type="text/css">

        #p_reports .row {
            margin: 0 -15px 15px -15px;
        }
        #p_reports [class*='col-'] {
            float: left;
            width: 100%;
        }
        #p_reports .col-13 {
            width: 33.3333%;
        }
        #p_reports .col-12 {
            width: 50%;
        }
        #p_reports .col-23 {
            width: 66.6666%;
        }
        #p_reports .chart-wrapper {
            margin: 0 15px 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        #p_reports .chart-wrapper .chart-title {
            padding: 10px;
            border-bottom: 1px solid #ccc;
            background-color: #f0f0f0;
            font-size: 1.2em;
            font-weight: bold;
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;
        }
        #p_reports .chart-wrapper .chart-options {
            padding: 15px;
            background-color: #fafafa;
        }
        #p_reports .chart-wrapper .chart-notes {
            padding: 10px;
            border-top: 1px solid #eee;
            color: #888;
        }

        #p_reports .funnel-main .chart-options .head {
            margin-bottom: 5px;
            font-weight: bold;
        }
        #p_reports .funnel-main .chart-options .step input[type="text"] {
            display: inline-block;
            width: 400px;
        }
        #p_reports .funnel-main .chart-options .step .what {
            float: left;
            margin-right: 15px;
        }
        #p_reports .funnel-main .chart-options .step .operator {
            float: left;
            margin-right: 15px;
        }
        #p_reports .funnel-main .chart-options .step .value {
            float: left;
        }
        #p_reports .funnel-main .chart-options .step a.add, #p_reports .funnel-main .chart-options .step a.delete {
            display: inline-block;
            height: 29px;
            margin-left: 15px;
            line-height: 29px;
        }
        #p_reports .funnel-main .chart-options .steps {
            margin-top: 15px;
        }
        #p_reports .funnel-main .chart-options .steps .step {
            margin-bottom: 5px;
        }
        #p_reports .funnel-main .chart-options .no-steps {
            margin-top: 20px;
        }

    </style>

    <script src="https://d26b395fwzu5fz.cloudfront.net/3.4.1/keen.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        <?php echo keen_JS_init(); ?>

        $(document).ready(function() {

            // Funnel - Main - Options - What - Change
            $('#p_reports .funnel-main .chart-options').on('change', '.step .what select', function(e) {
                var what = $(this).val();
                var parent = $(this).closest('.step');
                parent.find('.value [class*="what-"]').hide();
                parent.find('.value .what-' +what).show();
            });

            // Funnel - Main - Options - Add - Click
            $('#p_reports .funnel-main .chart-options .step .add').click(function(e) {
                e.preventDefault();
                var clone = $('#p_reports .funnel-main .chart-options .add .step').clone().appendTo('#p_reports .funnel-main .chart-options .steps .content');
                clone.find('.add').hide();
                clone.find('.delete').show();
                clone.find('.select').remove();
                clone.find('.custom_control').removeClass('custom_control');
                BigTreeCustomControls();
                $('#p_reports .funnel-main .chart-options .steps').show();
            });

            // Funnel - Main - Rule - Remove - Click
            $('#p_reports .funnel-main .chart-options .steps .content').on('click', '.step .delete', function(e) {
                e.preventDefault();
                $(this).closest('.step').remove();
            });

            // Funnel - Main - Options - Process - Click
            $('#p_reports .funnel-main .chart-options .process').click(function(e) {
                e.preventDefault();

                var sa = [];
                var la = [];

                $('#p_reports .funnel-main .chart-options .steps .content .step').each(function() {

                    var what = $(this).find('.what select').val();
                    var collection = '';
                    var property_name = '';

                    if (what == 'url') {
                        collection      = 'web_pageView';
                        property_name   = 'parsed_page_url.path';
                    } else if (what == 'web_event') {
                    }

                    if (collection) {

                        var val = $(this).find('.value .what-' +what+ ' input').val();
                        var op = $(this).find('.operator select').val();

                        sa.push(
                            {
                                event_collection: 'web_pageView',
                                actor_property: 'session_id',
                                filters: [
                                    {
                                        property_name: 'business_id',
                                        operator: 'eq',
                                        property_value: <?php echo $business_id; ?>
                                    },
                                    {
                                        property_name : property_name,
                                        operator : op,
                                        property_value : val
                                    }
                                ],
                                timeframe: 'this_14_days'
                            }
                        );
                        la.push(val);

                    }

                });

                if (sa.length > 0) {

                    // CHART - Funnel
                    var cq_fm = new Keen.Query('funnel', {
                        steps: sa
                    });
                    keen_client.draw(cq_fm, document.getElementById('chart_funnel-main'), {
                        library: 'google',
                        chartType: 'barchart', // or 'columnchart'
                        height: sa.length * 70,
                        title: null,
                        colors: ['#79CDCD'],
                        labels: la,
                        chartOptions: {
                            chartArea: { height: '85%', left: '20%', top: '5%' },
                            legend: { position: 'none' }
                        }
                    });

                } else {
                    alert('Please set your steps up first.');
                }

            });

        });

        Keen.ready(function(){

            // CHART - Pageviews - Hourly
            var cq_pvh = new Keen.Query('count', {
                event_collection: 'web_pageView',
                timeframe: 'today',
                interval: 'hourly',
                filters: [
                    {
                        property_name: 'business_id',
                        operator: 'eq',
                        property_value: <?php echo $business_id; ?>
                    }
                ]
            });
            keen_client.draw(cq_pvh, document.getElementById('chart_pageviews-hourly'), {
                chartType: 'areachart',
                title: false,
                labelMapping: {
                    'null': 'Unknown'
                },
                width: 'auto',
                height: 250,
                chartOptions: {
                    chartArea: {
                        height: '85%',
                        left: '5%',
                        top: '5%',
                        width: '95%'
                    },
                    isStacked: true
                }
            });

            // CHART - Pages - Top - Week
            var cq_ptw = new Keen.Query('count', {
                eventCollection: 'web_pageView',
                timeframe: 'this_7_days',
                groupBy: 'parsed_page_url.path',
                filters: [
                    {
                        property_name: 'business_id',
                        operator: 'eq',
                        property_value: <?php echo $business_id; ?>
                    }
                ]
            });
            var el_table = new Keen.Dataviz()
                .chartType('table')
                .el(document.getElementById('chart_pages-top-week'))
                .chartOptions({
                    title: false,
                    width: '100%',
                    height: 250
                });
            keen_client.run(cq_ptw, function(err, res) {
                el_table
                    .parseRequest(this)
                    .call(function() {
                        this.dataset.updateRow(0, function(value, index) {
                            return ['Page', 'Views'][index];
                        });
                        this.dataset.sortRows('desc', function(row) {
                            return row[1];
                        });
                    })
                    .render();
            });

            // CHART - Pageviews - Browser - Week
            var cq_pvbw = new Keen.Query('count', {
                eventCollection: 'web_pageView',
                timeframe: 'this_7_days',
                groupBy: 'parsed_user_agent.browser.family',
                interval: 'daily',
                filters: [
                    {
                        property_name: 'business_id',
                        operator: 'eq',
                        property_value: <?php echo $business_id; ?>
                    }
                ]
            });
            keen_client.draw(cq_pvbw, document.getElementById('chart_pageviews-browser-week'), {
                chartType: 'areachart',
                title: false,
                labelMapping: {
                    'null': 'Unknown'
                },
                width: 'auto',
                height: 250,
                chartOptions: {
                    chartArea: {
                        height: '85%',
                        left: '5%',
                        top: '5%',
                        width: '75%'
                    },
                    isStacked: true
                }
            });

            // CHART - Traffic - Browser - Week
            var cq_tfw = new Keen.Query('count', {
                eventCollection: 'web_pageView',
                timeframe: 'this_7_days',
                groupBy: 'parsed_user_agent.browser.family',
                filters: [
                    {
                        property_name: 'business_id',
                        operator: 'eq',
                        property_value: <?php echo $business_id; ?>
                    }
                ]
            });
            keen_client.draw(cq_tfw, document.getElementById('chart_traffic-browser-week'), {
                chartType: 'piechart',
                title: false,
                labelMapping: {
                    'null': 'Unknown'
                },
                width: 'auto',
                height: 250,
                chartOptions: {
                    chartArea: {
                        height: '85%',
                        left: '5%',
                        top: '5%',
                        width: '100%'
                    },
                    pieHole: .4
                }
            });

            // CHART - Devices (OS) - Week
            var cq_dosw = new Keen.Query('count', {
                eventCollection: 'web_pageView',
                timeframe: 'this_7_days',
                groupBy: 'parsed_user_agent.os.family',
                interval: 'daily',
                filters: [
                    {
                        property_name: 'business_id',
                        operator: 'eq',
                        property_value: <?php echo $business_id; ?>
                    }
                ]
            });
            keen_client.draw(cq_dosw, document.getElementById('chart_devices-week'), {
                chartType: 'columnchart',
                title: false,
                width: 'auto',
                height: 250,
                labelMapping: {
                    'null': 'Unknown'
                },
                chartOptions: {
                    chartArea: {
                        height: '75%',
                        left: '10%',
                        top: '5%',
                        width: '60%'
                    },
                    bar: {
                        groupWidth: '85%'
                    },
                    isStacked: true
                }
            });

            // CHART - States - Week
            var cq_sw = new Keen.Query('count', {
                eventCollection: 'web_pageView',
                timeframe: 'this_7_days',
                groupBy: 'ip_geo_info.province',
                interval: 'daily',
                filters: [
                    {
                        property_name: 'business_id',
                        operator: 'eq',
                        property_value: <?php echo $business_id; ?>
                    }
                ]
            });
            keen_client.draw(cq_sw, document.getElementById('chart_states-week'), {
                chartType: 'columnchart',
                title: false,
                width: 'auto',
                height: 250,
                labelMapping: {
                    'null': 'Unknown'
                },
                chartOptions: {
                    chartArea: {
                        height: '75%',
                        left: '10%',
                        top: '5%',
                        width: '60%'
                    },
                    bar: {
                        groupWidth: '85%'
                    },
                    isStacked: true
                }
            });

        });

    </script>

    <h1>
        <span class="page_icon analytics"></span>
        <a class="first" href="/admin/reports/">Reports</a>
    </h1>

    <? /*
    <nav id="sub_nav">
        <a class="active" href="/admin/accounts/"><span class="icon_small icon_small_list"></span>View Accounts</a>
        <a href="/admin/accounts/edit/"><span class="icon_small icon_small_add"></span>Add Account</a>
        <menu style="display: none;">
            <span class="icon"></span>
            <div></div>
        </menu>
    </nav>
    */ ?>

    <div id="p_reports">

        <div class="row clearfix">

            <div class="col-1 funnel-main">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        Traffic Funnel
                    </div>
                    <div class="chart-options">
                        <div class="add">
                            <div class="head">Add Step</div>
                            <div class="step clearfix">
                                <div class="what clearfix">
                                    <select>
                                        <option value="url">URL</option>
                                        <option value="web_event">Web Event</option>
                                    </select>
                                </div>
                                <div class="operator clearfix">
                                    <select>
                                        <option value="eq">Equal to</option>
                                        <option value="ne">Not equal to</option>
                                        <option value="gt">Greater than</option>
                                        <option value="eq">Less than</option>
                                        <option value="contains">Contains</option>
                                    </select>
                                </div>
                                <div class="value clearfix">
                                    <div class="what-url">
                                        <?php echo $bigtree["config"]["domain"]; ?><input type="text" name="url" value="/" />
                                    </div>
                                    <div class="what-web_event" style="display: none; line-height: 29px;">
                                        Coming soon.
                                    </div>
                                </div>
                                <a href="#" class="add button">Add</a><a href="#" class="delete" style="display: none;">Remove</a>
                            </div>
                        </div>
                        <div class="steps" style="display: none;">
                            <div class="head">Steps</div>
                            <div class="content"></div>
                            <a href="#" class="button process">Calculate</a>
                        </div>
                        <div class="no-steps">
                            Add steps to create your funnel.
                        </div>
                    </div>
                    <div class="chart-stage">
                        <div id="chart_funnel-main"></div>
                    </div>
                    <div class="chart-notes">
                        This is a sample text region to describe this chart.
                    </div>
                </div>
            </div>

        </div>

        <div class="row clearfix">

            <div class="col-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        Pageviews (today)
                    </div>
                    <div class="chart-stage">
                        <div id="chart_pageviews-hourly"></div>
                    </div>
                    <div class="chart-notes">
                        This is a sample text region to describe this chart.
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        Top Pages (past week)
                    </div>
                    <div class="chart-stage">
                        <div id="chart_pages-top-week"></div>
                    </div>
                    <div class="chart-notes">
                        This is a sample text region to describe this chart.
                    </div>
                </div>
            </div>

        </div>

        <div class="row clearfix">

            <div class="col-23">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        Pageviews by Browser (past week)
                    </div>
                    <div class="chart-stage">
                        <div id="chart_pageviews-browser-week"></div>
                    </div>
                    <div class="chart-notes">
                        This is a sample text region to describe this chart.
                    </div>
                </div>
            </div>

            <div class="col-13">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        Traffic by Browser (past week)
                    </div>
                    <div class="chart-stage">
                        <div id="chart_traffic-browser-week"></div>
                    </div>
                    <div class="chart-notes">
                        Notes go down here
                    </div>
                </div>
            </div>

        </div>

        <div class="row clearfix">

            <div class="col-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        Devices (past week)
                    </div>
                    <div class="chart-stage">
                        <div id="chart_devices-week"></div>
                    </div>
                    <div class="chart-notes">
                        Notes go down here
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        States (past week)
                    </div>
                    <div class="chart-stage">
                        <div id="chart_states-week"></div>
                    </div>
                    <div class="chart-notes">
                        Notes go down here
                    </div>
                </div>
            </div>

        </div>

    </div>

    <?php

// NO REVSOCIAL API KEY
} else {
    ?>
    <h1>
        <span class="page_icon analytics"></span>
        <a class="first" href="/admin/reports/">Reports</a>
    </h1>
    <div id="p_reports">
        <div class="container" style="padding: 15px; text-align: center;">
            RevSocial API not configured.
        </div>
    </div>
    <?php
}

?>