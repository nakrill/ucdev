<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// GET PAGES WITHOUT META DESCRIPTION
$page_query = "SELECT * FROM `bigtree_pages` WHERE (`meta_description`='') AND (`archived`='') ORDER BY `title` ASC";
$page_q = sqlquery($page_query);

?>

<style type="text/css">

    #p_seo .table .pages_title {
        width: 810px;
    }
    #p_seo .table .pages_status {
        width: 120px;
    }
    #p_seo .table .pages_view {
        width: 70px;
    }
    #p_seo .table .pages_edit {
        width: 50px;
    }

</style>

<h1>
    <span class="page_icon analytics"></span>
    <a href="../" class="first">SEO</a>
    <span class="divider">›</span>
    <a href="./" class="last">Pages without Meta Description</a>
</h1>

<div id="p_seo">

    <?php

    // HAVE PAGES
    if (sqlrows($page_q) > 0) {

        ?>

        <div class="table">
            <summary>
                <h2>Pages</h2>
            </summary>
            <header>
                <span class="pages_title">Title</span>
                <span class="pages_view">View</span>
                <span class="pages_edit">Edit</span>
            </header>
            <ul id="pages">
                <?php while ($page = sqlfetch($page_q)) { ?>
                    <li class="published">
                        <section class="pages_title">
                            <?php echo stripslashes($page['title']); ?>
                        </section>
                        <section class="pages_view">
                            <a href="/<?php echo stripslashes($page['path']); ?>/" class="icon_restore"></a>
                        </section>
                        <section class="pages_edit">
                            <a href="http://cms.ucdev.co/admin/pages/edit/<?php echo $page['id']; ?>/" title="Edit Page" class="icon_edit page"></a>
                        </section>
                    </li>
                <?php } ?>
            </ul>

        </div>

        <?php

    // NO PAGES
    } else {
        ?>
        <div class="container" style="padding: 15px;">
            <div style="text-align: center;">All pages have meta descriptions!</div>
        </div>
        <?php
    }

    ?>

</div>