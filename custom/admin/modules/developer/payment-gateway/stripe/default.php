<div class="container">
	<form method="post" action="<?=ADMIN_ROOT?>developer/payment-gateway/stripe/update/" class="module">
		<section>
			<div class="alert">
				<p>To enable usage of Stripe as your payment gateway, enter your access information below.</p>
			</div>
			<fieldset>
				<label>API Key</label>
				<input type="text" name="stripe-api-key" value="<?=htmlspecialchars($gateway->Settings["stripe-api-key"])?>" />
			</fieldset>
            <fieldset>
                <input type="checkbox" name="stripe-payment-profiles" value="1" <?php if ($gateway->Settings["stripe-payment-profiles"]) { echo 'checked="checked"'; } ?> />
                <label class="for_checkbox">Store Payment Profiles</label>
            </fieldset>
		</section>
		<footer>
			<input type="submit" class="button blue" value="Update" />
		</footer>
	</form>
</div>