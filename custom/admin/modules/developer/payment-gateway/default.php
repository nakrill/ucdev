<? $root = DEVELOPER_ROOT."payment-gateway/" ?>
<div class="table">
    <summary><h2>Configure</h2></summary>
    <section>
        <a class="box_select<? if ($gateway->Service == "authorize.net") { ?> connected<? } ?>" href="<?=$root?>authorize/">
            <span class="authorize"></span>
            <p>Authorize.Net</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "paypal-rest") { ?> connected<? } ?>" href="<?=$root?>paypal-rest/">
            <span class="paypal"></span>
            <p>PayPal REST API</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "paypal") { ?> connected<? } ?>" href="<?=$root?>paypal/">
            <span class="paypal"></span>
            <p>PayPal Payments Pro</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "payflow") { ?> connected<? } ?>" href="<?=$root?>payflow/">
            <span class="payflow"></span>
            <p>PayPal Payflow Gateway</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "linkpoint") { ?> connected<? } ?>" href="<?=$root?>linkpoint/">
            <span class="linkpoint"></span>
            <p>First Data / LinkPoint</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "stripe") { ?> connected<? } ?>" href="<?=$root?>stripe/">
            <span class="stripe"></span>
            <p>Stripe</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "instapay") { ?> connected<? } ?>" href="<?=$root?>instapay/">
            <span class="instapay"></span>
            <p>Instapay</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "basys") { ?> connected<? } ?>" href="<?=$root?>basys/">
            <span class="basys"></span>
            <p>Basys</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "waived") { ?> connected<? } ?>" href="<?=$root?>waived/">
            <span class="waived"></span>
            <p>Waived</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "donation") { ?> connected<? } ?>" href="<?=$root?>donation/">
            <span class="donation"></span>
            <p>Donation</p>
        </a>
        <a class="box_select<? if ($gateway->Service == "trade") { ?> connected<? } ?>" href="<?=$root?>trade/">
            <span class="trade"></span>
            <p>Trade</p>
        </a>
    </section>
</div>