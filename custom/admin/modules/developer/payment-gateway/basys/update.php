<?
    $gateway->Service = "basys";
    $gateway->Settings["basys-account-id"] = $_POST["basys-account-id"];
    $gateway->Settings["basys-password"] = $_POST["basys-password"];
    $gateway->Settings["basys-payment-profiles"] = $_POST["basys-payment-profiles"];
    $gateway->saveSettings();

    $admin->growl("Developer","Updated Payment Gateway");
    BigTree::redirect(DEVELOPER_ROOT);
?>