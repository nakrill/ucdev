<div class="container">
    <form method="post" action="<?=ADMIN_ROOT?>developer/payment-gateway/basys/update/" class="module">
        <section>
            <div class="alert">
                <p>To enable usage of Basys as your payment gateway, enter your access information below.</p>
            </div>
            <fieldset>
                <label>Account ID</label>
                <input type="text" name="basys-account-id" value="<?=htmlspecialchars($gateway->Settings["basys-account-id"])?>" />
            </fieldset>
            <fieldset>
                <label>Password</label>
                <input type="text" name="basys-password" value="<?=htmlspecialchars($gateway->Settings["basys-password"])?>" />
            </fieldset>
            <fieldset>
                <input type="checkbox" name="basys-payment-profiles" value="1" <?php if ($gateway->Settings["basys-payment-profiles"]) { echo 'checked="checked"'; } ?> />
                <label class="for_checkbox">Store Payment Profiles</label>
            </fieldset>
        </section>
        <footer>
            <input type="submit" class="button blue" value="Update" />
        </footer>
    </form>
</div>