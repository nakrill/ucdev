<div class="container">
    <form method="post" action="<?=ADMIN_ROOT?>developer/payment-gateway/instapay/update/" class="module">
        <section>
            <div class="alert">
                <p>To enable usage of InstaPay as your payment gateway, enter your access information below.</p>
            </div>
            <fieldset>
                <label>Account ID</label>
                <input type="text" name="instapay-account-id" value="<?=htmlspecialchars($gateway->Settings["instapay-account-id"])?>" />
            </fieldset>
            <fieldset>
                <label>Merchant PIN</label>
                <input type="text" name="instapay-merchant-pin" value="<?=htmlspecialchars($gateway->Settings["instapay-merchant-pin"])?>" />
            </fieldset>
            <fieldset>
                <label>Processing Environment</label>
                <select name="instapay-environment">
                    <option value="live">Live</option>
                    <option value="test"<? if ($gateway->Settings["instapay-environment"] == "test") { ?> selected="selected"<? } ?>>Test</option>
                </select>
            </fieldset>
        </section>
        <footer>
            <input type="submit" class="button blue" value="Update" />
        </footer>
    </form>
</div>