<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

// NUMBER OF RESULTS PER PAGE
$per_page = 20;

$query  = isset($_GET['query']) ? $_GET['query'] : '';
$page   = isset($_GET['page']) ? intval($_GET['page']) : 1;

// PREVENT SQL SHENANIGANS
$sort_by = 'a.created';
if (isset($_GET['sort'])) {
	$valid_columns = array(
        'status'    => 'a.status',
        'username'  => 'a.username',
        'email'     => 'a.email',
        'created'   => 'a.created',
        'deleted'   => 'a.deleted',
        'name'      => 'fullname'
    );
    if ($valid_columns[$_GET['sort']]) {
        $sort_by = $valid_columns[$_GET['sort']];
    }
}
if ($sort_by == 'a.created') {
    $sort_dir = (isset($_GET['sort_direction']) && $_GET['sort_direction'] == 'ASC') ? 'ASC' : 'DESC';
} else {
    $sort_dir = (isset($_GET['sort_direction']) && $_GET['sort_direction'] == 'DESC') ? 'DESC' : 'ASC';
}

// WHERE ARRAY
if ($_GET['deleted']) {
    $wa[] = "a.status=9";
} else {
    $wa[] = "a.status!=9";
}

// IS SEARCHING
if ($query) {
    $qparts = explode(" ", $query);
    $twa = array();
    foreach ($qparts as $part) {
        $part = sqlescape(strtolower($part));
        $twa[] = "(LOWER(a.username) LIKE '%$part%') OR (LOWER(a.email) LIKE '%$part%') OR (LOWER(ad.firstname) LIKE '%$part%') OR (LOWER(ad.lastname) LIKE '%$part%')";
    }
    $wa[] = "(" .implode(" OR ", $twa). ")";
}

// GET PAGED ACCOUNTS
$accounts_query = "
SELECT CONCAT_WS(' ', ad.firstname, ad.lastname) AS `fullname`, a.*, ad.*
FROM `uccms_accounts` AS `a`
INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
WHERE (" .implode(") AND (", $wa). ")
ORDER BY " .$sort_by. " " .$sort_dir. "
LIMIT " .(($page - 1) * $per_page). "," .$per_page;

//echo $accounts_query. '<br />';
//exit;

$accounts_q = sqlquery($accounts_query);

// NUMBER OF PAGED ACCOUNTS
$num_accounts = sqlrows($accounts_q);

// TOTAL NUMBER OF ACCOUNTS
$total_results = sqlfetch(sqlquery("
SELECT COUNT('x') AS `total`
FROM `uccms_accounts` AS `a`
INNER JOIN `uccms_accounts_details` AS `ad` ON a.id=ad.id
WHERE (" .implode(") AND (", $wa). ")
"));

// NUMBER OF PAGES
$pages = ceil($total_results['total'] / $per_page);

// HAVE ACCOUNTS
if ($num_accounts > 0) {

    // LOOP
    while ($account = sqlfetch($accounts_q)) {
        ?>

        <li id="row_<?=$account['id']?>">
	        <section class="view_column users_name"><?php echo trim(stripslashes($account['firstname']. ' ' .$account['lastname'])); ?></section>
	        <section class="view_column users_email"><?php echo stripslashes($account['email']); ?></section>
	        <section class="view_column users_created"><?php echo date('n/j/Y g:i a', strtotime($account['created'])); ?></section>
	        <section class="view_action">
		        <a href="<?=ADMIN_ROOT?>accounts/edit/?id=<?=$account['id']?>" class="icon_edit"></a>
	        </section>
	        <section class="view_action">
		        <a href="#<?=$account["id"]?>" class="icon_delete"></a>
	        </section>
        </li>

        <?
    }

// NO ACCOUNTs
} else {
    ?>
    <li style="text-align: center;">
        No frontend accounts.
    </li>
    <?php
}

?>

<script>
	BigTree.setPageCount("#view_paging",<?=$pages?>,<?=$page?>);
</script>