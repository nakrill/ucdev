<?

header('Content-type: text/javascript');

$id = intval($_POST['id']);

$admin->requireLevel(1);

if ($_uccms['_account']->delete($id)) {
    ?>
    $('#row_<?=$id?>').remove();
    BigTree.growl('Frontend Accounts', 'Deleted Account');
    <?php
} else {
    ?>
    BigTree.growl('Frontend Accounts', 'Failed to delete account.');
    <?php
}
?>