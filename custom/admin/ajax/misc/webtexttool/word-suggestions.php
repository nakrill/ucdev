<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

$out = array();

// WEBTEXTTOOL CLASS
require_once(SERVER_ROOT. '/uccms/includes/classes/webtexttool.php');

// INIT WEBTEXTTOOL
$_wtt = new WebTextTool();


// VARIABLES TO USE
$vars = $_REQUEST;

// ID FOR THIS RESULTS SECTION
$results_id = uniqid();

// GET KEYWORD SUGGESTIONS
$out['keywords'] = $_wtt->keywords($vars['keywords']);

$out['sent'] = array(
    'keywords' => $vars['keywords']
);

// JSON OUTPUT
if ($vars['format'] == 'json') {

    echo json_encode($out);

// HTML OUTPUT
} else {

    $num_keywords = count($out['keywords']);

    $colors = array(
        0 => '#F44336',
        1 => '#FF7043',
        2 => '#FDD835',
        3 => '#FFEB3B',
        4 => '#AED581',
        5 => '#388E3C'
    );

    ?>

    <div id="result-word-<?php echo $results_id; ?>" class="results-word">

        <style type="text/css">

            .webtexttool .results-word .reportcard {

            }

            .webtexttool .results-word .keywords {
                max-height: 200px;
                overflow-y: scroll;
            }
            .webtexttool .results-word .keywords .keyword {
                display: inline-block;
                margin: 0 5px 8px 0;
                padding: 8px 10px;
                border-radius: 3px;
                background-color: #ccc;
                color: #fff;
                cursor: default;
            }
            .webtexttool .results-word .keywords .keyword .tooltip_content {
                display: none;
            }

            .webtexttool .results-word .keywords .none {
                padding: 10px;
                text-align: center;
            }

        </style>

        <?php if ($out->error) { ?>

            <div class="error">
                <?php echo $out->error; ?>
            </div>

        <?php } else { ?>

            <script type="text/javascript">

                $(document).ready(function() {

                });

            </script>

            <div class="keywords clearfix">

                <?php

                // HAVE KEYWORDS
                if ($num_keywords > 0) {

                    ?>

                    <style type="text/css">

                        .tooltipster-light {
                            background-color: #fff;
                        }
                        /*
                        .tooltipster-light .tooltipster-content {
                            color: blue;
                            padding: 8px;
                        }
                        */

                        .wtt_ws-tt {
                            width: 170px;
                        }
                        .wtt_ws-tt .stat {
                            padding-bottom: 5px;
                            margin-bottom: 5px;
                            border-bottom: 1px dashed #aaa;
                            font-size: .9em;
                        }
                        .wtt_ws-tt .stat:last-child {
                            margin-bottom: 0px;
                            border-bottom: 0px none;
                        }
                        .wtt_ws-tt .stat .legend {
                            float: left;
                            width: 14px;
                            height: 14px;
                            margin-right: 8px;
                        }
                        .wtt_ws-tt .stat .label {
                            float: left;
                            margin-right: 5px;
                            line-height: 14px;
                            font-weight: bold;
                        }
                        .wtt_ws-tt .stat .value {
                            float: left;
                            line-height: 14px;
                        }
                        .wtt_ws-tt .stat.suggest {
                            padding: 5px 0;
                        }
                        .wtt_ws-tt .stat.suggest .value {
                            float: none;
                            width: 100%;
                            text-align: center;
                        }
                        .wtt_ws-tt .stat .button {
                            height: auto;
                            padding: 5px 10px;
                            line-height: 1em;
                        }

                    </style>

                    <?php

                    // LOOP
                    foreach ($out['keywords'] as $keyword) {

                        // CALCULATE SCORE PERCENT
                        $percent = round($keyword->OverallScore * 2 * 10);

                        /*
                        <div class="keyword" style="background-color: <?php echo $colors[$keyword->OverallScore]; ?>;">
                        */

                        ?>

                        <div class="keyword" style="background-color: <?php echo percentToHsl($percent); ?>;">
                            <?php echo $keyword->Keyword; ?>
                            <div class="tooltip_content wtt_ws-tt">
                                <div class="stat overall clearfix">
                                    <div class="legend" style="background-color: <?php echo $colors[$keyword->OverallScore]; ?>;"></div>
                                    <div class="label">Overall:</div>
                                    <div class="value"><?php echo $keyword->OverallLabel; ?></div>
                                </div>
                                <div class="stat volume clearfix">
                                    <div class="legend" style="background-color: <?php echo $colors[$keyword->VolumeScore]; ?>;"></div>
                                    <div class="label">Volume:</div>
                                    <div class="value"><?php echo $keyword->VolumeLabel; ?></div>
                                </div>
                                <div class="stat competition clearfix">
                                    <div class="legend" style="background-color: <?php echo $colors[$keyword->CompetitionScore]; ?>;"></div>
                                    <div class="label">Competition:</div>
                                    <div class="value"><?php echo $keyword->CompetitionLabel; ?></div>
                                </div>
                                <div class="stat suggest clearfix">
                                    <? /*
                                    <div class="legend"></div>
                                    <div class="label"></div>
                                    */ ?>
                                    <div class="value"><a href="#" class="button blue" data-keyword="<?php echo $keyword->Keyword; ?>">Get suggestions</a></div>
                                </div>
                            </div>
                        </div>
                        <?php

                    }

                    ?>

                    <script type="text/javascript">

                        // TOOLTIP
                        $('#result-word-<?php echo $results_id; ?> .keywords .keyword').each(function(i) {

                            var $el = $(this);
                            var $content = $el.find('.tooltip_content');

                            $content.find('.suggest a').click(function(e) {
                                e.preventDefault();
                                $('#<?php echo $vars['ui_id']; ?> .main-input .keywords input[name="keywords"]').val($(this).attr('data-keyword'));
                                $('#<?php echo $vars['ui_id']; ?> .main-input .buttons .suggest-word').click();
                            });

                            try {

                                $el.tooltipster({
                                    content: $content,
                                    interactive: true,
                                    theme: 'tooltipster-light',
                                });

                            } catch (e) {
                                //console.log(e);
                                console.log('Tooltipster Error', $el, e);
                            }

                        });

                    </script>

                    <?php

                // NO KEYWORDS
                } else {
                    ?>
                    <div class="none">
                        No results.
                    </div>
                    <?php
                }

                ?>

            </div>

            <?php

        }

        /*
        if ($_SERVER['REMOTE_ADDR'] == '68.7.118.150') {

            ?>

            <b>Sent</b>

            <pre>
                <?php echo print_r($out->sent); ?>
            </pre>

            <br />
            <br />

            <b>Received</b>

            <pre>
                <?php echo print_r($out); ?>
            </pre>

            <?php

        }
        */

        ?>

    </div>

    <?php

}

?>