<?php

//echo print_r($field);

// WEBTEXTTOOL CLASS
//require_once(SERVER_ROOT. '/uccms/includes/classes/webtexttool.php');

// INIT WEBTEXTTOOL
//$_wtt = new WebTextTool();

// WEBTEXTTOOL ENABLED
if ($cms->getSetting('webtexttool_username')) {

    ?>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-light.min.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js"></script>

    <style type="text/css">

        .webtexttool {
            margin-top: 10px;
        }

        .webtexttool .text-input-padding {
            padding-right: 20px;
        }

        .webtexttool .main-input input[name="keywords"] {
            display: block;
            width: 100%;
        }
        .webtexttool .main-input .buttons {
            margin: 0 0 15px;
            text-align: center;
        }
        .webtexttool .main-input .buttons .button_contain {
            float: left;
            width: 50%;
        }
        .webtexttool .main-input .buttons .button {
            display: block;
            margin: 8px 0 0;
            height: 30px;
            line-height: 30px;
        }
        .webtexttool .main-input .buttons .button.suggest-word {
            margin-right: 5px;
        }
        .webtexttool .main-input .buttons .button.suggest-page {
            margin-left: 5px;
        }

        .webtexttool .analyzing {
            display: none;
            text-align: center;
            line-height: 16px;
        }
        .webtexttool .analyzing img {
            width: 16px;
            height: 16px;
            margin-right: 2px;
        }

        .webtexttool .results {
            display: none;
            margin-top: 10px;
        }

        .webtexttool .results pre {
            max-width: 300px;
            overflow: auto;
        }

    </style>

    <?php

    //echo print_r($bigtree);

    ?>

    <script type="text/javascript">

        $(document).ready(function() {

            if (typeof _WTT != 'object') _WTT = {};
            if (typeof _WTT.uis != 'object') _WTT.uis = {};

            _WTT.uis['<?php echo $wtt_ui['id']; ?>'] = {};

            this_wtt = _WTT.uis['<?php echo $wtt_ui['id']; ?>'];

            if (typeof this_wtt.functions != 'object') this_wtt.functions = {};

            // DO - Word Suggestions
            this_wtt.functions.do_wordSuggestions = function(params) {

                $.post('/admin/ajax/misc/webtexttool/word-suggestions/', params, function(data) {

                    $('#wtt-<?php echo $wtt_ui['id']; ?> .analyzing').hide();
                    $('#wtt-<?php echo $wtt_ui['id']; ?> .results').html(data).show();

                    <?php

                    // HAVE CALLBACK
                    if ($wtt_ui['callbacks']['word']['after']) {
                        echo $wtt_ui['callbacks']['word']['after']. '(data);';
                    }

                    ?>

                }, 'html');

            };

            // DO - Page Suggestions
            this_wtt.functions.do_pageSuggestions = function(params) {

                $.post('/admin/ajax/misc/webtexttool/page-suggestions/', params, function(data) {

                    $('#wtt-<?php echo $wtt_ui['id']; ?> .analyzing').hide();
                    $('#wtt-<?php echo $wtt_ui['id']; ?> .results').html(data).show();

                    <?php

                    // HAVE CALLBACK
                    if ($wtt_ui['callbacks']['page']['after']) {
                        echo $wtt_ui['callbacks']['page']['after']. '(data);';
                    }

                    ?>

                }, 'html');

            };

            // Suggest words click
            $('#wtt-<?php echo $wtt_ui['id']; ?> .main-input .buttons .suggest-word').click(function(e) {
                e.preventDefault();

                var keywords = $('#wtt-<?php echo $wtt_ui['id']; ?> .main-input .keywords input[name="keywords"]').val();

                if (keywords) {

                    $('#wtt-<?php echo $wtt_ui['id']; ?> .results').hide().html('');
                    $('#wtt-<?php echo $wtt_ui['id']; ?> .analyzing').show();

                    var params = {
                        ui_id: 'wtt-<?php echo $wtt_ui['id']; ?>',
                        keywords: keywords
                    };

                    <?php

                    // HAVE CALLBACK
                    if ($wtt_ui['callbacks']['word']['before']) {
                        echo $wtt_ui['callbacks']['word']['before']. '(params, this_wtt.functions.do_wordSuggestions);';
                    } else {
                        echo 'this_wtt.functions.do_wordSuggestions(params);';
                    }

                    ?>

                } else {

                    alert('Please specify a keyword.');

                }

            });

            // Analyze Page click
            $('#wtt-<?php echo $wtt_ui['id']; ?> .main-input .buttons .suggest-page').click(function(e) {
                e.preventDefault();

                var keywords = $('#wtt-<?php echo $wtt_ui['id']; ?> .main-input .keywords input[name="keywords"]').val();

                if (keywords) {

                    $('#wtt-<?php echo $wtt_ui['id']; ?> .results').hide().html('');
                    $('#wtt-<?php echo $wtt_ui['id']; ?> .analyzing').show();

                    var params = {
                        ui_id: 'wtt-<?php echo $wtt_ui['id']; ?>',
                        keywords: keywords
                    };

                    <?php

                    if ($wtt_ui['page']['id']) {
                        ?>
                        params.page = {
                            id: <?php echo (is_numeric($wtt_ui['page']['id']) ? $wtt_ui['page']['id'] : "'" .$wtt_ui['page']['id']. "'"); ?>,
                        };
                        <?php
                    } else {
                        ?>
                        params.page = {
                            title: '<?php echo $wtt_ui['page']['title']; ?>',
                            description: '<?php echo $wtt_ui['page']['description']; ?>'
                        };
                        <?php
                    }

                    // HAVE CALLBACK
                    if ($wtt_ui['callbacks']['page']['before']) {
                        echo $wtt_ui['callbacks']['page']['before']. '(params, this_wtt.functions.do_pageSuggestions);';
                    } else {
                        echo 'this_wtt.functions.do_pageSuggestions(params);';
                    }

                    ?>

                } else {

                    alert('Please specify a keyword.');

                }

            });

        });

    </script>

    <div id="wtt-<?php echo $wtt_ui['id']; ?>" class="webtexttool">

        <div class="main-input">

            <div class="keywords text-input-padding">
                <input type="text" name="keywords" value="<?php echo $wtt_ui['keywords']; ?>" placeholder="Keyword" />
            </div>

            <div class="buttons clearfix">
                <div class="button_contain">
                    <a class="suggest-word button blue">Get Suggestions</a>
                </div>
                <div class="button_contain">
                    <a class="suggest-page button green">Analyze Content</a>
                </div>
            </div>

        </div>

        <div class="analyzing">
            <img src="/images/misc/loading-circle-1.gif" alt="" /> Analyzing..
        </div>

        <div class="results">

        </div>

    </div>

    <?php

// WEBTEXTTOOL NOT ENABLED, UPSELL
} else {

    //echo 'upsell';

}

?>