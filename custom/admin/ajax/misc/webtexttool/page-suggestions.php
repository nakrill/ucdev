<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

$out = array();

// WEBTEXTTOOL CLASS
require_once(SERVER_ROOT. '/uccms/includes/classes/webtexttool.php');

// INIT WEBTEXTTOOL
$_wtt = new WebTextTool();


// VARIABLES TO USE
$vars = $_REQUEST;

if ($vars['page']['id'] == 'home') {
    $vars['page']['id'] = 0;
} else if ($vars['page']['id']) {
    $vars['page']['id'] = (int)$vars['page']['id'];
} else {
    unset($vars['page']['id']);
}

// ID FOR THIS RESULTS SECTION
$results_id = uniqid();

// FULL CONTENT NOT SPECIFIED
if (!$vars['content']) {

    $page = array();

    // PAGE SPECIFIED
    if (isset($vars['page']['id'])) {

        // GET PAGE
        $page = $cms->getPage($vars['page']['id']);

    }

    // NO META TITLE
    if (!$vars['page']['title']) {
        $vars['page']['title'] = $page['title'];
    }

    // NO META TITLE
    if (!$vars['page']['description']) {
        $vars['page']['description'] = $page['meta_description'];
    }

    // BUILD CONTENT
    $vars['content'] = '<html><head><title>' .$vars['page']['title']. '</title><meta name="description" content="' .$vars['page']['description']. '"></head><body>' .$vars['page']['body']. '</body></html>';

}

// GET PAGE SUGGESTIONS
$out = $_wtt->pageSuggestions($vars['content'], $vars['keywords'], $vars['synonyms'], $vars['domain'], $vars['rule_set']);

$out->sent = array(
    'content' => htmlspecialchars($vars['content']),
    'keywords' => $vars['keywords']
);

// JSON OUTPUT
if ($vars['format'] == 'json') {

    echo json_encode($out);

// HTML OUTPUT
} else {

    ?>

    <div id="result-page-<?php echo $results_id; ?>" class="results-page">

        <style type="text/css">

            .webtexttool .results-page .reportcard {

            }
            .webtexttool .results-page .reportcard .score-circle {
                float: left;
                width: 60px;
                height: 60px;
                background-color: #ccc;
                border-radius: 60px;
                text-align: center;
                font-size: 1.5em;
                font-weight: bold;
                line-height: 60px;
                color: #fff;
            }
            .webtexttool .results-page .reportcard .score-circle .symb {
                padding-left: 1px;
            }
            .webtexttool .results-page .reportcard .score-tag {
                float: left;
                margin: 0 0 5px 15px;
                line-height: 60px;
                font-size: 1.5em;
                font-weight: bold;
            }

            .webtexttool .results-page .word-count {
                margin: 5px 0 0 0;
                text-align: center;
            }

            .webtexttool .results-page .suggestions {

            }
            .webtexttool .results-page .suggestions .group {
                margin-top: 10px;
                border: 1px solid #ddd;
                background-color: #fff;
            }
            .webtexttool .results-page .suggestions .group h4 {
                margin: 0px;
                padding: 5px 10px;
                background-color: #fff;
                cursor: pointer;
            }
            .webtexttool .results-page .suggestions .group h4.active, .webtexttool .results-page .suggestions .group h4:hover {
                background-color: #f5f5f5;
            }
            .webtexttool .results-page .suggestions .group h4 i {
                margin-right: 5px;
                color: #aaa;
            }

            .webtexttool .results-page .suggestions .group h4 i.fa-chevron-up {
                display: none;
            }
            .webtexttool .results-page .suggestions .group h4.active i.fa-chevron-down {
                display: none;
            }
            .webtexttool .results-page .suggestions .group h4.active i.fa-chevron-up {
                display: inline-block;
            }

            .webtexttool .results-page .suggestions .group h4 .complete {
                float: right;
                color: #000;
            }
            .webtexttool .results-page .suggestions .group .rules {
                display: none;
                padding: 0 10px 10px;
                border-top: 1px solid #eee;
            }
            .webtexttool .results-page .suggestions .group .rules .rule {
                margin-top: 10px;
            }
            .webtexttool .results-page .suggestions .group .rules .rule .complete {
                float: left;
                font-size: 1.2em;
            }
            .webtexttool .results-page .suggestions .group .rules .rule .complete i.fa-check-circle {
                /*color: #4DA46E;*/
                color: <?php echo percentToHsl(100); ?>;
            }
            .webtexttool .results-page .suggestions .group .rules .rule .title {
                float: left;
                max-width: 90%;
                margin-left: 8px;
                line-height: 1.3em;
            }
            .webtexttool .results-page .suggestions .group .rules .rule .title strong {
                color: #444;
            }

            .webtexttool .results-page .suggestions .group .rules .none {
                padding: 10px;
                text-align: center;
            }

        </style>

        <?php if ($out->error) { ?>

            <div class="error">
                <?php echo $out->error; ?>
            </div>

        <?php } else { ?>

            <script type="text/javascript">

                $(document).ready(function() {

                    /*
                    // REPORT SCORE CIRCLE COLOR
                    var $el = $('.webtexttool .results-page .reportcard .score-circle');
                    $el.css({
                        backgroundColor: percentageToHsl(parseInt($el.attr('data-percent'))),
                    });
                    */

                    // GROUP HEADING CLICK
                    $('#result-page-<?php echo $results_id; ?> .suggestions .group h4').click(function(e) {
                        e.preventDefault;
                        var $this = $(this);
                        var $rules = $this.next('.rules');
                        $rules.slideToggle(300, function() {
                            if ($rules.is(':visible')) {
                                $this.addClass('active');
                            } else {
                                $this.removeClass('active');
                            }
                        });
                    });

                });

            </script>

            <div class="reportcard clearfix">
                <div class="score-circle" data-percent="<?php echo round($out->PageScore); ?>" style="background-color: <?php echo percentToHsl(round($out->PageScore)); ?>;">
                    <span class="score"><span class="num"><?php echo round($out->PageScore); ?></span><span class="symb">%</span></span>
                </div>
                <?php if ($out->PageScoreTag) { ?>
                    <h2 class="score-tag"><?php echo $out->PageScoreTag; ?></h2>
                <?php } ?>
            </div>

            <? /*
            <div class="page-score-potential">
                Score Potential: <?php echo $out->PageScorePotential; ?>
            </div>

            <div class="keyword-count">
                Keyword Count: <?php echo $out->KeywordCount; ?>
            </div>
            */ ?>

            <div class="word-count">
                Word Count: <?php echo $out->WordCount; ?>
            </div>

            <?php

            // HAVE SUGGESTIONS
            if (count($out->Suggestions) > 0) {

                ?>

                <div class="suggestions">

                    <?php

                    // GROUPS
                    foreach ($out->Suggestions as $group) {

                        // NUMBER OF RULES
                        $num_rules = count($group->Rules);

                        $rules = array();

                        // RULES COMPLETE COUNT
                        $rcc = 0;

                        // GROUP % COMPLETE
                        $group_complete = 0;

                        // HAVE RULES
                        if ($num_rules > 0) {

                            // LOOP
                            foreach ($group->Rules as $rule) {

                                $rules[] = $rule;

                                // COMPLETE
                                if ($rule->Checked) {
                                    $rcc++;
                                }

                            }

                            // % COMPLETE
                            $group_complete = round(($rcc / $num_rules) * 100);

                        }

                        ?>

                        <div class="group">

                            <h4><i class="fa fa-chevron-down" aria-hidden="true"></i><i class="fa fa-chevron-up" aria-hidden="true"></i><?php echo $group->Tag; ?><span class="complete" style="color: <?php echo percentToHsl($group_complete); ?>;"><?php echo $group_complete; ?>%</span></h4>

                            <div class="rules">

                                <?php

                                // HAVE RULES
                                if (count($rules) > 0) {

                                    // LOOP
                                    foreach ($rules as $rule) {

                                        ?>

                                        <div class="rule clearfix">
                                            <div class="complete">
                                                <?php if ($rule->Checked) { ?>
                                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                <?php } else { ?>
                                                    <i class="fa fa-circle-o" aria-hidden="true"></i>
                                                <?php } ?>
                                            </div>
                                            <div class="title">
                                                <?php echo $rule->Text; ?>
                                            </div>
                                        </div>

                                        <?php

                                    }

                                // NO RULES
                                } else {
                                    ?>
                                    <div class="none">
                                        No suggestions.
                                    </div>
                                    <?php
                                }

                                ?>

                            </div>

                        </div>

                        <?php

                    }

                    ?>

                </div>

            <?php

            }

        }

        /*
        if ($_SERVER['REMOTE_ADDR'] == '68.7.118.150') {

            ?>

            <b>Sent</b>

            <pre>
                <?php echo print_r($out->sent); ?>
            </pre>

            <br />
            <br />

            <b>Received</b>

            <pre>
                <?php echo print_r($out); ?>
            </pre>

            <?php

        }
        */

        ?>

    </div>

    <?php

}

?>