<?php

if (!defined('WWW_ROOT')) exit;

###########################################################

// EXTENSION GLOBALS CLASS
require_once(SERVER_ROOT. '/uccms/includes/classes/extension-globals.php');

// INIT
$_extglobals = new ExtensionGlobals();

// DEFAULTS
$page   = ($_REQUEST['page'] ? (int)$_REQUEST['page'] : 0);
$limit  = ($_REQUEST['limit'] ? (int)$_REQUEST['limit'] : 10);
$offset = $page * $limit;

###########################################################


$out = array();

$vars = array();

if ($_REQUEST['module_id']) {
    $vars['module_id'] = (int)$_REQUEST['module_id'];
}

$params = array(
    'query' => array(
        'orderby' => array(
            'field'     => 'dt',
            'direction' => 'desc'
        ),
        'limit' => array(
            'num'       => $limit,
            'offset'    => $offset
        )
    )
);

if ($_REQUEST['q']) {
    $params['q'] = sqlescape($_REQUEST['q']);
}

if ($_REQUEST['date_from']) {
    $params['date']['from'] = date('Y-m-d', strtotime(urldecode($_REQUEST['date_from'])));
}
if ($_REQUEST['date_to']) {
    $params['date']['to'] = date('Y-m-d', strtotime(urldecode($_REQUEST['date_to'])));
}

//$out['params'] = $params;

// GET DATA
$data = $_extglobals->getMethodData('payment_transactions', $vars, $params);

// COMBINE EXTENSIONS
$data = $_extglobals->combineMethodData($data);

// SORT DATA
$data = $_extglobals->sortData($data, 'dt', 'desc', $limit);

// LOOP
foreach ($data as $item) {

    // FORMAT DATE
    $item['dt'] = date('n/j/Y g:i a', strtotime($item['dt']));

    // FORMAT METHOD
    $item['method'] = ucwords(str_replace(array('-', '_'), array(' ', ' / '), $item['method']));

    // FORMAT DESCRIPTION
    if ($item['lastfour']) {
        $item['description'] = trim($item['description']. ' (' .$item['lastfour']. ')');
    }

    // FORMAT AMOUNT
    $item['amount'] = '$' .number_format($item['amount'], 2);

    $out[] = $item;

}

echo json_encode($out);

?>