<?php

// CLEAN UP
$val = trim($_REQUEST['val']);

// HAVE VALUE
if ($val) {

    // BUSINESS LISTING CLASS'S
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/base.php');
    require_once(SERVER_ROOT. 'uccms/includes/classes/business-listing/admin.php');

    // INIT BUSINESS LISTING CLASS
    $_blisting = new BusinessListing_Admin();

    // LOCALEZE CLASS
    require_once(SERVER_ROOT. 'uccms/includes/classes/localeze.php');

    // INIT LOCALEZE CLASS
    $_localeze = new Localeze();

    // GET CATEGORY DATA
    $cdata = $_blisting->getCategories($val);

    //echo print_r($cdata);
    //exit;

    // HAVE RESULTS
    if (count($cdata) > 0) {

        ?>

        <div style="padding-bottom: 5px;">
            Keywords:
        </div>
        <select name="">
            <?php

            // LOOP THROUGH GROUPS
            foreach ($cdata as $group_id => $group) {

                // HAVE KEYWORDS
                if (count($group['attributes']) > 0) {
                    ?>
                    <optgroup label="<?=$group['title']?>" data-id="<?=$group_id?>">
                        <?php

                        // LOOP THROUGH KEYWORDS
                        foreach ($group['attributes'] as $l2_id => $l2) {

                            ?>
                            <option value="<?=$l2_id?>"><?=$l2['title']?></option>
                            <?php

                            /*
                            // HAVE LEVEL 3
                            if (count($l2['attributes']) > 0) {
                                foreach ($l2['attributes'] as $l3_id => $l3) {

                                    ?>
                                    <option value="<?=$l3_id?>">---- <?=$l3['title']?></option>
                                    <?php

                                }

                            }
                            */

                        }

                        ?>
                    </optgroup>
                    <?php

                }

            }

            ?>
        </select> <a href="javascript:void(0);" title="Add Keyword" class="add" style="font-size: 1.2em; font-weight: bold;" data-what="<?=$_REQUEST['what']?>">+</a>

        <?php

    } else {
        echo 'No results.';
    }

}

?>