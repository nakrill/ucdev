<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

?>

<div class="search">
    Or use existing content: <input type="text" name="" value="" placeholder="Search" />
</div>

<div class="results">
    <div class="loading">Getting results..</div>
    <div class="items"></div>
</div>

<script type="text/javascript">

    // DONE TYPING
    function lb_doSearch () {

        $('#wysiwyg_link_browser .results .items').hide();
        $('#wysiwyg_link_browser .results .loading').show();

        $.get('/admin/ajax/wysiwyg/link_browser/get/', {
            q: $('#wysiwyg_link_browser .search input[type="text"]').val()
        }, function(data) {
            $('#wysiwyg_link_browser .results .loading').hide();
            $('#wysiwyg_link_browser .results .items').html(data).show();
        }, 'html');

    }

    $(document).ready(function() {

        var lb_typingTimer;

        // INITIAL RESULTS
        lb_doSearch();

        // ON KEYUP, START THE COUNTDOWN
        $('#wysiwyg_link_browser .search input[type="text"]').on('keyup', function () {
            clearTimeout(lb_typingTimer);
            lb_typingTimer = setTimeout(lb_doSearch, 600);
            $('#wysiwyg_link_browser .results .items').hide();
            $('#wysiwyg_link_browser .results .loading').show();
        });

        // ON KEYDOWN, CLEAR THE COUNTDOWN
        $('#wysiwyg_link_browser .search input[type="text"]').on('keydown', function () {
            clearTimeout(lb_typingTimer);
        });

        // ITEM CLICK
        $('#wysiwyg_link_browser .results .items').on('click', '.item', function(e) {
            e.preventDefault();

            var url = $(this).attr('href');

            var parent = $('#wysiwyg_link_browser').closest('.mce-container.mce-formitem');

            // URL
            parent.prev('.mce-first').find('.mce-textbox').val(url);

            // TEXT TO DISPLAY
            if (!tinyMCE.activeEditor.selection.getContent()) {
                parent.next('.mce-container').find('.mce-textbox').val($(this).find('.title').text());
            }

        });

    });

</script>