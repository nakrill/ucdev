<?php

// REQUIRE ADMIN
$admin->requireLevel(1);

require_once(SERVER_ROOT. 'uccms/includes/classes/global-search.php');

// NEW GLOBAL SEARCH
$_gs = new uccms_globalSearch();

// GET RESULTS
$results = $_gs->search($_REQUEST['q']);

//echo print_r($results);

// HAVE RESULTS
if (count($results) > 0) {

    // LOOP
    foreach ($results as $result) {
        ?>
        <a href="<?php echo $result['url']; ?>" class="item clearfix <?php echo $result['type']; ?>">
            <span class="title"><?php echo $result['title']; ?></span>
            <span class="type"><?php echo $result['type']; ?></span>
        </a>
        <?php
    }

} else {
    ?>
    <div class="no-results">No results found.</div>
    <?php
}

?>