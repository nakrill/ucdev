// PAGE LOADED
$(function() {

    // SCROLLING SITE TITLE
    if ($('#sidebar .sidebar-header h3 span').width() > 220) {
        $('#sidebar .sidebar-header h3 span').css({
            'position': 'absolute',
            'padding-right': '30px'
        });
    }

    // BIGTREE TABS
    $('.tabbed-header header nav a').click(function(e) {
        e.preventDefault();
        var $parent = $(this).closest('.tabbed-header');
        $parent.find('header nav a').removeClass('active');
        $parent.find('section.tab').removeClass('active');
        $(this).addClass('active');
        $parent.find('section.tab.tab-' +$(this).attr('data-tab')).addClass('active');
    });

    // CONTAINER TOGGLE
    $('.container.expand header').click(function(e) {
        e.preventDefault();
        var pm = $(this).find('.plus-minus');
        $(this).siblings().slideToggle(400);
        if (pm.hasClass('fa-plus')) {
            pm.removeClass('fa-plus').addClass('fa-minus');
        } else {
            pm.removeClass('fa-minus').addClass('fa-plus');
        }
    });

    // CONTENT TOGGLE
    $('.uccms_toggle').click(function(e) {
        e.preventDefault;
        var what = $(this).attr('data-what');
        /*
        if ($(this).find('.icon_small').hasClass('icon_small_caret_right')) {
            $.cookie('bigtree_admin[page_properties_open]', 'on', { expires: 365, path: '/' });
        } else {
            $.cookie('bigtree_admin[page_properties_open]', '', { path: '/' });
        }
        */
        $(this).next('.uccms_toggle-' +what).slideToggle(400); //.css('margin-top', '10px')
        $(this).find('.icon_small').toggleClass('icon_small_caret_right').toggleClass('icon_small_caret_down');
    });

    // EQUAL HEIGHT ELEMENTS
    equalHeight = function(container) {
        var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
        $(container).each(function() {
            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;
            if (currentRowStart != topPostion) {
                for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }

    // PLUS / MINUS INPUT
    $('body').on('click', '.input-group .btn-number', function(e) {
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $(".input-group input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {
                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }
            } else if(type == 'plus') {
                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }
            }
        } else {
            input.val(0);
        }
    });
    $('body').on('focusin', '.input-group .input-number', function(e) {
       $(this).data('oldValue', $(this).val());
    });
    $('body').on('change', '.input-group .input-number', function(e) {
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".input-group .btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".input-group .btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
    });
    $('body').on('keydown', '.input-group .input-number', function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

});


// CONFIRM PROMPT
/* <a href="http://www.google.com/" onclick="return confirmPrompt(this.href, 'Are you sure you want to?');">Test</a> */
function confirmPrompt(url, msg) {
    if (confirm(msg)) window.location = url;
    return false;
}

// REMOVE PARAMETER FROM URL
function removeURLParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

// PERCENT TO HSL
function percentageToHsl(pos, start, end, s, l) {
    pos = pos / 100;
    if (!start) start = 0;
    if (!end) end = 130;
    if (!s) s = 75;
    if (!l) l = 50;
    var hue = (pos * (end - start)) + start;
    return 'hsl(' +hue+ ', ' +s+ '%, ' +l+ '%)';
}

// WAIT BEFORE EXECUTING
var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();