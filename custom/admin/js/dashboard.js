$(function() {

    // SIDEBAR SCROLLING
    const perfectScrollbar = new PerfectScrollbar('#sidebar ul.components');
    $('#sidebar ul.components').on('shown.bs.collapse', function() {
        perfectScrollbar.update();
    });

    // SIDEBAR EXPAND / COLLAPSE
    $('#sidebarCollapse').on('click', function(e) {
        e.preventDefault();
        var active = $('#sidebar').hasClass('active');
        if (active) {
            $('#sidebar, #content').removeClass('active');
        } else {
            $('#sidebar, #content').addClass('active');
        }
    });

    // MODAL LAYERING
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

});